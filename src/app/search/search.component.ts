import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { FormControl, ReactiveFormsModule, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { GeoplzService, Area } from '../geoplz.service';
import { Place } from '../account/account.component';
import { AngularFireStorage } from '@angular/fire/storage';
import { ActivatedRoute, Router } from '@angular/router';
import { MatInput } from '@angular/material/input';


@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {

  resultList: Place[];
  filteredAreas: Observable<Area[]>;
  geoplzService: GeoplzService;
  showHome = true;
  plzSearch = new FormControl('', [Validators.required]);
  @ViewChild('searchfield') searchfield;

  constructor(geoplz: GeoplzService, private storage: AngularFireStorage, private route: ActivatedRoute, private router: Router) {
    this.geoplzService = geoplz;
    this.filteredAreas = this.plzSearch.valueChanges
      .pipe(
        startWith(''),
        map(state => state ? this.getAreas(state) : null)
      );
  }

  ngOnInit(): void {
    var plz = this.route.snapshot.paramMap.get('plz');
    if (plz != null && plz != "") {
      var area = this.getAreas(plz)[0];
      this.setSearchboxValue(area);
      this.updateList(plz);
    }
  }

  ngAfterViewInit() {
    try {
      this.searchfield.nativeElement.focus();
      window.scroll(0, 0);
    }
    catch(err) {
      //TODO
    }    
  }

  setSearchboxValue(p: Area) {
    this.plzSearch.setValue(p.plz + " " + p.name);
    this.showHome = false;
  }

  private getAreas(value: string): Area[] {
    const filterValue = value.toLowerCase();
    if (filterValue.length > 1) {
      if (this.firstLetterIsNumber(filterValue)) {
        return GeoplzService.getAreaData().filter(area => area.plz.toLowerCase().indexOf(filterValue.match(/\d{1,5}/)[0]) === 0);
      }
      else {
        return GeoplzService.getAreaData().filter(area => area.name.toLowerCase().indexOf(filterValue.match(/[^\d]+/)[0].replace(/\s/g, "")) === 0);
      }
    }
    return null;
  }

  firstLetterIsNumber(str) {
    if (str.length > 0) {
      if (str.charAt(0).match(/[0-9]/i)) {
        return true;
      }
    }
    return false;
  }

  onSearch() {
    var area = this.getAreas(this.plzSearch.value)[0];
    if (area != null) {
      this.setSearchboxValue(area);
      this.router.navigate(['/suche/' + area.plz]);
      this.updateList(area.plz);
    }
  }

  updateList(plz: string) {
    this.geoplzService.getPlacesNearby(plz).then((plArray) => {
      this.resultList = plArray;
      this.resultList.forEach((placeElement) => { this.getImageStorageURL(placeElement.id).then((url) => { placeElement.imageurl = url }); });
    });
  }

  getImageStorageURL(uid: string) {
    return new Promise<string>((resolve, reject) => {
      var starsRef = this.storage.storage.ref(uid);
      starsRef.getDownloadURL().then((url) => {
        resolve(url);
      }).catch((error) => {
        console.log("could not load image");
        resolve(null);
      });
    });
  }

}
