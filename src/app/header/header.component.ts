import { Component, OnInit, Input } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import {Location} from '@angular/common';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  userMail : string;
  @Input() showBack: boolean;
  @Input() color: string = "#000000";

  constructor(public auth: AngularFireAuth, private location : Location) { 
  }

  ngOnInit(): void {
    if(this.auth.auth.currentUser != null){
      this.userMail = this.auth.auth.currentUser.email;
    }
  }

  backClicked() {
    this.location.back();
  }

}
