import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-cookiebanner',
  templateUrl: './cookiebanner.component.html',
  styleUrls: ['./cookiebanner.component.scss']
})
export class CookiebannerComponent implements OnInit {

  static sta_accepted: boolean = false;
  accepted: boolean = CookiebannerComponent.sta_accepted;

  constructor() { }

  ngOnInit(): void {
  }

  accept(){
    CookiebannerComponent.sta_accepted = true;
    this.accepted = CookiebannerComponent.sta_accepted;
  }

}
