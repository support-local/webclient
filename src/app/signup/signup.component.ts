import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {

  signupForm: FormGroup;
  suSubmitted = false;
  suSuccess = 0;
  suHidePW = true;
  tosaccept = false;
  suNoTos = false;

  constructor(public auth: AngularFireAuth, private firestore: AngularFirestore, private router: Router) { 
    this.signupForm = this.createSignupFormGroup();
  }

  ngOnInit(): void {
  }

  createSignupFormGroup() {
    return new FormGroup({
      suEmail: new FormControl('', [Validators.required, Validators.email]),
      suPassword: new FormControl('', [Validators.required, Validators.minLength(6)]),
      suName: new FormControl('', [Validators.required]),
      //suPLZ: new FormControl('', [Validators.required])
    });
  }

  suSubmit() {
    if (this.signupForm.valid) {
      if(!this.tosaccept){
        this.suNoTos = true;
        return;
      }
      this.suSubmitted = true;
      this.suSuccess = 0;
      this.signUp(this.signupForm.controls['suEmail'].value, this.signupForm.controls['suPassword'].value, this.signupForm.controls['suName'].value /*, this.signupForm.controls['suPLZ'].value */);
    }
  }

  signUp(email, password, name /*, PLZ*/) {
    this.auth.auth.createUserWithEmailAndPassword(email, password)
      .then((userRecord) => {
        console.log('Successfully created new user:', userRecord.user.uid);

        var data = {
          name: name,
          //plz: PLZ
        };

        this.firestore
          .collection("places").doc(userRecord.user.uid)
          .set(data)
          .then(res => {
            console.log(res);
            this.suSuccess = 1;
            this.router.navigate(['/account']);
          },
            err => {
              this.suSuccess = -1;
            });

      })
      .catch((err) => {
        this.suSuccess = -1;
      });
  }

}
