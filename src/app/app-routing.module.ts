import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SearchComponent } from './search/search.component';
import { PlaceComponent } from './place/place.component';
import { LoginComponent } from './login/login.component';
import { AccountComponent } from './account/account.component';
import { ImpressumComponent } from './impressum/impressum.component';
import { PrivacypolComponent } from './privacypol/privacypol.component';
import { TermsofserviceComponent } from './termsofservice/termsofservice.component';
import { AngularFireAuthGuard, redirectUnauthorizedTo, redirectLoggedInTo } from '@angular/fire/auth-guard';
import { HomeComponent } from './home/home.component';

const redirectUnauthorizedToLogin = () => redirectUnauthorizedTo(['login']);
const redirectLoggedInToAccount = () => redirectLoggedInTo(['account']);

const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
  },
  {
    path: 'suche',
    component: SearchComponent,
  },
  {
    path: 'suche/:plz',
    component: SearchComponent,
  },
  {
    path: 'login',
    component: LoginComponent, canActivate: [AngularFireAuthGuard], data: { authGuardPipe: redirectLoggedInToAccount }
  },
  {
    path: 'place/:id',
    component: PlaceComponent, 
  },
  {
    path: 'account',
    component: AccountComponent, canActivate: [AngularFireAuthGuard], data: { authGuardPipe: redirectUnauthorizedToLogin }
  },
  {
    path: 'impressum',
    component: ImpressumComponent,
  },
  {
    path: 'datenschutz',
    component: PrivacypolComponent,
  },
  {
    path: 'nutzungsbedingungen',
    component: TermsofserviceComponent,
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes, {scrollPositionRestoration: 'top'})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
