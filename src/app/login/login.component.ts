import { Component, OnInit, SystemJsNgModuleLoader } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmDialogComponent } from '../shared/confirm-dialog/confirm-dialog.component';
//import { auth } from 'firebase/app';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  loSubmitted = false;
  loSuccess = 0;
  prSuccess = 0;
  loHidePW = true;

  constructor(public auth: AngularFireAuth, private firestore: AngularFirestore, private router: Router, private dialog: MatDialog) {
    this.loginForm = this.createLoginFormGroup();
  }

  ngOnInit(): void {
  }

  createLoginFormGroup() {
    return new FormGroup({
      loEmail: new FormControl('', [Validators.required, Validators.email]),
      loPassword: new FormControl('', [Validators.required, Validators.minLength(6)]),
    });
  }

  /*revert() {
    this.signupForm.reset();
  }*/


  loSubmit() {
    if (this.loginForm.valid) {
      this.loSubmitted = true;
      this.loSuccess = 0;
      this.login(this.loginForm.controls['loEmail'].value, this.loginForm.controls['loPassword'].value);
    }
  }

  openPasswordResetDialog(): void {
    this.prSuccess = 0;
    if (this.loginForm.controls['loEmail'].valid) {
      const dialogRef = this.dialog.open(ConfirmDialogComponent, {
        width: '350px',
        data: "Möchten Sie eine Passwort-Reset Mail an '" + this.loginForm.controls['loEmail'].value + "' senden?"
      });
      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          this.auth.auth.sendPasswordResetEmail(this.loginForm.controls['loEmail'].value).then(() => {
            this.prSuccess = 1;
          }).catch((error) => {
            this.prSuccess = -2;
          });
          
        }
      });
    }else{
      this.prSuccess = -1;
    }
  }




  //Firebase Functions

  

  updatePlaceInfo() {

  }

  login(email, password) {
    this.auth.auth.signInWithEmailAndPassword(email, password).then(() => {
      this.router.navigate(['/account']);
    }).catch((err) => {
      this.loSubmitted = false;
      this.loSuccess = -1;
    });
  }

}
