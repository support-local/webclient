import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrivacypolComponent } from './privacypol.component';

describe('PrivacypolComponent', () => {
  let component: PrivacypolComponent;
  let fixture: ComponentFixture<PrivacypolComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrivacypolComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrivacypolComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
