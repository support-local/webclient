import { Component, OnInit, SystemJsNgModuleLoader } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';
import { FormControl, FormGroup, Validators, FormBuilder, FormArray } from '@angular/forms';
import { AngularFirestore } from '@angular/fire/firestore';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmDialogComponent } from '../shared/confirm-dialog/confirm-dialog.component';
import { MessageDialogComponent } from '../shared/message-dialog/message-dialog.component';
import { GeoplzService } from '../geoplz.service';
import { AngularFireStorage } from '@angular/fire/storage';
import { Observable } from 'rxjs';
import { getTreeMissingMatchingNodeDefError } from '@angular/cdk/tree';

export class Place {
  name: string;
  plz: string;
  location: Object;
  geohash: string;
  adress: string;
  times: string;
  about: string;
  delivery: boolean;
  email: string;
  phone: string;
  offers: Offer[];

  //internal
  imageurl: string;
  id: string;
}

export class Offer {
  name: string;
  price: string;
}

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.scss']
})
export class AccountComponent implements OnInit {

  fieldArray: Array<any> = [];
  placeForm: FormGroup;
  public lokal: Place = new Place();
  plSaveState = 0;
  plUploadingImg = false;
  imageURL: string;

  constructor(public auth: AngularFireAuth, private firestore: AngularFirestore, private router: Router, private dialog: MatDialog, private formBuilder: FormBuilder, private afStorage: AngularFireStorage) {
    this.placeForm = this.createPlaceGroup();
  }

  ngOnInit(): void {

    this.firestore.collection('places').doc<Place>(this.auth.auth.currentUser.uid)
      .get().subscribe((doc) => {
        this.lokal = doc.data() as Place;
        this.lokal.offers.forEach((element) => {
          var offer: Offer = element as Offer;
          this.addExistingOffer(offer.name, offer.price)
        });
        //this.placeForm.controls['plName'].setValue(this.lokal.name);
      });
    this.updateImage();
  }

  createPlaceGroup() {
    return new FormGroup({
      plName: new FormControl('', [Validators.required]),
      plPLZ: new FormControl('', [Validators.required]),
      plAdress: new FormControl('', [Validators.required]),
      plTimes: new FormControl('', [Validators.required]),
      plAbout: new FormControl('', [Validators.required]),

      plDeliv: new FormControl(true, [Validators.required]),
      plMail: new FormControl('', [Validators.required, Validators.email]),
      plPhone: new FormControl('', [Validators.required]),
      offers: this.formBuilder.array([])
    });
  }

  get offersFormArray() {
    return this.placeForm.get('offers') as FormArray;
  }

  initOffer() {
    return this.formBuilder.group({
      ofName: new FormControl('', [Validators.required]),
      ofPrice: new FormControl('', [Validators.required])

    });
  }
  addExistingOffer(name, price) {
    this.offersFormArray.push(
      this.formBuilder.group({
        ofName: new FormControl(name, [Validators.required]),
        ofPrice: new FormControl(price, [Validators.required])

      })
    );
  }
  addOffer() {
    this.offersFormArray.push(this.initOffer());
  }
  removeOffer(i: number) {
    this.offersFormArray.removeAt(i);
  }

  updateImage() {
    this.plUploadingImg = true;
    var starsRef = this.afStorage.storage.ref(this.auth.auth.currentUser.uid);
    starsRef.getDownloadURL().then((url) => {
      this.imageURL = url;
      console.log(this.imageURL);
      this.plUploadingImg = false;
    }).catch((error) => {
      console.log("could not load image");
      this.plUploadingImg = false;
    });
  }

  uploadImage(event) {
    const file = event.target.files[0];
    //var filetype = (file.type == "image/png") ? ".png" : ".jpg";
    const filePath = this.auth.auth.currentUser.uid;
    const ref = this.afStorage.ref(filePath);
    const task = ref.put(file).then((snapshot) => {
      this.updateImage();
    });
  }



  plSave() {
    if (this.placeForm.valid) {

      var offers: Offer[] = [];
      for (let i = 0; i < this.offersFormArray.length; i++) {
        var offerElement = this.offersFormArray.at(i) as FormGroup;
        offers.push({
          name: offerElement.controls['ofName'].value,
          price: offerElement.controls['ofPrice'].value
        })
      }

      var place: Object = GeoplzService.getObjectFromPLZ(this.placeForm.controls['plPLZ'].value);
      if (place == null) {
        this.dialog.open(MessageDialogComponent, {
          width: '350px',
          data: "Die von Ihnen angegebene PLZ kann nicht gefunden werden."
        });
        return;
      }


      this.lokal.name = this.placeForm.controls['plName'].value;
      this.lokal.location = place;
      this.lokal.plz = this.placeForm.controls['plPLZ'].value;

      this.lokal.adress = this.placeForm.controls['plAdress'].value;
      this.lokal.times = this.placeForm.controls['plTimes'].value;
      this.lokal.about = this.placeForm.controls['plAbout'].value;
      this.lokal.delivery = this.placeForm.controls['plDeliv'].value;
      this.lokal.email = this.placeForm.controls['plMail'].value;
      this.lokal.phone = this.placeForm.controls['plPhone'].value;
      this.lokal.offers = offers;
      //this.lokal.offers = this.placeForm.controls['plName'].value;

      this.plSaveState = 1;

      this.firestore.collection('places').doc(this.auth.auth.currentUser.uid).update(this.lokal).then(() => {
        this.plSaveState = 2;
      });
    }
  }


  logout() {
    this.auth.auth.signOut().then(() => {
      this.router.navigate(['/']);
    });
  }

  sendPasswordReset() {
    this.auth.auth.sendPasswordResetEmail(this.auth.auth.currentUser.email).then(function () {
      // Email sent.
    }).catch(function (error) {
      // An error happened.
    });

  }


  deleteAccount(): void {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: '350px',
      data: "Möchten Sie ihr Konto mit der Email-Adresse '" + this.auth.auth.currentUser.email + "' wirklich unwiederruflich löschen?"
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.auth.auth.currentUser.delete().then(() => {
          this.dialog.open(MessageDialogComponent, {
            width: '350px',
            data: "Ihr Account wurde erfolgreich gelöscht."
          });
          this.router.navigate(['/']);
        }).catch((error) => {
          console.log(error);
          this.dialog.open(MessageDialogComponent, {
            width: '350px',
            data: "Aus technischen Gründen bitten wir sie sich erneut anzumelden um Ihr Konto zu löschen"
          });
        });

      }
    });
  }









  // Dialog functions





}
