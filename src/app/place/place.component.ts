import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Place } from '../account/account.component';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireStorage } from '@angular/fire/storage';

@Component({
  selector: 'app-place',
  templateUrl: './place.component.html',
  styleUrls: ['./place.component.scss']
})
export class PlaceComponent implements OnInit {

  place : Place = new Place();
  phone = "+49 123 456 7890"
  mail = "info.laden@example.com";
  
  protMail : string;
  protTel : string;

  constructor(private route: ActivatedRoute , private firestore : AngularFirestore , private storage: AngularFireStorage) { 
    
  }

  ngOnInit(): void {
    var id = this.route.snapshot.paramMap.get('id');
    if(id != null && id != ""){
      var plDocRef = this.firestore.doc<Place>('places/' + id);
      var plDoc = plDocRef.snapshotChanges();
      plDoc.subscribe((doc) => {this.place = doc.payload.data(); this.getImageStorageURL(id).then((img)=>{this.place.imageurl = img}); console.log(this.place);});
    }
  }

  protectMail(adress){
   return adress.replace(new RegExp(/@/, 'g'), " [at] ").replace(new RegExp(/\./, 'g'), " [dot] ");
  }

  getImageStorageURL(uid: string) {
    return new Promise<string>((resolve, reject) => {
      var starsRef = this.storage.storage.ref(uid);
      starsRef.getDownloadURL().then((url) => {
        resolve(url);
      }).catch((error) => {
        console.log("could not load image");
        resolve(null);
      });
    });
  }

  //show sensitive data buttons
  showTel(){
    this.protTel = this.phone;
  }
  showMail(){
    this.protMail = this.mail;
  }



  //action buttons
  sendCall(){
    //console.log('sending mail');
    window.location.href = "tel:" + this.phone;
  }

  sendMail(){
    //console.log('sending call');
    window.location.href = "mailto:" + this.mail;
  }

}
