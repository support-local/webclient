import { Injectable } from '@angular/core';
import * as geofirex from 'geofirex';
import * as fb from 'firebase/app';
import * as firebase from 'firebase/app';
import { interval } from 'rxjs';
import { Place } from './account/account.component';


export interface Area {
  lati: number;
  longi: number;
  name: string;
  plz: string;
}

@Injectable({
  providedIn: 'root'
})

export class GeoplzService {

  firebase;
  static staticGeo = geofirex.init(firebase);
  geo;

  constructor() {

    var timer = setInterval(() => {
      if (fb.app()) {
        this.firebase = fb.app();
        this.geo = geofirex.init(this.firebase);
        this.getPlacesNearby("79183");
        console.log(this.firebase);
        clearInterval(timer);
      }
    }, 100)
    console.log("finalized");
  }

  static getAreaData(): Area[] {
    return areas;
  }

  static getAreaFromPLZ(plz: string): Area {
    return areas.find((area) => (area.plz == plz));
  }

  static getObjectFromPLZ(plz: string) {
    var area: Area = GeoplzService.getAreaFromPLZ(plz);
    if (area == null) {
      return null;
    }
    return this.staticGeo.point(area.lati, area.longi);
  }


  getPlacesNearby(plz: string) {

    return new Promise<Place[]>((resolve, reject) => {
      var timer = setInterval(() => {
        if (this.geo != null) {
          console.log("query");
          const center = GeoplzService.getObjectFromPLZ(plz);
          if (center != null) {
            const cities = this.firebase.firestore().collection('places');
            const query = this.geo.query(cities).within(center, 25, 'location');
            query.subscribe((result) => { resolve(result) });
          }
          clearInterval(timer);
        }
      }, 100)
    });
  }

}
export { areas };
const areas: Area[] = [
  {
    lati: 50.1887724285, longi: 8.5159547242,
    name: "Kronberg im Taunus",
    plz: "61476"
  },
  {
    lati: 48.2017434124, longi: 8.5841525734,
    name: "Villingendorf",
    plz: "78667 "
  },
  {
    lati: 48.7639748523, longi: 11.2325978452,
    name: "Bergheim",
    plz: "86673"
  },
  {
    lati: 51.1303193455, longi: 6.4483157691,
    name: "Mönchengladbach",
    plz: "41199"
  },
  {
    lati: 49.8816705909, longi: 9.51714833018,
    name: "Esselbach",
    plz: "97839"
  },
  {
    lati: 51.3445565996, longi: 12.4079946628,
    name: "Leipzig",
    plz: "04315"
  },
  {
    lati: 52.6420898308, longi: 13.0396286947,
    name: "Schönwalde",
    plz: "14621"
  },
  {
    lati: 49.0302666477, longi: 12.1832656782,
    name: "Tegernheim",
    plz: "93105"
  },
  {
    lati: 52.9769460421, longi: 10.6731183596,
    name: "Rätzlingen",
    plz: "29590"
  },
  {
    lati: 52.5716456619, longi: 13.5417479086,
    name: "Berlin Falkenberg",
    plz: "13057"
  },
  {
    lati: 48.6748285897, longi: 13.529336818,
    name: "Büchlberg",
    plz: "94124"
  },
  {
    lati: 51.4317822689, longi: 6.83380029025,
    name: "Mülheim an der Ruhr",
    plz: "45478"
  },
  {
    lati: 51.5659989379, longi: 6.97760005069,
    name: "Gladbeck",
    plz: "45964"
  },
  {
    lati: 51.4318392711, longi: 7.79290671693,
    name: "Menden",
    plz: "58706"
  },
  {
    lati: 52.0893613017, longi: 8.66017670765,
    name: "Herford",
    plz: "32052"
  },
  {
    lati: 49.7414441275, longi: 9.01120884125,
    name: "Bad König",
    plz: "64732"
  },
  {
    lati: 47.9154549039, longi: 9.02188431382,
    name: "Mühlingen",
    plz: "78357"
  },
  {
    lati: 47.841544479, longi: 9.31106384674,
    name: "Heiligenberg",
    plz: "88633"
  },
  {
    lati: 49.4363126143, longi: 8.5712540864,
    name: "Mannheim",
    plz: "68229"
  },
  {
    lati: 51.2287454676, longi: 6.7558654604,
    name: "Düsseldorf",
    plz: "40545"
  },
  {
    lati: 52.0995248067, longi: 7.29750702403,
    name: "Horstmar",
    plz: "48612"
  },
  {
    lati: 50.5596481483, longi: 7.39046098103,
    name: "Breitscheid, Dattenberg, Hausen, Hümmerich, Kasbach-Ohlenberg, Roßbach u",
    plz: "53547"
  },
  {
    lati: 52.3092083169, longi: 9.65610403963,
    name: "Ronnenberg",
    plz: "30952"
  },
  {
    lati: 54.6529546613, longi: 9.86349098128,
    name: "Rabenkirchen-Faulück",
    plz: "24407"
  },
  {
    lati: 47.8267671003, longi: 12.4774575821,
    name: "Übersee",
    plz: "83236"
  },
  {
    lati: 51.0568350909, longi: 12.6851018134,
    name: "Geithain",
    plz: "04643"
  },
  {
    lati: 51.0740837913, longi: 13.035762499,
    name: "Waldheim",
    plz: "04736"
  },
  {
    lati: 51.3622850019, longi: 11.2890094491,
    name: "Artern/Unstrut u.a.",
    plz: "06556"
  },
  {
    lati: 49.7612948458, longi: 11.3230039175,
    name: "Gößweinstein",
    plz: "91327"
  },
  {
    lati: 53.6610037163, longi: 13.8425481541,
    name: "Ferdinandshof u.a.",
    plz: "17379"
  },
  {
    lati: 49.6936883875, longi: 11.609826338,
    name: "Auerbach i.d. OPf.",
    plz: "91275"
  },
  {
    lati: 51.9700386779, longi: 11.8427365963,
    name: "Barby",
    plz: "39249"
  },
  {
    lati: 48.2260007108, longi: 10.6303306812,
    name: "Mickhausen",
    plz: "86866"
  },
  {
    lati: 52.4640143232, longi: 13.3746468492,
    name: "Berlin Tempelhof",
    plz: "12103"
  },
  {
    lati: 48.0077636705, longi: 7.85410590237,
    name: "Freiburg im Breisgau",
    plz: "79106"
  },
  {
    lati: 48.1922557464, longi: 7.800460208,
    name: "Kenzingen",
    plz: "79341"
  },
  {
    lati: 48.0489453507, longi: 8.14230334105,
    name: "Gütenbach",
    plz: "78148"
  },
  {
    lati: 51.5512606922, longi: 6.19889923674,
    name: "Kevelaer-Twisteden",
    plz: "47624"
  },
  {
    lati: 51.6552490462, longi: 7.06069302455,
    name: "Marl",
    plz: "45768"
  },
  {
    lati: 52.515156122, longi: 7.18006901693,
    name: "Wietmarschen",
    plz: "49835"
  },
  {
    lati: 48.0470416553, longi: 8.73166994704,
    name: "Hausen ob Verena",
    plz: "78595"
  },
  {
    lati: 50.337827057, longi: 10.0099016694,
    name: "Sandberg",
    plz: "97657"
  },
  {
    lati: 49.7031027948, longi: 10.0179287372,
    name: "Winterhausen",
    plz: "97286"
  },
  {
    lati: 49.7188932174, longi: 7.64736916114,
    name: "Rehborn",
    plz: "55592"
  },
  {
    lati: 52.6776971224, longi: 7.96815294683,
    name: "Quakenbrück",
    plz: "49610"
  },
  {
    lati: 47.7016427625, longi: 8.38869907842,
    name: "Eggingen",
    plz: "79805"
  },
  {
    lati: 50.1595812514, longi: 8.69650106189,
    name: "Frankfurt am Main",
    plz: "60435"
  },
  {
    lati: 49.6990752155, longi: 6.50065922922,
    name: "Pellingen",
    plz: "54331"
  },
  {
    lati: 51.2292748297, longi: 6.79187205206,
    name: "Düsseldorf",
    plz: "40211"
  },
  {
    lati: 51.1391654084, longi: 11.3978606473,
    name: "Buttstädt",
    plz: "99628"
  },
  {
    lati: 49.672678065, longi: 11.4154877757,
    name: "Betzenstein",
    plz: "91282"
  },
  {
    lati: 48.9847598742, longi: 11.9794359268,
    name: "Sinzing",
    plz: "93161"
  },
  {
    lati: 48.2254830272, longi: 9.64086595093,
    name: "Munderkingen",
    plz: "89597"
  },
  {
    lati: 53.1043158999, longi: 10.7192667892,
    name: "Himbergen",
    plz: "29584"
  },
  {
    lati: 51.3662436831, longi: 11.0094187814,
    name: "Kyffhäuserland",
    plz: "99707"
  },
  {
    lati: 54.0812909962, longi: 10.28370416,
    name: "Schmalensee",
    plz: "24638"
  },
  {
    lati: 51.5163571165, longi: 7.11922599007,
    name: "Gelsenkirchen",
    plz: "45888"
  },
  {
    lati: 49.3243418255, longi: 7.51008872209,
    name: "Wallhalben u.a.",
    plz: "66917"
  },
  {
    lati: 50.3072365971, longi: 9.78494856491,
    name: "Bad Brückenau",
    plz: "97769"
  },
  {
    lati: 48.1357662777, longi: 9.87309373538,
    name: "Maselheim",
    plz: "88437"
  },
  {
    lati: 49.4670766542, longi: 8.25719763861,
    name: "Ellerstadt",
    plz: "67158"
  },
  {
    lati: 50.0718546881, longi: 8.25926252868,
    name: "Wiesbaden",
    plz: "65189"
  },
  {
    lati: 52.261516285, longi: 8.22045432206,
    name: "Bissendorf",
    plz: "49143"
  },
  {
    lati: 48.640369654, longi: 9.0174022653,
    name: "Holzgerlingen",
    plz: "71088"
  },
  {
    lati: 49.1231360578, longi: 9.3247274568,
    name: "Lehrensteinsfeld",
    plz: "74251"
  },
  {
    lati: 54.5197294897, longi: 9.55799793704,
    name: "Schleswig",
    plz: "24837"
  },
  {
    lati: 53.8910043501, longi: 11.6947430334,
    name: "Neukloster",
    plz: "23992"
  },
  {
    lati: 52.3744582835, longi: 9.70607679619,
    name: "Hannover",
    plz: "30451"
  },
  {
    lati: 48.3147746873, longi: 9.7912835316,
    name: "Allmendingen",
    plz: "89604"
  },
  {
    lati: 48.1502665934, longi: 12.7228893141,
    name: "Burgkirchen an der Alz",
    plz: "84508"
  },
  {
    lati: 50.443329631, longi: 10.8755216616,
    name: "Eisfeld, Auengrund",
    plz: "98673"
  },
  {
    lati: 51.1206982524, longi: 6.63743074808,
    name: "Grevenbroich",
    plz: "41516"
  },
  {
    lati: 51.4716317261, longi: 7.26032281181,
    name: "Bochum",
    plz: "44803"
  },
  {
    lati: 53.4627514586, longi: 7.54791915031,
    name: "Aurich",
    plz: "26605"
  },
  {
    lati: 52.1722079287, longi: 7.83128139542,
    name: "Lengerich",
    plz: "49525"
  },
  {
    lati: 49.8280457456, longi: 9.11376155654,
    name: "Obernburg a.Main",
    plz: "63785"
  },
  {
    lati: 51.548961809, longi: 8.30916465167,
    name: "Anröchte",
    plz: "59609"
  },
  {
    lati: 50.5622912937, longi: 8.77769387256,
    name: "Fernwald",
    plz: "35463"
  },
  {
    lati: 49.6790343116, longi: 10.1020431664,
    name: "Frickenhausen",
    plz: "97252"
  },
  {
    lati: 52.5152028381, longi: 13.3056805796,
    name: "Berlin Charlottenburg",
    plz: "10585"
  },
  {
    lati: 53.8737966747, longi: 9.32152093565,
    name: "Brokdorf",
    plz: "25576"
  },
  {
    lati: 50.6155367152, longi: 6.95015103834,
    name: "Rheinbach",
    plz: "53359"
  },
  {
    lati: 50.9386555045, longi: 6.9550698736,
    name: "Köln",
    plz: "50667"
  },
  {
    lati: 51.2637066349, longi: 7.15569353545,
    name: "Wuppertal",
    plz: "42107"
  },
  {
    lati: 49.3401389306, longi: 7.18196518244,
    name: "Neunkirchen",
    plz: "66538"
  },
  {
    lati: 51.4531055181, longi: 7.23394109838,
    name: "Bochum",
    plz: "44799"
  },
  {
    lati: 51.3335461152, longi: 7.3401890916,
    name: "Gevelsberg",
    plz: "58285"
  },
  {
    lati: 54.3773852735, longi: 12.5825178988,
    name: "Barth",
    plz: "18356"
  },
  {
    lati: 49.9533161048, longi: 11.9386623857,
    name: "Ebnath",
    plz: "95683"
  },
  {
    lati: 50.9867743026, longi: 8.12749452596,
    name: "Hilchenbach",
    plz: "57271"
  },
  {
    lati: 50.8747651597, longi: 8.81827874966,
    name: "Cölbe",
    plz: "35091"
  },
  {
    lati: 52.7716099098, longi: 13.6378330848,
    name: "Biesenthal",
    plz: "16359"
  },
  {
    lati: 53.525734835, longi: 11.8245391403,
    name: "Domsühl, Mestlin, Obere Warnow u.a.",
    plz: "19374"
  },
  {
    lati: 48.4786318918, longi: 10.364173658,
    name: "Offingen",
    plz: "89362"
  },
  {
    lati: 51.3841432629, longi: 7.67658732134,
    name: "Iserlohn",
    plz: "58638"
  },
  {
    lati: 48.1321268962, longi: 11.5122375474,
    name: "München",
    plz: "80686"
  },
  {
    lati: 51.2530922058, longi: 6.38384528449,
    name: "Viersen",
    plz: "41747"
  },
  {
    lati: 50.0095839374, longi: 9.14423200416,
    name: "Glattbach",
    plz: "63864"
  },
  {
    lati: 49.9384665027, longi: 9.86293446234,
    name: "Thüngen",
    plz: "97289"
  },
  {
    lati: 54.3965017477, longi: 10.1595472143,
    name: "Kiel",
    plz: "24159"
  },
  {
    lati: 50.7498194496, longi: 7.65933681471,
    name: "Fürthen",
    plz: "57539"
  },
  {
    lati: 50.2514185436, longi: 7.83974595963,
    name: "Nastätten u.a.",
    plz: "56355"
  },
  {
    lati: 51.0550659916, longi: 8.40585731836,
    name: "Bad Berleburg",
    plz: "57319"
  },
  {
    lati: 50.7778169999, longi: 8.20886056222,
    name: "Haiger",
    plz: "35708"
  },
  {
    lati: 50.1949432641, longi: 9.54314317204,
    name: "Aura i. Sinngrund",
    plz: "97773"
  },
  {
    lati: 51.8236187784, longi: 7.06511271424,
    name: "Reken",
    plz: "48734"
  },
  {
    lati: 49.9115160754, longi: 7.26898470071,
    name: "Büchenbeuren",
    plz: "55491"
  },
  {
    lati: 51.0227420136, longi: 10.2607773308,
    name: "Marksuhl, Krauthausen u.a.",
    plz: "99819"
  },
  {
    lati: 54.3270294636, longi: 10.3684597993,
    name: "Fargau-Pratjau",
    plz: "24256"
  },
  {
    lati: 53.0186753725, longi: 8.62519289558,
    name: "Delmenhorst",
    plz: "27755"
  },
  {
    lati: 51.9870482303, longi: 12.3644685445,
    name: "Coswig (Anhalt)",
    plz: "06868"
  },
  {
    lati: 47.9687938501, longi: 11.2076544647,
    name: "Andechs",
    plz: "82346"
  },
  {
    lati: 49.5810599544, longi: 11.3451543542,
    name: "Schnaittach",
    plz: "91220"
  },
  {
    lati: 47.9535772215, longi: 10.6480941865,
    name: "Rieden",
    plz: "87668"
  },
  {
    lati: 48.7222115331, longi: 10.9207510858,
    name: "Niederschönenfeld",
    plz: "86694"
  },
  {
    lati: 48.3514346568, longi: 12.5163072729,
    name: "Neumarkt-Sankt Veit",
    plz: "84494"
  },
  {
    lati: 48.9512302912, longi: 13.2104504499,
    name: "Rinchnach",
    plz: "94269"
  },
  {
    lati: 54.5244405628, longi: 13.6147673341,
    name: "Sassnitz",
    plz: "18546"
  },
  {
    lati: 48.5531556074, longi: 7.9795927186,
    name: "Appenweier",
    plz: "77767"
  },
  {
    lati: 49.9073330351, longi: 9.55980801549,
    name: "Rothenfels",
    plz: "97851"
  },
  {
    lati: 48.3271036098, longi: 9.04048577596,
    name: "Jungingen",
    plz: "72417"
  },
  {
    lati: 48.6714080441, longi: 8.59659285212,
    name: "Neuweiler",
    plz: "75389"
  },
  {
    lati: 48.3611293345, longi: 10.161742316,
    name: "Pfaffenhofen a.d. Roth",
    plz: "89284"
  },
  {
    lati: 47.6961460552, longi: 10.3811514087,
    name: "Durach",
    plz: "87471"
  },
  {
    lati: 53.4061484144, longi: 9.7087118741,
    name: "Moisburg",
    plz: "21647"
  },
  {
    lati: 49.0027467252, longi: 9.77517004479,
    name: "Gaildorf",
    plz: "74405"
  },
  {
    lati: 48.0598161378, longi: 11.0250252233,
    name: "Windach",
    plz: "86949"
  },
  {
    lati: 51.0914070887, longi: 13.7448931831,
    name: "Dresden",
    plz: "01127"
  },
  {
    lati: 50.6004808225, longi: 10.702681228,
    name: "Suhl",
    plz: "98527"
  },
  {
    lati: 47.5672200337, longi: 10.7675122817,
    name: "Schwangau",
    plz: "87645"
  },
  {
    lati: 47.9960826326, longi: 11.4390119438,
    name: "Schäftlarn",
    plz: "82069"
  },
  {
    lati: 52.7862319691, longi: 13.0303030377,
    name: "Kremmen",
    plz: "16766"
  },
  {
    lati: 51.1818050048, longi: 6.39715368021,
    name: "Mönchengladbach",
    plz: "41069"
  },
  {
    lati: 51.9864501585, longi: 10.7251663821,
    name: "Osterwieck",
    plz: "38835"
  },
  {
    lati: 48.9263949671, longi: 12.8390359157,
    name: "Schwarzach",
    plz: "94374"
  },
  {
    lati: 54.0559975704, longi: 13.8087385429,
    name: "Wolgast",
    plz: "17438"
  },
  {
    lati: 52.3053210493, longi: 9.11253258928,
    name: "Helpsen, Seggebruch",
    plz: "31691"
  },
  {
    lati: 51.9333049536, longi: 12.4519830342,
    name: "Coswig (Anhalt)",
    plz: "06869"
  },
  {
    lati: 52.6929021864, longi: 11.404557837,
    name: "Kalbe, Bismark",
    plz: "39624"
  },
  {
    lati: 47.9510849127, longi: 9.36069927333,
    name: "Ostrach",
    plz: "88356"
  },
  {
    lati: 51.9033920621, longi: 9.38979606457,
    name: "Brevörde, Polle, Vahlbruch",
    plz: "37647"
  },
  {
    lati: 53.5518853503, longi: 9.91415027829,
    name: "Hamburg",
    plz: "22763"
  },
  {
    lati: 53.7257200336, longi: 10.2537241028,
    name: "Bargteheide, Delingsdorf u.a.",
    plz: "22941"
  },
  {
    lati: 53.0577407419, longi: 8.78827062072,
    name: "Bremen",
    plz: "28199"
  },
  {
    lati: 49.422092259, longi: 8.84771840818,
    name: "Neckarsteinach",
    plz: "69239"
  },
  {
    lati: 47.9351164266, longi: 7.85708093778,
    name: "Horben",
    plz: "79289"
  },
  {
    lati: 49.030763649, longi: 8.45239888999,
    name: "Karlsruhe",
    plz: "76139"
  },
  {
    lati: 51.1875320994, longi: 7.04138197187,
    name: "Solingen",
    plz: "42719"
  },
  {
    lati: 49.3219258725, longi: 7.21958102184,
    name: "Neunkirchen",
    plz: "66539"
  },
  {
    lati: 52.9317351025, longi: 7.66939818207,
    name: "Lorup, Rastdorf",
    plz: "26901"
  },
  {
    lati: 50.1106486182, longi: 8.31142540152,
    name: "Wiesbaden",
    plz: "65207"
  },
  {
    lati: 48.719021244, longi: 8.35612746611,
    name: "Weisenbach",
    plz: "76599"
  },
  {
    lati: 48.8096566016, longi: 9.10582985038,
    name: "Stuttgart",
    plz: "70499"
  },
  {
    lati: 50.0028090022, longi: 8.12451804847,
    name: "Heidesheim am Rhein",
    plz: "55262"
  },
  {
    lati: 50.6830550092, longi: 7.66456974502,
    name: "Altenkirchen (Westerwald)",
    plz: "57610"
  },
  {
    lati: 51.9939500143, longi: 7.78687963324,
    name: "Telgte",
    plz: "48291"
  },
  {
    lati: 50.8069581332, longi: 9.97004223343,
    name: "Unterbreizbach",
    plz: "36414"
  },
  {
    lati: 53.583613422, longi: 10.0057148063,
    name: "Hamburg",
    plz: "22301"
  },
  {
    lati: 52.379721742, longi: 9.76663492616,
    name: "Hannover",
    plz: "30175"
  },
  {
    lati: 49.2219869113, longi: 10.7338953082,
    name: "Wolframs-Eschenbach",
    plz: "91639"
  },
  {
    lati: 54.1220134125, longi: 10.3378803491,
    name: "Ascheberg",
    plz: "24326"
  },
  {
    lati: 48.3078566019, longi: 11.9962761501,
    name: "Bockhorn",
    plz: "85461"
  },
  {
    lati: 48.7814393362, longi: 12.3152915668,
    name: "Laberweinting",
    plz: "84082"
  },
  {
    lati: 50.9146236639, longi: 13.3340412413,
    name: "Freiberg",
    plz: "09599"
  },
  {
    lati: 52.6658741905, longi: 13.3618387314,
    name: "Mühlenbecker Land",
    plz: "16567"
  },
  {
    lati: 53.5440725874, longi: 11.3301399814,
    name: "Pampow",
    plz: "19075"
  },
  {
    lati: 51.2144308434, longi: 6.78398978191,
    name: "Düsseldorf",
    plz: "40215"
  },
  {
    lati: 50.053860492, longi: 7.46065353749,
    name: "Kastellaun",
    plz: "56288"
  },
  {
    lati: 52.0836833697, longi: 8.41764877938,
    name: "Werther (Westf.)",
    plz: "33824"
  },
  {
    lati: 53.4550873406, longi: 8.48387187303,
    name: "Nordenham",
    plz: "26954"
  },
  {
    lati: 51.1176931684, longi: 9.69365587509,
    name: "Spangenberg",
    plz: "34286"
  },
  {
    lati: 48.7869977957, longi: 9.94480459666,
    name: "Heubach",
    plz: "73540"
  },
  {
    lati: 48.5114890221, longi: 10.7077096467,
    name: "Laugna",
    plz: "86502"
  },
  {
    lati: 51.1436008012, longi: 10.4097179241,
    name: "Vogtei, Kammerforst u.a.",
    plz: "99986"
  },
  {
    lati: 52.4293043514, longi: 13.4905249539,
    name: "Berlin Rudow",
    plz: "12357"
  },
  {
    lati: 52.7573932782, longi: 13.7604853425,
    name: "Melchow, Chorin u.a.",
    plz: "16230"
  },
  {
    lati: 50.1798259271, longi: 7.0834658215,
    name: "Büchel",
    plz: "56823"
  },
  {
    lati: 51.0193408846, longi: 10.9972844286,
    name: "Erfurt",
    plz: "99091"
  },
  {
    lati: 48.8706708056, longi: 11.0909002692,
    name: "Dollnstein",
    plz: "91795"
  },
  {
    lati: 52.1575849153, longi: 11.6417843905,
    name: "Magdeburg",
    plz: "39124"
  },
  {
    lati: 49.4034998501, longi: 7.5600681847,
    name: "Landstuhl",
    plz: "66849"
  },
  {
    lati: 53.5854964198, longi: 7.75873506108,
    name: "Wittmund",
    plz: "26409"
  },
  {
    lati: 48.0699114782, longi: 7.83616429378,
    name: "Vörstetten",
    plz: "79279"
  },
  {
    lati: 52.21153981, longi: 8.33270720955,
    name: "Melle",
    plz: "49324"
  },
  {
    lati: 48.188363411, longi: 8.77425050371,
    name: "Weilen unter den Rinnen",
    plz: "72367"
  },
  {
    lati: 54.1103608821, longi: 10.0059441095,
    name: "Neumünster",
    plz: "24536"
  },
  {
    lati: 53.0732143203, longi: 10.589014971,
    name: "Bad Bevensen",
    plz: "29549"
  },
  {
    lati: 48.4538341641, longi: 10.6535035038,
    name: "Welden",
    plz: "86465"
  },
  {
    lati: 49.3499252709, longi: 10.6912621004,
    name: "Bruckberg",
    plz: "91590"
  },
  {
    lati: 48.3774599367, longi: 10.9024894927,
    name: "Augsburg",
    plz: "86153"
  },
  {
    lati: 49.4589702484, longi: 12.2852802729,
    name: "Guteneck",
    plz: "92543"
  },
  {
    lati: 48.4276481466, longi: 12.8892825258,
    name: "Postmünster",
    plz: "84389"
  },
  {
    lati: 51.1006340295, longi: 13.3071024326,
    name: "Nossen",
    plz: "01683"
  },
  {
    lati: 51.0421726226, longi: 13.9076698718,
    name: "Dresden",
    plz: "01328"
  },
  {
    lati: 49.4534851145, longi: 10.0685033878,
    name: "Creglingen",
    plz: "97993"
  },
  {
    lati: 48.6370441078, longi: 9.60690772505,
    name: "Bad Boll",
    plz: "73087"
  },
  {
    lati: 47.9003046965, longi: 12.0679684707,
    name: "Großkarolinenfeld",
    plz: "83109"
  },
  {
    lati: 50.0674418179, longi: 10.2422820223,
    name: "Schweinfurt",
    plz: "97422"
  },
  {
    lati: 49.9431746069, longi: 10.3440281024,
    name: "Sulzheim",
    plz: "97529"
  },
  {
    lati: 48.2318934615, longi: 11.2537491543,
    name: "Maisach",
    plz: "82216"
  },
  {
    lati: 49.3531749776, longi: 11.4429495526,
    name: "Berg b.Neumarkt i.d.OPf.",
    plz: "92348"
  },
  {
    lati: 51.3837389154, longi: 13.733519434,
    name: "Ortrand",
    plz: "01990"
  },
  {
    lati: 51.9743367497, longi: 7.44290968496,
    name: "Havixbeck",
    plz: "48329"
  },
  {
    lati: 52.6607895388, longi: 8.10019370907,
    name: "Dinklage",
    plz: "49413"
  },
  {
    lati: 48.2704293377, longi: 7.71723928153,
    name: "Rust",
    plz: "77977"
  },
  {
    lati: 50.3503447062, longi: 9.20592598011,
    name: "Kefenrod",
    plz: "63699"
  },
  {
    lati: 51.594942585, longi: 12.6417483439,
    name: "Bad Düben",
    plz: "04849"
  },
  {
    lati: 54.4485469477, longi: 12.9910707556,
    name: "Prohn",
    plz: "18445"
  },
  {
    lati: 50.4460219962, longi: 11.6236860557,
    name: "Lobenstein, Neundorf",
    plz: "07356"
  },
  {
    lati: 50.3125750107, longi: 7.56450275487,
    name: "Koblenz",
    plz: "56075"
  },
  {
    lati: 49.6694720403, longi: 7.576213479,
    name: "Grumbach",
    plz: "67745"
  },
  {
    lati: 49.4974513013, longi: 8.79016733653,
    name: "Heiligkreuzsteinach",
    plz: "69253"
  },
  {
    lati: 54.2650676911, longi: 9.59715549187,
    name: "Nübbel",
    plz: "24809"
  },
  {
    lati: 48.3940892242, longi: 10.9243065363,
    name: "Augsburg",
    plz: "86167"
  },
  {
    lati: 50.1334885996, longi: 6.23583996445,
    name: "Üttfeld",
    plz: "54619"
  },
  {
    lati: 50.9061723232, longi: 7.18382136453,
    name: "Rösrath",
    plz: "51503"
  },
  {
    lati: 48.032091331, longi: 8.04782173001,
    name: "Sankt Peter",
    plz: "79271"
  },
  {
    lati: 48.8491441152, longi: 9.1526344479,
    name: "Stuttgart",
    plz: "70439"
  },
  {
    lati: 50.5538318708, longi: 9.38004951148,
    name: "Herbstein",
    plz: "36358"
  },
  {
    lati: 52.6619553025, longi: 10.6941782301,
    name: "Wittingen",
    plz: "29379"
  },
  {
    lati: 52.2903540892, longi: 10.8961931602,
    name: "Königslutter am Elm",
    plz: "38154"
  },
  {
    lati: 50.9461843746, longi: 13.8414627243,
    name: "Heidenau",
    plz: "01809"
  },
  {
    lati: 47.7871129819, longi: 11.9995353376,
    name: "Bad Feilnbach",
    plz: "83075"
  },
  {
    lati: 49.1311293978, longi: 10.0553819973,
    name: "Crailsheim",
    plz: "74564"
  },
  {
    lati: 49.7086987939, longi: 10.1204816989,
    name: "Albertshofen",
    plz: "97320"
  },
  {
    lati: 53.2103287975, longi: 10.4759075638,
    name: "Wendisch Evern",
    plz: "21403"
  },
  {
    lati: 53.8011103522, longi: 10.5600694462,
    name: "Klein Wesenberg",
    plz: "23860"
  },
  {
    lati: 47.5840073023, longi: 12.8715390403,
    name: "Ramsau b. Berchtesgaden",
    plz: "83486"
  },
  {
    lati: 52.5019288194, longi: 13.2447383062,
    name: "Berlin Westend",
    plz: "14055"
  },
  {
    lati: 50.006205322, longi: 11.7030018383,
    name: "Goldkronach",
    plz: "95497"
  },
  {
    lati: 50.0310279942, longi: 10.7924483301,
    name: "Gerach",
    plz: "96161"
  },
  {
    lati: 49.7702990062, longi: 6.72446420445,
    name: "Mertesdorf",
    plz: "54318"
  },
  {
    lati: 53.6534399204, longi: 7.93285291459,
    name: "Wangerland",
    plz: "26434"
  },
  {
    lati: 50.3545121775, longi: 7.52658069181,
    name: "Koblenz",
    plz: "56072"
  },
  {
    lati: 51.5956408346, longi: 13.7246363094,
    name: "Finsterwalde",
    plz: "03238"
  },
  {
    lati: 48.3224307612, longi: 10.6399156705,
    name: "Ustersbach",
    plz: "86514"
  },
  {
    lati: 53.8417456418, longi: 9.95585129276,
    name: "Kaltenkirchen",
    plz: "24568"
  },
  {
    lati: 50.6960000713, longi: 10.2756145675,
    name: "Schwallungen",
    plz: "98590"
  },
  {
    lati: 49.9960606484, longi: 10.4561105699,
    name: "Wonfurt",
    plz: "97539"
  },
  {
    lati: 47.8175677293, longi: 11.7379064131,
    name: "Warngau",
    plz: "83627"
  },
  {
    lati: 48.1272381119, longi: 8.71255374523,
    name: "Frittlingen",
    plz: "78665"
  },
  {
    lati: 50.107363846, longi: 8.74631175848,
    name: "Offenbach am Main",
    plz: "63067"
  },
  {
    lati: 49.4976958568, longi: 8.07508773861,
    name: "Altleiningen",
    plz: "67317"
  },
  {
    lati: 51.7734344479, longi: 6.42041359593,
    name: "Rees",
    plz: "46459"
  },
  {
    lati: 52.5023392831, longi: 13.3194250957,
    name: "Berlin Charlottenburg",
    plz: "10623"
  },
  {
    lati: 53.9421190927, longi: 14.1419998471,
    name: "Ostseebad Heringsdorf",
    plz: "17424"
  },
  {
    lati: 50.9022011509, longi: 7.5355417128,
    name: "Nümbrecht",
    plz: "51588"
  },
  {
    lati: 49.4866916328, longi: 7.61867045358,
    name: "Weilerbach u.a.",
    plz: "67685"
  },
  {
    lati: 49.0282953062, longi: 11.1198810371,
    name: "Nennslingen",
    plz: "91790"
  },
  {
    lati: 48.7871911045, longi: 11.2246811441,
    name: "Egweil",
    plz: "85116"
  },
  {
    lati: 50.7404036835, longi: 8.09441221945,
    name: "Burbach",
    plz: "57299"
  },
  {
    lati: 52.2340513734, longi: 10.5682989635,
    name: "Braunschweig",
    plz: "38126"
  },
  {
    lati: 48.8489471183, longi: 9.95129359055,
    name: "Heuchlingen",
    plz: "73572"
  },
  {
    lati: 51.3455134151, longi: 10.0258421095,
    name: "Arenshausen, Uder u.a.",
    plz: "37318"
  },
  {
    lati: 48.3809733168, longi: 10.0671761958,
    name: "Neu-Ulm",
    plz: "89233"
  },
  {
    lati: 49.2223880001, longi: 6.96288759415,
    name: "Saarbrücken",
    plz: "66117"
  },
  {
    lati: 51.1132123811, longi: 6.14510276237,
    name: "Wassenberg",
    plz: "41849"
  },
  {
    lati: 49.3660120769, longi: 6.73228898116,
    name: "Dillingen/Saar",
    plz: "66763"
  },
  {
    lati: 49.9650477292, longi: 8.20768065185,
    name: "Mainz",
    plz: "55127"
  },
  {
    lati: 48.8392460923, longi: 8.2814708466,
    name: "Bischweier",
    plz: "76476"
  },
  {
    lati: 47.5844482484, longi: 8.41874365022,
    name: "Hohentengen am Hochrhein",
    plz: "79801"
  },
  {
    lati: 50.1308900038, longi: 8.53089739359,
    name: "Sulzbach (Taunus)",
    plz: "65843"
  },
  {
    lati: 52.5649754578, longi: 13.5694764502,
    name: "Berlin",
    plz: "12689"
  },
  {
    lati: 48.2423682381, longi: 12.7456717283,
    name: "Neuötting",
    plz: "84524"
  },
  {
    lati: 54.1792930895, longi: 13.3362318276,
    name: "Greifswald",
    plz: "17493"
  },
  {
    lati: 48.6900384647, longi: 13.4811608661,
    name: "Hutthurm",
    plz: "94116"
  },
  {
    lati: 51.0363873996, longi: 14.2231653563,
    name: "Neustadt i. Sa.",
    plz: "01844"
  },
  {
    lati: 48.0300864565, longi: 9.02139608574,
    name: "Leibertingen, Buchheim",
    plz: "88637"
  },
  {
    lati: 49.5045025592, longi: 6.42610240252,
    name: "Perl",
    plz: "66706"
  },
  {
    lati: 48.7613355187, longi: 9.6888318442,
    name: "Wäschenbeuren",
    plz: "73116"
  },
  {
    lati: 52.9935512473, longi: 10.3986708364,
    name: "Schwienau",
    plz: "29593"
  },
  {
    lati: 53.8778494858, longi: 10.523955086,
    name: "Zarpen",
    plz: "23619"
  },
  {
    lati: 48.9339461847, longi: 8.62769553694,
    name: "Kämpfelbach",
    plz: "75236"
  },
  {
    lati: 52.2770656185, longi: 8.05401615368,
    name: "Osnabrück",
    plz: "49074"
  },
  {
    lati: 53.442879415, longi: 12.2442446513,
    name: "Plau am See",
    plz: "19395"
  },
  {
    lati: 51.274170781, longi: 6.39798599159,
    name: "Viersen",
    plz: "41748"
  },
  {
    lati: 49.7380228783, longi: 9.1650142864,
    name: "Laudenbach",
    plz: "63925"
  },
  {
    lati: 48.9372362412, longi: 9.29614636065,
    name: "Erdmannhausen",
    plz: "71729"
  },
  {
    lati: 48.4179662656, longi: 9.99221638206,
    name: "Ulm",
    plz: "89075"
  },
  {
    lati: 50.815355034, longi: 12.8944086409,
    name: "Chemnitz",
    plz: "09119"
  },
  {
    lati: 52.4935618164, longi: 13.3528914689,
    name: "Berlin Schöneberg",
    plz: "10781"
  },
  {
    lati: 48.0015435359, longi: 7.96971386721,
    name: "Stegen",
    plz: "79252"
  },
  {
    lati: 49.1531051398, longi: 7.06955315701,
    name: "Kleinblittersdorf",
    plz: "66271"
  },
  {
    lati: 49.1518603742, longi: 7.66053751419,
    name: "Lemberg",
    plz: "66969"
  },
  {
    lati: 48.1682672133, longi: 11.8025794234,
    name: "Poing",
    plz: "85586"
  },
  {
    lati: 50.9405013115, longi: 6.94405100596,
    name: "Köln",
    plz: "50667"
  },
  {
    lati: 48.0867002711, longi: 8.92386461956,
    name: "Bärenthal",
    plz: "78580"
  },
  {
    lati: 51.3506264341, longi: 12.3601257983,
    name: "Leipzig",
    plz: "04105"
  },
  {
    lati: 49.9637552383, longi: 10.2138017343,
    name: "Röthlein",
    plz: "97520"
  },
  {
    lati: 48.1360856102, longi: 8.76292852765,
    name: "Gosheim",
    plz: "78559"
  },
  {
    lati: 47.8580582692, longi: 11.8149478083,
    name: "Weyarn",
    plz: "83629"
  },
  {
    lati: 48.9150343497, longi: 10.6313127485,
    name: "Munningen",
    plz: "86754"
  },
  {
    lati: 52.485081305, longi: 13.3132789974,
    name: "Berlin Wilmersdorf",
    plz: "10713"
  },
  {
    lati: 51.0361411946, longi: 13.7972845618,
    name: "Dresden",
    plz: "01277"
  },
  {
    lati: 53.641966175, longi: 9.54757800771,
    name: "Hetlingen",
    plz: "25491"
  },
  {
    lati: 52.4707335286, longi: 10.0175247703,
    name: "Burgdorf",
    plz: "31303"
  },
  {
    lati: 49.8871623222, longi: 10.1626232382,
    name: "Eisenheim",
    plz: "97247"
  },
  {
    lati: 51.6683967556, longi: 9.40726527073,
    name: "Lauenförde",
    plz: "37697"
  },
  {
    lati: 54.6313064953, longi: 9.18247420778,
    name: "Löwenstedt",
    plz: "25864"
  },
  {
    lati: 49.6006267031, longi: 7.83739205209,
    name: "Steinbach, Weitersweiler, Bennhausen, Mörsfeld, Würzweiler, Ruppertsecke",
    plz: "67808"
  },
  {
    lati: 48.9915854761, longi: 8.37789792864,
    name: "Karlsruhe",
    plz: "76135"
  },
  {
    lati: 49.3750287582, longi: 8.64633786806,
    name: "Heidelberg",
    plz: "69124"
  },
  {
    lati: 51.3854650075, longi: 7.08563093558,
    name: "Essen",
    plz: "45257"
  },
  {
    lati: 48.9995067141, longi: 10.3163214442,
    name: "Stödtlen",
    plz: "73495"
  },
  {
    lati: 48.8652569193, longi: 10.4349865047,
    name: "Riesbürg",
    plz: "73469"
  },
  {
    lati: 50.4714689923, longi: 12.776084483,
    name: "Breitenbrunn/Erzgeb.",
    plz: "08359"
  },
  {
    lati: 50.7645282872, longi: 9.67195702294,
    name: "Haunetal",
    plz: "36166"
  },
  {
    lati: 52.3598250213, longi: 9.6676309842,
    name: "Hannover",
    plz: "30455"
  },
  {
    lati: 54.4484228061, longi: 9.75888955355,
    name: "Osterby",
    plz: "24367"
  },
  {
    lati: 52.4441330244, longi: 13.3098112995,
    name: "Berlin Lichtenfelde",
    plz: "12203"
  },
  {
    lati: 52.5006432039, longi: 13.4647472134,
    name: "Berlin Friedrichshain",
    plz: "10245"
  },
  {
    lati: 52.8234790645, longi: 13.7924469295,
    name: "Eberswalde",
    plz: "16225"
  },
  {
    lati: 51.2306292542, longi: 7.62558864341,
    name: "Lüdenscheid",
    plz: "58507"
  },
  {
    lati: 48.1914018701, longi: 8.45069795778,
    name: "Eschbronn",
    plz: "78664"
  },
  {
    lati: 50.0493151835, longi: 8.51789468849,
    name: "Kelsterbach",
    plz: "65451"
  },
  {
    lati: 47.6252942033, longi: 10.0085469542,
    name: "Röthenbach (Allgäu)",
    plz: "88167"
  },
  {
    lati: 48.5207030368, longi: 10.2050832796,
    name: "Asselfingen",
    plz: "89176"
  },
  {
    lati: 51.5786968893, longi: 11.4896736867,
    name: "Klostermansfeld, Benndorf",
    plz: "06308"
  },
  {
    lati: 47.7714093167, longi: 11.6756463453,
    name: "Waakirchen",
    plz: "83666"
  },
  {
    lati: 50.6417362867, longi: 10.5999548458,
    name: "Benshausen",
    plz: "98554"
  },
  {
    lati: 52.4745155247, longi: 10.6761431431,
    name: "Osloß",
    plz: "38557"
  },
  {
    lati: 49.1964424283, longi: 12.7694991144,
    name: "Chamerau",
    plz: "93466"
  },
  {
    lati: 54.1526400913, longi: 9.31188446231,
    name: "Albersdorf",
    plz: "25767"
  },
  {
    lati: 53.8172593584, longi: 9.6055182986,
    name: "Horst",
    plz: "25358"
  },
  {
    lati: 47.9300037234, longi: 8.40052706845,
    name: "Bräunlingen",
    plz: "78199"
  },
  {
    lati: 47.5962315411, longi: 11.071178331,
    name: "Oberammergau",
    plz: "82487"
  },
  {
    lati: 48.1600739668, longi: 11.1731152002,
    name: "Landsberied",
    plz: "82290"
  },
  {
    lati: 47.7192383056, longi: 11.432702943,
    name: "Bichl",
    plz: "83673"
  },
  {
    lati: 51.1139648066, longi: 6.39856785744,
    name: "Mönchengladbach",
    plz: "41189"
  },
  {
    lati: 50.3467791792, longi: 11.8340303033,
    name: "Köditz",
    plz: "95189"
  },
  {
    lati: 53.2154665, longi: 13.325536839,
    name: "Lychen",
    plz: "17279"
  },
  {
    lati: 51.4098152545, longi: 14.8023301118,
    name: "Rietschen",
    plz: "02956"
  },
  {
    lati: 51.4324561361, longi: 8.00866518253,
    name: "Arnsberg",
    plz: "59759"
  },
  {
    lati: 52.4327687609, longi: 13.4555050429,
    name: "Berlin Buckow",
    plz: "12351"
  },
  {
    lati: 50.1132400298, longi: 6.48318493713,
    name: "Lasel",
    plz: "54612"
  },
  {
    lati: 52.859676791, longi: 7.22967592768,
    name: "Oberlangen, Niederlangen",
    plz: "49779"
  },
  {
    lati: 50.6223771027, longi: 9.45533210972,
    name: "Wartenberg",
    plz: "36367"
  },
  {
    lati: 47.8437039578, longi: 7.88915099305,
    name: "Wieden",
    plz: "79695"
  },
  {
    lati: 50.2129551936, longi: 8.40264719119,
    name: "Glashütten",
    plz: "61479"
  },
  {
    lati: 54.5347015828, longi: 9.78983411977,
    name: "Kosel, Rieseby u.a.",
    plz: "24354"
  },
  {
    lati: 53.6407682393, longi: 9.9513875233,
    name: "Hamburg",
    plz: "22455"
  },
  {
    lati: 48.192528717, longi: 11.1045628972,
    name: "Adelshofen",
    plz: "82276"
  },
  {
    lati: 52.3110621802, longi: 8.02050676919,
    name: "Osnabrück",
    plz: "49090"
  },
  {
    lati: 50.549901164, longi: 12.6434590076,
    name: "Zschorlau",
    plz: "08321"
  },
  {
    lati: 53.6170118652, longi: 10.2111297618,
    name: "Hamburg",
    plz: "22145"
  },
  {
    lati: 47.5798145744, longi: 13.0020670008,
    name: "Berchtesgaden &amp; Schönau",
    plz: "83471"
  },
  {
    lati: 48.2929164032, longi: 10.3103895288,
    name: "Wiesenbach",
    plz: "86519"
  },
  {
    lati: 48.91856107, longi: 11.8650607515,
    name: "Kelheim",
    plz: "93309"
  },
  {
    lati: 52.9392925129, longi: 10.7765862568,
    name: "Suhlendorf",
    plz: "29562"
  },
  {
    lati: 52.5589009017, longi: 13.2978691246,
    name: "Berlin Wedding",
    plz: "13405"
  },
  {
    lati: 48.1411828762, longi: 12.44573724,
    name: "Taufkirchen",
    plz: "84574"
  },
  {
    lati: 48.3896644632, longi: 12.6741110038,
    name: "Unterdietfurt",
    plz: "84339"
  },
  {
    lati: 52.69576557, longi: 12.8274732366,
    name: "Fehrbellin, Temnitzquell, Märkisch Linden u.a.",
    plz: "16818"
  },
  {
    lati: 51.4480378398, longi: 6.6233003893,
    name: "Moers",
    plz: "47441"
  },
  {
    lati: 54.0965900104, longi: 9.05490363892,
    name: "Meldorf",
    plz: "25704"
  },
  {
    lati: 54.3605608829, longi: 9.25715376154,
    name: "Norderstapel",
    plz: "25868"
  },
  {
    lati: 50.2831158995, longi: 8.75814559312,
    name: "Wöllstadt",
    plz: "61206"
  },
  {
    lati: 50.3654431343, longi: 8.09430544658,
    name: "Limburg",
    plz: "65550"
  },
  {
    lati: 49.5368978296, longi: 7.24002490718,
    name: "Freisen",
    plz: "66629"
  },
  {
    lati: 49.3617300702, longi: 7.26533584032,
    name: "Bexbach",
    plz: "66450"
  },
  {
    lati: 48.0079482167, longi: 11.1660822005,
    name: "Herrsching a. Ammersee",
    plz: "82211"
  },
  {
    lati: 49.6617337868, longi: 11.4662773169,
    name: "Plech",
    plz: "91287"
  },
  {
    lati: 50.9635827114, longi: 11.883144564,
    name: "Eisenberg, Gösen, Hainspitz",
    plz: "07607"
  },
  {
    lati: 48.9216519622, longi: 9.49612303821,
    name: "Weissach im Tal",
    plz: "71554"
  },
  {
    lati: 50.4780692372, longi: 12.1087982638,
    name: "Plauen, Rößnitz",
    plz: "08527"
  },
  {
    lati: 52.2087235586, longi: 14.0175588696,
    name: "Wendisch Rietz",
    plz: "15864"
  },
  {
    lati: 53.6426961806, longi: 10.0408830493,
    name: "Hamburg",
    plz: "22339"
  },
  {
    lati: 50.3377898614, longi: 7.76117997381,
    name: "Dausenau, Nievern",
    plz: "56132"
  },
  {
    lati: 48.7729251552, longi: 8.73448401917,
    name: "Bad Liebenzell",
    plz: "75378"
  },
  {
    lati: 51.6354960454, longi: 10.5685849218,
    name: "Wieda",
    plz: "37447"
  },
  {
    lati: 48.657862978, longi: 9.06674978184,
    name: "Schönaich",
    plz: "71101"
  },
  {
    lati: 53.5255602955, longi: 9.41852730729,
    name: "Fredenbeck",
    plz: "21717"
  },
  {
    lati: 49.0020308914, longi: 11.3824323044,
    name: "Kinding",
    plz: "85125"
  },
  {
    lati: 50.2458034287, longi: 9.71579504587,
    name: "Zeitlofs",
    plz: "97799"
  },
  {
    lati: 48.8838749801, longi: 10.6216336144,
    name: "Wechingen",
    plz: "86759"
  },
  {
    lati: 50.8284590219, longi: 6.26987875086,
    name: "Eschweiler",
    plz: "52249"
  },
  {
    lati: 50.5650464244, longi: 7.94259926382,
    name: "Bellingen, Kölbingen, Gemünden etc",
    plz: "56459"
  },
  {
    lati: 47.7196301875, longi: 8.17570059016,
    name: "Höchenschwand",
    plz: "79862"
  },
  {
    lati: 53.6389177035, longi: 7.63217099018,
    name: "Esens, Neuharlingersiel u.a.",
    plz: "26427"
  },
  {
    lati: 50.2364399701, longi: 9.15418061186,
    name: "Gründau",
    plz: "63584"
  },
  {
    lati: 48.5222709334, longi: 8.82593724305,
    name: "Bondorf",
    plz: "71149"
  },
  {
    lati: 52.5506376257, longi: 13.3328445489,
    name: "Berlin Wedding",
    plz: "13351"
  },
  {
    lati: 52.4263706063, longi: 13.35168466,
    name: "Berlin Lankwitz",
    plz: "12249"
  },
  {
    lati: 53.6137326017, longi: 10.11878085,
    name: "Hamburg",
    plz: "22159"
  },
  {
    lati: 51.7494977802, longi: 6.61742822619,
    name: "Hamminkeln",
    plz: "46499"
  },
  {
    lati: 49.1822250071, longi: 6.82232924502,
    name: "Großrosseln",
    plz: "66352"
  },
  {
    lati: 51.3777452892, longi: 7.35368266486,
    name: "Wetter (Ruhr)",
    plz: "58300"
  },
  {
    lati: 49.1015175597, longi: 12.4845506268,
    name: "Falkenstein",
    plz: "93167"
  },
  {
    lati: 50.9474579512, longi: 12.576224509,
    name: "Langenleuba-Niederhain",
    plz: "04618"
  },
  {
    lati: 50.8698320461, longi: 12.7210955005,
    name: "Limbach-Oberfrohna",
    plz: "09212"
  },
  {
    lati: 50.7471725724, longi: 13.0571963163,
    name: "Zschopau, Gornau",
    plz: "09405"
  },
  {
    lati: 54.1021162768, longi: 12.0452954046,
    name: "Rostock, Lambrechtshagen",
    plz: "18069"
  },
  {
    lati: 52.2876518424, longi: 13.6212301006,
    name: "Königs Wusterhausen",
    plz: "15711"
  },
  {
    lati: 51.8022265436, longi: 11.1670075893,
    name: "Quedlinburg",
    plz: "06484"
  },
  {
    lati: 50.1433152119, longi: 11.5871244447,
    name: "Kupferberg",
    plz: "95362"
  },
  {
    lati: 52.1326311687, longi: 11.6136782983,
    name: "Magdeburg",
    plz: "39108"
  },
  {
    lati: 47.8215123281, longi: 10.828052258,
    name: "Schwabbruck",
    plz: "86986"
  },
  {
    lati: 51.4867098458, longi: 7.52841076111,
    name: "Dortmund",
    plz: "44269"
  },
  {
    lati: 49.2892961385, longi: 11.2707521432,
    name: "Pyrbaum",
    plz: "90602"
  },
  {
    lati: 47.9948130009, longi: 11.8024892538,
    name: "Egmating",
    plz: "85658"
  },
  {
    lati: 51.3380041933, longi: 7.25411900389,
    name: "Sprockhövel",
    plz: "45549"
  },
  {
    lati: 49.5548288514, longi: 6.53831151064,
    name: "Ayl, Trassem u.a.",
    plz: "54441"
  },
  {
    lati: 51.4811320074, longi: 6.87979444492,
    name: "Oberhausen",
    plz: "46047"
  },
  {
    lati: 49.8573433995, longi: 12.1419128706,
    name: "Reuth b. Erbendorf",
    plz: "92717"
  },
  {
    lati: 48.1578604649, longi: 9.61079026285,
    name: "Uttenweiler",
    plz: "88524"
  },
  {
    lati: 50.940885565, longi: 10.392570314,
    name: "Wutha-Farnroda",
    plz: "99848"
  },
  {
    lati: 48.5627021351, longi: 10.427702035,
    name: "Lauingen (Donau)",
    plz: "89415"
  },
  {
    lati: 49.1898413202, longi: 8.75224429658,
    name: "Östringen",
    plz: "76684"
  },
  {
    lati: 49.0605467763, longi: 9.70733531153,
    name: "Rosengarten",
    plz: "74538"
  },
  {
    lati: 49.8362905832, longi: 9.85097209496,
    name: "Margetshöchheim",
    plz: "97276"
  },
  {
    lati: 53.7478341489, longi: 9.48908377989,
    name: "Kollmar, Pagensand",
    plz: "25377"
  },
  {
    lati: 52.4602546078, longi: 9.56147806087,
    name: "Garbsen",
    plz: "30826"
  },
  {
    lati: 52.3960118441, longi: 11.194674327,
    name: "Rätzlingen, Wegenstedt, Calvörde, Böddensell etc",
    plz: "39359"
  },
  {
    lati: 51.3382212388, longi: 7.42153178387,
    name: "Hagen",
    plz: "58135"
  },
  {
    lati: 49.6279721588, longi: 7.80890557292,
    name: "Rockenhausen, Bisterschied u.a.",
    plz: "67806"
  },
  {
    lati: 49.4609091788, longi: 8.06512764825,
    name: "Weisenheim am Sand",
    plz: "67256"
  },
  {
    lati: 54.5416661698, longi: 9.00482611955,
    name: "Hattstedt u.a.",
    plz: "25856"
  },
  {
    lati: 48.3056869925, longi: 9.26341984506,
    name: "Trochtelfingen",
    plz: "72818"
  },
  {
    lati: 50.0680439806, longi: 10.0241704056,
    name: "Wasserlosen",
    plz: "97535"
  },
  {
    lati: 47.8688939951, longi: 12.108034065,
    name: "Rosenheim",
    plz: "83024"
  },
  {
    lati: 49.4448749966, longi: 12.360863621,
    name: "Niedermurach",
    plz: "92545"
  },
  {
    lati: 49.8578204634, longi: 10.8428669314,
    name: "Stegaurach",
    plz: "96135"
  },
  {
    lati: 48.9153691853, longi: 13.0742543957,
    name: "Bischofsmais",
    plz: "94253"
  },
  {
    lati: 51.458300992, longi: 13.1198150864,
    name: "Belgern-Schildau",
    plz: "04874"
  },
  {
    lati: 54.5534343422, longi: 13.5436076567,
    name: "Sagard",
    plz: "18551"
  },
  {
    lati: 47.9088560502, longi: 7.76375645884,
    name: "Ehrenkirchen",
    plz: "79238"
  },
  {
    lati: 47.5855941412, longi: 8.06735825587,
    name: "Laufenburg (Baden)",
    plz: "79725"
  },
  {
    lati: 47.7126914606, longi: 9.38661085817,
    name: "Markdorf",
    plz: "88677"
  },
  {
    lati: 49.4793721371, longi: 8.42924523366,
    name: "Ludwigshafen am Rhein",
    plz: "67059"
  },
  {
    lati: 52.1217688476, longi: 8.95906878344,
    name: "Kalletal",
    plz: "32689"
  },
  {
    lati: 49.7595777964, longi: 10.0331107686,
    name: "Theilheim",
    plz: "97288"
  },
  {
    lati: 53.6984082871, longi: 10.0420955194,
    name: "Norderstedt",
    plz: "22851"
  },
  {
    lati: 53.5591179753, longi: 10.0501385916,
    name: "Hamburg",
    plz: "20535"
  },
  {
    lati: 49.2953561154, longi: 10.2427247248,
    name: "Schillingsfürst",
    plz: "91583"
  },
  {
    lati: 52.2589166772, longi: 8.03279677038,
    name: "Osnabrück",
    plz: "49080"
  },
  {
    lati: 47.666030349, longi: 9.46020966641,
    name: "Friedrichshafen",
    plz: "88045"
  },
  {
    lati: 53.454337415, longi: 13.7869787689,
    name: "Uckerland, Groß Luckow, Schönhausen",
    plz: "17337"
  },
  {
    lati: 53.2490764315, longi: 14.3576921694,
    name: "Gartz (Oder)",
    plz: "16307"
  },
  {
    lati: 49.8032619056, longi: 10.2337714644,
    name: "Schwarzach a. Main",
    plz: "97359"
  },
  {
    lati: 49.2550417168, longi: 10.297703556,
    name: "Dombühl",
    plz: "91601"
  },
  {
    lati: 49.709493819, longi: 10.8997377713,
    name: "Adelsdorf",
    plz: "91325"
  },
  {
    lati: 50.740772466, longi: 12.5780800828,
    name: "Mülsen",
    plz: "08132"
  },
  {
    lati: 50.8641125221, longi: 12.9659298521,
    name: "Chemnitz",
    plz: "09131"
  },
  {
    lati: 50.0606260899, longi: 8.16932545309,
    name: "Wiesbaden",
    plz: "65201"
  },
  {
    lati: 49.4424532181, longi: 7.32023488282,
    name: "Altenkirchen",
    plz: "66903"
  },
  {
    lati: 50.7219651412, longi: 11.7339679743,
    name: "Neustadt/ Orla",
    plz: "07806"
  },
  {
    lati: 49.115189889, longi: 10.9852445345,
    name: "Pleinfeld",
    plz: "91785"
  },
  {
    lati: 49.6720351147, longi: 10.4750631638,
    name: "Scheinfeld",
    plz: "91443"
  },
  {
    lati: 51.9322707111, longi: 9.77746569391,
    name: "Grünenplan, Delligsen",
    plz: "31073"
  },
  {
    lati: 48.8333183699, longi: 9.78696003903,
    name: "Mutlangen",
    plz: "73557"
  },
  {
    lati: 47.6740479407, longi: 9.36408980243,
    name: "Immenstaad am Bodensee",
    plz: "88090"
  },
  {
    lati: 51.8659791807, longi: 9.63187520935,
    name: "Stadtoldendorf u.a.",
    plz: "37627"
  },
  {
    lati: 49.0711898612, longi: 12.3030659353,
    name: "Altenthann",
    plz: "93177"
  },
  {
    lati: 52.3769594868, longi: 10.570293292,
    name: "Meine",
    plz: "38527"
  },
  {
    lati: 49.8816441376, longi: 9.5173477935,
    name: "Esselbach",
    plz: "97839"
  },
  {
    lati: 52.2237182187, longi: 8.95203177401,
    name: "Porta Westfalica",
    plz: "32457"
  },
  {
    lati: 52.2067584782, longi: 8.56532821635,
    name: "Bünde",
    plz: "32257"
  },
  {
    lati: 54.7605596872, longi: 9.31741492857,
    name: "Handewitt",
    plz: "24983"
  },
  {
    lati: 49.3585033532, longi: 10.4988033146,
    name: "Lehrberg",
    plz: "91611"
  },
  {
    lati: 52.0460903551, longi: 7.10067608209,
    name: "Legden",
    plz: "48739"
  },
  {
    lati: 51.2621640325, longi: 7.1389933171,
    name: "Wuppertal",
    plz: "42105"
  },
  {
    lati: 51.3745996399, longi: 6.50824562501,
    name: "Krefeld",
    plz: "47839"
  },
  {
    lati: 52.05874013, longi: 7.36505040479,
    name: "Laer",
    plz: "48366"
  },
  {
    lati: 51.4972208984, longi: 7.4625420251,
    name: "Dortmund",
    plz: "44139"
  },
  {
    lati: 51.7056908382, longi: 7.7477086262,
    name: "Hamm",
    plz: "59075"
  },
  {
    lati: 49.7199812926, longi: 7.75827528997,
    name: "Obermoschel, Schiersfeld",
    plz: "67823"
  },
  {
    lati: 50.6860394599, longi: 8.38940553503,
    name: "Mittenaar",
    plz: "35756"
  },
  {
    lati: 48.6083533832, longi: 9.00256883463,
    name: "Altdorf",
    plz: "71155"
  },
  {
    lati: 53.9007255745, longi: 9.53626513963,
    name: "Münsterdorf",
    plz: "25587"
  },
  {
    lati: 50.9672227349, longi: 11.3481996092,
    name: "Weimar",
    plz: "99425"
  },
  {
    lati: 53.5513869401, longi: 10.0003450649,
    name: "Hamburg",
    plz: "20095"
  },
  {
    lati: 50.1291825323, longi: 10.0778311764,
    name: "Ramsthal",
    plz: "97729"
  },
  {
    lati: 49.7009150965, longi: 10.6543666881,
    name: "Vestenbergsgreuth",
    plz: "91487"
  },
  {
    lati: 52.8765832339, longi: 10.424990621,
    name: "Suderburg",
    plz: "29556"
  },
  {
    lati: 52.216727316, longi: 13.2164186701,
    name: "Trebbin",
    plz: "14959"
  },
  {
    lati: 51.1204653473, longi: 13.7552014876,
    name: "Dresden",
    plz: "01109"
  },
  {
    lati: 50.2724050335, longi: 7.59159347466,
    name: "Rhens",
    plz: "56321"
  },
  {
    lati: 50.8345587274, longi: 8.45265747238,
    name: "Steffenberg",
    plz: "35239"
  },
  {
    lati: 50.6692442358, longi: 8.49203593305,
    name: "Hohenahr",
    plz: "35644"
  },
  {
    lati: 53.7603041568, longi: 9.9258881923,
    name: "Ellerau",
    plz: "25479"
  },
  {
    lati: 49.1799960174, longi: 9.14269153314,
    name: "Heilbronn",
    plz: "74078"
  },
  {
    lati: 48.3918814195, longi: 11.9210772726,
    name: "Berglern",
    plz: "85459"
  },
  {
    lati: 54.2429421558, longi: 12.6185839929,
    name: "Ahrenshagen-Daskow, Trinwillershagen u.a.",
    plz: "18320"
  },
  {
    lati: 54.4523385875, longi: 10.1122793941,
    name: "Dänischenhagen",
    plz: "24229"
  },
  {
    lati: 48.0528929362, longi: 11.0864479101,
    name: "Schondorf a. Ammersee",
    plz: "86938"
  },
  {
    lati: 48.7031611292, longi: 10.4019328851,
    name: "Dischingen",
    plz: "89561"
  },
  {
    lati: 52.4206897161, longi: 10.6382719994,
    name: "Calberlah",
    plz: "38547"
  },
  {
    lati: 47.8078734884, longi: 10.7846948827,
    name: "Ingenried",
    plz: "86980"
  },
  {
    lati: 53.6405628669, longi: 13.473527982,
    name: "Friedland, Galenbeck, Datzetal",
    plz: "17099"
  },
  {
    lati: 49.1507404779, longi: 7.23245494291,
    name: "Gersheim",
    plz: "66453"
  },
  {
    lati: 51.1100380187, longi: 7.718383512,
    name: "Meinerzhagen",
    plz: "58540"
  },
  {
    lati: 50.1037653655, longi: 8.57627945612,
    name: "Frankfurt am Main",
    plz: "65934"
  },
  {
    lati: 52.3767717424, longi: 9.37623334439,
    name: "Haste, Hohnhorst",
    plz: "31559"
  },
  {
    lati: 49.0041747458, longi: 12.8207726523,
    name: "Sankt Englmar",
    plz: "94379"
  },
  {
    lati: 49.4638764189, longi: 11.1487591712,
    name: "Nürnberg",
    plz: "90482"
  },
  {
    lati: 53.8263325695, longi: 11.3287068898,
    name: "Wismar, Groß Krankow u.a.",
    plz: "23966"
  },
  {
    lati: 49.6300579348, longi: 8.10644573315,
    name: "Albisheim (Pfrimm)",
    plz: "67308"
  },
  {
    lati: 49.7769945901, longi: 8.37970986311,
    name: "Gimbsheim",
    plz: "67578"
  },
  {
    lati: 49.266908561, longi: 8.49356644915,
    name: "Oberhausen-Rheinhausen",
    plz: "68794"
  },
  {
    lati: 48.7203104494, longi: 9.70260362938,
    name: "Göppingen",
    plz: "73037"
  },
  {
    lati: 52.3761572775, longi: 8.20364826891,
    name: "Ostercappeln",
    plz: "49179"
  },
  {
    lati: 47.7696760711, longi: 8.42931600198,
    name: "Stühlingen",
    plz: "79780"
  },
  {
    lati: 48.8526876466, longi: 8.44840933826,
    name: "Marxzell",
    plz: "76359"
  },
  {
    lati: 48.7363818394, longi: 11.1949237979,
    name: "Neuburg an der Donau",
    plz: "86633"
  },
  {
    lati: 49.0774561772, longi: 11.976906764,
    name: "Pielenhofen",
    plz: "93188"
  },
  {
    lati: 50.6366452868, longi: 10.0097310031,
    name: "Tann",
    plz: "36142"
  },
  {
    lati: 53.3644456298, longi: 10.4864889517,
    name: "Artlenburg",
    plz: "21380"
  },
  {
    lati: 49.2615642818, longi: 11.6785329646,
    name: "Velburg",
    plz: "92355"
  },
  {
    lati: 48.9854239499, longi: 9.71200503456,
    name: "Fichtenberg",
    plz: "74427"
  },
  {
    lati: 52.5906845785, longi: 13.4595634987,
    name: "Berlin Blankenburg",
    plz: "13129"
  },
  {
    lati: 49.93969811, longi: 11.161103538,
    name: "Königsfeld",
    plz: "96167"
  },
  {
    lati: 50.3981489114, longi: 11.4611981113,
    name: "Tschirn",
    plz: "96367"
  },
  {
    lati: 49.574424901, longi: 11.5857631761,
    name: "Hirschbach",
    plz: "92275"
  },
  {
    lati: 48.4343927246, longi: 11.854772841,
    name: "Langenbach",
    plz: "85416"
  },
  {
    lati: 49.0023898561, longi: 10.6783052709,
    name: "Westheim",
    plz: "91747"
  },
  {
    lati: 49.9955195256, longi: 10.8210389675,
    name: "Baunach",
    plz: "96148"
  },
  {
    lati: 49.5706232199, longi: 10.8787208544,
    name: "Herzogenaurach",
    plz: "91074"
  },
  {
    lati: 54.2751044894, longi: 10.251542708,
    name: "Schwentinetal",
    plz: "24223"
  },
  {
    lati: 50.5212334535, longi: 9.75464433898,
    name: "Künzell",
    plz: "36093"
  },
  {
    lati: 53.483308074, longi: 9.85383466258,
    name: "Hamburg",
    plz: "21147"
  },
  {
    lati: 50.2629307456, longi: 6.59782999486,
    name: "Pelm, Neroth u.a.",
    plz: "54570"
  },
  {
    lati: 48.0931436248, longi: 8.54985547091,
    name: "Dauchingen",
    plz: "78083"
  },
  {
    lati: 50.127217658, longi: 8.92461847516,
    name: "Hanau",
    plz: "63450"
  },
  {
    lati: 54.8243820111, longi: 9.17064757775,
    name: "Medelby",
    plz: "24994"
  },
  {
    lati: 52.3595290747, longi: 9.17829416908,
    name: "Nordsehl",
    plz: "31717"
  },
  {
    lati: 54.4631980398, longi: 9.25190544936,
    name: "Ostenfeld",
    plz: "25872"
  },
  {
    lati: 48.7249562904, longi: 12.4294037589,
    name: "Mengkofen",
    plz: "84152"
  },
  {
    lati: 49.812223936, longi: 9.69305505451,
    name: "Remlingen",
    plz: "97280"
  },
  {
    lati: 52.224233083, longi: 10.4781435345,
    name: "Braunschweig",
    plz: "38122"
  },
  {
    lati: 51.6368368577, longi: 12.1230790711,
    name: "Zörbig",
    plz: "06780"
  },
  {
    lati: 50.9500676102, longi: 11.0985823673,
    name: "Erfurt",
    plz: "99099"
  },
  {
    lati: 54.7547801182, longi: 9.1716074054,
    name: "Schafflund, Meyn u.a",
    plz: "24980"
  },
  {
    lati: 50.6618894624, longi: 7.18301256791,
    name: "Bonn",
    plz: "53179"
  },
  {
    lati: 51.2634326781, longi: 14.5342079547,
    name: "Großdubrau, Malschwitz",
    plz: "02694"
  },
  {
    lati: 47.8116270642, longi: 11.1305330829,
    name: "Polling",
    plz: "82398"
  },
  {
    lati: 47.7210587961, longi: 9.10968478247,
    name: "Reichenau",
    plz: "78479"
  },
  {
    lati: 51.2617157946, longi: 10.9200495636,
    name: "Greußen Clingen Großenehrich",
    plz: "99718"
  },
  {
    lati: 52.1543282349, longi: 10.5690377701,
    name: "Wolfenbüttel",
    plz: "38300"
  },
  {
    lati: 49.9243187738, longi: 7.05768661055,
    name: "Bernkastel-Kues u.a.",
    plz: "54470"
  },
  {
    lati: 49.8707222359, longi: 7.37737467827,
    name: "Bergen",
    plz: "55608"
  },
  {
    lati: 49.2765011191, longi: 8.3354983608,
    name: "Schwegenheim",
    plz: "67365"
  },
  {
    lati: 49.9113024112, longi: 8.534505963,
    name: "Büttelborn",
    plz: "64572"
  },
  {
    lati: 50.6695310814, longi: 12.4707545995,
    name: "Zwickau",
    plz: "08064"
  },
  {
    lati: 53.7844119855, longi: 9.07773894873,
    name: "Cadenberge",
    plz: "21781"
  },
  {
    lati: 49.7096876302, longi: 7.45857934534,
    name: "Niederwörresbach",
    plz: "55758"
  },
  {
    lati: 51.8203799556, longi: 6.57973229115,
    name: "Bocholt",
    plz: "46395"
  },
  {
    lati: 49.722413784, longi: 9.29890908079,
    name: "Bürgstadt",
    plz: "63927"
  },
  {
    lati: 53.8784745924, longi: 9.45642218308,
    name: "Kremperheide",
    plz: "25569"
  },
  {
    lati: 52.9278974046, longi: 9.98532401765,
    name: "Wietzendorf",
    plz: "29649"
  },
  {
    lati: 50.6632167931, longi: 10.6784435518,
    name: "Zella-Mehlis",
    plz: "98544"
  },
  {
    lati: 50.2870085789, longi: 10.9991551905,
    name: "Dörfles-Esbach",
    plz: "96487"
  },
  {
    lati: 47.7994274346, longi: 11.0677413187,
    name: "Peißenberg",
    plz: "82380"
  },
  {
    lati: 48.1821447954, longi: 11.7510068213,
    name: "Kirchheim b. München",
    plz: "85551"
  },
  {
    lati: 50.4731115908, longi: 7.83633348388,
    name: "Mogendorf, Ebernhahn, Staudt u.a.",
    plz: "56424"
  },
  {
    lati: 49.4685187732, longi: 11.9690604032,
    name: "Freudenberg",
    plz: "92272"
  },
  {
    lati: 48.5554748633, longi: 11.9367542053,
    name: "Gammelsdorf",
    plz: "85408"
  },
  {
    lati: 49.4248552588, longi: 6.91662120451,
    name: "Lebach",
    plz: "66822"
  },
  {
    lati: 48.5212466636, longi: 12.5274288899,
    name: "Frontenhausen",
    plz: "84160"
  },
  {
    lati: 49.4535999809, longi: 11.4107251221,
    name: "Engelthal/Offenhausen",
    plz: "91238"
  },
  {
    lati: 50.6231380273, longi: 7.92965641748,
    name: "Nisterau u.a.",
    plz: "56472"
  },
  {
    lati: 48.5203080326, longi: 8.13096835764,
    name: "Lautenbach",
    plz: "77794"
  },
  {
    lati: 51.2197374319, longi: 7.60532333886,
    name: "Lüdenscheid",
    plz: "58509"
  },
  {
    lati: 51.9515193351, longi: 7.67098422476,
    name: "Münster",
    plz: "48155"
  },
  {
    lati: 47.9116058108, longi: 11.8631682749,
    name: "Feldkirchen-Westerham",
    plz: "83620"
  },
  {
    lati: 51.2030687542, longi: 6.34521164602,
    name: "Mönchengladbach",
    plz: "41169"
  },
  {
    lati: 52.280520585, longi: 7.70871325666,
    name: "Ibbenbüren",
    plz: "49477"
  },
  {
    lati: 48.9107028079, longi: 9.17214299546,
    name: "Ludwigsburg",
    plz: "71634"
  },
  {
    lati: 52.3310426394, longi: 10.3723950255,
    name: "Wendeburg",
    plz: "38176"
  },
  {
    lati: 50.3618378164, longi: 8.66831461011,
    name: "Ober-Mörlen",
    plz: "61239"
  },
  {
    lati: 48.880819112, longi: 8.67838332769,
    name: "Pforzheim",
    plz: "75173"
  },
  {
    lati: 48.1918788557, longi: 11.48250248,
    name: "München",
    plz: "80997"
  },
  {
    lati: 48.1200752798, longi: 11.6011459158,
    name: "München",
    plz: "81669"
  },
  {
    lati: 48.1130230638, longi: 11.8052982428,
    name: "Baldham",
    plz: "85598"
  },
  {
    lati: 50.9955840856, longi: 11.056589538,
    name: "Erfurt",
    plz: "99085"
  },
  {
    lati: 49.4551582413, longi: 11.0793222195,
    name: "Nürnberg",
    plz: "90403"
  },
  {
    lati: 53.647686576, longi: 13.6742712379,
    name: "Friedland, Galenbeck, Datzetal",
    plz: "17099"
  },
  {
    lati: 53.5686596596, longi: 9.87982241884,
    name: "Hamburg",
    plz: "22607"
  },
  {
    lati: 48.2654649351, longi: 9.99352706583,
    name: "Schnürpflingen",
    plz: "89194"
  },
  {
    lati: 48.4601783398, longi: 9.15521299066,
    name: "Reutlingen",
    plz: "72770"
  },
  {
    lati: 47.9775345158, longi: 8.8092512264,
    name: "Tuttlingen",
    plz: "78532"
  },
  {
    lati: 53.6398045404, longi: 6.88506831353,
    name: "Juist, Memmert",
    plz: "26571"
  },
  {
    lati: 50.3800205732, longi: 6.94413537876,
    name: "Adenau, Kottenborn u.a.",
    plz: "53518"
  },
  {
    lati: 49.2345382472, longi: 12.0993587714,
    name: "Teublitz",
    plz: "93158"
  },
  {
    lati: 51.1502517984, longi: 12.4169229997,
    name: "Neukieritzsch, Deutzen",
    plz: "04575"
  },
  {
    lati: 47.9037880912, longi: 12.8221012785,
    name: "Petting",
    plz: "83367"
  },
  {
    lati: 52.5942949828, longi: 10.1120733397,
    name: "Celle",
    plz: "29227"
  },
  {
    lati: 48.9096211325, longi: 13.4766160867,
    name: "Neuschönau",
    plz: "94556"
  },
  {
    lati: 48.6954618836, longi: 13.7774994087,
    name: "Breitenberg",
    plz: "94139"
  },
  {
    lati: 50.5117569123, longi: 8.02253663551,
    name: "Dornburg",
    plz: "65599"
  },
  {
    lati: 50.7685750154, longi: 7.69785931109,
    name: "Fürthen",
    plz: "57539"
  },
  {
    lati: 49.6731079857, longi: 7.73669829658,
    name: "Winterborn, Waldgrehweiler, Niedermoschel, u.a.",
    plz: "67822"
  },
  {
    lati: 49.5499875403, longi: 7.75304914086,
    name: "Schneckenhausen",
    plz: "67699"
  },
  {
    lati: 50.9632769003, longi: 7.86356259478,
    name: "Wenden",
    plz: "57482"
  },
  {
    lati: 49.1936508869, longi: 8.55128379126,
    name: "Hambrücken",
    plz: "76707"
  },
  {
    lati: 50.2069295529, longi: 8.84353840351,
    name: "Schöneck",
    plz: "61137"
  },
  {
    lati: 49.7222369341, longi: 9.66453793059,
    name: "Neubrunn",
    plz: "97277"
  },
  {
    lati: 54.3480639687, longi: 10.1948647398,
    name: "Mönkeberg",
    plz: "24248"
  },
  {
    lati: 52.3404662386, longi: 10.6764506645,
    name: "Lehre",
    plz: "38165"
  },
  {
    lati: 50.7733504956, longi: 10.8769625929,
    name: "Arnstadt",
    plz: "99338"
  },
  {
    lati: 52.0617393971, longi: 10.309470798,
    name: "Haverlah",
    plz: "38275"
  },
  {
    lati: 50.075397678, longi: 10.5846865236,
    name: "Königsberg i. Bay.",
    plz: "97486"
  },
  {
    lati: 51.2843104424, longi: 12.0955844023,
    name: "Bad Dürrenberg",
    plz: "06231"
  },
  {
    lati: 51.4025353109, longi: 12.3341806989,
    name: "Leipzig",
    plz: "04158"
  },
  {
    lati: 49.5733253746, longi: 12.4246789692,
    name: "Moosbach",
    plz: "92709"
  },
  {
    lati: 48.9389183865, longi: 11.3826619484,
    name: "Kipfenberg",
    plz: "85110"
  },
  {
    lati: 51.1138308857, longi: 11.9322702289,
    name: "Weißenfels, Stößen",
    plz: "06667"
  },
  {
    lati: 54.3003019885, longi: 13.0516596081,
    name: "Stralsund",
    plz: "18437"
  },
  {
    lati: 51.8813664318, longi: 13.4273165057,
    name: "Dahme u.a.",
    plz: "15936"
  },
  {
    lati: 51.0105241827, longi: 14.7597859721,
    name: "Herrnhut",
    plz: "02747"
  },
  {
    lati: 48.8372857479, longi: 9.14118437131,
    name: "Stuttgart",
    plz: "70439"
  },
  {
    lati: 51.3884319917, longi: 12.8030502407,
    name: "Wurzen",
    plz: "04808"
  },
  {
    lati: 48.0141864475, longi: 12.4005829907,
    name: "Obing",
    plz: "83119"
  },
  {
    lati: 48.7693028014, longi: 13.3003285408,
    name: "Thurmansbang",
    plz: "94169"
  },
  {
    lati: 49.1552129705, longi: 11.6174936419,
    name: "Seubersdorf i.d. OPf.",
    plz: "92358"
  },
  {
    lati: 51.2851295926, longi: 9.41534053375,
    name: "Kassel",
    plz: "34132"
  },
  {
    lati: 49.2782012493, longi: 8.40972210396,
    name: "Römerberg",
    plz: "67354"
  },
  {
    lati: 47.9890845852, longi: 7.86344619493,
    name: "Freiburg im Breisgau",
    plz: "79117"
  },
  {
    lati: 52.5948513438, longi: 8.22288377273,
    name: "Steinfeld",
    plz: "49439"
  },
  {
    lati: 49.3023561806, longi: 6.78149202392,
    name: "Ensdorf",
    plz: "66806"
  },
  {
    lati: 51.4519837189, longi: 6.96755532639,
    name: "Essen",
    plz: "45144"
  },
  {
    lati: 52.9913365563, longi: 7.51683941372,
    name: "Surwold",
    plz: "26903"
  },
  {
    lati: 54.7676618699, longi: 8.79363571156,
    name: "Niebüll",
    plz: "25899"
  },
  {
    lati: 49.0142528188, longi: 9.13812041157,
    name: "Walheim",
    plz: "74399"
  },
  {
    lati: 48.5119740827, longi: 9.20391313692,
    name: "Reutlingen",
    plz: "72760"
  },
  {
    lati: 50.876850645, longi: 9.36103372909,
    name: "Neukirchen (Knüll)",
    plz: "34626"
  },
  {
    lati: 51.3971345583, longi: 7.49267522951,
    name: "Hagen",
    plz: "58099"
  },
  {
    lati: 53.491243321, longi: 10.0284574157,
    name: "Hamburg",
    plz: "21109"
  },
  {
    lati: 48.1684052756, longi: 10.0629359717,
    name: "Balzheim",
    plz: "88481"
  },
  {
    lati: 47.5772281687, longi: 9.68942845161,
    name: "Lindau (Bodensee)",
    plz: "88131"
  },
  {
    lati: 53.8157236292, longi: 10.6402312847,
    name: "Lübeck",
    plz: "23560"
  },
  {
    lati: 49.518596277, longi: 10.4240334445,
    name: "Bad Windsheim",
    plz: "91438"
  },
  {
    lati: 49.3261372209, longi: 7.33889410809,
    name: "Homburg",
    plz: "66424"
  },
  {
    lati: 51.3638523131, longi: 12.6123899946,
    name: "Machern",
    plz: "04827"
  },
  {
    lati: 49.42091595, longi: 12.1351935644,
    name: "Stulln",
    plz: "92551"
  },
  {
    lati: 53.4811908772, longi: 14.2413092037,
    name: "Löcknitz, Rothenklempenow",
    plz: "17321"
  },
  {
    lati: 48.167245791, longi: 11.4039387172,
    name: "München",
    plz: "81249"
  },
  {
    lati: 53.3591225713, longi: 7.25646579375,
    name: "Emden",
    plz: "26725"
  },
  {
    lati: 49.4687615609, longi: 8.04400503451,
    name: "Weisenheim am Berg",
    plz: "67273"
  },
  {
    lati: 49.8582440667, longi: 9.0747681582,
    name: "Mömlingen",
    plz: "63853"
  },
  {
    lati: 49.755380789, longi: 8.74524792644,
    name: "Modautal",
    plz: "64397"
  },
  {
    lati: 53.7241507423, longi: 8.77935222639,
    name: "Wanna",
    plz: "21776"
  },
  {
    lati: 49.9699453873, longi: 9.74393390228,
    name: "Karlstadt",
    plz: "97753"
  },
  {
    lati: 51.3114192385, longi: 9.40134102643,
    name: "Kassel",
    plz: "34131"
  },
  {
    lati: 51.2876005704, longi: 11.8887200794,
    name: "Braunsbedra",
    plz: "06242"
  },
  {
    lati: 52.0751660271, longi: 10.821837826,
    name: "Gevensleben",
    plz: "38384"
  },
  {
    lati: 51.0400386243, longi: 7.25584200252,
    name: "Kürten",
    plz: "51515"
  },
  {
    lati: 49.6784674269, longi: 6.88298113941,
    name: "Reinsfeld",
    plz: "54421"
  },
  {
    lati: 48.5307927196, longi: 11.8112499019,
    name: "Nandlstadt",
    plz: "85405"
  },
  {
    lati: 50.7937792566, longi: 7.83896271389,
    name: "Scheuerfeld",
    plz: "57584"
  },
  {
    lati: 49.8294145749, longi: 7.87307965106,
    name: "Bad Kreuznach",
    plz: "55543"
  },
  {
    lati: 49.7445007841, longi: 9.34854799387,
    name: "Freudenberg, Collenberg",
    plz: "97896"
  },
  {
    lati: 48.928972249, longi: 9.56455819377,
    name: "Althütte",
    plz: "71566"
  },
  {
    lati: 48.5152653661, longi: 9.61580871893,
    name: "Westerheim",
    plz: "72589"
  },
  {
    lati: 48.8573810343, longi: 9.79013613369,
    name: "Durlangen, Weggen-Ziegelhütte, Leinhäusle",
    plz: "73568"
  },
  {
    lati: 53.2894978332, longi: 9.78272376368,
    name: "Buchholz in der Nordheide",
    plz: "21244"
  },
  {
    lati: 53.2183616195, longi: 10.3867639108,
    name: "Lüneburg",
    plz: "21335"
  },
  {
    lati: 53.8669616001, longi: 10.6880618486,
    name: "Lübeck",
    plz: "23552"
  },
  {
    lati: 48.4133879072, longi: 13.1265470341,
    name: "Bayerbach",
    plz: "94137"
  },
  {
    lati: 47.9089033122, longi: 10.9823907147,
    name: "Rott",
    plz: "86935"
  },
  {
    lati: 48.4425758064, longi: 12.0461490028,
    name: "Buch a. Erlbach",
    plz: "84172"
  },
  {
    lati: 47.9488243346, longi: 10.7465418044,
    name: "Oberostendorf",
    plz: "86869"
  },
  {
    lati: 48.4421444256, longi: 8.0022106798,
    name: "Ohlsbach",
    plz: "77797"
  },
  {
    lati: 47.866792344, longi: 8.20005082729,
    name: "Lenzkirch",
    plz: "79853"
  },
  {
    lati: 47.6749983441, longi: 7.74280058981,
    name: "Steinen",
    plz: "79585"
  },
  {
    lati: 49.2636979326, longi: 8.69162794175,
    name: "Rauenberg",
    plz: "69231"
  },
  {
    lati: 49.1319526928, longi: 9.38753048563,
    name: "Obersulm",
    plz: "74182"
  },
  {
    lati: 47.8796359556, longi: 12.9280021032,
    name: "Saaldorf",
    plz: "83416"
  },
  {
    lati: 50.7962135539, longi: 13.2544328644,
    name: "Eppendorf",
    plz: "09575"
  },
  {
    lati: 52.4508784703, longi: 13.4281566109,
    name: "Berlin Britz",
    plz: "12347"
  },
  {
    lati: 50.4495111052, longi: 7.77518649033,
    name: "Dernbach (Westerwald)",
    plz: "56428"
  },
  {
    lati: 49.961407623, longi: 7.81502568324,
    name: "Waldalgesheim",
    plz: "55425"
  },
  {
    lati: 49.469686659, longi: 8.87660009657,
    name: "Hirschhorn, Brombach, Heddesbach",
    plz: "69434"
  },
  {
    lati: 54.5304809839, longi: 9.96681387199,
    name: "Waabs",
    plz: "24369"
  },
  {
    lati: 49.3459929405, longi: 9.80162063885,
    name: "Mulfingen",
    plz: "74673"
  },
  {
    lati: 52.3769319678, longi: 9.74796961893,
    name: "Hannover",
    plz: "30161"
  },
  {
    lati: 52.0290334956, longi: 10.2390335362,
    name: "Sehlde",
    plz: "38279"
  },
  {
    lati: 51.1397550607, longi: 6.90390839864,
    name: "Düsseldorf",
    plz: "40595"
  },
  {
    lati: 51.3592567381, longi: 7.48168321376,
    name: "Hagen",
    plz: "58095"
  },
  {
    lati: 50.7772432228, longi: 7.90687849111,
    name: "Emmerzhausen, Niederdreisbach, Steinebach",
    plz: "57520"
  },
  {
    lati: 51.5184846722, longi: 6.31050491951,
    name: "Geldern",
    plz: "47608"
  },
  {
    lati: 51.0315633025, longi: 7.05802641419,
    name: "Leverkusen",
    plz: "51375"
  },
  {
    lati: 49.2256391646, longi: 7.07799762018,
    name: "Saarbrücken",
    plz: "66132"
  },
  {
    lati: 48.1889376057, longi: 9.13871482461,
    name: "Winterlingen",
    plz: "72474"
  },
  {
    lati: 50.4095279917, longi: 9.13696572745,
    name: "Hirzenhain",
    plz: "63697"
  },
  {
    lati: 47.6790125539, longi: 9.32001141382,
    name: "Meersburg",
    plz: "88709"
  },
  {
    lati: 47.7836929079, longi: 9.40069798031,
    name: "Deggenhausertal",
    plz: "88693"
  },
  {
    lati: 51.6644317725, longi: 9.52364737654,
    name: "Bodenfelde, Wahlsburg",
    plz: "37194"
  },
  {
    lati: 49.8823152133, longi: 8.62896143531,
    name: "Darmstadt",
    plz: "64293"
  },
  {
    lati: 52.7540762489, longi: 8.75595637131,
    name: "Neuenkirchen, Scholen",
    plz: "27251"
  },
  {
    lati: 54.3746136987, longi: 8.83662528923,
    name: "Tetenbüll",
    plz: "25882"
  },
  {
    lati: 47.7044178819, longi: 12.1047140788,
    name: "Flintsbach a. Inn",
    plz: "83126"
  },
  {
    lati: 48.0141855452, longi: 11.5359401414,
    name: "Straßlach-Dingharting",
    plz: "82064"
  },
  {
    lati: 53.8944898855, longi: 9.99485470185,
    name: "Schmalfeld",
    plz: "24640"
  },
  {
    lati: 51.5303574159, longi: 7.69343479727,
    name: "Unna",
    plz: "59423"
  },
  {
    lati: 52.671926088, longi: 7.81643557831,
    name: "Menslage",
    plz: "49637"
  },
  {
    lati: 48.6786398695, longi: 8.9777028,
    name: "Böblingen",
    plz: "71034"
  },
  {
    lati: 47.8929803392, longi: 8.94540478004,
    name: "Stockach",
    plz: "78333"
  },
  {
    lati: 50.7774622955, longi: 9.1145608219,
    name: "Kirtorf",
    plz: "36320"
  },
  {
    lati: 47.8884783366, longi: 10.840170116,
    name: "Denklingen",
    plz: "86920"
  },
  {
    lati: 47.8252866403, longi: 10.5187311303,
    name: "Aitrang",
    plz: "87648"
  },
  {
    lati: 54.2428518083, longi: 11.0438017648,
    name: "Grube",
    plz: "23749"
  },
  {
    lati: 50.2211024173, longi: 8.65838691065,
    name: "Bad Homburg v.d. Höhe",
    plz: "61352"
  },
  {
    lati: 48.6811758924, longi: 9.27694068732,
    name: "Neuhausen auf den Fildern",
    plz: "73765"
  },
  {
    lati: 52.5890775269, longi: 9.15571645051,
    name: "Estorf",
    plz: "31629"
  },
  {
    lati: 48.9678275025, longi: 9.18376388889,
    name: "Ingersheim",
    plz: "74379"
  },
  {
    lati: 50.6653780558, longi: 7.99011581989,
    name: "Nisterau u.a.",
    plz: "56472"
  },
  {
    lati: 51.4391567124, longi: 6.7403321613,
    name: "Duisburg",
    plz: "47059"
  },
  {
    lati: 49.7618002039, longi: 7.77770532689,
    name: "Hallgarten",
    plz: "67826"
  },
  {
    lati: 51.0141715216, longi: 13.7015693251,
    name: "Dresden",
    plz: "01189"
  },
  {
    lati: 47.7566719509, longi: 11.2999313513,
    name: "Antdorf",
    plz: "82387"
  },
  {
    lati: 50.3331751611, longi: 8.04837524464,
    name: "Niederneisen",
    plz: "65629"
  },
  {
    lati: 53.999250072, longi: 10.4067418056,
    name: "Wensin",
    plz: "23827"
  },
  {
    lati: 51.5564895917, longi: 10.1101615402,
    name: "Seulingen, Waake u.a.",
    plz: "37136"
  },
  {
    lati: 49.5848542513, longi: 12.1285183212,
    name: "Luhe-Wildenau",
    plz: "92706"
  },
  {
    lati: 50.4889003015, longi: 12.4200586049,
    name: "Auerbach/Vogtl.",
    plz: "08209"
  },
  {
    lati: 51.4857833711, longi: 7.14119270299,
    name: "Bochum",
    plz: "44866"
  },
  {
    lati: 50.1443020168, longi: 8.65096507997,
    name: "Frankfurt am Main",
    plz: "60431"
  },
  {
    lati: 48.9822812569, longi: 12.7653997042,
    name: "Neukirchen",
    plz: "94362"
  },
  {
    lati: 48.8883879039, longi: 9.16679860833,
    name: "Ludwigsburg",
    plz: "71636"
  },
  {
    lati: 52.9291403574, longi: 9.36103875995,
    name: "Kirchlinteln",
    plz: "27308"
  },
  {
    lati: 53.8347499847, longi: 10.4809012726,
    name: "Reinfeld (Holstein)",
    plz: "23858"
  },
  {
    lati: 53.5850931673, longi: 10.4451792626,
    name: "Trittau",
    plz: "22946"
  },
  {
    lati: 52.1981738588, longi: 10.9514051609,
    name: "Wolsdorf",
    plz: "38379"
  },
  {
    lati: 49.9070785636, longi: 11.4194519846,
    name: "Mistelgau",
    plz: "95490"
  },
  {
    lati: 52.2895778585, longi: 7.58480803424,
    name: "Hörstel",
    plz: "48477"
  },
  {
    lati: 49.7771837515, longi: 7.79008222152,
    name: "Feilbingert",
    plz: "67824"
  },
  {
    lati: 51.0114876451, longi: 8.7745318273,
    name: "Burgwald",
    plz: "35099"
  },
  {
    lati: 48.7247017306, longi: 8.52799996137,
    name: "Bad Wildbad",
    plz: "75323"
  },
  {
    lati: 52.4011681001, longi: 9.80374299741,
    name: "Hannover",
    plz: "30655"
  },
  {
    lati: 50.7621583367, longi: 12.4755336543,
    name: "Zwickau",
    plz: "08058"
  },
  {
    lati: 54.5484121371, longi: 13.102687467,
    name: "Hiddensee",
    plz: "18565"
  },
  {
    lati: 52.4623941892, longi: 13.4822189348,
    name: "Berlin Baumschulenweg",
    plz: "12437"
  },
  {
    lati: 53.5383189963, longi: 13.7499249613,
    name: "Strasburg",
    plz: "17335"
  },
  {
    lati: 51.1445771492, longi: 7.06101567503,
    name: "Solingen",
    plz: "42657"
  },
  {
    lati: 51.183089521, longi: 8.31582689668,
    name: "Schmallenberg",
    plz: "57392"
  },
  {
    lati: 48.576119075, longi: 8.78114689821,
    name: "Jettingen",
    plz: "71131"
  },
  {
    lati: 52.1448066262, longi: 11.6497398,
    name: "Magdeburg",
    plz: "39106"
  },
  {
    lati: 51.441434433, longi: 11.8165192464,
    name: "Teutschenthal",
    plz: "06179"
  },
  {
    lati: 51.4459273288, longi: 7.01260698459,
    name: "Essen",
    plz: "45128"
  },
  {
    lati: 48.0753728066, longi: 9.02108378666,
    name: "Beuron",
    plz: "88631"
  },
  {
    lati: 48.7634645092, longi: 11.9141442632,
    name: "Kirchdorf",
    plz: "93348"
  },
  {
    lati: 49.8872204119, longi: 12.3506532457,
    name: "Tirschenreuth",
    plz: "95643"
  },
  {
    lati: 53.1544116297, longi: 12.4902971254,
    name: "Wittstock/Dosse, Heiligengrabe",
    plz: "16909"
  },
  {
    lati: 47.8935033622, longi: 8.07798607427,
    name: "Hinterzarten",
    plz: "79856"
  },
  {
    lati: 48.2774397593, longi: 10.478787363,
    name: "Thannhausen",
    plz: "86470"
  },
  {
    lati: 48.1480097835, longi: 11.6087753334,
    name: "München",
    plz: "81679"
  },
  {
    lati: 49.1763035608, longi: 10.917291073,
    name: "Spalt",
    plz: "91174"
  },
  {
    lati: 48.1224248548, longi: 11.1277672276,
    name: "Kottgeisering",
    plz: "82288"
  },
  {
    lati: 48.3874877074, longi: 9.40045049614,
    name: "Gomadingen",
    plz: "72532"
  },
  {
    lati: 48.0959063153, longi: 9.4727056892,
    name: "Ertingen",
    plz: "88521"
  },
  {
    lati: 48.5821544218, longi: 8.11407426616,
    name: "Kappelrodeck",
    plz: "77876"
  },
  {
    lati: 49.4881894519, longi: 8.29133530027,
    name: "Maxdorf",
    plz: "67133"
  },
  {
    lati: 49.5411399055, longi: 6.88583879799,
    name: "Wadern",
    plz: "66687"
  },
  {
    lati: 47.8642263677, longi: 11.6793105695,
    name: "Holzkirchen",
    plz: "83607"
  },
  {
    lati: 51.8386007651, longi: 11.7833819709,
    name: "Nienburg (Saale)",
    plz: "06429"
  },
  {
    lati: 48.940497333, longi: 11.803947338,
    name: "Essing",
    plz: "93343"
  },
  {
    lati: 50.0277237638, longi: 11.9124941321,
    name: "Tröstau",
    plz: "95709"
  },
  {
    lati: 47.9456390451, longi: 11.2847865463,
    name: "Feldafing",
    plz: "82340"
  },
  {
    lati: 49.2512456314, longi: 11.3853044293,
    name: "Berngau",
    plz: "92361"
  },
  {
    lati: 49.506335578, longi: 11.4274420646,
    name: "Hersbruck",
    plz: "91217"
  },
  {
    lati: 51.6074803963, longi: 13.3228447303,
    name: "Uebigau-Wahrenbrück",
    plz: "04938"
  },
  {
    lati: 49.1459737427, longi: 8.07103924667,
    name: "Billigheim-Ingenheim, Birkweiler",
    plz: "76831"
  },
  {
    lati: 51.2641815116, longi: 7.22024441839,
    name: "Wuppertal",
    plz: "42289"
  },
  {
    lati: 48.337369138, longi: 9.69161969189,
    name: "Allmendingen",
    plz: "89604"
  },
  {
    lati: 54.4985907354, longi: 9.86683221527,
    name: "Barkelsby",
    plz: "24360"
  },
  {
    lati: 49.7243435894, longi: 10.012076262,
    name: "Eibelstadt",
    plz: "97246"
  },
  {
    lati: 53.3807783943, longi: 10.1096944343,
    name: "Stelle",
    plz: "21435"
  },
  {
    lati: 48.6804719581, longi: 11.4732216753,
    name: "Baar-Ebenhausen",
    plz: "85107"
  },
  {
    lati: 48.4745828847, longi: 11.5635548645,
    name: "Paunzhausen",
    plz: "85307"
  },
  {
    lati: 48.6137358888, longi: 10.8641543349,
    name: "Ellgau",
    plz: "86679"
  },
  {
    lati: 48.2893046172, longi: 13.0156058797,
    name: "Simbach a. Inn",
    plz: "84359"
  },
  {
    lati: 49.3977854313, longi: 8.44931941781,
    name: "Waldsee",
    plz: "67165"
  },
  {
    lati: 49.7645866191, longi: 8.2782466663,
    name: "Dorn-Dürkheim",
    plz: "67585"
  },
  {
    lati: 50.8651070492, longi: 6.95061943423,
    name: "Köln",
    plz: "50997"
  },
  {
    lati: 47.7277324641, longi: 11.2871581077,
    name: "Habach",
    plz: "82392"
  },
  {
    lati: 48.7227079385, longi: 11.3947751197,
    name: "Ingolstadt",
    plz: "85051"
  },
  {
    lati: 49.7390848235, longi: 6.67666083214,
    name: "Trier",
    plz: "54295"
  },
  {
    lati: 48.7570602046, longi: 9.32526119392,
    name: "Esslingen am Neckar",
    plz: "73732"
  },
  {
    lati: 49.4066076175, longi: 12.4107954605,
    name: "Dieterskirchen",
    plz: "92542"
  },
  {
    lati: 48.1107153332, longi: 11.5890959961,
    name: "München",
    plz: "81539"
  },
  {
    lati: 53.3938591196, longi: 10.3517042563,
    name: "Marschacht",
    plz: "21436"
  },
  {
    lati: 48.2856947827, longi: 8.02849111153,
    name: "Steinach",
    plz: "77790"
  },
  {
    lati: 48.8631584188, longi: 8.18564481408,
    name: "Rastatt",
    plz: "76437"
  },
  {
    lati: 49.6085029673, longi: 8.1870183162,
    name: "Bockenheim an der Weinstraße",
    plz: "67278"
  },
  {
    lati: 50.8621005779, longi: 6.37089697847,
    name: "Inden",
    plz: "52459"
  },
  {
    lati: 52.3202590736, longi: 7.35273893568,
    name: "Salzbergen",
    plz: "48499"
  },
  {
    lati: 49.0728938233, longi: 9.30448534632,
    name: "Abstatt",
    plz: "74232"
  },
  {
    lati: 52.0470576329, longi: 8.52092544723,
    name: "Bielefeld",
    plz: "33613"
  },
  {
    lati: 47.9208708713, longi: 7.68083875002,
    name: "Bad Krozingen",
    plz: "79189"
  },
  {
    lati: 50.1480104281, longi: 9.64595627282,
    name: "Burgsinn",
    plz: "97775"
  },
  {
    lati: 53.1796154853, longi: 10.0486815001,
    name: "Eggestorf",
    plz: "21272"
  },
  {
    lati: 49.715934743, longi: 10.4306978584,
    name: "Oberscheinfeld",
    plz: "91483"
  },
  {
    lati: 48.217157311, longi: 11.5149477676,
    name: "München",
    plz: "80995"
  },
  {
    lati: 48.0606813897, longi: 11.6728973307,
    name: "Ottobrunn/Riemerling",
    plz: "85521"
  },
  {
    lati: 50.0782274361, longi: 11.8522368777,
    name: "Weißenstadt",
    plz: "95163"
  },
  {
    lati: 48.4493161346, longi: 10.9640349303,
    name: "Affing",
    plz: "86444"
  },
  {
    lati: 51.6127940779, longi: 7.11079584436,
    name: "Herten",
    plz: "45701"
  },
  {
    lati: 52.52799528, longi: 8.36541651472,
    name: "Lembruch",
    plz: "49459"
  },
  {
    lati: 50.1447587631, longi: 7.751681097,
    name: "Sankt Goarshausen",
    plz: "56346"
  },
  {
    lati: 51.005746311, longi: 12.9903617815,
    name: "Mittweida, Kriebstein",
    plz: "09648"
  },
  {
    lati: 48.2898294758, longi: 11.0743990832,
    name: "Ried",
    plz: "86510"
  },
  {
    lati: 47.9103738675, longi: 11.2449572592,
    name: "Tutzing",
    plz: "82327"
  },
  {
    lati: 52.4357504787, longi: 13.5432153349,
    name: "Berlin Teltowkanal III",
    plz: "12489"
  },
  {
    lati: 48.1281220582, longi: 11.6314949619,
    name: "München",
    plz: "81673"
  },
  {
    lati: 50.5967347798, longi: 12.2443231207,
    name: "Netzschkau, Limbach",
    plz: "08491"
  },
  {
    lati: 50.06384094, longi: 10.9612153361,
    name: "Ebensfeld",
    plz: "96250"
  },
  {
    lati: 50.7928658499, longi: 10.2903667795,
    name: "Barchfeld-Immelborn",
    plz: "36456"
  },
  {
    lati: 51.8866806972, longi: 12.8078018285,
    name: "Zahna-Elster",
    plz: "06895"
  },
  {
    lati: 52.1794399194, longi: 7.63818545028,
    name: "Saerbeck",
    plz: "48369"
  },
  {
    lati: 50.2006789069, longi: 7.81641628035,
    name: "Miehlen u.a.",
    plz: "56357"
  },
  {
    lati: 50.8459304181, longi: 8.55722265703,
    name: "Dautphetal",
    plz: "35232"
  },
  {
    lati: 49.3625409303, longi: 8.99266939391,
    name: "Aglasterhausen",
    plz: "74858"
  },
  {
    lati: 49.1062915101, longi: 8.86223516307,
    name: "Sulzfeld",
    plz: "75056"
  },
  {
    lati: 54.3088382388, longi: 9.58578305709,
    name: "Fockbek",
    plz: "24787"
  },
  {
    lati: 51.3858477386, longi: 9.89422069782,
    name: "Neu-Eichenberg",
    plz: "37249"
  },
  {
    lati: 53.6249743829, longi: 9.37316810763,
    name: "Hammah",
    plz: "21714"
  },
  {
    lati: 48.0329846016, longi: 11.6328981106,
    name: "Taufkirchen",
    plz: "82024"
  },
  {
    lati: 52.2980325103, longi: 8.86707474048,
    name: "Minden",
    plz: "32427"
  },
  {
    lati: 49.2998991119, longi: 7.07139443471,
    name: "Sulzbach/Saar",
    plz: "66280"
  },
  {
    lati: 49.2203961946, longi: 6.80256854397,
    name: "Völklingen",
    plz: "66333"
  },
  {
    lati: 50.9187242111, longi: 6.92513332887,
    name: "Köln",
    plz: "50939"
  },
  {
    lati: 48.7731601154, longi: 11.0478394406,
    name: "Rennertshofen",
    plz: "86643"
  },
  {
    lati: 54.2777062189, longi: 9.41802487473,
    name: "Meggerdorf, Friedrichsholm, Friedrichsgraben u.a.",
    plz: "24799"
  },
  {
    lati: 51.9500034158, longi: 13.5888837447,
    name: "Golßen",
    plz: "15938"
  },
  {
    lati: 52.5184530031, longi: 13.7638680523,
    name: "Fredersdorf-Vogelsdorf, Petershagen",
    plz: "15370"
  },
  {
    lati: 48.8237641854, longi: 9.57706414718,
    name: "Urbach",
    plz: "73660"
  },
  {
    lati: 49.454745486, longi: 9.77364351313,
    name: "Bad Mergentheim",
    plz: "97980"
  },
  {
    lati: 47.9790136506, longi: 10.8438111561,
    name: "Unterdießen",
    plz: "86944"
  },
  {
    lati: 49.10270139, longi: 10.3792392277,
    name: "Dürrwangen",
    plz: "91602"
  },
  {
    lati: 50.9424256309, longi: 6.93592964079,
    name: "Köln",
    plz: "50672"
  },
  {
    lati: 50.9572565239, longi: 8.67792966439,
    name: "Münchhausen",
    plz: "35117"
  },
  {
    lati: 48.6243399393, longi: 10.5746571855,
    name: "Höchstädt a.d. Donau",
    plz: "89420"
  },
  {
    lati: 52.5337125623, longi: 13.4490605171,
    name: "Berlin Prenzlauer Berg",
    plz: "10407"
  },
  {
    lati: 51.6164995094, longi: 7.99346393495,
    name: "Welver",
    plz: "59514"
  },
  {
    lati: 48.9191256814, longi: 9.33803350989,
    name: "Affalterbach",
    plz: "71563"
  },
  {
    lati: 53.0849066936, longi: 11.2634229413,
    name: "Langendorf",
    plz: "29484"
  },
  {
    lati: 49.6028039985, longi: 11.026104033,
    name: "Erlangen",
    plz: "91054"
  },
  {
    lati: 53.6522754367, longi: 9.73716356505,
    name: "Appen",
    plz: "25482"
  },
  {
    lati: 53.6803488585, longi: 9.76221109167,
    name: "Prisdorf",
    plz: "25497"
  },
  {
    lati: 48.2238030787, longi: 9.85084406484,
    name: "Laupheim",
    plz: "88471"
  },
  {
    lati: 47.9381516554, longi: 12.4555888441,
    name: "Seeon-Seebruck",
    plz: "83358"
  },
  {
    lati: 51.8188401676, longi: 12.9707924065,
    name: "Jessen (Elster)",
    plz: "06917"
  },
  {
    lati: 52.8621317207, longi: 12.2200896675,
    name: "Neustadt (Dosse) u.a.",
    plz: "16845"
  },
  {
    lati: 53.772225152, longi: 10.6617897253,
    name: "Krummesse, Klempau",
    plz: "23628"
  },
  {
    lati: 48.800519504, longi: 8.20157254685,
    name: "Baden-Baden",
    plz: "76532"
  },
  {
    lati: 47.6517146039, longi: 8.57413759874,
    name: "Jestetten",
    plz: "79798"
  },
  {
    lati: 54.3028664222, longi: 9.98622217033,
    name: "Achterwehr",
    plz: "24239"
  },
  {
    lati: 53.8310805939, longi: 10.1690152786,
    name: "Seth",
    plz: "23845"
  },
  {
    lati: 47.6636493669, longi: 12.1138060444,
    name: "Oberaudorf",
    plz: "83080"
  },
  {
    lati: 47.7177972656, longi: 7.85730252099,
    name: "Zell im Wiesental",
    plz: "79669"
  },
  {
    lati: 48.1616977326, longi: 11.5060080194,
    name: "München",
    plz: "80638"
  },
  {
    lati: 48.0864201303, longi: 11.883016719,
    name: "Kirchseeon",
    plz: "85614"
  },
  {
    lati: 54.0489243197, longi: 10.5260114317,
    name: "Glasau",
    plz: "23719"
  },
  {
    lati: 50.7685280548, longi: 10.602918114,
    name: "Tambach-Dietharz/ Thür.",
    plz: "99897"
  },
  {
    lati: 50.0276834769, longi: 10.8325415858,
    name: "Reckendorf",
    plz: "96182"
  },
  {
    lati: 51.982487376, longi: 7.54529113463,
    name: "Münster",
    plz: "48161"
  },
  {
    lati: 49.7204466611, longi: 7.56241378841,
    name: "Hundsbach",
    plz: "55621"
  },
  {
    lati: 48.5416877433, longi: 8.32529736412,
    name: "Baiersbronn",
    plz: "72270"
  },
  {
    lati: 48.3912209208, longi: 11.1634522168,
    name: "Sielenbach",
    plz: "86577"
  },
  {
    lati: 51.6866158875, longi: 7.1193060355,
    name: "Marl",
    plz: "45772"
  },
  {
    lati: 51.4473688531, longi: 6.38886135974,
    name: "Kerken",
    plz: "47647"
  },
  {
    lati: 51.3904235673, longi: 12.5077405095,
    name: "Taucha",
    plz: "04425"
  },
  {
    lati: 48.6858314657, longi: 9.14487048904,
    name: "Leinfelden-Echterdingen",
    plz: "70771"
  },
  {
    lati: 49.6478451896, longi: 7.56323301305,
    name: "Lauterecken u.a.",
    plz: "67742"
  },
  {
    lati: 50.7155069153, longi: 10.4216964547,
    name: "Schmalkalden",
    plz: "98574"
  },
  {
    lati: 48.4060997439, longi: 10.5963372628,
    name: "Zusmarshausen",
    plz: "86441"
  },
  {
    lati: 49.9290047393, longi: 8.30802597713,
    name: "Bodenheim",
    plz: "55294"
  },
  {
    lati: 49.3290606016, longi: 8.43431947496,
    name: "Speyer",
    plz: "67346"
  },
  {
    lati: 49.0908257167, longi: 8.38594769834,
    name: "Eggenstein-Leopoldshafen",
    plz: "76344"
  },
  {
    lati: 50.0427014808, longi: 8.56734091019,
    name: "Frankfurt am Main",
    plz: "60549"
  },
  {
    lati: 47.8946444284, longi: 9.62228624488,
    name: "Wolpertswende",
    plz: "88284"
  },
  {
    lati: 48.8353951705, longi: 9.88368566803,
    name: "Iggingen",
    plz: "73574"
  },
  {
    lati: 48.48947719, longi: 11.355709393,
    name: "Gerolsbach",
    plz: "85302"
  },
  {
    lati: 53.6376268134, longi: 11.3908064226,
    name: "Schwerin",
    plz: "19059"
  },
  {
    lati: 50.6865086082, longi: 6.6680374422,
    name: "Zülpich",
    plz: "53909"
  },
  {
    lati: 50.6196511658, longi: 7.5184286519,
    name: "Oberlahr",
    plz: "57641"
  },
  {
    lati: 51.9710143867, longi: 8.21499586024,
    name: "Harsewinkel",
    plz: "33428"
  },
  {
    lati: 48.6610339687, longi: 8.67429545113,
    name: "Neubulach",
    plz: "75387"
  },
  {
    lati: 52.2374013226, longi: 9.10249911671,
    name: "Heeßen, Bad Eilsen",
    plz: "31707"
  },
  {
    lati: 50.7044525387, longi: 9.97243164816,
    name: "Geisa",
    plz: "36419"
  },
  {
    lati: 49.1067581602, longi: 12.2445365949,
    name: "Bernhardswald",
    plz: "93170"
  },
  {
    lati: 50.5158364371, longi: 11.2855652669,
    name: "Gräfenthal",
    plz: "98743"
  },
  {
    lati: 48.65279343, longi: 13.0600793367,
    name: "Künzing",
    plz: "94550"
  },
  {
    lati: 49.4518000732, longi: 7.81341837637,
    name: "Kaiserslautern",
    plz: "67657"
  },
  {
    lati: 48.2022584882, longi: 8.25031750822,
    name: "Hornberg",
    plz: "78132"
  },
  {
    lati: 49.823383665, longi: 8.64477729357,
    name: "Darmstadt",
    plz: "64297"
  },
  {
    lati: 48.0131119457, longi: 8.6657502387,
    name: "Talheim",
    plz: "78607"
  },
  {
    lati: 49.0851004329, longi: 9.20129976773,
    name: "Talheim",
    plz: "74388"
  },
  {
    lati: 54.3599890736, longi: 13.2433828679,
    name: "Samtens",
    plz: "18573"
  },
  {
    lati: 50.9463038775, longi: 13.9598099092,
    name: "Pirna, Struppen, Dohma",
    plz: "01796"
  },
  {
    lati: 50.5007669279, longi: 7.35913633071,
    name: "Rheinbrohl",
    plz: "56598"
  },
  {
    lati: 49.3972859289, longi: 8.23864162781,
    name: "Meckenheim",
    plz: "67149"
  },
  {
    lati: 48.4268524319, longi: 10.2477688289,
    name: "Bubesheim",
    plz: "89347"
  },
  {
    lati: 49.9905385893, longi: 10.2583992354,
    name: "Schwebheim",
    plz: "97525"
  },
  {
    lati: 53.812635069, longi: 10.3489433151,
    name: "Bad Oldesloe",
    plz: "23843"
  },
  {
    lati: 52.5870648038, longi: 10.4427422427,
    name: "Ummern",
    plz: "29369"
  },
  {
    lati: 48.2004429425, longi: 10.9255455551,
    name: "Prittriching",
    plz: "86931"
  },
  {
    lati: 52.3939295672, longi: 12.5714257992,
    name: "Brandenburg/Havel",
    plz: "14776"
  },
  {
    lati: 48.463422091, longi: 7.92243101558,
    name: "Offenburg",
    plz: "77656"
  },
  {
    lati: 51.9013093508, longi: 8.75597037914,
    name: "Augustdorf",
    plz: "32832"
  },
  {
    lati: 51.4055758524, longi: 7.06225685786,
    name: "Essen",
    plz: "45259"
  },
  {
    lati: 51.7387152075, longi: 10.7361944942,
    name: "Oberharz am Brocken",
    plz: "38875"
  },
  {
    lati: 52.2480198885, longi: 10.4671639827,
    name: "Braunschweig",
    plz: "38120"
  },
  {
    lati: 48.0931192503, longi: 9.18917144257,
    name: "Sigmaringen",
    plz: "72488"
  },
  {
    lati: 54.5604311031, longi: 9.24861135666,
    name: "Behrendorf, Bondelum",
    plz: "25850"
  },
  {
    lati: 54.356606651, longi: 9.63234431735,
    name: "Alt Duvenstedt",
    plz: "24791"
  },
  {
    lati: 53.1968761129, longi: 11.2224034462,
    name: "Dömitz",
    plz: "19303"
  },
  {
    lati: 49.2390951175, longi: 7.63437901843,
    name: "Rodalben",
    plz: "66976"
  },
  {
    lati: 47.5526028979, longi: 9.94238506118,
    name: "Oberreute",
    plz: "88179"
  },
  {
    lati: 51.8314721133, longi: 12.4071044482,
    name: "Oranienbaum-Wörlitz",
    plz: "06785"
  },
  {
    lati: 51.2894203752, longi: 14.3216831037,
    name: "Königswartha",
    plz: "02699"
  },
  {
    lati: 51.2861332762, longi: 14.7405059664,
    name: "Niesky, Hohendubrau u.a.",
    plz: "02906"
  },
  {
    lati: 49.3672574933, longi: 11.0831227818,
    name: "Nürnberg",
    plz: "90455"
  },
  {
    lati: 48.1083862218, longi: 11.0938096093,
    name: "Türkenfeld",
    plz: "82299"
  },
  {
    lati: 52.3149462111, longi: 13.1254434611,
    name: "Nuthetal",
    plz: "14558"
  },
  {
    lati: 49.6684773983, longi: 7.5834445019,
    name: "Medard, Rathskirchen u.a.",
    plz: "67744"
  },
  {
    lati: 48.0517898248, longi: 8.42489042651,
    name: "Villingen-Schwenningen",
    plz: "78052"
  },
  {
    lati: 48.6844624052, longi: 9.05046814529,
    name: "Böblingen",
    plz: "71032"
  },
  {
    lati: 50.7027443864, longi: 8.71190160268,
    name: "Fronhausen",
    plz: "35112"
  },
  {
    lati: 50.9802950478, longi: 8.96409782813,
    name: "Gemünden",
    plz: "35285"
  },
  {
    lati: 49.6486289028, longi: 9.93612278976,
    name: "Giebelstadt",
    plz: "97232"
  },
  {
    lati: 51.344506154, longi: 12.3144829973,
    name: "Leipzig",
    plz: "04179"
  },
  {
    lati: 51.689191766, longi: 11.9204145458,
    name: "Südliches Anhalt, Köthen",
    plz: "06388"
  },
  {
    lati: 52.4136351056, longi: 8.27866283048,
    name: "Bohmte",
    plz: "49163"
  },
  {
    lati: 50.5577606769, longi: 7.56332138869,
    name: "Urbach",
    plz: "56317"
  },
  {
    lati: 49.7040274103, longi: 8.31611652752,
    name: "Osthofen",
    plz: "67574"
  },
  {
    lati: 52.6743589478, longi: 8.41215750327,
    name: "Drebber",
    plz: "49457"
  },
  {
    lati: 49.2640793527, longi: 8.61147734753,
    name: "Sankt Leon-Rot",
    plz: "68789"
  },
  {
    lati: 50.1256476049, longi: 8.60510946872,
    name: "Frankfurt am Main",
    plz: "60489"
  },
  {
    lati: 51.4641149555, longi: 7.08608241641,
    name: "Essen",
    plz: "45307"
  },
  {
    lati: 49.7178359774, longi: 6.91395425782,
    name: "Gusenburg",
    plz: "54413"
  },
  {
    lati: 51.3809839349, longi: 11.8353450567,
    name: "Bad Lauchstädt",
    plz: "06246"
  },
  {
    lati: 52.631988287, longi: 9.3428637488,
    name: "Stöckse",
    plz: "31638"
  },
  {
    lati: 48.4519261764, longi: 9.35457695819,
    name: "St. Johann",
    plz: "72813"
  },
  {
    lati: 49.3524542833, longi: 12.5308709374,
    name: "Rötz",
    plz: "92444"
  },
  {
    lati: 50.5397623696, longi: 12.3051954016,
    name: "Treuen",
    plz: "08233"
  },
  {
    lati: 48.7340596328, longi: 9.60331397616,
    name: "Wangen",
    plz: "73117"
  },
  {
    lati: 52.3765394558, longi: 9.80133148888,
    name: "Hannover",
    plz: "30625"
  },
  {
    lati: 48.2898610479, longi: 10.091140173,
    name: "Vöhringen",
    plz: "89269"
  },
  {
    lati: 49.969269895, longi: 12.1701845453,
    name: "Pechbrunn",
    plz: "95701"
  },
  {
    lati: 49.5003181025, longi: 12.3895287449,
    name: "Teunz",
    plz: "92552"
  },
  {
    lati: 53.5953767809, longi: 12.5437758537,
    name: "Grabowhöfe, Moltzow u.a.",
    plz: "17194"
  },
  {
    lati: 50.7120207377, longi: 8.5007250359,
    name: "Bischoffen",
    plz: "35649"
  },
  {
    lati: 48.6400023088, longi: 8.89794537193,
    name: "Gärtringen",
    plz: "71116"
  },
  {
    lati: 52.284156001, longi: 13.4528610726,
    name: "Rangsdorf",
    plz: "15834"
  },
  {
    lati: 48.6136465762, longi: 11.4715504785,
    name: "Pörnbach",
    plz: "85309"
  },
  {
    lati: 48.0567620478, longi: 11.5189721286,
    name: "Pullach i. Isartal",
    plz: "82049"
  },
  {
    lati: 48.587436542, longi: 11.783355919,
    name: "Rudelzhausen",
    plz: "84104"
  },
  {
    lati: 49.5073208421, longi: 11.036982528,
    name: "Nürnberg",
    plz: "90427"
  },
  {
    lati: 51.147503708, longi: 11.1793037023,
    name: "Sömmerda",
    plz: "99610"
  },
  {
    lati: 48.3076675511, longi: 11.3426446955,
    name: "Schwabhausen",
    plz: "85247"
  },
  {
    lati: 51.9237002106, longi: 10.4584421478,
    name: "Goslar",
    plz: "38642"
  },
  {
    lati: 53.8354108577, longi: 11.5219555211,
    name: "Wismar, Groß Krankow u.a.",
    plz: "23966"
  },
  {
    lati: 48.1592547919, longi: 11.5911393229,
    name: "München",
    plz: "80802"
  },
  {
    lati: 49.123445389, longi: 11.9245896272,
    name: "Duggendorf",
    plz: "93182"
  },
  {
    lati: 51.4842707157, longi: 11.9292250466,
    name: "Halle/ Saale",
    plz: "06122"
  },
  {
    lati: 54.128328586, longi: 12.0604856857,
    name: "Rostock",
    plz: "18106"
  },
  {
    lati: 51.8688239144, longi: 6.6631943291,
    name: "Bocholt",
    plz: "46397"
  },
  {
    lati: 51.0601436127, longi: 6.94606959215,
    name: "Leverkusen",
    plz: "51371"
  },
  {
    lati: 50.0603180747, longi: 7.12452252384,
    name: "Bullay, Alf, Zell",
    plz: "56859"
  },
  {
    lati: 53.0526357434, longi: 8.15568249278,
    name: "Wardenburg",
    plz: "26203"
  },
  {
    lati: 52.6356120218, longi: 9.01691814458,
    name: "Pennigsehl",
    plz: "31621"
  },
  {
    lati: 48.0128366361, longi: 9.5082081553,
    name: "Bad Saulgau, Allmannsweiler",
    plz: "88348"
  },
  {
    lati: 49.2982198234, longi: 8.34791475042,
    name: "Harthausen",
    plz: "67376"
  },
  {
    lati: 49.5137495916, longi: 9.96069146672,
    name: "Röttingen, Tauberrettersheim",
    plz: "97285"
  },
  {
    lati: 52.0723589076, longi: 10.0244188507,
    name: "Bad Salzdetfurth",
    plz: "31162"
  },
  {
    lati: 52.4031922024, longi: 13.4016077163,
    name: "Berlin",
    plz: "12305"
  },
  {
    lati: 48.6091327958, longi: 11.8628851657,
    name: "Volkenschwand",
    plz: "84106"
  },
  {
    lati: 53.5363998678, longi: 10.1775678997,
    name: "Hamburg, Oststeinbek",
    plz: "22113"
  },
  {
    lati: 51.2902993136, longi: 9.75591650034,
    name: "Witzenhausen, Gutsbezirk",
    plz: "37216"
  },
  {
    lati: 53.3643273368, longi: 9.74213398672,
    name: "Hollenstedt, Drestedt u.a.",
    plz: "21279"
  },
  {
    lati: 51.2698439377, longi: 6.82282103739,
    name: "Düsseldorf",
    plz: "40472"
  },
  {
    lati: 49.2156513384, longi: 7.26363936742,
    name: "Blieskastel",
    plz: "66440"
  },
  {
    lati: 52.0528409478, longi: 12.660343739,
    name: "Niemegk",
    plz: "14823"
  },
  {
    lati: 49.5718230481, longi: 8.13575623156,
    name: "Kindenheim",
    plz: "67271"
  },
  {
    lati: 52.5209645865, longi: 13.2686151905,
    name: "Berlin Westend",
    plz: "14050"
  },
  {
    lati: 52.089090742, longi: 11.5590813874,
    name: "Magdeburg",
    plz: "39116"
  },
  {
    lati: 52.6002758843, longi: 10.9090750031,
    name: "Brome",
    plz: "38465"
  },
  {
    lati: 49.9368190045, longi: 10.9212854159,
    name: "Gundelsheim",
    plz: "96163"
  },
  {
    lati: 49.8740877948, longi: 11.0022262671,
    name: "Strullendorf",
    plz: "96129"
  },
  {
    lati: 49.2428328655, longi: 7.43959689307,
    name: "Contwig",
    plz: "66497"
  },
  {
    lati: 48.0486428184, longi: 7.72620336835,
    name: "Gottenheim",
    plz: "79288"
  },
  {
    lati: 48.844239029, longi: 11.2037688577,
    name: "Adelschlag",
    plz: "85111"
  },
  {
    lati: 47.7286578142, longi: 12.1763891822,
    name: "Nußdorf a. Inn",
    plz: "83131"
  },
  {
    lati: 51.7807056108, longi: 12.6233680311,
    name: "Kemberg",
    plz: "06901"
  },
  {
    lati: 50.7685608774, longi: 12.9338654214,
    name: "Chemnitz",
    plz: "09123"
  },
  {
    lati: 48.7990033884, longi: 9.52972838802,
    name: "Schorndorf",
    plz: "73614"
  },
  {
    lati: 52.462639051, longi: 13.3184846311,
    name: "Berlin Steglitz",
    plz: "12163"
  },
  {
    lati: 53.5326099086, longi: 9.97850505249,
    name: "Hamburg",
    plz: "20457"
  },
  {
    lati: 51.3529745121, longi: 7.56862057877,
    name: "Hagen",
    plz: "58119"
  },
  {
    lati: 51.1700301844, longi: 8.79120915169,
    name: "Lichtenfels",
    plz: "35104"
  },
  {
    lati: 49.8699436425, longi: 9.54900019531,
    name: "Esselbach",
    plz: "97839"
  },
  {
    lati: 52.3710245146, longi: 9.68397259969,
    name: "Hannover",
    plz: "30453"
  },
  {
    lati: 48.6927330523, longi: 9.81378702842,
    name: "Donzdorf",
    plz: "73072"
  },
  {
    lati: 53.7751268918, longi: 9.50568613188,
    name: "Herzhorn, Kamerlanderdeich",
    plz: "25379"
  },
  {
    lati: 51.4311886316, longi: 11.4115952144,
    name: "Allstedt",
    plz: "06542"
  },
  {
    lati: 49.5517839615, longi: 10.7280218254,
    name: "Emskirchen",
    plz: "91448"
  },
  {
    lati: 50.2356172784, longi: 8.76776728599,
    name: "Karben",
    plz: "61184"
  },
  {
    lati: 53.8949802766, longi: 10.0714863323,
    name: "Hartenholm",
    plz: "24628"
  },
  {
    lati: 49.6678285188, longi: 10.7002467058,
    name: "Uehlfeld",
    plz: "91486"
  },
  {
    lati: 52.4854877399, longi: 13.3942349993,
    name: "Berlin Neukölln",
    plz: "10965"
  },
  {
    lati: 51.9456541385, longi: 13.8930983178,
    name: "Lübben/ Spreewald",
    plz: "15907"
  },
  {
    lati: 53.7060619251, longi: 14.2581687213,
    name: "Vogelsang-Warsin, Meiersberg, Mönkebude u.a.",
    plz: "17375"
  },
  {
    lati: 50.6148613829, longi: 7.77209217142,
    name: "Freilingen, Freirachdorf u.a.",
    plz: "56244"
  },
  {
    lati: 54.4765103988, longi: 9.49264869098,
    name: "Dannewerk",
    plz: "24867"
  },
  {
    lati: 48.9444590973, longi: 8.6763132049,
    name: "Eisingen",
    plz: "75239"
  },
  {
    lati: 48.1634376155, longi: 8.85347967852,
    name: "Obernheim",
    plz: "72364"
  },
  {
    lati: 50.1795647763, longi: 9.78256971632,
    name: "Wartmannsroth",
    plz: "97797"
  },
  {
    lati: 49.7464743149, longi: 6.75766197888,
    name: "Waldrach",
    plz: "54320"
  },
  {
    lati: 52.4379660827, longi: 7.73904534951,
    name: "Voltlage",
    plz: "49599"
  },
  {
    lati: 48.0738311121, longi: 7.71576327569,
    name: "Bötzingen",
    plz: "79268"
  },
  {
    lati: 49.2044988336, longi: 12.4839969012,
    name: "Roding",
    plz: "93426"
  },
  {
    lati: 47.7577291071, longi: 12.4514177971,
    name: "Marquartstein",
    plz: "83250"
  },
  {
    lati: 49.0172853645, longi: 12.0962090247,
    name: "Regensburg",
    plz: "93047"
  },
  {
    lati: 54.8776481574, longi: 8.35820245825,
    name: "Sylt",
    plz: "25980"
  },
  {
    lati: 51.8899141144, longi: 8.64831873887,
    name: "Schloß Holte-Stukenbrock",
    plz: "33758"
  },
  {
    lati: 53.3047377848, longi: 7.60695168305,
    name: "Hesel, Neukamperfehn u.a.",
    plz: "26835"
  },
  {
    lati: 50.1195691446, longi: 6.92123885218,
    name: "Gillenfeld",
    plz: "54558"
  },
  {
    lati: 49.6677269799, longi: 6.99524568301,
    name: "Gusenburg",
    plz: "54413"
  },
  {
    lati: 49.1016456651, longi: 10.167231026,
    name: "Stimpfach",
    plz: "74597"
  },
  {
    lati: 54.3629333057, longi: 10.2927953493,
    name: "Probsteierhagen",
    plz: "24253"
  },
  {
    lati: 47.8722200357, longi: 10.5277002471,
    name: "Friesenried",
    plz: "87654"
  },
  {
    lati: 51.8701842627, longi: 8.96895965782,
    name: "Horn-Bad Meinberg",
    plz: "32805"
  },
  {
    lati: 54.6192847819, longi: 9.27676177301,
    name: "Jörl",
    plz: "24992"
  },
  {
    lati: 53.4628794172, longi: 9.69195990828,
    name: "Buxtehude",
    plz: "21614"
  },
  {
    lati: 47.9101161712, longi: 9.90908683135,
    name: "Bad Wurzach",
    plz: "88410"
  },
  {
    lati: 49.9649978011, longi: 9.26130551696,
    name: "Bessenbach",
    plz: "63856"
  },
  {
    lati: 50.4953847681, longi: 9.33894774247,
    name: "Grebenhain",
    plz: "36355"
  },
  {
    lati: 51.6031983276, longi: 11.0391088689,
    name: "Südharz, Berga",
    plz: "06536"
  },
  {
    lati: 49.3857868397, longi: 12.3296384259,
    name: "Schwarzhofen",
    plz: "92447"
  },
  {
    lati: 48.4851304909, longi: 12.4200265138,
    name: "Gerzen",
    plz: "84175"
  },
  {
    lati: 53.9507710109, longi: 13.8100028983,
    name: "Kröslin, Krummin, Lassan u.a.",
    plz: "17440"
  },
  {
    lati: 48.6191932363, longi: 12.020820284,
    name: "Weihmichl",
    plz: "84107"
  },
  {
    lati: 52.3693212154, longi: 12.3458763057,
    name: "Wusterwitz, Rosenau, Bensdorf",
    plz: "14789"
  },
  {
    lati: 51.2044800573, longi: 13.2943637345,
    name: "Lommatzsch",
    plz: "01623"
  },
  {
    lati: 52.4988566316, longi: 13.3257004962,
    name: "Berlin Wilmersdorf",
    plz: "10719"
  },
  {
    lati: 52.4134849737, longi: 13.3751164487,
    name: "Berlin",
    plz: "12277"
  },
  {
    lati: 49.5798509236, longi: 10.8175458419,
    name: "Aurachtal",
    plz: "91086"
  },
  {
    lati: 49.9966855534, longi: 11.8171682734,
    name: "Warmensteinach",
    plz: "95485"
  },
  {
    lati: 49.3926279672, longi: 8.72523256796,
    name: "Heidelberg",
    plz: "69117"
  },
  {
    lati: 50.0127511336, longi: 8.88199881445,
    name: "Rodgau",
    plz: "63110"
  },
  {
    lati: 50.4185850432, longi: 8.01121149946,
    name: "Elz",
    plz: "65604"
  },
  {
    lati: 48.1086807267, longi: 8.41920916562,
    name: "Mönchweiler",
    plz: "78087"
  },
  {
    lati: 48.0745612294, longi: 8.63966300067,
    name: "Trossingen",
    plz: "78647"
  },
  {
    lati: 48.473153704, longi: 8.76177949721,
    name: "Eutingen im Gäu",
    plz: "72184"
  },
  {
    lati: 47.6733661855, longi: 7.99211532841,
    name: "Herrischried",
    plz: "79737"
  },
  {
    lati: 52.2690503651, longi: 10.0774726307,
    name: "Hohenhameln",
    plz: "31249"
  },
  {
    lati: 51.6452589354, longi: 10.2440040247,
    name: "Hattorf",
    plz: "37197"
  },
  {
    lati: 48.3753497431, longi: 10.2821377059,
    name: "Ichenhausen",
    plz: "89335"
  },
  {
    lati: 54.4029319345, longi: 10.2619513468,
    name: "Laboe",
    plz: "24235"
  },
  {
    lati: 48.1260523829, longi: 10.4851171499,
    name: "Salgen",
    plz: "87775"
  },
  {
    lati: 49.7301370569, longi: 10.5575708604,
    name: "Burghaslach",
    plz: "96152"
  },
  {
    lati: 48.960150308, longi: 12.0605700596,
    name: "Pentling",
    plz: "93080"
  },
  {
    lati: 49.7715306205, longi: 12.2152896761,
    name: "Püchersreuth",
    plz: "92715"
  },
  {
    lati: 52.6684124114, longi: 14.3644296681,
    name: "Letschin",
    plz: "15324"
  },
  {
    lati: 50.9367316351, longi: 6.9782010552,
    name: "Köln",
    plz: "50679"
  },
  {
    lati: 54.8521054085, longi: 8.67579765995,
    name: "Rodenäs",
    plz: "25924"
  },
  {
    lati: 52.8845512258, longi: 9.04462064249,
    name: "Martfeld, Schwarme",
    plz: "27327"
  },
  {
    lati: 49.3630209542, longi: 8.82574244259,
    name: "Wiesenbach",
    plz: "69257"
  },
  {
    lati: 53.8736054394, longi: 9.59089045367,
    name: "Lägerdorf",
    plz: "25566"
  },
  {
    lati: 51.0493189349, longi: 11.0444210006,
    name: "Erfurt",
    plz: "99095"
  },
  {
    lati: 49.9258983089, longi: 10.0257729056,
    name: "Hausen b. Würzburg",
    plz: "97262"
  },
  {
    lati: 48.7786269358, longi: 10.5219881061,
    name: "Hohenaltheim",
    plz: "86745"
  },
  {
    lati: 52.9289313416, longi: 7.23161676562,
    name: "Walchum",
    plz: "26907"
  },
  {
    lati: 51.4197352098, longi: 6.79892630255,
    name: "Duisburg",
    plz: "47057"
  },
  {
    lati: 52.2938761656, longi: 11.498888465,
    name: "Erxleben, Nordgermersleben u.a.",
    plz: "39343"
  },
  {
    lati: 47.6337853888, longi: 10.989726089,
    name: "Saulgrub",
    plz: "82442"
  },
  {
    lati: 50.0718803184, longi: 12.2370428332,
    name: "Schirnding",
    plz: "95706"
  },
  {
    lati: 50.7912958456, longi: 10.0438892076,
    name: "Vacha, Unterbreizbach",
    plz: "36404"
  },
  {
    lati: 48.5363481697, longi: 12.2823159165,
    name: "Adlkofen",
    plz: "84166"
  },
  {
    lati: 51.2918204337, longi: 12.398713297,
    name: "Leipzig",
    plz: "04279"
  },
  {
    lati: 47.6964675233, longi: 10.5981755094,
    name: "Lengenwang",
    plz: "87663"
  },
  {
    lati: 52.0170560569, longi: 13.7941093244,
    name: "Schönwalde",
    plz: "15910"
  },
  {
    lati: 49.7583242688, longi: 8.80630595676,
    name: "Fischbachtal",
    plz: "64405"
  },
  {
    lati: 48.7981454225, longi: 9.38960928832,
    name: "Weinstadt",
    plz: "71384"
  },
  {
    lati: 48.8996409893, longi: 9.38561438522,
    name: "Leutenbach",
    plz: "71397"
  },
  {
    lati: 50.6873200478, longi: 13.0202351324,
    name: "Drebach",
    plz: "09430"
  },
  {
    lati: 49.9133594933, longi: 9.39934667568,
    name: "Weibersbrunn, Rohrbrunner Forst",
    plz: "63879"
  },
  {
    lati: 51.6685105397, longi: 9.65084819697,
    name: "Uslar",
    plz: "37170"
  },
  {
    lati: 52.1025444943, longi: 12.0760485254,
    name: "Loburg, Leitzkau",
    plz: "39279"
  },
  {
    lati: 51.0173722392, longi: 12.1501770994,
    name: "Zeitz, Gutenborn u.a.",
    plz: "06712"
  },
  {
    lati: 50.5576839089, longi: 12.1978097185,
    name: "Pöhl",
    plz: "08543"
  },
  {
    lati: 51.3049728283, longi: 12.3709653826,
    name: "Leipzig",
    plz: "04277"
  },
  {
    lati: 49.8181480732, longi: 11.2492443788,
    name: "Wiesenttal",
    plz: "91346"
  },
  {
    lati: 49.2703986661, longi: 7.52920975563,
    name: "Maßweiler",
    plz: "66506"
  },
  {
    lati: 49.9571450317, longi: 8.95003506463,
    name: "Babenhausen",
    plz: "64832"
  },
  {
    lati: 54.0480851114, longi: 10.4206948018,
    name: "Seedorf",
    plz: "23823"
  },
  {
    lati: 48.1227486428, longi: 10.4447568933,
    name: "Pfaffenhausen",
    plz: "87772"
  },
  {
    lati: 51.3501409844, longi: 6.60188943324,
    name: "Krefeld",
    plz: "47800"
  },
  {
    lati: 50.4719464898, longi: 7.44767968582,
    name: "Neuwied",
    plz: "56567"
  },
  {
    lati: 51.2487651517, longi: 8.17537311171,
    name: "Eslohe",
    plz: "59889"
  },
  {
    lati: 51.3164375185, longi: 6.25028448503,
    name: "Nettetal",
    plz: "41334"
  },
  {
    lati: 48.3748341896, longi: 9.56216546992,
    name: "Mehrstetten",
    plz: "72537"
  },
  {
    lati: 50.1694879219, longi: 8.29683303048,
    name: "Niedernhausen",
    plz: "65527"
  },
  {
    lati: 48.0939589782, longi: 8.68293498387,
    name: "Aldingen",
    plz: "78554"
  },
  {
    lati: 50.9027696981, longi: 11.9670423339,
    name: "Bad Köstritz",
    plz: "07586"
  },
  {
    lati: 49.6636202822, longi: 10.1871677573,
    name: "Obernbreit",
    plz: "97342"
  },
  {
    lati: 50.2325920449, longi: 11.5432433788,
    name: "Presseck",
    plz: "95355"
  },
  {
    lati: 51.5170443072, longi: 11.5627477283,
    name: "Eisleben",
    plz: "06295"
  },
  {
    lati: 52.5737677743, longi: 13.0818969438,
    name: "Falkensee",
    plz: "14612"
  },
  {
    lati: 50.7492458976, longi: 12.6353721461,
    name: "Lichtenstein",
    plz: "09350"
  },
  {
    lati: 49.244309632, longi: 10.229861573,
    name: "Wörnitz",
    plz: "91637"
  },
  {
    lati: 49.2602615295, longi: 6.70538868124,
    name: "Überherrn",
    plz: "66802"
  },
  {
    lati: 51.4070534593, longi: 6.91604555328,
    name: "Mülheim an der Ruhr",
    plz: "45470"
  },
  {
    lati: 49.5166630338, longi: 8.45641555501,
    name: "Mannheim",
    plz: "68169"
  },
  {
    lati: 51.8771909378, longi: 8.52447870778,
    name: "Verl",
    plz: "33415"
  },
  {
    lati: 53.9963937621, longi: 9.2328188828,
    name: "Burg (Dithmarschen)",
    plz: "25712"
  },
  {
    lati: 50.5878032176, longi: 8.61818126046,
    name: "Heuchelheim",
    plz: "35452"
  },
  {
    lati: 52.5305335781, longi: 13.3214908139,
    name: "Berlin Moabit",
    plz: "10553"
  },
  {
    lati: 49.7987720692, longi: 10.3341483002,
    name: "Wiesentheid",
    plz: "97353"
  },
  {
    lati: 49.5702442422, longi: 10.3312032086,
    name: "Markt Nordheim",
    plz: "91478"
  },
  {
    lati: 50.0120480821, longi: 12.0288304658,
    name: "Bad Alexandersbad",
    plz: "95680"
  },
  {
    lati: 50.9273431469, longi: 11.5202426432,
    name: "Jena, Bucha, Großpürschütz u.a.",
    plz: "07751"
  },
  {
    lati: 51.249638118, longi: 9.25304770648,
    name: "Bad Emstal",
    plz: "34308"
  },
  {
    lati: 49.6546014483, longi: 9.27780744664,
    name: "Amorbach",
    plz: "63916"
  },
  {
    lati: 50.2133549677, longi: 9.37062693812,
    name: "Bad Orb",
    plz: "63619"
  },
  {
    lati: 49.9725061711, longi: 8.12684928675,
    name: "Wackernheim",
    plz: "55263"
  },
  {
    lati: 52.436869795, longi: 13.2385444855,
    name: "Berlin Zehlendorf",
    plz: "14163"
  },
  {
    lati: 52.1734925904, longi: 7.5228276113,
    name: "Emsdetten",
    plz: "48282"
  },
  {
    lati: 48.4796838164, longi: 9.22027038661,
    name: "Reutlingen",
    plz: "72766"
  },
  {
    lati: 48.8361916851, longi: 11.5194447784,
    name: "Kösching",
    plz: "85092"
  },
  {
    lati: 48.9473119523, longi: 8.9656022185,
    name: "Vaihingen an der Enz",
    plz: "71665"
  },
  {
    lati: 49.8208476741, longi: 10.7299962633,
    name: "Burgebrach",
    plz: "96138"
  },
  {
    lati: 49.8226159681, longi: 9.64318645151,
    name: "Erlenbach b. Marktheidenfeld",
    plz: "97837"
  },
  {
    lati: 49.1108700046, longi: 9.76084833308,
    name: "Schwäbisch Hall",
    plz: "74523"
  },
  {
    lati: 52.4171834704, longi: 9.83562616281,
    name: "Hannover",
    plz: "30659"
  },
  {
    lati: 49.8450481843, longi: 12.0983265408,
    name: "Krummennaab",
    plz: "92703"
  },
  {
    lati: 48.4459774411, longi: 12.1896264555,
    name: "Altfraunhofen",
    plz: "84169"
  },
  {
    lati: 48.9603329129, longi: 12.3807650131,
    name: "Pfatter",
    plz: "93102"
  },
  {
    lati: 50.6879083856, longi: 12.9104487287,
    name: "Auerbach",
    plz: "09392"
  },
  {
    lati: 49.2366880473, longi: 6.99783766747,
    name: "Saarbrücken",
    plz: "66111"
  },
  {
    lati: 50.9851610291, longi: 7.42390833725,
    name: "Engelskirchen",
    plz: "51766"
  },
  {
    lati: 51.9551261593, longi: 8.52944207026,
    name: "Bielefeld",
    plz: "33659"
  },
  {
    lati: 53.4562339022, longi: 8.6183288777,
    name: "Loxstedt",
    plz: "27612"
  },
  {
    lati: 50.8338245562, longi: 12.8026839488,
    name: "Chemnitz",
    plz: "09117"
  },
  {
    lati: 51.2589989174, longi: 13.2901694001,
    name: "Riesa, Stauchitz, Hirschstein",
    plz: "01594"
  },
  {
    lati: 52.4628491228, longi: 13.5788432622,
    name: "Berlin Köpenik",
    plz: "12555"
  },
  {
    lati: 48.9960840803, longi: 9.09794742759,
    name: "Löchgau",
    plz: "74369"
  },
  {
    lati: 50.9499800775, longi: 6.96284097605,
    name: "Köln",
    plz: "50668"
  },
  {
    lati: 49.5885806235, longi: 6.47607278367,
    name: "Saarburg",
    plz: "54439"
  },
  {
    lati: 53.7337710176, longi: 9.95994027059,
    name: "Norderstedt",
    plz: "22844"
  },
  {
    lati: 52.0607499547, longi: 10.389670286,
    name: "Salzgitter",
    plz: "38259"
  },
  {
    lati: 53.0620426293, longi: 10.4590466036,
    name: "Natendorf",
    plz: "29587"
  },
  {
    lati: 52.1347946377, longi: 10.7829584766,
    name: "Schöppenstedt",
    plz: "38170"
  },
  {
    lati: 53.0075724046, longi: 7.84368216778,
    name: "Friesoythe",
    plz: "26169"
  },
  {
    lati: 48.1911994686, longi: 8.80803105516,
    name: "Ratshausen",
    plz: "72365"
  },
  {
    lati: 48.597470689, longi: 8.86985970962,
    name: "Herrenberg",
    plz: "71083"
  },
  {
    lati: 47.625526577, longi: 7.91509879401,
    name: "Wehr",
    plz: "79664"
  },
  {
    lati: 49.9846862342, longi: 8.36310823213,
    name: "Bischofsheim",
    plz: "65474"
  },
  {
    lati: 53.7912119887, longi: 12.204602896,
    name: "Güstrow",
    plz: "18273"
  },
  {
    lati: 48.9485607205, longi: 12.261812363,
    name: "Mintraching",
    plz: "93098"
  },
  {
    lati: 49.3634468175, longi: 8.94167120533,
    name: "Reichartshausen",
    plz: "74934"
  },
  {
    lati: 53.7202459431, longi: 9.60931299779,
    name: "Seester",
    plz: "25370"
  },
  {
    lati: 51.3015225713, longi: 13.2707647067,
    name: "Riesa",
    plz: "01587"
  },
  {
    lati: 52.5027777328, longi: 13.3085503334,
    name: "Berlin Charlottenburg",
    plz: "10629"
  },
  {
    lati: 52.4252564778, longi: 13.4220879666,
    name: "Berlin Buckow",
    plz: "12349"
  },
  {
    lati: 50.8643346802, longi: 13.6476884207,
    name: "Dippoldiswalde",
    plz: "01744"
  },
  {
    lati: 51.8182129688, longi: 8.16684114388,
    name: "Oelde",
    plz: "59302"
  },
  {
    lati: 49.3636395098, longi: 8.7389821807,
    name: "Gaiberg",
    plz: "69251"
  },
  {
    lati: 51.8366166506, longi: 8.82976731258,
    name: "Schlangen",
    plz: "33189"
  },
  {
    lati: 50.0903518835, longi: 8.50235277607,
    name: "Frankfurt am Main",
    plz: "65931"
  },
  {
    lati: 52.5576313746, longi: 11.9477963433,
    name: "Tangermünde",
    plz: "39590"
  },
  {
    lati: 49.9425128278, longi: 10.9713676691,
    name: "Memmelsdorf",
    plz: "96117"
  },
  {
    lati: 50.7564819263, longi: 7.07299764826,
    name: "Bonn",
    plz: "53117"
  },
  {
    lati: 51.5002879861, longi: 6.80020936434,
    name: "Duisburg",
    plz: "47167"
  },
  {
    lati: 49.1935960607, longi: 8.29514858422,
    name: "Bellheim",
    plz: "76756"
  },
  {
    lati: 49.7477899675, longi: 8.44938468282,
    name: "Hamm",
    plz: "67580"
  },
  {
    lati: 48.8657467967, longi: 8.7417426092,
    name: "Pforzheim",
    plz: "75181"
  },
  {
    lati: 54.3677169858, longi: 11.0703618655,
    name: "Großenbrode",
    plz: "23775"
  },
  {
    lati: 50.236501884, longi: 11.0746701277,
    name: "Ebersdorf b. Coburg",
    plz: "96237"
  },
  {
    lati: 49.6512455095, longi: 12.4925256797,
    name: "Waidhaus",
    plz: "92726"
  },
  {
    lati: 49.167521129, longi: 12.5960615696,
    name: "Schorndorf",
    plz: "93489"
  },
  {
    lati: 48.9363930471, longi: 9.02247881082,
    name: "Oberriexingen",
    plz: "71739"
  },
  {
    lati: 47.8649017436, longi: 8.06757899844,
    name: "Feldberg",
    plz: "79868"
  },
  {
    lati: 50.6457519222, longi: 8.57599505335,
    name: "Biebertal",
    plz: "35444"
  },
  {
    lati: 48.2971719797, longi: 8.78133683097,
    name: "Geislingen",
    plz: "72351"
  },
  {
    lati: 49.416084705, longi: 8.28369451955,
    name: "Hochdorf-Assenheim",
    plz: "67126"
  },
  {
    lati: 50.9787596974, longi: 6.94338626718,
    name: "Köln",
    plz: "50735"
  },
  {
    lati: 50.9182547512, longi: 6.99651396505,
    name: "Köln",
    plz: "51105"
  },
  {
    lati: 51.2949579368, longi: 7.14050300067,
    name: "Wuppertal",
    plz: "42111"
  },
  {
    lati: 49.6087944212, longi: 7.23896245038,
    name: "Heimbach",
    plz: "55779"
  },
  {
    lati: 51.5482321491, longi: 7.30895787679,
    name: "Castrop-Rauxel",
    plz: "44575"
  },
  {
    lati: 51.0028292786, longi: 6.8853573964,
    name: "Köln",
    plz: "50767"
  },
  {
    lati: 51.1026508431, longi: 6.50660656605,
    name: "Jüchen",
    plz: "41363"
  },
  {
    lati: 51.4633913341, longi: 6.65591478815,
    name: "Moers",
    plz: "47443"
  },
  {
    lati: 47.7168567936, longi: 11.6097622339,
    name: "Gaißach",
    plz: "83674"
  },
  {
    lati: 48.6252832796, longi: 13.4801963794,
    name: "Salzweg",
    plz: "94121"
  },
  {
    lati: 52.4493074454, longi: 13.7614815021,
    name: "Woltersdorf",
    plz: "15569"
  },
  {
    lati: 48.4879671876, longi: 8.58216645071,
    name: "Waldachtal",
    plz: "72178"
  },
  {
    lati: 51.9686804982, longi: 9.60792914159,
    name: "Dielmissen",
    plz: "37633"
  },
  {
    lati: 49.9905048875, longi: 10.0836009302,
    name: "Werneck",
    plz: "97440"
  },
  {
    lati: 48.3277181433, longi: 10.304863164,
    name: "Ellzee",
    plz: "89352"
  },
  {
    lati: 54.1717338754, longi: 12.2858755313,
    name: "Rostock, Gelbensande, Rövershagen u.a.",
    plz: "18182"
  },
  {
    lati: 49.53510963, longi: 12.3287486023,
    name: "Tännesberg",
    plz: "92723"
  },
  {
    lati: 49.8740301658, longi: 9.22018844,
    name: "Hausen",
    plz: "63840"
  },
  {
    lati: 50.3479702578, longi: 8.53017566413,
    name: "Usingen",
    plz: "61250"
  },
  {
    lati: 50.9359455465, longi: 6.67797935613,
    name: "Bergheim",
    plz: "50127"
  },
  {
    lati: 49.0337212501, longi: 9.0876574738,
    name: "Bönnigheim",
    plz: "74357"
  },
  {
    lati: 49.6893834019, longi: 9.24486833824,
    name: "Miltenberg",
    plz: "63897"
  },
  {
    lati: 49.0255402457, longi: 9.16786211616,
    name: "Gemmrigheim",
    plz: "74376"
  },
  {
    lati: 52.0996630326, longi: 9.37813472021,
    name: "Hameln",
    plz: "31789"
  },
  {
    lati: 52.6334476312, longi: 13.4811556409,
    name: "Berlin Buch",
    plz: "13125"
  },
  {
    lati: 48.1158283343, longi: 7.91820823033,
    name: "Sexau",
    plz: "79350"
  },
  {
    lati: 48.5423916683, longi: 9.67239642659,
    name: "Hohenstadt/Drackenstein",
    plz: "73345"
  },
  {
    lati: 52.0644307924, longi: 9.70024517657,
    name: "Eime",
    plz: "31036"
  },
  {
    lati: 52.4703843613, longi: 13.3269688764,
    name: "Berlin Friedenau",
    plz: "12161"
  },
  {
    lati: 49.8645277247, longi: 6.9310371936,
    name: "Piesport",
    plz: "54498"
  },
  {
    lati: 48.4850429605, longi: 9.20985524273,
    name: "Reutlingen",
    plz: "72764"
  },
  {
    lati: 52.2591458212, longi: 9.37060577259,
    name: "Pohle, Lauenau, Messenkamp, Hülsede etc",
    plz: "31867"
  },
  {
    lati: 47.8280242584, longi: 9.57698005288,
    name: "Berg",
    plz: "88276"
  },
  {
    lati: 48.1986061781, longi: 9.0271089656,
    name: "Albstadt",
    plz: "72458"
  },
  {
    lati: 49.0217479311, longi: 9.20810107462,
    name: "Besigheim",
    plz: "74354"
  },
  {
    lati: 52.9488795574, longi: 8.04168175875,
    name: "Garrel",
    plz: "49681"
  },
  {
    lati: 52.1984878175, longi: 8.83100646466,
    name: "Bad Oeynhausen",
    plz: "32547"
  },
  {
    lati: 54.6444790315, longi: 8.92120364509,
    name: "Bordelum",
    plz: "25852"
  },
  {
    lati: 51.0959098769, longi: 9.09435828028,
    name: "Bad Wildungen",
    plz: "34537"
  },
  {
    lati: 51.4304624634, longi: 9.67862030896,
    name: "Hann. Münden, Gutsbezirk Reinhardswald",
    plz: "34346"
  },
  {
    lati: 48.3224827513, longi: 10.0689236952,
    name: "Senden",
    plz: "89250"
  },
  {
    lati: 49.5563053007, longi: 11.6877037961,
    name: "Edelsfeld",
    plz: "92265"
  },
  {
    lati: 50.0396068726, longi: 11.7187657613,
    name: "Bad Berneck im Fichtelgebirge",
    plz: "95460"
  },
  {
    lati: 49.3832739326, longi: 11.9954654083,
    name: "Ebermannsdorf",
    plz: "92263"
  },
  {
    lati: 50.3905190504, longi: 12.331230194,
    name: "Schöneck/Vogtl.",
    plz: "08261"
  },
  {
    lati: 53.6057028141, longi: 10.8145086554,
    name: "Sterley",
    plz: "23883"
  },
  {
    lati: 54.4652567366, longi: 9.05804954435,
    name: "Husum, Schwesing u.a.",
    plz: "25813"
  },
  {
    lati: 53.0464005768, longi: 8.89250609644,
    name: "Bremen",
    plz: "28309"
  },
  {
    lati: 50.613713808, longi: 6.32948178998,
    name: "Simmerath",
    plz: "52152"
  },
  {
    lati: 51.3405827904, longi: 8.40101229856,
    name: "Bestwig",
    plz: "59909"
  },
  {
    lati: 48.3235993994, longi: 8.66315880158,
    name: "Vöhringen",
    plz: "72189"
  },
  {
    lati: 49.729894265, longi: 6.67429023268,
    name: "Trier",
    plz: "54296"
  },
  {
    lati: 48.2933292789, longi: 12.7794018467,
    name: "Erlbach",
    plz: "84567"
  },
  {
    lati: 48.1359642378, longi: 11.5729048503,
    name: "München",
    plz: "80331"
  },
  {
    lati: 51.1309518851, longi: 13.4444888581,
    name: "Käbschütztal, Klipphausen, Diera-Zehren",
    plz: "01665"
  },
  {
    lati: 49.6613782498, longi: 10.0544825688,
    name: "Ochsenfurt",
    plz: "97199"
  },
  {
    lati: 48.4039732834, longi: 10.0000130638,
    name: "Ulm",
    plz: "89073"
  },
  {
    lati: 49.2969786042, longi: 9.28401619129,
    name: "Neudenau",
    plz: "74861"
  },
  {
    lati: 49.8144930298, longi: 7.7109583384,
    name: "Waldböckelheim",
    plz: "55596"
  },
  {
    lati: 49.3578303707, longi: 8.7755754154,
    name: "Bammental",
    plz: "69245"
  },
  {
    lati: 53.0725676309, longi: 9.04975267536,
    name: "Oyten",
    plz: "28876"
  },
  {
    lati: 47.7877127459, longi: 9.88125203926,
    name: "Kißlegg",
    plz: "88353"
  },
  {
    lati: 53.3862955262, longi: 14.1582483049,
    name: "Brüssow",
    plz: "17326"
  },
  {
    lati: 50.541105856, longi: 11.0495001196,
    name: "Katzhütte",
    plz: "98746"
  },
  {
    lati: 50.9972843117, longi: 11.0148021792,
    name: "Erfurt",
    plz: "99089"
  },
  {
    lati: 48.8189599469, longi: 11.0996587494,
    name: "Wellheim",
    plz: "91809"
  },
  {
    lati: 52.9127260172, longi: 11.3224841093,
    name: "Lemgow",
    plz: "29485"
  },
  {
    lati: 52.4810258871, longi: 12.0445718962,
    name: "Jerichow",
    plz: "39319"
  },
  {
    lati: 49.5030916939, longi: 12.6250124371,
    name: "Stadlern",
    plz: "92549"
  },
  {
    lati: 51.0937902262, longi: 9.35603517451,
    name: "Wabern",
    plz: "34590"
  },
  {
    lati: 52.7397739509, longi: 8.83994953998,
    name: "Schwaförden",
    plz: "27252"
  },
  {
    lati: 53.1662493287, longi: 8.47681420819,
    name: "Berne",
    plz: "27804"
  },
  {
    lati: 49.6105924059, longi: 7.66542428424,
    name: "Rothselberg u.a.",
    plz: "67753"
  },
  {
    lati: 51.7844521355, longi: 9.69035253233,
    name: "Dassel",
    plz: "37586"
  },
  {
    lati: 48.5089520465, longi: 10.7858379473,
    name: "Biberbach",
    plz: "86485"
  },
  {
    lati: 49.3206292664, longi: 10.7235416207,
    name: "Petersaurach",
    plz: "91580"
  },
  {
    lati: 50.5622188948, longi: 10.8627867662,
    name: "Schmiedefeld, Frauenwald, Suhl",
    plz: "98711"
  },
  {
    lati: 50.5399916049, longi: 11.2185978273,
    name: "Lichte",
    plz: "98739"
  },
  {
    lati: 53.0923259098, longi: 11.4948866797,
    name: "Lenzen",
    plz: "19309"
  },
  {
    lati: 53.6502913076, longi: 11.3490500266,
    name: "Schwerin",
    plz: "19057"
  },
  {
    lati: 50.0006411616, longi: 8.27215979556,
    name: "Mainz",
    plz: "55116"
  },
  {
    lati: 48.7965981997, longi: 8.44176586773,
    name: "Bad Herrenalb",
    plz: "76332"
  },
  {
    lati: 49.9845394011, longi: 8.56993774681,
    name: "Mörfelden-Walldorf",
    plz: "64546"
  },
  {
    lati: 49.8098110893, longi: 6.60402463056,
    name: "Newel",
    plz: "54309"
  },
  {
    lati: 51.4491398697, longi: 6.85378263778,
    name: "Mülheim an der Ruhr",
    plz: "45476"
  },
  {
    lati: 51.2826739622, longi: 7.22210003223,
    name: "Wuppertal",
    plz: "42277"
  },
  {
    lati: 52.2635462811, longi: 9.1469880074,
    name: "Obernkirchen",
    plz: "31683"
  },
  {
    lati: 50.9223465675, longi: 9.1987801194,
    name: "Schwalmstadt",
    plz: "34613"
  },
  {
    lati: 48.77358366, longi: 9.21110093378,
    name: "Stuttgart",
    plz: "70186"
  },
  {
    lati: 50.0684567954, longi: 10.0984755469,
    name: "Euerbach",
    plz: "97502"
  },
  {
    lati: 48.1629157525, longi: 11.570052315,
    name: "München",
    plz: "80796"
  },
  {
    lati: 47.9594658507, longi: 11.6356059909,
    name: "Sauerlach",
    plz: "82054"
  },
  {
    lati: 48.1335685838, longi: 7.61446619944,
    name: "Sasbach am Kaiserstuhl",
    plz: "79361"
  },
  {
    lati: 53.7686313733, longi: 9.30969694218,
    name: "Wischhafen",
    plz: "21737"
  },
  {
    lati: 50.2243970757, longi: 9.04747435245,
    name: "Ronneburg",
    plz: "63549"
  },
  {
    lati: 50.7228729871, longi: 12.4467047266,
    name: "Zwickau",
    plz: "08060"
  },
  {
    lati: 47.8698503493, longi: 12.702230436,
    name: "Surberg",
    plz: "83362"
  },
  {
    lati: 53.574140345, longi: 10.2140534244,
    name: "Barsbüttel",
    plz: "22885"
  },
  {
    lati: 53.4826594119, longi: 10.3295035073,
    name: "Kröppelshagen-Fahrendorf",
    plz: "21529"
  },
  {
    lati: 49.2294561237, longi: 7.03761807097,
    name: "Saarbrücken",
    plz: "66121"
  },
  {
    lati: 49.747266682, longi: 6.65587163874,
    name: "Trier",
    plz: "54295"
  },
  {
    lati: 50.7932008406, longi: 7.57257900393,
    name: "Windeck",
    plz: "51570"
  },
  {
    lati: 48.4362103235, longi: 8.51503955674,
    name: "Glatten",
    plz: "72293"
  },
  {
    lati: 48.6575329468, longi: 12.3118158543,
    name: "Postau",
    plz: "84103"
  },
  {
    lati: 52.4030656326, longi: 13.0671698402,
    name: "Potsdam",
    plz: "14467"
  },
  {
    lati: 49.6523426901, longi: 10.9757161616,
    name: "Möhrendorf/Mark",
    plz: "91096"
  },
  {
    lati: 48.5927756443, longi: 13.4396107882,
    name: "Passau",
    plz: "94034"
  },
  {
    lati: 51.5358733102, longi: 11.3574810264,
    name: "Allstedt",
    plz: "06542"
  },
  {
    lati: 49.6582628206, longi: 11.0297066382,
    name: "Baiersdorf",
    plz: "91083"
  },
  {
    lati: 54.7437906843, longi: 9.89778660753,
    name: "Gelting",
    plz: "24395"
  },
  {
    lati: 47.9194172696, longi: 12.230620061,
    name: "Söchtenau",
    plz: "83139"
  },
  {
    lati: 51.3058136214, longi: 12.274678861,
    name: "Leipzig",
    plz: "04207"
  },
  {
    lati: 50.7797530188, longi: 6.12591559572,
    name: "Aachen",
    plz: "52068"
  },
  {
    lati: 49.9766171148, longi: 6.64077134288,
    name: "Dudeldorf",
    plz: "54647"
  },
  {
    lati: 51.5134506123, longi: 7.0591685994,
    name: "Gelsenkirchen",
    plz: "45883"
  },
  {
    lati: 53.260790925, longi: 8.21105937127,
    name: "Rastede",
    plz: "26180"
  },
  {
    lati: 50.6855072049, longi: 8.18896634146,
    name: "Breitscheid",
    plz: "35767"
  },
  {
    lati: 52.6649931537, longi: 8.9608330972,
    name: "Borstel",
    plz: "27246"
  },
  {
    lati: 48.264666555, longi: 9.0184990662,
    name: "Albstadt",
    plz: "72461"
  },
  {
    lati: 54.063301067, longi: 9.56583536678,
    name: "Reher",
    plz: "25593"
  },
  {
    lati: 48.2380890168, longi: 8.43218306219,
    name: "Schramberg",
    plz: "78713"
  },
  {
    lati: 54.3539061066, longi: 9.72693472729,
    name: "Borgstedt",
    plz: "24794"
  },
  {
    lati: 48.9072933573, longi: 9.31377946563,
    name: "Marbach am Neckar",
    plz: "71672"
  },
  {
    lati: 49.9618224464, longi: 9.21001741267,
    name: "Haibach",
    plz: "63808"
  },
  {
    lati: 48.7574311143, longi: 12.8165195871,
    name: "Otzing",
    plz: "94563"
  },
  {
    lati: 48.4694013777, longi: 12.2670634332,
    name: "Geisenhausen",
    plz: "84144"
  },
  {
    lati: 52.5017059694, longi: 13.3377325468,
    name: "Berlin Schöneberg",
    plz: "10789"
  },
  {
    lati: 51.0317842705, longi: 14.543207348,
    name: "Neusalza-Spremberg",
    plz: "02742"
  },
  {
    lati: 52.0628688136, longi: 11.6525100697,
    name: "Magdeburg",
    plz: "39122"
  },
  {
    lati: 51.5699817026, longi: 7.54014962202,
    name: "Dortmund",
    plz: "44329"
  },
  {
    lati: 51.1418049366, longi: 7.57971519497,
    name: "Kierspe",
    plz: "58566"
  },
  {
    lati: 50.5208187112, longi: 7.75633939374,
    name: "Selters (Westerwald)",
    plz: "56242"
  },
  {
    lati: 53.152312303, longi: 8.23247990718,
    name: "Oldenburg (Oldenburg)",
    plz: "26123"
  },
  {
    lati: 50.3307104348, longi: 8.39185392592,
    name: "Weilrod",
    plz: "61276"
  },
  {
    lati: 50.5918241967, longi: 11.8148679581,
    name: "Schleiz",
    plz: "07907"
  },
  {
    lati: 51.5269548676, longi: 7.07988804438,
    name: "Gelsenkirchen",
    plz: "45881"
  },
  {
    lati: 50.2138448997, longi: 6.21031316919,
    name: "Winterspelt",
    plz: "54616"
  },
  {
    lati: 49.5509009543, longi: 6.80910541919,
    name: "Weiskirchen",
    plz: "66709"
  },
  {
    lati: 51.3477570758, longi: 12.4499799499,
    name: "Leipzig",
    plz: "04328"
  },
  {
    lati: 49.785862355, longi: 9.329356965,
    name: "Collenberg",
    plz: "97903"
  },
  {
    lati: 47.6689371304, longi: 9.14230820269,
    name: "Reichenau",
    plz: "78479"
  },
  {
    lati: 53.5986031466, longi: 9.87772793534,
    name: "Hamburg",
    plz: "22547"
  },
  {
    lati: 53.302369943, longi: 10.1367478537,
    name: "Wulfsen",
    plz: "21445"
  },
  {
    lati: 53.3061466866, longi: 10.2351512917,
    name: "Winsen",
    plz: "21423"
  },
  {
    lati: 48.825487052, longi: 9.81031199193,
    name: "Schwäbisch Gmünd, Täferrot",
    plz: "73527"
  },
  {
    lati: 49.8821783427, longi: 6.68220711162,
    name: "Zemmer",
    plz: "54313"
  },
  {
    lati: 51.2543293541, longi: 6.88590831321,
    name: "Düsseldorf",
    plz: "40629"
  },
  {
    lati: 50.9679299911, longi: 6.89928087858,
    name: "Köln",
    plz: "50827"
  },
  {
    lati: 49.3607155145, longi: 10.3101797247,
    name: "Geslau",
    plz: "91608"
  },
  {
    lati: 53.9303889179, longi: 10.6177100319,
    name: "Stockelsdorf",
    plz: "23617"
  },
  {
    lati: 51.3123376461, longi: 7.61756235668,
    name: "Nachrodt-Wiblingwerde",
    plz: "58769"
  },
  {
    lati: 49.2107409713, longi: 7.63286767063,
    name: "Pirmasens",
    plz: "66953"
  },
  {
    lati: 49.5886376612, longi: 7.89161596485,
    name: "Imsbach",
    plz: "67817"
  },
  {
    lati: 49.5228989818, longi: 7.9151177085,
    name: "Neuhemsbach",
    plz: "67680"
  },
  {
    lati: 49.8905162393, longi: 12.101816063,
    name: "Friedenfels",
    plz: "95688"
  },
  {
    lati: 52.7776114569, longi: 12.781530136,
    name: "Fehrbellin",
    plz: "16833"
  },
  {
    lati: 48.1435249232, longi: 12.2998586057,
    name: "Gars a. Inn",
    plz: "83555"
  },
  {
    lati: 48.1615810905, longi: 11.064262451,
    name: "Moorenweis",
    plz: "82272"
  },
  {
    lati: 48.1099654474, longi: 11.5639899691,
    name: "München",
    plz: "81543"
  },
  {
    lati: 51.8049618509, longi: 11.6666861827,
    name: "Ilberstedt",
    plz: "06408"
  },
  {
    lati: 52.4905929229, longi: 12.6418569768,
    name: "Beetzsee, Wollin, Wenzlow, Golzow u.a",
    plz: "14778"
  },
  {
    lati: 51.0733914272, longi: 12.9115600387,
    name: "Geringswalde",
    plz: "09326"
  },
  {
    lati: 51.5480401832, longi: 7.33687146526,
    name: "Castrop-Rauxel",
    plz: "44577"
  },
  {
    lati: 50.1338045727, longi: 9.13676290372,
    name: "Freigericht",
    plz: "63579"
  },
  {
    lati: 51.4372549848, longi: 9.2049666347,
    name: "Breuna",
    plz: "34479"
  },
  {
    lati: 51.0180135284, longi: 8.60945856215,
    name: "Battenberg",
    plz: "35088"
  },
  {
    lati: 48.6972027076, longi: 8.66618980778,
    name: "Bad Teinach-Zavelstein",
    plz: "75385"
  },
  {
    lati: 50.4704004733, longi: 8.90708944506,
    name: "Hungen",
    plz: "35410"
  },
  {
    lati: 48.8584224191, longi: 8.86820213869,
    name: "Mönsheim",
    plz: "71297"
  },
  {
    lati: 51.3521342701, longi: 9.77369448517,
    name: "Witzenhausen",
    plz: "37217"
  },
  {
    lati: 49.0266470171, longi: 9.89513136566,
    name: "Bühlerzell",
    plz: "74426"
  },
  {
    lati: 54.130615072, longi: 10.0684379354,
    name: "Großharrie",
    plz: "24625"
  },
  {
    lati: 47.9628230601, longi: 7.95065057963,
    name: "Kirchzarten",
    plz: "79199"
  },
  {
    lati: 48.6111600008, longi: 8.216226536,
    name: "Sasbach",
    plz: "77880"
  },
  {
    lati: 50.3659513899, longi: 9.85580502677,
    name: "Wildflecken",
    plz: "97772"
  },
  {
    lati: 48.6415472487, longi: 13.2636045274,
    name: "Windorf",
    plz: "94575"
  },
  {
    lati: 54.2041497586, longi: 13.7708920487,
    name: "Kröslin, Krummin, Lassan u.a.",
    plz: "17440"
  },
  {
    lati: 50.3286840376, longi: 7.09767946584,
    name: "Ettringen",
    plz: "56729"
  },
  {
    lati: 49.8867235382, longi: 7.47090196796,
    name: "Gemünden",
    plz: "55490"
  },
  {
    lati: 51.8992478111, longi: 7.65455846738,
    name: "Münster",
    plz: "48165"
  },
  {
    lati: 50.3861922203, longi: 7.82395442871,
    name: "Nentershausen, Hübingen, Niederelbert u.a.",
    plz: "56412"
  },
  {
    lati: 50.0593378589, longi: 8.23533624645,
    name: "Wiesbaden",
    plz: "65187"
  },
  {
    lati: 48.0580008132, longi: 8.35629413454,
    name: "Unterkirnach",
    plz: "78089"
  },
  {
    lati: 49.1265212202, longi: 8.34224480555,
    name: "Leimersheim",
    plz: "76774"
  },
  {
    lati: 49.6230756326, longi: 12.0948387113,
    name: "Etzenricht",
    plz: "92694"
  },
  {
    lati: 50.0325262637, longi: 7.93927235738,
    name: "Geisenheim",
    plz: "65366"
  },
  {
    lati: 51.4567953265, longi: 7.98192171391,
    name: "Arnsberg",
    plz: "59755"
  },
  {
    lati: 53.4946483763, longi: 8.4529433655,
    name: "Nordenham",
    plz: "26954"
  },
  {
    lati: 47.9299981393, longi: 7.61882521358,
    name: "Hartheim",
    plz: "79258"
  },
  {
    lati: 50.2058737305, longi: 6.81370629341,
    name: "Daun",
    plz: "54550"
  },
  {
    lati: 49.9983688065, longi: 7.30258748761,
    name: "Peterswald-Löffelscheid u.a.",
    plz: "56858"
  },
  {
    lati: 51.5713497331, longi: 7.39764846103,
    name: "Dortmund",
    plz: "44359"
  },
  {
    lati: 48.2279867548, longi: 10.110284933,
    name: "Illertissen",
    plz: "89257"
  },
  {
    lati: 48.5194228898, longi: 8.98773403105,
    name: "Rottenburg am Neckar",
    plz: "72108"
  },
  {
    lati: 52.0564342953, longi: 9.24461694213,
    name: "Aerzen",
    plz: "31855"
  },
  {
    lati: 51.2461009009, longi: 9.68427866598,
    name: "Helsa",
    plz: "34298"
  },
  {
    lati: 48.6091079178, longi: 9.54753204231,
    name: "Weilheim an der Teck",
    plz: "73235"
  },
  {
    lati: 52.5275482475, longi: 13.3057410002,
    name: "Berlin Charlottenburg",
    plz: "10589"
  },
  {
    lati: 52.4992430003, longi: 13.5183970959,
    name: "Berlin",
    plz: "10319"
  },
  {
    lati: 50.2882445162, longi: 8.95392757987,
    name: "Altenstadt",
    plz: "63674"
  },
  {
    lati: 53.8060099297, longi: 8.9981943327,
    name: "Neuhaus (Oste)",
    plz: "21785"
  },
  {
    lati: 47.7316415407, longi: 9.03711533708,
    name: "Reichenau",
    plz: "78479"
  },
  {
    lati: 49.2265726788, longi: 9.14827960617,
    name: "Bad Wimpfen",
    plz: "74206"
  },
  {
    lati: 47.7289441815, longi: 12.069904047,
    name: "Brannenburg",
    plz: "83098"
  },
  {
    lati: 49.5472506096, longi: 11.8726102152,
    name: "Gebenbach",
    plz: "92274"
  },
  {
    lati: 51.6155737773, longi: 11.6352466899,
    name: "Gerbstedt",
    plz: "06347"
  },
  {
    lati: 51.8889842824, longi: 11.042004534,
    name: "Halberstadt",
    plz: "38820"
  },
  {
    lati: 52.4702750518, longi: 9.69621290445,
    name: "Langenhagen",
    plz: "30855"
  },
  {
    lati: 53.1144358881, longi: 9.36834935765,
    name: "Rotenburg",
    plz: "27356"
  },
  {
    lati: 53.7039104146, longi: 10.1251102517,
    name: "Hamburg",
    plz: "22397"
  },
  {
    lati: 49.9940116955, longi: 10.2081559349,
    name: "Grafenrheinfeld",
    plz: "97506"
  },
  {
    lati: 50.5874368004, longi: 8.67722362258,
    name: "Gießen",
    plz: "35390"
  },
  {
    lati: 48.9076886795, longi: 8.7989050428,
    name: "Niefern-Öschelbronn",
    plz: "75223"
  },
  {
    lati: 48.0354613476, longi: 7.89020791725,
    name: "Gundelfingen, Heuweiler",
    plz: "79194"
  },
  {
    lati: 51.5040709735, longi: 7.49978455906,
    name: "Dortmund",
    plz: "44141"
  },
  {
    lati: 48.8566032954, longi: 8.66306179384,
    name: "Pforzheim",
    plz: "75180"
  },
  {
    lati: 49.9071773944, longi: 9.12143256557,
    name: "Niedernberg",
    plz: "63843"
  },
  {
    lati: 51.9621153474, longi: 9.50808863725,
    name: "Bodenwerder, Pegestorf, Kirchbrak, Hehlen",
    plz: "37619"
  },
  {
    lati: 50.1789061681, longi: 7.86913618092,
    name: "Nastätten u.a.",
    plz: "56355"
  },
  {
    lati: 49.2291386874, longi: 7.53781832181,
    name: "Höheinöd, Petersberg u.a.",
    plz: "66989"
  },
  {
    lati: 50.6387114033, longi: 10.9981593721,
    name: "Gehren",
    plz: "98708"
  },
  {
    lati: 50.2465800325, longi: 12.058357084,
    name: "Rehau",
    plz: "95111"
  },
  {
    lati: 50.0353936152, longi: 7.02059684417,
    name: "Bausendorf",
    plz: "54538"
  },
  {
    lati: 50.6137893571, longi: 8.44368735941,
    name: "Aßlar",
    plz: "35614"
  },
  {
    lati: 48.4321553632, longi: 9.07436523917,
    name: "Nehren",
    plz: "72147"
  },
  {
    lati: 47.709399407, longi: 9.69032250667,
    name: "Bodnegg",
    plz: "88285"
  },
  {
    lati: 49.8274138305, longi: 11.1709120426,
    name: "Unterleinleiter",
    plz: "91364"
  },
  {
    lati: 49.0347191342, longi: 12.6306119293,
    name: "Rattiszell",
    plz: "94372"
  },
  {
    lati: 48.125918916, longi: 10.6595914975,
    name: "Ettringen",
    plz: "86833"
  },
  {
    lati: 48.9574993752, longi: 10.8700790173,
    name: "Treuchtlingen",
    plz: "91757"
  },
  {
    lati: 51.0126158279, longi: 12.0286257828,
    name: "Droyßig, Wetterzeube",
    plz: "06722"
  },
  {
    lati: 49.987523097, longi: 11.3127228045,
    name: "Wonsees",
    plz: "96197"
  },
  {
    lati: 49.0453348621, longi: 11.764114755,
    name: "Hemau",
    plz: "93155"
  },
  {
    lati: 52.1509552058, longi: 11.7636269476,
    name: "Gerwisch, Biederitz, Menz, Körbelitz etc",
    plz: "39175"
  },
  {
    lati: 48.6997186615, longi: 11.821421616,
    name: "Elsendorf",
    plz: "84094"
  },
  {
    lati: 48.306388237, longi: 11.9141856401,
    name: "Erding",
    plz: "85435"
  },
  {
    lati: 49.9997834649, longi: 11.7815764905,
    name: "Warmensteinach",
    plz: "95485"
  },
  {
    lati: 53.7871741908, longi: 7.89463170212,
    name: "Wangerooge",
    plz: "26486"
  },
  {
    lati: 50.4878397677, longi: 8.48102196273,
    name: "Schöffengrund",
    plz: "35641"
  },
  {
    lati: 49.5578735006, longi: 8.56488523823,
    name: "Viernheim",
    plz: "68519"
  },
  {
    lati: 52.0792718645, longi: 9.80065494617,
    name: "Gronau (Leine)",
    plz: "31028"
  },
  {
    lati: 54.391708584, longi: 10.0997639501,
    name: "Altenholz",
    plz: "24161"
  },
  {
    lati: 51.6356502988, longi: 10.1852796223,
    name: "Hattorf",
    plz: "37197"
  },
  {
    lati: 51.7001542179, longi: 10.4776969856,
    name: "Herzberg, Elbingerode, Hörden",
    plz: "37412"
  },
  {
    lati: 52.4837740158, longi: 13.354234049,
    name: "Berlin",
    plz: "10827"
  },
  {
    lati: 48.2848984011, longi: 9.94386399862,
    name: "Hüttisheim",
    plz: "89185"
  },
  {
    lati: 48.6636692435, longi: 11.9427079587,
    name: "Pfeffenhausen",
    plz: "84076"
  },
  {
    lati: 50.6224994093, longi: 12.264138563,
    name: "Mylau",
    plz: "08499"
  },
  {
    lati: 51.3298968494, longi: 12.369859134,
    name: "Leipzig",
    plz: "04107"
  },
  {
    lati: 53.5621146969, longi: 10.4152820498,
    name: "Hamfelde, Kasseburg, Köthel, Rausdorf, Schönberg",
    plz: "22929"
  },
  {
    lati: 48.6515062056, longi: 10.7918146705,
    name: "Mertingen",
    plz: "86690"
  },
  {
    lati: 49.9698199695, longi: 10.9144894029,
    name: "Breitengüßbach",
    plz: "96149"
  },
  {
    lati: 47.737998184, longi: 11.1570820617,
    name: "Eglfing",
    plz: "82436"
  },
  {
    lati: 52.6305738464, longi: 13.2011299576,
    name: "Hennigsdorf",
    plz: "16761"
  },
  {
    lati: 48.8203349905, longi: 13.5511459922,
    name: "Freyung",
    plz: "94078"
  },
  {
    lati: 51.5583855922, longi: 7.04369211134,
    name: "Gelsenkirchen",
    plz: "45897"
  },
  {
    lati: 50.9179633642, longi: 6.18527617476,
    name: "Baesweiler",
    plz: "52499"
  },
  {
    lati: 47.6575423745, longi: 7.62627945083,
    name: "Schallbach",
    plz: "79597"
  },
  {
    lati: 52.7628283425, longi: 9.11705499626,
    name: "Schweringen, Warpe, Bücken",
    plz: "27333"
  },
  {
    lati: 48.6937610556, longi: 9.32098838833,
    name: "Denkendorf",
    plz: "73770"
  },
  {
    lati: 54.4214507597, longi: 12.7033313332,
    name: "Zingst a. Darß",
    plz: "18374"
  },
  {
    lati: 48.5780034515, longi: 13.075101429,
    name: "Aldersbach",
    plz: "94501"
  },
  {
    lati: 51.2492594408, longi: 12.4742158229,
    name: "Großpösna",
    plz: "04463"
  },
  {
    lati: 50.6190297572, longi: 13.0098535311,
    name: "Wiesa",
    plz: "09488"
  },
  {
    lati: 50.6766986164, longi: 12.1258540792,
    name: "Berga/Elster",
    plz: "07980"
  },
  {
    lati: 52.5215231099, longi: 13.3355165338,
    name: "Berlin Moabit",
    plz: "10555"
  },
  {
    lati: 51.0921264161, longi: 13.9833619691,
    name: "Arnsdorf b. Dresden",
    plz: "01477"
  },
  {
    lati: 52.9537499946, longi: 11.2447536774,
    name: "Woltersdorf",
    plz: "29497"
  },
  {
    lati: 48.073764229, longi: 11.262608311,
    name: "Weßling",
    plz: "82234"
  },
  {
    lati: 49.2611010459, longi: 8.06987572542,
    name: "Rhodt u.a.",
    plz: "76835"
  },
  {
    lati: 53.9906795614, longi: 9.09126089138,
    name: "Sankt Michaelisdonn,Gudendorf,Volsemenhusen,Trennewurth",
    plz: "25693"
  },
  {
    lati: 49.2860784141, longi: 10.6542924608,
    name: "Sachsen b. Ansbach",
    plz: "91623"
  },
  {
    lati: 49.347358524, longi: 10.800261009,
    name: "Heilsbronn",
    plz: "91560"
  },
  {
    lati: 48.8692587006, longi: 11.4708432981,
    name: "Stammham",
    plz: "85134"
  },
  {
    lati: 49.555348802, longi: 7.46585596565,
    name: "Altenglan",
    plz: "66885"
  },
  {
    lati: 50.7987741433, longi: 6.3643683641,
    name: "Langerwehe",
    plz: "52379"
  },
  {
    lati: 49.5357110945, longi: 9.02047750339,
    name: "Sensbachtal",
    plz: "64759"
  },
  {
    lati: 49.4038327577, longi: 8.67920081636,
    name: "Heidelberg",
    plz: "69115"
  },
  {
    lati: 49.3387308105, longi: 11.3289693772,
    name: "Burgthann",
    plz: "90559"
  },
  {
    lati: 48.1627913899, longi: 11.3464196134,
    name: "Puchheim",
    plz: "82178"
  },
  {
    lati: 50.1190143147, longi: 12.1089081865,
    name: "Thierstein",
    plz: "95199"
  },
  {
    lati: 54.0119322732, longi: 12.230057988,
    name: "Dummerstorf",
    plz: "18196"
  },
  {
    lati: 48.1204341448, longi: 11.5874447961,
    name: "München",
    plz: "81541"
  },
  {
    lati: 48.2665922764, longi: 11.0028727714,
    name: "Mering",
    plz: "86415"
  },
  {
    lati: 47.6959783921, longi: 11.0048094827,
    name: "Bayersoien",
    plz: "82435"
  },
  {
    lati: 49.464477453, longi: 10.7192205694,
    name: "Wilhermsdorf",
    plz: "91452"
  },
  {
    lati: 49.0069138476, longi: 10.836558946,
    name: "Markt Berolzheim",
    plz: "91801"
  },
  {
    lati: 51.6666867563, longi: 11.1426137547,
    name: "Ballenstedt, Harzgerode",
    plz: "06493"
  },
  {
    lati: 49.7755009638, longi: 10.4784899001,
    name: "Geiselwind",
    plz: "96160"
  },
  {
    lati: 51.4726503064, longi: 6.83482672589,
    name: "Oberhausen",
    plz: "46049"
  },
  {
    lati: 51.5959414365, longi: 7.76984970157,
    name: "Bönen",
    plz: "59199"
  },
  {
    lati: 50.707116825, longi: 7.79391655121,
    name: "Malberg, Norken, Höchstenbach u.a.",
    plz: "57629"
  },
  {
    lati: 49.4494829957, longi: 8.22077499426,
    name: "Friedelsheim",
    plz: "67159"
  },
  {
    lati: 50.4238783137, longi: 8.37042232836,
    name: "Weilmünster",
    plz: "35789"
  },
  {
    lati: 49.1812246294, longi: 9.03370846238,
    name: "Massenbachhausen",
    plz: "74252"
  },
  {
    lati: 49.1360590593, longi: 9.17526284548,
    name: "Heilbronn",
    plz: "74080"
  },
  {
    lati: 48.3765016234, longi: 12.0835107442,
    name: "Steinkirchen",
    plz: "84439"
  },
  {
    lati: 51.3003616401, longi: 9.8659518621,
    name: "Witzenhausen",
    plz: "37215"
  },
  {
    lati: 53.943994853, longi: 10.141241005,
    name: "Wahlstedt",
    plz: "23812"
  },
  {
    lati: 48.3468842197, longi: 10.4596994291,
    name: "Burtenbach",
    plz: "89349"
  },
  {
    lati: 53.3364941174, longi: 10.4486492643,
    name: "Brietlingen",
    plz: "21382"
  },
  {
    lati: 47.9783490105, longi: 12.5799213541,
    name: "St. Georgen",
    plz: "83368"
  },
  {
    lati: 49.874885153, longi: 11.1512097955,
    name: "Heiligenstadt i. OFr.",
    plz: "91332"
  },
  {
    lati: 52.3258333853, longi: 9.48072406464,
    name: "Barsinghausen",
    plz: "30890"
  },
  {
    lati: 48.565658827, longi: 9.17384779767,
    name: "Pliezhausen",
    plz: "72124"
  },
  {
    lati: 50.1961022842, longi: 8.1003769018,
    name: "Hohenstein",
    plz: "65329"
  },
  {
    lati: 54.1394856697, longi: 11.7454473819,
    name: "Kühlungsborn",
    plz: "18225"
  },
  {
    lati: 54.6805242793, longi: 9.72059609677,
    name: "Mohrkirch, Rügge",
    plz: "24405"
  },
  {
    lati: 54.0311830203, longi: 9.8961443478,
    name: "Padenstedt",
    plz: "24634"
  },
  {
    lati: 50.8852830558, longi: 12.0994445188,
    name: "Gera",
    plz: "07546"
  },
  {
    lati: 54.2670682354, longi: 12.8168260575,
    name: "Velgast",
    plz: "18469"
  },
  {
    lati: 53.1719378937, longi: 8.69179166999,
    name: "Bremen",
    plz: "28717"
  },
  {
    lati: 48.8351250691, longi: 8.82515213338,
    name: "Friolzheim",
    plz: "71292"
  },
  {
    lati: 51.4913521545, longi: 6.97131028298,
    name: "Essen",
    plz: "45356"
  },
  {
    lati: 51.6168044998, longi: 7.18548212934,
    name: "Recklinghausen",
    plz: "45659"
  },
  {
    lati: 51.0757086662, longi: 6.79894879217,
    name: "Dormagen",
    plz: "41540"
  },
  {
    lati: 49.0971680292, longi: 9.38999806215,
    name: "Löwenstein",
    plz: "74245"
  },
  {
    lati: 52.7568512447, longi: 10.282442403,
    name: "Eschede",
    plz: "29348"
  },
  {
    lati: 48.7518496773, longi: 9.66200441026,
    name: "Birenbach",
    plz: "73102"
  },
  {
    lati: 53.6767636554, longi: 10.4326490825,
    name: "Hamfelde, Kasseburg, Köthel, Rausdorf, Schönberg",
    plz: "22929"
  },
  {
    lati: 49.2258723501, longi: 11.8513871724,
    name: "Hohenfels",
    plz: "92366"
  },
  {
    lati: 52.0232796985, longi: 8.61855153727,
    name: "Bielefeld",
    plz: "33719"
  },
  {
    lati: 49.4249143162, longi: 7.89540083794,
    name: "Hochspeyer",
    plz: "67691"
  },
  {
    lati: 53.7464161154, longi: 9.71067744709,
    name: "Elmshorn",
    plz: "25337"
  },
  {
    lati: 52.4373470231, longi: 9.39274083127,
    name: "Wunstorf",
    plz: "31515"
  },
  {
    lati: 47.8125963613, longi: 10.0187349825,
    name: "Leutkirch im Allgäu",
    plz: "88299"
  },
  {
    lati: 47.5984200275, longi: 10.1113022223,
    name: "Missen-Wilhams",
    plz: "87547"
  },
  {
    lati: 49.4770471271, longi: 11.0651739976,
    name: "Nürnberg",
    plz: "90425"
  },
  {
    lati: 53.0776657673, longi: 11.1977716268,
    name: "Gusborn",
    plz: "29476"
  },
  {
    lati: 52.4713431729, longi: 13.448592434,
    name: "Berlin Neukölln",
    plz: "12055"
  },
  {
    lati: 49.2636057396, longi: 8.15916011069,
    name: "Edesheim",
    plz: "67483"
  },
  {
    lati: 49.4111688439, longi: 7.19388967159,
    name: "Ottweiler",
    plz: "66564"
  },
  {
    lati: 52.610048373, longi: 7.24154965287,
    name: "Geeste",
    plz: "49744"
  },
  {
    lati: 53.5145770004, longi: 7.25810551613,
    name: "Upgant-Schott, Osteel u.a.",
    plz: "26529"
  },
  {
    lati: 51.6601700858, longi: 8.34830886764,
    name: "Lippstadt",
    plz: "59557"
  },
  {
    lati: 48.8445735196, longi: 8.52379372107,
    name: "Straubenhardt",
    plz: "75334"
  },
  {
    lati: 50.5090635765, longi: 8.5639239583,
    name: "Hüttenberg",
    plz: "35625"
  },
  {
    lati: 49.1720804302, longi: 8.54482386155,
    name: "Bruchsal",
    plz: "76646"
  },
  {
    lati: 48.3614488024, longi: 10.865473443,
    name: "Augsburg",
    plz: "86157"
  },
  {
    lati: 50.0059451634, longi: 12.223888143,
    name: "Konnersreuth",
    plz: "95692"
  },
  {
    lati: 51.2185849811, longi: 12.3080119994,
    name: "Zwenkau",
    plz: "04442"
  },
  {
    lati: 53.5406296481, longi: 10.2129212073,
    name: "Glinde",
    plz: "21509"
  },
  {
    lati: 53.3964051478, longi: 11.5992673206,
    name: "Neustadt-Glewe",
    plz: "19306"
  },
  {
    lati: 50.9769268805, longi: 10.9447435601,
    name: "Erfurt",
    plz: "99092"
  },
  {
    lati: 53.5846108822, longi: 11.4134659224,
    name: "Schwerin",
    plz: "19061"
  },
  {
    lati: 52.4289039381, longi: 13.5885658321,
    name: "Berlin",
    plz: "12557"
  },
  {
    lati: 51.1116874111, longi: 13.8313714843,
    name: "Dresden",
    plz: "01465"
  },
  {
    lati: 48.3492958964, longi: 9.93098304692,
    name: "Ulm",
    plz: "89079"
  },
  {
    lati: 50.0652666488, longi: 9.33761879257,
    name: "Heinrichsthal",
    plz: "63871"
  },
  {
    lati: 49.0866456556, longi: 9.57062387516,
    name: "Mainhardt",
    plz: "74535"
  },
  {
    lati: 49.2919672037, longi: 7.57631674868,
    name: "Thaleischweiler-Fröschen",
    plz: "66987"
  },
  {
    lati: 53.5687317478, longi: 8.55980033058,
    name: "Bremerhaven, Bremen",
    plz: "27568"
  },
  {
    lati: 49.889828848, longi: 11.6831981588,
    name: "Emtmannsberg",
    plz: "95517"
  },
  {
    lati: 51.5328214621, longi: 11.7789587505,
    name: "Salzatal",
    plz: "06198"
  },
  {
    lati: 47.904317232, longi: 12.4167715972,
    name: "Gstadt a. Chiemsee",
    plz: "83257"
  },
  {
    lati: 48.7973459852, longi: 9.6777844373,
    name: "Lorch",
    plz: "73547"
  },
  {
    lati: 54.3260405298, longi: 9.67487624063,
    name: "Büdelsdorf, Rickert",
    plz: "24782"
  },
  {
    lati: 48.4642798973, longi: 9.77216808055,
    name: "Berghülen",
    plz: "89180"
  },
  {
    lati: 54.0655121831, longi: 10.2022742604,
    name: "Börnhöved",
    plz: "24619"
  },
  {
    lati: 50.8627241376, longi: 13.7578662768,
    name: "Glashütte",
    plz: "01768"
  },
  {
    lati: 51.3010731825, longi: 14.8821352736,
    name: "Hähnichen, Horka, Kodersdorf",
    plz: "02923"
  },
  {
    lati: 51.7305217288, longi: 13.9073747378,
    name: "Calau, Bronkow",
    plz: "03205"
  },
  {
    lati: 49.7229723022, longi: 8.59458327085,
    name: "Zwingenberg",
    plz: "64673"
  },
  {
    lati: 50.2807084454, longi: 9.97221121276,
    name: "Burkardroth",
    plz: "97705"
  },
  {
    lati: 50.3934909294, longi: 10.3305368998,
    name: "Oberstreu",
    plz: "97640"
  },
  {
    lati: 49.8701021227, longi: 10.3410770282,
    name: "Lülsfeld",
    plz: "97511"
  },
  {
    lati: 48.7108545405, longi: 9.05036332692,
    name: "Sindelfingen",
    plz: "71065"
  },
  {
    lati: 48.6675005474, longi: 10.9549294746,
    name: "Rain",
    plz: "86641"
  },
  {
    lati: 48.7917736059, longi: 9.4710403762,
    name: "Winterbach",
    plz: "73650"
  },
  {
    lati: 50.1147120752, longi: 8.72409896002,
    name: "Frankfurt am Main",
    plz: "60314"
  },
  {
    lati: 49.6555945797, longi: 8.79083537156,
    name: "Fürth",
    plz: "64658"
  },
  {
    lati: 49.8687813685, longi: 8.94395577996,
    name: "Groß-Umstadt",
    plz: "64823"
  },
  {
    lati: 49.8141869151, longi: 6.51728601127,
    name: "Ralingen",
    plz: "54310"
  },
  {
    lati: 51.3595889642, longi: 6.79168924397,
    name: "Duisburg",
    plz: "47269"
  },
  {
    lati: 50.0848917465, longi: 9.07981127709,
    name: "Alzenau",
    plz: "63755"
  },
  {
    lati: 50.7004722603, longi: 12.848508437,
    name: "Thalheim/Erzgebirge",
    plz: "09380"
  },
  {
    lati: 47.6830853984, longi: 13.0441954864,
    name: "Marktschellenberg",
    plz: "83487"
  },
  {
    lati: 48.3160233094, longi: 11.541885735,
    name: "Haimhausen",
    plz: "85778"
  },
  {
    lati: 52.1220166196, longi: 13.6040272009,
    name: "Teupitz",
    plz: "15755"
  },
  {
    lati: 52.4791807853, longi: 13.7057081775,
    name: "Schöneiche bei Berlin",
    plz: "15566"
  },
  {
    lati: 52.904332182, longi: 13.8897395306,
    name: "Melchow, Chorin u.a.",
    plz: "16230"
  },
  {
    lati: 49.2600786745, longi: 10.1663033556,
    name: "Wettringen",
    plz: "91631"
  },
  {
    lati: 50.0350362481, longi: 10.2087763931,
    name: "Schweinfurt",
    plz: "97424"
  },
  {
    lati: 52.4197832415, longi: 13.3132973793,
    name: "Berlin Lichtenfelde",
    plz: "12207"
  },
  {
    lati: 50.4488050405, longi: 6.42564938407,
    name: "Hellenthal",
    plz: "53940"
  },
  {
    lati: 51.2215103344, longi: 6.78938767077,
    name: "Düsseldorf",
    plz: "40210"
  },
  {
    lati: 50.3018036215, longi: 6.9012026263,
    name: "Bodenbach, Kelberg, Kirsbach u.a.",
    plz: "53539"
  },
  {
    lati: 52.6759118755, longi: 7.47079460638,
    name: "Haselünne",
    plz: "49740"
  },
  {
    lati: 49.1036942575, longi: 8.6002326141,
    name: "Bruchsal",
    plz: "76646"
  },
  {
    lati: 48.1100731241, longi: 8.74103673562,
    name: "Denkingen",
    plz: "78588"
  },
  {
    lati: 51.3513359427, longi: 9.85468550701,
    name: "Witzenhausen",
    plz: "37213"
  },
  {
    lati: 53.5013100457, longi: 9.97424133665,
    name: "Hamburg",
    plz: "21107"
  },
  {
    lati: 52.3703437059, longi: 13.0501683101,
    name: "Potsdam",
    plz: "14473"
  },
  {
    lati: 48.6110983023, longi: 9.26689460495,
    name: "Neckartailfingen",
    plz: "72666"
  },
  {
    lati: 49.8211545616, longi: 9.04214088921,
    name: "Breuberg",
    plz: "64747"
  },
  {
    lati: 49.2823310234, longi: 8.76441135783,
    name: "Dielheim",
    plz: "69234"
  },
  {
    lati: 51.46013826, longi: 7.04013330885,
    name: "Essen",
    plz: "45139"
  },
  {
    lati: 48.3992345182, longi: 8.44839791539,
    name: "Loßburg",
    plz: "72290"
  },
  {
    lati: 50.3613215049, longi: 7.64693074292,
    name: "Koblenz",
    plz: "56077"
  },
  {
    lati: 53.3103004579, longi: 9.63078549917,
    name: "Heidenau",
    plz: "21258"
  },
  {
    lati: 48.0607401451, longi: 10.1196244529,
    name: "Kirchdorf an der Iller",
    plz: "88457"
  },
  {
    lati: 52.0836863295, longi: 11.0780011798,
    name: "Hötensleben, Völpke, Ottleben, Hamersleben etc",
    plz: "39393"
  },
  {
    lati: 47.4637258975, longi: 11.2954861139,
    name: "Mittenwald",
    plz: "82481"
  },
  {
    lati: 54.086758735, longi: 13.415385814,
    name: "Greifswald",
    plz: "17489"
  },
  {
    lati: 50.7824713651, longi: 12.3938580761,
    name: "Neukirchen/Pleiße",
    plz: "08459"
  },
  {
    lati: 49.7511771302, longi: 11.1316272835,
    name: "Weilersbach",
    plz: "91365"
  },
  {
    lati: 49.8942966372, longi: 11.2411922196,
    name: "Aufseß",
    plz: "91347"
  },
  {
    lati: 50.5465833962, longi: 7.5139365722,
    name: "Straßenhaus",
    plz: "56587"
  },
  {
    lati: 51.4296611716, longi: 7.67457808466,
    name: "Iserlohn",
    plz: "58640"
  },
  {
    lati: 47.651889832, longi: 8.24289787897,
    name: "Waldshut-Tiengen",
    plz: "79761"
  },
  {
    lati: 51.4175172605, longi: 6.61582163406,
    name: "Moers",
    plz: "47447"
  },
  {
    lati: 48.0050554928, longi: 8.77967578244,
    name: "Wurmlingen",
    plz: "78573"
  },
  {
    lati: 54.2430865262, longi: 9.38883771976,
    name: "Dellstedt",
    plz: "25786"
  },
  {
    lati: 49.8890503766, longi: 9.48587438776,
    name: "Bischbrunn",
    plz: "97836"
  },
  {
    lati: 53.2165710631, longi: 10.1413414638,
    name: "Salzhausen",
    plz: "21376"
  },
  {
    lati: 50.8799224929, longi: 12.0824878035,
    name: "Gera",
    plz: "07545"
  },
  {
    lati: 49.6088969055, longi: 8.2339369485,
    name: "Offstein",
    plz: "67591"
  },
  {
    lati: 52.8793746109, longi: 7.30861761151,
    name: "Sustrum, Lathen u.a.",
    plz: "49762"
  },
  {
    lati: 50.1205140336, longi: 7.40731650776,
    name: "Beltheim",
    plz: "56290"
  },
  {
    lati: 49.5371461705, longi: 7.3924053802,
    name: "Kusel",
    plz: "66869"
  },
  {
    lati: 49.3456881942, longi: 9.0597784496,
    name: "Obrigheim",
    plz: "74847"
  },
  {
    lati: 52.3742767198, longi: 13.1346483472,
    name: "Potsdam",
    plz: "14480"
  },
  {
    lati: 53.9779168003, longi: 9.97448867289,
    name: "Großenaspe",
    plz: "24623"
  },
  {
    lati: 53.667967468, longi: 10.0356322979,
    name: "Hamburg",
    plz: "22417"
  },
  {
    lati: 49.0524536774, longi: 10.5551291322,
    name: "Röckingen",
    plz: "91740"
  },
  {
    lati: 53.322138357, longi: 7.436742714,
    name: "Moormerland",
    plz: "26802"
  },
  {
    lati: 50.8171488773, longi: 6.97706057975,
    name: "Wesseling",
    plz: "50389"
  },
  {
    lati: 50.1119318032, longi: 10.2058887551,
    name: "Dittelbrunn",
    plz: "97456"
  },
  {
    lati: 48.4864347798, longi: 12.0492929753,
    name: "Eching",
    plz: "84174"
  },
  {
    lati: 51.0867630471, longi: 12.2083095339,
    name: "Elsteraue",
    plz: "06729"
  },
  {
    lati: 51.2663219146, longi: 12.3872818964,
    name: "Markkleeberg",
    plz: "04416"
  },
  {
    lati: 49.5240391316, longi: 8.9168001921,
    name: "Rothenberg",
    plz: "64757"
  },
  {
    lati: 52.5854029123, longi: 9.32049080597,
    name: "Linsburg",
    plz: "31636"
  },
  {
    lati: 50.0992901408, longi: 11.5103415789,
    name: "Ködnitz",
    plz: "95361"
  },
  {
    lati: 50.2506545065, longi: 11.2262667032,
    name: "Mitwitz",
    plz: "96268"
  },
  {
    lati: 51.8094267607, longi: 11.335938551,
    name: "Seeland",
    plz: "06469"
  },
  {
    lati: 48.2076685802, longi: 10.3158799099,
    name: "Ebershausen",
    plz: "86491"
  },
  {
    lati: 52.104008111, longi: 11.7003117512,
    name: "Magdeburg",
    plz: "39114"
  },
  {
    lati: 48.2297696787, longi: 12.6118000162,
    name: "Teising",
    plz: "84576"
  },
  {
    lati: 53.664390364, longi: 10.6687659751,
    name: "Breitenfelde, Lankau",
    plz: "23881"
  },
  {
    lati: 48.6169873132, longi: 10.9533561016,
    name: "Holzheim",
    plz: "86684"
  },
  {
    lati: 51.4086410974, longi: 6.84203986973,
    name: "Mülheim an der Ruhr",
    plz: "45479"
  },
  {
    lati: 48.4043662626, longi: 9.52414049895,
    name: "Münsingen",
    plz: "72525"
  },
  {
    lati: 50.1533558916, longi: 11.8491641775,
    name: "Sparneck",
    plz: "95234"
  },
  {
    lati: 48.3213904072, longi: 9.74930587486,
    name: "Altheim",
    plz: "89605"
  },
  {
    lati: 49.374515414, longi: 9.07754752933,
    name: "Binau",
    plz: "74862"
  },
  {
    lati: 50.084674429, longi: 7.20702990374,
    name: "Senheim",
    plz: "56820"
  },
  {
    lati: 49.4548593847, longi: 8.08652435895,
    name: "Bad Dürkheim",
    plz: "67098"
  },
  {
    lati: 50.6294278164, longi: 10.3921961204,
    name: "Walldorf",
    plz: "98639"
  },
  {
    lati: 50.6483505041, longi: 7.82431560294,
    name: "Hachenburg",
    plz: "57627"
  },
  {
    lati: 51.8844799459, longi: 8.44317885553,
    name: "Bertelsmann",
    plz: "33333"
  },
  {
    lati: 47.5857620715, longi: 11.1975895876,
    name: "Eschenlohe",
    plz: "82438"
  },
  {
    lati: 50.6973141635, longi: 7.13507533492,
    name: "Bonn",
    plz: "53175"
  },
  {
    lati: 51.5248197243, longi: 6.92521035133,
    name: "Bottrop",
    plz: "46236"
  },
  {
    lati: 51.2055394948, longi: 12.3826579282,
    name: "Böhlen",
    plz: "04564"
  },
  {
    lati: 52.3556973317, longi: 9.25124120365,
    name: "Lüdersfeld",
    plz: "31702"
  },
  {
    lati: 48.4317281041, longi: 9.86672950422,
    name: "Blaustein",
    plz: "89134"
  },
  {
    lati: 53.6179786074, longi: 9.96597452636,
    name: "Hamburg",
    plz: "22453"
  },
  {
    lati: 47.7745156845, longi: 10.812187125,
    name: "Burggen",
    plz: "86977"
  },
  {
    lati: 49.3469190435, longi: 8.19582411219,
    name: "Neustadt an der Weinstraße",
    plz: "67435"
  },
  {
    lati: 53.2981603305, longi: 10.2519230747,
    name: "Vierhöfen",
    plz: "21444"
  },
  {
    lati: 48.5010389209, longi: 10.4081928887,
    name: "Gundremmingen",
    plz: "89355"
  },
  {
    lati: 54.3631471205, longi: 10.4712590827,
    name: "Hohenfelde",
    plz: "24257"
  },
  {
    lati: 53.7737556528, longi: 9.84928363464,
    name: "Hemdingen",
    plz: "25485"
  },
  {
    lati: 54.780297582, longi: 9.47408193627,
    name: "Flensburg, Tastrup",
    plz: "24943"
  },
  {
    lati: 52.3701155388, longi: 11.0067980899,
    name: "Bahrdorf",
    plz: "38459"
  },
  {
    lati: 50.2319704417, longi: 11.0239636231,
    name: "Grub a. Forst",
    plz: "96271"
  },
  {
    lati: 49.2474380092, longi: 6.77702861453,
    name: "Wadgassen",
    plz: "66787"
  },
  {
    lati: 49.0716561469, longi: 10.932247375,
    name: "Ellingen",
    plz: "91792"
  },
  {
    lati: 49.6320225334, longi: 10.5407024563,
    name: "Baudenbach",
    plz: "91460"
  },
  {
    lati: 49.2327657126, longi: 8.08025884497,
    name: "Frankweiler",
    plz: "76833"
  },
  {
    lati: 53.8026414081, longi: 8.9099968089,
    name: "Otterndorf",
    plz: "21762"
  },
  {
    lati: 49.2099730161, longi: 12.6656652065,
    name: "Cham",
    plz: "93413"
  },
  {
    lati: 47.6913595685, longi: 13.0189041215,
    name: "Marktschellenberg",
    plz: "83487"
  },
  {
    lati: 51.9066258473, longi: 12.2006828556,
    name: "Dessau-Roßlau",
    plz: "06861"
  },
  {
    lati: 48.8228015754, longi: 10.9639162029,
    name: "Tagmersheim",
    plz: "86704"
  },
  {
    lati: 49.788429615, longi: 6.59777544418,
    name: "Welschbillig, Igel, Aach",
    plz: "54298"
  },
  {
    lati: 48.1856201911, longi: 8.08326036851,
    name: "Elzach, Biederbach",
    plz: "79215"
  },
  {
    lati: 48.5367237696, longi: 8.78117673011,
    name: "Mötzingen",
    plz: "71159"
  },
  {
    lati: 53.1687110955, longi: 8.89644644726,
    name: "Lilienthal",
    plz: "28865"
  },
  {
    lati: 49.4168886592, longi: 8.22156662579,
    name: "Niederkirchen bei Deidesheim",
    plz: "67150"
  },
  {
    lati: 50.1916427034, longi: 9.63608675251,
    name: "Mittelsinn",
    plz: "97785"
  },
  {
    lati: 48.8249596599, longi: 10.090773687,
    name: "Aalen",
    plz: "73431"
  },
  {
    lati: 52.3983311672, longi: 9.74685836253,
    name: "Hannover",
    plz: "30163"
  },
  {
    lati: 52.3381734082, longi: 11.3865320631,
    name: "Haldensleben, Flechtingen, Bülstringen u.a.",
    plz: "39345"
  },
  {
    lati: 47.6451987218, longi: 11.3078010177,
    name: "Schlehdorf",
    plz: "82444"
  },
  {
    lati: 51.6516598316, longi: 8.76994148031,
    name: "Borchen",
    plz: "33178"
  },
  {
    lati: 48.0020691318, longi: 12.5058025856,
    name: "Altenmarkt a.d. Alz",
    plz: "83352"
  },
  {
    lati: 50.9322741418, longi: 6.91840423924,
    name: "Köln",
    plz: "50931"
  },
  {
    lati: 47.865007676, longi: 9.42475916618,
    name: "Wilhelmsdorf",
    plz: "88271"
  },
  {
    lati: 48.8453512196, longi: 11.9882281411,
    name: "Hausen",
    plz: "93345"
  },
  {
    lati: 53.7713202059, longi: 12.1601723729,
    name: "Reimershagen, Lohmen, Zehna, Hägerfelde u.a.",
    plz: "18276"
  },
  {
    lati: 49.2981169994, longi: 12.9373815402,
    name: "Eschlkam",
    plz: "93458"
  },
  {
    lati: 52.4174165111, longi: 13.3291894475,
    name: "Berlin Lichtenfelde",
    plz: "12209"
  },
  {
    lati: 49.000130061, longi: 9.00353627985,
    name: "Sachsenheim",
    plz: "74343"
  },
  {
    lati: 52.4378172105, longi: 12.5000855287,
    name: "Brandenburg/ Havel",
    plz: "14772"
  },
  {
    lati: 51.9781422139, longi: 12.0987512166,
    name: "Zerbst/Anhalt",
    plz: "39261"
  },
  {
    lati: 50.6210625865, longi: 12.3324541868,
    name: "Reichenbach/Vogtl.",
    plz: "08468"
  },
  {
    lati: 53.1773761776, longi: 10.3301222111,
    name: "Embsen",
    plz: "21409"
  },
  {
    lati: 48.1815823318, longi: 10.4618948452,
    name: "Kirchheim in Schwaben",
    plz: "87757"
  },
  {
    lati: 48.0614880321, longi: 10.6357555413,
    name: "Türkheim",
    plz: "86842"
  },
  {
    lati: 49.5285725926, longi: 10.7628868631,
    name: "Hagenbüchach",
    plz: "91469"
  },
  {
    lati: 49.0799607541, longi: 10.8402591273,
    name: "Theilenhofen",
    plz: "91741"
  },
  {
    lati: 53.7061747869, longi: 9.97146542167,
    name: "Norderstedt",
    plz: "22846"
  },
  {
    lati: 53.5685345335, longi: 9.99635389694,
    name: "Hamburg",
    plz: "20148"
  },
  {
    lati: 50.3313737154, longi: 6.95021515196,
    name: "Reifferscheid, Kaltenborn, Wershofen u.a.",
    plz: "53520"
  },
  {
    lati: 53.5703873304, longi: 7.53147279034,
    name: "Blomberg, Neuschoo",
    plz: "26487"
  },
  {
    lati: 50.8619199712, longi: 8.69145176602,
    name: "Lahntal",
    plz: "35094"
  },
  {
    lati: 52.6175243972, longi: 7.87578426691,
    name: "Nortrup",
    plz: "49638"
  },
  {
    lati: 49.3278468072, longi: 7.69000556366,
    name: "Geiselberg",
    plz: "67715"
  },
  {
    lati: 49.2031552509, longi: 10.1855350789,
    name: "Schnelldorf",
    plz: "91625"
  },
  {
    lati: 50.2148198951, longi: 9.62619571708,
    name: "Obersinn",
    plz: "97791"
  },
  {
    lati: 48.1342159275, longi: 10.3866078117,
    name: "Breitenbrunn",
    plz: "87739"
  },
  {
    lati: 50.6583850659, longi: 6.18337544503,
    name: "Roetgen",
    plz: "52159"
  },
  {
    lati: 51.5418966929, longi: 6.70530606148,
    name: "Duisburg",
    plz: "47178"
  },
  {
    lati: 53.9582962878, longi: 12.6771017296,
    name: "Gnoien u.a.",
    plz: "17179"
  },
  {
    lati: 48.7016408755, longi: 12.9308890112,
    name: "Buchhofen",
    plz: "94533"
  },
  {
    lati: 49.0767573618, longi: 13.1066455577,
    name: "Bodenmais",
    plz: "94249"
  },
  {
    lati: 48.5236858753, longi: 11.0164040275,
    name: "Petersdorf",
    plz: "86574"
  },
  {
    lati: 48.1292438593, longi: 12.3346137329,
    name: "Unterreit",
    plz: "83567"
  },
  {
    lati: 51.4321297287, longi: 6.76803363522,
    name: "Duisburg",
    plz: "47051"
  },
  {
    lati: 51.2090230768, longi: 7.23401443148,
    name: "Remscheid",
    plz: "42899"
  },
  {
    lati: 49.2206626734, longi: 8.20909270604,
    name: "Hochstadt",
    plz: "76879"
  },
  {
    lati: 50.482993112, longi: 7.66917128403,
    name: "Nauort",
    plz: "56237"
  },
  {
    lati: 48.3143094529, longi: 9.1184885408,
    name: "Burladingen",
    plz: "72393"
  },
  {
    lati: 54.2458518614, longi: 9.19204144503,
    name: "Linden, Barkenholm",
    plz: "25791"
  },
  {
    lati: 53.184189813, longi: 8.61375872244,
    name: "Bremen",
    plz: "28755"
  },
  {
    lati: 52.3415348304, longi: 9.7222365695,
    name: "Hannover",
    plz: "30459"
  },
  {
    lati: 52.0859559747, longi: 11.3915851193,
    name: "Wanzleben-Börde",
    plz: "39164"
  },
  {
    lati: 52.6789849908, longi: 10.2310634452,
    name: "Höfer",
    plz: "29361"
  },
  {
    lati: 48.5903484693, longi: 10.7923023514,
    name: "Ehingen",
    plz: "86678"
  },
  {
    lati: 53.6532409168, longi: 10.3236170559,
    name: "Hoisdorf",
    plz: "22955"
  },
  {
    lati: 52.6445611576, longi: 10.4629549155,
    name: "Groß Oesingen",
    plz: "29393"
  },
  {
    lati: 49.6747055043, longi: 12.1710697569,
    name: "Weiden in der OPf., Theisseil",
    plz: "92637"
  },
  {
    lati: 48.8581529284, longi: 13.5141514656,
    name: "Hohenau",
    plz: "94545"
  },
  {
    lati: 50.5252072099, longi: 6.19646322017,
    name: "Monschau",
    plz: "52156"
  },
  {
    lati: 53.0138119896, longi: 11.7526097394,
    name: "Wittenberge, Rühstädt",
    plz: "19322"
  },
  {
    lati: 48.2501629867, longi: 7.86179694889,
    name: "Ettenheim",
    plz: "77955"
  },
  {
    lati: 49.4781123366, longi: 8.56705279361,
    name: "Ilvesheim",
    plz: "68549"
  },
  {
    lati: 54.3432322581, longi: 8.69707376155,
    name: "Tating, Westerhever, Tümlauer Koog",
    plz: "25881"
  },
  {
    lati: 53.0948644932, longi: 8.7745450115,
    name: "Bremen",
    plz: "28217"
  },
  {
    lati: 52.1962185745, longi: 9.61656731855,
    name: "Springe",
    plz: "31832"
  },
  {
    lati: 54.3026131365, longi: 10.2105357899,
    name: "Schwentinental",
    plz: "24222"
  },
  {
    lati: 51.3568795176, longi: 10.8045191213,
    name: "Sondershausen",
    plz: "99706"
  },
  {
    lati: 52.2095895488, longi: 10.0005080801,
    name: "Harsum",
    plz: "31177"
  },
  {
    lati: 50.4555297958, longi: 10.1956929007,
    name: "Ostheim v.d. Rhön",
    plz: "97645"
  },
  {
    lati: 50.9066488582, longi: 9.69121348208,
    name: "Bad Hersfeld, Ludwigsau",
    plz: "36251"
  },
  {
    lati: 48.7338157767, longi: 9.75358389071,
    name: "Ottenbach",
    plz: "73113"
  },
  {
    lati: 48.2352737276, longi: 10.4895574594,
    name: "Balzhausen",
    plz: "86483"
  },
  {
    lati: 50.9677481172, longi: 7.005732833,
    name: "Köln",
    plz: "51063"
  },
  {
    lati: 49.9403945467, longi: 7.57602496598,
    name: "Riesweiler",
    plz: "55499"
  },
  {
    lati: 54.9645502722, longi: 8.3473937524,
    name: "Kampen (Sylt)",
    plz: "25999"
  },
  {
    lati: 51.8234798549, longi: 9.23232455536,
    name: "Marienmünster",
    plz: "37696"
  },
  {
    lati: 49.514999233, longi: 9.32190752592,
    name: "Buchen",
    plz: "74722"
  },
  {
    lati: 54.5216780553, longi: 9.46695026698,
    name: "Schuby",
    plz: "24850"
  },
  {
    lati: 50.3565165441, longi: 9.55519063622,
    name: "Schlüchtern",
    plz: "36381"
  },
  {
    lati: 52.4175477552, longi: 13.2534916698,
    name: "Berlin Zehlendorf",
    plz: "14165"
  },
  {
    lati: 50.8259815133, longi: 7.9460873939,
    name: "Mudersbach",
    plz: "57555"
  },
  {
    lati: 52.0228078758, longi: 8.5364634843,
    name: "Bielefeld",
    plz: "33602"
  },
  {
    lati: 53.0687210574, longi: 8.949841623,
    name: "Bremen",
    plz: "28325"
  },
  {
    lati: 48.4523006431, longi: 9.22092355088,
    name: "Pfullingen",
    plz: "72793"
  },
  {
    lati: 51.5693002118, longi: 9.64572060822,
    name: "Uslar",
    plz: "37170"
  },
  {
    lati: 53.5118697845, longi: 11.5308845215,
    name: "Banzkow, Sukow",
    plz: "19079"
  },
  {
    lati: 51.1898663261, longi: 6.41654635795,
    name: "Mönchengladbach",
    plz: "41069"
  },
  {
    lati: 49.6764644169, longi: 6.46584578846,
    name: "Ayl, Trassem u.a.",
    plz: "54441"
  },
  {
    lati: 52.5127951, longi: 6.75495320737,
    name: "Itterbeck/Wielen",
    plz: "49847"
  },
  {
    lati: 51.3787326534, longi: 7.43644996289,
    name: "Hagen",
    plz: "58089"
  },
  {
    lati: 51.4820544521, longi: 7.21713282543,
    name: "Bochum",
    plz: "44787"
  },
  {
    lati: 51.4630553507, longi: 6.79041665123,
    name: "Duisburg",
    plz: "47138"
  },
  {
    lati: 50.248102835, longi: 10.8726639159,
    name: "Weitramsdorf",
    plz: "96479"
  },
  {
    lati: 48.0065222926, longi: 10.9173282243,
    name: "Pürgen",
    plz: "86932"
  },
  {
    lati: 49.8113988825, longi: 10.9781218419,
    name: "Hirschaid",
    plz: "96114"
  },
  {
    lati: 51.0394872323, longi: 14.0575044355,
    name: "Stolpen, Dürrröhrsdorf-Dittersbach",
    plz: "01833"
  },
  {
    lati: 50.9994832736, longi: 12.3301038836,
    name: "Rositz, Starkenberg, Treben",
    plz: "04617"
  },
  {
    lati: 51.1566404359, longi: 10.5689883149,
    name: "Großengottern, Heroldishausen",
    plz: "99991"
  },
  {
    lati: 49.2455037677, longi: 9.91696107059,
    name: "Gerabronn",
    plz: "74582"
  },
  {
    lati: 51.007370395, longi: 10.921476405,
    name: "Erfurt",
    plz: "99090"
  },
  {
    lati: 51.8633897663, longi: 10.9449807331,
    name: "Langenstein, Derenburg",
    plz: "38895"
  },
  {
    lati: 54.2140023098, longi: 9.88630352494,
    name: "Warder",
    plz: "24646"
  },
  {
    lati: 53.7540457909, longi: 9.61204745628,
    name: "Elmshorn",
    plz: "25335"
  },
  {
    lati: 54.3381187617, longi: 10.1431953305,
    name: "Kiel",
    plz: "24105"
  },
  {
    lati: 51.2118446988, longi: 7.36012832136,
    name: "Radevormwald",
    plz: "42477"
  },
  {
    lati: 51.1659887042, longi: 6.82064248595,
    name: "Düsseldorf",
    plz: "40589"
  },
  {
    lati: 50.0458813019, longi: 8.15079011918,
    name: "Walluf",
    plz: "65396"
  },
  {
    lati: 49.8659134096, longi: 7.59310117578,
    name: "Sobernheim",
    plz: "55566"
  },
  {
    lati: 49.6764925633, longi: 8.24306781234,
    name: "Gundheim",
    plz: "67599"
  },
  {
    lati: 48.8663119045, longi: 8.28132533605,
    name: "Muggensturm",
    plz: "76461"
  },
  {
    lati: 54.7177712364, longi: 8.49880774756,
    name: "Föhr",
    plz: "25938"
  },
  {
    lati: 53.0991700809, longi: 10.6546982926,
    name: "Römstedt",
    plz: "29591"
  },
  {
    lati: 50.4383503556, longi: 10.0776375397,
    name: "Oberelsbach",
    plz: "97656"
  },
  {
    lati: 50.5288079773, longi: 10.1415363932,
    name: "Fladungen",
    plz: "97650"
  },
  {
    lati: 50.8052662596, longi: 10.4278388083,
    name: "Brotterode-Trusetal",
    plz: "98596"
  },
  {
    lati: 53.109307648, longi: 12.8995068148,
    name: "Rheinsberg",
    plz: "16831"
  },
  {
    lati: 49.982472226, longi: 11.934925433,
    name: "Nagel",
    plz: "95697"
  },
  {
    lati: 53.0458757165, longi: 14.0075487468,
    name: "Angermünde",
    plz: "16278"
  },
  {
    lati: 49.9520694752, longi: 11.8068437739,
    name: "Immenreuth",
    plz: "95505"
  },
  {
    lati: 50.1522029541, longi: 8.48592095635,
    name: "Bad Soden am Taunus",
    plz: "65812"
  },
  {
    lati: 51.010668081, longi: 9.4128393975,
    name: "Homberg",
    plz: "34576"
  },
  {
    lati: 50.7062783424, longi: 13.2491249306,
    name: "Pockau-Lengefeld (Pockau)",
    plz: "09509"
  },
  {
    lati: 52.4420009749, longi: 13.5816973955,
    name: "Berlin Köpenick",
    plz: "12559"
  },
  {
    lati: 48.5531647892, longi: 9.36894763545,
    name: "Neuffen",
    plz: "72639"
  },
  {
    lati: 49.9137787369, longi: 7.92263507523,
    name: "Aspisheim, Grolsheim",
    plz: "55459"
  },
  {
    lati: 49.4079839204, longi: 7.98587201136,
    name: "Weidenthal",
    plz: "67475"
  },
  {
    lati: 47.7114765428, longi: 8.0986761683,
    name: "Dachsberg",
    plz: "79875"
  },
  {
    lati: 51.0487923889, longi: 9.27650056227,
    name: "Borken",
    plz: "34582"
  },
  {
    lati: 49.9976730697, longi: 10.6901933039,
    name: "Ebelsbach",
    plz: "97500"
  },
  {
    lati: 49.1582103193, longi: 10.8609481719,
    name: "Absberg",
    plz: "91720"
  },
  {
    lati: 49.8910347769, longi: 10.8899597615,
    name: "Bamberg",
    plz: "96047"
  },
  {
    lati: 49.9211485754, longi: 10.173291362,
    name: "Wipfeld",
    plz: "97537"
  },
  {
    lati: 48.2196175371, longi: 12.1469901111,
    name: "Sankt Wolfgang",
    plz: "84427"
  },
  {
    lati: 51.2760130866, longi: 12.2997654607,
    name: "Leipzig",
    plz: "04249"
  },
  {
    lati: 50.6252281483, longi: 12.7484815245,
    name: "Lößnitz",
    plz: "08294"
  },
  {
    lati: 48.9348526191, longi: 8.75229166221,
    name: "Kieselbronn",
    plz: "75249"
  },
  {
    lati: 51.0784258172, longi: 13.1757261049,
    name: "Roßwein",
    plz: "04741"
  },
  {
    lati: 52.5351586776, longi: 13.4255510177,
    name: "Berlin Prenzlauer Berg",
    plz: "10405"
  },
  {
    lati: 52.4874139722, longi: 13.4434460922,
    name: "Berlin Neukölln",
    plz: "12059"
  },
  {
    lati: 51.4203027158, longi: 13.554741987,
    name: "Röderland, Großthiemig u.a.",
    plz: "04932"
  },
  {
    lati: 48.7828597615, longi: 9.1702837376,
    name: "Stuttgart",
    plz: "70174"
  },
  {
    lati: 50.5740229839, longi: 9.54704591652,
    name: "Großenlüder",
    plz: "36137"
  },
  {
    lati: 53.5494621558, longi: 10.0488032741,
    name: "Hamburg",
    plz: "20537"
  },
  {
    lati: 49.0260804589, longi: 10.3564890323,
    name: "Mönchsroth",
    plz: "91614"
  },
  {
    lati: 48.5083394628, longi: 10.4501631873,
    name: "Aislingen",
    plz: "89344"
  },
  {
    lati: 48.5436943946, longi: 10.6657371839,
    name: "Wertingen",
    plz: "86637"
  },
  {
    lati: 48.2296314402, longi: 11.46269776,
    name: "Karlsfeld",
    plz: "85757"
  },
  {
    lati: 50.1700071306, longi: 11.636986865,
    name: "Marktleugast",
    plz: "95352"
  },
  {
    lati: 47.9779123832, longi: 11.8845301606,
    name: "Glonn",
    plz: "85625"
  },
  {
    lati: 54.306861661, longi: 8.63681860726,
    name: "Sankt Peter-Ording",
    plz: "25826"
  },
  {
    lati: 48.822400557, longi: 8.14375207845,
    name: "Iffezheim",
    plz: "76473"
  },
  {
    lati: 47.9420725282, longi: 9.64890701839,
    name: "Aulendorf",
    plz: "88326"
  },
  {
    lati: 51.0114188166, longi: 9.74119985142,
    name: "Rotenburg an der Fulda",
    plz: "36199"
  },
  {
    lati: 48.1198738712, longi: 10.2158773868,
    name: "Winterrieden",
    plz: "87785"
  },
  {
    lati: 48.2280166016, longi: 12.6658131495,
    name: "Altötting",
    plz: "84503"
  },
  {
    lati: 50.8251822835, longi: 12.8684898889,
    name: "Chemnitz",
    plz: "09116"
  },
  {
    lati: 47.6896037376, longi: 11.4222533718,
    name: "Benediktbeuern",
    plz: "83671"
  },
  {
    lati: 50.6560827612, longi: 7.90579950998,
    name: "Unnau",
    plz: "57648"
  },
  {
    lati: 51.442936748, longi: 7.10751636798,
    name: "Essen",
    plz: "45279"
  },
  {
    lati: 53.2110983011, longi: 7.63807477368,
    name: "Detern",
    plz: "26847"
  },
  {
    lati: 47.7526993053, longi: 7.79427021833,
    name: "Kleines Wiesental",
    plz: "79692"
  },
  {
    lati: 48.087293764, longi: 11.5573967819,
    name: "München",
    plz: "81545"
  },
  {
    lati: 48.433959035, longi: 11.590388149,
    name: "Allershausen",
    plz: "85391"
  },
  {
    lati: 50.8541807061, longi: 7.33082885912,
    name: "Neunkirchen-Seelscheid",
    plz: "53819"
  },
  {
    lati: 48.7901790378, longi: 8.49375331479,
    name: "Dobel",
    plz: "75335"
  },
  {
    lati: 52.0602998847, longi: 8.5616520391,
    name: "Bielefeld",
    plz: "33609"
  },
  {
    lati: 53.9313855066, longi: 12.0868454923,
    name: "Schwaan u.a.",
    plz: "18258"
  },
  {
    lati: 51.4264729667, longi: 7.93255675814,
    name: "Arnsberg",
    plz: "59757"
  },
  {
    lati: 52.8873641433, longi: 10.6112095776,
    name: "Wrestedt",
    plz: "29559"
  },
  {
    lati: 48.1982367264, longi: 11.7953605363,
    name: "Pliening",
    plz: "85652"
  },
  {
    lati: 50.0544382693, longi: 10.7937028643,
    name: "Rentweinsdorf",
    plz: "96184"
  },
  {
    lati: 53.4183838876, longi: 10.9499433795,
    name: "Vellahn",
    plz: "19260"
  },
  {
    lati: 54.3256836732, longi: 11.0330604692,
    name: "Neukirchen",
    plz: "23779"
  },
  {
    lati: 54.7844358562, longi: 9.43029901025,
    name: "Flensburg",
    plz: "24937"
  },
  {
    lati: 51.5930717726, longi: 10.2055263745,
    name: "Gieboldehausen, Rhumequelle",
    plz: "37434"
  },
  {
    lati: 48.5840415935, longi: 9.23767333939,
    name: "Neckartenzlingen",
    plz: "72654"
  },
  {
    lati: 50.9088356859, longi: 9.44563613033,
    name: "Schwarzenborn",
    plz: "34639"
  },
  {
    lati: 48.8530527632, longi: 8.81532388144,
    name: "Wimsheim",
    plz: "71299"
  },
  {
    lati: 50.0984418131, longi: 6.80982645728,
    name: "Manderscheid",
    plz: "54531"
  },
  {
    lati: 48.8908556799, longi: 10.4743780198,
    name: "Wallerstein",
    plz: "86757"
  },
  {
    lati: 52.371745164, longi: 10.5049435626,
    name: "Vordorf",
    plz: "38533"
  },
  {
    lati: 50.490766562, longi: 11.8603159587,
    name: "Tanna",
    plz: "07922"
  },
  {
    lati: 48.7081666692, longi: 11.1098054281,
    name: "Oberhausen",
    plz: "86697"
  },
  {
    lati: 52.2838330089, longi: 11.6221203482,
    name: "Wolmirstedt u.a.",
    plz: "39326"
  },
  {
    lati: 54.1885430556, longi: 13.2046434649,
    name: "Miltzow u.a.",
    plz: "18519"
  },
  {
    lati: 51.1597622426, longi: 6.98832141186,
    name: "Solingen",
    plz: "42697"
  },
  {
    lati: 51.0431908065, longi: 6.2307979527,
    name: "Hückelhoven",
    plz: "41836"
  },
  {
    lati: 50.0089378509, longi: 6.623767297,
    name: "Badem, Gindorf, Neidenbach",
    plz: "54657"
  },
  {
    lati: 49.2940025515, longi: 9.71111834756,
    name: "Künzelsau, Ingelfingen",
    plz: "74653"
  },
  {
    lati: 47.8227418412, longi: 10.859444029,
    name: "Altenstadt",
    plz: "86972"
  },
  {
    lati: 49.4152052992, longi: 8.39312747528,
    name: "Limburgerhof",
    plz: "67117"
  },
  {
    lati: 48.0735720726, longi: 8.7702451632,
    name: "Balgheim",
    plz: "78582"
  },
  {
    lati: 51.257085415, longi: 6.97778275428,
    name: "Mettmann",
    plz: "40822"
  },
  {
    lati: 48.9241248323, longi: 9.12447054139,
    name: "Tamm",
    plz: "71732"
  },
  {
    lati: 50.7767827409, longi: 9.50069048799,
    name: "Breitenbach am Herzberg",
    plz: "36287"
  },
  {
    lati: 48.1120255048, longi: 11.7736032087,
    name: "Vaterstetten",
    plz: "85591"
  },
  {
    lati: 48.646373109, longi: 10.6143676184,
    name: "Blindheim",
    plz: "89434"
  },
  {
    lati: 52.6908716019, longi: 13.5824582039,
    name: "Bernau",
    plz: "16321"
  },
  {
    lati: 51.2069952134, longi: 6.70696959362,
    name: "Neuss",
    plz: "41460"
  },
  {
    lati: 49.3903010537, longi: 9.39526503663,
    name: "Adelsheim",
    plz: "74740"
  },
  {
    lati: 47.8380219145, longi: 8.5564274654,
    name: "Blumberg",
    plz: "78176"
  },
  {
    lati: 51.6426059972, longi: 7.73780722686,
    name: "Hamm",
    plz: "59077"
  },
  {
    lati: 53.0871601199, longi: 8.82913047278,
    name: "Bremen",
    plz: "28209"
  },
  {
    lati: 50.5267599299, longi: 9.68820634005,
    name: "Fulda",
    plz: "36043"
  },
  {
    lati: 53.2663509137, longi: 10.2231334786,
    name: "Vierhöfen",
    plz: "21444"
  },
  {
    lati: 47.9729785187, longi: 10.2679865612,
    name: "Hawangen",
    plz: "87749"
  },
  {
    lati: 49.2016095061, longi: 12.9380126644,
    name: "Hohenwarth",
    plz: "93480"
  },
  {
    lati: 50.812583141, longi: 9.39308088052,
    name: "Ottrau",
    plz: "34633"
  },
  {
    lati: 49.793766835, longi: 9.45057694047,
    name: "Faulbach",
    plz: "97906"
  },
  {
    lati: 51.9528436981, longi: 9.05795341122,
    name: "Blomberg",
    plz: "32825"
  },
  {
    lati: 51.7662989978, longi: 6.83325121394,
    name: "Raesfeld",
    plz: "46348"
  },
  {
    lati: 50.745601499, longi: 7.06239203616,
    name: "Bonn",
    plz: "53119"
  },
  {
    lati: 50.0274019022, longi: 7.19408940098,
    name: "Zell (Mosel)",
    plz: "56856"
  },
  {
    lati: 51.256991111, longi: 7.17478205346,
    name: "Wuppertal",
    plz: "42285"
  },
  {
    lati: 50.2048561148, longi: 7.82163565662,
    name: "Nastätten u.a.",
    plz: "56355"
  },
  {
    lati: 52.3097916519, longi: 8.62160595146,
    name: "Lübbecke",
    plz: "32312"
  },
  {
    lati: 49.7329129268, longi: 7.50081626798,
    name: "Bergen",
    plz: "55608"
  },
  {
    lati: 52.9086160664, longi: 12.4720232443,
    name: "Wusterhausen",
    plz: "16868"
  },
  {
    lati: 50.7273679949, longi: 12.5267466356,
    name: "Zwickau",
    plz: "08066"
  },
  {
    lati: 49.4351030598, longi: 10.9252378569,
    name: "Zirndorf",
    plz: "90513"
  },
  {
    lati: 48.348576197, longi: 10.9449046969,
    name: "Augsburg",
    plz: "86163"
  },
  {
    lati: 52.9848078971, longi: 11.1591313945,
    name: "Lüchow",
    plz: "29439"
  },
  {
    lati: 51.120094827, longi: 10.617453156,
    name: "Bad Langensalza",
    plz: "99947"
  },
  {
    lati: 50.197002783, longi: 10.5315249559,
    name: "Bundorf",
    plz: "97494"
  },
  {
    lati: 48.7279868111, longi: 13.6875205446,
    name: "Jandelsbrunn",
    plz: "94118"
  },
  {
    lati: 50.8183469063, longi: 11.6036829465,
    name: "Jena, Bucha, Großpürschütz u.a.",
    plz: "07751"
  },
  {
    lati: 47.5706122893, longi: 10.5326686716,
    name: "Pfronten",
    plz: "87459"
  },
  {
    lati: 48.8537095551, longi: 13.1423719781,
    name: "Lalling",
    plz: "94551"
  },
  {
    lati: 48.1690150783, longi: 12.516454014,
    name: "Oberneukirchen",
    plz: "84565"
  },
  {
    lati: 49.814379086, longi: 7.60015951037,
    name: "Monzingen",
    plz: "55569"
  },
  {
    lati: 50.7930178723, longi: 6.76687756959,
    name: "Erftstadt",
    plz: "50374"
  },
  {
    lati: 49.9697676, longi: 8.65540500071,
    name: "Egelsbach",
    plz: "63329"
  },
  {
    lati: 53.1110980694, longi: 8.79604289719,
    name: "Bremen",
    plz: "28219"
  },
  {
    lati: 54.0334085207, longi: 9.70538395397,
    name: "Hennstedt",
    plz: "25581"
  },
  {
    lati: 47.8312092303, longi: 12.1043403374,
    name: "Rosenheim",
    plz: "83026"
  },
  {
    lati: 49.441901825, longi: 8.34717150176,
    name: "Mutterstadt",
    plz: "67112"
  },
  {
    lati: 50.8596889335, longi: 7.03599271527,
    name: "Köln",
    plz: "51143"
  },
  {
    lati: 50.0048145619, longi: 7.1270044336,
    name: "Irmenach",
    plz: "56843"
  },
  {
    lati: 49.8339543668, longi: 7.40646799823,
    name: "Kirn",
    plz: "55606"
  },
  {
    lati: 47.7976181775, longi: 12.2824135479,
    name: "Frasdorf",
    plz: "83112"
  },
  {
    lati: 54.4911184352, longi: 13.2641659548,
    name: "Gingst",
    plz: "18569"
  },
  {
    lati: 52.5808559406, longi: 13.5216924057,
    name: "Berlin Wartenberg",
    plz: "13059"
  },
  {
    lati: 49.8768438704, longi: 10.0316124254,
    name: "Unterpleichfeld",
    plz: "97294"
  },
  {
    lati: 51.5348757717, longi: 7.40492646114,
    name: "Dortmund",
    plz: "44369"
  },
  {
    lati: 49.8714156151, longi: 6.42753448014,
    name: "Ferschweiler",
    plz: "54668"
  },
  {
    lati: 49.8635542243, longi: 6.76945106933,
    name: "Föhren",
    plz: "54343"
  },
  {
    lati: 48.3072690414, longi: 12.3030429194,
    name: "Buchbach",
    plz: "84428"
  },
  {
    lati: 49.7523428839, longi: 10.9763400633,
    name: "Hallerndorf",
    plz: "91352"
  },
  {
    lati: 52.5345857473, longi: 13.1407235098,
    name: "Berlin Staaken",
    plz: "13591"
  },
  {
    lati: 47.7377911988, longi: 7.99770442061,
    name: "Todtmoos",
    plz: "79682"
  },
  {
    lati: 48.0775366123, longi: 11.5236193236,
    name: "München",
    plz: "81479"
  },
  {
    lati: 52.4567374421, longi: 9.85065225039,
    name: "Isernhagen",
    plz: "30916"
  },
  {
    lati: 50.5413811445, longi: 11.9193414133,
    name: "Kirschkau, Pausa-Mühltroff",
    plz: "07919"
  },
  {
    lati: 49.6374495622, longi: 12.0249318623,
    name: "Weiherhammer",
    plz: "92729"
  },
  {
    lati: 50.4106245197, longi: 10.7006151294,
    name: "Hildburghausen",
    plz: "98646"
  },
  {
    lati: 52.9499271316, longi: 10.9309973405,
    name: "Clenze",
    plz: "29459"
  },
  {
    lati: 48.3720983292, longi: 10.8933803682,
    name: "Augsburg",
    plz: "86152"
  },
  {
    lati: 51.2023192662, longi: 6.44515158704,
    name: "Mönchengladbach",
    plz: "41063"
  },
  {
    lati: 53.3832041637, longi: 7.20690576352,
    name: "Emden",
    plz: "26721"
  },
  {
    lati: 49.9288625978, longi: 8.14057730763,
    name: "Ober-Olm",
    plz: "55270"
  },
  {
    lati: 51.1134571386, longi: 8.60898388089,
    name: "Bromskirchen, Hallenberg",
    plz: "59969"
  },
  {
    lati: 50.6455175234, longi: 8.64722317978,
    name: "Wettenberg",
    plz: "35435"
  },
  {
    lati: 51.2725809249, longi: 7.18236596669,
    name: "Wuppertal",
    plz: "42283"
  },
  {
    lati: 48.400544254, longi: 12.6174907702,
    name: "Massing",
    plz: "84323"
  },
  {
    lati: 54.6108837832, longi: 8.95658699118,
    name: "Bredstedt, Breklum u.a.",
    plz: "25821"
  },
  {
    lati: 48.7695049197, longi: 11.6125015089,
    name: "Vohburg a.d. Donau",
    plz: "85088"
  },
  {
    lati: 53.2528232625, longi: 10.3342247152,
    name: "Reppenstedt, Lüneburg",
    plz: "21391"
  },
  {
    lati: 47.9518082237, longi: 10.5441151054,
    name: "Baisweil",
    plz: "87650"
  },
  {
    lati: 52.6096731482, longi: 10.6009664052,
    name: "Wahrenholz",
    plz: "29399"
  },
  {
    lati: 48.1737005724, longi: 7.70688753377,
    name: "Forchheim",
    plz: "79362"
  },
  {
    lati: 51.7817987037, longi: 11.3059934452,
    name: "Seeland",
    plz: "06467"
  },
  {
    lati: 51.316328167, longi: 7.10408982077,
    name: "Velbert",
    plz: "42553"
  },
  {
    lati: 51.5375291314, longi: 7.26853154933,
    name: "Herne",
    plz: "44627"
  },
  {
    lati: 51.28282225, longi: 7.36583935771,
    name: "Ennepetal",
    plz: "58256"
  },
  {
    lati: 49.7119490813, longi: 6.79874073359,
    name: "Osburg, Gusterath, Farschweiler, Kasel u.a.",
    plz: "54317"
  },
  {
    lati: 51.0959057252, longi: 6.89270712451,
    name: "Monheim am Rhein",
    plz: "40789"
  },
  {
    lati: 54.0885971598, longi: 12.1050132546,
    name: "Rostock",
    plz: "18057"
  },
  {
    lati: 51.1317760949, longi: 12.3083459256,
    name: "Groitzsch",
    plz: "04539"
  },
  {
    lati: 52.2766747519, longi: 13.0021300093,
    name: "Seddinger See",
    plz: "14554"
  },
  {
    lati: 48.8755505789, longi: 9.32445766828,
    name: "Waiblingen",
    plz: "71336"
  },
  {
    lati: 52.4725431478, longi: 13.7966131607,
    name: "Rüdersdorf",
    plz: "15562"
  },
  {
    lati: 48.3045351547, longi: 10.1736152287,
    name: "Weißenhorn",
    plz: "89264"
  },
  {
    lati: 53.2743459252, longi: 10.6368934107,
    name: "Neetze",
    plz: "21398"
  },
  {
    lati: 51.674360073, longi: 10.8544359999,
    name: "Oberharz am Brocken",
    plz: "38899"
  },
  {
    lati: 51.5821393191, longi: 7.66608210265,
    name: "Kamen",
    plz: "59174"
  },
  {
    lati: 52.7380777491, longi: 7.75555067747,
    name: "Löningen",
    plz: "49624"
  },
  {
    lati: 50.7339990569, longi: 7.85211854171,
    name: "Emmerzhausen, Niederdreisbach, Steinebach",
    plz: "57520"
  },
  {
    lati: 52.2606753238, longi: 10.500514317,
    name: "Braunschweig",
    plz: "38118"
  },
  {
    lati: 50.0528374185, longi: 8.31572549377,
    name: "Wiesbaden",
    plz: "65205"
  },
  {
    lati: 48.0573460767, longi: 8.44089643173,
    name: "Villingen-Schwenningen",
    plz: "78050"
  },
  {
    lati: 50.8534913812, longi: 14.689548339,
    name: "Jonsdorf",
    plz: "02796"
  },
  {
    lati: 51.2917259611, longi: 13.3247041497,
    name: "Riesa",
    plz: "01589"
  },
  {
    lati: 52.5224264166, longi: 13.6688166735,
    name: "Neuenhagen, Hoppegarten",
    plz: "15366"
  },
  {
    lati: 50.5975576296, longi: 6.24743652329,
    name: "Monschau",
    plz: "52156"
  },
  {
    lati: 51.472420792, longi: 6.95143858794,
    name: "Essen",
    plz: "45355"
  },
  {
    lati: 49.6884535148, longi: 10.9428190391,
    name: "Hemhofen",
    plz: "91334"
  },
  {
    lati: 50.2691953146, longi: 7.67565583169,
    name: "Braubach",
    plz: "56338"
  },
  {
    lati: 52.2276938692, longi: 9.0871901883,
    name: "Luhden",
    plz: "31711"
  },
  {
    lati: 49.0929091993, longi: 12.6552750466,
    name: "Loitzendorf",
    plz: "94359"
  },
  {
    lati: 50.8200529057, longi: 12.0194830291,
    name: "Gera, Zedlitz u.a.",
    plz: "07557"
  },
  {
    lati: 48.8098435137, longi: 11.45426085,
    name: "Lenting",
    plz: "85101"
  },
  {
    lati: 51.3169690718, longi: 13.2442198174,
    name: "Riesa",
    plz: "01591"
  },
  {
    lati: 51.0122361814, longi: 13.3848659511,
    name: "Reinsberg",
    plz: "09629"
  },
  {
    lati: 53.4575469836, longi: 13.5613636348,
    name: "Woldegk",
    plz: "17348"
  },
  {
    lati: 50.9420934079, longi: 7.01671631169,
    name: "Köln",
    plz: "51103"
  },
  {
    lati: 50.1033884789, longi: 7.14226592635,
    name: "Ediger-Eller",
    plz: "56814"
  },
  {
    lati: 50.1137872974, longi: 7.23132560178,
    name: "Ellenz-Poltersdorf",
    plz: "56821"
  },
  {
    lati: 49.7872729463, longi: 8.97675040812,
    name: "Höchst i. Odw.",
    plz: "64739"
  },
  {
    lati: 54.3450982691, longi: 9.0918096765,
    name: "Sankt Annen, Rehm-Flehde-Bargen",
    plz: "25776"
  },
  {
    lati: 47.7088046867, longi: 9.27950748728,
    name: "Meersburg",
    plz: "88709"
  },
  {
    lati: 54.7956628441, longi: 9.64077754949,
    name: "Langballig",
    plz: "24977"
  },
  {
    lati: 49.2824999674, longi: 11.4729749167,
    name: "Neumarkt i.d. OPf.",
    plz: "92318"
  },
  {
    lati: 53.8993955368, longi: 13.3274203947,
    name: "Jarmen",
    plz: "17126"
  },
  {
    lati: 52.4801367118, longi: 13.4522035958,
    name: "Berlin Neukölln",
    plz: "12059"
  },
  {
    lati: 49.596586156, longi: 6.61865220327,
    name: "Irsch",
    plz: "54451"
  },
  {
    lati: 49.4021787365, longi: 6.97923284152,
    name: "Eppelborn",
    plz: "66571"
  },
  {
    lati: 53.0966620696, longi: 8.71467857929,
    name: "Bremen",
    plz: "28197"
  },
  {
    lati: 50.3403605333, longi: 7.55833147506,
    name: "Koblenz",
    plz: "56073"
  },
  {
    lati: 53.0864207455, longi: 10.9904998726,
    name: "Karwitz",
    plz: "29481"
  },
  {
    lati: 50.072760027, longi: 12.1258575409,
    name: "Thiersheim",
    plz: "95707"
  },
  {
    lati: 48.585984225, longi: 8.45572584138,
    name: "Seewald",
    plz: "72297"
  },
  {
    lati: 49.5741520571, longi: 7.08517596172,
    name: "Nohfelden",
    plz: "66625"
  },
  {
    lati: 50.7062789891, longi: 10.8308132903,
    name: "Geschwenda",
    plz: "98716"
  },
  {
    lati: 52.8893862261, longi: 10.8682023842,
    name: "Schnega",
    plz: "29465"
  },
  {
    lati: 50.0460586187, longi: 10.2217156666,
    name: "Schweinfurt",
    plz: "97421"
  },
  {
    lati: 48.08858708, longi: 10.2725914623,
    name: "Egg an der Günz",
    plz: "87743"
  },
  {
    lati: 51.8089073801, longi: 10.3362658753,
    name: "Clausthal-Zellerfeld, Oberschulenberg",
    plz: "38678"
  },
  {
    lati: 48.7187843996, longi: 10.5796362339,
    name: "Bissingen",
    plz: "86657"
  },
  {
    lati: 49.6936009986, longi: 9.04437994358,
    name: "Michelstadt",
    plz: "64720"
  },
  {
    lati: 50.2915838159, longi: 9.1087518242,
    name: "Büdingen",
    plz: "63654"
  },
  {
    lati: 48.2382794323, longi: 12.3406331843,
    name: "Heldenstein",
    plz: "84431"
  },
  {
    lati: 52.3075079514, longi: 9.21774939562,
    name: "Stadthagen",
    plz: "31655"
  },
  {
    lati: 52.0699065776, longi: 12.8601611203,
    name: "Treuenbrietzen",
    plz: "14929"
  },
  {
    lati: 50.8965110129, longi: 12.3075034019,
    name: "Schmölln, Altkirchen, Nöbdenitz u.a.",
    plz: "04626"
  },
  {
    lati: 48.8488692634, longi: 13.056901375,
    name: "Schaufling",
    plz: "94571"
  },
  {
    lati: 52.5707465441, longi: 13.4409590244,
    name: "Berlin Heinelsdorf",
    plz: "13089"
  },
  {
    lati: 50.9814052863, longi: 13.7233463059,
    name: "Bannewitz",
    plz: "01728"
  },
  {
    lati: 51.3754210907, longi: 14.0658529924,
    name: "Bernsdorf",
    plz: "02994"
  },
  {
    lati: 51.1080227885, longi: 14.079291234,
    name: "Großharthau, Frankenthal",
    plz: "01909"
  },
  {
    lati: 53.652572619, longi: 10.3717348168,
    name: "Lütjensee",
    plz: "22952"
  },
  {
    lati: 49.3657675125, longi: 11.1131849325,
    name: "Nürnberg",
    plz: "90455"
  },
  {
    lati: 49.3127373054, longi: 9.6008112615,
    name: "Weißbach",
    plz: "74679"
  },
  {
    lati: 49.7846219873, longi: 9.70876207396,
    name: "Üttingen, Holzkirchen",
    plz: "97292"
  },
  {
    lati: 53.8021061017, longi: 9.68262706058,
    name: "Klein Offenseth-Sparrieshoop",
    plz: "25365"
  },
  {
    lati: 49.4977445605, longi: 8.54697556628,
    name: "Mannheim",
    plz: "68259"
  },
  {
    lati: 49.5091539531, longi: 8.21284100976,
    name: "Freinsheim",
    plz: "67251"
  },
  {
    lati: 51.4801967854, longi: 6.71619757517,
    name: "Duisburg",
    plz: "47139"
  },
  {
    lati: 48.7599642632, longi: 8.86407773034,
    name: "Weil der Stadt",
    plz: "71263"
  },
  {
    lati: 49.0568936448, longi: 8.958664877,
    name: "Pfaffenhofen",
    plz: "74397"
  },
  {
    lati: 52.6776739887, longi: 9.47155188529,
    name: "Rodewald",
    plz: "31637"
  },
  {
    lati: 51.6682989753, longi: 7.83260496236,
    name: "Hamm",
    plz: "59063"
  },
  {
    lati: 52.00072213, longi: 7.68320164866,
    name: "Münster",
    plz: "48157"
  },
  {
    lati: 54.1776099192, longi: 10.1725136832,
    name: "Nettelsee",
    plz: "24250"
  },
  {
    lati: 50.0565021111, longi: 9.69296136718,
    name: "Gemünden a. Main",
    plz: "97737"
  },
  {
    lati: 49.9896269121, longi: 10.3400081606,
    name: "Grettstadt",
    plz: "97508"
  },
  {
    lati: 49.2921931121, longi: 10.5637176661,
    name: "Ansbach",
    plz: "91522"
  },
  {
    lati: 48.7669454495, longi: 13.0265745993,
    name: "Niederalteich",
    plz: "94557"
  },
  {
    lati: 47.9332090973, longi: 12.3852032505,
    name: "Eggstätt",
    plz: "83125"
  },
  {
    lati: 51.9415019326, longi: 11.4884989454,
    name: "Egeln, Unseburg, Wolmirsleben, Borne etc",
    plz: "39435"
  },
  {
    lati: 51.5259691113, longi: 7.17459497943,
    name: "Herne",
    plz: "44652"
  },
  {
    lati: 48.1710873313, longi: 7.8573228352,
    name: "Malterdingen",
    plz: "79364"
  },
  {
    lati: 47.9395963289, longi: 8.08745355305,
    name: "Breitnau",
    plz: "79874"
  },
  {
    lati: 53.0384217607, longi: 8.81865497406,
    name: "Bremen",
    plz: "28277"
  },
  {
    lati: 50.7045799594, longi: 11.9291239549,
    name: "Auma-Weidatal",
    plz: "07955"
  },
  {
    lati: 48.046804731, longi: 10.069528638,
    name: "Berkheim",
    plz: "88450"
  },
  {
    lati: 49.2505510115, longi: 12.6826872406,
    name: "Willmering",
    plz: "93497"
  },
  {
    lati: 50.6066174037, longi: 7.24684305654,
    name: "Bruchhausen, Unkel",
    plz: "53572"
  },
  {
    lati: 52.4003689067, longi: 7.31213250054,
    name: "Emsbüren",
    plz: "48488"
  },
  {
    lati: 51.5616412073, longi: 6.29194002831,
    name: "Kevelaer-Wetten",
    plz: "47625"
  },
  {
    lati: 53.9536255958, longi: 11.1382781578,
    name: "Klütz",
    plz: "23948"
  },
  {
    lati: 52.1506326166, longi: 11.4611790298,
    name: "Eichenbarleben, Ochtmersleben, Irxleben, Schnarsleben, Wellen etc",
    plz: "39167"
  },
  {
    lati: 48.1523349172, longi: 11.5750463062,
    name: "München",
    plz: "80799"
  },
  {
    lati: 49.657854398, longi: 7.67919707012,
    name: "Becherbach",
    plz: "67827"
  },
  {
    lati: 48.5962286401, longi: 8.60181230973,
    name: "Altensteig",
    plz: "72213"
  },
  {
    lati: 48.7186712943, longi: 9.4209497778,
    name: "Plochingen",
    plz: "73207"
  },
  {
    lati: 52.262858337, longi: 10.6818264913,
    name: "Cremlingen",
    plz: "38162"
  },
  {
    lati: 48.3034678571, longi: 10.7146344692,
    name: "Gessertshausen",
    plz: "86459"
  },
  {
    lati: 48.772225674, longi: 8.92451307566,
    name: "Renningen",
    plz: "71272"
  },
  {
    lati: 52.4227652798, longi: 13.4589146951,
    name: "Berlin Gropiusstadt",
    plz: "12353"
  },
  {
    lati: 51.5937864151, longi: 13.9872039562,
    name: "Großräschen",
    plz: "01983"
  },
  {
    lati: 54.0708499956, longi: 9.88449691169,
    name: "Wasbek",
    plz: "24647"
  },
  {
    lati: 50.5800215462, longi: 11.1576510063,
    name: "Oberweißbach u.a.",
    plz: "98744"
  },
  {
    lati: 48.8810126562, longi: 10.4036825261,
    name: "Kirchheim am Ries",
    plz: "73467"
  },
  {
    lati: 53.7235908956, longi: 10.7079812969,
    name: "Ziethen",
    plz: "23911"
  },
  {
    lati: 51.4268289779, longi: 14.219317296,
    name: "Hoyerswerda",
    plz: "02977"
  },
  {
    lati: 52.5901304805, longi: 7.42013306419,
    name: "Bawinkel",
    plz: "49844"
  },
  {
    lati: 49.7663695187, longi: 6.56546877343,
    name: "Trierweiler",
    plz: "54311"
  },
  {
    lati: 49.4887896826, longi: 7.84754016216,
    name: "Mehlingen",
    plz: "67678"
  },
  {
    lati: 50.8940126279, longi: 7.79192097191,
    name: "Friesenhagen",
    plz: "51598"
  },
  {
    lati: 50.8399630854, longi: 8.90947774433,
    name: "Kirchhain",
    plz: "35274"
  },
  {
    lati: 48.4797040041, longi: 8.50296511025,
    name: "Dornstetten",
    plz: "72280"
  },
  {
    lati: 49.5960534606, longi: 9.56461991225,
    name: "Königheim",
    plz: "97953"
  },
  {
    lati: 47.8452003114, longi: 12.9750447514,
    name: "Freilassing",
    plz: "83395"
  },
  {
    lati: 49.2398007949, longi: 9.32364845156,
    name: "Neuenstadt am Kocher",
    plz: "74196"
  },
  {
    lati: 48.9372999621, longi: 12.4777667594,
    name: "Aholfing",
    plz: "94345"
  },
  {
    lati: 49.1625958525, longi: 12.8147170397,
    name: "Blaibach",
    plz: "93476"
  },
  {
    lati: 51.8472527435, longi: 12.1801878068,
    name: "Dessau-Roßlau",
    plz: "06846"
  },
  {
    lati: 47.874911149, longi: 12.3280183394,
    name: "Rimsting",
    plz: "83253"
  },
  {
    lati: 52.5272503536, longi: 13.2194949645,
    name: "Berlin Spandau",
    plz: "13597"
  },
  {
    lati: 52.4798926751, longi: 13.4371046176,
    name: "Berlin Neukölln",
    plz: "12043"
  },
  {
    lati: 49.1949629552, longi: 8.09618053874,
    name: "Landau in der Pfalz",
    plz: "76829"
  },
  {
    lati: 50.3956793576, longi: 8.02533833953,
    name: "Limburg",
    plz: "65556"
  },
  {
    lati: 50.2831867309, longi: 8.09483486469,
    name: "Burgschwalbach",
    plz: "65558"
  },
  {
    lati: 50.8236412024, longi: 9.72956482652,
    name: "Hauneck",
    plz: "36282"
  },
  {
    lati: 53.0551614826, longi: 10.799175788,
    name: "Stoetze",
    plz: "29597"
  },
  {
    lati: 49.9589172529, longi: 7.6165992763,
    name: "Argenthal",
    plz: "55496"
  },
  {
    lati: 47.8541346274, longi: 7.63195719123,
    name: "Buggingen",
    plz: "79426"
  },
  {
    lati: 47.5885757897, longi: 7.7006392434,
    name: "Inzlingen",
    plz: "79594"
  },
  {
    lati: 49.5568522608, longi: 8.03801378887,
    name: "Eisenberg (Pfalz)",
    plz: "67304"
  },
  {
    lati: 52.0987452927, longi: 8.08419242345,
    name: "Bad Laer",
    plz: "49196"
  },
  {
    lati: 49.5317616326, longi: 8.12699959636,
    name: "Kindenheim",
    plz: "67271"
  },
  {
    lati: 51.7581345476, longi: 6.20695860313,
    name: "Bedburg-Hau",
    plz: "47551"
  },
  {
    lati: 51.444108347, longi: 7.31537780144,
    name: "Witten",
    plz: "58455"
  },
  {
    lati: 51.7335493854, longi: 8.74692027936,
    name: "Paderborn",
    plz: "33102"
  },
  {
    lati: 49.744040099, longi: 8.84286077437,
    name: "Fränkisch-Crumbach",
    plz: "64407"
  },
  {
    lati: 48.386968081, longi: 11.4150242664,
    name: "Weichs",
    plz: "85258"
  },
  {
    lati: 47.8509204182, longi: 12.363734263,
    name: "Prien a. Chiemsee, Herrenchiemssee",
    plz: "83209"
  },
  {
    lati: 53.5549157587, longi: 9.60367534086,
    name: "Grünendeich",
    plz: "21720"
  },
  {
    lati: 48.607598276, longi: 9.72081609372,
    name: "Deggingen",
    plz: "73326"
  },
  {
    lati: 53.624613872, longi: 9.83843871031,
    name: "Halstenbek",
    plz: "25469"
  },
  {
    lati: 48.3070662965, longi: 9.82991129029,
    name: "Oberdischingen",
    plz: "89610"
  },
  {
    lati: 49.4043458758, longi: 11.0946826239,
    name: "Nürnberg",
    plz: "90469"
  },
  {
    lati: 49.713892904, longi: 11.2629432998,
    name: "Egloffstein",
    plz: "91349"
  },
  {
    lati: 48.1009321465, longi: 11.3994282538,
    name: "Planegg/Krailling",
    plz: "82152"
  },
  {
    lati: 50.0462366195, longi: 10.506993334,
    name: "Haßfurt",
    plz: "97437"
  },
  {
    lati: 51.5860018214, longi: 9.44787491254,
    name: "Trendelburg",
    plz: "34388"
  },
  {
    lati: 50.7235369435, longi: 7.08931638444,
    name: "Bonn",
    plz: "53115"
  },
  {
    lati: 51.520871429, longi: 6.88123662592,
    name: "Oberhausen",
    plz: "46119"
  },
  {
    lati: 52.2209751544, longi: 8.41973306634,
    name: "Melle",
    plz: "49328"
  },
  {
    lati: 49.4555001459, longi: 9.07888374321,
    name: "Waldbrunn",
    plz: "69429"
  },
  {
    lati: 48.1611918341, longi: 12.3736285247,
    name: "Jettenbach",
    plz: "84555"
  },
  {
    lati: 51.3878586014, longi: 9.81951385632,
    name: "Witzenhausen",
    plz: "37218"
  },
  {
    lati: 53.5466011559, longi: 10.0197875557,
    name: "Hamburg",
    plz: "20097"
  },
  {
    lati: 50.3803374547, longi: 11.7801720495,
    name: "Berg",
    plz: "95180"
  },
  {
    lati: 48.4766511567, longi: 11.9306818611,
    name: "Moosburg a.d. Isar",
    plz: "85368"
  },
  {
    lati: 53.9213174767, longi: 8.50053955679,
    name: "Neuwerk",
    plz: "27499"
  },
  {
    lati: 47.77191549, longi: 9.54208665169,
    name: "Ravensburg",
    plz: "88213"
  },
  {
    lati: 51.4510590822, longi: 6.47407529317,
    name: "Rheurdt",
    plz: "47509"
  },
  {
    lati: 50.4082878963, longi: 6.6883167836,
    name: "Blankenheim",
    plz: "53945"
  },
  {
    lati: 52.5436633372, longi: 13.1823400337,
    name: "Berlin Spandau",
    plz: "13583"
  },
  {
    lati: 48.6358134433, longi: 13.3699528119,
    name: "Tiefenbach",
    plz: "94113"
  },
  {
    lati: 52.4310633464, longi: 13.3918344311,
    name: "Berlin Mariendorf",
    plz: "12107"
  },
  {
    lati: 48.295135936, longi: 9.47139876372,
    name: "Hayingen",
    plz: "72534"
  },
  {
    lati: 50.0252408028, longi: 11.4973579539,
    name: "Neudrossenfeld",
    plz: "95512"
  },
  {
    lati: 48.1062760569, longi: 10.5558754846,
    name: "Tussenhausen",
    plz: "86874"
  },
  {
    lati: 51.164886288, longi: 6.44623021925,
    name: "Mönchengladbach",
    plz: "41236"
  },
  {
    lati: 51.5560055834, longi: 6.61001848318,
    name: "Rheinberg",
    plz: "47495"
  },
  {
    lati: 53.4985045614, longi: 8.56831607989,
    name: "Bremerhaven",
    plz: "27572"
  },
  {
    lati: 52.4644571325, longi: 13.4021859811,
    name: "Berlin Tempelhof",
    plz: "12099"
  },
  {
    lati: 50.5008844779, longi: 12.5192235019,
    name: "Schönheide",
    plz: "08304"
  },
  {
    lati: 51.1718685389, longi: 12.5475515052,
    name: "Kitzscher",
    plz: "04567"
  },
  {
    lati: 47.6621155259, longi: 12.9441067718,
    name: "Bischofswiesen",
    plz: "83483"
  },
  {
    lati: 49.8880566282, longi: 7.24807554352,
    name: "Dickenschied u.a.",
    plz: "55483"
  },
  {
    lati: 50.9541858294, longi: 6.91041790268,
    name: "Köln",
    plz: "50825"
  },
  {
    lati: 48.740728696, longi: 9.30829562746,
    name: "Esslingen am Neckar",
    plz: "73728"
  },
  {
    lati: 49.6995055107, longi: 10.3622781734,
    name: "Markt Bibart",
    plz: "91477"
  },
  {
    lati: 48.3217699536, longi: 10.8305598471,
    name: "Augsburg",
    plz: "86199"
  },
  {
    lati: 52.0084131999, longi: 10.8906128205,
    name: "Badersleben u.a.",
    plz: "38836"
  },
  {
    lati: 49.6219549656, longi: 11.3443610284,
    name: "Simmelsdorf",
    plz: "91245"
  },
  {
    lati: 48.0902395329, longi: 7.63612556956,
    name: "Vogtsburg im Kaiserstuhl",
    plz: "79235"
  },
  {
    lati: 50.1410097929, longi: 7.75589020526,
    name: "Sankt Goarshausen",
    plz: "56346"
  },
  {
    lati: 53.6606572031, longi: 9.31628910442,
    name: "Engelschoff",
    plz: "21710"
  },
  {
    lati: 53.4933631008, longi: 10.4268929985,
    name: "Brunstorf",
    plz: "21524"
  },
  {
    lati: 52.9257399384, longi: 11.1106260557,
    name: "Wustrow",
    plz: "29462"
  },
  {
    lati: 49.9711347872, longi: 8.05897643707,
    name: "Ingelheim am Rhein",
    plz: "55218"
  },
  {
    lati: 52.0853235964, longi: 7.19919984236,
    name: "Schöppingen",
    plz: "48624"
  },
  {
    lati: 52.9596440753, longi: 7.44627940704,
    name: "Neubörger, Neulehe",
    plz: "26909"
  },
  {
    lati: 49.5410815674, longi: 12.158989288,
    name: "Wernberg-Köblitz",
    plz: "92533"
  },
  {
    lati: 48.727419162, longi: 11.8245578325,
    name: "Train",
    plz: "93358"
  },
  {
    lati: 54.1768140395, longi: 10.9700354303,
    name: "Grömitz",
    plz: "23743"
  },
  {
    lati: 50.3641846408, longi: 7.56153777036,
    name: "Koblenz",
    plz: "56070"
  },
  {
    lati: 48.8259925189, longi: 8.57264923726,
    name: "Neuenbürg",
    plz: "75305"
  },
  {
    lati: 52.3279892199, longi: 9.104487641,
    name: "Hespe",
    plz: "31693"
  },
  {
    lati: 47.8274258004, longi: 7.96110274992,
    name: "Todtnau",
    plz: "79674"
  },
  {
    lati: 50.9254813871, longi: 7.99584748809,
    name: "Siegen",
    plz: "57078"
  },
  {
    lati: 50.3744858949, longi: 8.01343228885,
    name: "Diez, Hambach, Aull",
    plz: "65582"
  },
  {
    lati: 54.1912701501, longi: 10.5869256255,
    name: "Malente, Kirchnüchel",
    plz: "23714"
  },
  {
    lati: 51.1939878636, longi: 11.3757472313,
    name: "Rastenberg",
    plz: "99636"
  },
  {
    lati: 53.565488192, longi: 13.6938838493,
    name: "Uckerland, Groß Luckow, Schönhausen",
    plz: "17337"
  },
  {
    lati: 52.6319124143, longi: 14.2358094277,
    name: "Neutrebbin, Neuhardenberg",
    plz: "15320"
  },
  {
    lati: 47.7414211904, longi: 9.65978654295,
    name: "Grünkraut",
    plz: "88287"
  },
  {
    lati: 48.9413610088, longi: 9.23689441956,
    name: "Benningen am Neckar",
    plz: "71726"
  },
  {
    lati: 48.4826743952, longi: 9.2149401,
    name: "Reutlingen",
    plz: "72762"
  },
  {
    lati: 48.9676568819, longi: 9.33840404536,
    name: "Marbach am Neckar",
    plz: "71672"
  },
  {
    lati: 48.491427481, longi: 9.04364119392,
    name: "Tübingen",
    plz: "72072"
  },
  {
    lati: 53.7703190664, longi: 7.7300521789,
    name: "Spiekeroog",
    plz: "26474"
  },
  {
    lati: 50.594920703, longi: 8.52572156417,
    name: "Wetzlar",
    plz: "35584"
  },
  {
    lati: 53.8766267996, longi: 10.3751399319,
    name: "Seth",
    plz: "23845"
  },
  {
    lati: 53.6456229607, longi: 10.4723452933,
    name: "Linau",
    plz: "22959"
  },
  {
    lati: 53.4286953008, longi: 10.5394496403,
    name: "Gülzow",
    plz: "21483"
  },
  {
    lati: 49.0489792003, longi: 10.5111108673,
    name: "Gerolfingen",
    plz: "91726"
  },
  {
    lati: 48.9090170426, longi: 12.9864479706,
    name: "Grafling",
    plz: "94539"
  },
  {
    lati: 48.52064935, longi: 13.0577594157,
    name: "Egglham",
    plz: "84385"
  },
  {
    lati: 50.7874343483, longi: 12.7351170067,
    name: "Oberlungwitz",
    plz: "09353"
  },
  {
    lati: 52.536226572, longi: 9.88894673458,
    name: "Burgwedel",
    plz: "30938"
  },
  {
    lati: 47.604429585, longi: 9.890563432,
    name: "Lindenberg im Allgäu",
    plz: "88161"
  },
  {
    lati: 48.6672508776, longi: 10.1836526757,
    name: "Heidenheim an der Brenz",
    plz: "89522"
  },
  {
    lati: 54.3675162937, longi: 10.9731121628,
    name: "Heiligenhafen",
    plz: "23774"
  },
  {
    lati: 52.5310523384, longi: 13.1793658637,
    name: "Berlin Spandau",
    plz: "13581"
  },
  {
    lati: 52.4368717012, longi: 14.4722994874,
    name: "Zeschdorf, Podelzig, Lebus",
    plz: "15326"
  },
  {
    lati: 51.41994484, longi: 6.96327716377,
    name: "Essen",
    plz: "45149"
  },
  {
    lati: 53.3588744913, longi: 7.11649068421,
    name: "Emden",
    plz: "26723"
  },
  {
    lati: 51.7502157138, longi: 6.31136956028,
    name: "Kalkar",
    plz: "47546"
  },
  {
    lati: 50.562362263, longi: 8.67946198335,
    name: "Gießen",
    plz: "35392"
  },
  {
    lati: 53.2167216119, longi: 11.8958991337,
    name: "Perleberg, Berge u.a.",
    plz: "19348"
  },
  {
    lati: 48.0376934491, longi: 11.8730152931,
    name: "Moosach",
    plz: "85665"
  },
  {
    lati: 47.668332608, longi: 10.9437826229,
    name: "Wildsteig",
    plz: "82409"
  },
  {
    lati: 51.8712418914, longi: 10.5642421377,
    name: "Bad Harzburg, Torfhaus",
    plz: "38667"
  },
  {
    lati: 50.0152044583, longi: 12.3155824152,
    name: "Waldsassen",
    plz: "95652"
  },
  {
    lati: 49.9374843026, longi: 9.55247397524,
    name: "Neustadt a. Main",
    plz: "97845"
  },
  {
    lati: 50.8022125673, longi: 9.60233637897,
    name: "Niederaula",
    plz: "36272"
  },
  {
    lati: 55.0237766688, longi: 8.40153339386,
    name: "List",
    plz: "25992"
  },
  {
    lati: 52.1063843922, longi: 9.51651473584,
    name: "Coppenbrügge",
    plz: "31863"
  },
  {
    lati: 49.0368266298, longi: 12.545002105,
    name: "Wiesenfelden",
    plz: "94344"
  },
  {
    lati: 49.31455643, longi: 12.1911836743,
    name: "Wackersdorf",
    plz: "92442"
  },
  {
    lati: 50.270666719, longi: 10.480695602,
    name: "Bad Königshofen i. Grabfeld",
    plz: "97631"
  },
  {
    lati: 50.2585196428, longi: 7.64212058808,
    name: "Spay",
    plz: "56322"
  },
  {
    lati: 47.9607507044, longi: 7.73539701037,
    name: "Schallstadt",
    plz: "79227"
  },
  {
    lati: 53.6207773409, longi: 14.003517754,
    name: "Torgelow",
    plz: "17358"
  },
  {
    lati: 51.1277956898, longi: 8.07894667122,
    name: "Lennestadt",
    plz: "57368"
  },
  {
    lati: 49.4697604585, longi: 10.2137063728,
    name: "Ohrenbach",
    plz: "91620"
  },
  {
    lati: 50.6711370875, longi: 6.22747807716,
    name: "Roetgen",
    plz: "52159"
  },
  {
    lati: 51.3799144084, longi: 7.21689406049,
    name: "Hattingen",
    plz: "45527"
  },
  {
    lati: 53.3030183283, longi: 9.50677109865,
    name: "Sittensen u.a.",
    plz: "27419"
  },
  {
    lati: 48.8542624373, longi: 9.46902067793,
    name: "Berglen",
    plz: "73663"
  },
  {
    lati: 52.0670409986, longi: 9.60799814863,
    name: "Salzhemmendorf",
    plz: "31020"
  },
  {
    lati: 53.1959979768, longi: 8.57588922858,
    name: "Bremen",
    plz: "28779"
  },
  {
    lati: 50.4505468281, longi: 7.52651856444,
    name: "Neuwied",
    plz: "56566"
  },
  {
    lati: 50.7730989311, longi: 8.92490538456,
    name: "Amöneburg",
    plz: "35287"
  },
  {
    lati: 47.5281312198, longi: 10.0580716804,
    name: "Oberstaufen",
    plz: "87534"
  },
  {
    lati: 50.2524333894, longi: 10.244756922,
    name: "Münnerstadt",
    plz: "97702"
  },
  {
    lati: 51.0492211051, longi: 13.7894326147,
    name: "Dresden",
    plz: "01309"
  },
  {
    lati: 49.7044446812, longi: 12.4083991231,
    name: "Georgenberg",
    plz: "92697"
  },
  {
    lati: 48.7690843765, longi: 9.16761999273,
    name: "Stuttgart",
    plz: "70178"
  },
  {
    lati: 49.0409506027, longi: 9.20816499298,
    name: "Neckarwestheim",
    plz: "74382"
  },
  {
    lati: 50.5320959554, longi: 6.81576626052,
    name: "Bad Münstereifel",
    plz: "53902"
  },
  {
    lati: 52.2132157447, longi: 7.18246741507,
    name: "Ochtrup",
    plz: "48607"
  },
  {
    lati: 49.5870145378, longi: 8.19627325573,
    name: "Obrigheim (Pfalz)",
    plz: "67283"
  },
  {
    lati: 50.094637289, longi: 8.40892787115,
    name: "Hofheim am Taunus",
    plz: "65719"
  },
  {
    lati: 51.9780005881, longi: 8.50514023804,
    name: "Bielefeld",
    plz: "33647"
  },
  {
    lati: 49.9831678371, longi: 7.63335362729,
    name: "Ellern (Hunsrück), Schnorbach",
    plz: "55497"
  },
  {
    lati: 52.3251273846, longi: 7.7796012646,
    name: "Mettingen",
    plz: "49497"
  },
  {
    lati: 48.2940426772, longi: 7.79818566215,
    name: "Mahlberg",
    plz: "77972"
  },
  {
    lati: 52.4028186854, longi: 9.71998194024,
    name: "Hannover",
    plz: "30165"
  },
  {
    lati: 48.294645566, longi: 12.7142588943,
    name: "Reischach",
    plz: "84571"
  },
  {
    lati: 53.9522647289, longi: 10.7762761298,
    name: "Ratekau",
    plz: "23626"
  },
  {
    lati: 50.0137978444, longi: 10.9523456083,
    name: "Zapfendorf",
    plz: "96199"
  },
  {
    lati: 49.0008706448, longi: 11.2175728749,
    name: "Titting",
    plz: "85135"
  },
  {
    lati: 48.9268730309, longi: 10.5087757314,
    name: "Maihingen",
    plz: "86747"
  },
  {
    lati: 48.6184727018, longi: 13.766915833,
    name: "Wegscheid",
    plz: "94110"
  },
  {
    lati: 51.177185435, longi: 13.8214781966,
    name: "Ottendorf-Okrilla",
    plz: "01458"
  },
  {
    lati: 51.5969006802, longi: 11.9066105765,
    name: "Petersberg",
    plz: "06193"
  },
  {
    lati: 49.01926653, longi: 12.0600640297,
    name: "Regensburg",
    plz: "93049"
  },
  {
    lati: 48.7101955131, longi: 12.2992872868,
    name: "Bayerbach bei Ergoldsbach",
    plz: "84092"
  },
  {
    lati: 49.5008042703, longi: 10.624222625,
    name: "Markt Erlbach",
    plz: "91459"
  },
  {
    lati: 50.8598050276, longi: 10.6052545342,
    name: "Leinatal",
    plz: "99894"
  },
  {
    lati: 48.0606863711, longi: 11.3551023003,
    name: "Gauting",
    plz: "82131"
  },
  {
    lati: 49.3899172947, longi: 12.4600719647,
    name: "Thanstein",
    plz: "92554"
  },
  {
    lati: 51.3262083756, longi: 12.4669356047,
    name: "Leipzig",
    plz: "04316"
  },
  {
    lati: 49.6544406672, longi: 7.88918403726,
    name: "Steinbach, Weitersweiler, Bennhausen, Mörsfeld, Würzweiler, Ruppertsecke",
    plz: "67808"
  },
  {
    lati: 51.5682576538, longi: 9.74917572164,
    name: "Adelebsen",
    plz: "37139"
  },
  {
    lati: 53.5633937684, longi: 9.79893915346,
    name: "Hamburg",
    plz: "22587"
  },
  {
    lati: 49.9640370849, longi: 9.39815662926,
    name: "Rothenbuch, Rothenbucher Forst",
    plz: "63860"
  },
  {
    lati: 48.3796443536, longi: 10.009840937,
    name: "Neu-Ulm",
    plz: "89231"
  },
  {
    lati: 49.1973654694, longi: 12.3910394517,
    name: "Walderbach",
    plz: "93194"
  },
  {
    lati: 49.127834755, longi: 8.4076427178,
    name: "Linkenheim-Hochstetten",
    plz: "76351"
  },
  {
    lati: 49.4660332349, longi: 11.0762010707,
    name: "Nürnberg",
    plz: "90408"
  },
  {
    lati: 48.2607226633, longi: 11.1851231183,
    name: "Egenhofen",
    plz: "82281"
  },
  {
    lati: 47.8108321649, longi: 12.6779086277,
    name: "Siegsdorf",
    plz: "83313"
  },
  {
    lati: 53.8528416473, longi: 13.034844395,
    name: "Demmin u.a.",
    plz: "17111"
  },
  {
    lati: 50.5279218874, longi: 11.9934104068,
    name: "Rosenbach",
    plz: "08539"
  },
  {
    lati: 51.7238756165, longi: 12.47693395,
    name: "Gräfenhainichen",
    plz: "06773"
  },
  {
    lati: 52.5448409936, longi: 13.4125906804,
    name: "Berlin Prenzlauer Berg",
    plz: "10437"
  },
  {
    lati: 50.1352519644, longi: 10.8939367484,
    name: "Itzgrund",
    plz: "96274"
  },
  {
    lati: 50.5881515668, longi: 10.9975455511,
    name: "Großbreitenbach",
    plz: "98701"
  },
  {
    lati: 49.041842585, longi: 8.33528054148,
    name: "Karlsruhe",
    plz: "76187"
  },
  {
    lati: 49.2133964012, longi: 8.64257556005,
    name: "Bad Schönborn",
    plz: "76669"
  },
  {
    lati: 50.1837076235, longi: 8.80811637815,
    name: "Niederdorfelden",
    plz: "61138"
  },
  {
    lati: 48.4097627774, longi: 8.90015713384,
    name: "Hirrlingen",
    plz: "72145"
  },
  {
    lati: 49.0576833608, longi: 10.7781332699,
    name: "Dittenheim",
    plz: "91723"
  },
  {
    lati: 51.8268561785, longi: 10.4692086143,
    name: "Altenau, Schulenberg",
    plz: "38707"
  },
  {
    lati: 54.1162746814, longi: 13.7741687545,
    name: "Kröslin, Krummin, Lassan u.a.",
    plz: "17440"
  },
  {
    lati: 49.3845967095, longi: 8.98057758883,
    name: "Schwarzach",
    plz: "74869"
  },
  {
    lati: 52.2036221254, longi: 11.5925568609,
    name: "Barleben",
    plz: "39179"
  },
  {
    lati: 49.6089436463, longi: 11.9077923728,
    name: "Freihung",
    plz: "92271"
  },
  {
    lati: 51.4312940994, longi: 11.0883745882,
    name: "Kelbra (Kyffhäuser)",
    plz: "06537"
  },
  {
    lati: 52.8201511267, longi: 12.1463380492,
    name: "Havelberg",
    plz: "39539"
  },
  {
    lati: 47.9899886049, longi: 12.1897988188,
    name: "Griesstätt",
    plz: "83556"
  },
  {
    lati: 52.6909329044, longi: 10.7667863223,
    name: "Wittingen",
    plz: "29378"
  },
  {
    lati: 51.0920412933, longi: 10.7516361516,
    name: "Großvargula, Tonna",
    plz: "99958"
  },
  {
    lati: 50.4075857554, longi: 6.81257130218,
    name: "Antweiler, Aremberg, Dorsel, Eichenbach, Aremberg, Fuchshofen und Müsch",
    plz: "53533"
  },
  {
    lati: 48.0038154191, longi: 7.82580220736,
    name: "Freiburg im Breisgau",
    plz: "79110"
  },
  {
    lati: 50.8304838996, longi: 8.11515035504,
    name: "Wilnsdorf",
    plz: "57234"
  },
  {
    lati: 53.5286198658, longi: 8.62485252946,
    name: "Bremerhaven",
    plz: "27574"
  },
  {
    lati: 49.41723175, longi: 8.68757675725,
    name: "Heidelberg",
    plz: "69120"
  },
  {
    lati: 47.9364305371, longi: 10.0611157476,
    name: "Aitrach",
    plz: "88319"
  },
  {
    lati: 50.5978244294, longi: 12.5252363956,
    name: "Kirchberg",
    plz: "08107"
  },
  {
    lati: 51.6186432611, longi: 12.8448924505,
    name: "Dommitzsch",
    plz: "04880"
  },
  {
    lati: 50.8385764162, longi: 12.0596257317,
    name: "Gera",
    plz: "07549"
  },
  {
    lati: 51.6238299501, longi: 12.3321112117,
    name: "Bitterfeld-Wolfen",
    plz: "06749"
  },
  {
    lati: 53.5455773645, longi: 7.96405195336,
    name: "Schortens",
    plz: "26419"
  },
  {
    lati: 49.4542143695, longi: 11.6964534312,
    name: "Illschwang",
    plz: "92278"
  },
  {
    lati: 50.9536953115, longi: 11.778010901,
    name: "Bürgel u.a.",
    plz: "07616"
  },
  {
    lati: 49.1778752032, longi: 7.61000690347,
    name: "Pirmasens",
    plz: "66955"
  },
  {
    lati: 50.1247074278, longi: 7.78251597726,
    name: "Bornich, Patersberg",
    plz: "56348"
  },
  {
    lati: 50.7519256005, longi: 11.3457507991,
    name: "Rudolstadt",
    plz: "07407"
  },
  {
    lati: 53.8766141879, longi: 11.9246010181,
    name: "Bützow u.a.",
    plz: "18246"
  },
  {
    lati: 48.0454036355, longi: 11.5353295032,
    name: "Grünwald",
    plz: "82031"
  },
  {
    lati: 49.6136897622, longi: 7.1982971388,
    name: "Hoppstädten-Weiersbach",
    plz: "55768"
  },
  {
    lati: 51.4405150595, longi: 6.79545275754,
    name: "Duisburg",
    plz: "47058"
  },
  {
    lati: 47.6575576765, longi: 11.986981614,
    name: "Bayrischzell",
    plz: "83735"
  },
  {
    lati: 53.5381902293, longi: 9.53830502941,
    name: "Dollern",
    plz: "21739"
  },
  {
    lati: 51.2307866622, longi: 9.18551910052,
    name: "Naumburg",
    plz: "34311"
  },
  {
    lati: 50.7297932118, longi: 13.197167501,
    name: "Pockau-Lengefeld (Lengefeld)",
    plz: "09514"
  },
  {
    lati: 52.5891628748, longi: 13.3005480766,
    name: "Berlin Reinickendorf",
    plz: "13509"
  },
  {
    lati: 51.8383847798, longi: 14.1994173749,
    name: "Burg/Spreewald u.a.",
    plz: "03096"
  },
  {
    lati: 52.5116463228, longi: 14.3609819788,
    name: "Seelow, Lietzen u.a.",
    plz: "15306"
  },
  {
    lati: 51.384648782, longi: 14.3980865319,
    name: "Lohsa",
    plz: "02999"
  },
  {
    lati: 52.3365802731, longi: 14.5517958746,
    name: "Frankfurt/ Oder",
    plz: "15230"
  },
  {
    lati: 49.7875937677, longi: 7.76247784278,
    name: "Norheim u.a.",
    plz: "55585"
  },
  {
    lati: 48.9722444553, longi: 8.63488927848,
    name: "Königsbach-Stein",
    plz: "75203"
  },
  {
    lati: 52.5079877837, longi: 13.3029962939,
    name: "Berlin Charlottenburg",
    plz: "10627"
  },
  {
    lati: 48.2764069726, longi: 11.5597614291,
    name: "Unterschleißheim",
    plz: "85716"
  },
  {
    lati: 51.1070015642, longi: 6.75652479215,
    name: "Dormagen",
    plz: "41542"
  },
  {
    lati: 51.2122172054, longi: 6.8318885713,
    name: "Düsseldorf",
    plz: "40231"
  },
  {
    lati: 51.4659967487, longi: 7.13641527474,
    name: "Bochum",
    plz: "44867"
  },
  {
    lati: 51.7683118568, longi: 8.73068109703,
    name: "Paderborn",
    plz: "33104"
  },
  {
    lati: 48.1294405768, longi: 9.0727149625,
    name: "Stetten am kalten Markt",
    plz: "72510"
  },
  {
    lati: 48.5607450817, longi: 12.0897292036,
    name: "Landshut, Linden, Altdorf",
    plz: "84032"
  },
  {
    lati: 53.6569953417, longi: 12.3031178955,
    name: "Krakow, Dobbin-Linstow u.a.",
    plz: "18292"
  },
  {
    lati: 48.186839857, longi: 12.256216777,
    name: "Reichertsheim",
    plz: "84437"
  },
  {
    lati: 50.4946866432, longi: 11.7041920487,
    name: "Saalburg-Ebersdorf",
    plz: "07929"
  },
  {
    lati: 48.3104719144, longi: 11.7297403298,
    name: "Hallbergmoos",
    plz: "85399"
  },
  {
    lati: 50.6292082783, longi: 12.4625360027,
    name: "Hirschfeld",
    plz: "08144"
  },
  {
    lati: 48.7355830078, longi: 12.7521596445,
    name: "Wallersdorf",
    plz: "94522"
  },
  {
    lati: 52.9120089581, longi: 12.8023074342,
    name: "Neuruppin",
    plz: "16816"
  },
  {
    lati: 47.9493488737, longi: 12.8219065056,
    name: "Kirchanschöring",
    plz: "83417"
  },
  {
    lati: 49.854490588, longi: 6.56819047661,
    name: "Welschbillig, Igel, Aach",
    plz: "54298"
  },
  {
    lati: 51.3699253875, longi: 6.64128848723,
    name: "Krefeld",
    plz: "47829"
  },
  {
    lati: 49.8595738455, longi: 7.22564583303,
    name: "Monzelfeld, Hochscheid u.a.",
    plz: "54472"
  },
  {
    lati: 49.3930899114, longi: 7.43939716313,
    name: "Bruchmühlbach-Miesau",
    plz: "66892"
  },
  {
    lati: 50.6372396807, longi: 7.86762894011,
    name: "Alpenrod",
    plz: "57642"
  },
  {
    lati: 49.4273186942, longi: 8.1861912602,
    name: "Forst an der Weinstraße",
    plz: "67147"
  },
  {
    lati: 53.0597548406, longi: 9.48257796798,
    name: "Bothel, Kirchwalsede u.a.",
    plz: "27386"
  },
  {
    lati: 51.9822933704, longi: 8.7775118972,
    name: "Lage",
    plz: "32791"
  },
  {
    lati: 49.7689155532, longi: 8.88764278217,
    name: "Brensbach",
    plz: "64395"
  },
  {
    lati: 53.4112358833, longi: 9.6217780278,
    name: "Beckdorf",
    plz: "21643"
  },
  {
    lati: 52.5462644153, longi: 10.1984755715,
    name: "Eicklingen",
    plz: "29358"
  },
  {
    lati: 50.0284216568, longi: 10.367030311,
    name: "Gädheim",
    plz: "97503"
  },
  {
    lati: 49.2278817851, longi: 10.4814663782,
    name: "Herrieden",
    plz: "91567"
  },
  {
    lati: 51.2436396177, longi: 9.49263829528,
    name: "Fuldabrück",
    plz: "34277"
  },
  {
    lati: 50.8541874278, longi: 9.55426116025,
    name: "Kirchheim (Hessen)",
    plz: "36275"
  },
  {
    lati: 51.05418296, longi: 13.7702222162,
    name: "Dresden",
    plz: "01307"
  },
  {
    lati: 52.2027215741, longi: 13.926832328,
    name: "Storkow",
    plz: "15859"
  },
  {
    lati: 50.9369114311, longi: 14.6054260284,
    name: "Seifhennersdorf",
    plz: "02782"
  },
  {
    lati: 49.740049714, longi: 7.59361895965,
    name: "Kirn",
    plz: "55606"
  },
  {
    lati: 53.7525317488, longi: 10.4813013444,
    name: "Lasbek",
    plz: "23847"
  },
  {
    lati: 53.1393934067, longi: 10.4839733705,
    name: "Bienenbüttel",
    plz: "29553"
  },
  {
    lati: 48.0797618322, longi: 11.5566343407,
    name: "München",
    plz: "81545"
  },
  {
    lati: 50.7278648617, longi: 12.759545231,
    name: "Niederwürschnitz",
    plz: "09399"
  },
  {
    lati: 51.3284467531, longi: 12.0909375276,
    name: "Leuna",
    plz: "06237"
  },
  {
    lati: 49.1824709572, longi: 12.1009335969,
    name: "Maxhütte-Haidhof",
    plz: "93142"
  },
  {
    lati: 48.5918170547, longi: 12.3202133656,
    name: "Niederaichbach",
    plz: "84100"
  },
  {
    lati: 49.516838134, longi: 8.52861637584,
    name: "Mannheim",
    plz: "68309"
  },
  {
    lati: 48.7527822822, longi: 8.76851639473,
    name: "Simmozheim",
    plz: "75397"
  },
  {
    lati: 50.3947003719, longi: 10.1781019335,
    name: "Bastheim",
    plz: "97654"
  },
  {
    lati: 53.2812426595, longi: 10.1696035868,
    name: "Garstedt",
    plz: "21441"
  },
  {
    lati: 50.5387702776, longi: 9.01740574071,
    name: "Laubach",
    plz: "35321"
  },
  {
    lati: 50.8326045153, longi: 9.01844303484,
    name: "Stadtallendorf",
    plz: "35260"
  },
  {
    lati: 48.7869679379, longi: 9.12658746734,
    name: "Stuttgart",
    plz: "70195"
  },
  {
    lati: 52.2938385536, longi: 9.16202358065,
    name: "Nienstädt",
    plz: "31688"
  },
  {
    lati: 54.2435892044, longi: 9.81741278696,
    name: "Emkendorf",
    plz: "24802"
  },
  {
    lati: 49.2144148011, longi: 9.39663588028,
    name: "Langenbrettach",
    plz: "74243"
  },
  {
    lati: 48.4061735241, longi: 12.7507332038,
    name: "Eggenfelden",
    plz: "84307"
  },
  {
    lati: 48.6868572577, longi: 12.8775458168,
    name: "Wallerfing",
    plz: "94574"
  },
  {
    lati: 52.5371274388, longi: 13.6135477473,
    name: "Berlin Hellersdorf",
    plz: "12627"
  },
  {
    lati: 53.9247303817, longi: 9.25968492105,
    name: "Sankt Margarethen",
    plz: "25572"
  },
  {
    lati: 49.275509842, longi: 12.6077701433,
    name: "Pemfling",
    plz: "93482"
  },
  {
    lati: 47.8879239193, longi: 12.1916975273,
    name: "Prutting",
    plz: "83134"
  },
  {
    lati: 49.875344662, longi: 10.7727592775,
    name: "Walsdorf",
    plz: "96194"
  },
  {
    lati: 50.6868375313, longi: 9.3037056265,
    name: "Schwalmtal",
    plz: "36318"
  },
  {
    lati: 49.0091476694, longi: 9.28537194548,
    name: "Großbottwar",
    plz: "71723"
  },
  {
    lati: 54.0061665829, longi: 9.52206638823,
    name: "Hohenaspe",
    plz: "25582"
  },
  {
    lati: 48.4734352218, longi: 8.90695615585,
    name: "Rottenburg am Neckar",
    plz: "72108"
  },
  {
    lati: 53.0973065476, longi: 8.84181674668,
    name: "Bremen",
    plz: "28213"
  },
  {
    lati: 50.688988878, longi: 7.16295843129,
    name: "Bonn",
    plz: "53173"
  },
  {
    lati: 50.5506107163, longi: 7.65706125361,
    name: "Dierdorf",
    plz: "56269"
  },
  {
    lati: 50.6520226153, longi: 7.76078533097,
    name: "Hattert",
    plz: "57644"
  },
  {
    lati: 48.6787453288, longi: 8.87888993339,
    name: "Aidlingen",
    plz: "71134"
  },
  {
    lati: 48.488171687, longi: 9.21734956449,
    name: "Reutlingen",
    plz: "72764"
  },
  {
    lati: 51.4580025849, longi: 7.52431925003,
    name: "Dortmund",
    plz: "44267"
  },
  {
    lati: 50.3161009377, longi: 6.60334508396,
    name: "Lissendorf",
    plz: "54587"
  },
  {
    lati: 49.4423787379, longi: 12.5900170815,
    name: "Tiefenbach",
    plz: "93464"
  },
  {
    lati: 48.7812623346, longi: 12.6543375566,
    name: "Oberschneiding",
    plz: "94363"
  },
  {
    lati: 50.8469377832, longi: 12.9104552974,
    name: "Chemnitz",
    plz: "09113"
  },
  {
    lati: 48.0869462297, longi: 11.0262742549,
    name: "Eresing",
    plz: "86922"
  },
  {
    lati: 51.3533328315, longi: 12.2708264607,
    name: "Leipzig",
    plz: "04178"
  },
  {
    lati: 54.2788659458, longi: 13.1126404714,
    name: "Stralsund",
    plz: "18439"
  },
  {
    lati: 50.2211558646, longi: 6.41860202155,
    name: "Prüm",
    plz: "54595"
  },
  {
    lati: 51.5308104241, longi: 6.85836675819,
    name: "Oberhausen",
    plz: "46145"
  },
  {
    lati: 48.6146333152, longi: 8.15961698145,
    name: "Sasbachwalden",
    plz: "77887"
  },
  {
    lati: 49.8378857128, longi: 8.16496126916,
    name: "Armsheim",
    plz: "55288"
  },
  {
    lati: 49.8560886733, longi: 7.5345784359,
    name: "Seesbach",
    plz: "55629"
  },
  {
    lati: 51.4296052429, longi: 9.49547703387,
    name: "Immenhausen",
    plz: "34376"
  },
  {
    lati: 48.130884668, longi: 11.4853221416,
    name: "München",
    plz: "80689"
  },
  {
    lati: 51.9609221226, longi: 9.95285747807,
    name: "Freden (Leine)",
    plz: "31084"
  },
  {
    lati: 48.4566497082, longi: 10.8131921266,
    name: "Gablingen",
    plz: "86456"
  },
  {
    lati: 48.8596099039, longi: 12.2199702287,
    name: "Pfakofen",
    plz: "93101"
  },
  {
    lati: 51.1918407826, longi: 13.7308185642,
    name: "Radeburg",
    plz: "01471"
  },
  {
    lati: 47.9727501727, longi: 11.283750542,
    name: "Pöcking",
    plz: "82343"
  },
  {
    lati: 48.118047987, longi: 11.7328464344,
    name: "Haar",
    plz: "85540"
  },
  {
    lati: 49.4533734727, longi: 8.47964148493,
    name: "Mannheim",
    plz: "68199"
  },
  {
    lati: 49.0263111172, longi: 8.49566093436,
    name: "Karlsruhe",
    plz: "76229"
  },
  {
    lati: 53.6661050076, longi: 10.0037611292,
    name: "Hamburg",
    plz: "22419"
  },
  {
    lati: 49.378467337, longi: 10.2294459457,
    name: "Neusitz",
    plz: "91616"
  },
  {
    lati: 52.5349532637, longi: 10.7693363612,
    name: "Barwedel",
    plz: "38476"
  },
  {
    lati: 51.4872487085, longi: 11.9615768194,
    name: "Halle/ Saale",
    plz: "06108"
  },
  {
    lati: 49.0470955843, longi: 12.0102924012,
    name: "Pettendorf",
    plz: "93186"
  },
  {
    lati: 47.8422333952, longi: 12.5972376609,
    name: "Vachendorf",
    plz: "83377"
  },
  {
    lati: 51.41260245, longi: 14.5969693488,
    name: "Weißwasser, Boxberg",
    plz: "02943"
  },
  {
    lati: 52.4125889278, longi: 10.5935356607,
    name: "Wasbüttel",
    plz: "38553"
  },
  {
    lati: 48.3317643989, longi: 11.4582887236,
    name: "Röhrmoos",
    plz: "85244"
  },
  {
    lati: 51.1324705269, longi: 6.82993466728,
    name: "Dormagen",
    plz: "41541"
  },
  {
    lati: 49.3724973092, longi: 7.62146628476,
    name: "Mittelbrunn, Queidersbach u.a.",
    plz: "66851"
  },
  {
    lati: 51.0319018104, longi: 7.77077550561,
    name: "Drolshagen",
    plz: "57489"
  },
  {
    lati: 49.5330479509, longi: 8.35706569785,
    name: "Frankenthal (Pfalz)",
    plz: "67227"
  },
  {
    lati: 48.566471594, longi: 8.52968169989,
    name: "Grömbach",
    plz: "72294"
  },
  {
    lati: 50.1513072414, longi: 8.5324369315,
    name: "Schwalbach am Taunus",
    plz: "65824"
  },
  {
    lati: 50.4915839114, longi: 9.16036173409,
    name: "Schotten",
    plz: "63679"
  },
  {
    lati: 48.5906987618, longi: 9.16585512412,
    name: "Walddorfhäslach",
    plz: "72141"
  },
  {
    lati: 50.5753187075, longi: 12.8106721756,
    name: "Grünhain-Beierfeld",
    plz: "08344"
  },
  {
    lati: 49.5375157444, longi: 11.8115611106,
    name: "Hahnbach",
    plz: "92256"
  },
  {
    lati: 51.8449685535, longi: 12.5906483478,
    name: "Wittenberg",
    plz: "06888"
  },
  {
    lati: 52.4066200233, longi: 12.961994669,
    name: "Potsdam",
    plz: "14476"
  },
  {
    lati: 48.2680699589, longi: 11.4398733747,
    name: "Dachau",
    plz: "85221"
  },
  {
    lati: 50.6436695032, longi: 8.3251137869,
    name: "Sinn",
    plz: "35764"
  },
  {
    lati: 49.5204568465, longi: 10.8832061046,
    name: "Veitsbronn",
    plz: "90587"
  },
  {
    lati: 49.5874883771, longi: 10.9539553458,
    name: "Erlangen",
    plz: "91056"
  },
  {
    lati: 50.8228412888, longi: 7.80063074054,
    name: "Katzwinkel (Sieg)",
    plz: "57581"
  },
  {
    lati: 50.30358697, longi: 7.94286391581,
    name: "Schönborn",
    plz: "56370"
  },
  {
    lati: 50.7151954026, longi: 6.14988367979,
    name: "Aachen",
    plz: "52076"
  },
  {
    lati: 49.3092451621, longi: 6.94183167732,
    name: "Riegelsberg",
    plz: "66292"
  },
  {
    lati: 51.1783229694, longi: 7.27140258046,
    name: "Remscheid",
    plz: "42897"
  },
  {
    lati: 53.6110379827, longi: 9.07717779004,
    name: "Lamstedt",
    plz: "21769"
  },
  {
    lati: 53.4898832935, longi: 9.31515403645,
    name: "Kutenholz",
    plz: "27449"
  },
  {
    lati: 48.9047202363, longi: 9.46975945464,
    name: "Allmersbach im Tal",
    plz: "71573"
  },
  {
    lati: 47.9097847741, longi: 8.27675049461,
    name: "Friedenweiler",
    plz: "79877"
  },
  {
    lati: 48.7757925323, longi: 8.40392318562,
    name: "Loffenau",
    plz: "76597"
  },
  {
    lati: 49.3498192759, longi: 11.2676244794,
    name: "Schwarzenbruck",
    plz: "90592"
  },
  {
    lati: 50.4124582371, longi: 10.8094369643,
    name: "Veilsdorf",
    plz: "98669"
  },
  {
    lati: 47.6536818601, longi: 10.1484171526,
    name: "Weitnau",
    plz: "87480"
  },
  {
    lati: 49.6073715263, longi: 10.4122665435,
    name: "Sugenheim",
    plz: "91484"
  },
  {
    lati: 50.3210744422, longi: 11.7547973084,
    name: "Selbitz",
    plz: "95152"
  },
  {
    lati: 51.7269067704, longi: 11.8916789721,
    name: "Südliches Anhalt u.a.",
    plz: "06369"
  },
  {
    lati: 48.2072376644, longi: 12.7625639479,
    name: "Emmerting",
    plz: "84547"
  },
  {
    lati: 50.7438289574, longi: 13.0057646668,
    name: "Amtsberg",
    plz: "09439"
  },
  {
    lati: 48.6604454576, longi: 13.408067038,
    name: "Ruderting",
    plz: "94161"
  },
  {
    lati: 50.2396258591, longi: 11.6940595961,
    name: "Helmbrechts",
    plz: "95233"
  },
  {
    lati: 48.1815565052, longi: 10.5967826029,
    name: "Mittelneufnach",
    plz: "86868"
  },
  {
    lati: 52.6270091449, longi: 10.2477232287,
    name: "Lachendorf",
    plz: "29331"
  },
  {
    lati: 50.1040403753, longi: 10.4395750987,
    name: "Riedbach",
    plz: "97519"
  },
  {
    lati: 47.8602265922, longi: 9.36915946691,
    name: "Illmensee",
    plz: "88636"
  },
  {
    lati: 52.2358367023, longi: 8.49730878884,
    name: "Rödinghausen",
    plz: "32289"
  },
  {
    lati: 51.9853773237, longi: 8.6221581462,
    name: "Bielefeld",
    plz: "33699"
  },
  {
    lati: 48.9103778107, longi: 12.0522934026,
    name: "Bad Abbach",
    plz: "93077"
  },
  {
    lati: 48.7332165713, longi: 13.2214866782,
    name: "Außernzell",
    plz: "94532"
  },
  {
    lati: 54.3996867052, longi: 13.2134277946,
    name: "Samtens",
    plz: "18573"
  },
  {
    lati: 52.5307453195, longi: 13.3371221193,
    name: "Berlin Moabit",
    plz: "10551"
  },
  {
    lati: 48.2631516279, longi: 9.78637457949,
    name: "Griesingen",
    plz: "89608"
  },
  {
    lati: 51.0927392293, longi: 12.1198646573,
    name: "Zeitz",
    plz: "06711"
  },
  {
    lati: 49.9922459186, longi: 8.66166270882,
    name: "Langen",
    plz: "63225"
  },
  {
    lati: 49.1114123141, longi: 8.29203007714,
    name: "Rheinzabern",
    plz: "76764"
  },
  {
    lati: 51.3158482659, longi: 9.49172449645,
    name: "Kassel",
    plz: "34117"
  },
  {
    lati: 48.3215119818, longi: 8.24746061196,
    name: "Wolfach, Oberwolfach",
    plz: "77709"
  },
  {
    lati: 51.1532419391, longi: 6.71901454773,
    name: "Neuss",
    plz: "41469"
  },
  {
    lati: 49.5522969791, longi: 7.63231511351,
    name: "Kreimbach-Kaulbach",
    plz: "67757"
  },
  {
    lati: 52.5245663499, longi: 13.482076657,
    name: "Berlin Lichtenberg",
    plz: "10367"
  },
  {
    lati: 51.762475797, longi: 14.3765621277,
    name: "Cottbus",
    plz: "03042"
  },
  {
    lati: 48.2217026163, longi: 9.56714963193,
    name: "Obermarchtal",
    plz: "89611"
  },
  {
    lati: 50.8983292015, longi: 11.2632177685,
    name: "Bad Berka u.a.",
    plz: "99438"
  },
  {
    lati: 49.5746766298, longi: 8.11529862794,
    name: "Ebertsheim",
    plz: "67280"
  },
  {
    lati: 49.0923024499, longi: 10.6190128038,
    name: "Unterschwaningen",
    plz: "91743"
  },
  {
    lati: 53.3817706224, longi: 10.4148846181,
    name: "Tespe",
    plz: "21395"
  },
  {
    lati: 49.1990649712, longi: 13.0532271995,
    name: "Lam",
    plz: "93462"
  },
  {
    lati: 51.7379399112, longi: 13.2199793067,
    name: "Herzberg/ Elster",
    plz: "04916"
  },
  {
    lati: 51.4917851314, longi: 7.08530556179,
    name: "Gelsenkirchen Rotthausen",
    plz: "45884"
  },
  {
    lati: 52.5565028037, longi: 13.5646206743,
    name: "Berlin",
    plz: "12687"
  },
  {
    lati: 52.5601751554, longi: 13.4717618743,
    name: "Berlin Weißensee",
    plz: "13088"
  },
  {
    lati: 54.4007974935, longi: 9.19954343354,
    name: "Schwabstedt",
    plz: "25876"
  },
  {
    lati: 54.4779474461, longi: 9.40786532391,
    name: "Ellingstedt",
    plz: "24870"
  },
  {
    lati: 48.3927619035, longi: 9.96907725842,
    name: "Ulm",
    plz: "89077"
  },
  {
    lati: 49.7510963395, longi: 10.0059220113,
    name: "Randersacker",
    plz: "97236"
  },
  {
    lati: 53.5795043591, longi: 9.96741485885,
    name: "Hamburg",
    plz: "20253"
  },
  {
    lati: 54.3411922772, longi: 10.1184280494,
    name: "Kiel",
    plz: "24118"
  },
  {
    lati: 53.0237206985, longi: 10.4169431068,
    name: "Ebstorf",
    plz: "29574"
  },
  {
    lati: 49.8025274289, longi: 8.1040548305,
    name: "Alzey",
    plz: "55232"
  },
  {
    lati: 48.067661794, longi: 8.43400434426,
    name: "Villingen-Schwenningen",
    plz: "78052"
  },
  {
    lati: 48.1801887086, longi: 8.41502641168,
    name: "Hardt",
    plz: "78739"
  },
  {
    lati: 51.1798837545, longi: 6.87051726851,
    name: "Düsseldorf",
    plz: "40599"
  },
  {
    lati: 47.741400902, longi: 9.60867120116,
    name: "Ravensburg",
    plz: "88214"
  },
  {
    lati: 53.8271275276, longi: 9.52199531797,
    name: "Krempe, Grevenkop, Süderau, Muchelndorf",
    plz: "25361"
  },
  {
    lati: 51.3428263696, longi: 9.65519442652,
    name: "Staufenberg",
    plz: "34355"
  },
  {
    lati: 47.7639029089, longi: 11.124969598,
    name: "Huglfing",
    plz: "82386"
  },
  {
    lati: 49.0596716617, longi: 11.3283072805,
    name: "Greding",
    plz: "91171"
  },
  {
    lati: 50.9386754278, longi: 11.4319586727,
    name: "Lehnstedt u.a.",
    plz: "99441"
  },
  {
    lati: 52.4668760032, longi: 13.4298002755,
    name: "Berlin Neukölln",
    plz: "12051"
  },
  {
    lati: 50.4224088352, longi: 7.24202453689,
    name: "Wehr",
    plz: "56653"
  },
  {
    lati: 48.909053605, longi: 8.19632244904,
    name: "Steinmauern",
    plz: "76479"
  },
  {
    lati: 49.7805107103, longi: 7.62327681314,
    name: "Sobernheim",
    plz: "55566"
  },
  {
    lati: 47.6502662246, longi: 7.60205128523,
    name: "Fischingen",
    plz: "79592"
  },
  {
    lati: 52.6207325584, longi: 8.69216174102,
    name: "Freistatt, Varrel, Wehrbleck",
    plz: "27259"
  },
  {
    lati: 47.8953018303, longi: 8.50504377941,
    name: "Hüfingen",
    plz: "78183"
  },
  {
    lati: 49.2129411375, longi: 11.5393760754,
    name: "Deining",
    plz: "92364"
  },
  {
    lati: 49.3219930834, longi: 7.02874797595,
    name: "Quierschied",
    plz: "66287"
  },
  {
    lati: 51.1562608658, longi: 6.94319086135,
    name: "Hilden",
    plz: "40723"
  },
  {
    lati: 49.5409126871, longi: 7.44169071159,
    name: "Rammelsbach u.a.",
    plz: "66887"
  },
  {
    lati: 49.537176785, longi: 7.65287517796,
    name: "Olsbrücken",
    plz: "67737"
  },
  {
    lati: 48.357153661, longi: 8.53004801817,
    name: "Dornhan",
    plz: "72175"
  },
  {
    lati: 47.9055392784, longi: 7.96075558085,
    name: "Oberried",
    plz: "79254"
  },
  {
    lati: 52.0263230721, longi: 10.548528948,
    name: "Schladen-Werla",
    plz: "38315"
  },
  {
    lati: 50.3724120511, longi: 11.5251309705,
    name: "Nordhalben",
    plz: "96365"
  },
  {
    lati: 48.2832619214, longi: 10.6509245413,
    name: "Fischach",
    plz: "86850"
  },
  {
    lati: 48.2405231738, longi: 11.1466548492,
    name: "Oberschweinbach",
    plz: "82294"
  },
  {
    lati: 51.065823683, longi: 13.6212744223,
    name: "Dresden",
    plz: "01156"
  },
  {
    lati: 50.0369684145, longi: 9.26606167827,
    name: "Sailauf",
    plz: "63877"
  },
  {
    lati: 50.4694583111, longi: 12.6100787262,
    name: "Eibenstock",
    plz: "08309"
  },
  {
    lati: 50.6963465083, longi: 12.7710507765,
    name: "Stollberg/Erzgeb.",
    plz: "09366"
  },
  {
    lati: 52.42948394, longi: 9.3244742895,
    name: "Hagenburg",
    plz: "31558"
  },
  {
    lati: 47.8677555911, longi: 9.55468195062,
    name: "Fronreute",
    plz: "88273"
  },
  {
    lati: 50.5115443267, longi: 8.1130609709,
    name: "Waldbrunn",
    plz: "65620"
  },
  {
    lati: 49.2252980428, longi: 7.750016352,
    name: "Hinterweidenthal",
    plz: "66999"
  },
  {
    lati: 48.9050089533, longi: 8.50337288033,
    name: "Karlsbad",
    plz: "76307"
  },
  {
    lati: 49.2113193744, longi: 7.00166633677,
    name: "Saarbrücken",
    plz: "66119"
  },
  {
    lati: 51.1427870828, longi: 6.24950208158,
    name: "Wegberg",
    plz: "41844"
  },
  {
    lati: 53.2801389978, longi: 10.2998414574,
    name: "Mechtersen",
    plz: "21358"
  },
  {
    lati: 53.0445800574, longi: 10.5268984149,
    name: "Barum",
    plz: "29576"
  },
  {
    lati: 50.2763424094, longi: 11.7420897103,
    name: "Schauenstein",
    plz: "95197"
  },
  {
    lati: 48.3396909846, longi: 12.1363025941,
    name: "Taufkirchen (Vils)",
    plz: "84416"
  },
  {
    lati: 51.4194384095, longi: 12.4352147026,
    name: "Leipzig",
    plz: "04356"
  },
  {
    lati: 49.807170963, longi: 9.85930349681,
    name: "Zell a. Main",
    plz: "97299"
  },
  {
    lati: 53.6274155528, longi: 9.93142137017,
    name: "Hamburg",
    plz: "22457"
  },
  {
    lati: 48.0193512548, longi: 11.0695483979,
    name: "Utting a. Ammersee",
    plz: "86919"
  },
  {
    lati: 52.5726016319, longi: 13.3509122827,
    name: "Berlin-West",
    plz: "13407"
  },
  {
    lati: 53.3855406879, longi: 14.3263599029,
    name: "Blankensee, Grambow u.a.",
    plz: "17322"
  },
  {
    lati: 51.7320156334, longi: 14.3399486975,
    name: "Cottbus",
    plz: "03050"
  },
  {
    lati: 49.6802616542, longi: 7.87430965109,
    name: "Schwarzengraben, St. Alban, Gerbach",
    plz: "67813"
  },
  {
    lati: 49.8818338005, longi: 8.08755905128,
    name: "Armsheim",
    plz: "55288"
  },
  {
    lati: 49.1855649253, longi: 7.36441907064,
    name: "Hornbach",
    plz: "66500"
  },
  {
    lati: 50.6176679245, longi: 8.78990914564,
    name: "Buseck",
    plz: "35418"
  },
  {
    lati: 53.1025999877, longi: 8.86309829963,
    name: "Bremen",
    plz: "28359"
  },
  {
    lati: 48.1150959226, longi: 8.86436383737,
    name: "Egesheim",
    plz: "78592"
  },
  {
    lati: 50.4335642397, longi: 11.1610181557,
    name: "Steinach",
    plz: "96523"
  },
  {
    lati: 53.208327767, longi: 10.8244426499,
    name: "Tosterglope",
    plz: "21371"
  },
  {
    lati: 48.4288657947, longi: 10.3831575479,
    name: "Burgau",
    plz: "89331"
  },
  {
    lati: 48.7304678579, longi: 9.10489864043,
    name: "Stuttgart",
    plz: "70563"
  },
  {
    lati: 48.177710628, longi: 9.21184783784,
    name: "Veringenstadt",
    plz: "72519"
  },
  {
    lati: 48.45002117, longi: 13.4114261749,
    name: "Neuhaus a. Inn",
    plz: "94152"
  },
  {
    lati: 49.8632855437, longi: 10.6730170871,
    name: "Schönbrunn i. Steigerwald",
    plz: "96185"
  },
  {
    lati: 53.6200921378, longi: 11.5511750932,
    name: "Pinnow",
    plz: "19065"
  },
  {
    lati: 51.6865464836, longi: 11.7486350787,
    name: "Könnern",
    plz: "06420"
  },
  {
    lati: 53.3205091994, longi: 9.85918407022,
    name: "Buchholz in der Nordheide",
    plz: "21244"
  },
  {
    lati: 51.6291016043, longi: 9.44659353918,
    name: "Bad Karlshafen",
    plz: "34385"
  },
  {
    lati: 53.7277114558, longi: 10.0002168826,
    name: "Norderstedt",
    plz: "22844"
  },
  {
    lati: 50.4076281038, longi: 8.82644064585,
    name: "Wölfersheim",
    plz: "61200"
  },
  {
    lati: 49.4620502777, longi: 8.29301344181,
    name: "Fußgönheim",
    plz: "67136"
  },
  {
    lati: 49.6390267248, longi: 6.55504649768,
    name: "Ayl, Trassem u.a.",
    plz: "54441"
  },
  {
    lati: 50.6854552503, longi: 6.9211932072,
    name: "Swisttal",
    plz: "53913"
  },
  {
    lati: 51.419947551, longi: 7.11367430865,
    name: "Essen",
    plz: "45289"
  },
  {
    lati: 51.9342833296, longi: 7.6261590577,
    name: "Münster",
    plz: "48153"
  },
  {
    lati: 52.8425716502, longi: 9.26819340347,
    name: "Dörverden",
    plz: "27313"
  },
  {
    lati: 49.4920560526, longi: 9.54607859967,
    name: "Ahorn",
    plz: "74744"
  },
  {
    lati: 50.6501663825, longi: 7.95145756658,
    name: "Bad Marienberg (Westerwald)",
    plz: "56470"
  },
  {
    lati: 49.4688918219, longi: 7.74456322981,
    name: "Kaiserslautern",
    plz: "67659"
  },
  {
    lati: 48.7489246829, longi: 9.82536861405,
    name: "Waldstetten",
    plz: "73550"
  },
  {
    lati: 53.8270284861, longi: 10.7186587925,
    name: "Lübeck",
    plz: "23562"
  },
  {
    lati: 48.0652852066, longi: 10.2852661617,
    name: "Lauben",
    plz: "87761"
  },
  {
    lati: 53.7445803984, longi: 10.3159719598,
    name: "Tremsbüttel",
    plz: "22967"
  },
  {
    lati: 50.4873949949, longi: 7.15241716621,
    name: "Schalkenbach, Königsfeld, Dedenbach",
    plz: "53426"
  },
  {
    lati: 49.92701682, longi: 7.20041655647,
    name: "Irmenach",
    plz: "56843"
  },
  {
    lati: 50.2207625077, longi: 7.26296035172,
    name: "Binningen",
    plz: "56754"
  },
  {
    lati: 47.8125478601, longi: 11.4665617543,
    name: "Königsdorf",
    plz: "82549"
  },
  {
    lati: 50.7789134359, longi: 6.44827552259,
    name: "Düren",
    plz: "52355"
  },
  {
    lati: 48.3322283958, longi: 8.01086368598,
    name: "Biberach",
    plz: "77781"
  },
  {
    lati: 50.9989729677, longi: 7.56183919388,
    name: "Gummersbach",
    plz: "51645"
  },
  {
    lati: 47.7708710637, longi: 7.58234727578,
    name: "Neuenburg am Rhein",
    plz: "79395"
  },
  {
    lati: 50.0692455423, longi: 8.94438094247,
    name: "Hainburg",
    plz: "63512"
  },
  {
    lati: 50.1196876813, longi: 9.19471829694,
    name: "Geiselbach",
    plz: "63826"
  },
  {
    lati: 52.433012829, longi: 9.79060590405,
    name: "Hannover",
    plz: "30657"
  },
  {
    lati: 48.0364168347, longi: 11.2238881973,
    name: "Seefeld",
    plz: "82229"
  },
  {
    lati: 53.5519535199, longi: 10.9317302703,
    name: "Zarrentin",
    plz: "19246"
  },
  {
    lati: 47.801011477, longi: 10.9351728042,
    name: "Peiting",
    plz: "86971"
  },
  {
    lati: 48.054642033, longi: 10.4957402977,
    name: "Mindelheim",
    plz: "87719"
  },
  {
    lati: 50.3971523616, longi: 7.1716313576,
    name: "Bell",
    plz: "56745"
  },
  {
    lati: 49.1874648199, longi: 7.15152198757,
    name: "Mandelbachtal",
    plz: "66399"
  },
  {
    lati: 50.4127830272, longi: 7.32300094672,
    name: "Nickenich",
    plz: "56645"
  },
  {
    lati: 51.3406983547, longi: 6.3503216977,
    name: "Grefrath",
    plz: "47929"
  },
  {
    lati: 51.4003508934, longi: 6.65557455704,
    name: "Duisburg",
    plz: "47239"
  },
  {
    lati: 51.3296558948, longi: 8.48837338656,
    name: "Olsberg",
    plz: "59939"
  },
  {
    lati: 48.5870560591, longi: 9.6909068541,
    name: "Bad Ditzenbach",
    plz: "73342"
  },
  {
    lati: 49.8035157564, longi: 9.81436932208,
    name: "Hettstadt",
    plz: "97265"
  },
  {
    lati: 53.5848140957, longi: 9.91189293635,
    name: "Hamburg",
    plz: "22525"
  },
  {
    lati: 48.3234450523, longi: 10.00718681,
    name: "Illerkirchberg",
    plz: "89171"
  },
  {
    lati: 53.502465272, longi: 10.1600393809,
    name: "Hamburg",
    plz: "21033"
  },
  {
    lati: 49.426699779, longi: 10.2104156771,
    name: "Steinsfeld",
    plz: "91628"
  },
  {
    lati: 48.8779920866, longi: 10.7091274653,
    name: "Wemding",
    plz: "86650"
  },
  {
    lati: 48.4071813286, longi: 10.7666188059,
    name: "Aystetten",
    plz: "86482"
  },
  {
    lati: 47.4546249185, longi: 11.0068217727,
    name: "Grainau",
    plz: "82491"
  },
  {
    lati: 48.9629388965, longi: 9.01506642192,
    name: "Sersheim",
    plz: "74372"
  },
  {
    lati: 50.2819649493, longi: 12.2499459968,
    name: "Bad Elster",
    plz: "08645"
  },
  {
    lati: 52.4473413301, longi: 13.4531706264,
    name: "Berlin Britz",
    plz: "12359"
  },
  {
    lati: 54.6154482204, longi: 9.97668031639,
    name: "Dörphof",
    plz: "24398"
  },
  {
    lati: 53.588676225, longi: 10.0502363227,
    name: "Hamburg",
    plz: "22305"
  },
  {
    lati: 51.9945227438, longi: 11.0176746316,
    name: "Huy",
    plz: "38838"
  },
  {
    lati: 51.1680409924, longi: 14.3155269056,
    name: "Göda",
    plz: "02633"
  },
  {
    lati: 50.944667855, longi: 7.07001867981,
    name: "Köln",
    plz: "51109"
  },
  {
    lati: 50.1637008011, longi: 8.98291125104,
    name: "Erlensee",
    plz: "63526"
  },
  {
    lati: 49.0178162118, longi: 9.08859963944,
    name: "Erligheim",
    plz: "74391"
  },
  {
    lati: 47.6940028841, longi: 9.30269418898,
    name: "Stetten",
    plz: "88719"
  },
  {
    lati: 51.0521338966, longi: 12.4550340331,
    name: "Rositz, Starkenberg, Treben",
    plz: "04617"
  },
  {
    lati: 53.8116867436, longi: 11.9058004644,
    name: "Bernitt, Qualitz, Warnow, Zernin u.a.",
    plz: "18249"
  },
  {
    lati: 54.428079383, longi: 13.1194957639,
    name: "Gingst",
    plz: "18569"
  },
  {
    lati: 54.5080736031, longi: 13.162249254,
    name: "Gingst",
    plz: "18569"
  },
  {
    lati: 50.8750096178, longi: 8.06612840098,
    name: "Siegen",
    plz: "57074"
  },
  {
    lati: 48.5909497067, longi: 9.2156318325,
    name: "Altenriet",
    plz: "72657"
  },
  {
    lati: 49.5950537403, longi: 8.64678308608,
    name: "Hemsbach",
    plz: "69502"
  },
  {
    lati: 51.3334500247, longi: 10.1536999883,
    name: "Heiligenstadt",
    plz: "37308"
  },
  {
    lati: 47.8560939674, longi: 10.142701735,
    name: "Legau",
    plz: "87764"
  },
  {
    lati: 52.3623295618, longi: 9.71410490039,
    name: "Hannover",
    plz: "30449"
  },
  {
    lati: 53.0809092242, longi: 11.8459179834,
    name: "Perleberg, Berge u.a.",
    plz: "19348"
  },
  {
    lati: 49.6460027216, longi: 6.48793849585,
    name: "Tawern",
    plz: "54456"
  },
  {
    lati: 49.8271988109, longi: 6.8527148723,
    name: "Leiwen u.a.",
    plz: "54340"
  },
  {
    lati: 53.5811948146, longi: 7.17951608394,
    name: "Norden",
    plz: "26506"
  },
  {
    lati: 49.7222970687, longi: 6.5447844616,
    name: "Welschbillig, Igel, Aach",
    plz: "54298"
  },
  {
    lati: 48.9683422412, longi: 8.71115773933,
    name: "Neulingen",
    plz: "75245"
  },
  {
    lati: 48.2612747672, longi: 12.4028165445,
    name: "Ampfing",
    plz: "84539"
  },
  {
    lati: 50.8136240363, longi: 12.7896899947,
    name: "Chemnitz",
    plz: "09224"
  },
  {
    lati: 48.6042527245, longi: 11.2103422095,
    name: "Langenmosen",
    plz: "86571"
  },
  {
    lati: 48.4625143051, longi: 10.72695172,
    name: "Welden",
    plz: "86465"
  },
  {
    lati: 51.3092402244, longi: 9.66160409437,
    name: "Nieste",
    plz: "34329"
  },
  {
    lati: 48.2342730527, longi: 9.68754985915,
    name: "Rottenacker",
    plz: "89616"
  },
  {
    lati: 52.0239217798, longi: 9.40835753445,
    name: "Emmerthal",
    plz: "31860"
  },
  {
    lati: 49.3696393298, longi: 10.1582700328,
    name: "Rothenburg ob der Tauber",
    plz: "91541"
  },
  {
    lati: 49.2978586234, longi: 7.85008165039,
    name: "Wilgartswiesen",
    plz: "76848"
  },
  {
    lati: 51.5047688409, longi: 8.43991622014,
    name: "Rüthen",
    plz: "59602"
  },
  {
    lati: 52.6745509696, longi: 8.78562578444,
    name: "Sulingen",
    plz: "27232"
  },
  {
    lati: 49.2944398738, longi: 8.90516634498,
    name: "Waibstadt",
    plz: "74915"
  },
  {
    lati: 47.8630434761, longi: 12.0031514696,
    name: "Bad Aibling",
    plz: "83043"
  },
  {
    lati: 51.2956990776, longi: 13.408412317,
    name: "Nünchritz, Glaubitz",
    plz: "01612"
  },
  {
    lati: 48.8328712257, longi: 10.7075735836,
    name: "Huisheim",
    plz: "86685"
  },
  {
    lati: 52.2441611028, longi: 10.2280780613,
    name: "Lahstedt",
    plz: "31246"
  },
  {
    lati: 53.5305672665, longi: 10.263815807,
    name: "Reinbek",
    plz: "21465"
  },
  {
    lati: 48.601274376, longi: 11.9353763515,
    name: "Obersüßbach",
    plz: "84101"
  },
  {
    lati: 52.1108720119, longi: 11.610468467,
    name: "Magdeburg",
    plz: "39112"
  },
  {
    lati: 47.5680999261, longi: 7.95656530231,
    name: "Bad Säckingen",
    plz: "79713"
  },
  {
    lati: 50.7770654027, longi: 6.08646922231,
    name: "Aachen",
    plz: "52062"
  },
  {
    lati: 51.1615102367, longi: 6.42084906781,
    name: "Mönchengladbach",
    plz: "41239"
  },
  {
    lati: 49.2627946019, longi: 7.57769704967,
    name: "Thaleischweiler-Fröschen",
    plz: "66987"
  },
  {
    lati: 49.1969943043, longi: 7.43930412717,
    name: "Battweiler u.a.",
    plz: "66484"
  },
  {
    lati: 52.541596851, longi: 13.3494874268,
    name: "Berlin Wedding",
    plz: "13353"
  },
  {
    lati: 49.6219818127, longi: 11.1402210455,
    name: "Neunkirchen a. Brand",
    plz: "91077"
  },
  {
    lati: 50.1938523547, longi: 11.2769242143,
    name: "Küps",
    plz: "96328"
  },
  {
    lati: 53.1728932715, longi: 8.19532298961,
    name: "Oldenburg (Oldenburg)",
    plz: "26127"
  },
  {
    lati: 47.7987073593, longi: 8.9149525325,
    name: "Steißlingen",
    plz: "78256"
  },
  {
    lati: 49.607252497, longi: 9.1429957645,
    name: "Kirchzell",
    plz: "63931"
  },
  {
    lati: 48.4856187579, longi: 9.6887981539,
    name: "Laichingen",
    plz: "89150"
  },
  {
    lati: 51.7958212997, longi: 12.1863209316,
    name: "Dessau-Roßlau",
    plz: "06847"
  },
  {
    lati: 48.9038796412, longi: 8.26257567189,
    name: "Bietigheim",
    plz: "76467"
  },
  {
    lati: 52.5579848885, longi: 13.3473482108,
    name: "Berlin Wedding",
    plz: "13349"
  },
  {
    lati: 51.3553436682, longi: 14.9446433931,
    name: "Rothenburg/O.L.",
    plz: "02929"
  },
  {
    lati: 50.8724055164, longi: 9.09084019858,
    name: "Neustadt",
    plz: "35279"
  },
  {
    lati: 51.3960910197, longi: 7.18230664756,
    name: "Hattingen",
    plz: "45525"
  },
  {
    lati: 53.9689784951, longi: 9.50250646126,
    name: "Ottenbüttel",
    plz: "25591"
  },
  {
    lati: 49.2783670677, longi: 9.61329344213,
    name: "Niedernhall",
    plz: "74676"
  },
  {
    lati: 52.3969267152, longi: 9.86496727978,
    name: "Hannover",
    plz: "30629"
  },
  {
    lati: 47.9361667483, longi: 10.605127583,
    name: "Pforzen",
    plz: "87666"
  },
  {
    lati: 48.2075590062, longi: 11.1611193515,
    name: "Mammendorf",
    plz: "82291"
  },
  {
    lati: 48.1492412073, longi: 11.5296343596,
    name: "München",
    plz: "80634"
  },
  {
    lati: 48.6570523141, longi: 11.8471312763,
    name: "Attenhofen",
    plz: "84091"
  },
  {
    lati: 50.5462375863, longi: 7.59903598693,
    name: "Dürrholz",
    plz: "56307"
  },
  {
    lati: 48.2304823676, longi: 10.1963072433,
    name: "Buch",
    plz: "89290"
  },
  {
    lati: 50.5668193802, longi: 7.96668093907,
    name: "Westerburg",
    plz: "56457"
  },
  {
    lati: 49.9127475054, longi: 8.02832775018,
    name: "Ockenheim",
    plz: "55437"
  },
  {
    lati: 48.688931616, longi: 8.14860039387,
    name: "Bühl",
    plz: "77815"
  },
  {
    lati: 49.7850720007, longi: 7.25086985703,
    name: "Niederwörresbach",
    plz: "55758"
  },
  {
    lati: 50.0992795475, longi: 11.8858069245,
    name: "Weißenstadt",
    plz: "95163"
  },
  {
    lati: 48.0638689005, longi: 7.77724319219,
    name: "March",
    plz: "79232"
  },
  {
    lati: 48.2949575424, longi: 7.8472243483,
    name: "Kippenheim",
    plz: "77971"
  },
  {
    lati: 50.3032938278, longi: 8.2874754088,
    name: "Bad Camberg",
    plz: "65520"
  },
  {
    lati: 49.6170051047, longi: 8.46914535721,
    name: "Lampertheim",
    plz: "68623"
  },
  {
    lati: 47.9650854076, longi: 8.96783531586,
    name: "Neuhausen ob Eck",
    plz: "78579"
  },
  {
    lati: 50.3567229356, longi: 9.08341727527,
    name: "Ortenberg",
    plz: "63683"
  },
  {
    lati: 49.4902040244, longi: 8.03243909824,
    name: "Carlsberg",
    plz: "67316"
  },
  {
    lati: 51.0296004343, longi: 11.7058481105,
    name: "Dornburg-Camburg u.a.",
    plz: "07774"
  },
  {
    lati: 51.8833153889, longi: 10.6969378911,
    name: "Ilsenburg, Nordharz",
    plz: "38871"
  },
  {
    lati: 49.6226213699, longi: 9.65535455192,
    name: "Tauberbischofsheim",
    plz: "97941"
  },
  {
    lati: 49.6816347364, longi: 9.88830689176,
    name: "Geroldshausen",
    plz: "97256"
  },
  {
    lati: 49.4100779719, longi: 9.53105950866,
    name: "Ravenstein",
    plz: "74747"
  },
  {
    lati: 52.4209509375, longi: 12.764140664,
    name: "Groß Kreutz",
    plz: "14550"
  },
  {
    lati: 50.8168243497, longi: 12.9394728799,
    name: "Chemnitz",
    plz: "09126"
  },
  {
    lati: 52.2394040025, longi: 9.24338937617,
    name: "Auetal",
    plz: "31749"
  },
  {
    lati: 53.4101603034, longi: 7.72796412201,
    name: "Wiesmoor",
    plz: "26639"
  },
  {
    lati: 48.6494663098, longi: 8.8230696769,
    name: "Deckenpfronn",
    plz: "75392"
  },
  {
    lati: 49.034688273, longi: 8.86259879291,
    name: "Sternenfels",
    plz: "75447"
  },
  {
    lati: 49.708868529, longi: 7.34927770818,
    name: "Idar-Oberstein",
    plz: "55743"
  },
  {
    lati: 52.5307427162, longi: 7.01316270676,
    name: "Esche, Georgsdorf, Lage, Neuenhaus, Osterwald",
    plz: "49828"
  },
  {
    lati: 50.5003289459, longi: 12.1729706513,
    name: "Plauen",
    plz: "08529"
  },
  {
    lati: 50.1162500159, longi: 11.2360839754,
    name: "Altenkunstadt",
    plz: "96264"
  },
  {
    lati: 54.0844123285, longi: 13.3742652868,
    name: "Greifswald",
    plz: "17489"
  },
  {
    lati: 52.7078080009, longi: 14.1201227101,
    name: "Wriezen",
    plz: "16269"
  },
  {
    lati: 52.0065343776, longi: 14.3324724506,
    name: "Lieberose",
    plz: "15868"
  },
  {
    lati: 51.5603949483, longi: 14.2101893766,
    name: "Welzow",
    plz: "03119"
  },
  {
    lati: 47.8276321598, longi: 8.42580594636,
    name: "Wutach",
    plz: "79879"
  },
  {
    lati: 48.9926433511, longi: 8.47444295376,
    name: "Karlsruhe",
    plz: "76227"
  },
  {
    lati: 51.5550302315, longi: 8.5844898136,
    name: "Büren",
    plz: "33142"
  },
  {
    lati: 47.9740261518, longi: 8.26569402296,
    name: "Eisenbach",
    plz: "79871"
  },
  {
    lati: 49.9264474508, longi: 11.558226859,
    name: "Bayreuth",
    plz: "95447"
  },
  {
    lati: 52.1256658324, longi: 11.5689508402,
    name: "Magdeburg",
    plz: "39110"
  },
  {
    lati: 50.6045405288, longi: 11.7130923663,
    name: "Ziegenrück",
    plz: "07924"
  },
  {
    lati: 51.7056180588, longi: 11.1154204562,
    name: "Quedlinburg, Ballenstedt",
    plz: "06485"
  },
  {
    lati: 50.6858798222, longi: 10.7468168653,
    name: "Oberhof",
    plz: "98559"
  },
  {
    lati: 51.5203166206, longi: 11.9852526518,
    name: "Halle/ Saale",
    plz: "06118"
  },
  {
    lati: 52.7365073483, longi: 12.4133870184,
    name: "Rhinow",
    plz: "14728"
  },
  {
    lati: 54.2162211676, longi: 9.27353872423,
    name: "Tellingstedt",
    plz: "25782"
  },
  {
    lati: 50.6844862604, longi: 9.55598874635,
    name: "Schlitz",
    plz: "36110"
  },
  {
    lati: 50.6318547225, longi: 9.50373779169,
    name: "Bad Salzschlirf",
    plz: "36364"
  },
  {
    lati: 48.999064634, longi: 8.81833673115,
    name: "Maulbronn",
    plz: "75433"
  },
  {
    lati: 48.8975329103, longi: 11.0013328285,
    name: "Solnhofen",
    plz: "91807"
  },
  {
    lati: 49.8783233771, longi: 6.74648005326,
    name: "Leiwen u.a.",
    plz: "54340"
  },
  {
    lati: 48.4261868932, longi: 11.9676947552,
    name: "Langenpreising",
    plz: "85465"
  },
  {
    lati: 50.9130773929, longi: 10.4242632692,
    name: "Seebach",
    plz: "99846"
  },
  {
    lati: 50.283051073, longi: 7.7748752776,
    name: "Singhofen",
    plz: "56379"
  },
  {
    lati: 49.8409526971, longi: 10.0512727319,
    name: "Kürnach",
    plz: "97273"
  },
  {
    lati: 52.0358671725, longi: 9.9049491846,
    name: "Sibbesse",
    plz: "31079"
  },
  {
    lati: 47.7967010984, longi: 9.16066671541,
    name: "Überlingen",
    plz: "88662"
  },
  {
    lati: 48.7646108967, longi: 9.19577193142,
    name: "Stuttgart",
    plz: "70184"
  },
  {
    lati: 53.5914394063, longi: 10.0219793211,
    name: "Hamburg",
    plz: "22303"
  },
  {
    lati: 53.3044203736, longi: 10.5408748271,
    name: "Scharnebeck, Echem, Lüdersburg, Rullstorf",
    plz: "21379"
  },
  {
    lati: 51.175654236, longi: 9.53394707824,
    name: "Körle",
    plz: "34327"
  },
  {
    lati: 53.0672269529, longi: 8.66958492466,
    name: "Delmenhorst",
    plz: "27751"
  },
  {
    lati: 50.932835828, longi: 6.93408680499,
    name: "Köln",
    plz: "50674"
  },
  {
    lati: 51.6663965536, longi: 7.32154991651,
    name: "Datteln",
    plz: "45711"
  },
  {
    lati: 50.7021850688, longi: 7.67819865578,
    name: "Mammelzen",
    plz: "57636"
  },
  {
    lati: 52.7134833312, longi: 10.4239104495,
    name: "Steinhorst",
    plz: "29367"
  },
  {
    lati: 48.821212822, longi: 10.4232092092,
    name: "Riesbürg",
    plz: "73469"
  },
  {
    lati: 48.7727294958, longi: 13.1907129966,
    name: "Schöllnach",
    plz: "94508"
  },
  {
    lati: 52.448574548, longi: 13.3337061449,
    name: "Berlin Steglitz",
    plz: "12167"
  },
  {
    lati: 53.8870742051, longi: 11.4427349965,
    name: "Wismar, Groß Krankow u.a.",
    plz: "23966"
  },
  {
    lati: 48.1552112683, longi: 12.2640262609,
    name: "Gars a. Inn",
    plz: "83536"
  },
  {
    lati: 51.8179075961, longi: 12.2207737336,
    name: "Dessau-Roßlau",
    plz: "06842"
  },
  {
    lati: 50.1995937103, longi: 11.1502158387,
    name: "Weidhausen b. Coburg",
    plz: "96279"
  },
  {
    lati: 48.5713697383, longi: 7.84953013026,
    name: "Kehl",
    plz: "77694"
  },
  {
    lati: 50.8950557497, longi: 7.89767203799,
    name: "Freudenberg",
    plz: "57258"
  },
  {
    lati: 47.8134229306, longi: 8.16204471567,
    name: "Schluchsee",
    plz: "79859"
  },
  {
    lati: 52.3301839343, longi: 9.69062438214,
    name: "Hannover",
    plz: "30457"
  },
  {
    lati: 50.4170716266, longi: 9.55182422579,
    name: "Flieden",
    plz: "36103"
  },
  {
    lati: 50.1618376367, longi: 11.5697433035,
    name: "Guttenberg",
    plz: "95358"
  },
  {
    lati: 49.4093277813, longi: 7.72741759818,
    name: "Kaiserslautern",
    plz: "67661"
  },
  {
    lati: 51.0566682779, longi: 8.11274498968,
    name: "Kirchhundem",
    plz: "57399"
  },
  {
    lati: 48.2425389514, longi: 8.20389517397,
    name: "Gutach (Schwarzwaldbahn)",
    plz: "77793"
  },
  {
    lati: 49.0109383038, longi: 8.35836450548,
    name: "Karlsruhe",
    plz: "76185"
  },
  {
    lati: 50.8749845178, longi: 7.02039153646,
    name: "Köln",
    plz: "50999"
  },
  {
    lati: 50.2115400351, longi: 6.5660771486,
    name: "Büdesheim",
    plz: "54610"
  },
  {
    lati: 49.9388765971, longi: 6.66132558376,
    name: "Speicher",
    plz: "54662"
  },
  {
    lati: 49.709875051, longi: 11.1408950935,
    name: "Wiesenthau",
    plz: "91369"
  },
  {
    lati: 48.1391052689, longi: 11.6027305035,
    name: "München",
    plz: "81675"
  },
  {
    lati: 52.1221931896, longi: 9.34543379076,
    name: "Hameln",
    plz: "31787"
  },
  {
    lati: 48.7462541857, longi: 9.48317809804,
    name: "Lichtenwald",
    plz: "73669"
  },
  {
    lati: 52.387989426, longi: 9.18182243769,
    name: "Pollhagen",
    plz: "31718"
  },
  {
    lati: 47.9322340025, longi: 10.3256352427,
    name: "Ottobeuren",
    plz: "87724"
  },
  {
    lati: 49.0796659161, longi: 12.7636876281,
    name: "Rattenberg",
    plz: "94371"
  },
  {
    lati: 52.3320125318, longi: 8.76971903239,
    name: "Hille",
    plz: "32479"
  },
  {
    lati: 49.1605967773, longi: 10.71907578,
    name: "Muhr a. See",
    plz: "91735"
  },
  {
    lati: 52.547460655, longi: 13.5696515331,
    name: "Berlin",
    plz: "12685"
  },
  {
    lati: 49.697597371, longi: 11.0193759207,
    name: "Hausen",
    plz: "91353"
  },
  {
    lati: 49.6258952527, longi: 11.2704747432,
    name: "Weißenohe",
    plz: "91367"
  },
  {
    lati: 51.1306049806, longi: 11.6906278943,
    name: "Lanitz-Hassel-Tal, Molauer Land",
    plz: "06628"
  },
  {
    lati: 49.0291314805, longi: 12.2260828085,
    name: "Donaustauf",
    plz: "93093"
  },
  {
    lati: 49.6208577937, longi: 12.3277763087,
    name: "Vohenstrauß",
    plz: "92648"
  },
  {
    lati: 47.8803493439, longi: 10.6165047977,
    name: "Kaufbeuren",
    plz: "87600"
  },
  {
    lati: 52.5244558242, longi: 10.9246547335,
    name: "Parsau",
    plz: "38470"
  },
  {
    lati: 51.0540102759, longi: 6.08869457364,
    name: "Waldfeucht, Heinsberg",
    plz: "52525"
  },
  {
    lati: 50.4323280069, longi: 7.37208579622,
    name: "Andernach",
    plz: "56626"
  },
  {
    lati: 49.5448248569, longi: 7.90455690525,
    name: "Gundersweiler, Gonbach u.a.",
    plz: "67724"
  },
  {
    lati: 52.5779585962, longi: 8.01289628075,
    name: "Gehrde",
    plz: "49596"
  },
  {
    lati: 47.616061744, longi: 7.6596718456,
    name: "Lörrach",
    plz: "79539"
  },
  {
    lati: 53.836550492, longi: 9.20746663178,
    name: "Krummendeich",
    plz: "21732"
  },
  {
    lati: 53.9301328374, longi: 9.36000023058,
    name: "Wilster",
    plz: "25554"
  },
  {
    lati: 48.8688521503, longi: 10.1086671795,
    name: "Aalen",
    plz: "73433"
  },
  {
    lati: 52.0859349185, longi: 10.138222019,
    name: "Holle",
    plz: "31188"
  },
  {
    lati: 53.5634937608, longi: 10.0241527611,
    name: "Hamburg",
    plz: "22087"
  },
  {
    lati: 49.4877340927, longi: 6.99536040812,
    name: "Tholey",
    plz: "66636"
  },
  {
    lati: 50.1525890961, longi: 11.9505850633,
    name: "Kirchenlamitz",
    plz: "95158"
  },
  {
    lati: 52.4413361182, longi: 13.4043930503,
    name: "Berlin Mariendorf",
    plz: "12107"
  },
  {
    lati: 53.7939413176, longi: 11.4249565261,
    name: "Bad Kleinen u.a.",
    plz: "23996"
  },
  {
    lati: 52.5051788231, longi: 11.418094093,
    name: "Gardelegen",
    plz: "39638"
  },
  {
    lati: 47.9044629379, longi: 11.1059475156,
    name: "Raisting",
    plz: "82399"
  },
  {
    lati: 52.3657318725, longi: 9.21429257681,
    name: "Lauenhagen",
    plz: "31714"
  },
  {
    lati: 54.1193104105, longi: 13.7424952698,
    name: "Kröslin, Krummin, Lassan u.a.",
    plz: "17440"
  },
  {
    lati: 49.8616201794, longi: 7.98169763374,
    name: "Sprendlingen",
    plz: "55576"
  },
  {
    lati: 49.3695311124, longi: 8.05503885978,
    name: "Lambrecht (Pfalz)",
    plz: "67466"
  },
  {
    lati: 50.1783828945, longi: 8.62638652665,
    name: "Frankfurt am Main",
    plz: "60438"
  },
  {
    lati: 50.0721920456, longi: 8.86238057424,
    name: "Obertshausen",
    plz: "63179"
  },
  {
    lati: 52.557532807, longi: 13.1679102081,
    name: "Berlin Falkenhagener Feld",
    plz: "13589"
  },
  {
    lati: 48.6878962601, longi: 9.92409117684,
    name: "Böhmenkirch",
    plz: "89558"
  },
  {
    lati: 48.0211876141, longi: 11.0132090934,
    name: "Finning",
    plz: "86923"
  },
  {
    lati: 49.4277368539, longi: 11.2036842996,
    name: "Nürnberg",
    plz: "90475"
  },
  {
    lati: 50.1955165627, longi: 11.7626670019,
    name: "Münchberg",
    plz: "95213"
  },
  {
    lati: 51.527090135, longi: 7.55604268628,
    name: "Dortmund",
    plz: "44309"
  },
  {
    lati: 49.3402756988, longi: 8.64362608847,
    name: "Sandhausen",
    plz: "69207"
  },
  {
    lati: 49.3745658888, longi: 8.88402589151,
    name: "Lobbach",
    plz: "74931"
  },
  {
    lati: 49.7708385802, longi: 9.90838424063,
    name: "Würzburg",
    plz: "97082"
  },
  {
    lati: 48.107798004, longi: 10.1084121225,
    name: "Dettingen an der Iller",
    plz: "88451"
  },
  {
    lati: 49.4362873363, longi: 12.5045765434,
    name: "Winklarn",
    plz: "92559"
  },
  {
    lati: 50.6241875019, longi: 12.9157389552,
    name: "Geyer",
    plz: "09468"
  },
  {
    lati: 50.4418311657, longi: 12.3563047262,
    name: "Falkenstein/Vogtl.",
    plz: "08223"
  },
  {
    lati: 51.2083936696, longi: 11.0573503958,
    name: "Weißensee",
    plz: "99631"
  },
  {
    lati: 50.469130401, longi: 7.00960677249,
    name: "Ahrbrück, Heckenbach, Hönningen, Kesseling, Rech",
    plz: "53506"
  },
  {
    lati: 50.9250083129, longi: 7.08902495071,
    name: "Köln",
    plz: "51107"
  },
  {
    lati: 49.8736458735, longi: 7.71506600291,
    name: "Hargesheim",
    plz: "55595"
  },
  {
    lati: 52.4955922737, longi: 7.95315421985,
    name: "Alfhausen",
    plz: "49594"
  },
  {
    lati: 50.469896676, longi: 9.58165861194,
    name: "Neuhof",
    plz: "36119"
  },
  {
    lati: 53.1252872284, longi: 8.26038668293,
    name: "Oldenburg (Oldenburg)",
    plz: "26135"
  },
  {
    lati: 48.7339358009, longi: 8.39481247338,
    name: "Gernsbach",
    plz: "76593"
  },
  {
    lati: 51.2446703958, longi: 9.80251212444,
    name: "Großalmerode",
    plz: "37247"
  },
  {
    lati: 51.1471786616, longi: 9.99453556824,
    name: "Wehretal",
    plz: "37287"
  },
  {
    lati: 53.4105039836, longi: 10.4686016466,
    name: "Geesthacht",
    plz: "21502"
  },
  {
    lati: 50.2287225082, longi: 8.98240465797,
    name: "Hammersbach",
    plz: "63546"
  },
  {
    lati: 51.0396429836, longi: 7.8864766398,
    name: "Olpe",
    plz: "57462"
  },
  {
    lati: 49.6394610968, longi: 7.99106856386,
    name: "Bolanden",
    plz: "67295"
  },
  {
    lati: 48.7260221787, longi: 9.38026854979,
    name: "Altbach",
    plz: "73776"
  },
  {
    lati: 49.8322255624, longi: 9.47533068705,
    name: "Schollbrunn",
    plz: "97852"
  },
  {
    lati: 51.465059872, longi: 13.5219219726,
    name: "Elsterwerda",
    plz: "04910"
  },
  {
    lati: 49.2438798323, longi: 8.67212104268,
    name: "Malsch",
    plz: "69254"
  },
  {
    lati: 48.0723420791, longi: 8.73291684037,
    name: "Spaichingen",
    plz: "78549"
  },
  {
    lati: 51.5083217358, longi: 7.4487794716,
    name: "Dortmund",
    plz: "44137"
  },
  {
    lati: 51.9776088065, longi: 7.63545548816,
    name: "Münster",
    plz: "48147"
  },
  {
    lati: 51.0512433931, longi: 10.8355084076,
    name: "Großfahner, Dachwig u.a.",
    plz: "99100"
  },
  {
    lati: 48.1455583623, longi: 11.5539521328,
    name: "München",
    plz: "80335"
  },
  {
    lati: 50.4420343085, longi: 12.0051788591,
    name: "Weischlitz u.a.",
    plz: "08538"
  },
  {
    lati: 50.1205688424, longi: 8.57428398771,
    name: "Frankfurt am Main",
    plz: "65936"
  },
  {
    lati: 49.9483646937, longi: 8.82908010702,
    name: "Eppertshausen",
    plz: "64859"
  },
  {
    lati: 50.7083781086, longi: 7.52382585213,
    name: "Weyerbusch",
    plz: "57635"
  },
  {
    lati: 48.7300473557, longi: 10.1938447898,
    name: "Heidenheim an der Brenz",
    plz: "89520"
  },
  {
    lati: 47.8085881951, longi: 11.629492927,
    name: "Sachsenkam",
    plz: "83679"
  },
  {
    lati: 50.0960989216, longi: 10.7279232768,
    name: "Ebern",
    plz: "96106"
  },
  {
    lati: 51.7818347385, longi: 10.9236238373,
    name: "Blankenburg, Oberharz am Brocken",
    plz: "38889"
  },
  {
    lati: 48.0494762265, longi: 10.4056217264,
    name: "Kammlach",
    plz: "87754"
  },
  {
    lati: 54.0021613892, longi: 10.5669549017,
    name: "Ahrensbök",
    plz: "23623"
  },
  {
    lati: 48.8877876528, longi: 9.20797979973,
    name: "Ludwigsburg",
    plz: "71638"
  },
  {
    lati: 49.3458639857, longi: 11.1652270247,
    name: "Wendelstein",
    plz: "90530"
  },
  {
    lati: 48.7630071364, longi: 11.3426647761,
    name: "Ingolstadt",
    plz: "85049"
  },
  {
    lati: 48.5415044394, longi: 12.167522107,
    name: "Landshut",
    plz: "84028"
  },
  {
    lati: 49.7256887693, longi: 12.2778088236,
    name: "Floß",
    plz: "92685"
  },
  {
    lati: 50.6937907972, longi: 12.5561267209,
    name: "Reinsdorf",
    plz: "08141"
  },
  {
    lati: 48.3315120656, longi: 8.35290562108,
    name: "Schenkenzell",
    plz: "77773"
  },
  {
    lati: 51.6761792838, longi: 6.29358827561,
    name: "Uedem",
    plz: "47589"
  },
  {
    lati: 51.5361717959, longi: 7.11133477538,
    name: "Gelsenkirchen",
    plz: "45889"
  },
  {
    lati: 50.0786019593, longi: 7.13253608607,
    name: "Peterswald-Löffelscheid u.a.",
    plz: "56858"
  },
  {
    lati: 48.8917733663, longi: 12.497832101,
    name: "Atting",
    plz: "94348"
  },
  {
    lati: 48.1452756567, longi: 11.4368110446,
    name: "München",
    plz: "81243"
  },
  {
    lati: 47.754491542, longi: 11.5337182766,
    name: "Bad Tölz, Wackersberg",
    plz: "83646"
  },
  {
    lati: 52.7862030964, longi: 11.7127350242,
    name: "Osterburg, Altmärkische Höhe",
    plz: "39606"
  },
  {
    lati: 48.6144086746, longi: 11.5440186541,
    name: "Rohrbach",
    plz: "85296"
  },
  {
    lati: 48.7242465583, longi: 11.5677973081,
    name: "Ernsgaden",
    plz: "85119"
  },
  {
    lati: 51.0396194473, longi: 13.6666945214,
    name: "Dresden",
    plz: "01169"
  },
  {
    lati: 50.9791098866, longi: 14.1400036846,
    name: "Hohnstein",
    plz: "01848"
  },
  {
    lati: 50.6183281571, longi: 7.83355800006,
    name: "Alpenrod",
    plz: "57642"
  },
  {
    lati: 50.0635895611, longi: 6.60421286861,
    name: "Kyllburg",
    plz: "54655"
  },
  {
    lati: 51.8973153811, longi: 6.95390579427,
    name: "Velen",
    plz: "46342"
  },
  {
    lati: 50.5937971183, longi: 7.20537140631,
    name: "Remagen",
    plz: "53424"
  },
  {
    lati: 51.3249688261, longi: 9.17617643079,
    name: "Wolfhagen",
    plz: "34466"
  },
  {
    lati: 50.8215122316, longi: 7.87674167653,
    name: "Kirchen (Sieg)",
    plz: "57548"
  },
  {
    lati: 53.1516799935, longi: 8.70945748652,
    name: "Bremen",
    plz: "28719"
  },
  {
    lati: 49.9300047766, longi: 8.75606225417,
    name: "Messel",
    plz: "64409"
  },
  {
    lati: 47.9241718137, longi: 10.1979412645,
    name: "Woringen",
    plz: "87789"
  },
  {
    lati: 47.903117405, longi: 10.2586881164,
    name: "Wolfertschwenden",
    plz: "87787"
  },
  {
    lati: 52.5038122285, longi: 10.2608255921,
    name: "Langlingen",
    plz: "29364"
  },
  {
    lati: 48.978069377, longi: 10.4742807496,
    name: "Fremdingen",
    plz: "86742"
  },
  {
    lati: 51.717269508, longi: 10.5500356015,
    name: "Braunlage",
    plz: "37444"
  },
  {
    lati: 53.4267094933, longi: 11.8454276274,
    name: "Parchim",
    plz: "19370"
  },
  {
    lati: 51.5481453364, longi: 14.6637445382,
    name: "Bad Muskau, Groß Düben, Gablenz",
    plz: "02953"
  },
  {
    lati: 48.0804646092, longi: 11.0758816053,
    name: "Greifenberg",
    plz: "86926"
  },
  {
    lati: 48.6683032959, longi: 11.3995413385,
    name: "Karlskron",
    plz: "85123"
  },
  {
    lati: 51.7893886479, longi: 12.3002714238,
    name: "Dessau-Roßlau",
    plz: "06842"
  },
  {
    lati: 50.0480034548, longi: 9.52021347936,
    name: "Partenstein",
    plz: "97846"
  },
  {
    lati: 50.527778875, longi: 7.22575421989,
    name: "Sinzig",
    plz: "53489"
  },
  {
    lati: 50.1550324006, longi: 8.36692767821,
    name: "Eppstein",
    plz: "65817"
  },
  {
    lati: 50.6451370364, longi: 7.52397217466,
    name: "Flammersfeld",
    plz: "57632"
  },
  {
    lati: 47.7030471798, longi: 9.93909063523,
    name: "Argenbühl",
    plz: "88260"
  },
  {
    lati: 49.8812007183, longi: 10.9289759666,
    name: "Bamberg",
    plz: "96050"
  },
  {
    lati: 54.2355347246, longi: 10.0831411445,
    name: "Flintbek",
    plz: "24220"
  },
  {
    lati: 52.909788637, longi: 13.5924573038,
    name: "Schorfheide",
    plz: "16244"
  },
  {
    lati: 52.6216967606, longi: 13.8907888856,
    name: "Lichtenow, Altlandsberg u.a.",
    plz: "15345"
  },
  {
    lati: 51.6621835544, longi: 14.0225565856,
    name: "Altdöbern, Luckaitztal",
    plz: "03229"
  },
  {
    lati: 48.6106903345, longi: 10.7192929688,
    name: "Buttenwiesen",
    plz: "86647"
  },
  {
    lati: 50.3389746173, longi: 10.2867199843,
    name: "Hohenroth",
    plz: "97618"
  },
  {
    lati: 48.455499283, longi: 10.3578023174,
    name: "Rettenbach",
    plz: "89364"
  },
  {
    lati: 52.5207748714, longi: 13.4958550077,
    name: "Berlin Lichtenberg",
    plz: "10365"
  },
  {
    lati: 53.8684481765, longi: 11.4932636277,
    name: "Wismar, Groß Krankow u.a.",
    plz: "23966"
  },
  {
    lati: 53.6583165392, longi: 13.1062860584,
    name: "Rosenow",
    plz: "17091"
  },
  {
    lati: 49.9716278669, longi: 7.43258100419,
    name: "Dickenschied u.a.",
    plz: "55483"
  },
  {
    lati: 53.5831749405, longi: 7.45467894976,
    name: "Westerholt, Schweindorf u.a.",
    plz: "26556"
  },
  {
    lati: 49.1040850871, longi: 8.33244875077,
    name: "Neupotz",
    plz: "76777"
  },
  {
    lati: 52.0065904581, longi: 8.5181903941,
    name: "Bielefeld",
    plz: "33617"
  },
  {
    lati: 49.8775400081, longi: 7.05590383553,
    name: "Monzelfeld, Hochscheid u.a.",
    plz: "54472"
  },
  {
    lati: 54.3854679348, longi: 9.08841113949,
    name: "Friedrichstadt, Koldenbüttel u.a.",
    plz: "25840"
  },
  {
    lati: 48.9116393507, longi: 9.24761634724,
    name: "Ludwigsburg",
    plz: "71642"
  },
  {
    lati: 49.4902037498, longi: 9.63513389079,
    name: "Boxberg",
    plz: "97944"
  },
  {
    lati: 49.7431631432, longi: 9.52227762413,
    name: "Wertheim",
    plz: "97877"
  },
  {
    lati: 51.7263866354, longi: 12.3774473401,
    name: "Gräfenhainichen",
    plz: "06772"
  },
  {
    lati: 50.0903397856, longi: 8.1679259263,
    name: "Wiesbaden",
    plz: "65199"
  },
  {
    lati: 49.4249668364, longi: 7.47968984297,
    name: "Hütschenhausen",
    plz: "66882"
  },
  {
    lati: 47.9092113823, longi: 11.4271534491,
    name: "Wolfratshausen",
    plz: "82515"
  },
  {
    lati: 48.7400453374, longi: 9.03980934403,
    name: "Sindelfingen",
    plz: "71069"
  },
  {
    lati: 54.4494195986, longi: 9.1546290101,
    name: "Rantrum",
    plz: "25873"
  },
  {
    lati: 52.2007249149, longi: 12.7658041813,
    name: "Brück, Borkheide u.a",
    plz: "14822"
  },
  {
    lati: 52.9200995337, longi: 9.65316854244,
    name: "Bomlitz",
    plz: "29699"
  },
  {
    lati: 53.8665453634, longi: 10.0620911035,
    name: "Struvenhütten",
    plz: "24643"
  },
  {
    lati: 54.2660773483, longi: 10.6630239555,
    name: "Blekendorf",
    plz: "24327"
  },
  {
    lati: 49.6384713906, longi: 10.8942213848,
    name: "Heßdorf",
    plz: "91093"
  },
  {
    lati: 53.6051958415, longi: 10.4722514052,
    name: "Hamfelde, Kasseburg, Köthel, Rausdorf, Schönberg",
    plz: "22929"
  },
  {
    lati: 50.5749371878, longi: 10.5687774219,
    name: "Suhl, Marisfeld, Rohr u.a.",
    plz: "98530"
  },
  {
    lati: 50.4367464296, longi: 7.59469541813,
    name: "Bendorf",
    plz: "56170"
  },
  {
    lati: 47.6599037006, longi: 9.70239326966,
    name: "Neukirch",
    plz: "88099"
  },
  {
    lati: 48.6825783539, longi: 12.9982543156,
    name: "Osterhofen",
    plz: "94486"
  },
  {
    lati: 52.2213466387, longi: 8.6403310976,
    name: "Kirchlengern",
    plz: "32278"
  },
  {
    lati: 51.4470059579, longi: 13.2807553335,
    name: "Mühlberg, Bad Liebenwerda",
    plz: "04931"
  },
  {
    lati: 48.4059583406, longi: 8.03270172083,
    name: "Gengenbach",
    plz: "77723"
  },
  {
    lati: 53.2869157064, longi: 9.28855243466,
    name: "Zeven, Elsdorf",
    plz: "27404"
  },
  {
    lati: 48.0720067024, longi: 9.26274550867,
    name: "Sigmaringendorf",
    plz: "72517"
  },
  {
    lati: 48.9084156883, longi: 11.617833503,
    name: "Altmannstein",
    plz: "93336"
  },
  {
    lati: 49.284992207, longi: 12.6671240883,
    name: "Waffenbrunn",
    plz: "93494"
  },
  {
    lati: 48.9169789068, longi: 12.2055566577,
    name: "Alteglofsheim",
    plz: "93087"
  },
  {
    lati: 48.1474332399, longi: 10.5939842718,
    name: "Markt Wald",
    plz: "86865"
  },
  {
    lati: 51.1907123983, longi: 6.44748176322,
    name: "Mönchengladbach",
    plz: "41061"
  },
  {
    lati: 49.7753504963, longi: 6.68866633729,
    name: "Trier",
    plz: "54292"
  },
  {
    lati: 51.2238679501, longi: 6.78138349737,
    name: "Düsseldorf",
    plz: "40212"
  },
  {
    lati: 51.0098608385, longi: 7.10995857061,
    name: "Bergisch Gladbach",
    plz: "51467"
  },
  {
    lati: 50.9847124414, longi: 7.10902655118,
    name: "Bergisch Gladbach",
    plz: "51469"
  },
  {
    lati: 52.0338323206, longi: 9.03246084767,
    name: "Dörentrup",
    plz: "32694"
  },
  {
    lati: 48.0177863711, longi: 8.72405975097,
    name: "Seitingen-Oberflacht",
    plz: "78606"
  },
  {
    lati: 54.1231913353, longi: 10.2209097368,
    name: "Wankendorf",
    plz: "24601"
  },
  {
    lati: 50.8777735671, longi: 9.86662573699,
    name: "Friedewald",
    plz: "36289"
  },
  {
    lati: 50.2565397619, longi: 7.30789717909,
    name: "Mertloch, Welling u.a.",
    plz: "56753"
  },
  {
    lati: 52.3846244493, longi: 12.4271177873,
    name: "Brandenburg/ Havel",
    plz: "14774"
  },
  {
    lati: 48.5216228159, longi: 8.06706166517,
    name: "Oberkirch",
    plz: "77704"
  },
  {
    lati: 52.2506875971, longi: 13.1130917599,
    name: "Nuthetal",
    plz: "14558"
  },
  {
    lati: 52.9149255541, longi: 11.2068044022,
    name: "Lübbow",
    plz: "29488"
  },
  {
    lati: 47.5448625729, longi: 10.2992137329,
    name: "Burgberg im Allgäu",
    plz: "87545"
  },
  {
    lati: 49.7934421941, longi: 7.71818092151,
    name: "Hargesheim",
    plz: "55595"
  },
  {
    lati: 51.9181459589, longi: 8.37336722729,
    name: "Gütersloh",
    plz: "33330"
  },
  {
    lati: 49.4015980563, longi: 11.1391827654,
    name: "Nürnberg",
    plz: "90473"
  },
  {
    lati: 49.8461841321, longi: 11.4911355764,
    name: "Hummeltal",
    plz: "95503"
  },
  {
    lati: 51.599877451, longi: 7.29850775759,
    name: "Castrop-Rauxel",
    plz: "44577"
  },
  {
    lati: 53.4539624775, longi: 12.4385914221,
    name: "Malchow u.a.",
    plz: "17213"
  },
  {
    lati: 48.8835017825, longi: 8.96183685486,
    name: "Eberdingen",
    plz: "71735"
  },
  {
    lati: 48.6252107047, longi: 9.21862897162,
    name: "Aichtal",
    plz: "72631"
  },
  {
    lati: 54.1302369977, longi: 9.9139169704,
    name: "Timmaspe",
    plz: "24644"
  },
  {
    lati: 50.7251504074, longi: 7.92884098513,
    name: "Weitefeld",
    plz: "57586"
  },
  {
    lati: 48.1226963236, longi: 8.32935940423,
    name: "St. Georgen",
    plz: "78112"
  },
  {
    lati: 51.0602569076, longi: 13.7178040368,
    name: "Dresden",
    plz: "01067"
  },
  {
    lati: 49.0709419176, longi: 11.0099177888,
    name: "Höttingen",
    plz: "91798"
  },
  {
    lati: 51.511818597, longi: 6.75674948924,
    name: "Duisburg",
    plz: "47169"
  },
  {
    lati: 51.8521658815, longi: 6.83168723557,
    name: "Borken",
    plz: "46325"
  },
  {
    lati: 51.2221034531, longi: 6.81124566419,
    name: "Düsseldorf",
    plz: "40233"
  },
  {
    lati: 48.1941182142, longi: 10.3806886704,
    name: "Aletshausen",
    plz: "86480"
  },
  {
    lati: 51.5341760828, longi: 7.74559460236,
    name: "Unna",
    plz: "59427"
  },
  {
    lati: 49.4771781716, longi: 8.702942901,
    name: "Schriesheim",
    plz: "69198"
  },
  {
    lati: 53.0371232901, longi: 8.67017426347,
    name: "Delmenhorst",
    plz: "27755"
  },
  {
    lati: 49.0035243834, longi: 12.5910958226,
    name: "Falkenfels",
    plz: "94350"
  },
  {
    lati: 48.394883981, longi: 11.9842622688,
    name: "Wartenberg",
    plz: "85456"
  },
  {
    lati: 48.3644988873, longi: 10.8927861113,
    name: "Augsburg",
    plz: "86150"
  },
  {
    lati: 52.7078241025, longi: 11.9385266136,
    name: "Goldbeck, Arneburg u.a.",
    plz: "39596"
  },
  {
    lati: 50.9251597643, longi: 14.2764619875,
    name: "Sebnitz",
    plz: "01855"
  },
  {
    lati: 49.9932200392, longi: 6.91295488942,
    name: "Wittlich",
    plz: "54516"
  },
  {
    lati: 51.4610603727, longi: 6.97941546992,
    name: "Essen",
    plz: "45143"
  },
  {
    lati: 48.6828873754, longi: 8.1970472902,
    name: "Bühlertal",
    plz: "77830"
  },
  {
    lati: 49.4962233079, longi: 8.49284231493,
    name: "Mannheim",
    plz: "68167"
  },
  {
    lati: 52.3718955029, longi: 9.75631117567,
    name: "Hannover",
    plz: "30159"
  },
  {
    lati: 48.0551083341, longi: 9.84060135424,
    name: "Ummendorf",
    plz: "88444"
  },
  {
    lati: 48.1289887339, longi: 10.1367684638,
    name: "Kellmünz a.d. Iller",
    plz: "89293"
  },
  {
    lati: 49.773515041, longi: 9.07791195927,
    name: "Lützelbach",
    plz: "64750"
  },
  {
    lati: 50.0403433302, longi: 8.04132416181,
    name: "Eltville am Rhein",
    plz: "65347"
  },
  {
    lati: 53.9269872182, longi: 9.75830471904,
    name: "Wrist",
    plz: "25563"
  },
  {
    lati: 49.5274666679, longi: 9.84679956633,
    name: "Igersheim",
    plz: "97999"
  },
  {
    lati: 52.5475289508, longi: 13.2359519144,
    name: "Berlin Haselhorst",
    plz: "13599"
  },
  {
    lati: 50.8753863733, longi: 6.59052688128,
    name: "Kerpen",
    plz: "50170"
  },
  {
    lati: 49.9097534063, longi: 8.38570448581,
    name: "Trebur",
    plz: "65468"
  },
  {
    lati: 49.754102696, longi: 10.2443432906,
    name: "Albertshofen",
    plz: "97320"
  },
  {
    lati: 48.5374903696, longi: 10.3213588327,
    name: "Bächingen a.d. Brenz",
    plz: "89431"
  },
  {
    lati: 48.6449983162, longi: 10.6572726415,
    name: "Schwenningen",
    plz: "89443"
  },
  {
    lati: 48.0982326193, longi: 8.38065032727,
    name: "Unterkirnach",
    plz: "78089"
  },
  {
    lati: 52.5981895629, longi: 8.5092734023,
    name: "Barver, Dickel, Hemsloh, Rehden, Wetschen, Wehrblecker Heide",
    plz: "49453"
  },
  {
    lati: 49.3302948453, longi: 8.61193310129,
    name: "Leimen",
    plz: "69181"
  },
  {
    lati: 50.5922271873, longi: 7.51510472521,
    name: "Horhausen (Westerwald)",
    plz: "56593"
  },
  {
    lati: 50.6686233712, longi: 7.59061919924,
    name: "Neitersen",
    plz: "57638"
  },
  {
    lati: 51.6488931731, longi: 6.97307037786,
    name: "Dorsten",
    plz: "46282"
  },
  {
    lati: 50.7114562461, longi: 6.37285797352,
    name: "Hürtgenwald",
    plz: "52393"
  },
  {
    lati: 53.3352337089, longi: 9.9746736581,
    name: "Bendestorf",
    plz: "21227"
  },
  {
    lati: 51.0282382021, longi: 10.1270039556,
    name: "Herleshausen",
    plz: "37293"
  },
  {
    lati: 50.2884042319, longi: 11.9027191744,
    name: "Hof",
    plz: "95032"
  },
  {
    lati: 51.6613234526, longi: 10.7242204774,
    name: "Oberharz am Brocken",
    plz: "38877"
  },
  {
    lati: 49.6735481017, longi: 9.17194594621,
    name: "Weilbach",
    plz: "63937"
  },
  {
    lati: 48.945719403, longi: 9.51952592381,
    name: "Auenwald",
    plz: "71549"
  },
  {
    lati: 51.299581951, longi: 11.1290554913,
    name: "Bilzingsleben Kannawurf Oldisleben",
    plz: "06578"
  },
  {
    lati: 51.5993223864, longi: 12.1854298794,
    name: "Sandersdorf-Brehna",
    plz: "06794"
  },
  {
    lati: 53.9184789556, longi: 12.3369791831,
    name: "Laage, Wardow u.a.",
    plz: "18299"
  },
  {
    lati: 52.612694146, longi: 12.8229121647,
    name: "Nauen",
    plz: "14641"
  },
  {
    lati: 53.5644996704, longi: 13.1756543237,
    name: "Sponholz, Neunkirchen u.a.",
    plz: "17039"
  },
  {
    lati: 50.4375351799, longi: 10.3172544825,
    name: "Mellrichstadt",
    plz: "97638"
  },
  {
    lati: 51.1275831005, longi: 11.5350805143,
    name: "Eckartsberga",
    plz: "06648"
  },
  {
    lati: 50.5339100251, longi: 9.8137083957,
    name: "Dipperz",
    plz: "36160"
  },
  {
    lati: 52.2402704693, longi: 9.85493066783,
    name: "Sarstedt",
    plz: "31157"
  },
  {
    lati: 53.2557216982, longi: 8.79246044364,
    name: "Osterholz-Scharmbeck",
    plz: "27711"
  },
  {
    lati: 52.2734345166, longi: 8.08512541811,
    name: "Osnabrück",
    plz: "49084"
  },
  {
    lati: 51.2113339444, longi: 6.42719881239,
    name: "Mönchengladbach",
    plz: "41063"
  },
  {
    lati: 49.8713634278, longi: 7.44609285879,
    name: "Dickenschied u.a.",
    plz: "55483"
  },
  {
    lati: 49.7414607101, longi: 8.32219911981,
    name: "Mettenheim",
    plz: "67582"
  },
  {
    lati: 48.8969436578, longi: 8.57638439853,
    name: "Keltern",
    plz: "75210"
  },
  {
    lati: 49.741835965, longi: 8.59144206749,
    name: "Alsbach-Hähnlein",
    plz: "64665"
  },
  {
    lati: 52.7402131946, longi: 9.25229589998,
    name: "Haßbergen",
    plz: "31626"
  },
  {
    lati: 49.7660605451, longi: 9.2552492746,
    name: "Röllbach",
    plz: "63934"
  },
  {
    lati: 48.9475480152, longi: 9.34701242188,
    name: "Kirchberg",
    plz: "71737"
  },
  {
    lati: 51.1209614059, longi: 7.89779765967,
    name: "Attendorn",
    plz: "57439"
  },
  {
    lati: 53.4919228173, longi: 8.00002883032,
    name: "Sande",
    plz: "26452"
  },
  {
    lati: 53.1462568972, longi: 10.6205289035,
    name: "Altenmedingen",
    plz: "29575"
  },
  {
    lati: 49.714860198, longi: 10.8336639259,
    name: "Höchstadt a.d.Aisch",
    plz: "91315"
  },
  {
    lati: 48.9768729715, longi: 10.357630265,
    name: "Tannhausen",
    plz: "73497"
  },
  {
    lati: 51.5124831767, longi: 7.37457815848,
    name: "Dortmund",
    plz: "44379"
  },
  {
    lati: 50.5517065807, longi: 7.41623847534,
    name: "Waldbreitbach, Hasuen",
    plz: "56588"
  },
  {
    lati: 51.1957387395, longi: 6.46625743523,
    name: "Mönchengladbach",
    plz: "41065"
  },
  {
    lati: 50.9219206881, longi: 6.89526789753,
    name: "Köln",
    plz: "50935"
  },
  {
    lati: 51.5576109088, longi: 12.9491100533,
    name: "Torgau, Dreiheide",
    plz: "04860"
  },
  {
    lati: 50.5788114693, longi: 12.8864461093,
    name: "Scheibenberg",
    plz: "09481"
  },
  {
    lati: 50.0103232251, longi: 12.1014727605,
    name: "Marktredwitz",
    plz: "95615"
  },
  {
    lati: 52.5442846741, longi: 13.4414535924,
    name: "Berlin Prenzlauer Berg",
    plz: "10409"
  },
  {
    lati: 51.0116667136, longi: 13.8107776569,
    name: "Dresden",
    plz: "01257"
  },
  {
    lati: 50.0374465408, longi: 7.12228294174,
    name: "Pünderich",
    plz: "56862"
  },
  {
    lati: 49.6608960686, longi: 7.77793712624,
    name: "Steinbach, Weitersweiler, Bennhausen, Mörsfeld, Würzweiler, Ruppertsecke",
    plz: "67808"
  },
  {
    lati: 49.4689471814, longi: 9.19608394962,
    name: "Limbach",
    plz: "74838"
  },
  {
    lati: 49.7164282732, longi: 8.70955151126,
    name: "Lautertal (Odenwald)",
    plz: "64686"
  },
  {
    lati: 47.6785287113, longi: 8.90233404686,
    name: "Öhningen",
    plz: "78337"
  },
  {
    lati: 50.8366451409, longi: 9.2859489596,
    name: "Schrecksbach",
    plz: "34637"
  },
  {
    lati: 52.9203444957, longi: 11.8808088381,
    name: "Wittenberge, Rühstädt",
    plz: "19322"
  },
  {
    lati: 49.0289956045, longi: 10.1203230415,
    name: "Jagstzell",
    plz: "73489"
  },
  {
    lati: 49.2655202898, longi: 10.7049968471,
    name: "Lichtenau",
    plz: "91586"
  },
  {
    lati: 47.7279696536, longi: 10.9639564909,
    name: "Rottenbuch",
    plz: "82401"
  },
  {
    lati: 51.9258073606, longi: 10.4384377313,
    name: "Goslar",
    plz: "38642"
  },
  {
    lati: 51.6185443437, longi: 12.2429249853,
    name: "Sandersdorf-Brehna",
    plz: "06792"
  },
  {
    lati: 52.6119126133, longi: 13.3417419633,
    name: "Berlin Lübars",
    plz: "13469"
  },
  {
    lati: 50.974300111, longi: 6.87055550561,
    name: "Köln",
    plz: "50829"
  },
  {
    lati: 51.0006784966, longi: 11.0320952141,
    name: "Erfurt",
    plz: "99086"
  },
  {
    lati: 47.8204416808, longi: 11.8970122151,
    name: "Irschenberg",
    plz: "83737"
  },
  {
    lati: 48.4183253117, longi: 8.55342556192,
    name: "Schopfloch",
    plz: "72296"
  },
  {
    lati: 51.1359046581, longi: 9.44857314766,
    name: "Felsberg",
    plz: "34587"
  },
  {
    lati: 51.3793694651, longi: 9.53425783473,
    name: "Kassel, Fuldatal",
    plz: "34233"
  },
  {
    lati: 48.5185061939, longi: 9.75011244298,
    name: "Merklingen",
    plz: "89188"
  },
  {
    lati: 47.5864451618, longi: 10.3191110055,
    name: "Rettenberg",
    plz: "87549"
  },
  {
    lati: 48.2598972788, longi: 10.6029983459,
    name: "Langenneufnach",
    plz: "86863"
  },
  {
    lati: 48.7751775924, longi: 9.18551838328,
    name: "Stuttgart",
    plz: "70182"
  },
  {
    lati: 50.8574985055, longi: 12.5637630905,
    name: "Remse",
    plz: "08373"
  },
  {
    lati: 50.8314061687, longi: 12.9049063926,
    name: "Chemnitz",
    plz: "09112"
  },
  {
    lati: 53.4615111741, longi: 9.93335713524,
    name: "Hamburg",
    plz: "21075"
  },
  {
    lati: 47.9905078328, longi: 9.83376600158,
    name: "Eberhardzell",
    plz: "88436"
  },
  {
    lati: 48.1799643022, longi: 11.9738226877,
    name: "Forstern",
    plz: "85659"
  },
  {
    lati: 50.9133377891, longi: 10.53662545,
    name: "Waltershausen",
    plz: "99880"
  },
  {
    lati: 51.0472675777, longi: 7.0596567546,
    name: "Leverkusen",
    plz: "51377"
  },
  {
    lati: 48.0057824358, longi: 7.8767347318,
    name: "Freiburg im Breisgau",
    plz: "79104"
  },
  {
    lati: 52.1480384212, longi: 8.05442588472,
    name: "Bad Iburg",
    plz: "49186"
  },
  {
    lati: 49.5849744707, longi: 8.37349165372,
    name: "Bobenheim-Roxheim",
    plz: "67240"
  },
  {
    lati: 49.9827660147, longi: 8.44980948191,
    name: "Rüsselsheim",
    plz: "65428"
  },
  {
    lati: 48.5849457667, longi: 9.28536931591,
    name: "Großbettlingen",
    plz: "72663"
  },
  {
    lati: 54.3093997284, longi: 12.5384451338,
    name: "Saal",
    plz: "18317"
  },
  {
    lati: 50.9931245254, longi: 13.1343460682,
    name: "Hainichen, Rossau, Striegistal",
    plz: "09661"
  },
  {
    lati: 52.1569063008, longi: 11.5780931798,
    name: "Magdeburg",
    plz: "39130"
  },
  {
    lati: 48.0322039354, longi: 11.8139736556,
    name: "Oberpframmern",
    plz: "85667"
  },
  {
    lati: 48.1544318227, longi: 12.3330291681,
    name: "Gars a. Inn",
    plz: "83559"
  },
  {
    lati: 52.3659109466, longi: 13.0929529264,
    name: "Potsdam",
    plz: "14478"
  },
  {
    lati: 49.6253136589, longi: 7.93354321538,
    name: "Dannenfels",
    plz: "67814"
  },
  {
    lati: 48.2382085791, longi: 8.12233701704,
    name: "Mühlenbach",
    plz: "77796"
  },
  {
    lati: 48.2036157832, longi: 8.5586987022,
    name: "Rottweil",
    plz: "78628"
  },
  {
    lati: 54.0042988932, longi: 8.90404271207,
    name: "Friedrichskoog",
    plz: "25718"
  },
  {
    lati: 47.7531682804, longi: 11.8278552991,
    name: "Hausham",
    plz: "83734"
  },
  {
    lati: 50.7732075076, longi: 6.2188032935,
    name: "Stolberg (Rhld.)",
    plz: "52222"
  },
  {
    lati: 48.7646930499, longi: 8.24133349523,
    name: "Baden-Baden",
    plz: "76530"
  },
  {
    lati: 51.499699445, longi: 7.39682179679,
    name: "Dortmund",
    plz: "44149"
  },
  {
    lati: 49.7819433503, longi: 9.13973378088,
    name: "Wörth a.Main",
    plz: "63939"
  },
  {
    lati: 50.3088637216, longi: 11.0360296736,
    name: "Rödental",
    plz: "96472"
  },
  {
    lati: 52.7789961161, longi: 10.8886995344,
    name: "Dähre, Diesdorf, Wallstawe",
    plz: "29413"
  },
  {
    lati: 50.8404157964, longi: 14.7520147166,
    name: "Oybin",
    plz: "02797"
  },
  {
    lati: 47.9933980763, longi: 12.0623176456,
    name: "Emmering",
    plz: "83550"
  },
  {
    lati: 53.6360495158, longi: 9.95555388086,
    name: "Hamburg",
    plz: "22453"
  },
  {
    lati: 53.779884633, longi: 9.59280012333,
    name: "Kiebitzreihe",
    plz: "25368"
  },
  {
    lati: 51.2022990045, longi: 9.91532734675,
    name: "Meißner",
    plz: "37290"
  },
  {
    lati: 52.5490395131, longi: 13.3655170875,
    name: "Berlin Wedding",
    plz: "13347"
  },
  {
    lati: 50.73303725, longi: 7.05907064567,
    name: "Bonn",
    plz: "53121"
  },
  {
    lati: 51.4922825743, longi: 6.75464707036,
    name: "Duisburg",
    plz: "47166"
  },
  {
    lati: 51.4582757698, longi: 6.73521729406,
    name: "Duisburg",
    plz: "47119"
  },
  {
    lati: 49.2944502981, longi: 8.52339744532,
    name: "Neulußheim",
    plz: "68809"
  },
  {
    lati: 50.0330690046, longi: 8.95999945875,
    name: "Seligenstadt",
    plz: "63500"
  },
  {
    lati: 51.8485661783, longi: 9.05488920827,
    name: "Steinheim",
    plz: "32839"
  },
  {
    lati: 49.8687499955, longi: 8.76202490346,
    name: "Roßdorf",
    plz: "64380"
  },
  {
    lati: 52.5185288628, longi: 8.87879453808,
    name: "Uchte",
    plz: "31600"
  },
  {
    lati: 49.0491546276, longi: 13.2810910225,
    name: "Zwiesel",
    plz: "94227"
  },
  {
    lati: 53.647576385, longi: 9.85201970213,
    name: "Rellingen",
    plz: "25462"
  },
  {
    lati: 49.8341798182, longi: 10.0149850259,
    name: "Estenfeld",
    plz: "97230"
  },
  {
    lati: 54.3274933193, longi: 10.1113837172,
    name: "Kiel",
    plz: "24116"
  },
  {
    lati: 48.9576329744, longi: 12.5184425667,
    name: "Kirchroth",
    plz: "94356"
  },
  {
    lati: 48.2384852231, longi: 12.8539267802,
    name: "Marktl",
    plz: "84533"
  },
  {
    lati: 48.3605162766, longi: 12.0018079817,
    name: "Fraunberg",
    plz: "85447"
  },
  {
    lati: 49.0621961373, longi: 8.90835324845,
    name: "Zaberfeld",
    plz: "74374"
  },
  {
    lati: 52.3446036264, longi: 8.02400780069,
    name: "Wallenhorst",
    plz: "49134"
  },
  {
    lati: 49.7933856065, longi: 6.77443217791,
    name: "Leiwen u.a.",
    plz: "54340"
  },
  {
    lati: 52.364762903, longi: 13.3142158347,
    name: "Großbeeren",
    plz: "14979"
  },
  {
    lati: 52.5417220403, longi: 13.3904164894,
    name: "Berlin Wedding",
    plz: "13355"
  },
  {
    lati: 49.2048510624, longi: 7.56240401151,
    name: "Pirmasens",
    plz: "66954"
  },
  {
    lati: 49.0328278684, longi: 9.3579149634,
    name: "Oberstenfeld",
    plz: "71720"
  },
  {
    lati: 54.0250127595, longi: 9.3648700542,
    name: "Wacken",
    plz: "25596"
  },
  {
    lati: 48.6894621941, longi: 9.39139538477,
    name: "Köngen",
    plz: "73257"
  },
  {
    lati: 51.7867385108, longi: 11.605546847,
    name: "Güsten",
    plz: "39439"
  },
  {
    lati: 49.234539553, longi: 10.9353048342,
    name: "Abenberg",
    plz: "91183"
  },
  {
    lati: 54.5729391356, longi: 9.75500291535,
    name: "Ulsnis",
    plz: "24897"
  },
  {
    lati: 51.8836653198, longi: 10.0407810339,
    name: "Bad Gandersheim",
    plz: "37581"
  },
  {
    lati: 51.7464356021, longi: 11.9736678136,
    name: "Köthen",
    plz: "06366"
  },
  {
    lati: 50.7010076997, longi: 12.2420304278,
    name: "Mohlsdorf-Teichwolframsdorf",
    plz: "07987"
  },
  {
    lati: 51.2919164779, longi: 12.4721314895,
    name: "Leipzig",
    plz: "04288"
  },
  {
    lati: 48.9698848787, longi: 12.9999541382,
    name: "Zachenberg",
    plz: "94239"
  },
  {
    lati: 48.2945447034, longi: 8.1661002881,
    name: "Hausach",
    plz: "77756"
  },
  {
    lati: 48.9535458337, longi: 12.6032594734,
    name: "Steinach",
    plz: "94377"
  },
  {
    lati: 54.3741620209, longi: 12.9846061896,
    name: "Prohn",
    plz: "18445"
  },
  {
    lati: 48.5125043849, longi: 9.00608521093,
    name: "Rottenburg am Neckar",
    plz: "72108"
  },
  {
    lati: 48.2384549461, longi: 10.3039935768,
    name: "Breitenthal",
    plz: "86488"
  },
  {
    lati: 51.868937761, longi: 11.4573996391,
    name: "Hecklingen",
    plz: "39444"
  },
  {
    lati: 50.106952649, longi: 7.41442757008,
    name: "Kastellaun",
    plz: "56288"
  },
  {
    lati: 52.1308519096, longi: 7.76247429299,
    name: "Ladbergen",
    plz: "49549"
  },
  {
    lati: 53.1822225246, longi: 8.02837745568,
    name: "Bad Zwischenahn",
    plz: "26160"
  },
  {
    lati: 49.4323391332, longi: 8.25906617956,
    name: "Rödersheim-Gronau",
    plz: "67127"
  },
  {
    lati: 52.7707312543, longi: 8.40183138399,
    name: "Goldenstedt",
    plz: "49424"
  },
  {
    lati: 50.2410962857, longi: 6.47869251297,
    name: "Pronsfeld",
    plz: "54597"
  },
  {
    lati: 48.0060688291, longi: 9.2329931425,
    name: "Krauchenwies",
    plz: "72505"
  },
  {
    lati: 52.1865211449, longi: 10.1867641204,
    name: "Söhlde",
    plz: "31185"
  },
  {
    lati: 51.7717163915, longi: 11.4807220923,
    name: "Aschersleben",
    plz: "06449"
  },
  {
    lati: 48.174124612, longi: 12.171952352,
    name: "Haag i. OB",
    plz: "83527"
  },
  {
    lati: 47.9481489506, longi: 12.320214476,
    name: "Höslwang",
    plz: "83129"
  },
  {
    lati: 48.568899527, longi: 10.8196180944,
    name: "Westendorf, Kühlenthal",
    plz: "86707"
  },
  {
    lati: 51.5457205148, longi: 7.1642507073,
    name: "Herne",
    plz: "44653"
  },
  {
    lati: 48.1695369744, longi: 7.65002628573,
    name: "Wyhl",
    plz: "79369"
  },
  {
    lati: 50.3206589854, longi: 7.68164852557,
    name: "Dausenau, Nievern",
    plz: "56132"
  },
  {
    lati: 47.6432389948, longi: 7.77828302739,
    name: "Maulburg",
    plz: "79689"
  },
  {
    lati: 49.125784481, longi: 9.25007261928,
    name: "Heilbronn",
    plz: "74074"
  },
  {
    lati: 51.3872595935, longi: 9.27169436835,
    name: "Zierenberg",
    plz: "34289"
  },
  {
    lati: 53.192359227, longi: 8.7294827415,
    name: "Ritterhude",
    plz: "27721"
  },
  {
    lati: 54.5499195092, longi: 8.81116970306,
    name: "Nordstrand, Elisabeth-Sophien-Koog, Südfall",
    plz: "25845"
  },
  {
    lati: 47.5832214559, longi: 10.6636816877,
    name: "Füssen",
    plz: "87629"
  },
  {
    lati: 49.61607284, longi: 11.2252960204,
    name: "Igensdorf",
    plz: "91338"
  },
  {
    lati: 51.3808537068, longi: 14.2410209382,
    name: "Wittichenau",
    plz: "02997"
  },
  {
    lati: 50.0605147074, longi: 9.78712503207,
    name: "Karsbach",
    plz: "97783"
  },
  {
    lati: 48.8503362669, longi: 9.87104678704,
    name: "Leinzell",
    plz: "73575"
  },
  {
    lati: 52.3416110173, longi: 9.37299803963,
    name: "Bad Nenndorf",
    plz: "31542"
  },
  {
    lati: 50.5911101255, longi: 9.28266744419,
    name: "Lautertal",
    plz: "36369"
  },
  {
    lati: 49.9192652879, longi: 9.30069958726,
    name: "Mespelbrunn",
    plz: "63875"
  },
  {
    lati: 51.7331523488, longi: 9.5069829897,
    name: "Boffzen, Derental",
    plz: "37691"
  },
  {
    lati: 49.2392764507, longi: 9.06454419865,
    name: "Bad Rappenau",
    plz: "74906"
  },
  {
    lati: 49.1720659795, longi: 8.42100618095,
    name: "Dettenheim",
    plz: "76706"
  },
  {
    lati: 49.9108802105, longi: 8.47699215801,
    name: "Groß-Gerau",
    plz: "64521"
  },
  {
    lati: 49.4587445792, longi: 8.55557755317,
    name: "Mannheim",
    plz: "68239"
  },
  {
    lati: 47.6594568641, longi: 10.3556027806,
    name: "Sulzberg",
    plz: "87477"
  },
  {
    lati: 50.7370906088, longi: 11.8325028565,
    name: "Triptis",
    plz: "07819"
  },
  {
    lati: 51.5966942503, longi: 12.3311135358,
    name: "Bitterfeld-Wolfen",
    plz: "06808"
  },
  {
    lati: 49.1446104092, longi: 12.4559220276,
    name: "Roding",
    plz: "93426"
  },
  {
    lati: 48.3473189197, longi: 12.7291120481,
    name: "Mitterskirchen",
    plz: "84335"
  },
  {
    lati: 49.8990103312, longi: 9.77954822995,
    name: "Zellingen",
    plz: "97225"
  },
  {
    lati: 47.576736752, longi: 9.91671832696,
    name: "Weiler-Simmerberg",
    plz: "88171"
  },
  {
    lati: 50.0691125685, longi: 10.1703679502,
    name: "Niederwerrn",
    plz: "97464"
  },
  {
    lati: 54.1741568003, longi: 13.8118849943,
    name: "Karlshagen",
    plz: "17449"
  },
  {
    lati: 52.3946510414, longi: 14.1872408189,
    name: "Briesen, Rauen u.a.",
    plz: "15518"
  },
  {
    lati: 50.6574197958, longi: 6.7856138404,
    name: "Euskirchen",
    plz: "53879"
  },
  {
    lati: 49.8642222067, longi: 9.644888584,
    name: "Karbach",
    plz: "97842"
  },
  {
    lati: 52.4172725756, longi: 9.74673481578,
    name: "Hannover",
    plz: "30179"
  },
  {
    lati: 51.96506226, longi: 9.89337555164,
    name: "Sibbesse",
    plz: "31079"
  },
  {
    lati: 54.2741731447, longi: 10.1436550193,
    name: "Kiel",
    plz: "24145"
  },
  {
    lati: 53.9444393968, longi: 11.2003801994,
    name: "Boltenhagen",
    plz: "23946"
  },
  {
    lati: 52.2744868875, longi: 10.5876418459,
    name: "Braunschweig",
    plz: "38104"
  },
  {
    lati: 48.0288885526, longi: 10.739486516,
    name: "Buchloe",
    plz: "86807"
  },
  {
    lati: 49.9771985453, longi: 10.7780164367,
    name: "Lauter",
    plz: "96169"
  },
  {
    lati: 48.3984475784, longi: 8.97064417312,
    name: "Bodelshausen",
    plz: "72411"
  },
  {
    lati: 52.5527875606, longi: 7.8508573077,
    name: "Kettenkamp, Eggermühlen, Ankum",
    plz: "49577"
  },
  {
    lati: 50.5625455507, longi: 8.59526498921,
    name: "Wetzlar",
    plz: "35582"
  },
  {
    lati: 50.1049778929, longi: 8.76741958658,
    name: "Offenbach am Main",
    plz: "63065"
  },
  {
    lati: 48.2659139653, longi: 8.86240672367,
    name: "Balingen",
    plz: "72336"
  },
  {
    lati: 50.0280712915, longi: 8.42095149282,
    name: "Flörsheim am Main",
    plz: "65439"
  },
  {
    lati: 49.1986443906, longi: 9.23524395848,
    name: "Neckarsulm",
    plz: "74172"
  },
  {
    lati: 48.764811305, longi: 9.26036533927,
    name: "Stuttgart",
    plz: "70329"
  },
  {
    lati: 49.0636331198, longi: 12.4569344366,
    name: "Rettenbach",
    plz: "93191"
  },
  {
    lati: 51.3531208285, longi: 12.5408738855,
    name: "Borsdorf",
    plz: "04451"
  },
  {
    lati: 50.67441727, longi: 12.9497416378,
    name: "Thum",
    plz: "09419"
  },
  {
    lati: 50.8148621349, longi: 13.0182644912,
    name: "Chemnitz",
    plz: "09128"
  },
  {
    lati: 51.7302802901, longi: 7.55501578,
    name: "Nordkirchen",
    plz: "59394"
  },
  {
    lati: 51.6858595802, longi: 8.41182324771,
    name: "Lippstadt",
    plz: "59558"
  },
  {
    lati: 48.7392963453, longi: 8.63249399195,
    name: "Oberreichenbach",
    plz: "75394"
  },
  {
    lati: 48.6214009605, longi: 8.89688287893,
    name: "Nufringen",
    plz: "71154"
  },
  {
    lati: 48.8739840256, longi: 9.9272680834,
    name: "Schechingen",
    plz: "73579"
  },
  {
    lati: 52.1280287829, longi: 9.98914742062,
    name: "Hildesheim",
    plz: "31141"
  },
  {
    lati: 50.821909777, longi: 6.04972121163,
    name: "Aachen",
    plz: "52072"
  },
  {
    lati: 53.1479707356, longi: 7.44444915377,
    name: "Westoverledingen",
    plz: "26810"
  },
  {
    lati: 48.7596276476, longi: 8.04759871637,
    name: "Rheinmünster",
    plz: "77836"
  },
  {
    lati: 50.3918754521, longi: 8.10079511605,
    name: "Limburg",
    plz: "65552"
  },
  {
    lati: 49.751530812, longi: 6.63570639253,
    name: "Trier",
    plz: "54290"
  },
  {
    lati: 49.353510568, longi: 6.84061330143,
    name: "Saarwellingen",
    plz: "66793"
  },
  {
    lati: 51.7956083581, longi: 9.51304537277,
    name: "Holzminden",
    plz: "37603"
  },
  {
    lati: 49.4173195121, longi: 8.43413899036,
    name: "Neuhofen",
    plz: "67141"
  },
  {
    lati: 50.5158320484, longi: 8.73372504887,
    name: "Pohlheim",
    plz: "35415"
  },
  {
    lati: 49.2250763352, longi: 8.77312430721,
    name: "Angelbachtal",
    plz: "74918"
  },
  {
    lati: 48.3869420896, longi: 8.87607213786,
    name: "Rangendingen",
    plz: "72414"
  },
  {
    lati: 48.3973254414, longi: 10.6853854089,
    name: "Horgau",
    plz: "86497"
  },
  {
    lati: 47.6626665625, longi: 10.7063365905,
    name: "Roßhaupten",
    plz: "87672"
  },
  {
    lati: 47.8540189255, longi: 10.8969057404,
    name: "Hohenfurch",
    plz: "86978"
  },
  {
    lati: 51.9187921531, longi: 12.2599338552,
    name: "Dessau-Roßlau",
    plz: "06862"
  },
  {
    lati: 47.5742299337, longi: 10.1952485944,
    name: "Immenstadt im Allgäu",
    plz: "87509"
  },
  {
    lati: 51.2989707976, longi: 12.4280612754,
    name: "Leipzig",
    plz: "04289"
  },
  {
    lati: 50.7149514423, longi: 12.7023310382,
    name: "Oelsnitz/Erzgebirge",
    plz: "09376"
  },
  {
    lati: 48.6112796565, longi: 9.83632831511,
    name: "Geislingen an der Steige",
    plz: "73312"
  },
  {
    lati: 49.8098762847, longi: 12.4119473943,
    name: "Bärnau",
    plz: "95671"
  },
  {
    lati: 52.4964123113, longi: 13.362386019,
    name: "Berlin Schöneberg",
    plz: "10783"
  },
  {
    lati: 51.5300482273, longi: 11.0307623627,
    name: "Südharz, Berga",
    plz: "06536"
  },
  {
    lati: 53.9140915728, longi: 9.87423434025,
    name: "Bad Bramstedt",
    plz: "24576"
  },
  {
    lati: 50.4989171302, longi: 9.88620525496,
    name: "Poppenhausen",
    plz: "36163"
  },
  {
    lati: 54.2245375818, longi: 10.5130975569,
    name: "Grebin",
    plz: "24329"
  },
  {
    lati: 51.6718982084, longi: 11.3229835979,
    name: "Falkenstein, Arnstein",
    plz: "06543"
  },
  {
    lati: 49.9057965999, longi: 8.82069658014,
    name: "Dieburg",
    plz: "64807"
  },
  {
    lati: 49.8170543523, longi: 8.36071467425,
    name: "Mommenheim",
    plz: "55278"
  },
  {
    lati: 48.0484138735, longi: 8.20919137503,
    name: "Furtwangen im Schwarzwald",
    plz: "78120"
  },
  {
    lati: 51.5001392602, longi: 6.89284721581,
    name: "Oberhausen",
    plz: "46117"
  },
  {
    lati: 52.860361501, longi: 7.69223889259,
    name: "Werlte, Vrees, Lahn",
    plz: "49757"
  },
  {
    lati: 51.6298914717, longi: 13.4933652377,
    name: "Doberlug-Kirchhain",
    plz: "03253"
  },
  {
    lati: 51.6884226354, longi: 14.4321042616,
    name: "Neuhausen/Spree",
    plz: "03058"
  },
  {
    lati: 48.9358463122, longi: 9.26520053627,
    name: "Marbach am Neckar",
    plz: "71672"
  },
  {
    lati: 53.5001402122, longi: 9.57893523055,
    name: "Horneburg",
    plz: "21640"
  },
  {
    lati: 53.8483291276, longi: 11.1708497842,
    name: "Grevesmühlen, Stepenitztal, Upahl u.a.",
    plz: "23936"
  },
  {
    lati: 50.029849935, longi: 11.1362361719,
    name: "Wattendorf",
    plz: "96196"
  },
  {
    lati: 52.7340792615, longi: 8.14529737776,
    name: "Bakum",
    plz: "49456"
  },
  {
    lati: 53.3793096158, longi: 9.02315264221,
    name: "Gnarrenburg",
    plz: "27442"
  },
  {
    lati: 54.5984745442, longi: 9.18168215552,
    name: "Haselund",
    plz: "25855"
  },
  {
    lati: 49.4009632332, longi: 10.684914711,
    name: "Dietenhofen",
    plz: "90599"
  },
  {
    lati: 54.0039938119, longi: 9.75590491583,
    name: "Fitzbek",
    plz: "25579"
  },
  {
    lati: 49.6386882436, longi: 10.1425308392,
    name: "Marktbreit",
    plz: "97340"
  },
  {
    lati: 54.3226429732, longi: 10.1343802684,
    name: "Kiel",
    plz: "24103"
  },
  {
    lati: 50.7585177611, longi: 12.7516344725,
    name: "Lugau/Erzgeb.",
    plz: "09385"
  },
  {
    lati: 48.0561639269, longi: 8.35725725239,
    name: "Unterkirnach",
    plz: "78089"
  },
  {
    lati: 53.0822274346, longi: 8.85473448101,
    name: "Bremen",
    plz: "28211"
  },
  {
    lati: 52.3512042685, longi: 13.4018704315,
    name: "Blankenfelde-Mahlow u.a.",
    plz: "15831"
  },
  {
    lati: 50.6512250152, longi: 12.8314418677,
    name: "Zwönitz",
    plz: "08297"
  },
  {
    lati: 50.9450188599, longi: 7.29522745222,
    name: "Overath",
    plz: "51491"
  },
  {
    lati: 52.3916574525, longi: 7.48049822936,
    name: "Lünne, Schapen, Spelle",
    plz: "48480"
  },
  {
    lati: 50.3336749506, longi: 6.51696819991,
    name: "Stadtkyll",
    plz: "54589"
  },
  {
    lati: 51.4717995416, longi: 9.85423712755,
    name: "Rosdorf",
    plz: "37124"
  },
  {
    lati: 52.3240596514, longi: 9.80490135057,
    name: "Hannover",
    plz: "30521"
  },
  {
    lati: 49.1752966472, longi: 10.3213499058,
    name: "Feuchtwangen",
    plz: "91555"
  },
  {
    lati: 48.7097400302, longi: 8.73580896428,
    name: "Calw",
    plz: "75365"
  },
  {
    lati: 50.9217617039, longi: 8.53286976342,
    name: "Biedenkopf",
    plz: "35216"
  },
  {
    lati: 49.8696493103, longi: 8.31757013251,
    name: "Nierstein",
    plz: "55283"
  },
  {
    lati: 50.0921731294, longi: 9.93634000491,
    name: "Fuchsstadt",
    plz: "97727"
  },
  {
    lati: 53.8670999787, longi: 12.5797757194,
    name: "Jördenstorf, Prebberede u.a.",
    plz: "17168"
  },
  {
    lati: 51.0111417903, longi: 11.3026404402,
    name: "Weimar",
    plz: "99427"
  },
  {
    lati: 48.8369271286, longi: 13.3316624374,
    name: "Schönberg",
    plz: "94513"
  },
  {
    lati: 52.3384078655, longi: 13.8879535077,
    name: "Spreenhagen",
    plz: "15528"
  },
  {
    lati: 52.260376421, longi: 7.99150317221,
    name: "Osnabrück",
    plz: "49078"
  },
  {
    lati: 49.7271903433, longi: 8.29246566025,
    name: "Bechtheim",
    plz: "67595"
  },
  {
    lati: 48.8685315406, longi: 11.9306200684,
    name: "Saal a.d. Donau",
    plz: "93342"
  },
  {
    lati: 51.3719502863, longi: 12.3883910459,
    name: "Leipzig",
    plz: "04129"
  },
  {
    lati: 50.1051049461, longi: 11.5929024573,
    name: "Neuenmarkt",
    plz: "95339"
  },
  {
    lati: 54.1364514049, longi: 13.7496021479,
    name: "Kröslin, Krummin, Lassan u.a.",
    plz: "17440"
  },
  {
    lati: 52.7030599287, longi: 7.28613480427,
    name: "Meppen",
    plz: "49716"
  },
  {
    lati: 50.0686067998, longi: 7.27414001203,
    name: "Peterswald-Löffelscheid u.a.",
    plz: "56858"
  },
  {
    lati: 50.2291221995, longi: 7.48743728301,
    name: "Wildenbungert, Gondershausen, Nörtershausen u.a.",
    plz: "56283"
  },
  {
    lati: 49.4870973668, longi: 8.26001785467,
    name: "Birkenheide",
    plz: "67134"
  },
  {
    lati: 54.3105188431, longi: 8.99885317389,
    name: "Lunden",
    plz: "25774"
  },
  {
    lati: 51.6453988672, longi: 12.4498236288,
    name: "Muldestausee",
    plz: "06774"
  },
  {
    lati: 49.3070087913, longi: 8.07533150508,
    name: "Maikammer",
    plz: "67487"
  },
  {
    lati: 49.9115334551, longi: 10.8957875973,
    name: "Bamberg",
    plz: "96052"
  },
  {
    lati: 50.3248311829, longi: 11.4605689001,
    name: "Steinwiesen",
    plz: "96349"
  },
  {
    lati: 50.9746044121, longi: 14.6102948974,
    name: "Ebersbach-Neugersdorf",
    plz: "02727"
  },
  {
    lati: 50.9539862944, longi: 9.98193599668,
    name: "Wildeck",
    plz: "36208"
  },
  {
    lati: 53.3230722904, longi: 12.9732052441,
    name: "Möllenbeck",
    plz: "17237"
  },
  {
    lati: 53.3918771739, longi: 9.43995481387,
    name: "Ahlerstedt",
    plz: "21702"
  },
  {
    lati: 51.6770310715, longi: 7.9129983162,
    name: "Hamm",
    plz: "59071"
  },
  {
    lati: 53.5929332211, longi: 8.07239894585,
    name: "Wilhelmshaven",
    plz: "26388"
  },
  {
    lati: 49.4898979519, longi: 8.17253945367,
    name: "Kallstadt",
    plz: "67169"
  },
  {
    lati: 49.5677965976, longi: 8.96067769656,
    name: "Beerfelden",
    plz: "64743"
  },
  {
    lati: 50.9759461844, longi: 6.28226932201,
    name: "Linnich",
    plz: "52441"
  },
  {
    lati: 51.4908111406, longi: 6.60852710832,
    name: "Moers",
    plz: "47445"
  },
  {
    lati: 52.3332933906, longi: 10.5485029771,
    name: "Braunschweig",
    plz: "38110"
  },
  {
    lati: 53.6909144593, longi: 9.67694911876,
    name: "Uetersen",
    plz: "25436"
  },
  {
    lati: 49.559384395, longi: 9.95287606773,
    name: "Riedenheim",
    plz: "97283"
  },
  {
    lati: 49.2333432939, longi: 11.1161633579,
    name: "Roth",
    plz: "91154"
  },
  {
    lati: 48.395404711, longi: 13.3278716558,
    name: "Pocking",
    plz: "94060"
  },
  {
    lati: 48.0989790379, longi: 8.80838096989,
    name: "Böttingen",
    plz: "78583"
  },
  {
    lati: 51.8991410389, longi: 11.85631391,
    name: "Calbe, Rosenburg, Lödderitz etc",
    plz: "39240"
  },
  {
    lati: 49.1531051309, longi: 11.024428502,
    name: "Röttenbach",
    plz: "91187"
  },
  {
    lati: 48.4300495228, longi: 10.822174352,
    name: "Gersthofen",
    plz: "86368"
  },
  {
    lati: 49.6376745397, longi: 8.32691945846,
    name: "Worms",
    plz: "67549"
  },
  {
    lati: 51.9083334936, longi: 11.3682806903,
    name: "Börde-Hakel",
    plz: "39448"
  },
  {
    lati: 51.3584408604, longi: 6.71193579426,
    name: "Duisburg",
    plz: "47259"
  },
  {
    lati: 48.9103718944, longi: 9.06873808268,
    name: "Markgröningen",
    plz: "71706"
  },
  {
    lati: 48.5242005458, longi: 11.9021977089,
    name: "Mauern",
    plz: "85419"
  },
  {
    lati: 53.1380647469, longi: 7.78323130632,
    name: "Barßel",
    plz: "26676"
  },
  {
    lati: 48.7878990021, longi: 13.2558744796,
    name: "Zenting",
    plz: "94579"
  },
  {
    lati: 48.735032619, longi: 13.603877888,
    name: "Waldkirchen",
    plz: "94065"
  },
  {
    lati: 51.6826111584, longi: 6.97475717841,
    name: "Dorsten",
    plz: "46284"
  },
  {
    lati: 53.4514680845, longi: 9.46411736537,
    name: "Harsefeld",
    plz: "21698"
  },
  {
    lati: 50.3969142713, longi: 8.23008982938,
    name: "Villmar",
    plz: "65606"
  },
  {
    lati: 50.5530648431, longi: 8.33123824822,
    name: "Leun",
    plz: "35638"
  },
  {
    lati: 49.4485133011, longi: 8.39692854595,
    name: "Ludwigshafen am Rhein",
    plz: "67067"
  },
  {
    lati: 53.1367530557, longi: 8.59653717421,
    name: "Lemwerder",
    plz: "27809"
  },
  {
    lati: 50.299557743, longi: 7.75974374853,
    name: "Singhofen",
    plz: "56379"
  },
  {
    lati: 53.0804186967, longi: 8.80517608782,
    name: "Bremen",
    plz: "28195"
  },
  {
    lati: 48.543659024, longi: 9.90948176893,
    name: "Lonsee",
    plz: "89173"
  },
  {
    lati: 50.1474367028, longi: 11.7011239268,
    name: "Stammbach",
    plz: "95236"
  },
  {
    lati: 50.3117172887, longi: 11.111256151,
    name: "Neustadt b. Coburg",
    plz: "96465"
  },
  {
    lati: 49.376020817, longi: 12.1637993566,
    name: "Schwarzenfeld",
    plz: "92521"
  },
  {
    lati: 48.9691066322, longi: 12.6683150439,
    name: "Mitterfels",
    plz: "94360"
  },
  {
    lati: 48.1877844942, longi: 12.6867932705,
    name: "Kastl",
    plz: "84556"
  },
  {
    lati: 52.3017514766, longi: 9.33151697336,
    name: "Apelern, Rodenberg",
    plz: "31552"
  },
  {
    lati: 48.7526849897, longi: 9.28313380378,
    name: "Esslingen am Neckar",
    plz: "73733"
  },
  {
    lati: 48.561247647, longi: 9.33268483951,
    name: "Kohlberg",
    plz: "72664"
  },
  {
    lati: 52.3474339248, longi: 9.12113549964,
    name: "Meerbeck",
    plz: "31715"
  },
  {
    lati: 50.0886293623, longi: 6.42912106067,
    name: "Pronsfeld",
    plz: "54597"
  },
  {
    lati: 51.4839566165, longi: 7.18178854515,
    name: "Bochum",
    plz: "44793"
  },
  {
    lati: 49.3326496363, longi: 7.74073531535,
    name: "Schmalenberg",
    plz: "67718"
  },
  {
    lati: 47.9943491852, longi: 12.8227478017,
    name: "Fridolfing",
    plz: "83413"
  },
  {
    lati: 49.5940801762, longi: 10.7699444385,
    name: "Oberreichenbach",
    plz: "91097"
  },
  {
    lati: 49.1358840329, longi: 10.1911993887,
    name: "Kreßberg",
    plz: "74594"
  },
  {
    lati: 48.3733721393, longi: 10.3674777576,
    name: "Kammeltal",
    plz: "89358"
  },
  {
    lati: 51.8709984426, longi: 10.4712966076,
    name: "Goslar",
    plz: "38644"
  },
  {
    lati: 51.1622893261, longi: 13.4783723376,
    name: "Meißen",
    plz: "01662"
  },
  {
    lati: 51.6651249371, longi: 14.2167729454,
    name: "Drebkau",
    plz: "03116"
  },
  {
    lati: 50.0570205832, longi: 11.6094140635,
    name: "Himmelkron",
    plz: "95502"
  },
  {
    lati: 49.8116027424, longi: 12.1559576628,
    name: "Windischeschenbach",
    plz: "92670"
  },
  {
    lati: 49.3449298522, longi: 12.3603789865,
    name: "Neunburg vorm Wald",
    plz: "92431"
  },
  {
    lati: 51.9144214216, longi: 10.3245360322,
    name: "Langelsheim",
    plz: "38685"
  },
  {
    lati: 49.898738348, longi: 10.3357923628,
    name: "Gerolzhofen",
    plz: "97447"
  },
  {
    lati: 53.6942954245, longi: 13.2401124468,
    name: "Altentreptow",
    plz: "17087"
  },
  {
    lati: 51.4245892489, longi: 13.8751239361,
    name: "Ruhland",
    plz: "01945"
  },
  {
    lati: 48.7832966608, longi: 12.9009196613,
    name: "Plattling",
    plz: "94447"
  },
  {
    lati: 51.5256483721, longi: 6.73688045431,
    name: "Duisburg",
    plz: "47179"
  },
  {
    lati: 49.820040946, longi: 6.91548613713,
    name: "Trittenheim",
    plz: "54349"
  },
  {
    lati: 49.9510282336, longi: 7.14356867501,
    name: "Irmenach",
    plz: "56843"
  },
  {
    lati: 50.3479418337, longi: 7.3848843939,
    name: "Ochtendung",
    plz: "56299"
  },
  {
    lati: 49.0090138382, longi: 9.05226049751,
    name: "Freudental",
    plz: "74392"
  },
  {
    lati: 49.8069318422, longi: 9.17662523778,
    name: "Erlenbach a.Main",
    plz: "63906"
  },
  {
    lati: 53.5220082883, longi: 9.84815048516,
    name: "Hamburg",
    plz: "21129"
  },
  {
    lati: 52.1458353081, longi: 9.95262399313,
    name: "Hildesheim",
    plz: "31134"
  },
  {
    lati: 48.1343752242, longi: 10.0748538245,
    name: "Kirchberg an der Iller",
    plz: "88486"
  },
  {
    lati: 53.7539160495, longi: 12.3945294475,
    name: "Lalendorf, Langhagen",
    plz: "18279"
  },
  {
    lati: 50.8291960106, longi: 11.9387668915,
    name: "Münchenbernsdorf, Schwarzbach, Bocka",
    plz: "07589"
  },
  {
    lati: 50.4274561469, longi: 7.80578701385,
    name: "Montabaur",
    plz: "56410"
  },
  {
    lati: 47.990320732, longi: 8.101082733,
    name: "St. Märgen",
    plz: "79274"
  },
  {
    lati: 50.2465760726, longi: 8.56492088191,
    name: "Bad Homburg v.d. Höhe",
    plz: "61350"
  },
  {
    lati: 52.0426644093, longi: 8.57799057126,
    name: "Bielefeld",
    plz: "33609"
  },
  {
    lati: 53.5382474988, longi: 8.59508841459,
    name: "Bremerhaven",
    plz: "27570"
  },
  {
    lati: 51.6735808528, longi: 6.54067732102,
    name: "Wesel",
    plz: "46487"
  },
  {
    lati: 47.7873631983, longi: 11.2219250316,
    name: "Eberfing",
    plz: "82390"
  },
  {
    lati: 53.0584567674, longi: 11.4338473229,
    name: "Höhbeck",
    plz: "29478"
  },
  {
    lati: 50.7642437064, longi: 9.2993051806,
    name: "Alsfeld",
    plz: "36304"
  },
  {
    lati: 48.6267051167, longi: 12.3414191224,
    name: "Wörth a.d. Isar",
    plz: "84109"
  },
  {
    lati: 48.8459351185, longi: 10.6367596932,
    name: "Alerheim",
    plz: "86733"
  },
  {
    lati: 49.2782184424, longi: 6.96756785539,
    name: "Saarbrücken",
    plz: "66115"
  },
  {
    lati: 53.1466061531, longi: 7.61324660505,
    name: "Ostrhauderfehn",
    plz: "26842"
  },
  {
    lati: 48.7933719906, longi: 10.0215918142,
    name: "Essingen",
    plz: "73457"
  },
  {
    lati: 54.282936871, longi: 10.020953501,
    name: "Mielkendorf",
    plz: "24247"
  },
  {
    lati: 48.1402009345, longi: 10.1780864834,
    name: "Osterberg",
    plz: "89296"
  },
  {
    lati: 48.2697974952, longi: 12.6557986932,
    name: "Winhöring",
    plz: "84543"
  },
  {
    lati: 49.9162372406, longi: 8.59342060846,
    name: "Weiterstadt",
    plz: "64331"
  },
  {
    lati: 49.5399041166, longi: 8.19054732017,
    name: "Kirchheim an der Weinstraße",
    plz: "67281"
  },
  {
    lati: 52.9666646947, longi: 8.93971507107,
    name: "Riede",
    plz: "27339"
  },
  {
    lati: 48.8429602307, longi: 9.37643490056,
    name: "Korb",
    plz: "71404"
  },
  {
    lati: 49.2813744621, longi: 9.53187296756,
    name: "Forchtenberg",
    plz: "74670"
  },
  {
    lati: 50.6149581297, longi: 11.205620092,
    name: "Döschnitz, Sitzendorf, Rohrbach",
    plz: "07429"
  },
  {
    lati: 47.6317404262, longi: 11.2394558179,
    name: "Ohlstadt",
    plz: "82441"
  },
  {
    lati: 50.7970548519, longi: 6.50923996607,
    name: "Düren",
    plz: "52351"
  },
  {
    lati: 49.6783764928, longi: 6.60667687689,
    name: "Konz",
    plz: "54329"
  },
  {
    lati: 50.8655957995, longi: 7.24830582369,
    name: "Lohmar",
    plz: "53797"
  },
  {
    lati: 49.6594573016, longi: 8.20372518263,
    name: "Flörsheim-Dalsheim",
    plz: "67592"
  },
  {
    lati: 50.5160632467, longi: 7.46291879645,
    name: "Melsbach",
    plz: "56581"
  },
  {
    lati: 49.1409616883, longi: 8.53648668199,
    name: "Karlsdorf-Neuthard",
    plz: "76689"
  },
  {
    lati: 51.8135252973, longi: 14.3090446358,
    name: "Cottbus",
    plz: "03055"
  },
  {
    lati: 50.3797236885, longi: 11.6696466906,
    name: "Lichtenberg",
    plz: "95192"
  },
  {
    lati: 53.5555422586, longi: 9.93496378789,
    name: "Hamburg",
    plz: "22765"
  },
  {
    lati: 49.9157723218, longi: 9.06483489515,
    name: "Großostheim",
    plz: "63762"
  },
  {
    lati: 50.7399474446, longi: 7.098889425,
    name: "Bonn",
    plz: "53111"
  },
  {
    lati: 49.4845573549, longi: 12.4485207812,
    name: "Oberviechtach",
    plz: "92526"
  },
  {
    lati: 53.0543957956, longi: 8.61146891997,
    name: "Delmenhorst",
    plz: "27753"
  },
  {
    lati: 52.1955898996, longi: 13.4585409374,
    name: "Zossen",
    plz: "15806"
  },
  {
    lati: 49.0305004873, longi: 10.2688592376,
    name: "Wört",
    plz: "73499"
  },
  {
    lati: 47.7802160402, longi: 10.3091308851,
    name: "Lauben",
    plz: "87493"
  },
  {
    lati: 52.5045451634, longi: 10.5245739614,
    name: "Gifhorn",
    plz: "38518"
  },
  {
    lati: 51.5091303199, longi: 10.8098685305,
    name: "Nordhausen",
    plz: "99734"
  },
  {
    lati: 51.2568540209, longi: 7.55873559647,
    name: "Schalksmühle",
    plz: "58579"
  },
  {
    lati: 54.6377495719, longi: 8.59727095721,
    name: "Langeneß",
    plz: "25863"
  },
  {
    lati: 48.9445148796, longi: 11.2114418155,
    name: "Pollenfeld",
    plz: "85131"
  },
  {
    lati: 49.4854853552, longi: 10.9654438814,
    name: "Fürth",
    plz: "90766"
  },
  {
    lati: 54.0684738644, longi: 12.3831455375,
    name: "Sanitz",
    plz: "18190"
  },
  {
    lati: 51.1153381226, longi: 12.5022256917,
    name: "Borna",
    plz: "04552"
  },
  {
    lati: 52.4197361883, longi: 13.1440100861,
    name: "Berlin Wannsee",
    plz: "14109"
  },
  {
    lati: 52.0824933041, longi: 13.7045057583,
    name: "Halbe",
    plz: "15757"
  },
  {
    lati: 53.557973298, longi: 9.84980841681,
    name: "Hamburg",
    plz: "22609"
  },
  {
    lati: 49.0102523387, longi: 10.0300430894,
    name: "Rosenberg",
    plz: "73494"
  },
  {
    lati: 48.5979457889, longi: 10.2649613121,
    name: "Hermaringen",
    plz: "89568"
  },
  {
    lati: 52.094614862, longi: 8.2862846983,
    name: "Borgholzhausen",
    plz: "33829"
  },
  {
    lati: 49.8849442998, longi: 8.7059252915,
    name: "Darmstadt",
    plz: "64287"
  },
  {
    lati: 49.9736108351, longi: 8.80674544974,
    name: "Rödermark",
    plz: "63322"
  },
  {
    lati: 54.4988871989, longi: 8.88297240397,
    name: "Nordstrand, Elisabeth-Sophien-Koog, Südfall",
    plz: "25845"
  },
  {
    lati: 48.6886480329, longi: 11.1642565063,
    name: "Rohrenfels",
    plz: "86701"
  },
  {
    lati: 49.5192566596, longi: 11.3742100474,
    name: "Reichenschwand",
    plz: "91244"
  },
  {
    lati: 53.9934830954, longi: 11.4362483259,
    name: "Insel Poel",
    plz: "23999"
  },
  {
    lati: 48.1099539189, longi: 11.6404177327,
    name: "München",
    plz: "81735"
  },
  {
    lati: 51.2100585383, longi: 11.9391714666,
    name: "Weißenfels, Stößen",
    plz: "06667"
  },
  {
    lati: 51.0097962234, longi: 6.54510624121,
    name: "Bedburg",
    plz: "50181"
  },
  {
    lati: 49.7686084276, longi: 6.79535795313,
    name: "Fell",
    plz: "54341"
  },
  {
    lati: 51.9895864615, longi: 7.31444419963,
    name: "Billerbeck",
    plz: "48727"
  },
  {
    lati: 47.6370738216, longi: 7.69519958306,
    name: "Lörrach",
    plz: "79541"
  },
  {
    lati: 50.1344521542, longi: 8.04585163073,
    name: "Bad Schwalbach",
    plz: "65307"
  },
  {
    lati: 52.1991770713, longi: 8.72244328342,
    name: "Löhne",
    plz: "32584"
  },
  {
    lati: 53.6471998045, longi: 10.118196396,
    name: "Hamburg",
    plz: "22393"
  },
  {
    lati: 48.19622215, longi: 10.1891578192,
    name: "Unterroth",
    plz: "89299"
  },
  {
    lati: 49.528777501, longi: 12.2608958694,
    name: "Trausnitz",
    plz: "92555"
  },
  {
    lati: 49.9383939867, longi: 11.7359566785,
    name: "Weidenberg, Kirchenpingarten",
    plz: "95466"
  },
  {
    lati: 48.4613696711, longi: 12.5614914678,
    name: "Gangkofen",
    plz: "84140"
  },
  {
    lati: 52.4810115071, longi: 13.5796111861,
    name: "Berlin Mahlsdorf",
    plz: "12623"
  },
  {
    lati: 51.7542294795, longi: 14.1990791004,
    name: "Kolkwitz",
    plz: "03099"
  },
  {
    lati: 51.2478948221, longi: 6.78202155254,
    name: "Düsseldorf",
    plz: "40476"
  },
  {
    lati: 51.3340721931, longi: 9.53393446793,
    name: "Kassel",
    plz: "34125"
  },
  {
    lati: 47.6325926094, longi: 8.4945982948,
    name: "Dettighofen",
    plz: "79802"
  },
  {
    lati: 50.9862147921, longi: 8.55476296112,
    name: "Hatzfeld",
    plz: "35116"
  },
  {
    lati: 47.6959088613, longi: 8.76691638126,
    name: "Gailingen am Hochrhein",
    plz: "78262"
  },
  {
    lati: 48.1445735176, longi: 8.81573614433,
    name: "Wehingen, Reichenbach",
    plz: "78564"
  },
  {
    lati: 52.2806136665, longi: 10.4575069365,
    name: "Braunschweig",
    plz: "38116"
  },
  {
    lati: 48.0636893058, longi: 10.5824837181,
    name: "Rammingen",
    plz: "86871"
  },
  {
    lati: 49.6118019968, longi: 9.82976175963,
    name: "Wittighausen",
    plz: "97957"
  },
  {
    lati: 49.1334368546, longi: 8.73314862184,
    name: "Kraichtal",
    plz: "76703"
  },
  {
    lati: 49.4164763427, longi: 7.62909800097,
    name: "Kindsbach",
    plz: "66862"
  },
  {
    lati: 47.8697952044, longi: 7.733518863,
    name: "Staufen im Breisgau",
    plz: "79219"
  },
  {
    lati: 50.7463896926, longi: 7.81104814904,
    name: "Gebhardshain",
    plz: "57580"
  },
  {
    lati: 47.8127947834, longi: 8.32483255555,
    name: "Bonndorf im Schwarzwald",
    plz: "79848"
  },
  {
    lati: 48.4921527551, longi: 8.24424697695,
    name: "Freudenstadt",
    plz: "72250"
  },
  {
    lati: 50.0773174085, longi: 10.3488453927,
    name: "Schonungen",
    plz: "97453"
  },
  {
    lati: 52.4486002109, longi: 10.435060816,
    name: "Leiferde",
    plz: "38542"
  },
  {
    lati: 48.4623599629, longi: 10.583206337,
    name: "Altenmünster",
    plz: "86450"
  },
  {
    lati: 48.6020353804, longi: 12.3831355625,
    name: "Niederviehbach",
    plz: "84183"
  },
  {
    lati: 49.6857565124, longi: 8.04627710641,
    name: "Bischheim u.a.",
    plz: "67294"
  },
  {
    lati: 52.5455958569, longi: 8.62438863128,
    name: "Wagenfeld",
    plz: "49419"
  },
  {
    lati: 52.9100451217, longi: 8.84634643076,
    name: "Syke",
    plz: "28857"
  },
  {
    lati: 47.6462303306, longi: 7.65109566598,
    name: "Rümmingen",
    plz: "79595"
  },
  {
    lati: 53.5503245752, longi: 9.96439808248,
    name: "Hamburg",
    plz: "20359"
  },
  {
    lati: 49.5834733828, longi: 11.7120968698,
    name: "Edelsfeld",
    plz: "92265"
  },
  {
    lati: 51.4519564743, longi: 10.8968011716,
    name: "Heringen/ Helme",
    plz: "99765"
  },
  {
    lati: 49.0457654269, longi: 9.14162528692,
    name: "Kirchheim am Neckar",
    plz: "74366"
  },
  {
    lati: 47.8017150986, longi: 11.006629948,
    name: "Hohenpeißenberg",
    plz: "82383"
  },
  {
    lati: 50.9692851418, longi: 13.5221224765,
    name: "Tharandt u.a.",
    plz: "01737"
  },
  {
    lati: 52.3604824938, longi: 13.5973370277,
    name: "Schulzendorf b. Eichenwade",
    plz: "15732"
  },
  {
    lati: 49.6644143529, longi: 10.2418714715,
    name: "Rödelsee",
    plz: "97348"
  },
  {
    lati: 50.2778188286, longi: 11.4996342947,
    name: "Wallenfels",
    plz: "96346"
  },
  {
    lati: 51.7069889956, longi: 9.83342369661,
    name: "Moringen",
    plz: "37186"
  },
  {
    lati: 48.2894202855, longi: 9.80722952124,
    name: "Öpfingen",
    plz: "89614"
  },
  {
    lati: 50.3219284029, longi: 9.88496011497,
    name: "Riedenberg",
    plz: "97792"
  },
  {
    lati: 49.9880843986, longi: 8.27092554409,
    name: "Mainz",
    plz: "55131"
  },
  {
    lati: 48.5648139488, longi: 8.6177095644,
    name: "Egenhausen",
    plz: "72227"
  },
  {
    lati: 50.1269810317, longi: 8.75528909177,
    name: "Frankfurt am Main",
    plz: "60386"
  },
  {
    lati: 49.1374896808, longi: 8.90714418392,
    name: "Eppingen",
    plz: "75031"
  },
  {
    lati: 54.1481523175, longi: 9.18284597818,
    name: "Nordhastedt",
    plz: "25785"
  },
  {
    lati: 49.7180208729, longi: 9.90628511727,
    name: "Reichenberg, Guttenberger Wald",
    plz: "97234"
  },
  {
    lati: 48.6786446182, longi: 10.3121922569,
    name: "Zöschingen",
    plz: "89447"
  },
  {
    lati: 49.6400666297, longi: 7.16754558371,
    name: "Birkenfeld u.a.",
    plz: "55765"
  },
  {
    lati: 50.5923717143, longi: 12.6208930913,
    name: "Schneeberg",
    plz: "08289"
  },
  {
    lati: 49.0756899787, longi: 12.17720737,
    name: "Wenzenbach",
    plz: "93173"
  },
  {
    lati: 51.4108967146, longi: 12.2453900106,
    name: "Schkeuditz",
    plz: "04435"
  },
  {
    lati: 52.0899457824, longi: 12.4291542597,
    name: "Wiesenburg",
    plz: "14827"
  },
  {
    lati: 52.4869683962, longi: 13.4663173603,
    name: "Berlin Alt Treptow",
    plz: "12435"
  },
  {
    lati: 48.4076251215, longi: 11.4768179184,
    name: "Petershausen",
    plz: "85238"
  },
  {
    lati: 54.1764572452, longi: 9.76263522755,
    name: "Bargstedt, Brammer, Oldenbüttel",
    plz: "24793"
  },
  {
    lati: 49.0566708652, longi: 9.34429281919,
    name: "Beilstein",
    plz: "71717"
  },
  {
    lati: 50.1434732755, longi: 11.2873808844,
    name: "Burgkunstadt",
    plz: "96224"
  },
  {
    lati: 51.7981235985, longi: 12.2560446643,
    name: "Dessau-Roßlau",
    plz: "06849"
  },
  {
    lati: 48.9043705107, longi: 12.4019261214,
    name: "Mötzing",
    plz: "93099"
  },
  {
    lati: 49.0267921445, longi: 13.1525338762,
    name: "Langdorf",
    plz: "94264"
  },
  {
    lati: 52.1635682058, longi: 13.6920203346,
    name: "Groß Köris",
    plz: "15746"
  },
  {
    lati: 50.7095826501, longi: 7.00664211854,
    name: "Alfter",
    plz: "53347"
  },
  {
    lati: 50.8418920493, longi: 7.15614847426,
    name: "Troisdorf",
    plz: "53842"
  },
  {
    lati: 51.5466254623, longi: 7.21591380956,
    name: "Herne",
    plz: "44623"
  },
  {
    lati: 51.4965099305, longi: 6.67899749453,
    name: "Duisburg",
    plz: "47199"
  },
  {
    lati: 53.0113146781, longi: 11.4362095092,
    name: "Gartow",
    plz: "29471"
  },
  {
    lati: 52.8563104411, longi: 8.03661504674,
    name: "Cloppenburg",
    plz: "49661"
  },
  {
    lati: 49.7829541098, longi: 8.45484697891,
    name: "Biebesheim am Rhein",
    plz: "64584"
  },
  {
    lati: 49.1539570009, longi: 9.30244659444,
    name: "Weinsberg",
    plz: "74189"
  },
  {
    lati: 52.3230560896, longi: 9.58816830772,
    name: "Gehrden",
    plz: "30989"
  },
  {
    lati: 53.5864944559, longi: 10.0694110984,
    name: "Hamburg",
    plz: "22049"
  },
  {
    lati: 52.5253535712, longi: 10.1441869053,
    name: "Wathlingen",
    plz: "29339"
  },
  {
    lati: 53.0672553432, longi: 10.3619387547,
    name: "Hanstedt",
    plz: "29582"
  },
  {
    lati: 49.5757715341, longi: 10.7376970373,
    name: "Wilhelmsdorf",
    plz: "91489"
  },
  {
    lati: 48.3942722054, longi: 10.8786844609,
    name: "Augsburg",
    plz: "86154"
  },
  {
    lati: 48.9907610815, longi: 12.1022400609,
    name: "Regensburg",
    plz: "93053"
  },
  {
    lati: 53.6453453021, longi: 9.6006049163,
    name: "Haseldorf",
    plz: "25489"
  },
  {
    lati: 50.248361577, longi: 9.85392408338,
    name: "Schondra",
    plz: "97795"
  },
  {
    lati: 50.2119072541, longi: 11.20336442,
    name: "Schneckenlohe",
    plz: "96277"
  },
  {
    lati: 49.7562445371, longi: 7.00513616419,
    name: "Thalfang",
    plz: "54424"
  },
  {
    lati: 50.056936831, longi: 7.35062297067,
    name: "Mastershausen",
    plz: "56869"
  },
  {
    lati: 49.389887676, longi: 8.8262217755,
    name: "Neckargemünd",
    plz: "69151"
  },
  {
    lati: 51.2539082933, longi: 6.31962025732,
    name: "Viersen",
    plz: "41751"
  },
  {
    lati: 49.9161625984, longi: 7.32329436471,
    name: "Sohren",
    plz: "55487"
  },
  {
    lati: 50.6500145691, longi: 12.5965841019,
    name: "Langenweißbach, Wildenfels",
    plz: "08134"
  },
  {
    lati: 50.4344177835, longi: 12.6947458592,
    name: "Johanngeorgenstadt",
    plz: "08349"
  },
  {
    lati: 51.7902067029, longi: 12.0143637619,
    name: "Aken, Osternienburger Land, Südliches Anhalt",
    plz: "06386"
  },
  {
    lati: 47.9521647527, longi: 7.7886039116,
    name: "Ebringen",
    plz: "79285"
  },
  {
    lati: 52.3849180219, longi: 13.2735350881,
    name: "Teltow",
    plz: "14513"
  },
  {
    lati: 52.4901833301, longi: 13.4164417744,
    name: "Berlin Kreuzberg",
    plz: "10967"
  },
  {
    lati: 49.4917447389, longi: 7.90785248586,
    name: "Enkenbach-Alsenborn",
    plz: "67677"
  },
  {
    lati: 49.3671975848, longi: 7.11654237078,
    name: "Schiffweiler",
    plz: "66578"
  },
  {
    lati: 51.3501398462, longi: 7.06073176505,
    name: "Velbert",
    plz: "42551"
  },
  {
    lati: 54.5722516789, longi: 9.51177905744,
    name: "Idstedt",
    plz: "24879"
  },
  {
    lati: 51.9019295893, longi: 9.16829841618,
    name: "Schieder-Schwalenberg",
    plz: "32816"
  },
  {
    lati: 51.6091287174, longi: 8.8942855118,
    name: "Lichtenau",
    plz: "33165"
  },
  {
    lati: 49.9118734295, longi: 9.62199677396,
    name: "Roden",
    plz: "97849"
  },
  {
    lati: 51.2165444051, longi: 10.0717710642,
    name: "Meinhard",
    plz: "37276"
  },
  {
    lati: 53.5484453944, longi: 10.0835330336,
    name: "Hamburg",
    plz: "22111"
  },
  {
    lati: 53.342024146, longi: 10.3388709394,
    name: "Handorf",
    plz: "21447"
  },
  {
    lati: 48.4346251781, longi: 10.5162613002,
    name: "Landensberg",
    plz: "89361"
  },
  {
    lati: 52.238255176, longi: 14.6049517764,
    name: "Brieskow-Finkenheerd",
    plz: "15295"
  },
  {
    lati: 48.8699120616, longi: 11.3700947137,
    name: "Böhmfeld",
    plz: "85113"
  },
  {
    lati: 52.4062537582, longi: 12.5131825509,
    name: "Brandenburg/ Havel",
    plz: "14770"
  },
  {
    lati: 54.2866360041, longi: 9.14699187151,
    name: "Hennstedt",
    plz: "25779"
  },
  {
    lati: 54.0579738057, longi: 8.68739454083,
    name: "Friedrichskoog",
    plz: "25718"
  },
  {
    lati: 49.0346047649, longi: 8.18648560146,
    name: "Wörth",
    plz: "76744"
  },
  {
    lati: 48.9994479946, longi: 8.41427200764,
    name: "Karlsruhe",
    plz: "76137"
  },
  {
    lati: 49.7513695947, longi: 8.51266718686,
    name: "Gernsheim",
    plz: "64579"
  },
  {
    lati: 49.0552973013, longi: 8.53615757128,
    name: "Weingarten",
    plz: "76356"
  },
  {
    lati: 52.642957752, longi: 9.81866847953,
    name: "Wietze",
    plz: "29323"
  },
  {
    lati: 47.9254555539, longi: 12.9005317965,
    name: "Laufen",
    plz: "83410"
  },
  {
    lati: 50.0005304304, longi: 11.4016049609,
    name: "Thurnau",
    plz: "95349"
  },
  {
    lati: 49.0067125536, longi: 10.5904148954,
    name: "Auhausen",
    plz: "86736"
  },
  {
    lati: 48.8484350362, longi: 13.2726629812,
    name: "Innernzell",
    plz: "94548"
  },
  {
    lati: 52.5598715211, longi: 13.3851033604,
    name: "Berlin Gesundbrunnen",
    plz: "13359"
  },
  {
    lati: 48.7533261393, longi: 13.5166386013,
    name: "Röhrnbach",
    plz: "94133"
  },
  {
    lati: 48.1747755004, longi: 11.5180235173,
    name: "München",
    plz: "80992"
  },
  {
    lati: 49.7007545191, longi: 11.3308784822,
    name: "Obertrubach",
    plz: "91286"
  },
  {
    lati: 51.205736636, longi: 8.51816066721,
    name: "Winterberg",
    plz: "59955"
  },
  {
    lati: 51.4787604924, longi: 7.06948673886,
    name: "Essen",
    plz: "45309"
  },
  {
    lati: 51.4471275647, longi: 7.22818042379,
    name: "Bochum",
    plz: "44801"
  },
  {
    lati: 49.6419320209, longi: 9.17717248074,
    name: "Amorbach",
    plz: "63916"
  },
  {
    lati: 48.3823997062, longi: 9.19231500548,
    name: "Sonnenbühl",
    plz: "72820"
  },
  {
    lati: 50.7305200071, longi: 8.8392696943,
    name: "Ebsdorfergrund",
    plz: "35085"
  },
  {
    lati: 54.1781133293, longi: 9.87586052048,
    name: "Nortorf",
    plz: "24589"
  },
  {
    lati: 48.0398265042, longi: 10.1613143424,
    name: "Heimertingen",
    plz: "87751"
  },
  {
    lati: 47.9995877628, longi: 7.80936891547,
    name: "Freiburg im Breisgau",
    plz: "79114"
  },
  {
    lati: 48.0049694556, longi: 7.83935426637,
    name: "Freiburg im Breisgau",
    plz: "79106"
  },
  {
    lati: 47.8224290718, longi: 8.81695672129,
    name: "Mühlhausen-Ehingen",
    plz: "78259"
  },
  {
    lati: 52.6652500328, longi: 7.09664012784,
    name: "Twist",
    plz: "49767"
  },
  {
    lati: 48.1509055143, longi: 11.509219185,
    name: "München",
    plz: "80639"
  },
  {
    lati: 49.7598000588, longi: 11.6968760206,
    name: "Kirchenthumbach",
    plz: "91281"
  },
  {
    lati: 52.3452624662, longi: 13.6127528782,
    name: "Zeuthen",
    plz: "15738"
  },
  {
    lati: 48.185155455, longi: 9.69387784724,
    name: "Oberstadion",
    plz: "89613"
  },
  {
    lati: 52.5604986618, longi: 10.0306843754,
    name: "Adelheidsdorf",
    plz: "29352"
  },
  {
    lati: 48.6065539924, longi: 10.3624205144,
    name: "Haunsheim",
    plz: "89437"
  },
  {
    lati: 50.9312685998, longi: 6.95244489349,
    name: "Köln",
    plz: "50676"
  },
  {
    lati: 49.4671990684, longi: 8.7679018976,
    name: "Wilhelmsfeld",
    plz: "69259"
  },
  {
    lati: 48.5545889019, longi: 13.2181894554,
    name: "Ortenburg",
    plz: "94496"
  },
  {
    lati: 52.6121495553, longi: 13.2486593001,
    name: "Berlin Tegel",
    plz: "13503"
  },
  {
    lati: 53.9434169047, longi: 13.4223798566,
    name: "Gützkow",
    plz: "17506"
  },
  {
    lati: 50.1791873527, longi: 11.1217022508,
    name: "Michelau i. OFr.",
    plz: "96247"
  },
  {
    lati: 48.5039454738, longi: 11.2495439901,
    name: "Gachenbach",
    plz: "86565"
  },
  {
    lati: 48.349208952, longi: 10.2899755254,
    name: "Waldstetten",
    plz: "89367"
  },
  {
    lati: 53.6473213403, longi: 10.4120237314,
    name: "Grönwohld",
    plz: "22956"
  },
  {
    lati: 49.9557353021, longi: 11.5499483139,
    name: "Bayreuth",
    plz: "95445"
  },
  {
    lati: 50.0334795013, longi: 11.5711273565,
    name: "Harsdorf",
    plz: "95499"
  },
  {
    lati: 48.1446519983, longi: 11.5912947093,
    name: "München",
    plz: "80538"
  },
  {
    lati: 52.5893218875, longi: 12.9695695489,
    name: "Brieselang",
    plz: "14656"
  },
  {
    lati: 49.6488175211, longi: 12.4212669662,
    name: "Pleystein",
    plz: "92714"
  },
  {
    lati: 49.8309349026, longi: 10.9284735436,
    name: "Pettstadt",
    plz: "96175"
  },
  {
    lati: 49.8758829998, longi: 8.14210189371,
    name: "Saulheim",
    plz: "55291"
  },
  {
    lati: 49.9662341852, longi: 7.75606394336,
    name: "Stromberg",
    plz: "55442"
  },
  {
    lati: 52.5075267849, longi: 13.5592001527,
    name: "Berlin Biesdorf",
    plz: "12683"
  },
  {
    lati: 51.1203596444, longi: 11.8200105849,
    name: "Naumburg",
    plz: "06618"
  },
  {
    lati: 52.3611630752, longi: 9.14546618994,
    name: "Niederwöhren",
    plz: "31712"
  },
  {
    lati: 51.3271822112, longi: 6.90018195917,
    name: "Ratingen",
    plz: "40883"
  },
  {
    lati: 50.6533689777, longi: 7.27842927684,
    name: "Bad Honnef",
    plz: "53604"
  },
  {
    lati: 50.804676341, longi: 13.1176479733,
    name: "Leubsdorf, Gornau, Augustusburg",
    plz: "09573"
  },
  {
    lati: 48.7088864463, longi: 12.8344379825,
    name: "Oberpöring",
    plz: "94562"
  },
  {
    lati: 51.3831730369, longi: 8.05331547862,
    name: "Arnsberg",
    plz: "59821"
  },
  {
    lati: 48.568266059, longi: 13.4647152641,
    name: "Passau",
    plz: "94032"
  },
  {
    lati: 49.4740273928, longi: 10.9940585387,
    name: "Fürth",
    plz: "90762"
  },
  {
    lati: 53.625704436, longi: 11.4065330254,
    name: "Schwerin",
    plz: "19053"
  },
  {
    lati: 51.0847790795, longi: 11.6112904287,
    name: "Bad Sulza",
    plz: "99518"
  },
  {
    lati: 47.7875621947, longi: 11.8352890263,
    name: "Miesbach",
    plz: "83714"
  },
  {
    lati: 54.3946494821, longi: 10.3711579477,
    name: "Schönberg (Holstein)",
    plz: "24217"
  },
  {
    lati: 53.249829558, longi: 8.58645010977,
    name: "Schwanewede",
    plz: "28790"
  },
  {
    lati: 52.8487953747, longi: 11.4509652701,
    name: "Arendsee",
    plz: "39619"
  },
  {
    lati: 47.6791309527, longi: 11.1988996846,
    name: "Murnau a. Staffelsee",
    plz: "82418"
  },
  {
    lati: 49.7790670629, longi: 7.40403198377,
    name: "Bergen",
    plz: "55608"
  },
  {
    lati: 50.0626705879, longi: 6.1782795399,
    name: "Daleiden, Preischeid u.a.",
    plz: "54689"
  },
  {
    lati: 50.329966336, longi: 6.7928392364,
    name: "Walsdorf, Nohn u.a.",
    plz: "54578"
  },
  {
    lati: 48.6291308389, longi: 12.5354591459,
    name: "Gottfrieding",
    plz: "84177"
  },
  {
    lati: 53.0775206572, longi: 8.91049609208,
    name: "Bremen",
    plz: "28329"
  },
  {
    lati: 48.8961559015, longi: 9.22569766259,
    name: "Ludwigsburg",
    plz: "71640"
  },
  {
    lati: 52.5321386654, longi: 13.3846723824,
    name: "Berlin Mitte",
    plz: "10115"
  },
  {
    lati: 54.3040887893, longi: 10.065843113,
    name: "Kiel Russee",
    plz: "24111"
  },
  {
    lati: 47.9585186832, longi: 10.9168243645,
    name: "Vilgertshofen",
    plz: "86946"
  },
  {
    lati: 49.1538162606, longi: 7.82600813089,
    name: "Fischbach, Erfweiler u.a.",
    plz: "66996"
  },
  {
    lati: 48.1067453258, longi: 8.20162352297,
    name: "Schönwald im Schwarzwald",
    plz: "78141"
  },
  {
    lati: 47.907151586, longi: 10.5628539211,
    name: "Irsee",
    plz: "87660"
  },
  {
    lati: 51.1015194623, longi: 8.92003889269,
    name: "Frankenau",
    plz: "35110"
  },
  {
    lati: 48.9772175493, longi: 8.40988926046,
    name: "Karlsruhe",
    plz: "76199"
  },
  {
    lati: 50.434823734, longi: 8.49152953649,
    name: "Waldolms",
    plz: "35647"
  },
  {
    lati: 48.8010293868, longi: 8.57740338619,
    name: "Höfen an der Enz",
    plz: "75339"
  },
  {
    lati: 49.5727389309, longi: 8.25115039544,
    name: "Dirmstein",
    plz: "67246"
  },
  {
    lati: 53.7023969604, longi: 9.82271686015,
    name: "Borstel-Hohenraden",
    plz: "25494"
  },
  {
    lati: 50.6361395186, longi: 6.2154101149,
    name: "Roetgen",
    plz: "52159"
  },
  {
    lati: 51.1651576684, longi: 6.87718430615,
    name: "Düsseldorf",
    plz: "40597"
  },
  {
    lati: 48.2288647396, longi: 10.5872926522,
    name: "Walkertshofen",
    plz: "86877"
  },
  {
    lati: 50.6751047351, longi: 7.91430687142,
    name: "Malberg, Norken, Höchstenbach u.a.",
    plz: "57629"
  },
  {
    lati: 51.4688809671, longi: 11.9708602268,
    name: "Halle/ Saale",
    plz: "06110"
  },
  {
    lati: 48.1465155299, longi: 12.0916608206,
    name: "Maitenbeth",
    plz: "83558"
  },
  {
    lati: 48.9201158912, longi: 12.6051025215,
    name: "Parkstetten",
    plz: "94365"
  },
  {
    lati: 49.6372504567, longi: 12.2186458894,
    name: "Bechtsried",
    plz: "92699"
  },
  {
    lati: 49.6529281244, longi: 10.9358537464,
    name: "Röttenbach",
    plz: "91341"
  },
  {
    lati: 52.305077203, longi: 14.369697015,
    name: "Briesen, Rauen u.a.",
    plz: "15518"
  },
  {
    lati: 51.227899485, longi: 7.19881165148,
    name: "Wuppertal",
    plz: "42369"
  },
  {
    lati: 52.4432002558, longi: 7.8124509874,
    name: "Merzen, Neuenkirchen",
    plz: "49586"
  },
  {
    lati: 50.4906112758, longi: 7.74937514198,
    name: "Mogendorf, Ebernhahn, Staudt u.a.",
    plz: "56424"
  },
  {
    lati: 48.5459289503, longi: 9.50157196999,
    name: "Lenningen",
    plz: "73252"
  },
  {
    lati: 49.3753913601, longi: 8.57881541879,
    name: "Schwetzingen",
    plz: "68723"
  },
  {
    lati: 49.6104885886, longi: 8.6444978762,
    name: "Laudenbach",
    plz: "69514"
  },
  {
    lati: 54.336516472, longi: 10.0825535172,
    name: "Kronshagen",
    plz: "24119"
  },
  {
    lati: 52.3617887302, longi: 9.3995982457,
    name: "Suthfeld",
    plz: "31555"
  },
  {
    lati: 47.9179819649, longi: 9.74943055751,
    name: "Bad Waldsee",
    plz: "88339"
  },
  {
    lati: 47.5346461428, longi: 11.1076167093,
    name: "Farchant",
    plz: "82490"
  },
  {
    lati: 48.6895780876, longi: 13.3969641729,
    name: "Neukirchen vorm Wald",
    plz: "94154"
  },
  {
    lati: 49.0438699903, longi: 8.70288964816,
    name: "Bretten",
    plz: "75015"
  },
  {
    lati: 53.3486765966, longi: 8.62616210342,
    name: "Hagen",
    plz: "27628"
  },
  {
    lati: 52.9292889326, longi: 10.3139354402,
    name: "Eimke",
    plz: "29578"
  },
  {
    lati: 49.138757366, longi: 10.4824922788,
    name: "Burk",
    plz: "91596"
  },
  {
    lati: 50.6568001089, longi: 13.0731006035,
    name: "Wolkenstein",
    plz: "09429"
  },
  {
    lati: 51.3450374373, longi: 11.9744041628,
    name: "Merseburg",
    plz: "06217"
  },
  {
    lati: 51.7381658149, longi: 7.92404360276,
    name: "Ahlen",
    plz: "59229"
  },
  {
    lati: 51.2445899716, longi: 6.74092560373,
    name: "Düsseldorf",
    plz: "40547"
  },
  {
    lati: 49.8155107699, longi: 7.09926885683,
    name: "Morbach",
    plz: "54497"
  },
  {
    lati: 51.2323855917, longi: 7.27745302806,
    name: "Wuppertal",
    plz: "42399"
  },
  {
    lati: 49.5768959273, longi: 7.29955851549,
    name: "Berschweiler bei Baumholder",
    plz: "55777"
  },
  {
    lati: 53.7030190761, longi: 10.7565521965,
    name: "Ratzeburg",
    plz: "23909"
  },
  {
    lati: 48.7135741365, longi: 10.2718494165,
    name: "Nattheim",
    plz: "89564"
  },
  {
    lati: 52.2610552695, longi: 9.57722991432,
    name: "Wennigsen (Deister)",
    plz: "30974"
  },
  {
    lati: 48.081307271, longi: 11.33651011,
    name: "Pentenried",
    plz: "82349"
  },
  {
    lati: 48.0541977835, longi: 12.2004563437,
    name: "Wasserburg a. Inn",
    plz: "83512"
  },
  {
    lati: 49.073824703, longi: 12.7080631876,
    name: "Konzell",
    plz: "94357"
  },
  {
    lati: 51.0434237664, longi: 13.6977013964,
    name: "Dresden",
    plz: "01159"
  },
  {
    lati: 51.8489253077, longi: 13.9513907318,
    name: "Lübbenau/ Spreewald",
    plz: "03222"
  },
  {
    lati: 51.5861592075, longi: 10.541831119,
    name: "Bad Sachsa",
    plz: "37441"
  },
  {
    lati: 48.018728738, longi: 9.73171876772,
    name: "Ingoldingen",
    plz: "88456"
  },
  {
    lati: 49.7026326145, longi: 9.84479540806,
    name: "Kleinrinderfeld",
    plz: "97271"
  },
  {
    lati: 51.5324046983, longi: 9.903795085,
    name: "Göttingen",
    plz: "37081"
  },
  {
    lati: 53.7142560374, longi: 10.1161549093,
    name: "Tangstedt",
    plz: "22889"
  },
  {
    lati: 51.5826144826, longi: 7.14815046507,
    name: "Herten",
    plz: "45699"
  },
  {
    lati: 49.8367810444, longi: 8.26274448318,
    name: "Mommenheim",
    plz: "55278"
  },
  {
    lati: 53.5404171651, longi: 8.73219447397,
    name: "Schiffdorf",
    plz: "27619"
  },
  {
    lati: 50.244420567, longi: 8.89537257146,
    name: "Nidderau",
    plz: "61130"
  },
  {
    lati: 50.09300676, longi: 7.78616931987,
    name: "Kaub",
    plz: "56349"
  },
  {
    lati: 48.6523562001, longi: 9.70594785868,
    name: "Schlat",
    plz: "73114"
  },
  {
    lati: 52.7003491797, longi: 9.87908509438,
    name: "Winsen (Aller)",
    plz: "29308"
  },
  {
    lati: 48.3559358326, longi: 10.6924651833,
    name: "Kutzenhausen",
    plz: "86500"
  },
  {
    lati: 50.3433264394, longi: 10.9598867863,
    name: "Lautertal",
    plz: "96486"
  },
  {
    lati: 53.4723616957, longi: 7.33292420842,
    name: "Südbrookmerland",
    plz: "26624"
  },
  {
    lati: 50.0031528804, longi: 7.46805273514,
    name: "Tiefenbach u.a.",
    plz: "55471"
  },
  {
    lati: 49.7680072801, longi: 11.9740626481,
    name: "Pressath",
    plz: "92690"
  },
  {
    lati: 49.0048849929, longi: 12.1512995684,
    name: "Regensburg",
    plz: "93055"
  },
  {
    lati: 54.0270571985, longi: 14.027099847,
    name: "Koserow",
    plz: "17459"
  },
  {
    lati: 51.1976265343, longi: 11.4440770067,
    name: "Bad Bibra, Finne u.a.",
    plz: "06647"
  },
  {
    lati: 51.4853493179, longi: 6.92913374668,
    name: "Essen",
    plz: "45357"
  },
  {
    lati: 53.5177086506, longi: 8.11413321884,
    name: "Wilhelmshaven",
    plz: "26382"
  },
  {
    lati: 49.1887579611, longi: 7.50314696408,
    name: "Bottenbach",
    plz: "66504"
  },
  {
    lati: 52.5975370851, longi: 7.70652651686,
    name: "Berge, Bippen",
    plz: "49626"
  },
  {
    lati: 54.5283991186, longi: 8.51219811591,
    name: "Hallig Hooge",
    plz: "25859"
  },
  {
    lati: 54.7685357469, longi: 9.75484931853,
    name: "Steinberg, Steinbergkirche",
    plz: "24972"
  },
  {
    lati: 48.1986341882, longi: 11.5536439057,
    name: "München",
    plz: "80935"
  },
  {
    lati: 49.795213192, longi: 10.0385784304,
    name: "Rottendorf",
    plz: "97228"
  },
  {
    lati: 51.7884887076, longi: 10.2000537444,
    name: "Bad Grund",
    plz: "37539"
  },
  {
    lati: 54.1190483821, longi: 13.0496523116,
    name: "Grimmen",
    plz: "18507"
  },
  {
    lati: 53.801810468, longi: 10.2308786419,
    name: "Sülfeld",
    plz: "23867"
  },
  {
    lati: 49.767892298, longi: 10.62914461,
    name: "Schlüsselfeld",
    plz: "96132"
  },
  {
    lati: 52.5815066736, longi: 13.493480356,
    name: "Berlin Neu-Schönhausen",
    plz: "13051"
  },
  {
    lati: 48.5146925386, longi: 11.6138433887,
    name: "Schweitenkirchen",
    plz: "85301"
  },
  {
    lati: 48.7584908102, longi: 11.8498842425,
    name: "Siegenburg",
    plz: "93354"
  },
  {
    lati: 48.8188998425, longi: 8.26696815387,
    name: "Kuppenheim",
    plz: "76456"
  },
  {
    lati: 49.2837836119, longi: 7.49787516724,
    name: "Reifenberg",
    plz: "66507"
  },
  {
    lati: 52.3430719962, longi: 9.30901977921,
    name: "Beckedorf",
    plz: "31699"
  },
  {
    lati: 47.8845275554, longi: 9.5008788857,
    name: "Unterwaldhausen",
    plz: "88379"
  },
  {
    lati: 50.9746237307, longi: 10.1226539,
    name: "Gerstungen",
    plz: "99834"
  },
  {
    lati: 53.739392975, longi: 10.0864414494,
    name: "Tangstedt",
    plz: "22889"
  },
  {
    lati: 53.8490693926, longi: 10.1075240658,
    name: "Sievershütten",
    plz: "24641"
  },
  {
    lati: 53.6259896097, longi: 10.0991042224,
    name: "Hamburg",
    plz: "22175"
  },
  {
    lati: 52.4187972554, longi: 10.3926767936,
    name: "Hillerse",
    plz: "38543"
  },
  {
    lati: 48.7093728361, longi: 13.2559987575,
    name: "Eging a. See",
    plz: "94535"
  },
  {
    lati: 51.3397372663, longi: 12.3309615582,
    name: "Leipzig",
    plz: "04177"
  },
  {
    lati: 53.2613443515, longi: 10.3869525235,
    name: "Lüneburg",
    plz: "21339"
  },
  {
    lati: 53.6910476625, longi: 10.5380834515,
    name: "Sandesneben u.a.",
    plz: "23898"
  },
  {
    lati: 51.0663362367, longi: 14.5134782997,
    name: "Beiersdorf, Oppach",
    plz: "02736"
  },
  {
    lati: 50.8273327756, longi: 7.74426450724,
    name: "Birken-Honigsessen",
    plz: "57587"
  },
  {
    lati: 54.7692218661, longi: 8.28909248488,
    name: "Hörnum (Sylt)",
    plz: "25997"
  },
  {
    lati: 50.8103389189, longi: 8.36276398634,
    name: "Eschenburg",
    plz: "35713"
  },
  {
    lati: 47.8702822925, longi: 11.4600302353,
    name: "Geretsried",
    plz: "82538"
  },
  {
    lati: 48.6614978389, longi: 9.3398566282,
    name: "Unterensingen",
    plz: "72669"
  },
  {
    lati: 50.1024028603, longi: 8.62818984709,
    name: "Frankfurt am Main",
    plz: "60326"
  },
  {
    lati: 52.4551013396, longi: 8.74651886742,
    name: "Diepenau",
    plz: "31603"
  },
  {
    lati: 50.4225862166, longi: 8.74682238855,
    name: "Rockenberg",
    plz: "35519"
  },
  {
    lati: 50.5838996465, longi: 9.86461704002,
    name: "Hofbieber",
    plz: "36145"
  },
  {
    lati: 52.1116657151, longi: 10.5951813776,
    name: "Kissenbrück",
    plz: "38324"
  },
  {
    lati: 53.2810627207, longi: 10.2722362663,
    name: "Reppenstedt, Lüneburg",
    plz: "21391"
  },
  {
    lati: 51.274484136, longi: 11.6353390133,
    name: "Karsdorf",
    plz: "06638"
  },
  {
    lati: 48.0543977078, longi: 11.4562241023,
    name: "Neuried",
    plz: "82061"
  },
  {
    lati: 50.2449032975, longi: 7.36259971917,
    name: "Münstermaifeld",
    plz: "56294"
  },
  {
    lati: 51.207304613, longi: 7.13505773017,
    name: "Wuppertal",
    plz: "42349"
  },
  {
    lati: 52.7619233256, longi: 8.89802648404,
    name: "Asendorf",
    plz: "27330"
  },
  {
    lati: 51.0485288723, longi: 9.16485634747,
    name: "Bad Zwesten",
    plz: "34596"
  },
  {
    lati: 53.5748316302, longi: 9.26456227367,
    name: "Oldendorf",
    plz: "21726"
  },
  {
    lati: 53.2065562435, longi: 8.52596272315,
    name: "Bremen",
    plz: "28777"
  },
  {
    lati: 50.3095068635, longi: 11.2101493865,
    name: "Neuhaus-Schierschnitz",
    plz: "96524"
  },
  {
    lati: 52.8437016969, longi: 10.6955187622,
    name: "Bad Bodenteich",
    plz: "29389"
  },
  {
    lati: 48.4845739006, longi: 10.9174486592,
    name: "Rehling",
    plz: "86508"
  },
  {
    lati: 49.7429355096, longi: 12.2031307752,
    name: "Störnstein",
    plz: "92721"
  },
  {
    lati: 49.0232712969, longi: 12.2942586485,
    name: "Bach an der Donau",
    plz: "93090"
  },
  {
    lati: 49.6540948408, longi: 9.86008411576,
    name: "Kirchheim",
    plz: "97268"
  },
  {
    lati: 52.5073040988, longi: 13.3642756938,
    name: "Berlin Tiergarten",
    plz: "10785"
  },
  {
    lati: 48.1353479015, longi: 11.006670003,
    name: "Geltendorf",
    plz: "82269"
  },
  {
    lati: 48.1113578522, longi: 11.2762312364,
    name: "Gilching",
    plz: "82205"
  },
  {
    lati: 49.2898596122, longi: 11.35444665,
    name: "Postbauer-Heng",
    plz: "92353"
  },
  {
    lati: 51.8465572463, longi: 10.7996316789,
    name: "Wernigerode, Nordharz",
    plz: "38855"
  },
  {
    lati: 51.0085484484, longi: 10.4809600703,
    name: "Hörselberg-Hainich",
    plz: "99820"
  },
  {
    lati: 50.991084782, longi: 7.05854146106,
    name: "Köln",
    plz: "51069"
  },
  {
    lati: 51.4258542001, longi: 7.0773634092,
    name: "Essen",
    plz: "45277"
  },
  {
    lati: 51.040056724, longi: 7.14910534312,
    name: "Odenthal",
    plz: "51519"
  },
  {
    lati: 50.4582762258, longi: 7.2665325522,
    name: "Burgbrohl",
    plz: "56659"
  },
  {
    lati: 50.7421763114, longi: 6.60569120731,
    name: "Vettweiß",
    plz: "52391"
  },
  {
    lati: 49.1475221211, longi: 8.14962836863,
    name: "Insheim",
    plz: "76865"
  },
  {
    lati: 52.3627520254, longi: 7.70731764114,
    name: "Recke",
    plz: "49509"
  },
  {
    lati: 48.2070822943, longi: 11.9412202749,
    name: "Pastetten",
    plz: "85669"
  },
  {
    lati: 51.3681108151, longi: 13.314589411,
    name: "Zeithain",
    plz: "01619"
  },
  {
    lati: 51.6005737383, longi: 14.1338653771,
    name: "Neu-Seeland, Neupetershain",
    plz: "03103"
  },
  {
    lati: 48.0906335268, longi: 10.0538307658,
    name: "Erolzheim",
    plz: "88453"
  },
  {
    lati: 47.5024683249, longi: 10.1858402034,
    name: "Blaichach",
    plz: "87544"
  },
  {
    lati: 50.636988617, longi: 10.511050669,
    name: "Viernau u.a.",
    plz: "98547"
  },
  {
    lati: 48.8195157263, longi: 12.397795683,
    name: "Geiselhöring",
    plz: "94333"
  },
  {
    lati: 49.8248151279, longi: 11.9111888056,
    name: "Kastl",
    plz: "95506"
  },
  {
    lati: 51.7867236659, longi: 8.82374703471,
    name: "Bad Lippspringe",
    plz: "33175"
  },
  {
    lati: 49.8794366398, longi: 7.88940315622,
    name: "Bretzenheim",
    plz: "55559"
  },
  {
    lati: 50.3183865982, longi: 8.15190511505,
    name: "Hünfelden",
    plz: "65597"
  },
  {
    lati: 50.1061887752, longi: 8.19714397411,
    name: "Wiesbaden",
    plz: "65195"
  },
  {
    lati: 51.1354796143, longi: 14.417429614,
    name: "Doberschau-Gaußig, Großpostwitz, Obergurig",
    plz: "02692"
  },
  {
    lati: 50.5821928421, longi: 7.74991517224,
    name: "Herschbach",
    plz: "56249"
  },
  {
    lati: 53.4564430278, longi: 11.9320820151,
    name: "Spornitz",
    plz: "19372"
  },
  {
    lati: 48.0875323919, longi: 8.08865805401,
    name: "Simonswald",
    plz: "79263"
  },
  {
    lati: 49.4164441925, longi: 9.03548658389,
    name: "Zwingenberg",
    plz: "69439"
  },
  {
    lati: 53.9142367465, longi: 10.8090983163,
    name: "Lübeck",
    plz: "23569"
  },
  {
    lati: 51.1653643042, longi: 10.8291938549,
    name: "Bad Tennstedt",
    plz: "99955"
  },
  {
    lati: 53.4596617788, longi: 12.0680379215,
    name: "Lübz, Passow",
    plz: "19386"
  },
  {
    lati: 51.5321974295, longi: 7.23659277315,
    name: "Herne",
    plz: "44623"
  },
  {
    lati: 47.8619553394, longi: 8.77113632993,
    name: "Engen",
    plz: "78234"
  },
  {
    lati: 49.2741295474, longi: 12.5235483294,
    name: "Stamsried",
    plz: "93491"
  },
  {
    lati: 52.131409007, longi: 13.8117308238,
    name: "Märkisch Buchholz",
    plz: "15748"
  },
  {
    lati: 50.4123555982, longi: 9.21368556457,
    name: "Gedern",
    plz: "63688"
  },
  {
    lati: 51.0194888716, longi: 6.86838410022,
    name: "Köln",
    plz: "50765"
  },
  {
    lati: 47.9035926927, longi: 9.51431461934,
    name: "Ebenweiler",
    plz: "88370"
  },
  {
    lati: 48.4851551759, longi: 9.97635041838,
    name: "Beimerstetten",
    plz: "89179"
  },
  {
    lati: 53.5267883218, longi: 10.0457707909,
    name: "Hamburg",
    plz: "20539"
  },
  {
    lati: 53.4314024723, longi: 9.79583873729,
    name: "Neu Wulmstorf",
    plz: "21629"
  },
  {
    lati: 48.9950047924, longi: 9.9308378234,
    name: "Bühlerzell",
    plz: "74426"
  },
  {
    lati: 47.9663543407, longi: 10.2173143029,
    name: "Benningen",
    plz: "87734"
  },
  {
    lati: 52.3221676856, longi: 10.2851159941,
    name: "Peine",
    plz: "31224"
  },
  {
    lati: 52.4307450806, longi: 10.4971441614,
    name: "Ribbesbüttel",
    plz: "38551"
  },
  {
    lati: 49.5129905364, longi: 8.25150365945,
    name: "Weisenheim am Sand",
    plz: "67256"
  },
  {
    lati: 50.6018460741, longi: 11.8977064629,
    name: "Kirschkau, Pausa-Mühltroff",
    plz: "07919"
  },
  {
    lati: 51.4696299462, longi: 11.9309691011,
    name: "Halle/ Saale",
    plz: "06124"
  },
  {
    lati: 50.6925500321, longi: 12.350977631,
    name: "Fraureuth",
    plz: "08427"
  },
  {
    lati: 54.6476476254, longi: 9.70180065475,
    name: "Böel",
    plz: "24401"
  },
  {
    lati: 49.6107570021, longi: 10.5149061975,
    name: "Langenfeld",
    plz: "91474"
  },
  {
    lati: 51.5502323369, longi: 7.20966750577,
    name: "Herne",
    plz: "44629"
  },
  {
    lati: 50.8348091489, longi: 12.1102710945,
    name: "Gera",
    plz: "07551"
  },
  {
    lati: 50.9050429618, longi: 7.04645056376,
    name: "Köln",
    plz: "51149"
  },
  {
    lati: 51.6092544665, longi: 7.2488917051,
    name: "Recklinghausen",
    plz: "45665"
  },
  {
    lati: 53.0563920955, longi: 7.40185816445,
    name: "Papenburg",
    plz: "26871"
  },
  {
    lati: 54.2386087709, longi: 8.99858907782,
    name: "Neuenkirchen",
    plz: "25792"
  },
  {
    lati: 49.1048954964, longi: 11.9815273794,
    name: "Wolfsegg",
    plz: "93195"
  },
  {
    lati: 49.5205913453, longi: 7.85986228736,
    name: "Sembach",
    plz: "67681"
  },
  {
    lati: 52.5922781923, longi: 10.3611371213,
    name: "Hohne",
    plz: "29362"
  },
  {
    lati: 48.9568309401, longi: 10.5789975222,
    name: "Oettingen i. Bay.",
    plz: "86732"
  },
  {
    lati: 54.8607699752, longi: 8.89177167764,
    name: "Süderlügum, Braderup u.a.",
    plz: "25923"
  },
  {
    lati: 53.7254805223, longi: 10.6388979192,
    name: "Berkenthin",
    plz: "23919"
  },
  {
    lati: 50.0376498977, longi: 10.8808608959,
    name: "Rattelsdorf",
    plz: "96179"
  },
  {
    lati: 52.0297338095, longi: 11.2641808328,
    name: "Oschersleben (Bode)",
    plz: "39387"
  },
  {
    lati: 50.3185803363, longi: 11.3833601497,
    name: "Wilhelmsthal",
    plz: "96352"
  },
  {
    lati: 52.5160995627, longi: 13.4655041805,
    name: "Berlin Friedrichshain",
    plz: "10247"
  },
  {
    lati: 48.6870690417, longi: 9.3568575041,
    name: "Köngen",
    plz: "73257"
  },
  {
    lati: 48.2533030342, longi: 12.9334038363,
    name: "Julbach",
    plz: "84387"
  },
  {
    lati: 50.52922349, longi: 13.0457327835,
    name: "Bärenstein",
    plz: "09471"
  },
  {
    lati: 50.4569658732, longi: 7.68911825417,
    name: "Hilgert",
    plz: "56206"
  },
  {
    lati: 50.5927571197, longi: 7.70164305983,
    name: "Dierdorf",
    plz: "56269"
  },
  {
    lati: 49.6087043187, longi: 7.7180805955,
    name: "Medard, Rathskirchen u.a.",
    plz: "67744"
  },
  {
    lati: 48.0198541606, longi: 7.8104261626,
    name: "Freiburg im Breisgau",
    plz: "79110"
  },
  {
    lati: 51.9995515286, longi: 8.38073437645,
    name: "Steinhagen",
    plz: "33803"
  },
  {
    lati: 52.3214096855, longi: 8.38213783882,
    name: "Bad Essen",
    plz: "49152"
  },
  {
    lati: 53.1337045909, longi: 8.74293141672,
    name: "Bremen",
    plz: "28239"
  },
  {
    lati: 50.7020652411, longi: 7.08538245594,
    name: "Bonn",
    plz: "53127"
  },
  {
    lati: 50.2554997288, longi: 11.8389043979,
    name: "Konradsreuth",
    plz: "95176"
  },
  {
    lati: 49.3537980589, longi: 11.8712069925,
    name: "Ensdorf",
    plz: "92266"
  },
  {
    lati: 49.2473470966, longi: 12.3153641214,
    name: "Bruck i.d. OPf.",
    plz: "92436"
  },
  {
    lati: 53.6359889857, longi: 9.94503642473,
    name: "Hamburg",
    plz: "22453"
  },
  {
    lati: 53.5564628826, longi: 10.1079917067,
    name: "Hamburg",
    plz: "22119"
  },
  {
    lati: 52.3743244792, longi: 14.0575132657,
    name: "Fürstenwalde/ Spree",
    plz: "15517"
  },
  {
    lati: 49.7867067081, longi: 7.48667327198,
    name: "Kirn",
    plz: "55606"
  },
  {
    lati: 52.4722429476, longi: 7.51861932463,
    name: "Andervenne, Beesten, Freren, Messingen, Thuine",
    plz: "49832"
  },
  {
    lati: 50.150963345, longi: 8.43302189029,
    name: "Kelkheim",
    plz: "65779"
  },
  {
    lati: 53.5924260391, longi: 9.59085119752,
    name: "Hollern-Twielenfleth",
    plz: "21723"
  },
  {
    lati: 49.5472895912, longi: 10.0433744421,
    name: "Aub",
    plz: "97239"
  },
  {
    lati: 49.672773913, longi: 11.1586594008,
    name: "Kunreuth",
    plz: "91358"
  },
  {
    lati: 53.9264233214, longi: 10.6951650281,
    name: "Bad Schwartau",
    plz: "23611"
  },
  {
    lati: 48.3928867572, longi: 12.2234505321,
    name: "Neufraunhofen",
    plz: "84181"
  },
  {
    lati: 54.5616191991, longi: 9.57805299957,
    name: "Nübel",
    plz: "24881"
  },
  {
    lati: 50.6328650493, longi: 7.65707923391,
    name: "Oberdreis",
    plz: "57639"
  },
  {
    lati: 53.8573907362, longi: 8.67692470548,
    name: "Cuxhaven",
    plz: "27474"
  },
  {
    lati: 52.3999162733, longi: 8.99689148284,
    name: "Petershagen",
    plz: "32469"
  },
  {
    lati: 48.8428438768, longi: 10.0817017901,
    name: "Aalen",
    plz: "73430"
  },
  {
    lati: 48.6580697905, longi: 12.7157529499,
    name: "Landau a.d. Isar",
    plz: "94405"
  },
  {
    lati: 48.2414628122, longi: 11.9137433577,
    name: "Wörth",
    plz: "85457"
  },
  {
    lati: 48.5479413026, longi: 11.4951352092,
    name: "Pfaffenhofen a.d.Ilm",
    plz: "85276"
  },
  {
    lati: 50.2891620558, longi: 6.61711674191,
    name: "Walsdorf, Nohn u.a.",
    plz: "54578"
  },
  {
    lati: 51.1678754781, longi: 6.91446282151,
    name: "Hilden, Düsseldorf",
    plz: "40721"
  },
  {
    lati: 50.1708467402, longi: 7.18639975124,
    name: "Klotten",
    plz: "56818"
  },
  {
    lati: 51.5285433364, longi: 7.44141491151,
    name: "Dortmund",
    plz: "44147"
  },
  {
    lati: 49.5460942822, longi: 8.1067147971,
    name: "Tiefenthal",
    plz: "67311"
  },
  {
    lati: 48.7987695649, longi: 9.22534913753,
    name: "Stuttgart",
    plz: "70372"
  },
  {
    lati: 48.9753449703, longi: 9.60817433289,
    name: "Murrhardt",
    plz: "71540"
  },
  {
    lati: 49.953570918, longi: 8.63191218881,
    name: "Erzhausen",
    plz: "64390"
  },
  {
    lati: 53.177751902, longi: 8.63540284469,
    name: "Bremen",
    plz: "28757"
  },
  {
    lati: 53.1262364034, longi: 9.80371716774,
    name: "Schneverdingen, Heimbuch",
    plz: "29640"
  },
  {
    lati: 52.0073881033, longi: 10.1202156238,
    name: "Bockenem",
    plz: "31167"
  },
  {
    lati: 50.3077128643, longi: 9.29556326115,
    name: "Brachttal",
    plz: "63636"
  },
  {
    lati: 54.6599793253, longi: 9.38135144401,
    name: "Tarp",
    plz: "24963"
  },
  {
    lati: 49.7317148132, longi: 11.1539568218,
    name: "Kirchehrenbach",
    plz: "91356"
  },
  {
    lati: 52.6396906262, longi: 13.3692156465,
    name: "Mühlenbecker Land",
    plz: "16552"
  },
  {
    lati: 49.8405335334, longi: 6.74317074351,
    name: "Schweich",
    plz: "54338"
  },
  {
    lati: 51.2540211653, longi: 6.80748718734,
    name: "Düsseldorf",
    plz: "40470"
  },
  {
    lati: 51.6077694513, longi: 7.32704550868,
    name: "Castrop-Rauxel",
    plz: "44577"
  },
  {
    lati: 53.7030233141, longi: 8.89487629304,
    name: "Ihlienworth",
    plz: "21775"
  },
  {
    lati: 49.9783288667, longi: 10.5903617345,
    name: "Sand a. Main",
    plz: "97522"
  },
  {
    lati: 47.6859987812, longi: 11.8764014337,
    name: "Schliersee",
    plz: "83727"
  },
  {
    lati: 50.570824099, longi: 12.5007561543,
    name: "Crinitzberg",
    plz: "08147"
  },
  {
    lati: 47.7301566672, longi: 12.8611986181,
    name: "Bad Reichenhall",
    plz: "83435"
  },
  {
    lati: 48.9074204297, longi: 12.1411199342,
    name: "Thalmassing",
    plz: "93107"
  },
  {
    lati: 50.0759927753, longi: 8.23957930191,
    name: "Wiesbaden",
    plz: "65185"
  },
  {
    lati: 49.7615783791, longi: 8.59847738457,
    name: "Bickenbach",
    plz: "64404"
  },
  {
    lati: 51.9651722882, longi: 7.59732223227,
    name: "Münster",
    plz: "48149"
  },
  {
    lati: 49.7621298367, longi: 7.05109128521,
    name: "Deuselbach, Hermeskeil, Rorodt",
    plz: "54411"
  },
  {
    lati: 51.6591808073, longi: 7.14476272511,
    name: "Marl",
    plz: "45770"
  },
  {
    lati: 49.3423561412, longi: 10.2143152356,
    name: "Gebsattel",
    plz: "91607"
  },
  {
    lati: 49.7007088051, longi: 10.9739037763,
    name: "Heroldsbach",
    plz: "91336"
  },
  {
    lati: 49.066839192, longi: 10.4586117018,
    name: "Wittelshofen",
    plz: "91749"
  },
  {
    lati: 50.7563225398, longi: 12.2938659055,
    name: "Langenbernsdorf",
    plz: "08428"
  },
  {
    lati: 53.3323953534, longi: 13.4060081195,
    name: "Feldberger Seenlandschaft",
    plz: "17258"
  },
  {
    lati: 52.414094766, longi: 13.8659759796,
    name: "Erkner",
    plz: "15537"
  },
  {
    lati: 52.1660899141, longi: 14.2311336503,
    name: "Beeskow",
    plz: "15848"
  },
  {
    lati: 49.2862486718, longi: 8.98499216615,
    name: "Neckarbischofsheim",
    plz: "74924"
  },
  {
    lati: 51.0399105535, longi: 13.7368423564,
    name: "Dresden",
    plz: "01069"
  },
  {
    lati: 50.1382075804, longi: 11.0843514738,
    name: "Lichtenfels",
    plz: "96215"
  },
  {
    lati: 51.5050695954, longi: 11.4312120845,
    name: "Wallhausen, Blankenheim",
    plz: "06528"
  },
  {
    lati: 49.0326071713, longi: 9.93013635028,
    name: "Bühlertann",
    plz: "74424"
  },
  {
    lati: 48.1801177145, longi: 10.0225700725,
    name: "Wain",
    plz: "88489"
  },
  {
    lati: 51.1634289209, longi: 6.48893585325,
    name: "Mönchengladbach",
    plz: "41238"
  },
  {
    lati: 53.1074648621, longi: 9.25203494929,
    name: "Sottrum, Reeßum, Bötersen u.a.",
    plz: "27367"
  },
  {
    lati: 53.3786779758, longi: 8.01227029006,
    name: "Bockhorn",
    plz: "26345"
  },
  {
    lati: 51.5338406601, longi: 7.48502776495,
    name: "Dortmund",
    plz: "44145"
  },
  {
    lati: 49.5374669196, longi: 10.2062410552,
    name: "Uffenheim",
    plz: "97215"
  },
  {
    lati: 54.7190152101, longi: 9.7461605807,
    name: "Sterup",
    plz: "24996"
  },
  {
    lati: 49.7473498685, longi: 10.7135632527,
    name: "Wachenroth",
    plz: "96193"
  },
  {
    lati: 52.2359012684, longi: 10.9960512131,
    name: "Helmstedt",
    plz: "38350"
  },
  {
    lati: 50.100233651, longi: 11.0211042573,
    name: "Staffelstein",
    plz: "96231"
  },
  {
    lati: 51.2738240529, longi: 7.20525247666,
    name: "Wuppertal",
    plz: "42275"
  },
  {
    lati: 51.5572272269, longi: 7.24418126193,
    name: "Herne",
    plz: "44628"
  },
  {
    lati: 51.4225403132, longi: 7.32768871178,
    name: "Witten",
    plz: "58452"
  },
  {
    lati: 49.6295653647, longi: 6.89615284279,
    name: "Gusenburg",
    plz: "54413"
  },
  {
    lati: 47.7301795908, longi: 12.6273399613,
    name: "Ruhpolding",
    plz: "83324"
  },
  {
    lati: 51.9511987141, longi: 12.6592083635,
    name: "Wittenberg",
    plz: "06889"
  },
  {
    lati: 50.9154023086, longi: 12.0649246585,
    name: "Gera",
    plz: "07552"
  },
  {
    lati: 50.8144270317, longi: 7.16475524867,
    name: "Troisdorf",
    plz: "53840"
  },
  {
    lati: 49.6669339122, longi: 7.49722791037,
    name: "Langweiler",
    plz: "67746"
  },
  {
    lati: 53.9542227066, longi: 8.43721737143,
    name: "Neuwerk",
    plz: "27499"
  },
  {
    lati: 48.2005505462, longi: 9.26484983362,
    name: "Hettingen",
    plz: "72513"
  },
  {
    lati: 49.0450827258, longi: 9.44687678269,
    name: "Spiegelberg",
    plz: "71579"
  },
  {
    lati: 49.4931908252, longi: 11.1000666387,
    name: "Nürnberg",
    plz: "90411"
  },
  {
    lati: 53.7217530194, longi: 12.7632979355,
    name: "Malchin",
    plz: "17139"
  },
  {
    lati: 47.6919575205, longi: 10.7835540074,
    name: "Lechbruck",
    plz: "86983"
  },
  {
    lati: 47.7613375003, longi: 12.2150266621,
    name: "Samerberg",
    plz: "83122"
  },
  {
    lati: 52.5491613826, longi: 13.5627264818,
    name: "Berlin",
    plz: "12679"
  },
  {
    lati: 50.2164561841, longi: 7.06557583846,
    name: "Düngenheim",
    plz: "56761"
  },
  {
    lati: 50.7570285802, longi: 6.16142220987,
    name: "Aachen",
    plz: "52078"
  },
  {
    lati: 51.3823645861, longi: 6.75911170609,
    name: "Duisburg",
    plz: "47249"
  },
  {
    lati: 50.4698726488, longi: 11.4568187232,
    name: "Lehesten",
    plz: "07349"
  },
  {
    lati: 48.0037159235, longi: 11.5813504508,
    name: "Oberhaching",
    plz: "82041"
  },
  {
    lati: 50.1451150899, longi: 7.679631226,
    name: "Sankt Goar",
    plz: "56329"
  },
  {
    lati: 49.8560445546, longi: 8.04589515758,
    name: "Wallertheim",
    plz: "55578"
  },
  {
    lati: 49.5154997131, longi: 8.61055199663,
    name: "Heddesheim",
    plz: "68542"
  },
  {
    lati: 53.805049168, longi: 8.72978939537,
    name: "Cuxhaven",
    plz: "27478"
  },
  {
    lati: 50.1974592657, longi: 8.6819594037,
    name: "Frankfurt am Main",
    plz: "60437"
  },
  {
    lati: 52.0385805938, longi: 7.4779731672,
    name: "Altenberge",
    plz: "48341"
  },
  {
    lati: 54.4737241401, longi: 9.69350075315,
    name: "Fleckeby u.a.",
    plz: "24357"
  },
  {
    lati: 47.8262946668, longi: 9.79267434312,
    name: "Wolfegg",
    plz: "88364"
  },
  {
    lati: 52.3952008693, longi: 9.7694722148,
    name: "Hannover",
    plz: "30177"
  },
  {
    lati: 48.8331563391, longi: 10.8715344488,
    name: "Monheim",
    plz: "86653"
  },
  {
    lati: 52.4271477034, longi: 13.0264493062,
    name: "Potsdam",
    plz: "14469"
  },
  {
    lati: 54.1087432707, longi: 9.58456656405,
    name: "Osterstedt",
    plz: "25590"
  },
  {
    lati: 51.258856082, longi: 6.68420177962,
    name: "Meerbusch",
    plz: "40667"
  },
  {
    lati: 49.8352266524, longi: 8.01850691666,
    name: "Gau-Bickelheim",
    plz: "55599"
  },
  {
    lati: 49.1449431281, longi: 8.21675490008,
    name: "Herxheim",
    plz: "76863"
  },
  {
    lati: 50.3095002907, longi: 7.81153861546,
    name: "Nassau",
    plz: "56377"
  },
  {
    lati: 52.7586601681, longi: 9.00054053922,
    name: "Asendorf",
    plz: "27330"
  },
  {
    lati: 51.7096199146, longi: 9.17980171369,
    name: "Brakel",
    plz: "33034"
  },
  {
    lati: 52.9373395483, longi: 9.23611648073,
    name: "Verden",
    plz: "27283"
  },
  {
    lati: 50.2727515714, longi: 9.27132641451,
    name: "Wächtersbach",
    plz: "63607"
  },
  {
    lati: 48.1667839921, longi: 11.4677649912,
    name: "München",
    plz: "81247"
  },
  {
    lati: 50.3369284664, longi: 7.47400789901,
    name: "Lehmen, Niederfell, Oberfell, Wolken u.a.",
    plz: "56332"
  },
  {
    lati: 49.7903540728, longi: 7.55116598944,
    name: "Merxheim",
    plz: "55627"
  },
  {
    lati: 52.2949583837, longi: 8.95033665807,
    name: "Minden",
    plz: "32423"
  },
  {
    lati: 48.619051146, longi: 10.0451081779,
    name: "Gerstetten",
    plz: "89547"
  },
  {
    lati: 48.4487313559, longi: 10.0842453596,
    name: "Elchingen",
    plz: "89275"
  },
  {
    lati: 49.0155657462, longi: 10.9868838312,
    name: "Weißenburg i. Bay.",
    plz: "91781"
  },
  {
    lati: 49.4645173571, longi: 10.3161456646,
    name: "Burgbernheim",
    plz: "91593"
  },
  {
    lati: 52.1765050412, longi: 10.4290648938,
    name: "Salzgitter",
    plz: "38239"
  },
  {
    lati: 51.5082507167, longi: 11.9058124249,
    name: "Halle/ Saale",
    plz: "06120"
  },
  {
    lati: 51.4271170313, longi: 7.22671458131,
    name: "Bochum",
    plz: "44797"
  },
  {
    lati: 51.1886180521, longi: 7.49537996475,
    name: "Halver",
    plz: "58553"
  },
  {
    lati: 49.6460981776, longi: 7.48002010327,
    name: "Medard, Rathskirchen u.a.",
    plz: "67744"
  },
  {
    lati: 50.4846725113, longi: 8.27033422779,
    name: "Weilburg",
    plz: "35781"
  },
  {
    lati: 54.4304726188, longi: 9.23126804999,
    name: "Winnert",
    plz: "25887"
  },
  {
    lati: 48.5002282791, longi: 9.53735064708,
    name: "Römerstein",
    plz: "72587"
  },
  {
    lati: 49.4405754536, longi: 11.0466484394,
    name: "Nürnberg",
    plz: "90439"
  },
  {
    lati: 52.8504141978, longi: 12.4570912499,
    name: "Neustadt (Dosse) u.a.",
    plz: "16845"
  },
  {
    lati: 47.8850053159, longi: 10.3124973509,
    name: "Böhen",
    plz: "87736"
  },
  {
    lati: 48.95764858, longi: 9.85540112239,
    name: "Sulzbach-Laufen",
    plz: "74429"
  },
  {
    lati: 51.2368754494, longi: 9.91470292352,
    name: "Berkatal",
    plz: "37297"
  },
  {
    lati: 48.8592741196, longi: 9.8946678065,
    name: "Göggingen",
    plz: "73571"
  },
  {
    lati: 50.9560787637, longi: 12.4711248367,
    name: "Nobitz, Göhren, Windischleuba",
    plz: "04603"
  },
  {
    lati: 47.9140839594, longi: 12.5394121499,
    name: "Chieming",
    plz: "83339"
  },
  {
    lati: 50.7331490797, longi: 13.1006476368,
    name: "Großolbersdorf, Zschopau",
    plz: "09434"
  },
  {
    lati: 52.5377681828, longi: 13.4111444872,
    name: "Berlin Prenzlauer Berg",
    plz: "10435"
  },
  {
    lati: 48.0402381984, longi: 12.4675062156,
    name: "Kienberg",
    plz: "83361"
  },
  {
    lati: 48.1956046694, longi: 10.7281748136,
    name: "Schwabmünchen",
    plz: "86830"
  },
  {
    lati: 48.1259645537, longi: 10.7486078505,
    name: "Langerringen",
    plz: "86853"
  },
  {
    lati: 53.0506586133, longi: 9.69796131061,
    name: "Neuenkirchen",
    plz: "29643"
  },
  {
    lati: 54.7590193739, longi: 9.56053325525,
    name: "Husby",
    plz: "24975"
  },
  {
    lati: 53.6685291983, longi: 10.2369006549,
    name: "Ahrensburg",
    plz: "22926"
  },
  {
    lati: 53.472098781, longi: 7.48652403245,
    name: "Aurich",
    plz: "26603"
  },
  {
    lati: 49.7200168588, longi: 7.85858171683,
    name: "Winterborn, Waldgrehweiler, Niedermoschel, u.a.",
    plz: "67822"
  },
  {
    lati: 48.0993991844, longi: 8.33391424897,
    name: "Unterkirnach",
    plz: "78089"
  },
  {
    lati: 50.1003083792, longi: 8.90618075097,
    name: "Hanau",
    plz: "63456"
  },
  {
    lati: 52.5078624457, longi: 13.3438723393,
    name: "Berlin Tiergarten",
    plz: "10787"
  },
  {
    lati: 52.4498961864, longi: 11.0214298369,
    name: "Oebisfelde",
    plz: "39646"
  },
  {
    lati: 50.459793194, longi: 9.7933203624,
    name: "Ebersburg",
    plz: "36157"
  },
  {
    lati: 53.233373705, longi: 9.79703088912,
    name: "Welle",
    plz: "21261"
  },
  {
    lati: 47.9427571721, longi: 10.2454808306,
    name: "Lachen",
    plz: "87760"
  },
  {
    lati: 48.8301619443, longi: 12.051565966,
    name: "Langquaid",
    plz: "84085"
  },
  {
    lati: 50.553719965, longi: 12.0836119407,
    name: "Rosenbach",
    plz: "08548"
  },
  {
    lati: 48.0322585662, longi: 8.77758980428,
    name: "Rietheim-Weilheim",
    plz: "78604"
  },
  {
    lati: 49.9027160505, longi: 8.2851903427,
    name: "Harxheim",
    plz: "55296"
  },
  {
    lati: 50.0448165915, longi: 9.02873451382,
    name: "Karlstein am Main",
    plz: "63791"
  },
  {
    lati: 50.5296159919, longi: 6.95892217322,
    name: "Altenahr, Berg, Kalenborn, Kirchsahr",
    plz: "53505"
  },
  {
    lati: 49.4717250419, longi: 7.69024101901,
    name: "Kaiserslautern",
    plz: "67661"
  },
  {
    lati: 50.4843801185, longi: 7.77383918556,
    name: "Siershahn",
    plz: "56427"
  },
  {
    lati: 53.1960528617, longi: 7.24949741248,
    name: "Bunde",
    plz: "26831"
  },
  {
    lati: 54.0732726044, longi: 13.426488286,
    name: "Greifswald",
    plz: "17491"
  },
  {
    lati: 51.7813726615, longi: 14.3327042747,
    name: "Cottbus",
    plz: "03044"
  },
  {
    lati: 52.2232291308, longi: 7.80613005685,
    name: "Tecklenburg",
    plz: "49545"
  },
  {
    lati: 51.267151824, longi: 6.77657731731,
    name: "Düsseldorf",
    plz: "40468"
  },
  {
    lati: 50.9511006395, longi: 6.92650250588,
    name: "Köln",
    plz: "50823"
  },
  {
    lati: 50.0668045944, longi: 11.5336381616,
    name: "Trebgast",
    plz: "95367"
  },
  {
    lati: 50.4826706223, longi: 7.9312478339,
    name: "Meudt, Molsberg, Hundsangen, Niederahr u.a.",
    plz: "56414"
  },
  {
    lati: 53.1506982729, longi: 8.16682189167,
    name: "Oldenburg (Oldenburg)",
    plz: "26129"
  },
  {
    lati: 48.6023852013, longi: 9.63020203043,
    name: "Gruibingen",
    plz: "73344"
  },
  {
    lati: 51.2871002199, longi: 7.18958858271,
    name: "Wuppertal",
    plz: "42281"
  },
  {
    lati: 52.1356042447, longi: 8.70801230015,
    name: "Herford",
    plz: "32049"
  },
  {
    lati: 48.8844815713, longi: 8.72358241322,
    name: "Pforzheim",
    plz: "75175"
  },
  {
    lati: 49.532290969, longi: 8.74052544359,
    name: "Gorxheimertal",
    plz: "69517"
  },
  {
    lati: 48.0601843945, longi: 8.89282174344,
    name: "Kolbingen",
    plz: "78600"
  },
  {
    lati: 48.8399818189, longi: 13.6293742451,
    name: "Hinterschmiding",
    plz: "94146"
  },
  {
    lati: 53.6941018085, longi: 13.9953135808,
    name: "Vogelsang-Warsin, Meiersberg, Mönkebude u.a.",
    plz: "17375"
  },
  {
    lati: 49.0011054063, longi: 12.6869861267,
    name: "Haselbach",
    plz: "94354"
  },
  {
    lati: 52.4837993604, longi: 13.3412839677,
    name: "Berlin",
    plz: "10825"
  },
  {
    lati: 53.9113132829, longi: 9.13470297097,
    name: "Brunsbüttel",
    plz: "25541"
  },
  {
    lati: 51.2244195436, longi: 6.77257257557,
    name: "Düsseldorf",
    plz: "40213"
  },
  {
    lati: 49.5866606673, longi: 9.36414903708,
    name: "Walldürn",
    plz: "74731"
  },
  {
    lati: 49.2184273039, longi: 9.59639596795,
    name: "Neuenstein",
    plz: "74632"
  },
  {
    lati: 48.7617503154, longi: 9.59623146708,
    name: "Adelberg",
    plz: "73099"
  },
  {
    lati: 48.3519295396, longi: 10.7632640305,
    name: "Diedorf",
    plz: "86420"
  },
  {
    lati: 48.9435888334, longi: 8.8574331314,
    name: "Mühlacker",
    plz: "75417"
  },
  {
    lati: 47.7431779875, longi: 12.3099304174,
    name: "Aschau i. Chiemgau",
    plz: "83229"
  },
  {
    lati: 54.2931017453, longi: 9.04808358463,
    name: "Sankt Annen, Rehm-Flehde-Bargen",
    plz: "25776"
  },
  {
    lati: 49.0684631023, longi: 10.1224870112,
    name: "Stimpfach",
    plz: "74597"
  },
  {
    lati: 48.5163727276, longi: 10.182501986,
    name: "Rammingen",
    plz: "89192"
  },
  {
    lati: 54.3186777991, longi: 10.2689746613,
    name: "Schönkirchen",
    plz: "24232"
  },
  {
    lati: 48.6771322354, longi: 11.290698917,
    name: "Karlshuld",
    plz: "86668"
  },
  {
    lati: 50.1055188331, longi: 11.3543044978,
    name: "Mainleus",
    plz: "95336"
  },
  {
    lati: 50.6343342625, longi: 13.4606421319,
    name: "Seiffen/Erzgeb.",
    plz: "09548"
  },
  {
    lati: 49.5955017359, longi: 7.73186834761,
    name: "Nußbach",
    plz: "67759"
  },
  {
    lati: 50.7144886294, longi: 7.04462183344,
    name: "Bonn",
    plz: "53123"
  },
  {
    lati: 50.7673424951, longi: 6.07856872506,
    name: "Aachen",
    plz: "52064"
  },
  {
    lati: 49.9813184304, longi: 6.48943933445,
    name: "Rittersdorf u.a.",
    plz: "54636"
  },
  {
    lati: 48.3649338989, longi: 7.76396214108,
    name: "Schwanau",
    plz: "77963"
  },
  {
    lati: 50.081509558, longi: 11.9822344464,
    name: "Röslau",
    plz: "95195"
  },
  {
    lati: 47.8289250779, longi: 10.2830490005,
    name: "Dietmannsried",
    plz: "87463"
  },
  {
    lati: 49.8518753981, longi: 8.65918689865,
    name: "Darmstadt",
    plz: "64285"
  },
  {
    lati: 48.6025717859, longi: 11.6305710834,
    name: "Wolnzach",
    plz: "85283"
  },
  {
    lati: 49.4397503947, longi: 11.1046866782,
    name: "Nürnberg",
    plz: "90478"
  },
  {
    lati: 49.1849793751, longi: 11.3405615052,
    name: "Freystadt",
    plz: "92342"
  },
  {
    lati: 52.2976535042, longi: 9.72797897553,
    name: "Hemmingen",
    plz: "30966"
  },
  {
    lati: 49.3645185889, longi: 9.33702741551,
    name: "Roigheim",
    plz: "74255"
  },
  {
    lati: 54.7407366747, longi: 9.40861606262,
    name: "Handewitt",
    plz: "24976"
  },
  {
    lati: 49.8611478946, longi: 10.1211223271,
    name: "Prosselsheim",
    plz: "97279"
  },
  {
    lati: 51.7022654641, longi: 12.7254584294,
    name: "Bad Schmiedeberg",
    plz: "06905"
  },
  {
    lati: 47.8841464749, longi: 8.35311767273,
    name: "Löffingen",
    plz: "79843"
  },
  {
    lati: 53.0482836042, longi: 8.63968951752,
    name: "Delmenhorst",
    plz: "27749"
  },
  {
    lati: 50.3579920568, longi: 11.6362717055,
    name: "Bad Steben",
    plz: "95138"
  },
  {
    lati: 47.7200519571, longi: 12.4713392678,
    name: "Unterwössen",
    plz: "83246"
  },
  {
    lati: 53.2078870465, longi: 9.92393600811,
    name: "Undeloh",
    plz: "21274"
  },
  {
    lati: 53.5882288167, longi: 9.98906571956,
    name: "Hamburg",
    plz: "20249"
  },
  {
    lati: 54.3490439642, longi: 13.4645167214,
    name: "Putbus",
    plz: "18581"
  },
  {
    lati: 52.3213269738, longi: 13.6216688091,
    name: "Wildau",
    plz: "15745"
  },
  {
    lati: 51.0162613682, longi: 13.7431298543,
    name: "Dresden",
    plz: "01217"
  },
  {
    lati: 50.417116968, longi: 8.06697321511,
    name: "Limburg",
    plz: "65555"
  },
  {
    lati: 52.472275872, longi: 7.04757423198,
    name: "Nordhorn",
    plz: "48527"
  },
  {
    lati: 50.72321434, longi: 7.15679068549,
    name: "Bonn",
    plz: "53227"
  },
  {
    lati: 49.8383720168, longi: 8.35298463433,
    name: "Oppenheim",
    plz: "55276"
  },
  {
    lati: 49.298625402, longi: 8.82166753998,
    name: "Zuzenhausen",
    plz: "74939"
  },
  {
    lati: 54.0518862015, longi: 11.7897221904,
    name: "Kröpelin, Carinerland",
    plz: "18236"
  },
  {
    lati: 49.0299708407, longi: 13.0108279052,
    name: "Teisnach",
    plz: "94244"
  },
  {
    lati: 49.8995798886, longi: 9.23965713083,
    name: "Leidersbach",
    plz: "63849"
  },
  {
    lati: 48.0836689511, longi: 9.31697566364,
    name: "Scheer",
    plz: "72516"
  },
  {
    lati: 50.5889239168, longi: 8.4934895561,
    name: "Wetzlar",
    plz: "35586"
  },
  {
    lati: 51.7385964183, longi: 8.93741921791,
    name: "Altenbeken",
    plz: "33184"
  },
  {
    lati: 50.1180253792, longi: 9.45462174384,
    name: "Flörsbachtal",
    plz: "63639"
  },
  {
    lati: 48.1219040581, longi: 11.6182992419,
    name: "München",
    plz: "81671"
  },
  {
    lati: 47.8799932, longi: 10.707908361,
    name: "Stöttwang",
    plz: "87677"
  },
  {
    lati: 48.006427871, longi: 7.85424067419,
    name: "Freiburg im Breisgau",
    plz: "79106"
  },
  {
    lati: 52.4908120432, longi: 13.3275338013,
    name: "Berlin Wilmersdorf",
    plz: "10717"
  },
  {
    lati: 48.6427166272, longi: 9.45651195794,
    name: "Kirchheim unter Teck",
    plz: "73230"
  },
  {
    lati: 47.8681141257, longi: 9.1902296135,
    name: "Herdwangen-Schönach",
    plz: "88634"
  },
  {
    lati: 47.78718877, longi: 7.88462535529,
    name: "Schönau im Schwarzwald",
    plz: "79677"
  },
  {
    lati: 48.7450980502, longi: 8.97970121276,
    name: "Magstadt",
    plz: "71106"
  },
  {
    lati: 49.9102081791, longi: 9.00483706797,
    name: "Schaafheim",
    plz: "64850"
  },
  {
    lati: 48.410027631, longi: 9.78586128385,
    name: "Blaubeuren",
    plz: "89143"
  },
  {
    lati: 48.5906668994, longi: 12.4284354504,
    name: "Loiching",
    plz: "84180"
  },
  {
    lati: 47.368730621, longi: 10.2680046581,
    name: "Oberstdorf",
    plz: "87561"
  },
  {
    lati: 50.87095714, longi: 12.9093563509,
    name: "Chemnitz",
    plz: "09114"
  },
  {
    lati: 49.7684155256, longi: 11.4187392841,
    name: "Pottenstein",
    plz: "91278"
  },
  {
    lati: 48.0146885855, longi: 11.4801412762,
    name: "Baierbrunn",
    plz: "82065"
  },
  {
    lati: 51.5550521129, longi: 11.4955708783,
    name: "Helbra",
    plz: "06311"
  },
  {
    lati: 47.9913108775, longi: 10.2232590173,
    name: "Memmingerberg",
    plz: "87766"
  },
  {
    lati: 52.0630795149, longi: 14.61401587,
    name: "Neuzelle",
    plz: "15898"
  },
  {
    lati: 50.8919540587, longi: 11.5880504481,
    name: "Jena, Bucha, Großpürschütz u.a.",
    plz: "07751"
  },
  {
    lati: 47.9639132446, longi: 8.32174397053,
    name: "Donaueschingen",
    plz: "78166"
  },
  {
    lati: 50.4604542496, longi: 8.05899829032,
    name: "Hadamar",
    plz: "65589"
  },
  {
    lati: 48.4404793531, longi: 8.22882009991,
    name: "Bad Peterstal-Griesbach",
    plz: "77740"
  },
  {
    lati: 50.9318330484, longi: 6.56232863079,
    name: "Elsdorf",
    plz: "50189"
  },
  {
    lati: 49.7616247755, longi: 6.73439776929,
    name: "Osburg, Gusterath, Farschweiler, Kasel u.a.",
    plz: "54317"
  },
  {
    lati: 48.1700423606, longi: 8.93190473053,
    name: "Meßstetten",
    plz: "72469"
  },
  {
    lati: 53.3957202401, longi: 9.89212844117,
    name: "Rosengarten",
    plz: "21224"
  },
  {
    lati: 54.6278458134, longi: 9.92246090766,
    name: "Arnis, Marienhof",
    plz: "24399"
  },
  {
    lati: 48.4906147862, longi: 12.1123824302,
    name: "Tiefenbach",
    plz: "84184"
  },
  {
    lati: 51.6111434238, longi: 8.31102426781,
    name: "Erwitte",
    plz: "59597"
  },
  {
    lati: 49.8984515534, longi: 8.68000587889,
    name: "Darmstadt",
    plz: "64289"
  },
  {
    lati: 51.2749715424, longi: 6.40231938762,
    name: "Viersen",
    plz: "41747"
  },
  {
    lati: 52.5809388322, longi: 11.6193703669,
    name: "Bismark",
    plz: "39599"
  },
  {
    lati: 51.2039184031, longi: 12.4735381353,
    name: "Rötha",
    plz: "04571"
  },
  {
    lati: 51.3201902967, longi: 12.5644595792,
    name: "Brandis",
    plz: "04824"
  },
  {
    lati: 52.4588976043, longi: 13.2829232107,
    name: "Berlin Dahlem",
    plz: "14195"
  },
  {
    lati: 50.395965384, longi: 6.51099121621,
    name: "Dahlem",
    plz: "53949"
  },
  {
    lati: 49.9511135806, longi: 9.65020108603,
    name: "Steinfeld",
    plz: "97854"
  },
  {
    lati: 50.4451159053, longi: 9.89938203512,
    name: "Gersfeld",
    plz: "36129"
  },
  {
    lati: 53.4871536659, longi: 10.1511251736,
    name: "Hamburg",
    plz: "21035"
  },
  {
    lati: 50.045928947, longi: 12.1810276702,
    name: "Arzberg",
    plz: "95659"
  },
  {
    lati: 50.5030838123, longi: 12.9298877725,
    name: "Crottendorf",
    plz: "09474"
  },
  {
    lati: 52.8545906671, longi: 8.73674403936,
    name: "Bassum",
    plz: "27211"
  },
  {
    lati: 52.3038902858, longi: 10.9633399337,
    name: "Rennau, Querenhorst, Mariental, Grasleben",
    plz: "38368"
  },
  {
    lati: 50.8896752266, longi: 11.6149065106,
    name: "Jena",
    plz: "07747"
  },
  {
    lati: 47.7086772826, longi: 11.1077764215,
    name: "Uffing a. Staffelsee",
    plz: "82449"
  },
  {
    lati: 50.6017112357, longi: 11.2385540672,
    name: "Saalfeld/Saale, Wittgendorf",
    plz: "07318"
  },
  {
    lati: 48.0887770009, longi: 11.6612348635,
    name: "München",
    plz: "81739"
  },
  {
    lati: 48.6210479464, longi: 12.4796393417,
    name: "Dingolfing",
    plz: "84130"
  },
  {
    lati: 52.8499509438, longi: 12.8841527745,
    name: "Fehrbellin, Temnitzquell, Märkisch Linden u.a.",
    plz: "16818"
  },
  {
    lati: 49.5417466394, longi: 7.88145689805,
    name: "Münchweiler an der Alsenz",
    plz: "67728"
  },
  {
    lati: 51.488805315, longi: 7.63154900654,
    name: "Holzwickede",
    plz: "59439"
  },
  {
    lati: 51.9196651315, longi: 8.3726743738,
    name: "Gütersloh",
    plz: "33334"
  },
  {
    lati: 51.9978254753, longi: 8.57737713124,
    name: "Bielefeld",
    plz: "33605"
  },
  {
    lati: 48.7130729761, longi: 13.3230511691,
    name: "Fürstenstein",
    plz: "94538"
  },
  {
    lati: 52.5427013455, longi: 10.0999667847,
    name: "Nienhagen",
    plz: "29336"
  },
  {
    lati: 54.8117667046, longi: 9.38352314368,
    name: "Harrislee",
    plz: "24955"
  },
  {
    lati: 49.5829546138, longi: 9.42719758788,
    name: "Höpfingen",
    plz: "74746"
  },
  {
    lati: 53.6610144365, longi: 9.03286782556,
    name: "Mittelstenahe",
    plz: "21770"
  },
  {
    lati: 51.4512810458, longi: 7.1970463572,
    name: "Bochum",
    plz: "44795"
  },
  {
    lati: 49.8513167564, longi: 8.6080298991,
    name: "Darmstadt",
    plz: "64295"
  },
  {
    lati: 48.8994881769, longi: 8.65442073372,
    name: "Pforzheim",
    plz: "75179"
  },
  {
    lati: 52.4905329563, longi: 13.4284807201,
    name: "Berlin Kreuzberg",
    plz: "12047"
  },
  {
    lati: 49.7815861339, longi: 11.1951334933,
    name: "Ebermannstadt",
    plz: "91320"
  },
  {
    lati: 51.0789023824, longi: 10.3445609027,
    name: "Mihla",
    plz: "99826"
  },
  {
    lati: 48.8097444598, longi: 10.5874890288,
    name: "Möttingen",
    plz: "86753"
  },
  {
    lati: 48.1792593211, longi: 11.5530892217,
    name: "München",
    plz: "80809"
  },
  {
    lati: 51.2231492945, longi: 12.1103800738,
    name: "Lützen",
    plz: "06686"
  },
  {
    lati: 47.937250329, longi: 12.7304498437,
    name: "Waging a. See",
    plz: "83329"
  },
  {
    lati: 52.4733621068, longi: 13.311789526,
    name: "Berlin Wilmersdorf",
    plz: "14197"
  },
  {
    lati: 50.2615786607, longi: 7.75443986273,
    name: "Nastätten u.a.",
    plz: "56355"
  },
  {
    lati: 49.1524788022, longi: 8.28154481675,
    name: "Rülzheim",
    plz: "76761"
  },
  {
    lati: 51.6875605898, longi: 8.33845241131,
    name: "Lippstadt",
    plz: "59555"
  },
  {
    lati: 51.06281679, longi: 9.61094732452,
    name: "Morschen",
    plz: "34326"
  },
  {
    lati: 54.7077148228, longi: 9.42970688682,
    name: "Oeversee",
    plz: "24988"
  },
  {
    lati: 47.4753261302, longi: 11.2032621037,
    name: "Krün",
    plz: "82493"
  },
  {
    lati: 50.8427325801, longi: 6.5501195746,
    name: "Merzenich",
    plz: "52399"
  },
  {
    lati: 50.8729182932, longi: 10.9908407745,
    name: "Elleben, Wachsenburg",
    plz: "99334"
  },
  {
    lati: 48.0784713419, longi: 10.939715919,
    name: "Penzing",
    plz: "86929"
  },
  {
    lati: 48.0724279705, longi: 11.1565896261,
    name: "Inning a. Ammersee",
    plz: "82266"
  },
  {
    lati: 48.7801674386, longi: 11.404468211,
    name: "Ingolstadt",
    plz: "85057"
  },
  {
    lati: 48.9060471192, longi: 9.14279853571,
    name: "Asperg",
    plz: "71679"
  },
  {
    lati: 54.6839174921, longi: 9.32016648515,
    name: "Wanderup",
    plz: "24997"
  },
  {
    lati: 48.5881185497, longi: 12.0197684312,
    name: "Furth",
    plz: "84095"
  },
  {
    lati: 49.3974495557, longi: 12.2122793524,
    name: "Schwarzach b. Nabburg",
    plz: "92548"
  },
  {
    lati: 54.2720136511, longi: 10.4306712949,
    name: "Selent",
    plz: "24238"
  },
  {
    lati: 50.028654026, longi: 10.4246265751,
    name: "Theres",
    plz: "97531"
  },
  {
    lati: 50.0380652742, longi: 8.06697085604,
    name: "Eltville am Rhein",
    plz: "65346"
  },
  {
    lati: 49.7386982345, longi: 8.15594376651,
    name: "Albig",
    plz: "55234"
  },
  {
    lati: 52.5159251968, longi: 13.2560695552,
    name: "Berlin Westend",
    plz: "14052"
  },
  {
    lati: 50.9808159033, longi: 6.9211374592,
    name: "Köln",
    plz: "50739"
  },
  {
    lati: 50.7283668788, longi: 7.88473191574,
    name: "Elkenroth",
    plz: "57578"
  },
  {
    lati: 49.0910416295, longi: 7.9139313859,
    name: "Bad Bergzabern u.a.",
    plz: "76887"
  },
  {
    lati: 50.748266847, longi: 7.91636052223,
    name: "Emmerzhausen, Niederdreisbach, Steinebach",
    plz: "57520"
  },
  {
    lati: 49.6220072284, longi: 8.04307696284,
    name: "Marnheim",
    plz: "67297"
  },
  {
    lati: 52.2961205105, longi: 7.46279759146,
    name: "Rheine",
    plz: "48432"
  },
  {
    lati: 50.2771103133, longi: 7.53893998934,
    name: "Waldesch, Hünenfeld",
    plz: "56323"
  },
  {
    lati: 49.7577023505, longi: 10.3383705168,
    name: "Kleinlangheim",
    plz: "97355"
  },
  {
    lati: 47.7099109085, longi: 10.3021684486,
    name: "Kempten (Allgäu)",
    plz: "87435"
  },
  {
    lati: 51.1610853868, longi: 10.9744625671,
    name: "Straußfurt",
    plz: "99634"
  },
  {
    lati: 50.7895388997, longi: 8.79511350377,
    name: "Marburg",
    plz: "35043"
  },
  {
    lati: 49.8228812305, longi: 8.90017944462,
    name: "Otzberg",
    plz: "64853"
  },
  {
    lati: 51.4425534149, longi: 7.49070870145,
    name: "Dortmund",
    plz: "44265"
  },
  {
    lati: 50.8676996137, longi: 7.61048052139,
    name: "Waldbröl",
    plz: "51545"
  },
  {
    lati: 54.4011338015, longi: 9.86592194858,
    name: "Holtsee",
    plz: "24363"
  },
  {
    lati: 53.8138748945, longi: 10.0479071332,
    name: "Kisdorf",
    plz: "24629"
  },
  {
    lati: 50.7548116752, longi: 10.3257366161,
    name: "Breitungen/Werra",
    plz: "98597"
  },
  {
    lati: 52.384144969, longi: 10.4061483348,
    name: "Didderse",
    plz: "38530"
  },
  {
    lati: 53.6120984207, longi: 10.5904300784,
    name: "Breitenfelde, Lankau",
    plz: "23881"
  },
  {
    lati: 52.4707842901, longi: 13.1516446281,
    name: "Berlin Gatow",
    plz: "14089"
  },
  {
    lati: 52.468382479, longi: 13.4631816511,
    name: "Berlin Neukölln",
    plz: "12057"
  },
  {
    lati: 51.0175535116, longi: 13.7987303158,
    name: "Dresden",
    plz: "01237"
  },
  {
    lati: 47.5725212477, longi: 11.0442624553,
    name: "Ettal",
    plz: "82488"
  },
  {
    lati: 47.8197130501, longi: 12.9272278962,
    name: "Ainring",
    plz: "83404"
  },
  {
    lati: 47.7100361421, longi: 12.9022161713,
    name: "Bayerisch Gmain",
    plz: "83457"
  },
  {
    lati: 50.6267832408, longi: 7.89514196875,
    name: "Nistertal, Enspel",
    plz: "57647"
  },
  {
    lati: 50.5568447049, longi: 6.45162053852,
    name: "Schleiden",
    plz: "53937"
  },
  {
    lati: 51.1968105751, longi: 6.77683571353,
    name: "Düsseldorf",
    plz: "40223"
  },
  {
    lati: 50.4669580936, longi: 7.38650496472,
    name: "Leutesdorf",
    plz: "56599"
  },
  {
    lati: 54.6368840656, longi: 8.76643551796,
    name: "Habel, Gröde",
    plz: "25869"
  },
  {
    lati: 54.4015280962, longi: 9.75928918871,
    name: "Groß Wittensee",
    plz: "24361"
  },
  {
    lati: 49.7913022994, longi: 9.37262501389,
    name: "Dorfprozelten",
    plz: "97904"
  },
  {
    lati: 52.051655733, longi: 9.43412256148,
    name: "Hameln",
    plz: "31789"
  },
  {
    lati: 52.9928473398, longi: 9.55535256875,
    name: "Visselhövede",
    plz: "27374"
  },
  {
    lati: 51.7382267024, longi: 12.2539944668,
    name: "Raguhn-Jeßnitz",
    plz: "06779"
  },
  {
    lati: 52.4164632731, longi: 7.99645440043,
    name: "Bramsche",
    plz: "49565"
  },
  {
    lati: 49.3300669078, longi: 8.00212892216,
    name: "Kirrweiler (Pfalz)",
    plz: "67489"
  },
  {
    lati: 48.0699467494, longi: 8.83402691768,
    name: "Mahlstetten",
    plz: "78601"
  },
  {
    lati: 51.5713397235, longi: 7.1120640272,
    name: "Gelsenkirchen",
    plz: "45892"
  },
  {
    lati: 51.8472171129, longi: 7.28911599407,
    name: "Dülmen",
    plz: "48249"
  },
  {
    lati: 48.089313485, longi: 11.4809391455,
    name: "München",
    plz: "81475"
  },
  {
    lati: 48.7252915099, longi: 9.15578986562,
    name: "Stuttgart",
    plz: "70567"
  },
  {
    lati: 50.5440726455, longi: 12.4770159276,
    name: "Steinberg",
    plz: "08237"
  },
  {
    lati: 49.454323609, longi: 12.1778065032,
    name: "Nabburg",
    plz: "92507"
  },
  {
    lati: 50.5361515321, longi: 12.4182418471,
    name: "Rodewisch",
    plz: "08228"
  },
  {
    lati: 47.8886492188, longi: 10.9478500968,
    name: "Apfeldorf",
    plz: "86974"
  },
  {
    lati: 53.5488536232, longi: 10.5083038521,
    name: "Fuhlenhagen",
    plz: "21493"
  },
  {
    lati: 50.1817171528, longi: 7.25338749406,
    name: "Pommern",
    plz: "56829"
  },
  {
    lati: 49.876464862, longi: 9.85689572977,
    name: "Thüngershem",
    plz: "97291"
  },
  {
    lati: 48.6661746158, longi: 12.3646834519,
    name: "Weng",
    plz: "84187"
  },
  {
    lati: 51.1761345728, longi: 12.9377992281,
    name: "Leisnig",
    plz: "04703"
  },
  {
    lati: 50.6703085288, longi: 12.4167068731,
    name: "Lichtentanne",
    plz: "08115"
  },
  {
    lati: 50.4593762158, longi: 8.74355079998,
    name: "Münzenberg",
    plz: "35516"
  },
  {
    lati: 50.1160667702, longi: 8.67017694803,
    name: "Frankfurt am Main, Opernturm",
    plz: "60306"
  },
  {
    lati: 47.8397758653, longi: 10.8104458438,
    name: "Schwabsoien",
    plz: "86987"
  },
  {
    lati: 53.4829722314, longi: 10.2268464815,
    name: "Hamburg",
    plz: "21029"
  },
  {
    lati: 48.7646948808, longi: 10.5957064402,
    name: "Mönchsdeggingen",
    plz: "86751"
  },
  {
    lati: 52.5238119439, longi: 13.4428782231,
    name: "Berlin Friedrichshain",
    plz: "10249"
  },
  {
    lati: 48.2419409949, longi: 9.16087061525,
    name: "Neufra",
    plz: "72419"
  },
  {
    lati: 49.400607871, longi: 9.20725276157,
    name: "Elztal",
    plz: "74834"
  },
  {
    lati: 54.8113215113, longi: 9.4780570205,
    name: "Flensburg",
    plz: "24944"
  },
  {
    lati: 50.1330501736, longi: 11.7994704186,
    name: "Zell",
    plz: "95239"
  },
  {
    lati: 47.7574912724, longi: 9.72260042207,
    name: "Waldburg",
    plz: "88289"
  },
  {
    lati: 50.0256376245, longi: 7.09839511996,
    name: "Reil",
    plz: "56861"
  },
  {
    lati: 50.7415075901, longi: 7.16822793874,
    name: "Bonn",
    plz: "53229"
  },
  {
    lati: 49.5709815849, longi: 7.78156030594,
    name: "Gundersweiler, Gonbach u.a.",
    plz: "67724"
  },
  {
    lati: 49.324836222, longi: 9.35522960958,
    name: "Möckmühl",
    plz: "74219"
  },
  {
    lati: 52.3322773, longi: 8.89734282771,
    name: "Minden",
    plz: "32425"
  },
  {
    lati: 54.3336788699, longi: 10.1921459564,
    name: "Kiel",
    plz: "24149"
  },
  {
    lati: 49.4680866926, longi: 9.47403820162,
    name: "Rosenberg",
    plz: "74749"
  },
  {
    lati: 48.5425379389, longi: 9.14668077105,
    name: "Kirchentellinsfurt",
    plz: "72138"
  },
  {
    lati: 51.2945038101, longi: 6.84727505168,
    name: "Ratingen",
    plz: "40878"
  },
  {
    lati: 49.9477273555, longi: 7.93733075122,
    name: "Bingen am Rhein",
    plz: "55411"
  },
  {
    lati: 50.1253019526, longi: 8.68648074982,
    name: "Frankfurt am Main",
    plz: "60318"
  },
  {
    lati: 48.2208616136, longi: 8.72167347704,
    name: "Zimmern unter der Burg",
    plz: "72369"
  },
  {
    lati: 51.7198768581, longi: 13.7655979453,
    name: "Crinitz",
    plz: "03246"
  },
  {
    lati: 52.8829197749, longi: 14.0181788961,
    name: "Oderberg u.a.",
    plz: "16248"
  },
  {
    lati: 51.8792154446, longi: 14.5263589926,
    name: "Jänschwalde",
    plz: "03197"
  },
  {
    lati: 48.4591077629, longi: 11.6622177184,
    name: "Kirchdorf a.d. Amper",
    plz: "85414"
  },
  {
    lati: 48.5095322588, longi: 10.5373656957,
    name: "Holzheim",
    plz: "89438"
  },
  {
    lati: 50.7157339136, longi: 7.66334527829,
    name: "Birnbach",
    plz: "57612"
  },
  {
    lati: 50.8484676605, longi: 8.29676655144,
    name: "Dietzhölztal",
    plz: "35716"
  },
  {
    lati: 53.1068680263, longi: 8.39131558865,
    name: "Hude (Oldenburg)",
    plz: "27798"
  },
  {
    lati: 50.055775782, longi: 11.2355005991,
    name: "Weismain",
    plz: "96260"
  },
  {
    lati: 47.6958776973, longi: 10.9658197377,
    name: "Rottenbuch",
    plz: "82401"
  },
  {
    lati: 49.2058841903, longi: 12.2448878791,
    name: "Nittenau",
    plz: "93149"
  },
  {
    lati: 54.3056134999, longi: 9.2418529033,
    name: "Delve",
    plz: "25788"
  },
  {
    lati: 53.1947402066, longi: 14.1656573931,
    name: "Biesendahlshof, Berkholz-Meyenburg",
    plz: "16306"
  },
  {
    lati: 52.6339106261, longi: 10.0965011831,
    name: "Celle",
    plz: "29223"
  },
  {
    lati: 51.3115991359, longi: 7.52008571125,
    name: "Hagen",
    plz: "58091"
  },
  {
    lati: 50.1475708361, longi: 10.7350097622,
    name: "Pfarrweisach",
    plz: "96176"
  },
  {
    lati: 50.2761924369, longi: 10.1594333202,
    name: "Burglauer",
    plz: "97724"
  },
  {
    lati: 49.6161816197, longi: 12.1840141925,
    name: "Pirk",
    plz: "92712"
  },
  {
    lati: 49.5372169265, longi: 12.0434554648,
    name: "Schnaittenbach",
    plz: "92253"
  },
  {
    lati: 50.8618047876, longi: 12.8342808695,
    name: "Röhrsdorf",
    plz: "09247"
  },
  {
    lati: 54.304138257, longi: 13.1199681984,
    name: "Stralsund",
    plz: "18439"
  },
  {
    lati: 48.574240556, longi: 13.6862095665,
    name: "Untergriesbach",
    plz: "94107"
  },
  {
    lati: 52.2704004, longi: 8.85142768144,
    name: "Minden",
    plz: "32429"
  },
  {
    lati: 50.3898646433, longi: 8.8987771794,
    name: "Echzell",
    plz: "61209"
  },
  {
    lati: 50.404743184, longi: 9.68416788771,
    name: "Kalbach",
    plz: "36148"
  },
  {
    lati: 48.0251399804, longi: 9.79200575944,
    name: "Hochdorf",
    plz: "88454"
  },
  {
    lati: 51.2933052476, longi: 11.3817729743,
    name: "Roßleben, Wiehe, Donndorf, Nausitz, Gehofen",
    plz: "06571"
  },
  {
    lati: 51.4892083609, longi: 14.3055664603,
    name: "Spreetal, Elsterheide",
    plz: "02979"
  },
  {
    lati: 50.8800315812, longi: 6.74563254233,
    name: "Kerpen",
    plz: "50169"
  },
  {
    lati: 50.7427481004, longi: 12.6743117492,
    name: "Hohndorf",
    plz: "09394"
  },
  {
    lati: 50.8372879939, longi: 12.950358568,
    name: "Chemnitz",
    plz: "09130"
  },
  {
    lati: 52.2936587387, longi: 13.057608438,
    name: "Michendorf",
    plz: "14552"
  },
  {
    lati: 48.4143691725, longi: 12.1973337286,
    name: "Baierbach",
    plz: "84171"
  },
  {
    lati: 47.9385423377, longi: 8.72303921867,
    name: "Immendingen",
    plz: "78194"
  },
  {
    lati: 48.0564162393, longi: 8.8018581602,
    name: "Dürbheim",
    plz: "78589"
  },
  {
    lati: 51.1680134644, longi: 7.05722736733,
    name: "Solingen",
    plz: "42655"
  },
  {
    lati: 51.1970857615, longi: 6.79428130751,
    name: "Düsseldorf",
    plz: "40225"
  },
  {
    lati: 54.6318729137, longi: 9.89729374518,
    name: "Rabenkirchen-Faulück",
    plz: "24407"
  },
  {
    lati: 48.9206753902, longi: 10.1508653068,
    name: "Rainau",
    plz: "73492"
  },
  {
    lati: 52.1982191023, longi: 10.8703151346,
    name: "Räbke",
    plz: "38375"
  },
  {
    lati: 47.9925132061, longi: 9.30232593141,
    name: "Mengen",
    plz: "88512"
  },
  {
    lati: 52.2914386642, longi: 13.2635105439,
    name: "Ludwigsfelde",
    plz: "14974"
  },
  {
    lati: 54.0241702501, longi: 13.9246158062,
    name: "Kröslin, Krummin, Lassan u.a.",
    plz: "17440"
  },
  {
    lati: 54.1531786513, longi: 12.0592257453,
    name: "Rostock",
    plz: "18109"
  },
  {
    lati: 50.9631037638, longi: 13.389607505,
    name: "Halsbrücke",
    plz: "09633"
  },
  {
    lati: 51.5578823529, longi: 13.9269699392,
    name: "Schipkau",
    plz: "01994"
  },
  {
    lati: 49.4500213441, longi: 10.4887292645,
    name: "Obernzenn",
    plz: "91619"
  },
  {
    lati: 49.7402069935, longi: 11.5241556921,
    name: "Pegnitz",
    plz: "91257"
  },
  {
    lati: 49.3087154948, longi: 11.9295181839,
    name: "Rieden",
    plz: "92286"
  },
  {
    lati: 50.4290157577, longi: 11.329650767,
    name: "Steinbach a. Wald",
    plz: "96361"
  },
  {
    lati: 48.0097590511, longi: 9.65440805033,
    name: "Bad Schussenried",
    plz: "88427"
  },
  {
    lati: 48.582870605, longi: 10.0184399398,
    name: "Altheim (Alb)",
    plz: "89174"
  },
  {
    lati: 49.5080216642, longi: 10.0163580936,
    name: "Bieberehren",
    plz: "97243"
  },
  {
    lati: 49.1077838896, longi: 8.811390774,
    name: "Zaisenhausen",
    plz: "75059"
  },
  {
    lati: 47.7727829751, longi: 8.0952784301,
    name: "St. Blasien, Ibach",
    plz: "79837"
  },
  {
    lati: 48.0050481162, longi: 7.85388467402,
    name: "Freiburg im Breisgau",
    plz: "79106"
  },
  {
    lati: 51.8030209137, longi: 6.12485320557,
    name: "Kleve",
    plz: "47533"
  },
  {
    lati: 51.1955595749, longi: 7.19624853396,
    name: "Remscheid",
    plz: "42855"
  },
  {
    lati: 49.4752119891, longi: 7.36461077237,
    name: "Herschweiler-Pettersheim",
    plz: "66909"
  },
  {
    lati: 49.474660428, longi: 7.58347148936,
    name: "Mackenbach",
    plz: "67686"
  },
  {
    lati: 48.124165752, longi: 7.80703322072,
    name: "Teningen",
    plz: "79331"
  },
  {
    lati: 49.6808020897, longi: 8.52856957893,
    name: "Einhausen",
    plz: "64683"
  },
  {
    lati: 49.3432738521, longi: 8.92003224721,
    name: "Epfenbach",
    plz: "74925"
  },
  {
    lati: 54.6988896555, longi: 9.21195055647,
    name: "Großenwiehe, Lindewitt",
    plz: "24969"
  },
  {
    lati: 47.7957487633, longi: 7.70618446536,
    name: "Badenweiler",
    plz: "79410"
  },
  {
    lati: 47.7096892115, longi: 10.0569398946,
    name: "Isny im Allgäu",
    plz: "88316"
  },
  {
    lati: 49.1730352835, longi: 10.6552409683,
    name: "Ornbau",
    plz: "91737"
  },
  {
    lati: 49.4181777534, longi: 10.5781766704,
    name: "Rügland",
    plz: "91622"
  },
  {
    lati: 49.9456885992, longi: 7.11671400564,
    name: "Traben-Trarbach",
    plz: "56841"
  },
  {
    lati: 51.5067771044, longi: 7.33469548005,
    name: "Dortmund",
    plz: "44388"
  },
  {
    lati: 51.2269005088, longi: 6.46961530843,
    name: "Mönchengladbach",
    plz: "41066"
  },
  {
    lati: 51.2980775497, longi: 6.81960857511,
    name: "Ratingen",
    plz: "40880"
  },
  {
    lati: 51.2623975901, longi: 6.51883760501,
    name: "Willich",
    plz: "47877"
  },
  {
    lati: 53.1637002925, longi: 7.33369628982,
    name: "Weener",
    plz: "26826"
  },
  {
    lati: 49.1085511748, longi: 7.97889017804,
    name: "Bad Bergzabern u.a.",
    plz: "76887"
  },
  {
    lati: 50.7508401969, longi: 8.41744678327,
    name: "Siegbach",
    plz: "35768"
  },
  {
    lati: 53.9568398109, longi: 9.70464635724,
    name: "Kellinghusen",
    plz: "25548"
  },
  {
    lati: 52.1639265181, longi: 11.1738470853,
    name: "Harbke, Sommersdorf, Wefensleben, Ummendorf, Eilsleben",
    plz: "39365"
  },
  {
    lati: 53.0147061421, longi: 10.6970445322,
    name: "Oetzen",
    plz: "29588"
  },
  {
    lati: 53.4536018822, longi: 10.2668642004,
    name: "Hamburg",
    plz: "21039"
  },
  {
    lati: 53.47919479, longi: 14.1298756787,
    name: "Blankensee, Grambow u.a.",
    plz: "17322"
  },
  {
    lati: 50.1973293385, longi: 7.15319108667,
    name: "Ediger-Eller",
    plz: "56814"
  },
  {
    lati: 51.1577041749, longi: 6.35251221199,
    name: "Mönchengladbach",
    plz: "41179"
  },
  {
    lati: 48.1290197464, longi: 11.360343399,
    name: "Germering",
    plz: "82110"
  },
  {
    lati: 50.9831550768, longi: 11.3239812021,
    name: "Weimar",
    plz: "99423"
  },
  {
    lati: 52.9789039883, longi: 12.3281793578,
    name: "Kyritz u.a.",
    plz: "16866"
  },
  {
    lati: 48.0150097656, longi: 12.1468956272,
    name: "Ramerberg",
    plz: "83561"
  },
  {
    lati: 49.5129591532, longi: 12.5435869377,
    name: "Schönsee",
    plz: "92539"
  },
  {
    lati: 48.3169129664, longi: 12.6288198467,
    name: "Pleiskirchen",
    plz: "84568"
  },
  {
    lati: 48.5170490115, longi: 13.4028188692,
    name: "Neuburg a. Inn",
    plz: "94127"
  },
  {
    lati: 48.6698617371, longi: 10.5391505981,
    name: "Lutzingen",
    plz: "89440"
  },
  {
    lati: 50.1005430597, longi: 7.24154058645,
    name: "Ediger-Eller",
    plz: "56814"
  },
  {
    lati: 49.2479864669, longi: 7.36559359843,
    name: "Zweibrücken",
    plz: "66482"
  },
  {
    lati: 48.3589092486, longi: 8.40574207461,
    name: "Alpirsbach",
    plz: "72275"
  },
  {
    lati: 49.2446853358, longi: 8.35792371352,
    name: "Lingenfeld",
    plz: "67360"
  },
  {
    lati: 48.4582700066, longi: 8.54797517285,
    name: "Schopfloch",
    plz: "72296"
  },
  {
    lati: 48.8161413271, longi: 9.20335401095,
    name: "Stuttgart",
    plz: "70376"
  },
  {
    lati: 50.9893833774, longi: 11.6107179788,
    name: "Neuengönna u.a.",
    plz: "07778"
  },
  {
    lati: 53.990548296, longi: 9.35574060337,
    name: "Vaale",
    plz: "25594"
  },
  {
    lati: 50.76993341, longi: 12.0255433552,
    name: "Weida, Harth-Pöllnitz, Wünschendorf",
    plz: "07570"
  },
  {
    lati: 51.7053816463, longi: 12.0450877724,
    name: "Südliches Anhalt u.a.",
    plz: "06369"
  },
  {
    lati: 49.2955592132, longi: 12.287100636,
    name: "Bodenwöhr",
    plz: "92439"
  },
  {
    lati: 53.5738302005, longi: 13.2901804561,
    name: "Neubrandenburg",
    plz: "17034"
  },
  {
    lati: 53.1222831416, longi: 8.16781648008,
    name: "Oldenburg (Oldenburg)",
    plz: "26131"
  },
  {
    lati: 50.6323692882, longi: 7.7600650083,
    name: "Malberg, Norken, Höchstenbach u.a.",
    plz: "57629"
  },
  {
    lati: 53.5677903438, longi: 9.9814166678,
    name: "Hamburg",
    plz: "20146"
  },
  {
    lati: 53.5947917374, longi: 9.99945262835,
    name: "Hamburg",
    plz: "22299"
  },
  {
    lati: 49.4672224532, longi: 10.2679623105,
    name: "Gallmersgarten",
    plz: "91605"
  },
  {
    lati: 47.7267121752, longi: 10.4179915569,
    name: "Betzigau",
    plz: "87488"
  },
  {
    lati: 51.3994768946, longi: 6.32629922822,
    name: "Wachtendonk",
    plz: "47669"
  },
  {
    lati: 51.5217567752, longi: 6.43388574511,
    name: "Issum",
    plz: "47661"
  },
  {
    lati: 51.490081697, longi: 7.25109141799,
    name: "Bochum",
    plz: "44791"
  },
  {
    lati: 49.2863374921, longi: 8.09412845901,
    name: "Edenkoben",
    plz: "67480"
  },
  {
    lati: 50.7682277797, longi: 6.95507403697,
    name: "Bornheim",
    plz: "53332"
  },
  {
    lati: 49.8123185898, longi: 8.44510484484,
    name: "Stockstadt am Rhein",
    plz: "64589"
  },
  {
    lati: 48.1697496318, longi: 8.78747377766,
    name: "Deilingen",
    plz: "78586"
  },
  {
    lati: 49.4909221395, longi: 11.2009691358,
    name: "Schwaig b. Nürnberg, Behringersdorfer Forst",
    plz: "90571"
  },
  {
    lati: 48.0115133412, longi: 12.3103736984,
    name: "Amerang",
    plz: "83123"
  },
  {
    lati: 50.8196652457, longi: 13.304050546,
    name: "Brand-Erbisdorf, Großhartmannsdorf",
    plz: "09618"
  },
  {
    lati: 54.0292297258, longi: 13.7172141038,
    name: "Wolgast",
    plz: "17438"
  },
  {
    lati: 53.247677607, longi: 11.5999418843,
    name: "Grabow u.a.",
    plz: "19300"
  },
  {
    lati: 52.2239300802, longi: 10.9052939108,
    name: "Süpplingen, Frellstedt",
    plz: "38373"
  },
  {
    lati: 52.4357151427, longi: 10.9059543905,
    name: "Danndorf",
    plz: "38461"
  },
  {
    lati: 48.3135689851, longi: 10.4553809346,
    name: "Münsterhausen",
    plz: "86505"
  },
  {
    lati: 48.5747512987, longi: 9.65372227134,
    name: "Mühlhausen im Täle",
    plz: "73347"
  },
  {
    lati: 54.1177802272, longi: 9.53413144189,
    name: "Beringstedt",
    plz: "25575"
  },
  {
    lati: 50.2188730677, longi: 10.13555748,
    name: "Nüdlingen",
    plz: "97720"
  },
  {
    lati: 50.8371817463, longi: 10.1766641282,
    name: "Tiefenort",
    plz: "36469"
  },
  {
    lati: 49.7674857986, longi: 7.02572191589,
    name: "Malborn",
    plz: "54426"
  },
  {
    lati: 52.0622539754, longi: 6.81072583003,
    name: "Vreden",
    plz: "48691"
  },
  {
    lati: 51.5250541701, longi: 7.64926481135,
    name: "Unna",
    plz: "59425"
  },
  {
    lati: 49.2936594294, longi: 8.28909281303,
    name: "Gommersheim",
    plz: "67377"
  },
  {
    lati: 52.136731478, longi: 8.47051188024,
    name: "Spenge",
    plz: "32139"
  },
  {
    lati: 48.4144621534, longi: 8.86678877526,
    name: "Hirrlingen",
    plz: "72145"
  },
  {
    lati: 48.3457136472, longi: 13.2526333863,
    name: "Kirchham",
    plz: "94148"
  },
  {
    lati: 53.7766240811, longi: 13.7798563752,
    name: "Ducherow",
    plz: "17398"
  },
  {
    lati: 53.9623956902, longi: 10.3974206299,
    name: "Rohlstorf",
    plz: "23821"
  },
  {
    lati: 48.4263446343, longi: 10.4594066901,
    name: "Röfingen",
    plz: "89365"
  },
  {
    lati: 47.9531496527, longi: 11.0498161942,
    name: "Dießen a. Ammersee",
    plz: "86911"
  },
  {
    lati: 53.9039394093, longi: 11.5441756331,
    name: "Wismar",
    plz: "23970"
  },
  {
    lati: 50.5105172261, longi: 8.83040483049,
    name: "Lich",
    plz: "35423"
  },
  {
    lati: 51.5018049037, longi: 7.97043181931,
    name: "Ense",
    plz: "59469"
  },
  {
    lati: 50.4202067954, longi: 8.16072815005,
    name: "Runkel",
    plz: "65594"
  },
  {
    lati: 51.212686691, longi: 6.77420779155,
    name: "Düsseldorf",
    plz: "40217"
  },
  {
    lati: 49.0826445981, longi: 7.6948626626,
    name: "Fischbach, Erfweiler u.a.",
    plz: "66996"
  },
  {
    lati: 51.5120609018, longi: 7.27809085648,
    name: "Bochum",
    plz: "44805"
  },
  {
    lati: 51.5078816456, longi: 13.5654960064,
    name: "Hohenleipisch",
    plz: "04934"
  },
  {
    lati: 48.673071751, longi: 13.7164735917,
    name: "Sonnen",
    plz: "94164"
  },
  {
    lati: 49.6546489135, longi: 7.64880017846,
    name: "Lauterecken u.a.",
    plz: "67742"
  },
  {
    lati: 50.2798455112, longi: 7.83342795933,
    name: "Singhofen",
    plz: "56379"
  },
  {
    lati: 50.1828703035, longi: 9.04690677389,
    name: "Langenselbold",
    plz: "63505"
  },
  {
    lati: 50.3250758303, longi: 11.9352181211,
    name: "Hof",
    plz: "95028"
  },
  {
    lati: 48.8970162913, longi: 12.3440199307,
    name: "Sünching",
    plz: "93104"
  },
  {
    lati: 51.4576475601, longi: 12.3653370075,
    name: "Rackwitz",
    plz: "04519"
  },
  {
    lati: 48.8457368652, longi: 12.8163664157,
    name: "Mariaposching",
    plz: "94553"
  },
  {
    lati: 50.9020768878, longi: 6.96598560621,
    name: "Köln",
    plz: "50968"
  },
  {
    lati: 50.7576000178, longi: 6.1048022781,
    name: "Aachen",
    plz: "52066"
  },
  {
    lati: 50.745460404, longi: 6.21775210328,
    name: "Stolberg (Rhld.)",
    plz: "52223"
  },
  {
    lati: 47.5983372886, longi: 8.32809405878,
    name: "Küssaberg",
    plz: "79790"
  },
  {
    lati: 50.0445870366, longi: 8.66371279879,
    name: "Neu-Isenburg",
    plz: "63263"
  },
  {
    lati: 49.8597828935, longi: 8.56050661033,
    name: "Griesheim",
    plz: "64347"
  },
  {
    lati: 52.6188091327, longi: 13.4334714758,
    name: "Berlin Französisch Buchholz",
    plz: "13127"
  },
  {
    lati: 51.130768056, longi: 12.799654648,
    name: "Colditz",
    plz: "04680"
  },
  {
    lati: 49.9888203158, longi: 7.00294783129,
    name: "Ürzig",
    plz: "54539"
  },
  {
    lati: 47.7118097284, longi: 9.27140424323,
    name: "Daisendorf",
    plz: "88718"
  },
  {
    lati: 50.5417950909, longi: 9.98682340606,
    name: "Hilders, Ehrenberg",
    plz: "36115"
  },
  {
    lati: 48.5243658018, longi: 10.000160216,
    name: "Breitingen",
    plz: "89183"
  },
  {
    lati: 50.9576587093, longi: 7.5301080665,
    name: "Wiehl",
    plz: "51674"
  },
  {
    lati: 51.2912749077, longi: 8.65574384532,
    name: "Willingen (Upland)",
    plz: "34508"
  },
  {
    lati: 48.7961648495, longi: 9.16344565994,
    name: "Stuttgart",
    plz: "70192"
  },
  {
    lati: 51.6407299905, longi: 10.4455122035,
    name: "Bad Lauterberg",
    plz: "37431"
  },
  {
    lati: 50.890812377, longi: 13.4462824638,
    name: "Bobritzsch",
    plz: "09627"
  },
  {
    lati: 51.4995574083, longi: 7.19796145498,
    name: "Bochum",
    plz: "44809"
  },
  {
    lati: 51.5386701214, longi: 7.22414079616,
    name: "Herne",
    plz: "44623"
  },
  {
    lati: 53.0529620552, longi: 10.6981259224,
    name: "Weste",
    plz: "29599"
  },
  {
    lati: 53.6479563553, longi: 14.1294622629,
    name: "Eggesin",
    plz: "17367"
  },
  {
    lati: 51.7260421786, longi: 14.6016868433,
    name: "Forst/ Lausitz",
    plz: "03149"
  },
  {
    lati: 49.2889620402, longi: 7.60521147127,
    name: "Höheinöd, Petersberg u.a.",
    plz: "66989"
  },
  {
    lati: 48.6911431184, longi: 12.2141591843,
    name: "Ergoldsbach",
    plz: "84061"
  },
  {
    lati: 48.0894150415, longi: 7.9696615824,
    name: "Waldkirch",
    plz: "79183"
  },
  {
    lati: 51.5656363728, longi: 8.08596955252,
    name: "Soest",
    plz: "59494"
  },
  {
    lati: 52.0648346247, longi: 10.6940569132,
    name: "Hedeper",
    plz: "38322"
  },
  {
    lati: 49.2120973192, longi: 10.8025623064,
    name: "Mitteleschenbach",
    plz: "91734"
  },
  {
    lati: 52.3888089356, longi: 13.3902016822,
    name: "Berlin",
    plz: "12307"
  },
  {
    lati: 53.8790734031, longi: 9.40506594841,
    name: "Beidenfleth, Klein Kampen",
    plz: "25573"
  },
  {
    lati: 48.0643921076, longi: 9.43537002054,
    name: "Herbertingen",
    plz: "88518"
  },
  {
    lati: 48.5510409653, longi: 9.42782074815,
    name: "Erkenbrechtsweiler",
    plz: "73268"
  },
  {
    lati: 54.0058042525, longi: 10.1956966794,
    name: "Rickling",
    plz: "24635"
  },
  {
    lati: 53.8225035901, longi: 12.7790033173,
    name: "Neukalen",
    plz: "17154"
  },
  {
    lati: 50.6363996036, longi: 8.17381434933,
    name: "Driedorf",
    plz: "35759"
  },
  {
    lati: 50.5755429271, longi: 7.62762319397,
    name: "Raubach",
    plz: "56316"
  },
  {
    lati: 50.7197289261, longi: 7.11980153417,
    name: "Bonn",
    plz: "53113"
  },
  {
    lati: 53.6764996986, longi: 6.97506063354,
    name: "Juist, Memmert",
    plz: "26571"
  },
  {
    lati: 53.8084173359, longi: 10.2719952026,
    name: "Seth",
    plz: "23845"
  },
  {
    lati: 51.9077716542, longi: 10.4098020127,
    name: "Goslar",
    plz: "38644"
  },
  {
    lati: 51.57977901, longi: 12.2720945596,
    name: "Roitzsch, Petersroda",
    plz: "06809"
  },
  {
    lati: 51.0079328498, longi: 9.9424970244,
    name: "Nentershausen",
    plz: "36214"
  },
  {
    lati: 53.3491620043, longi: 9.98307025386,
    name: "Harmstorf",
    plz: "21228"
  },
  {
    lati: 53.5565863669, longi: 9.98002007308,
    name: "Hamburg",
    plz: "20355"
  },
  {
    lati: 54.1771011885, longi: 13.8081249605,
    name: "Karlshagen",
    plz: "17449"
  },
  {
    lati: 49.9228809786, longi: 10.75536086,
    name: "Viereth-Trunstadt",
    plz: "96191"
  },
  {
    lati: 52.4831228487, longi: 13.2365323322,
    name: "Berlin Grunewald",
    plz: "14193"
  },
  {
    lati: 52.4394653423, longi: 13.3462048804,
    name: "Berlin Lankwitz",
    plz: "12247"
  },
  {
    lati: 51.0231900733, longi: 13.7659262538,
    name: "Dresden",
    plz: "01219"
  },
  {
    lati: 50.9921664962, longi: 14.6519787243,
    name: "Kottmar",
    plz: "02739"
  },
  {
    lati: 52.4058461195, longi: 7.62745102843,
    name: "Hopsten",
    plz: "48496"
  },
  {
    lati: 50.9135841182, longi: 6.7841380802,
    name: "Frechen",
    plz: "50226"
  },
  {
    lati: 47.6029486507, longi: 9.60202214601,
    name: "Kressbronn am Bodensee",
    plz: "88079"
  },
  {
    lati: 48.1935676023, longi: 11.3753306363,
    name: "Gröbenzell",
    plz: "82194"
  },
  {
    lati: 49.2030835628, longi: 10.6898531852,
    name: "Merkendorf",
    plz: "91732"
  },
  {
    lati: 51.7370437555, longi: 11.0156266476,
    name: "Thale, Blankenburg",
    plz: "06502"
  },
  {
    lati: 49.6784097889, longi: 12.0304045692,
    name: "Mantel",
    plz: "92708"
  },
  {
    lati: 48.7070359949, longi: 8.95046436899,
    name: "Sindelfingen",
    plz: "71069"
  },
  {
    lati: 50.0862947651, longi: 6.27367205829,
    name: "Arzfeld",
    plz: "54687"
  },
  {
    lati: 48.2178001285, longi: 7.81436047318,
    name: "Herbolzheim",
    plz: "79336"
  },
  {
    lati: 52.5180439147, longi: 13.9513364728,
    name: "Lichtenow, Altlandsberg u.a.",
    plz: "15345"
  },
  {
    lati: 49.1795022108, longi: 9.26776794651,
    name: "Erlenbach",
    plz: "74235"
  },
  {
    lati: 47.6991942028, longi: 9.56495798017,
    name: "Meckenbeuren",
    plz: "88074"
  },
  {
    lati: 49.1951569703, longi: 11.025238319,
    name: "Georgensgmünd",
    plz: "91166"
  },
  {
    lati: 50.2211543787, longi: 10.9909223047,
    name: "Niederfüllbach",
    plz: "96489"
  },
  {
    lati: 50.7866558192, longi: 8.01618910975,
    name: "Neunkirchen",
    plz: "57290"
  },
  {
    lati: 53.5368030036, longi: 8.14409157703,
    name: "Wilhelmshaven",
    plz: "26384"
  },
  {
    lati: 50.1986218988, longi: 10.9645629133,
    name: "Untersiemau",
    plz: "96253"
  },
  {
    lati: 54.7110482611, longi: 9.80839988257,
    name: "Esgrus, Schrepperie",
    plz: "24402"
  },
  {
    lati: 48.8605142371, longi: 10.0286849832,
    name: "Aalen",
    plz: "73434"
  },
  {
    lati: 50.822017337, longi: 12.6800690108,
    name: "Callenberg, Hohenstein-Ernstthal, Bernsdorf",
    plz: "09337"
  },
  {
    lati: 54.7493674692, longi: 8.86722215544,
    name: "Risum-Lindholm, Stedesand",
    plz: "25920"
  },
  {
    lati: 51.5107852129, longi: 12.8311058259,
    name: "Mockrehna",
    plz: "04862"
  },
  {
    lati: 48.5554776477, longi: 12.8216339299,
    name: "Arnstorf",
    plz: "94424"
  },
  {
    lati: 48.3888488791, longi: 13.0044533618,
    name: "Triftern",
    plz: "84371"
  },
  {
    lati: 51.4377243459, longi: 7.00977408303,
    name: "Essen",
    plz: "45130"
  },
  {
    lati: 52.295915557, longi: 7.10855476037,
    name: "Bad Bentheim",
    plz: "48455"
  },
  {
    lati: 48.0737461697, longi: 10.1525193612,
    name: "Fellheim",
    plz: "87748"
  },
  {
    lati: 53.4858454642, longi: 10.5475208821,
    name: "Woltersdorf, Müssen u.a.",
    plz: "21516"
  },
  {
    lati: 50.2641166677, longi: 10.9641166626,
    name: "Coburg",
    plz: "96450"
  },
  {
    lati: 50.2135930797, longi: 7.56545342328,
    name: "Boppard",
    plz: "56154"
  },
  {
    lati: 51.5132780049, longi: 7.47537902257,
    name: "Dortmund",
    plz: "44135"
  },
  {
    lati: 48.0543757834, longi: 7.65792212329,
    name: "Ihringen",
    plz: "79241"
  },
  {
    lati: 50.1630944027, longi: 8.6234911407,
    name: "Frankfurt am Main",
    plz: "60439"
  },
  {
    lati: 53.2513682019, longi: 8.94638825539,
    name: "Worpswede",
    plz: "27726"
  },
  {
    lati: 49.8124649079, longi: 8.06365765074,
    name: "Armsheim",
    plz: "55288"
  },
  {
    lati: 48.0325090415, longi: 12.2466908079,
    name: "Eiselfing",
    plz: "83549"
  },
  {
    lati: 51.5158043152, longi: 6.52022943269,
    name: "Kamp-Lintfort",
    plz: "47475"
  },
  {
    lati: 50.3392249203, longi: 6.82315366626,
    name: "Reifferscheid, Kaltenborn, Wershofen u.a.",
    plz: "53520"
  },
  {
    lati: 54.1950787901, longi: 9.08895183405,
    name: "Heide u.a.",
    plz: "25746"
  },
  {
    lati: 53.301753175, longi: 10.0573087109,
    name: "Brackel",
    plz: "21438"
  },
  {
    lati: 53.4466511537, longi: 10.1467454011,
    name: "Hamburg",
    plz: "21037"
  },
  {
    lati: 47.6198317408, longi: 10.5048149996,
    name: "Nesselwang",
    plz: "87484"
  },
  {
    lati: 47.7330055508, longi: 8.75707196573,
    name: "Gottmadingen",
    plz: "78244"
  },
  {
    lati: 47.7280214614, longi: 8.84382468689,
    name: "Rielasingen-Worblingen",
    plz: "78239"
  },
  {
    lati: 49.0209119164, longi: 8.60164291055,
    name: "Walzbachtal",
    plz: "75045"
  },
  {
    lati: 49.2685890234, longi: 11.9222763703,
    name: "Schmidmühlen",
    plz: "92287"
  },
  {
    lati: 51.4689816839, longi: 7.2208807946,
    name: "Bochum",
    plz: "44789"
  },
  {
    lati: 50.9809602233, longi: 6.70062346995,
    name: "Bergheim",
    plz: "50129"
  },
  {
    lati: 49.7878506699, longi: 6.829441307,
    name: "Mehring",
    plz: "54346"
  },
  {
    lati: 50.505571817, longi: 7.5589806216,
    name: "Anhausen",
    plz: "56584"
  },
  {
    lati: 48.6246294388, longi: 8.51300807822,
    name: "Simmersfeld",
    plz: "72226"
  },
  {
    lati: 48.9333987553, longi: 9.18939244312,
    name: "Freiberg am Neckar",
    plz: "71691"
  },
  {
    lati: 50.6843851725, longi: 12.1058292964,
    name: "Langenwetzendorf",
    plz: "07957"
  },
  {
    lati: 49.1848196467, longi: 7.83915589735,
    name: "Hauenstein",
    plz: "76846"
  },
  {
    lati: 50.4037817761, longi: 8.08721594593,
    name: "Limburg",
    plz: "65553"
  },
  {
    lati: 49.0970027277, longi: 10.5440917495,
    name: "Ehingen",
    plz: "91725"
  },
  {
    lati: 51.7200081184, longi: 8.82868778113,
    name: "Paderborn",
    plz: "33100"
  },
  {
    lati: 49.7020037507, longi: 8.85692144117,
    name: "Reichelsheim (Odenwald)",
    plz: "64385"
  },
  {
    lati: 50.4939595743, longi: 10.630128016,
    name: "Themar",
    plz: "98660"
  },
  {
    lati: 50.6455184519, longi: 12.9599863271,
    name: "Ehrenfriedersdorf",
    plz: "09427"
  },
  {
    lati: 53.8675450179, longi: 9.52847368058,
    name: "Dägeling, Neuenbrook",
    plz: "25578"
  },
  {
    lati: 50.4311466868, longi: 9.0234203288,
    name: "Nidda",
    plz: "63667"
  },
  {
    lati: 51.2172758686, longi: 7.6494553423,
    name: "Lüdenscheid",
    plz: "58511"
  },
  {
    lati: 50.3618572299, longi: 7.77914250238,
    name: "Singhofen",
    plz: "56379"
  },
  {
    lati: 49.9106029234, longi: 8.33327407894,
    name: "Nackenheim",
    plz: "55299"
  },
  {
    lati: 50.5015260859, longi: 8.38818853233,
    name: "Braunfels",
    plz: "35619"
  },
  {
    lati: 50.0363278838, longi: 6.79874747105,
    name: "Großlittgen",
    plz: "54534"
  },
  {
    lati: 54.027476045, longi: 12.4985197973,
    name: "Tessin, Grammow u.a.",
    plz: "18195"
  },
  {
    lati: 51.3743749469, longi: 12.3684301146,
    name: "Leipzig",
    plz: "04157"
  },
  {
    lati: 51.3321200669, longi: 12.3903492663,
    name: "Leipzig",
    plz: "04103"
  },
  {
    lati: 53.2563921156, longi: 9.98596661638,
    name: "Hanstedt, Asendorf",
    plz: "21271"
  },
  {
    lati: 52.8252477117, longi: 10.097011072,
    name: "Hermannsburg",
    plz: "29320"
  },
  {
    lati: 48.1804254202, longi: 11.232804933,
    name: "Fürstenfeldbruck",
    plz: "82256"
  },
  {
    lati: 50.9903825734, longi: 13.2816332029,
    name: "Großschirma",
    plz: "09603"
  },
  {
    lati: 48.5753399007, longi: 13.378781544,
    name: "Passau",
    plz: "94036"
  },
  {
    lati: 48.907088349, longi: 13.3837902512,
    name: "Riedlhütte",
    plz: "94566"
  },
  {
    lati: 52.4926276508, longi: 13.3974671705,
    name: "Berlin Kreuzberg",
    plz: "10961"
  },
  {
    lati: 49.3039131284, longi: 7.43409396027,
    name: "Kleinbundenbach",
    plz: "66501"
  },
  {
    lati: 51.4553171175, longi: 7.46132330491,
    name: "Dortmund",
    plz: "44229"
  },
  {
    lati: 48.0384410754, longi: 8.66877078428,
    name: "Durchhausen",
    plz: "78591"
  },
  {
    lati: 49.6446535348, longi: 8.91583487374,
    name: "Mossautal",
    plz: "64756"
  },
  {
    lati: 48.147342342, longi: 9.69006815538,
    name: "Attenweiler",
    plz: "88448"
  },
  {
    lati: 48.1141612048, longi: 10.3100859155,
    name: "Oberschönegg",
    plz: "87770"
  },
  {
    lati: 48.2638036702, longi: 10.3266902608,
    name: "Deisenhausen",
    plz: "86489"
  },
  {
    lati: 50.7283566767, longi: 9.00958108137,
    name: "Homberg (Ohm)",
    plz: "35315"
  },
  {
    lati: 49.4376012417, longi: 9.32424064081,
    name: "Seckach",
    plz: "74743"
  },
  {
    lati: 49.2230969853, longi: 11.4480720519,
    name: "Sengenthal",
    plz: "92369"
  },
  {
    lati: 51.4278903544, longi: 6.88614734166,
    name: "Mülheim an der Ruhr",
    plz: "45468"
  },
  {
    lati: 47.9278832482, longi: 9.4272717474,
    name: "Königseggwald",
    plz: "88376"
  },
  {
    lati: 47.8810612884, longi: 9.47902764509,
    name: "Fleischwangen",
    plz: "88373"
  },
  {
    lati: 47.7682187483, longi: 12.7682495079,
    name: "Inzell",
    plz: "83334"
  },
  {
    lati: 50.9230371657, longi: 11.6125789147,
    name: "Jena",
    plz: "07749"
  },
  {
    lati: 52.0917989604, longi: 11.6060202722,
    name: "Magdeburg",
    plz: "39118"
  },
  {
    lati: 47.7081779949, longi: 10.8673653143,
    name: "Steingaden",
    plz: "86989"
  },
  {
    lati: 48.6613726547, longi: 10.3985541689,
    name: "Ziertheim",
    plz: "89446"
  },
  {
    lati: 50.158176761, longi: 10.4572812295,
    name: "Aidhausen",
    plz: "97491"
  },
  {
    lati: 51.5659938593, longi: 7.46686043808,
    name: "Dortmund",
    plz: "44339"
  },
  {
    lati: 52.5371994519, longi: 13.5368381266,
    name: "Berlin",
    plz: "12681"
  },
  {
    lati: 48.6358807938, longi: 8.15531103112,
    name: "Lauf",
    plz: "77886"
  },
  {
    lati: 48.4332106304, longi: 13.2683108305,
    name: "Tettenweis",
    plz: "94167"
  },
  {
    lati: 51.0806053874, longi: 13.688444034,
    name: "Dresden",
    plz: "01139"
  },
  {
    lati: 51.2017442544, longi: 7.08175349435,
    name: "Solingen",
    plz: "42653"
  },
  {
    lati: 50.3717848982, longi: 7.93566522135,
    name: "Burgschwalbach",
    plz: "65558"
  },
  {
    lati: 48.4756525157, longi: 7.9829011113,
    name: "Offenburg",
    plz: "77654"
  },
  {
    lati: 52.0886417961, longi: 10.2800836515,
    name: "Elbe",
    plz: "38274"
  },
  {
    lati: 50.9047978471, longi: 11.5624221451,
    name: "Jena",
    plz: "07745"
  },
  {
    lati: 49.9429567504, longi: 11.5770216465,
    name: "Bayreuth",
    plz: "95444"
  },
  {
    lati: 54.1302237562, longi: 12.1259014046,
    name: "Rostock",
    plz: "18147"
  },
  {
    lati: 49.0257472927, longi: 10.8035677209,
    name: "Meinheim",
    plz: "91802"
  },
  {
    lati: 52.2259701131, longi: 9.12688120624,
    name: "Buchholz",
    plz: "31710"
  },
  {
    lati: 49.7189196278, longi: 9.18382597012,
    name: "Kleinheubach, Rüdenau",
    plz: "63924"
  },
  {
    lati: 50.2048087702, longi: 6.87130277098,
    name: "Mehren u.a.",
    plz: "54552"
  },
  {
    lati: 49.7392134808, longi: 7.92974231992,
    name: "Steinbach, Weitersweiler, Bennhausen, Mörsfeld, Würzweiler, Ruppertsecke",
    plz: "67808"
  },
  {
    lati: 47.5832749905, longi: 8.02325677724,
    name: "Murg",
    plz: "79730"
  },
  {
    lati: 50.1533842605, longi: 7.58258912329,
    name: "Emmelshausen",
    plz: "56281"
  },
  {
    lati: 48.5447862612, longi: 10.8335755004,
    name: "Meitingen",
    plz: "86405"
  },
  {
    lati: 54.4054065779, longi: 10.0561820219,
    name: "Felm",
    plz: "24244"
  },
  {
    lati: 49.8980095091, longi: 10.0867444815,
    name: "Bergtheim, Oberpleichfeld",
    plz: "97241"
  },
  {
    lati: 54.5449280852, longi: 13.1205498741,
    name: "Hiddensee",
    plz: "18565"
  },
  {
    lati: 54.11975745, longi: 13.7665302053,
    name: "Kröslin, Krummin, Lassan u.a.",
    plz: "17440"
  },
  {
    lati: 52.1919056461, longi: 11.6611131372,
    name: "Magdeburg",
    plz: "39126"
  },
  {
    lati: 52.1957916707, longi: 11.9897446691,
    name: "Möckern, Schermen, Nedlitz etc",
    plz: "39291"
  },
  {
    lati: 48.1072693193, longi: 12.1143512567,
    name: "Albaching",
    plz: "83544"
  },
  {
    lati: 51.3193435823, longi: 12.2947494556,
    name: "Leipzig",
    plz: "04209"
  },
  {
    lati: 49.6442708245, longi: 10.7164895359,
    name: "Dachsbach",
    plz: "91462"
  },
  {
    lati: 49.8575753963, longi: 10.4200853299,
    name: "Oberschwarzach",
    plz: "97516"
  },
  {
    lati: 52.3467434686, longi: 10.4472635062,
    name: "Schwülper",
    plz: "38179"
  },
  {
    lati: 51.0668457274, longi: 13.7413104526,
    name: "Dresden",
    plz: "01097"
  },
  {
    lati: 51.7372705426, longi: 14.3051520995,
    name: "Cottbus",
    plz: "03048"
  },
  {
    lati: 50.3412861765, longi: 11.5751006938,
    name: "Geroldsgrün",
    plz: "95179"
  },
  {
    lati: 51.9439257856, longi: 8.5885677727,
    name: "Bielefeld",
    plz: "33689"
  },
  {
    lati: 52.7896710846, longi: 8.08778464615,
    name: "Cappeln (Oldenburg)",
    plz: "49692"
  },
  {
    lati: 48.3435848107, longi: 8.0828485169,
    name: "Zell am Harmersbach",
    plz: "77736"
  },
  {
    lati: 50.4352869342, longi: 8.08065856752,
    name: "Limburg",
    plz: "65554"
  },
  {
    lati: 51.2821157648, longi: 9.46498177256,
    name: "Kassel",
    plz: "34134"
  },
  {
    lati: 50.1627114681, longi: 7.30510208303,
    name: "Treis-Karden",
    plz: "56253"
  },
  {
    lati: 51.4006061126, longi: 7.28244532744,
    name: "Witten",
    plz: "58456"
  },
  {
    lati: 48.3566300781, longi: 11.1282441095,
    name: "Adelzhausen",
    plz: "86559"
  },
  {
    lati: 48.1428156161, longi: 11.7498841769,
    name: "Feldkirchen",
    plz: "85622"
  },
  {
    lati: 49.0733954055, longi: 11.9026016492,
    name: "Laaber, Brunn",
    plz: "93164"
  },
  {
    lati: 48.7348682159, longi: 9.35968422907,
    name: "Esslingen am Neckar",
    plz: "73730"
  },
  {
    lati: 50.79748965, longi: 12.6235980949,
    name: "St. Egidien",
    plz: "09356"
  },
  {
    lati: 50.9388744382, longi: 12.8799195812,
    name: "Claußnitz",
    plz: "09236"
  },
  {
    lati: 52.4977780607, longi: 13.4909600623,
    name: "Berlin Rummelsburg",
    plz: "10317"
  },
  {
    lati: 52.4320276564, longi: 9.73579901663,
    name: "Langenhagen",
    plz: "30851"
  },
  {
    lati: 50.164826562, longi: 6.70088209509,
    name: "Pelm, Neroth u.a.",
    plz: "54570"
  },
  {
    lati: 50.36574566, longi: 6.60129157345,
    name: "Esch",
    plz: "54585"
  },
  {
    lati: 49.1678026067, longi: 9.73083171535,
    name: "Untermünkheim",
    plz: "74547"
  },
  {
    lati: 49.7855538885, longi: 9.80067335019,
    name: "Waldbüttelbrunn",
    plz: "97297"
  },
  {
    lati: 53.9569682117, longi: 9.90557661165,
    name: "Wiemersdorf",
    plz: "24649"
  },
  {
    lati: 48.0198351564, longi: 8.2948119221,
    name: "Vöhrenbach",
    plz: "78147"
  },
  {
    lati: 48.1511454902, longi: 8.42123968804,
    name: "Königsfeld im Schwarzwald",
    plz: "78126"
  },
  {
    lati: 50.1380444613, longi: 11.1692204369,
    name: "Hochstadt a. Main",
    plz: "96272"
  },
  {
    lati: 51.0418900187, longi: 11.1467193232,
    name: "Udestedt, Mönchenholzhausen u.a.",
    plz: "99198"
  },
  {
    lati: 50.9220866361, longi: 12.8042815492,
    name: "Burgstädt",
    plz: "09217"
  },
  {
    lati: 48.2587741903, longi: 11.9766927785,
    name: "Walpertskirchen",
    plz: "85469"
  },
  {
    lati: 49.9588836561, longi: 10.6686416005,
    name: "Eltmann",
    plz: "97483"
  },
  {
    lati: 52.272184726, longi: 10.8114644912,
    name: "Königslutter am Elm",
    plz: "38154"
  },
  {
    lati: 52.5803025528, longi: 10.8744033048,
    name: "Tülau",
    plz: "38474"
  },
  {
    lati: 49.6465391512, longi: 6.46279496151,
    name: "Nittel",
    plz: "54453"
  },
  {
    lati: 51.4018434554, longi: 6.772433091,
    name: "Duisburg",
    plz: "47055"
  },
  {
    lati: 53.5287778774, longi: 8.07006368465,
    name: "Wilhelmshaven",
    plz: "26389"
  },
  {
    lati: 49.4212276622, longi: 7.79598750829,
    name: "Kaiserslautern",
    plz: "67663"
  },
  {
    lati: 52.8426704944, longi: 9.13366406868,
    name: "Hoya, Hoyerhagen, Hilgermissen",
    plz: "27318"
  },
  {
    lati: 49.8685955859, longi: 9.18972824847,
    name: "Kleinwallstadt",
    plz: "63839"
  },
  {
    lati: 49.8305734143, longi: 9.28865532921,
    name: "Eschau",
    plz: "63863"
  },
  {
    lati: 54.0567621624, longi: 9.2627265216,
    name: "Eggstedt",
    plz: "25721"
  },
  {
    lati: 48.379184407, longi: 10.1106416568,
    name: "Holzheim",
    plz: "89291"
  },
  {
    lati: 53.6379370479, longi: 9.45965478431,
    name: "Stade",
    plz: "21683"
  },
  {
    lati: 51.4651547815, longi: 6.9245699563,
    name: "Essen",
    plz: "45359"
  },
  {
    lati: 53.4471421243, longi: 7.09463964306,
    name: "Krummhörn",
    plz: "26736"
  },
  {
    lati: 51.2481132944, longi: 7.22353554267,
    name: "Wuppertal",
    plz: "42287"
  },
  {
    lati: 51.9220373444, longi: 7.36173985708,
    name: "Nottuln",
    plz: "48301"
  },
  {
    lati: 50.772964078, longi: 13.07272858,
    name: "Börnichen, Gornau",
    plz: "09437"
  },
  {
    lati: 50.9848892896, longi: 14.01489089,
    name: "Lohmen",
    plz: "01847"
  },
  {
    lati: 51.8314804545, longi: 14.3512922045,
    name: "Cottbus",
    plz: "03054"
  },
  {
    lati: 51.4777019747, longi: 7.44993627273,
    name: "Dortmund",
    plz: "44225"
  },
  {
    lati: 49.7640209547, longi: 8.3230922586,
    name: "Alsheim",
    plz: "67577"
  },
  {
    lati: 47.9471126193, longi: 11.4235553417,
    name: "Icking",
    plz: "82057"
  },
  {
    lati: 48.3564312302, longi: 11.6395958919,
    name: "Neufahrn b. Freising",
    plz: "85376"
  },
  {
    lati: 51.705297748, longi: 7.37991083808,
    name: "Olfen",
    plz: "59399"
  },
  {
    lati: 49.847711202, longi: 7.406467885,
    name: "Bergen",
    plz: "55608"
  },
  {
    lati: 49.0296411027, longi: 11.9018666377,
    name: "Deuerling",
    plz: "93180"
  },
  {
    lati: 49.6585254461, longi: 11.9338997336,
    name: "Kaltenbrunn",
    plz: "92700"
  },
  {
    lati: 48.1044910386, longi: 12.2147296428,
    name: "Soyen",
    plz: "83564"
  },
  {
    lati: 54.2431408575, longi: 12.4291069774,
    name: "Ribnitz-Damgarten",
    plz: "18311"
  },
  {
    lati: 48.1847808126, longi: 12.7909315856,
    name: "Mehring",
    plz: "84561"
  },
  {
    lati: 48.678603094, longi: 13.1670426406,
    name: "Hofkirchen",
    plz: "94544"
  },
  {
    lati: 53.3870026429, longi: 9.97311924527,
    name: "Seevetal",
    plz: "21218"
  },
  {
    lati: 47.9878688292, longi: 10.1310942975,
    name: "Buxheim",
    plz: "87740"
  },
  {
    lati: 48.0572456153, longi: 10.2005928754,
    name: "Niederrieden",
    plz: "87767"
  },
  {
    lati: 50.0815811703, longi: 7.69744267118,
    name: "Oberwesel",
    plz: "55430"
  },
  {
    lati: 53.2541501058, longi: 8.10559049545,
    name: "Wiefelstede",
    plz: "26215"
  },
  {
    lati: 48.9649615963, longi: 8.3156969586,
    name: "Rheinstetten",
    plz: "76287"
  },
  {
    lati: 53.8531405479, longi: 8.71008261282,
    name: "Cuxhaven",
    plz: "27472"
  },
  {
    lati: 52.5477287418, longi: 13.2049079246,
    name: "Berlin Spandau",
    plz: "13585"
  },
  {
    lati: 48.6533205102, longi: 8.45477732396,
    name: "Enzklösterle",
    plz: "75337"
  },
  {
    lati: 51.2037012851, longi: 6.74587825299,
    name: "Düsseldorf",
    plz: "40221"
  },
  {
    lati: 49.5243649618, longi: 10.5015444424,
    name: "Ipsheim",
    plz: "91472"
  },
  {
    lati: 47.6344245563, longi: 7.62141161257,
    name: "Binzen",
    plz: "79589"
  },
  {
    lati: 49.351224837, longi: 7.91143119852,
    name: "Elmstein",
    plz: "67471"
  },
  {
    lati: 53.6066922737, longi: 8.65092945223,
    name: "Bremerhaven",
    plz: "27578"
  },
  {
    lati: 48.8743379729, longi: 8.81220753345,
    name: "Wurmberg",
    plz: "75449"
  },
  {
    lati: 53.6109576438, longi: 10.0552515739,
    name: "Hamburg",
    plz: "22309"
  },
  {
    lati: 51.2329399468, longi: 6.71658842637,
    name: "Düsseldorf",
    plz: "40549"
  },
  {
    lati: 51.299451589, longi: 7.23742190351,
    name: "Wuppertal",
    plz: "42279"
  },
  {
    lati: 51.4957135533, longi: 9.49589123971,
    name: "Hofgeismar",
    plz: "34369"
  },
  {
    lati: 53.6325875782, longi: 10.2834260214,
    name: "Siek",
    plz: "22962"
  },
  {
    lati: 52.2578573988, longi: 10.5368220048,
    name: "Braunschweig",
    plz: "38102"
  },
  {
    lati: 48.4570407447, longi: 9.10161957887,
    name: "Gomaringen",
    plz: "72810"
  },
  {
    lati: 48.5029601977, longi: 7.94229675561,
    name: "Offenburg",
    plz: "77652"
  },
  {
    lati: 52.3431707843, longi: 13.6960417048,
    name: "Königs Wusterhausen",
    plz: "15713"
  },
  {
    lati: 51.5213101297, longi: 6.9642229819,
    name: "Bottrop",
    plz: "46238"
  },
  {
    lati: 52.0088165445, longi: 8.68732350175,
    name: "Leopoldshöhe",
    plz: "33818"
  },
  {
    lati: 50.1197827929, longi: 8.69704354911,
    name: "Frankfurt am Main",
    plz: "60316"
  },
  {
    lati: 47.8861930624, longi: 8.91025931433,
    name: "Eigeltingen",
    plz: "78253"
  },
  {
    lati: 49.9463849761, longi: 10.7987540704,
    name: "Oberhaid",
    plz: "96173"
  },
  {
    lati: 53.6918601251, longi: 11.1066745999,
    name: "Gadebusch",
    plz: "19205"
  },
  {
    lati: 53.3464344362, longi: 11.9442220706,
    name: "Siggelkow",
    plz: "19376"
  },
  {
    lati: 49.9602521488, longi: 11.9023277936,
    name: "Brand",
    plz: "95682"
  },
  {
    lati: 49.2650493821, longi: 12.8179109135,
    name: "Arnschwang",
    plz: "93473"
  },
  {
    lati: 51.3386275455, longi: 13.1222556391,
    name: "Oschatz",
    plz: "04758"
  },
  {
    lati: 49.2341993108, longi: 8.28853131584,
    name: "Lustadt",
    plz: "67363"
  },
  {
    lati: 50.1149628695, longi: 6.96434491108,
    name: "Mehren u.a.",
    plz: "54552"
  },
  {
    lati: 49.9412465464, longi: 6.83073921858,
    name: "Binsfeld, Heckenmünster",
    plz: "54518"
  },
  {
    lati: 51.4322290416, longi: 11.601943029,
    name: "Schraplau, Farnstädt",
    plz: "06279"
  },
  {
    lati: 47.7146339627, longi: 11.7640529988,
    name: "Tegernsee",
    plz: "83684"
  },
  {
    lati: 48.2326775003, longi: 10.5398018589,
    name: "Aichen",
    plz: "86479"
  },
  {
    lati: 50.0853854161, longi: 8.97620761032,
    name: "Großkrotzenburg",
    plz: "63538"
  },
  {
    lati: 54.6492081281, longi: 9.06512412197,
    name: "Högel",
    plz: "25858"
  },
  {
    lati: 48.5780182481, longi: 11.3359861372,
    name: "Waidhofen",
    plz: "86579"
  },
  {
    lati: 49.8026369089, longi: 12.2957591534,
    name: "Plößberg",
    plz: "95703"
  },
  {
    lati: 52.4921206046, longi: 13.3394552372,
    name: "Berlin Schöneberg",
    plz: "10779"
  },
  {
    lati: 52.6343964574, longi: 9.1045003991,
    name: "Binnen",
    plz: "31619"
  },
  {
    lati: 48.7773376984, longi: 9.16166195172,
    name: "Stuttgart",
    plz: "70176"
  },
  {
    lati: 48.525983109, longi: 12.465211739,
    name: "Aham",
    plz: "84168"
  },
  {
    lati: 51.3013393827, longi: 12.9641575845,
    name: "Wermsdorf",
    plz: "04779"
  },
  {
    lati: 52.4965700517, longi: 13.3137336057,
    name: "Berlin Wilmersdorf",
    plz: "10707"
  },
  {
    lati: 51.445662779, longi: 9.41897623471,
    name: "Grebenstein",
    plz: "34393"
  },
  {
    lati: 51.7671011639, longi: 8.31919685634,
    name: "Langenberg",
    plz: "33449"
  },
  {
    lati: 48.8841359215, longi: 8.34716831038,
    name: "Malsch",
    plz: "76316"
  },
  {
    lati: 50.69528091, longi: 7.87835911786,
    name: "Nauroth",
    plz: "57583"
  },
  {
    lati: 52.195889329, longi: 7.96328698065,
    name: "Hagen am Teutoburger Wald",
    plz: "49170"
  },
  {
    lati: 52.4874519208, longi: 8.02096917309,
    name: "Rieste",
    plz: "49597"
  },
  {
    lati: 49.3513953068, longi: 7.46384222664,
    name: "Bechhofen",
    plz: "66894"
  },
  {
    lati: 50.38626161, longi: 7.71002912396,
    name: "Neuhäusel",
    plz: "56335"
  },
  {
    lati: 48.560892536, longi: 8.56295984185,
    name: "Wörnersberg",
    plz: "72299"
  },
  {
    lati: 49.2103604307, longi: 7.97052578732,
    name: "Annweiler am Trifels",
    plz: "76855"
  },
  {
    lati: 49.2042069953, longi: 8.02405555514,
    name: "Billigheim-Ingenheim, Birkweiler",
    plz: "76831"
  },
  {
    lati: 50.3697074344, longi: 7.45447426855,
    name: "Urmitz",
    plz: "56220"
  },
  {
    lati: 49.5984767607, longi: 10.625618363,
    name: "Diespeck",
    plz: "91456"
  },
  {
    lati: 48.0351120001, longi: 10.6719808198,
    name: "Wiedergeltingen",
    plz: "86879"
  },
  {
    lati: 52.4434678978, longi: 10.5833136315,
    name: "Isenbüttel",
    plz: "38550"
  },
  {
    lati: 51.248663817, longi: 11.0948314216,
    name: "Kindelbrück",
    plz: "99638"
  },
  {
    lati: 51.3196043403, longi: 12.3720270432,
    name: "Leipzig",
    plz: "04275"
  },
  {
    lati: 47.8743102263, longi: 11.3627327383,
    name: "Münsing",
    plz: "82541"
  },
  {
    lati: 48.8338348481, longi: 11.4111628377,
    name: "Wettstetten",
    plz: "85139"
  },
  {
    lati: 51.0466099308, longi: 14.4302486845,
    name: "Sohland a. d. Spree",
    plz: "02689"
  },
  {
    lati: 50.8930032868, longi: 6.47502183863,
    name: "Niederzier",
    plz: "52382"
  },
  {
    lati: 49.6654462867, longi: 7.82757211982,
    name: "Dielkirchen",
    plz: "67811"
  },
  {
    lati: 49.0854914995, longi: 7.984366846,
    name: "Klingenmünster u.a.",
    plz: "76889"
  },
  {
    lati: 50.0678522219, longi: 8.09993311201,
    name: "Eltville am Rhein",
    plz: "65345"
  },
  {
    lati: 49.8455195783, longi: 7.80604737504,
    name: "Rüdesheim",
    plz: "55593"
  },
  {
    lati: 50.753267471, longi: 8.69753037471,
    name: "Weimar (Lahn)",
    plz: "35096"
  },
  {
    lati: 53.624256938, longi: 9.69449526059,
    name: "Holm",
    plz: "25488"
  },
  {
    lati: 53.5054386574, longi: 10.6511551076,
    name: "Bröthen",
    plz: "21514"
  },
  {
    lati: 47.942846157, longi: 10.6800144829,
    name: "Germaringen",
    plz: "87656"
  },
  {
    lati: 52.5040641814, longi: 10.7543986319,
    name: "Jembke",
    plz: "38477"
  },
  {
    lati: 51.3621753026, longi: 10.4345868973,
    name: "Niederorschel u.a.",
    plz: "37355"
  },
  {
    lati: 48.728874038, longi: 13.3965224408,
    name: "Tittling",
    plz: "94104"
  },
  {
    lati: 51.1762490465, longi: 7.10222495835,
    name: "Solingen",
    plz: "42651"
  },
  {
    lati: 52.0978096966, longi: 6.96297147481,
    name: "Ahaus",
    plz: "48683"
  },
  {
    lati: 51.232363251, longi: 11.6589421114,
    name: "Laucha an der Unstrut",
    plz: "06636"
  },
  {
    lati: 48.0167309474, longi: 7.62160029848,
    name: "Breisach am Rhein",
    plz: "79206"
  },
  {
    lati: 49.7396838878, longi: 7.79123348561,
    name: "Winterborn, Waldgrehweiler, Niedermoschel, u.a.",
    plz: "67822"
  },
  {
    lati: 48.1230108516, longi: 7.86526371712,
    name: "Emmendingen",
    plz: "79312"
  },
  {
    lati: 51.3105629554, longi: 8.00578314858,
    name: "Sundern",
    plz: "59846"
  },
  {
    lati: 53.3132474381, longi: 8.36146317722,
    name: "Ovelgönne",
    plz: "26939"
  },
  {
    lati: 48.7945290761, longi: 8.64705436603,
    name: "Schömberg",
    plz: "75328"
  },
  {
    lati: 53.6732368729, longi: 10.3430118475,
    name: "Hoisdorf",
    plz: "22961"
  },
  {
    lati: 50.6378056743, longi: 10.7356027405,
    name: "Suhl",
    plz: "98528"
  },
  {
    lati: 47.8796351191, longi: 12.6439927239,
    name: "Traunstein",
    plz: "83278"
  },
  {
    lati: 53.1478369018, longi: 13.5878163336,
    name: "Templin, Boitzenburg u.a.",
    plz: "17268"
  },
  {
    lati: 47.8706941783, longi: 11.184931848,
    name: "Wielenbach",
    plz: "82407"
  },
  {
    lati: 50.1217242659, longi: 10.2839084409,
    name: "Üchtelhausen",
    plz: "97532"
  },
  {
    lati: 52.5325138406, longi: 10.6473823988,
    name: "Sassenburg",
    plz: "38524"
  },
  {
    lati: 53.0057964888, longi: 11.2891802133,
    name: "Trebel",
    plz: "29494"
  },
  {
    lati: 47.5417013839, longi: 11.2759407456,
    name: "Wallgau",
    plz: "82499"
  },
  {
    lati: 52.5453258528, longi: 13.5752531709,
    name: "Berlin",
    plz: "12679"
  },
  {
    lati: 53.1176606892, longi: 7.99336725769,
    name: "Edewecht",
    plz: "26188"
  },
  {
    lati: 49.8172980437, longi: 7.97341043844,
    name: "Wöllstein",
    plz: "55597"
  },
  {
    lati: 49.7872787069, longi: 8.04658568106,
    name: "Flonheim",
    plz: "55237"
  },
  {
    lati: 50.3227120711, longi: 8.90294106813,
    name: "Florstadt",
    plz: "61197"
  },
  {
    lati: 48.6195811535, longi: 9.34054323678,
    name: "Nürtingen",
    plz: "72622"
  },
  {
    lati: 48.1279493282, longi: 11.5714425954,
    name: "München",
    plz: "80469"
  },
  {
    lati: 51.8807216433, longi: 12.6147274279,
    name: "Wittenberg",
    plz: "06886"
  },
  {
    lati: 47.9523616678, longi: 12.2674652545,
    name: "Halfing",
    plz: "83128"
  },
  {
    lati: 50.7677785157, longi: 13.5349563658,
    name: "Frauenstein",
    plz: "09623"
  },
  {
    lati: 47.6590379372, longi: 8.05435184274,
    name: "Görwihl",
    plz: "79733"
  },
  {
    lati: 50.0043472442, longi: 7.79730841729,
    name: "Weiler bei Bingen",
    plz: "55413"
  },
  {
    lati: 48.5300746079, longi: 8.64543371536,
    name: "Haiterbach",
    plz: "72221"
  },
  {
    lati: 50.6660271768, longi: 9.75218810048,
    name: "Hünfeld",
    plz: "36088"
  },
  {
    lati: 54.5714232024, longi: 9.69095546615,
    name: "Taarstedt",
    plz: "24893"
  },
  {
    lati: 50.5389374342, longi: 6.27593204323,
    name: "Monschau",
    plz: "52156"
  },
  {
    lati: 50.0150072936, longi: 9.21486101236,
    name: "Hösbach",
    plz: "63768"
  },
  {
    lati: 48.4384357748, longi: 11.4075376774,
    name: "Jetzendorf",
    plz: "85305"
  },
  {
    lati: 50.0178137904, longi: 10.7154823246,
    name: "Breitbrunn",
    plz: "96151"
  },
  {
    lati: 48.3798689189, longi: 10.9389937277,
    name: "Augsburg",
    plz: "86165"
  },
  {
    lati: 50.6247693364, longi: 11.9975159985,
    name: "Zeulenroda-Triebes, Langenwolschendorf",
    plz: "07937"
  },
  {
    lati: 47.8073698542, longi: 12.1860773478,
    name: "Rohrdorf",
    plz: "83101"
  },
  {
    lati: 51.0653399339, longi: 10.2183891759,
    name: "Creuzburg, Ifta",
    plz: "99831"
  },
  {
    lati: 53.7016539919, longi: 11.532652621,
    name: "Leezen",
    plz: "19067"
  },
  {
    lati: 49.7576681826, longi: 9.70514180453,
    name: "Helmstadt",
    plz: "97264"
  },
  {
    lati: 48.185354886, longi: 11.7154242548,
    name: "Aschheim",
    plz: "85609"
  },
  {
    lati: 48.4661717119, longi: 10.2802294518,
    name: "Günzburg",
    plz: "89312"
  },
  {
    lati: 49.4629892738, longi: 10.5794989962,
    name: "Trautskirchen",
    plz: "90619"
  },
  {
    lati: 53.5501304171, longi: 9.88687732118,
    name: "Hamburg",
    plz: "22605"
  },
  {
    lati: 48.874893033, longi: 9.35262550562,
    name: "Schwaikheim",
    plz: "71409"
  },
  {
    lati: 51.5675815012, longi: 9.98456280154,
    name: "Göttingen",
    plz: "37077"
  },
  {
    lati: 54.2231071839, longi: 10.0051235589,
    name: "Blumenthal",
    plz: "24241"
  },
  {
    lati: 54.409429511, longi: 8.98854515026,
    name: "Uelvesbüll, Witzwort",
    plz: "25889"
  },
  {
    lati: 48.716348782, longi: 9.1238441216,
    name: "Stuttgart",
    plz: "70565"
  },
  {
    lati: 47.9185201294, longi: 9.25802235335,
    name: "Pfullendorf",
    plz: "88630"
  },
  {
    lati: 52.3822085757, longi: 11.9902140286,
    name: "Elbe-Parey",
    plz: "39317"
  },
  {
    lati: 54.1056128055, longi: 12.2746485222,
    name: "Roggentin, Broderstorf u.a.",
    plz: "18184"
  },
  {
    lati: 50.4570302912, longi: 10.9738423648,
    name: "Sachsenbrunn",
    plz: "98678"
  },
  {
    lati: 47.7136513127, longi: 9.75957994067,
    name: "Amtzell",
    plz: "88279"
  },
  {
    lati: 49.2571303035, longi: 9.85583250117,
    name: "Langenburg",
    plz: "74595"
  },
  {
    lati: 53.6853894943, longi: 9.85662497993,
    name: "Tangstedt",
    plz: "25499"
  },
  {
    lati: 48.5052000209, longi: 10.0216901019,
    name: "Bernstadt",
    plz: "89182"
  },
  {
    lati: 48.7365175716, longi: 12.8962375621,
    name: "Aholming",
    plz: "94527"
  },
  {
    lati: 51.9503795418, longi: 11.2238642679,
    name: "Schwanebeck, Gröningen, Kroppenstedt",
    plz: "39397"
  },
  {
    lati: 50.638933499, longi: 11.2746554992,
    name: "Bad Blankenburg, Saalfelder Höhe",
    plz: "07422"
  },
  {
    lati: 49.2977871181, longi: 8.50292400205,
    name: "Altlußheim",
    plz: "68804"
  },
  {
    lati: 48.8175354705, longi: 8.34472073162,
    name: "Gaggenau",
    plz: "76571"
  },
  {
    lati: 48.975007918, longi: 9.27809563929,
    name: "Steinheim, Murr",
    plz: "71711"
  },
  {
    lati: 50.8579488748, longi: 7.86459309827,
    name: "Niederfischbach",
    plz: "57572"
  },
  {
    lati: 47.7738043145, longi: 8.24995550307,
    name: "Grafenhausen",
    plz: "79865"
  },
  {
    lati: 50.7532879075, longi: 8.33033666276,
    name: "Dillenburg",
    plz: "35689"
  },
  {
    lati: 50.995219681, longi: 8.25869833831,
    name: "Erndtebrück",
    plz: "57339"
  },
  {
    lati: 49.8358723715, longi: 6.64359109986,
    name: "Kordel",
    plz: "54306"
  },
  {
    lati: 51.5277466915, longi: 7.2141786385,
    name: "Herne",
    plz: "44625"
  },
  {
    lati: 52.6083821093, longi: 9.088160713,
    name: "Liebenau",
    plz: "31618"
  },
  {
    lati: 52.4190873713, longi: 9.23391948276,
    name: "Wölpinghausen",
    plz: "31556"
  },
  {
    lati: 48.7502889928, longi: 9.44417210053,
    name: "Baltmannsweiler",
    plz: "73666"
  },
  {
    lati: 54.0567544807, longi: 12.031533584,
    name: "Stäbelow, Kritzmow",
    plz: "18198"
  },
  {
    lati: 48.1102093425, longi: 11.493310889,
    name: "München",
    plz: "81377"
  },
  {
    lati: 50.2883131964, longi: 11.6119103232,
    name: "Schwarzenbach a. Wald",
    plz: "95131"
  },
  {
    lati: 50.0393512213, longi: 10.2626390104,
    name: "Sennfeld",
    plz: "97526"
  },
  {
    lati: 49.3973795092, longi: 7.49640381,
    name: "Mittelbrunn, Queidersbach u.a.",
    plz: "66851"
  },
  {
    lati: 53.8985788662, longi: 14.1360832456,
    name: "Seebad Ahlbeck",
    plz: "17419"
  },
  {
    lati: 54.2188605889, longi: 9.93436915608,
    name: "Langwedel",
    plz: "24631"
  },
  {
    lati: 50.9088551436, longi: 10.2339991672,
    name: "Marksuhl, Krauthausen u.a.",
    plz: "99819"
  },
  {
    lati: 49.3127396909, longi: 10.1625879439,
    name: "Insingen",
    plz: "91610"
  },
  {
    lati: 47.7439469902, longi: 10.2145438432,
    name: "Wiggensbach",
    plz: "87487"
  },
  {
    lati: 50.2764282646, longi: 9.80316619757,
    name: "Oberleichtersbach",
    plz: "97789"
  },
  {
    lati: 49.2008997489, longi: 9.85184442965,
    name: "Ilshofen",
    plz: "74532"
  },
  {
    lati: 52.4974634363, longi: 13.3427406268,
    name: "Berlin Wilmersdorf",
    plz: "10777"
  },
  {
    lati: 53.5290030978, longi: 13.5309891574,
    name: "Groß Miltzow",
    plz: "17349"
  },
  {
    lati: 51.0643546321, longi: 13.6676836217,
    name: "Dresden",
    plz: "01157"
  },
  {
    lati: 50.9937053714, longi: 7.15716632936,
    name: "Bergisch Gladbach",
    plz: "51465"
  },
  {
    lati: 48.2359331335, longi: 8.32285921465,
    name: "Lauterbach",
    plz: "78730"
  },
  {
    lati: 50.0238490119, longi: 8.36848953976,
    name: "Hochheim am Main",
    plz: "65239"
  },
  {
    lati: 50.6913047072, longi: 7.76394203099,
    name: "Hachenburg",
    plz: "57627"
  },
  {
    lati: 49.7881496674, longi: 8.81899584094,
    name: "Groß-Bieberau",
    plz: "64401"
  },
  {
    lati: 51.1186943776, longi: 10.1266864006,
    name: "Weißenborn",
    plz: "37299"
  },
  {
    lati: 48.1680503523, longi: 10.130407359,
    name: "Altenstadt",
    plz: "89281"
  },
  {
    lati: 53.3090158086, longi: 10.2689608682,
    name: "Radbruch",
    plz: "21449"
  },
  {
    lati: 50.0471605343, longi: 11.671853675,
    name: "Bad Berneck im Fichtelgebirge",
    plz: "95460"
  },
  {
    lati: 48.9203813525, longi: 11.303472899,
    name: "Walting",
    plz: "85137"
  },
  {
    lati: 49.2400282962, longi: 9.21792299811,
    name: "Bad Friedrichshall",
    plz: "74177"
  },
  {
    lati: 51.2048375592, longi: 6.15424336859,
    name: "Niederkrüchten",
    plz: "41372"
  },
  {
    lati: 48.9377939257, longi: 10.6642772727,
    name: "Megesheim",
    plz: "86750"
  },
  {
    lati: 53.9895058334, longi: 10.0558824909,
    name: "Boostedt",
    plz: "24598"
  },
  {
    lati: 48.4478430988, longi: 10.4575156075,
    name: "Haldenwang",
    plz: "89356"
  },
  {
    lati: 52.4736790422, longi: 13.3369219871,
    name: "Berlin Friedenau",
    plz: "12159"
  },
  {
    lati: 51.5885730246, longi: 14.4383354634,
    name: "Spremberg, Tschernitz u.a.",
    plz: "03130"
  },
  {
    lati: 49.5008646445, longi: 12.2036887341,
    name: "Pfreimd",
    plz: "92536"
  },
  {
    lati: 49.6498244582, longi: 12.1765598,
    name: "Schirmitz",
    plz: "92718"
  },
  {
    lati: 49.165040171, longi: 10.7943914298,
    name: "Haundorf",
    plz: "91729"
  },
  {
    lati: 53.6958507267, longi: 10.3468556622,
    name: "Todendorf",
    plz: "22965"
  },
  {
    lati: 49.2400632432, longi: 10.396348567,
    name: "Aurach",
    plz: "91589"
  },
  {
    lati: 51.5064216046, longi: 13.00119961,
    name: "Torgau",
    plz: "04861"
  },
  {
    lati: 51.8021315187, longi: 8.42302229542,
    name: "Rietberg",
    plz: "33397"
  },
  {
    lati: 50.2493649278, longi: 8.083886449,
    name: "Aarbergen",
    plz: "65326"
  },
  {
    lati: 49.4219817416, longi: 6.74151791906,
    name: "Beckingen",
    plz: "66701"
  },
  {
    lati: 50.6117092812, longi: 7.32089851369,
    name: "Vettelschloß, Kretzhaus (Linz am Rhein)",
    plz: "53560"
  },
  {
    lati: 48.6561769579, longi: 8.9459544717,
    name: "Ehningen",
    plz: "71139"
  },
  {
    lati: 49.6005114918, longi: 12.0233504008,
    name: "Kohlberg",
    plz: "92702"
  },
  {
    lati: 50.6411849793, longi: 12.1729086972,
    name: "Greiz",
    plz: "07973"
  },
  {
    lati: 50.8365167802, longi: 12.2090607355,
    name: "Ronneburg, Braunichswalde, Großenstein u.a.",
    plz: "07580"
  },
  {
    lati: 50.8119475641, longi: 12.3581064512,
    name: "Crimmitschau",
    plz: "08451"
  },
  {
    lati: 53.6844042362, longi: 11.8914585627,
    name: "Sternberg",
    plz: "19406"
  },
  {
    lati: 49.5220159327, longi: 7.68415137976,
    name: "Hirschhorn",
    plz: "67732"
  },
  {
    lati: 51.6346895745, longi: 8.49218754058,
    name: "Geseke",
    plz: "59590"
  },
  {
    lati: 54.0927446553, longi: 11.6654008569,
    name: "Rerik, Bastorf, Biendorf",
    plz: "18230"
  },
  {
    lati: 47.6805203431, longi: 9.19330874678,
    name: "Konstanz",
    plz: "78464"
  },
  {
    lati: 50.7599455438, longi: 12.700243832,
    name: "Gersdorf",
    plz: "09355"
  },
  {
    lati: 54.6926865276, longi: 9.87122546514,
    name: "Stoltebüll",
    plz: "24409"
  },
  {
    lati: 50.7648178821, longi: 9.81468459677,
    name: "Eiterfeld",
    plz: "36132"
  },
  {
    lati: 48.7955935432, longi: 9.79949677057,
    name: "Stadt Schwäbisch Gmünd",
    plz: "73525"
  },
  {
    lati: 53.7335160478, longi: 9.90382868954,
    name: "Quickborn",
    plz: "25451"
  },
  {
    lati: 47.7861667584, longi: 12.0831364201,
    name: "Raubling",
    plz: "83064"
  },
  {
    lati: 49.6597082597, longi: 8.25279046634,
    name: "Offstein",
    plz: "67591"
  },
  {
    lati: 53.873537964, longi: 10.6153257977,
    name: "Lübeck",
    plz: "23556"
  },
  {
    lati: 52.5145883481, longi: 14.1286102837,
    name: "Müncheberg",
    plz: "15374"
  },
  {
    lati: 50.9620941472, longi: 14.7211695412,
    name: "Oderwitz",
    plz: "02791"
  },
  {
    lati: 47.8226550689, longi: 9.26413238019,
    name: "Frickingen",
    plz: "88699"
  },
  {
    lati: 52.6828576851, longi: 9.6312739905,
    name: "Schwarmstedt u.a.",
    plz: "29690"
  },
  {
    lati: 51.9698273896, longi: 11.6789330756,
    name: "Welsleben, Biere, Eickendorf, Eggersdorf, Großmühlingen etc",
    plz: "39221"
  },
  {
    lati: 49.9150748861, longi: 11.0393744137,
    name: "Litzendorf",
    plz: "96123"
  },
  {
    lati: 51.6519662679, longi: 9.80369814444,
    name: "Hardegsen",
    plz: "37181"
  },
  {
    lati: 49.9039323177, longi: 6.66662781935,
    name: "Welschbillig, Igel, Aach",
    plz: "54298"
  },
  {
    lati: 50.5916093762, longi: 7.35705246715,
    name: "Sankt Katharinen (Landkreis Neuwied)",
    plz: "53562"
  },
  {
    lati: 48.7988861547, longi: 9.18508537184,
    name: "Stuttgart",
    plz: "70191"
  },
  {
    lati: 48.2313018303, longi: 9.95049161285,
    name: "Burgrieden",
    plz: "88483"
  },
  {
    lati: 53.5966211336, longi: 9.95847095903,
    name: "Hamburg",
    plz: "22529"
  },
  {
    lati: 52.5304769232, longi: 13.4053278168,
    name: "Berlin Mitte",
    plz: "10119"
  },
  {
    lati: 49.7835896583, longi: 11.898396258,
    name: "Trabitz",
    plz: "92724"
  },
  {
    lati: 47.6676438805, longi: 12.5210123436,
    name: "Reit im Winkl",
    plz: "83242"
  },
  {
    lati: 50.3090391, longi: 12.3484900767,
    name: "Markneukirchen",
    plz: "08258"
  },
  {
    lati: 51.701663132, longi: 13.6357725198,
    name: "Sonnewalde",
    plz: "03249"
  },
  {
    lati: 48.083084775, longi: 11.1162939658,
    name: "Eching a. Ammersee",
    plz: "82279"
  },
  {
    lati: 49.1708280512, longi: 11.2290576162,
    name: "Hilpoltstein",
    plz: "91161"
  },
  {
    lati: 47.8127106601, longi: 10.8965094923,
    name: "Schongau",
    plz: "86956"
  },
  {
    lati: 50.3219110732, longi: 7.5127560836,
    name: "Winningen",
    plz: "56333"
  },
  {
    lati: 48.2969976992, longi: 11.6254324028,
    name: "Eching",
    plz: "85386"
  },
  {
    lati: 49.5708545259, longi: 6.6044094506,
    name: "Serrig",
    plz: "54455"
  },
  {
    lati: 49.0753637806, longi: 9.154453809,
    name: "Lauffen am Neckar",
    plz: "74348"
  },
  {
    lati: 50.701030578, longi: 9.20250135082,
    name: "Romrod",
    plz: "36329"
  },
  {
    lati: 48.9917177335, longi: 9.15202491586,
    name: "Besigheim",
    plz: "74354"
  },
  {
    lati: 52.9787848273, longi: 13.7617887196,
    name: "Joachimsthal u.a.",
    plz: "16247"
  },
  {
    lati: 50.5563343857, longi: 9.68438148397,
    name: "Fulda",
    plz: "36037"
  },
  {
    lati: 49.2226798133, longi: 10.5862466027,
    name: "Burgoberbach",
    plz: "91595"
  },
  {
    lati: 48.9957513917, longi: 10.7442426786,
    name: "Heidenheim",
    plz: "91719"
  },
  {
    lati: 52.5112124049, longi: 10.8170072974,
    name: "Tiddische",
    plz: "38473"
  },
  {
    lati: 49.0391218714, longi: 10.8687479544,
    name: "Alesheim",
    plz: "91793"
  },
  {
    lati: 49.9799796299, longi: 8.23426033416,
    name: "Mainz",
    plz: "55128"
  },
  {
    lati: 50.3535764687, longi: 8.27751701336,
    name: "Selters",
    plz: "65618"
  },
  {
    lati: 50.9801821833, longi: 8.83198266108,
    name: "Rosenthal",
    plz: "35119"
  },
  {
    lati: 48.2583742179, longi: 8.61867135269,
    name: "Epfendorf",
    plz: "78736"
  },
  {
    lati: 52.0978292933, longi: 8.15654094005,
    name: "Bad Rothenfelde",
    plz: "49214"
  },
  {
    lati: 50.7987677994, longi: 11.5501183244,
    name: "Kahla",
    plz: "07768"
  },
  {
    lati: 49.4465229032, longi: 6.62695068949,
    name: "Merzig",
    plz: "66663"
  },
  {
    lati: 49.2261134546, longi: 6.87185580274,
    name: "Saarbrücken",
    plz: "66127"
  },
  {
    lati: 49.2290234373, longi: 7.9453205381,
    name: "Albersweiler, Silz u.a.",
    plz: "76857"
  },
  {
    lati: 50.3599609334, longi: 8.17722667784,
    name: "Brechen",
    plz: "65611"
  },
  {
    lati: 47.7606924436, longi: 8.87034260152,
    name: "Singen",
    plz: "78224"
  },
  {
    lati: 50.6069209542, longi: 8.954028975,
    name: "Grünberg",
    plz: "35305"
  },
  {
    lati: 53.1809330401, longi: 9.01056543433,
    name: "Grasberg",
    plz: "28879"
  },
  {
    lati: 50.2640177075, longi: 8.9910395217,
    name: "Limeshain",
    plz: "63694"
  },
  {
    lati: 48.7815875063, longi: 9.18184901351,
    name: "Stuttgart",
    plz: "70173"
  },
  {
    lati: 47.8549135236, longi: 12.1292261643,
    name: "Rosenheim",
    plz: "83022"
  },
  {
    lati: 49.234537198, longi: 12.8856875367,
    name: "Rimbach",
    plz: "93485"
  },
  {
    lati: 49.1021255836, longi: 12.8215872858,
    name: "Prackenbach",
    plz: "94267"
  },
  {
    lati: 48.8941281259, longi: 13.1856078251,
    name: "Kirchberg",
    plz: "94259"
  },
  {
    lati: 51.1886032378, longi: 6.6718570079,
    name: "Neuss",
    plz: "41464"
  },
  {
    lati: 51.4200618077, longi: 6.6950490576,
    name: "Duisburg",
    plz: "47228"
  },
  {
    lati: 50.8688635571, longi: 7.1075194408,
    name: "Köln",
    plz: "51147"
  },
  {
    lati: 49.5877067123, longi: 8.03152839234,
    name: "Göllheim",
    plz: "67307"
  },
  {
    lati: 50.085558468, longi: 9.01313836808,
    name: "Kahl am Main",
    plz: "63796"
  },
  {
    lati: 49.3812699502, longi: 9.1339847966,
    name: "Mosbach",
    plz: "74821"
  },
  {
    lati: 50.5669374812, longi: 8.48604227999,
    name: "Wetzlar",
    plz: "35576"
  },
  {
    lati: 50.5900936905, longi: 8.56608529358,
    name: "Lahnau",
    plz: "35633"
  },
  {
    lati: 54.3023320623, longi: 9.65428618905,
    name: "Rendsburg",
    plz: "24768"
  },
  {
    lati: 51.9313552044, longi: 9.92752998905,
    name: "Freden (Leine)",
    plz: "31084"
  },
  {
    lati: 53.5753034054, longi: 9.94485923116,
    name: "Hamburg",
    plz: "20257"
  },
  {
    lati: 53.4686668967, longi: 10.4622639191,
    name: "Kollow",
    plz: "21527"
  },
  {
    lati: 52.0112342664, longi: 8.0460580698,
    name: "Sassenberg",
    plz: "48336"
  },
  {
    lati: 52.4776644589, longi: 13.2950793311,
    name: "Berlin Schmargendorf",
    plz: "14199"
  },
  {
    lati: 48.7365967211, longi: 13.4533829964,
    name: "Fürsteneck",
    plz: "94142"
  },
  {
    lati: 50.8380382654, longi: 13.952057494,
    name: "Bad Gottleuba-Berggießhübel",
    plz: "01816"
  },
  {
    lati: 53.5192618132, longi: 14.2841302321,
    name: "Blankensee, Grambow u.a.",
    plz: "17322"
  },
  {
    lati: 50.0729595222, longi: 8.75720485544,
    name: "Offenbach am Main",
    plz: "63069"
  },
  {
    lati: 49.6681694314, longi: 10.9182637543,
    name: "Röttenbach",
    plz: "91341"
  },
  {
    lati: 49.375103022, longi: 11.8011748163,
    name: "Ursensollen",
    plz: "92289"
  },
  {
    lati: 51.3386015092, longi: 12.3651549623,
    name: "Leipzig",
    plz: "04109"
  },
  {
    lati: 50.9062539032, longi: 6.93868116653,
    name: "Köln",
    plz: "50969"
  },
  {
    lati: 53.5985930531, longi: 10.0441957539,
    name: "Hamburg",
    plz: "22307"
  },
  {
    lati: 50.6066002442, longi: 10.8261362592,
    name: "Schmiedefeld, Frauenwald, Suhl",
    plz: "98711"
  },
  {
    lati: 49.8771409671, longi: 10.8769178344,
    name: "Bamberg",
    plz: "96049"
  },
  {
    lati: 47.5025467715, longi: 10.2913153431,
    name: "Sonthofen",
    plz: "87527"
  },
  {
    lati: 51.9114043774, longi: 9.26534884062,
    name: "Lügde",
    plz: "32676"
  },
  {
    lati: 54.6734136484, longi: 9.74046808165,
    name: "Mohrkirch, Rügge",
    plz: "24405"
  },
  {
    lati: 49.8846183861, longi: 9.29614444582,
    name: "Heimbuchenthal",
    plz: "63872"
  },
  {
    lati: 52.1697622155, longi: 12.5814384413,
    name: "Belzig",
    plz: "14806"
  },
  {
    lati: 50.6205171562, longi: 13.206815627,
    name: "Marienberg",
    plz: "09496"
  },
  {
    lati: 54.0373144394, longi: 13.852410753,
    name: "Kröslin, Krummin, Lassan u.a.",
    plz: "17440"
  },
  {
    lati: 54.3156016376, longi: 12.7017291537,
    name: "Löbnitz",
    plz: "18314"
  },
  {
    lati: 53.5254933642, longi: 12.9387270697,
    name: "Möllenhagen",
    plz: "17219"
  },
  {
    lati: 53.0277645925, longi: 11.5390615307,
    name: "Schnackenburg",
    plz: "29493"
  },
  {
    lati: 50.7006294265, longi: 11.6206022018,
    name: "Pößneck",
    plz: "07381"
  },
  {
    lati: 52.621845223, longi: 9.94167380465,
    name: "Hambühren",
    plz: "29313"
  },
  {
    lati: 51.7865651735, longi: 9.34464986692,
    name: "Höxter",
    plz: "37671"
  },
  {
    lati: 48.7097746624, longi: 9.38548050865,
    name: "Deizisau",
    plz: "73779"
  },
  {
    lati: 49.9856284238, longi: 9.51984239734,
    name: "Rechtenbach",
    plz: "97848"
  },
  {
    lati: 49.3286703772, longi: 8.82247732992,
    name: "Meckesheim",
    plz: "74909"
  },
  {
    lati: 48.9323165433, longi: 8.2889324619,
    name: "Durmersheim",
    plz: "76448"
  },
  {
    lati: 49.4690929445, longi: 8.50322345641,
    name: "Mannheim",
    plz: "68163"
  },
  {
    lati: 50.1811457585, longi: 7.04268093045,
    name: "Alflen",
    plz: "56828"
  },
  {
    lati: 52.3958392713, longi: 7.08385940599,
    name: "Nordhorn",
    plz: "48529"
  },
  {
    lati: 53.2427842702, longi: 7.56236302892,
    name: "Nortmoor",
    plz: "26845"
  },
  {
    lati: 50.771206691, longi: 8.26134442799,
    name: "Dillenburg",
    plz: "35685"
  },
  {
    lati: 50.7430688651, longi: 8.28153239618,
    name: "Dillenburg",
    plz: "35683"
  },
  {
    lati: 49.2659396364, longi: 8.29593648103,
    name: "Weingarten (Pfalz)",
    plz: "67366"
  },
  {
    lati: 48.2732010373, longi: 8.33949398352,
    name: "Schiltach",
    plz: "77761"
  },
  {
    lati: 51.6745070891, longi: 8.60206647843,
    name: "Salzkotten",
    plz: "33154"
  },
  {
    lati: 49.2654432968, longi: 9.3914319749,
    name: "Hardthausen am Kocher",
    plz: "74239"
  },
  {
    lati: 50.2418518911, longi: 7.73864890965,
    name: "Nastätten u.a.",
    plz: "56355"
  },
  {
    lati: 52.067111659, longi: 10.853449515,
    name: "Beierstedt",
    plz: "38382"
  },
  {
    lati: 49.3064719651, longi: 6.83278780741,
    name: "Schwalbach",
    plz: "66773"
  },
  {
    lati: 49.4616766615, longi: 11.0567621114,
    name: "Nürnberg",
    plz: "90419"
  },
  {
    lati: 48.2014573146, longi: 11.8732548927,
    name: "Markt Schwaben",
    plz: "85570"
  },
  {
    lati: 52.5564336997, longi: 13.4486189706,
    name: "Berlin Weißensee",
    plz: "13086"
  },
  {
    lati: 50.9192440448, longi: 11.8791209833,
    name: "Bad Klosterlausnitz",
    plz: "07639"
  },
  {
    lati: 51.8345232406, longi: 6.45528639614,
    name: "Isselburg",
    plz: "46419"
  },
  {
    lati: 51.2054707335, longi: 6.87768347922,
    name: "Düsseldorf",
    plz: "40627"
  },
  {
    lati: 49.5152797177, longi: 7.43279941739,
    name: "Pfeffelbach",
    plz: "66871"
  },
  {
    lati: 47.5697210621, longi: 9.85147465158,
    name: "Scheidegg",
    plz: "88175"
  },
  {
    lati: 51.302091602, longi: 9.47831570511,
    name: "Kassel",
    plz: "34121"
  },
  {
    lati: 48.3448340129, longi: 12.4244442978,
    name: "Schönberg",
    plz: "84573"
  },
  {
    lati: 49.1122061657, longi: 10.3026129107,
    name: "Schopfloch",
    plz: "91626"
  },
  {
    lati: 49.9105555559, longi: 11.9144774217,
    name: "Kulmain",
    plz: "95508"
  },
  {
    lati: 51.2944187908, longi: 12.2098974342,
    name: "Markranstädt",
    plz: "04420"
  },
  {
    lati: 50.9026682962, longi: 14.678962975,
    name: "Großschönau",
    plz: "02779"
  },
  {
    lati: 51.788682162, longi: 6.01783261579,
    name: "Kranenburg",
    plz: "47559"
  },
  {
    lati: 51.0569403429, longi: 6.69069804527,
    name: "Rommerskirchen",
    plz: "41569"
  },
  {
    lati: 50.4805344684, longi: 11.100984784,
    name: "Neuhaus am Rennweg, Lauscha",
    plz: "98724"
  },
  {
    lati: 50.2445882571, longi: 11.3349313925,
    name: "Kronach",
    plz: "96317"
  },
  {
    lati: 49.8474346004, longi: 11.441452193,
    name: "Ahorntal",
    plz: "95491"
  },
  {
    lati: 51.302573168, longi: 11.7965199077,
    name: "Mücheln/ Geiseltal",
    plz: "06249"
  },
  {
    lati: 52.288799841, longi: 7.9775693459,
    name: "Osnabrück",
    plz: "49076"
  },
  {
    lati: 51.938355337, longi: 8.87402765345,
    name: "Detmold",
    plz: "32756"
  },
  {
    lati: 48.8423656056, longi: 8.92175495259,
    name: "Weissach",
    plz: "71287"
  },
  {
    lati: 48.5884256739, longi: 9.30668165723,
    name: "Großbettlingen",
    plz: "72663"
  },
  {
    lati: 48.3835583338, longi: 9.67950962274,
    name: "Schelklingen",
    plz: "89601"
  },
  {
    lati: 52.0565356989, longi: 10.2380636537,
    name: "Heere",
    plz: "38277"
  },
  {
    lati: 50.3639636806, longi: 8.9851685015,
    name: "Ranstadt",
    plz: "63691"
  },
  {
    lati: 50.1012830791, longi: 9.63572087481,
    name: "Rieneck",
    plz: "97794"
  },
  {
    lati: 51.5180087254, longi: 11.2505430273,
    name: "Sangerhausen",
    plz: "06526"
  },
  {
    lati: 50.9601695705, longi: 10.7243293175,
    name: "Drei Gleichen",
    plz: "99869"
  },
  {
    lati: 50.8083001513, longi: 10.625285731,
    name: "Tambach-Dietharz/ Thür.",
    plz: "99897"
  },
  {
    lati: 50.3630229455, longi: 11.319728565,
    name: "Pressig",
    plz: "96332"
  },
  {
    lati: 51.2913878424, longi: 7.03583542666,
    name: "Wülfrath",
    plz: "42489"
  },
  {
    lati: 51.2781566123, longi: 7.83062126446,
    name: "Neuenrade",
    plz: "58809"
  },
  {
    lati: 51.3351272223, longi: 7.86088098899,
    name: "Balve",
    plz: "58802"
  },
  {
    lati: 49.2990979139, longi: 8.71481151077,
    name: "Wiesloch",
    plz: "69168"
  },
  {
    lati: 48.4541654242, longi: 9.04785959167,
    name: "Dußlingen",
    plz: "72144"
  },
  {
    lati: 51.1433484174, longi: 9.26132011419,
    name: "Fritzlar",
    plz: "34560"
  },
  {
    lati: 49.8409268577, longi: 9.41114169091,
    name: "Altenbuch",
    plz: "97901"
  },
  {
    lati: 48.2818250548, longi: 9.65899272477,
    name: "Ehingen (Donau), Lauterach",
    plz: "89584"
  },
  {
    lati: 47.9722332332, longi: 12.4591368849,
    name: "Seeon-Seebruck",
    plz: "83370"
  },
  {
    lati: 49.1931290105, longi: 12.9938374579,
    name: "Arrach",
    plz: "93474"
  },
  {
    lati: 49.3540048884, longi: 12.6020314878,
    name: "Schönthal",
    plz: "93488"
  },
  {
    lati: 48.164943326, longi: 12.8154060245,
    name: "Burghausen",
    plz: "84489"
  },
  {
    lati: 47.991377972, longi: 11.9931908748,
    name: "Aßling",
    plz: "85617"
  },
  {
    lati: 52.8750853491, longi: 7.90398992534,
    name: "Molbergen",
    plz: "49696"
  },
  {
    lati: 49.505095643, longi: 8.02168354399,
    name: "Wattenheim",
    plz: "67319"
  },
  {
    lati: 49.6896626148, longi: 7.70355706205,
    name: "Callbach",
    plz: "67829"
  },
  {
    lati: 51.8387820012, longi: 8.30569030512,
    name: "Rheda-Wiedenbrück",
    plz: "33378"
  },
  {
    lati: 50.1534539525, longi: 8.83554051113,
    name: "Maintal",
    plz: "63477"
  },
  {
    lati: 51.5388333008, longi: 9.94818054847,
    name: "Göttingen",
    plz: "37085"
  },
  {
    lati: 53.5692000046, longi: 10.1050809208,
    name: "Hamburg",
    plz: "22043"
  },
  {
    lati: 54.2745118341, longi: 9.90706037525,
    name: "Westensee",
    plz: "24259"
  },
  {
    lati: 49.8697638798, longi: 9.54869709447,
    name: "Esselbach",
    plz: "97839"
  },
  {
    lati: 49.0290508753, longi: 11.5908064821,
    name: "Dietfurt a.d. Altmühl",
    plz: "92345"
  },
  {
    lati: 49.5346518723, longi: 11.5819553044,
    name: "Etzelwang",
    plz: "92268"
  },
  {
    lati: 49.9032939378, longi: 7.42231345972,
    name: "Dickenschied u.a.",
    plz: "55483"
  },
  {
    lati: 51.4930471256, longi: 7.76066129579,
    name: "Fröndenberg/Ruhr",
    plz: "58730"
  },
  {
    lati: 50.0019372901, longi: 7.35936112609,
    name: "Dickenschied u.a.",
    plz: "55483"
  },
  {
    lati: 53.6553874814, longi: 8.9718217883,
    name: "Stinstedt",
    plz: "21772"
  },
  {
    lati: 52.5133719029, longi: 9.02293969089,
    name: "Stolzenau",
    plz: "31592"
  },
  {
    lati: 53.2373208224, longi: 9.10281298106,
    name: "Tarmstedt, Breddorf u.a.",
    plz: "27412"
  },
  {
    lati: 49.3488689185, longi: 9.53129188657,
    name: "Schöntal",
    plz: "74214"
  },
  {
    lati: 52.7347072698, longi: 8.68444319691,
    name: "Ehrenburg",
    plz: "27248"
  },
  {
    lati: 53.3340343782, longi: 8.8548931999,
    name: "Hambergen, Holste u.a.",
    plz: "27729"
  },
  {
    lati: 49.7073649553, longi: 10.7457257832,
    name: "Lonnerstadt",
    plz: "91475"
  },
  {
    lati: 51.0802335732, longi: 14.3252596958,
    name: "Neukirch/Lausitz",
    plz: "01904"
  },
  {
    lati: 52.0860287945, longi: 10.5451328993,
    name: "Dorstadt, Flöthe, Börßum u.a.",
    plz: "38312"
  },
  {
    lati: 49.6793514649, longi: 9.77151688599,
    name: "Großrinderfeld",
    plz: "97950"
  },
  {
    lati: 53.8652143865, longi: 9.85934265244,
    name: "Lentföhrden",
    plz: "24632"
  },
  {
    lati: 49.8404808536, longi: 9.88785381636,
    name: "Veitshöchheim",
    plz: "97209"
  },
  {
    lati: 48.7168223977, longi: 13.0966828904,
    name: "Winzer",
    plz: "94577"
  },
  {
    lati: 50.0940721426, longi: 11.7327737879,
    name: "Gefrees",
    plz: "95482"
  },
  {
    lati: 49.974740546, longi: 10.7293893883,
    name: "Stettfeld",
    plz: "96188"
  },
  {
    lati: 48.3463746051, longi: 10.92092906,
    name: "Augsburg",
    plz: "86161"
  },
  {
    lati: 48.586647178, longi: 9.44330946992,
    name: "Owen",
    plz: "73277"
  },
  {
    lati: 51.7526666231, longi: 7.18602049678,
    name: "Haltern am See",
    plz: "45721"
  },
  {
    lati: 51.5659188083, longi: 7.07287261005,
    name: "Gelsenkirchen",
    plz: "45897"
  },
  {
    lati: 51.8716865335, longi: 6.5872019347,
    name: "Bocholt",
    plz: "46399"
  },
  {
    lati: 51.5462610041, longi: 6.93536759484,
    name: "Bottrop",
    plz: "46240"
  },
  {
    lati: 49.8842488731, longi: 8.2205772522,
    name: "Ober-Olm",
    plz: "55270"
  },
  {
    lati: 52.2919900957, longi: 9.04008317493,
    name: "Bückeburg",
    plz: "31675"
  },
  {
    lati: 48.7122830072, longi: 9.20438816804,
    name: "Stuttgart",
    plz: "70599"
  },
  {
    lati: 48.6088484594, longi: 13.5509766538,
    name: "Thyrnau",
    plz: "94136"
  },
  {
    lati: 51.1613506716, longi: 14.7700037255,
    name: "Reichenbach, Vierkirchen",
    plz: "02894"
  },
  {
    lati: 49.2192525587, longi: 9.6966740943,
    name: "Kupferzell",
    plz: "74635"
  },
  {
    lati: 52.3640551757, longi: 9.73336013719,
    name: "Hannover",
    plz: "30169"
  },
  {
    lati: 54.2857825777, longi: 10.1900627441,
    name: "Kiel",
    plz: "24146"
  },
  {
    lati: 48.0312602552, longi: 12.5656244866,
    name: "Trostberg",
    plz: "83308"
  },
  {
    lati: 50.9124676899, longi: 13.0746689077,
    name: "Frankenberg",
    plz: "09669"
  },
  {
    lati: 48.1631342413, longi: 11.1335166679,
    name: "Jesenwang",
    plz: "82287"
  },
  {
    lati: 51.8849019679, longi: 11.1887890849,
    name: "Wegeleben",
    plz: "38828"
  },
  {
    lati: 48.1229801597, longi: 11.1669804542,
    name: "Grafrath",
    plz: "82284"
  },
  {
    lati: 48.1354507381, longi: 8.5045276417,
    name: "Niedereschach",
    plz: "78078"
  },
  {
    lati: 53.1754930596, longi: 8.25534655791,
    name: "Oldenburg (Oldenburg)",
    plz: "26125"
  },
  {
    lati: 49.3936897274, longi: 7.88980165114,
    name: "Waldleiningen, Fischbach",
    plz: "67693"
  },
  {
    lati: 51.6998679194, longi: 6.83500288962,
    name: "Schermbeck",
    plz: "46514"
  },
  {
    lati: 50.8327438131, longi: 13.404030148,
    name: "Lichtenberg/Erzgeb.",
    plz: "09638"
  },
  {
    lati: 54.4052590978, longi: 9.30055926842,
    name: "Wohlde",
    plz: "24899"
  },
  {
    lati: 54.4879604254, longi: 9.54969235528,
    name: "Busdorf",
    plz: "24866"
  },
  {
    lati: 49.2938478549, longi: 6.89515224475,
    name: "Püttlingen",
    plz: "66346"
  },
  {
    lati: 49.8235576593, longi: 11.0674828959,
    name: "Buttenheim",
    plz: "96155"
  },
  {
    lati: 48.5012992736, longi: 11.4333267457,
    name: "Scheyern",
    plz: "85298"
  },
  {
    lati: 48.9692224826, longi: 11.6907255759,
    name: "Riedenburg",
    plz: "93339"
  },
  {
    lati: 51.4747785474, longi: 8.9950120558,
    name: "Diemelstadt",
    plz: "34474"
  },
  {
    lati: 53.7380266898, longi: 8.98329333134,
    name: "Bülkau",
    plz: "21782"
  },
  {
    lati: 48.9989597868, longi: 9.18467173756,
    name: "Hessigheim",
    plz: "74394"
  },
  {
    lati: 49.689641775, longi: 10.5483027877,
    name: "Markt Taschendorf",
    plz: "91480"
  },
  {
    lati: 54.2748482799, longi: 9.71411408332,
    name: "Osterrönfeld",
    plz: "24783"
  },
  {
    lati: 52.3578732738, longi: 9.76265265114,
    name: "Hannover",
    plz: "30173"
  },
  {
    lati: 53.6400396363, longi: 9.91543382119,
    name: "Hamburg",
    plz: "22457"
  },
  {
    lati: 50.0554568384, longi: 8.48093424303,
    name: "Hattersheim",
    plz: "65795"
  },
  {
    lati: 52.3680529826, longi: 13.5048076157,
    name: "Schönefeld",
    plz: "12529"
  },
  {
    lati: 51.579101509, longi: 6.23234247174,
    name: "Kevelaer-Mitte",
    plz: "47623"
  },
  {
    lati: 49.9611244448, longi: 9.32214445395,
    name: "Waldaschaff, Waldaschaffer Forst",
    plz: "63857"
  },
  {
    lati: 53.728819722, longi: 9.66692422926,
    name: "Elmshorn",
    plz: "25336"
  },
  {
    lati: 49.897156394, longi: 10.5595782905,
    name: "Rauhenebrach",
    plz: "96181"
  },
  {
    lati: 49.408472899, longi: 10.7893622634,
    name: "Großhabersdorf",
    plz: "90613"
  },
  {
    lati: 50.2455373198, longi: 11.4126038255,
    name: "Marktrodach",
    plz: "96364"
  },
  {
    lati: 49.1566405105, longi: 11.9568277978,
    name: "Kallmünz",
    plz: "93183"
  },
  {
    lati: 51.430161412, longi: 7.57660636571,
    name: "Schwerte",
    plz: "58239"
  },
  {
    lati: 51.1965431372, longi: 7.62921451427,
    name: "Lüdenscheid",
    plz: "58515"
  },
  {
    lati: 48.1489459969, longi: 8.18003277597,
    name: "Schonach im Schwarzwald",
    plz: "78136"
  },
  {
    lati: 49.1126594041, longi: 12.1683144793,
    name: "Zeitlarn",
    plz: "93197"
  },
  {
    lati: 49.7922350482, longi: 9.26350270498,
    name: "Mönchberg",
    plz: "63933"
  },
  {
    lati: 49.7768439951, longi: 9.87301165647,
    name: "Höchberg",
    plz: "97204"
  },
  {
    lati: 48.895105637, longi: 9.45222184215,
    name: "Leutenbach",
    plz: "71397"
  },
  {
    lati: 50.9003709934, longi: 12.7642883558,
    name: "Mühlau",
    plz: "09241"
  },
  {
    lati: 50.4516082686, longi: 11.2774586931,
    name: "Tettau",
    plz: "96355"
  },
  {
    lati: 51.079463699, longi: 13.7298900766,
    name: "Dresden",
    plz: "01127"
  },
  {
    lati: 49.4900656679, longi: 8.11298329373,
    name: "Weisenheim am Sand",
    plz: "67256"
  },
  {
    lati: 50.1265144596, longi: 7.00567919453,
    name: "Lutzerath",
    plz: "56826"
  },
  {
    lati: 52.9272412156, longi: 7.52335514628,
    name: "Börger",
    plz: "26904"
  },
  {
    lati: 50.1814284265, longi: 7.69232367221,
    name: "Sankt Goarshausen",
    plz: "56346"
  },
  {
    lati: 49.8025350814, longi: 7.82806492524,
    name: "Bad Kreuznach",
    plz: "55583"
  },
  {
    lati: 52.0583624531, longi: 8.71132569073,
    name: "Bad Salzuflen",
    plz: "32107"
  },
  {
    lati: 48.5347420394, longi: 12.367285573,
    name: "Kröning",
    plz: "84178"
  },
  {
    lati: 52.3746633976, longi: 11.7151435436,
    name: "Tangerhütte u.a.",
    plz: "39517"
  },
  {
    lati: 48.0661722043, longi: 10.80662354,
    name: "Igling",
    plz: "86859"
  },
  {
    lati: 52.8415974593, longi: 11.1420466561,
    name: "Salzwedel",
    plz: "29410"
  },
  {
    lati: 50.2326106685, longi: 7.87222245243,
    name: "Nastätten u.a.",
    plz: "56355"
  },
  {
    lati: 52.4973742058, longi: 8.10085167862,
    name: "Neuenkirchen-Vörden",
    plz: "49434"
  },
  {
    lati: 52.1845598123, longi: 8.79627834346,
    name: "Bad Oeynhausen",
    plz: "32545"
  },
  {
    lati: 52.9986209578, longi: 10.7988895448,
    name: "Rosche",
    plz: "29571"
  },
  {
    lati: 50.197830801, longi: 10.8100649952,
    name: "Seßlach",
    plz: "96145"
  },
  {
    lati: 50.9482779624, longi: 14.6590545447,
    name: "Leutersdorf, Spitzkunnersdorf",
    plz: "02794"
  },
  {
    lati: 51.1471109222, longi: 14.9791602411,
    name: "Görlitz",
    plz: "02826"
  },
  {
    lati: 49.8585193506, longi: 9.84034143795,
    name: "Erlabrunn",
    plz: "97250"
  },
  {
    lati: 51.3628348749, longi: 9.46592434866,
    name: "Vellmar",
    plz: "34246"
  },
  {
    lati: 48.560162328, longi: 12.5554678672,
    name: "Marklkofen",
    plz: "84163"
  },
  {
    lati: 48.6334175255, longi: 12.8451057922,
    name: "Eichendorf",
    plz: "94428"
  },
  {
    lati: 50.1137047969, longi: 9.28680390268,
    name: "Kleinkahl",
    plz: "63828"
  },
  {
    lati: 50.1087417008, longi: 9.3548452124,
    name: "Wiesen, Wiesener Forst",
    plz: "63831"
  },
  {
    lati: 52.6386377795, longi: 9.23328610729,
    name: "Nienburg/Weser",
    plz: "31582"
  },
  {
    lati: 49.3885665638, longi: 8.03470630775,
    name: "Frankenstein, Neidenfels, Frankeneck",
    plz: "67468"
  },
  {
    lati: 48.1428980037, longi: 8.03975506836,
    name: "Winden im Elztal",
    plz: "79297"
  },
  {
    lati: 51.6678099675, longi: 8.27724612474,
    name: "Lippstadt",
    plz: "59556"
  },
  {
    lati: 50.151004972, longi: 7.11465057379,
    name: "Ediger-Eller",
    plz: "56814"
  },
  {
    lati: 51.4714295801, longi: 6.76954049833,
    name: "Duisburg",
    plz: "47137"
  },
  {
    lati: 48.5031633775, longi: 10.2387697329,
    name: "Niederstotzingen",
    plz: "89168"
  },
  {
    lati: 48.440731381, longi: 12.3455168129,
    name: "Vilsbiburg",
    plz: "84137"
  },
  {
    lati: 48.2573639294, longi: 9.96664219537,
    name: "Laupheim",
    plz: "88471"
  },
  {
    lati: 51.7906533835, longi: 10.0828801815,
    name: "Kalefeld",
    plz: "37589"
  },
  {
    lati: 52.8241355734, longi: 10.2707142068,
    name: "Unterlüß",
    plz: "29345"
  },
  {
    lati: 51.7928992394, longi: 11.3730048467,
    name: "Seeland",
    plz: "06464"
  },
  {
    lati: 48.7605955471, longi: 10.9456704145,
    name: "Marxheim",
    plz: "86688"
  },
  {
    lati: 51.6747566753, longi: 8.08884217908,
    name: "Lippetal",
    plz: "59510"
  },
  {
    lati: 50.5784417438, longi: 7.30282974278,
    name: "Linz am Rhein, Ockenfels",
    plz: "53545"
  },
  {
    lati: 48.6605668657, longi: 9.65623371765,
    name: "Heiningen",
    plz: "73092"
  },
  {
    lati: 49.6629679596, longi: 11.1079113073,
    name: "Effeltrich",
    plz: "91090"
  },
  {
    lati: 51.5654643021, longi: 10.862720591,
    name: "Neustadt/Harz u.a.",
    plz: "99762"
  },
  {
    lati: 50.947818088, longi: 12.6910321069,
    name: "Penig",
    plz: "09322"
  },
  {
    lati: 52.361033618, longi: 9.28848780558,
    name: "Lindhorst",
    plz: "31698"
  },
  {
    lati: 52.8330245178, longi: 9.59893516054,
    name: "Walsrode, Ostenholz",
    plz: "29664"
  },
  {
    lati: 52.049705944, longi: 8.54734665034,
    name: "Bielefeld",
    plz: "33611"
  },
  {
    lati: 48.9420182945, longi: 8.57237527553,
    name: "Remchingen",
    plz: "75196"
  },
  {
    lati: 51.2662297341, longi: 8.84255763671,
    name: "Korbach",
    plz: "34497"
  },
  {
    lati: 51.1429360291, longi: 7.12616965458,
    name: "Solingen",
    plz: "42659"
  },
  {
    lati: 54.2258690607, longi: 9.49099502825,
    name: "Hamdorf",
    plz: "24805"
  },
  {
    lati: 48.5873777054, longi: 12.1352266611,
    name: "Ergolding, Landshut",
    plz: "84030"
  },
  {
    lati: 50.9732206728, longi: 12.3665890035,
    name: "Nobitz, Göhren, Windischleuba",
    plz: "04603"
  },
  {
    lati: 50.960786664, longi: 11.5457665345,
    name: "Jena, Bucha, Großpürschütz u.a.",
    plz: "07751"
  },
  {
    lati: 48.1451962896, longi: 11.5821419504,
    name: "München",
    plz: "80539"
  },
  {
    lati: 51.066701556, longi: 11.9523529997,
    name: "Meineweh, Osterfeld",
    plz: "06721"
  },
  {
    lati: 52.8043055712, longi: 10.6570354866,
    name: "Lüder",
    plz: "29394"
  },
  {
    lati: 53.2824591207, longi: 10.3395153835,
    name: "Vögelsen",
    plz: "21360"
  },
  {
    lati: 48.9288503189, longi: 10.4672983557,
    name: "Marktoffingen",
    plz: "86748"
  },
  {
    lati: 50.1740658917, longi: 7.49867235626,
    name: "Wildenbungert, Gondershausen, Nörtershausen u.a.",
    plz: "56283"
  },
  {
    lati: 52.6399516308, longi: 13.2895840416,
    name: "Berlin Frohnau",
    plz: "13465"
  },
  {
    lati: 49.8412069629, longi: 9.56831151848,
    name: "Marktheidenfeld",
    plz: "97828"
  },
  {
    lati: 49.0577488755, longi: 9.25441478026,
    name: "Ilsfeld",
    plz: "74360"
  },
  {
    lati: 52.5058062286, longi: 7.68273219705,
    name: "Fürstenau",
    plz: "49584"
  },
  {
    lati: 49.2651382637, longi: 7.7503814918,
    name: "Clausen",
    plz: "66978"
  },
  {
    lati: 48.3007969217, longi: 9.98568340159,
    name: "Staig",
    plz: "89195"
  },
  {
    lati: 47.8729208935, longi: 10.2298572112,
    name: "Bad Grönenbach",
    plz: "87730"
  },
  {
    lati: 48.3317582631, longi: 11.1092345797,
    name: "Eurasburg",
    plz: "86495"
  },
  {
    lati: 50.6461635562, longi: 11.2011665408,
    name: "Schwarzburg",
    plz: "07427"
  },
  {
    lati: 54.432373792, longi: 9.3554424708,
    name: "Dörpstedt",
    plz: "24869"
  },
  {
    lati: 47.84367286, longi: 8.85819484842,
    name: "Aach",
    plz: "78267"
  },
  {
    lati: 50.6067443934, longi: 7.27293269271,
    name: "Erpel",
    plz: "53579"
  },
  {
    lati: 49.3252845145, longi: 7.98613435937,
    name: "Venningen",
    plz: "67482"
  },
  {
    lati: 52.0951282172, longi: 7.63361499356,
    name: "Greven",
    plz: "48268"
  },
  {
    lati: 47.6012477985, longi: 7.66936812715,
    name: "Lörrach",
    plz: "79540"
  },
  {
    lati: 48.8417767965, longi: 12.628185829,
    name: "Aiterhofen",
    plz: "94330"
  },
  {
    lati: 49.3679033903, longi: 12.6973243587,
    name: "Waldmünchen",
    plz: "93449"
  },
  {
    lati: 53.3295862305, longi: 12.76218222,
    name: "Rechlin",
    plz: "17248"
  },
  {
    lati: 50.6796093006, longi: 10.7047771069,
    name: "Oberhof",
    plz: "98559"
  },
  {
    lati: 49.5582627021, longi: 11.007071425,
    name: "Erlangen",
    plz: "91058"
  },
  {
    lati: 50.1356877978, longi: 11.5179375884,
    name: "Untersteinach",
    plz: "95369"
  },
  {
    lati: 47.8150844133, longi: 12.3749498154,
    name: "Bernau a. Chiemsee",
    plz: "83233"
  },
  {
    lati: 49.5358675226, longi: 8.07322144347,
    name: "Hettenleidelheim",
    plz: "67310"
  },
  {
    lati: 52.6615673978, longi: 8.23152202476,
    name: "Lohne",
    plz: "49393"
  },
  {
    lati: 50.3075915925, longi: 6.70912385889,
    name: "Walsdorf, Nohn u.a.",
    plz: "54578"
  },
  {
    lati: 50.9214860552, longi: 6.95048094656,
    name: "Köln",
    plz: "50677"
  },
  {
    lati: 54.6795201677, longi: 8.70455514321,
    name: "Oland",
    plz: "25867"
  },
  {
    lati: 47.7803358461, longi: 8.76077777423,
    name: "Hilzingen",
    plz: "78247"
  },
  {
    lati: 51.7103625215, longi: 8.75301617284,
    name: "Paderborn",
    plz: "33098"
  },
  {
    lati: 49.3879219408, longi: 9.63580741143,
    name: "Krautheim",
    plz: "74238"
  },
  {
    lati: 54.5187001592, longi: 9.31617501013,
    name: "Treia, Ahrenviölfeld",
    plz: "24896"
  },
  {
    lati: 48.4993142434, longi: 10.1067272032,
    name: "Langenau",
    plz: "89129"
  },
  {
    lati: 48.8362204094, longi: 11.5973340367,
    name: "Oberdolling",
    plz: "85129"
  },
  {
    lati: 51.7749047606, longi: 11.7676552374,
    name: "Bernburg (Saale)",
    plz: "06406"
  },
  {
    lati: 50.2464557548, longi: 7.68833742573,
    name: "Osterspai",
    plz: "56340"
  },
  {
    lati: 49.1607742852, longi: 8.58708241665,
    name: "Forst",
    plz: "76694"
  },
  {
    lati: 49.2460491298, longi: 7.06439681236,
    name: "Saarbrücken",
    plz: "66133"
  },
  {
    lati: 52.5109400335, longi: 7.37641123688,
    name: "Lingen",
    plz: "49811"
  },
  {
    lati: 51.2313652217, longi: 6.85569781626,
    name: "Düsseldorf",
    plz: "40625"
  },
  {
    lati: 49.3443779957, longi: 6.94798371538,
    name: "Heusweiler",
    plz: "66265"
  },
  {
    lati: 51.486381288, longi: 9.29190699389,
    name: "Liebenau (Hessen)",
    plz: "34396"
  },
  {
    lati: 48.3855148801, longi: 9.29781178619,
    name: "Engstingen",
    plz: "72829"
  },
  {
    lati: 48.7915979789, longi: 9.32946138291,
    name: "Kernen im Remstal",
    plz: "71394"
  },
  {
    lati: 54.5751861517, longi: 9.42459885728,
    name: "Bollingstedt, Jübek",
    plz: "24855"
  },
  {
    lati: 50.8668384704, longi: 13.0174315148,
    name: "Niederwiesa",
    plz: "09577"
  },
  {
    lati: 47.8626902587, longi: 12.1809562317,
    name: "Stephanskirchen",
    plz: "83071"
  },
  {
    lati: 54.7817068849, longi: 9.68290057297,
    name: "Dollerup",
    plz: "24989"
  },
  {
    lati: 48.0555606294, longi: 9.94756029808,
    name: "Ochsenhausen",
    plz: "88416"
  },
  {
    lati: 47.8942275421, longi: 10.0605679777,
    name: "Aichstetten",
    plz: "88317"
  },
  {
    lati: 49.7567503704, longi: 10.7601038893,
    name: "Mühlhausen",
    plz: "96172"
  },
  {
    lati: 52.2513040741, longi: 10.3662776829,
    name: "Vechelde",
    plz: "38159"
  },
  {
    lati: 50.6223675572, longi: 7.24793279021,
    name: "Rheinbreitbach",
    plz: "53619"
  },
  {
    lati: 52.2892419544, longi: 9.83985634831,
    name: "Laatzen",
    plz: "30880"
  },
  {
    lati: 51.3910471082, longi: 12.0409728987,
    name: "Schkopau",
    plz: "06258"
  },
  {
    lati: 51.48777249, longi: 12.6217060139,
    name: "Eilenburg u.a.",
    plz: "04838"
  },
  {
    lati: 49.143621441, longi: 12.723900965,
    name: "Zandt",
    plz: "93499"
  },
  {
    lati: 47.893334409, longi: 10.7518544631,
    name: "Kaltental",
    plz: "87662"
  },
  {
    lati: 52.6328718115, longi: 13.3298046019,
    name: "Glienicke/Nordbahn",
    plz: "16548"
  },
  {
    lati: 48.8255975882, longi: 13.7436055183,
    name: "Haidmühle",
    plz: "94145"
  },
  {
    lati: 52.7265958347, longi: 7.96247077676,
    name: "Essen (Oldenburg)",
    plz: "49632"
  },
  {
    lati: 48.5567880789, longi: 8.95689094381,
    name: "Ammerbuch",
    plz: "72119"
  },
  {
    lati: 53.566400598, longi: 9.47552403824,
    name: "Stade",
    plz: "21684"
  },
  {
    lati: 49.3276363757, longi: 11.5414192168,
    name: "Pilsach",
    plz: "92367"
  },
  {
    lati: 49.7850420753, longi: 11.0878088227,
    name: "Eggolsheim",
    plz: "91330"
  },
  {
    lati: 49.0760889746, longi: 11.0629473687,
    name: "Ettenstatt",
    plz: "91796"
  },
  {
    lati: 47.8839055379, longi: 12.46940768,
    name: "Chiemsee",
    plz: "83256"
  },
  {
    lati: 48.5393617275, longi: 12.0042408985,
    name: "Bruckberg",
    plz: "84079"
  },
  {
    lati: 50.6777703612, longi: 7.85150127733,
    name: "Nister",
    plz: "57645"
  },
  {
    lati: 52.2743248814, longi: 7.42760189153,
    name: "Rheine",
    plz: "48431"
  },
  {
    lati: 50.0201409653, longi: 8.17416552041,
    name: "Budenheim",
    plz: "55257"
  },
  {
    lati: 51.0811534336, longi: 9.5029267117,
    name: "Malsfeld",
    plz: "34323"
  },
  {
    lati: 51.0917810804, longi: 13.827603781,
    name: "Dresden",
    plz: "01099"
  },
  {
    lati: 48.739042882, longi: 10.1035870143,
    name: "Königsbronn",
    plz: "89551"
  },
  {
    lati: 52.2735916806, longi: 10.1914480709,
    name: "Ilsede",
    plz: "31241"
  },
  {
    lati: 53.590173824, longi: 10.1664243832,
    name: "Hamburg",
    plz: "22149"
  },
  {
    lati: 50.4708653615, longi: 9.71935605892,
    name: "Eichenzell",
    plz: "36124"
  },
  {
    lati: 53.7087732188, longi: 9.36696309689,
    name: "Drochtersen",
    plz: "21706"
  },
  {
    lati: 53.9242698203, longi: 9.52866118429,
    name: "Itzehoe",
    plz: "25524"
  },
  {
    lati: 48.7711839323, longi: 9.11283320723,
    name: "Stuttgart",
    plz: "70197"
  },
  {
    lati: 51.1474059921, longi: 12.6587519418,
    name: "Bad Lausick",
    plz: "04651"
  },
  {
    lati: 51.0143421282, longi: 12.6833117318,
    name: "Narsdorf",
    plz: "04657"
  },
  {
    lati: 52.9875149321, longi: 12.866317798,
    name: "Neuruppin",
    plz: "16827"
  },
  {
    lati: 47.8132758775, longi: 7.91107877953,
    name: "Utzenfeld",
    plz: "79694"
  },
  {
    lati: 50.5524393965, longi: 8.17241243329,
    name: "Mengerskirchen",
    plz: "35794"
  },
  {
    lati: 52.5009613456, longi: 13.4354823355,
    name: "Berlin Kreuzberg",
    plz: "10997"
  },
  {
    lati: 48.7094373826, longi: 11.3289717737,
    name: "Weichering",
    plz: "86706"
  },
  {
    lati: 48.7357359641, longi: 12.1507432167,
    name: "Neufahrn i. NB",
    plz: "84088"
  },
  {
    lati: 52.7676527747, longi: 9.39114149895,
    name: "Rethem (Aller), Häuslingen, Frankenfeld",
    plz: "27336"
  },
  {
    lati: 52.4177206336, longi: 9.58466579165,
    name: "Garbsen",
    plz: "30823"
  },
  {
    lati: 52.5205297595, longi: 13.2879145945,
    name: "Berlin Charlottenburg",
    plz: "14059"
  },
  {
    lati: 54.6904695856, longi: 10.0090431074,
    name: "Maasholm, Schleimünde",
    plz: "24404"
  },
  {
    lati: 48.7776581833, longi: 13.3559647191,
    name: "Saldenburg",
    plz: "94163"
  },
  {
    lati: 49.039215025, longi: 8.42218610209,
    name: "Karlsruhe",
    plz: "76131"
  },
  {
    lati: 50.008347118, longi: 9.84748415627,
    name: "Eußenheim",
    plz: "97776"
  },
  {
    lati: 49.4266006215, longi: 11.0140477789,
    name: "Nürnberg",
    plz: "90449"
  },
  {
    lati: 51.3693303864, longi: 7.16259050473,
    name: "Hattingen",
    plz: "45529"
  },
  {
    lati: 49.6245273982, longi: 7.36852941711,
    name: "Baumholder",
    plz: "55774"
  },
  {
    lati: 48.687607814, longi: 10.8225876446,
    name: "Asbach-Bäumenheim",
    plz: "86663"
  },
  {
    lati: 48.2278444551, longi: 8.65912813957,
    name: "Dietingen",
    plz: "78661"
  },
  {
    lati: 48.3402872662, longi: 8.87927803923,
    name: "Grosselfingen",
    plz: "72415"
  },
  {
    lati: 48.4087982203, longi: 12.0623623982,
    name: "Kirchberg",
    plz: "84434"
  },
  {
    lati: 49.2339406752, longi: 12.5503763805,
    name: "Pösing",
    plz: "93483"
  },
  {
    lati: 48.5260699035, longi: 12.7397361298,
    name: "Malgersdorf",
    plz: "84333"
  },
  {
    lati: 51.3213742569, longi: 6.7654226449,
    name: "Düsseldorf",
    plz: "40489"
  },
  {
    lati: 50.4086731623, longi: 7.53827582342,
    name: "Urmitz",
    plz: "56220"
  },
  {
    lati: 47.7542865469, longi: 7.62754082945,
    name: "Schliengen",
    plz: "79418"
  },
  {
    lati: 51.2814153826, longi: 9.32861533556,
    name: "Schauenburg",
    plz: "34270"
  },
  {
    lati: 49.9016712911, longi: 9.54148500046,
    name: "Hafenlohr, Rothenbuch",
    plz: "97840"
  },
  {
    lati: 50.1366195645, longi: 9.55683478569,
    name: "Fellen",
    plz: "97778"
  },
  {
    lati: 53.0722899361, longi: 8.91825742264,
    name: "Bremen",
    plz: "28327"
  },
  {
    lati: 48.9207567257, longi: 9.64237572031,
    name: "Kaisersbach",
    plz: "73667"
  },
  {
    lati: 52.3622497159, longi: 9.84751237019,
    name: "Hannover",
    plz: "30559"
  },
  {
    lati: 53.5678700005, longi: 10.0480007353,
    name: "Hamburg",
    plz: "22089"
  },
  {
    lati: 54.046856708, longi: 10.1818504361,
    name: "Trappenkamp",
    plz: "24610"
  },
  {
    lati: 51.3248927073, longi: 9.43736304859,
    name: "Kassel",
    plz: "34130"
  },
  {
    lati: 49.0975792693, longi: 11.2238148435,
    name: "Thalmässing",
    plz: "91177"
  },
  {
    lati: 49.2430751906, longi: 11.2374410712,
    name: "Allersberg",
    plz: "90584"
  },
  {
    lati: 50.7684732049, longi: 13.1622242185,
    name: "Grünhainichen",
    plz: "09579"
  },
  {
    lati: 51.2631453769, longi: 6.16442091645,
    name: "Brüggen",
    plz: "41379"
  },
  {
    lati: 51.1587683901, longi: 6.65371898514,
    name: "Neuss",
    plz: "41472"
  },
  {
    lati: 51.4444159159, longi: 7.25642189494,
    name: "Bochum",
    plz: "44799"
  },
  {
    lati: 53.3325850506, longi: 8.46308214658,
    name: "Brake",
    plz: "26919"
  },
  {
    lati: 51.2616000894, longi: 10.3653225358,
    name: "Rodeberg, Dünwald u.a.",
    plz: "99976"
  },
  {
    lati: 48.1385179055, longi: 11.6312074217,
    name: "München",
    plz: "81677"
  },
  {
    lati: 51.0283587295, longi: 12.8291176088,
    name: "Rochlitz",
    plz: "09306"
  },
  {
    lati: 48.1097942725, longi: 8.59992136447,
    name: "Deißlingen",
    plz: "78652"
  },
  {
    lati: 49.9030046126, longi: 7.02281448376,
    name: "Mülheim (Mosel)",
    plz: "54486"
  },
  {
    lati: 50.1509414689, longi: 6.44878618087,
    name: "Schönecken",
    plz: "54614"
  },
  {
    lati: 52.0170116723, longi: 11.7370164946,
    name: "Schönebeck",
    plz: "39218"
  },
  {
    lati: 50.0567758251, longi: 9.42040826102,
    name: "Frammersbach",
    plz: "97833"
  },
  {
    lati: 47.8247002671, longi: 11.2629307759,
    name: "Seeshaupt",
    plz: "82402"
  },
  {
    lati: 48.6167950255, longi: 13.1804519801,
    name: "Vilshofen an der Donau",
    plz: "94474"
  },
  {
    lati: 54.0717576339, longi: 13.9025355147,
    name: "Zinnowitz",
    plz: "17454"
  },
  {
    lati: 49.1805187477, longi: 11.4298708777,
    name: "Mühlhausen",
    plz: "92360"
  },
  {
    lati: 48.8164406164, longi: 9.43491893875,
    name: "Remshalden",
    plz: "73630"
  },
  {
    lati: 50.56060638, longi: 8.6268724289,
    name: "Gießen",
    plz: "35398"
  },
  {
    lati: 50.6761483611, longi: 6.48554902626,
    name: "Nideggen",
    plz: "52385"
  },
  {
    lati: 50.3329628612, longi: 7.3163670236,
    name: "Mertloch, Welling u.a.",
    plz: "56753"
  },
  {
    lati: 52.5819855597, longi: 8.81609366622,
    name: "Bahrenborstel, Barenburg, Kirchdorf",
    plz: "27245"
  },
  {
    lati: 47.729518276, longi: 9.13682632517,
    name: "Konstanz",
    plz: "78465"
  },
  {
    lati: 49.2375524413, longi: 9.26654478038,
    name: "Oedheim",
    plz: "74229"
  },
  {
    lati: 53.0253500112, longi: 7.99025208893,
    name: "Bösel",
    plz: "26219"
  },
  {
    lati: 52.1697959721, longi: 9.92666502155,
    name: "Hildesheim",
    plz: "31137"
  },
  {
    lati: 54.0466594184, longi: 9.9935935965,
    name: "Neumünster",
    plz: "24539"
  },
  {
    lati: 47.9028321207, longi: 10.1506272505,
    name: "Kronburg",
    plz: "87758"
  },
  {
    lati: 54.2307312296, longi: 10.2845658968,
    name: "Preetz",
    plz: "24211"
  },
  {
    lati: 49.8528216065, longi: 9.79486821371,
    name: "Leinach",
    plz: "97274"
  },
  {
    lati: 51.2889615872, longi: 10.5903907886,
    name: "Menteroda, Obermehler",
    plz: "99996"
  },
  {
    lati: 49.2972836606, longi: 7.25264242751,
    name: "Kirkel",
    plz: "66459"
  },
  {
    lati: 49.9426560681, longi: 7.27763785395,
    name: "Dickenschied u.a.",
    plz: "55483"
  },
  {
    lati: 49.8010348223, longi: 6.72584475379,
    name: "Kenn",
    plz: "54344"
  },
  {
    lati: 48.8809560685, longi: 12.5740828069,
    name: "Straubing",
    plz: "94315"
  },
  {
    lati: 52.9343706573, longi: 11.0341303005,
    name: "Luckau (Wendland)",
    plz: "29487"
  },
  {
    lati: 47.6644455251, longi: 11.0739150377,
    name: "Bad Kohlgrub",
    plz: "82433"
  },
  {
    lati: 51.0915356372, longi: 14.4161807225,
    name: "Wilthen",
    plz: "02681"
  },
  {
    lati: 50.965490451, longi: 7.04182462404,
    name: "Köln",
    plz: "51067"
  },
  {
    lati: 49.6900901804, longi: 7.14863179691,
    name: "Birkenfeld u.a.",
    plz: "55765"
  },
  {
    lati: 49.9437357441, longi: 7.87985977741,
    name: "Münster-Sarmsheim",
    plz: "55424"
  },
  {
    lati: 48.9868049333, longi: 8.1198673198,
    name: "Scheibenhardt",
    plz: "76779"
  },
  {
    lati: 51.4884338698, longi: 7.49877627606,
    name: "Dortmund",
    plz: "44263"
  },
  {
    lati: 51.6651212681, longi: 7.78432398133,
    name: "Hamm",
    plz: "59067"
  },
  {
    lati: 48.8886676195, longi: 8.87340712921,
    name: "Wiernsheim",
    plz: "75446"
  },
  {
    lati: 48.0775347205, longi: 9.7416889278,
    name: "Mittelbiberach",
    plz: "88441"
  },
  {
    lati: 54.5230748631, longi: 9.88512077428,
    name: "Loose",
    plz: "24366"
  },
  {
    lati: 48.63735584, longi: 9.52753510542,
    name: "Holzmaden",
    plz: "73271"
  },
  {
    lati: 53.5555540532, longi: 11.4979397335,
    name: "Plate",
    plz: "19086"
  },
  {
    lati: 54.0748356304, longi: 9.98074939226,
    name: "Neumünster",
    plz: "24534"
  },
  {
    lati: 47.7834157457, longi: 12.4548608509,
    name: "Grassau",
    plz: "83224"
  },
  {
    lati: 53.6713377331, longi: 10.8557508699,
    name: "Ziethen",
    plz: "23911"
  },
  {
    lati: 51.5278016492, longi: 13.3892276456,
    name: "Bad Liebenwerda",
    plz: "04924"
  },
  {
    lati: 50.7579859422, longi: 13.4140873657,
    name: "Dorfchemnitz, Mulda, Sayda",
    plz: "09619"
  },
  {
    lati: 53.7623276392, longi: 13.9260229751,
    name: "Vogelsang-Warsin, Meiersberg, Mönkebude u.a.",
    plz: "17375"
  },
  {
    lati: 51.5575963054, longi: 7.08294260034,
    name: "Gelsenkirchen",
    plz: "45891"
  },
  {
    lati: 51.4874466945, longi: 7.30933360201,
    name: "Bochum",
    plz: "44894"
  },
  {
    lati: 50.4824405564, longi: 11.3740476192,
    name: "Ludwigsstadt",
    plz: "96337"
  },
  {
    lati: 47.7005997677, longi: 11.6947355153,
    name: "Bad Wiessee",
    plz: "83707"
  },
  {
    lati: 51.9613060856, longi: 7.62645305556,
    name: "Münster",
    plz: "48143"
  },
  {
    lati: 48.3097430809, longi: 8.93124255853,
    name: "Bisingen",
    plz: "72406"
  },
  {
    lati: 48.6498203381, longi: 9.53080376186,
    name: "Ohmden",
    plz: "73275"
  },
  {
    lati: 48.5573419354, longi: 10.073404902,
    name: "Ballendorf",
    plz: "89177"
  },
  {
    lati: 49.5181068801, longi: 10.308682908,
    name: "Ergersheim",
    plz: "91465"
  },
  {
    lati: 54.1516895477, longi: 9.08742621275,
    name: "Hemmingstedt",
    plz: "25770"
  },
  {
    lati: 47.9556301287, longi: 12.5824327957,
    name: "Traunreut",
    plz: "83301"
  },
  {
    lati: 52.5148149904, longi: 13.1672609863,
    name: "Berlin Wilhelmstadt",
    plz: "13593"
  },
  {
    lati: 48.7595386811, longi: 9.63547825319,
    name: "Börtlingen",
    plz: "73104"
  },
  {
    lati: 50.7047072349, longi: 12.1107169169,
    name: "Berga/Elster",
    plz: "07980"
  },
  {
    lati: 51.0835393541, longi: 12.3295721443,
    name: "Lucka",
    plz: "04613"
  },
  {
    lati: 49.686943392, longi: 10.3050086555,
    name: "Rödelsee",
    plz: "97348"
  },
  {
    lati: 48.8932316823, longi: 13.3035628121,
    name: "Eppenschlag",
    plz: "94536"
  },
  {
    lati: 51.189598364, longi: 14.9822236562,
    name: "Görlitz",
    plz: "02828"
  },
  {
    lati: 49.484647184, longi: 7.51899746513,
    name: "Steinwenden u.a.",
    plz: "66879"
  },
  {
    lati: 50.3445781199, longi: 7.68192746909,
    name: "Fachbach, Exklave Lahnstein",
    plz: "56133"
  },
  {
    lati: 48.6348453381, longi: 8.74540089054,
    name: "Wildberg",
    plz: "72218"
  },
  {
    lati: 48.153797221, longi: 8.71168378204,
    name: "Wellendingen",
    plz: "78669"
  },
  {
    lati: 50.6803532717, longi: 8.82351132267,
    name: "Allendorf",
    plz: "35469"
  },
  {
    lati: 52.0849760457, longi: 8.51817338973,
    name: "Bielefeld",
    plz: "33739"
  },
  {
    lati: 53.6756810672, longi: 12.9254209512,
    name: "Stavenhagen",
    plz: "17153"
  },
  {
    lati: 51.4543039318, longi: 12.9623601099,
    name: "Belgern-Schildau",
    plz: "04889"
  },
  {
    lati: 48.4459445699, longi: 13.0677583908,
    name: "Bad Birnbach",
    plz: "84364"
  },
  {
    lati: 49.7711408862, longi: 11.8208743545,
    name: "Eschenbach i.d. OPf.",
    plz: "92676"
  },
  {
    lati: 48.6740000616, longi: 9.52250713448,
    name: "Schlierbach",
    plz: "73278"
  },
  {
    lati: 49.9180182723, longi: 12.0061200051,
    name: "Pullenreuth",
    plz: "95704"
  },
  {
    lati: 51.4700704537, longi: 11.9987366934,
    name: "Halle/ Saale",
    plz: "06112"
  },
  {
    lati: 51.0364427207, longi: 11.5034471418,
    name: "Apolda",
    plz: "99510"
  },
  {
    lati: 51.8011763095, longi: 7.72712420973,
    name: "Drensteinfurt",
    plz: "48317"
  },
  {
    lati: 48.3642574044, longi: 8.79876023561,
    name: "Haigerloch",
    plz: "72401"
  },
  {
    lati: 54.3587889829, longi: 10.1208941689,
    name: "Kiel",
    plz: "24106"
  },
  {
    lati: 50.1786060942, longi: 10.9205339357,
    name: "Großheirath",
    plz: "96269"
  },
  {
    lati: 47.7529447225, longi: 11.7448349854,
    name: "Gmund a. Tegernsee",
    plz: "83703"
  },
  {
    lati: 50.1489183939, longi: 7.73854371707,
    name: "Sankt Goarshausen",
    plz: "56346"
  },
  {
    lati: 51.7841582667, longi: 7.88430279485,
    name: "Ahlen",
    plz: "59227"
  },
  {
    lati: 50.3561798405, longi: 7.99977273697,
    name: "Birlenbach",
    plz: "65626"
  },
  {
    lati: 50.3855484731, longi: 7.3836100942,
    name: "Plaidt",
    plz: "56637"
  },
  {
    lati: 48.1666739227, longi: 9.35034032114,
    name: "Langenenslingen",
    plz: "88515"
  },
  {
    lati: 49.1550008423, longi: 8.47937806756,
    name: "Graben-Neudorf",
    plz: "76676"
  },
  {
    lati: 49.393513191, longi: 11.3813140111,
    name: "Altdorf bei Nürnberg",
    plz: "90518"
  },
  {
    lati: 51.1390373879, longi: 13.8517053033,
    name: "Dresden",
    plz: "01465"
  },
  {
    lati: 50.8651845063, longi: 14.6451320363,
    name: "Großschönau",
    plz: "02799"
  },
  {
    lati: 49.8905790286, longi: 9.95620501792,
    name: "Rimpar",
    plz: "97222"
  },
  {
    lati: 52.9635509759, longi: 10.4228023816,
    name: "Gerdau",
    plz: "29581"
  },
  {
    lati: 50.654299354, longi: 11.6843816367,
    name: "Ranis",
    plz: "07389"
  },
  {
    lati: 48.2312114967, longi: 11.7958026867,
    name: "Finsing",
    plz: "85464"
  },
  {
    lati: 48.836921007, longi: 12.7198101669,
    name: "Straßkirchen",
    plz: "94342"
  },
  {
    lati: 48.7726239408, longi: 12.5242046382,
    name: "Leiblfing",
    plz: "94339"
  },
  {
    lati: 48.5619753346, longi: 10.9153895364,
    name: "Thierhaupten",
    plz: "86672"
  },
  {
    lati: 50.0002063081, longi: 10.1674879018,
    name: "Bergrheinfeld",
    plz: "97493"
  },
  {
    lati: 50.5378720206, longi: 8.25594911452,
    name: "Löhnberg",
    plz: "35792"
  },
  {
    lati: 51.4577336189, longi: 14.0244962244,
    name: "Senftenberg",
    plz: "01996"
  },
  {
    lati: 49.1109098045, longi: 10.7283876039,
    name: "Gunzenhausen",
    plz: "91710"
  },
  {
    lati: 48.0728596087, longi: 11.7604234348,
    name: "Grasbrunn",
    plz: "85630"
  },
  {
    lati: 47.9269357082, longi: 11.5108512267,
    name: "Egling",
    plz: "82544"
  },
  {
    lati: 51.3155727398, longi: 9.5865739813,
    name: "Niestetal",
    plz: "34266"
  },
  {
    lati: 49.4401548615, longi: 7.56739563409,
    name: "Ramstein-Miesenbach",
    plz: "66877"
  },
  {
    lati: 53.2085011372, longi: 7.78615364925,
    name: "Apen",
    plz: "26689"
  },
  {
    lati: 52.0919171712, longi: 9.39847771892,
    name: "Hameln",
    plz: "31789"
  },
  {
    lati: 52.9524028974, longi: 11.9500757749,
    name: "Legde/Quitzöbel, Bad Wilsnack",
    plz: "19336"
  },
  {
    lati: 50.2048611385, longi: 9.19344680005,
    name: "Gelnhausen",
    plz: "63571"
  },
  {
    lati: 50.0143859942, longi: 8.47176288913,
    name: "Raunheim",
    plz: "65479"
  },
  {
    lati: 49.2309723908, longi: 8.53637421738,
    name: "Waghäusel",
    plz: "68753"
  },
  {
    lati: 48.6827051351, longi: 12.480169565,
    name: "Moosthenning",
    plz: "84164"
  },
  {
    lati: 49.2552577965, longi: 13.0010607765,
    name: "Neukirchen b. Hl. Blut",
    plz: "93453"
  },
  {
    lati: 47.7051794544, longi: 9.19527174202,
    name: "Konstanz",
    plz: "78465"
  },
  {
    lati: 50.4970877011, longi: 7.26588019446,
    name: "Bad Breisig, Waldorf, Gönnersdorf",
    plz: "53498"
  },
  {
    lati: 54.6410800416, longi: 9.46338771605,
    name: "Sieverstedt",
    plz: "24885"
  },
  {
    lati: 48.272275982, longi: 10.03686349,
    name: "Illerrieden",
    plz: "89186"
  },
  {
    lati: 48.5662855261, longi: 10.1545828918,
    name: "Herbrechtingen",
    plz: "89542"
  },
  {
    lati: 49.8217735838, longi: 9.76272710942,
    name: "Greußenheim",
    plz: "97259"
  },
  {
    lati: 48.5013660795, longi: 10.4880350246,
    name: "Glött",
    plz: "89353"
  },
  {
    lati: 51.8598781661, longi: 10.4712893857,
    name: "Goslar",
    plz: "38644"
  },
  {
    lati: 51.6883462284, longi: 7.8099464198,
    name: "Hamm",
    plz: "59065"
  },
  {
    lati: 49.6133898508, longi: 8.68309418003,
    name: "Heppenheim (Bergstraße)",
    plz: "64646"
  },
  {
    lati: 50.4365821361, longi: 8.29175463575,
    name: "Weinbach",
    plz: "35796"
  },
  {
    lati: 49.5240003958, longi: 8.3934242962,
    name: "Ludwigshafen am Rhein",
    plz: "67069"
  },
  {
    lati: 49.1658353658, longi: 9.91251165552,
    name: "Ilshofen",
    plz: "74532"
  },
  {
    lati: 50.1749421996, longi: 9.31419372494,
    name: "Biebergemünd",
    plz: "63599"
  },
  {
    lati: 47.7494331708, longi: 10.608707234,
    name: "Marktoberdorf",
    plz: "87616"
  },
  {
    lati: 48.5818348803, longi: 12.9283392767,
    name: "Roßbach",
    plz: "94439"
  },
  {
    lati: 52.38563023, longi: 13.6338295415,
    name: "Berlin Schmöckwitz",
    plz: "12527"
  },
  {
    lati: 50.3945366259, longi: 8.00784565608,
    name: "Burgschwalbach",
    plz: "65558"
  },
  {
    lati: 50.5887359084, longi: 7.27136590116,
    name: "Breitscheid, Dattenberg, Hausen, Hümmerich, Kasbach-Ohlenberg, Roßbach u",
    plz: "53547"
  },
  {
    lati: 47.8310812734, longi: 7.57026902859,
    name: "Neuenburg am Rhein",
    plz: "79395"
  },
  {
    lati: 49.5653544696, longi: 8.72364820999,
    name: "Birkenau",
    plz: "69488"
  },
  {
    lati: 50.6811955732, longi: 10.9709008386,
    name: "Langewiesen",
    plz: "98704"
  },
  {
    lati: 51.5758491795, longi: 7.28904211269,
    name: "Castrop-Rauxel",
    plz: "44579"
  },
  {
    lati: 50.9004815865, longi: 7.40692490753,
    name: "Much",
    plz: "53804"
  },
  {
    lati: 49.6086105615, longi: 6.44210775336,
    name: "Wincheringen",
    plz: "54457"
  },
  {
    lati: 50.5946639455, longi: 8.25677113769,
    name: "Greifenstein",
    plz: "35753"
  },
  {
    lati: 48.7301654832, longi: 8.78552231629,
    name: "Althengstett",
    plz: "75382"
  },
  {
    lati: 48.2481089857, longi: 11.5610909975,
    name: "Oberschleißheim",
    plz: "85764"
  },
  {
    lati: 49.4707307733, longi: 11.2517980437,
    name: "Röthenbach an der Pegnitz",
    plz: "90552"
  },
  {
    lati: 49.6885492624, longi: 9.66019402307,
    name: "Werbach",
    plz: "97956"
  },
  {
    lati: 47.6980803402, longi: 9.47183726902,
    name: "Friedrichshafen",
    plz: "88048"
  },
  {
    lati: 51.1998641439, longi: 9.50469198646,
    name: "Guxhagen",
    plz: "34302"
  },
  {
    lati: 51.2688337723, longi: 9.55308010905,
    name: "Lohfelden",
    plz: "34253"
  },
  {
    lati: 53.0872673486, longi: 7.69397292645,
    name: "Saterland",
    plz: "26683"
  },
  {
    lati: 51.2750338345, longi: 6.74871820185,
    name: "Düsseldorf",
    plz: "40474"
  },
  {
    lati: 49.6989766602, longi: 6.53509391273,
    name: "Wasserliesch",
    plz: "54332"
  },
  {
    lati: 50.8263551761, longi: 9.82318213947,
    name: "Schenklengsfeld",
    plz: "36277"
  },
  {
    lati: 53.6692060245, longi: 9.96505415184,
    name: "Norderstedt",
    plz: "22848"
  },
  {
    lati: 49.7705515775, longi: 10.1365805704,
    name: "Albertshofen",
    plz: "97320"
  },
  {
    lati: 50.9308957698, longi: 10.8915035638,
    name: "Nesse-Apfelstädt, Nottleben",
    plz: "99192"
  },
  {
    lati: 49.3419868571, longi: 10.9278943094,
    name: "Rohr",
    plz: "91189"
  },
  {
    lati: 51.0458669924, longi: 7.57507958513,
    name: "Gummersbach",
    plz: "51647"
  },
  {
    lati: 50.0387154498, longi: 7.28590074733,
    name: "Blankenrath u.a.",
    plz: "56865"
  },
  {
    lati: 48.3985238876, longi: 10.2015539271,
    name: "Bibertal",
    plz: "89346"
  },
  {
    lati: 52.2846522839, longi: 11.856585661,
    name: "Burg",
    plz: "39288"
  },
  {
    lati: 49.1350281234, longi: 10.6311454876,
    name: "Arberg",
    plz: "91722"
  },
  {
    lati: 49.9150353499, longi: 10.4542696262,
    name: "Michelau i. Steigerwald, Hundelshausen",
    plz: "97513"
  },
  {
    lati: 49.7353280693, longi: 9.23360529471,
    name: "Großheubach",
    plz: "63920"
  },
  {
    lati: 49.239331853, longi: 8.32324938123,
    name: "Westheim (Pfalz)",
    plz: "67368"
  },
  {
    lati: 50.567849318, longi: 8.53080713531,
    name: "Wetzlar Garbeinheim",
    plz: "35583"
  },
  {
    lati: 51.6679131777, longi: 11.4550970021,
    name: "Hettstedt, Endorf",
    plz: "06333"
  },
  {
    lati: 52.4397478892, longi: 7.12933186359,
    name: "Nordhorn",
    plz: "48531"
  },
  {
    lati: 48.1111208434, longi: 8.98912166368,
    name: "Schwenningen",
    plz: "72477"
  },
  {
    lati: 52.1597716998, longi: 10.330036974,
    name: "Salzgitter",
    plz: "38226"
  },
  {
    lati: 52.5905826366, longi: 13.3284571763,
    name: "Berlin Reinickendorf",
    plz: "13437"
  },
  {
    lati: 48.5510444267, longi: 7.89561618912,
    name: "Willstätt",
    plz: "77731"
  },
  {
    lati: 53.4252586821, longi: 10.0463037842,
    name: "Seevetal",
    plz: "21217"
  },
  {
    lati: 53.6101567331, longi: 9.61964224797,
    name: "Hetlingen",
    plz: "25491"
  },
  {
    lati: 51.1827615684, longi: 13.5612680638,
    name: "Weinböhla",
    plz: "01689"
  },
  {
    lati: 48.1605335603, longi: 7.79431855254,
    name: "Malterdingen",
    plz: "79364"
  },
  {
    lati: 52.0924082282, longi: 8.74149228941,
    name: "Bad Salzuflen",
    plz: "32105"
  },
  {
    lati: 48.2418737773, longi: 8.74796040939,
    name: "Dautmergen",
    plz: "72356"
  },
  {
    lati: 48.9334500946, longi: 9.75447972761,
    name: "Gschwend",
    plz: "74417"
  },
  {
    lati: 51.4456168981, longi: 10.3514350368,
    name: "Worbis",
    plz: "37339"
  },
  {
    lati: 51.9546060422, longi: 10.5491949141,
    name: "Goslar",
    plz: "38690"
  },
  {
    lati: 52.4373457563, longi: 11.7770768589,
    name: "Tangerhütte u.a.",
    plz: "39517"
  },
  {
    lati: 51.8768600465, longi: 14.4123168989,
    name: "Peitz",
    plz: "03185"
  },
  {
    lati: 48.5282778423, longi: 9.28942175508,
    name: "Metzingen",
    plz: "72555"
  },
  {
    lati: 49.9748989014, longi: 9.05062561098,
    name: "Stockstadt am Main",
    plz: "63811"
  },
  {
    lati: 48.866525035, longi: 9.1865356744,
    name: "Kornwestheim",
    plz: "70806"
  },
  {
    lati: 52.5870493738, longi: 8.98227274784,
    name: "Steyerberg",
    plz: "31595"
  },
  {
    lati: 50.5284395781, longi: 7.33316995929,
    name: "Bad Hönningen",
    plz: "53557"
  },
  {
    lati: 52.1986834357, longi: 9.89095830553,
    name: "Giesen",
    plz: "31180"
  },
  {
    lati: 47.9514954951, longi: 10.4760256139,
    name: "Unteregg",
    plz: "87782"
  },
  {
    lati: 52.4854637044, longi: 13.4392232951,
    name: "Berlin Neukölln",
    plz: "12045"
  },
  {
    lati: 48.4685038966, longi: 11.7680055709,
    name: "Zolling",
    plz: "85406"
  },
  {
    lati: 54.2421630977, longi: 12.2344475794,
    name: "Rostock, Graal-Müritz",
    plz: "18181"
  },
  {
    lati: 47.7644662957, longi: 10.4914119479,
    name: "Unterthingau",
    plz: "87647"
  },
  {
    lati: 49.9374713047, longi: 11.281730373,
    name: "Hollfeld",
    plz: "96142"
  },
  {
    lati: 48.5681866944, longi: 12.6325715055,
    name: "Reisbach",
    plz: "94419"
  },
  {
    lati: 50.8154603392, longi: 12.983230773,
    name: "Chemnitz",
    plz: "09127"
  },
  {
    lati: 48.8790546884, longi: 9.13334737592,
    name: "Möglingen",
    plz: "71696"
  },
  {
    lati: 47.6629482463, longi: 9.16972711045,
    name: "Konstanz",
    plz: "78462"
  },
  {
    lati: 49.226339922, longi: 8.61351572546,
    name: "Kronau",
    plz: "76709"
  },
  {
    lati: 51.219301638, longi: 9.30704165181,
    name: "Niedenstein",
    plz: "34305"
  },
  {
    lati: 50.9775659863, longi: 12.4374063194,
    name: "Altenburg",
    plz: "04600"
  },
  {
    lati: 48.1723731934, longi: 11.5771764484,
    name: "München",
    plz: "80804"
  },
  {
    lati: 50.3839597345, longi: 7.57694933461,
    name: "Koblenz",
    plz: "56070"
  },
  {
    lati: 47.7217955069, longi: 8.29618642094,
    name: "Ühlingen-Birkendorf",
    plz: "79777"
  },
  {
    lati: 49.6452992489, longi: 8.66141651235,
    name: "Heppenheim (Bergstraße)",
    plz: "64646"
  },
  {
    lati: 49.2221548394, longi: 7.10908682736,
    name: "Saarbrücken",
    plz: "66131"
  },
  {
    lati: 52.5167612556, longi: 7.33149743823,
    name: "Lingen",
    plz: "49809"
  },
  {
    lati: 49.9324597404, longi: 6.3052188372,
    name: "Körperich u.a.",
    plz: "54675"
  },
  {
    lati: 49.6674156297, longi: 11.0657933163,
    name: "Poxdorf",
    plz: "91099"
  },
  {
    lati: 48.8892113337, longi: 11.1954059743,
    name: "Eichstätt",
    plz: "85072"
  },
  {
    lati: 49.8459959391, longi: 11.3285206524,
    name: "Waischenfeld",
    plz: "91344"
  },
  {
    lati: 52.0805880087, longi: 11.6155952691,
    name: "Magdeburg",
    plz: "39120"
  },
  {
    lati: 51.7171265516, longi: 9.99577238553,
    name: "Northeim",
    plz: "37154"
  },
  {
    lati: 53.6592893124, longi: 10.0216520164,
    name: "Hamburg",
    plz: "22415"
  },
  {
    lati: 49.4078005958, longi: 10.4354814411,
    name: "Oberdachstetten",
    plz: "91617"
  },
  {
    lati: 51.2766469377, longi: 7.14820371497,
    name: "Wuppertal",
    plz: "42109"
  },
  {
    lati: 51.3442556665, longi: 6.85922308009,
    name: "Ratingen",
    plz: "40885"
  },
  {
    lati: 48.587920642, longi: 9.89957596448,
    name: "Amstetten",
    plz: "73340"
  },
  {
    lati: 49.0909685047, longi: 12.9155660376,
    name: "Viechtach",
    plz: "94234"
  },
  {
    lati: 48.0034522848, longi: 10.2173279224,
    name: "Trunkelsberg",
    plz: "87779"
  },
  {
    lati: 48.3404250519, longi: 10.582381699,
    name: "Dinkelscherben",
    plz: "86424"
  },
  {
    lati: 52.5153562012, longi: 13.2381957975,
    name: "Berlin Westend",
    plz: "14053"
  },
  {
    lati: 49.8391788843, longi: 8.10727887743,
    name: "Wörrstadt",
    plz: "55286"
  },
  {
    lati: 51.5765197471, longi: 9.25584383055,
    name: "Borgentreich",
    plz: "34434"
  },
  {
    lati: 48.7793257071, longi: 9.25177305018,
    name: "Stuttgart",
    plz: "70327"
  },
  {
    lati: 47.9744953696, longi: 11.4581752266,
    name: "Schäftlarn",
    plz: "82067"
  },
  {
    lati: 50.3070422794, longi: 11.2867817321,
    name: "Stockheim",
    plz: "96342"
  },
  {
    lati: 51.4952012308, longi: 9.60523252508,
    name: "Reinhardshagen",
    plz: "34359"
  },
  {
    lati: 49.8550240942, longi: 12.224715681,
    name: "Falkenberg",
    plz: "95685"
  },
  {
    lati: 50.0338641389, longi: 7.2399757015,
    name: "Bullay, Alf, Zell",
    plz: "56859"
  },
  {
    lati: 50.7070924152, longi: 7.97196718134,
    name: "Emmerzhausen, Niederdreisbach, Steinebach",
    plz: "57520"
  },
  {
    lati: 49.6172421372, longi: 7.55603959497,
    name: "Offenbach-Hundheim",
    plz: "67749"
  },
  {
    lati: 50.1056387961, longi: 7.65843989357,
    name: "Niederburg",
    plz: "55432"
  },
  {
    lati: 52.9286625479, longi: 9.13659609095,
    name: "Blender",
    plz: "27337"
  },
  {
    lati: 52.1557748251, longi: 8.63567084149,
    name: "Hiddenhausen",
    plz: "32120"
  },
  {
    lati: 50.0971837543, longi: 8.67062243161,
    name: "Frankfurt am Main",
    plz: "60596"
  },
  {
    lati: 52.6967672553, longi: 8.89721684238,
    name: "Maasen, Mellinghausen",
    plz: "27249"
  },
  {
    lati: 51.9826515964, longi: 10.2523343209,
    name: "Hahausen, Lutter, Wallmoden",
    plz: "38729"
  },
  {
    lati: 52.5398366415, longi: 13.2991821424,
    name: "Berlin Charlottenburg-Nord",
    plz: "13627"
  },
  {
    lati: 49.5356784692, longi: 9.17383736596,
    name: "Mudau",
    plz: "69427"
  },
  {
    lati: 47.9673535699, longi: 7.90143809625,
    name: "Freiburg im Breisgau",
    plz: "79117"
  },
  {
    lati: 52.2447680917, longi: 8.04971704651,
    name: "Osnabrück",
    plz: "49082"
  },
  {
    lati: 48.959634367, longi: 10.5440060892,
    name: "Ehingen a. Ries",
    plz: "86741"
  },
  {
    lati: 53.1875265493, longi: 10.7217178309,
    name: "Dahlenburg",
    plz: "21368"
  },
  {
    lati: 50.3358869424, longi: 7.62778264401,
    name: "Koblenz",
    plz: "56076"
  },
  {
    lati: 48.201727332, longi: 7.67708543395,
    name: "Weisweil",
    plz: "79367"
  },
  {
    lati: 49.5723985289, longi: 7.69511326642,
    name: "Niederkirchen",
    plz: "67700"
  },
  {
    lati: 49.3191425426, longi: 8.34461353362,
    name: "Hanhofen",
    plz: "67374"
  },
  {
    lati: 49.747743261, longi: 8.24396508446,
    name: "Dittelsheim-Heßloch",
    plz: "67596"
  },
  {
    lati: 50.7922425398, longi: 7.10881324769,
    name: "Troisdorf",
    plz: "53844"
  },
  {
    lati: 51.0550111117, longi: 12.3406263065,
    name: "Meuselwitz",
    plz: "04610"
  },
  {
    lati: 50.3855161627, longi: 12.4702881329,
    name: "Klingenthal/Sa.",
    plz: "08248"
  },
  {
    lati: 48.7628902603, longi: 9.38755420777,
    name: "Aichwald",
    plz: "73773"
  },
  {
    lati: 51.1081984033, longi: 14.9535457737,
    name: "Görlitz",
    plz: "02827"
  },
  {
    lati: 50.3875743341, longi: 9.77007990281,
    name: "Motten",
    plz: "97786"
  },
  {
    lati: 53.4196195466, longi: 10.7704002881,
    name: "Boizenburg/ Elbe",
    plz: "19258"
  },
  {
    lati: 48.2489726767, longi: 10.8041171795,
    name: "Wehringen",
    plz: "86517"
  },
  {
    lati: 52.1701472654, longi: 8.16603187995,
    name: "Hilter",
    plz: "49176"
  },
  {
    lati: 48.0777741025, longi: 8.35025225393,
    name: "Unterkirnach",
    plz: "78089"
  },
  {
    lati: 47.6315760863, longi: 8.41809421661,
    name: "Klettgau",
    plz: "79771"
  },
  {
    lati: 49.5418310638, longi: 8.67463925712,
    name: "Weinheim",
    plz: "69469"
  },
  {
    lati: 47.7002239552, longi: 8.68938802623,
    name: "Büsingen am Hochrhein",
    plz: "78266"
  },
  {
    lati: 52.701905456, longi: 8.54279491418,
    name: "Barnstorf, Eydelstedt, Drentwede",
    plz: "49406"
  },
  {
    lati: 49.0134574166, longi: 8.25273159246,
    name: "Hagenbach",
    plz: "76767"
  },
  {
    lati: 53.5841981582, longi: 9.85462293027,
    name: "Hamburg",
    plz: "22549"
  },
  {
    lati: 48.9257550044, longi: 11.1068092969,
    name: "Schernfeld",
    plz: "85132"
  },
  {
    lati: 50.6447610728, longi: 11.3526229772,
    name: "Saalfeld/Saale, Wittgendorf",
    plz: "07318"
  },
  {
    lati: 48.1234427007, longi: 10.947368799,
    name: "Weil",
    plz: "86947"
  },
  {
    lati: 53.9412824994, longi: 10.311174212,
    name: "Bad Segeberg",
    plz: "23795"
  },
  {
    lati: 50.3806966161, longi: 10.3235683085,
    name: "Mellrichstadt",
    plz: "97638"
  },
  {
    lati: 49.6558637112, longi: 10.4072062052,
    name: "Markt Bibart",
    plz: "91477"
  },
  {
    lati: 50.6419331696, longi: 7.65998876775,
    name: "Steimel",
    plz: "57614"
  },
  {
    lati: 48.4848044284, longi: 8.02872000317,
    name: "Durbach",
    plz: "77770"
  },
  {
    lati: 52.6481370267, longi: 12.0864691924,
    name: "Sandau",
    plz: "39524"
  },
  {
    lati: 54.0282428615, longi: 12.0857588101,
    name: "Rostock, Papendorf u.a.",
    plz: "18059"
  },
  {
    lati: 49.0278545454, longi: 12.3696980522,
    name: "Wiesent",
    plz: "93109"
  },
  {
    lati: 47.5599659303, longi: 11.1440652384,
    name: "Oberau",
    plz: "82496"
  },
  {
    lati: 48.5838922898, longi: 10.9662885366,
    name: "Baar",
    plz: "86674"
  },
  {
    lati: 49.5049538804, longi: 11.5259072826,
    name: "Pommelsbrunn",
    plz: "91224"
  },
  {
    lati: 52.5422581791, longi: 13.2662765024,
    name: "Berlin Siemensstadt",
    plz: "13629"
  },
  {
    lati: 52.593178125, longi: 13.383449686,
    name: "Berlin Rosenthal",
    plz: "13158"
  },
  {
    lati: 52.9616518157, longi: 7.24046934197,
    name: "Dersum",
    plz: "26906"
  },
  {
    lati: 49.9514011778, longi: 8.46851267283,
    name: "Nauheim",
    plz: "64569"
  },
  {
    lati: 49.8003394912, longi: 8.58830858983,
    name: "Pfungstadt",
    plz: "64319"
  },
  {
    lati: 52.6097208192, longi: 8.36384898578,
    name: "Diepholz",
    plz: "49356"
  },
  {
    lati: 51.2659322996, longi: 10.2462300138,
    name: "Küllstedt",
    plz: "37359"
  },
  {
    lati: 48.0098355859, longi: 10.4340052627,
    name: "Stetten",
    plz: "87778"
  },
  {
    lati: 53.5789432663, longi: 9.95239237264,
    name: "Hamburg",
    plz: "20255"
  },
  {
    lati: 50.0966418238, longi: 9.2647047547,
    name: "Schöllkrippen, Blankenbach",
    plz: "63825"
  },
  {
    lati: 47.9390394702, longi: 7.82367149193,
    name: "Wittnau",
    plz: "79299"
  },
  {
    lati: 50.5824416017, longi: 9.73842895212,
    name: "Petersberg",
    plz: "36100"
  },
  {
    lati: 48.0753410704, longi: 8.44170275141,
    name: "Villingen-Schwenningen",
    plz: "78048"
  },
  {
    lati: 52.6715463942, longi: 10.3445090482,
    name: "Eldingen",
    plz: "29351"
  },
  {
    lati: 53.4395762635, longi: 10.4016254983,
    name: "Geesthacht",
    plz: "21502"
  },
  {
    lati: 49.8961354267, longi: 7.94580875374,
    name: "Gensingen",
    plz: "55457"
  },
  {
    lati: 52.5722248162, longi: 8.12429643718,
    name: "Holdorf",
    plz: "49451"
  },
  {
    lati: 48.9598268288, longi: 8.48041382963,
    name: "Karlsruhe",
    plz: "76228"
  },
  {
    lati: 49.693030465, longi: 8.62373527596,
    name: "Bensheim",
    plz: "64625"
  },
  {
    lati: 48.0981979856, longi: 8.8644327346,
    name: "Königsheim",
    plz: "78598"
  },
  {
    lati: 50.5021382792, longi: 7.63758032159,
    name: "Großmaischeid",
    plz: "56276"
  },
  {
    lati: 52.1434663777, longi: 10.9555107327,
    name: "Schöningen",
    plz: "38364"
  },
  {
    lati: 51.6341408353, longi: 10.3388782318,
    name: "Herzberg, Elbingerode, Hörden",
    plz: "37412"
  },
  {
    lati: 48.9352675722, longi: 10.3682202959,
    name: "Unterschneidheim",
    plz: "73485"
  },
  {
    lati: 49.7245632368, longi: 9.74794312367,
    name: "Altertheim",
    plz: "97237"
  },
  {
    lati: 50.7302013818, longi: 9.43611935749,
    name: "Grebenau",
    plz: "36323"
  },
  {
    lati: 53.5882621038, longi: 9.47454894357,
    name: "Stade",
    plz: "21680"
  },
  {
    lati: 53.4385547508, longi: 9.60218878296,
    name: "Apensen",
    plz: "21641"
  },
  {
    lati: 48.4866847205, longi: 11.0610049514,
    name: "Hollenbach",
    plz: "86568"
  },
  {
    lati: 49.4088789795, longi: 11.3055320818,
    name: "Winkelhaid",
    plz: "90610"
  },
  {
    lati: 50.7705457363, longi: 12.8633116454,
    name: "Neukirchen/Erzgeb.",
    plz: "09221"
  },
  {
    lati: 51.3612971645, longi: 12.4266302241,
    name: "Leipzig",
    plz: "04347"
  },
  {
    lati: 47.9018198994, longi: 12.7179230988,
    name: "Wonneberg",
    plz: "83379"
  },
  {
    lati: 52.4340350383, longi: 13.2944343306,
    name: "Berlin Lichtenfelde",
    plz: "12205"
  },
  {
    lati: 48.342738298, longi: 10.8932355746,
    name: "Augsburg",
    plz: "86159"
  },
  {
    lati: 48.9532557156, longi: 11.8503131594,
    name: "Ihrlerstein",
    plz: "93346"
  },
  {
    lati: 50.0822065596, longi: 8.20775518312,
    name: "Wiesbaden",
    plz: "65197"
  },
  {
    lati: 53.6162275945, longi: 7.28467455678,
    name: "Hage, Halbemond u.a.",
    plz: "26524"
  },
  {
    lati: 49.1157152909, longi: 8.23988202821,
    name: "Hatzenbühl",
    plz: "76770"
  },
  {
    lati: 49.3504908433, longi: 8.26629559083,
    name: "Haßloch",
    plz: "67454"
  },
  {
    lati: 48.7449096476, longi: 9.22129776325,
    name: "Stuttgart",
    plz: "70619"
  },
  {
    lati: 50.4675874351, longi: 7.7900480842,
    name: "Wirges, Stadt",
    plz: "56422"
  },
  {
    lati: 52.1381920012, longi: 10.6065580339,
    name: "Denkte",
    plz: "38321"
  },
  {
    lati: 50.670548521, longi: 7.05726171719,
    name: "Bonn",
    plz: "53125"
  },
  {
    lati: 51.663168107, longi: 6.657579982,
    name: "Wesel",
    plz: "46485"
  },
  {
    lati: 51.2702159358, longi: 12.574792236,
    name: "Naunhof",
    plz: "04683"
  },
  {
    lati: 52.3575648253, longi: 14.3736468758,
    name: "Treplin, Jacobsdorf, Frankfurt (Oder)",
    plz: "15236"
  },
  {
    lati: 50.3895571917, longi: 7.49869584751,
    name: "Mülheim-Kärlich",
    plz: "56218"
  },
  {
    lati: 50.4660735186, longi: 7.76209000443,
    name: "Mogendorf, Ebernhahn, Staudt u.a.",
    plz: "56424"
  },
  {
    lati: 51.9186789213, longi: 7.84240795955,
    name: "Everswinkel",
    plz: "48351"
  },
  {
    lati: 50.8497618541, longi: 11.1915025924,
    name: "Kranichfeld u.a.",
    plz: "99448"
  },
  {
    lati: 52.3835641972, longi: 12.8847508374,
    name: "Werder/ Havel",
    plz: "14542"
  },
  {
    lati: 53.9824362299, longi: 10.7114973888,
    name: "Pansdorf",
    plz: "23689"
  },
  {
    lati: 54.1483991726, longi: 12.0167894871,
    name: "Elmenhorst/Lichtenhagen, Rostock",
    plz: "18107"
  },
  {
    lati: 51.096995949, longi: 12.403118665,
    name: "Regis-Breitingen",
    plz: "04565"
  },
  {
    lati: 49.8541299064, longi: 6.36278240325,
    name: "Bollendorf",
    plz: "54669"
  },
  {
    lati: 49.0897878507, longi: 7.82954658617,
    name: "Bruchweiler-Bärenbach u.a.",
    plz: "76891"
  },
  {
    lati: 49.0953948082, longi: 8.05150385473,
    name: "Bad Bergzabern u.a.",
    plz: "76887"
  },
  {
    lati: 48.3829863774, longi: 8.15548072754,
    name: "Oberharmersbach",
    plz: "77784"
  },
  {
    lati: 49.7500219135, longi: 8.40090697262,
    name: "Eich",
    plz: "67575"
  },
  {
    lati: 51.3170139722, longi: 9.46435451521,
    name: "Kassel",
    plz: "34119"
  },
  {
    lati: 49.0722171161, longi: 10.2070275053,
    name: "Fichtenau",
    plz: "74579"
  },
  {
    lati: 53.1319816319, longi: 10.3034325256,
    name: "Betzendorf",
    plz: "21386"
  },
  {
    lati: 49.4813496758, longi: 12.5639753876,
    name: "Weiding",
    plz: "92557"
  },
  {
    lati: 53.8361773762, longi: 13.6974978159,
    name: "Anklam",
    plz: "17389"
  },
  {
    lati: 49.9662045789, longi: 12.4249674673,
    name: "Neualbenreuth",
    plz: "95698"
  },
  {
    lati: 48.5748641261, longi: 10.5312743072,
    name: "Dillingen a.d. Donau",
    plz: "89407"
  },
  {
    lati: 50.2231294903, longi: 10.9242026605,
    name: "Ahorn",
    plz: "96482"
  },
  {
    lati: 50.5824242582, longi: 11.4922910277,
    name: "Kaulsdorf",
    plz: "07338"
  },
  {
    lati: 51.1466740674, longi: 7.01797645209,
    name: "Solingen",
    plz: "42699"
  },
  {
    lati: 51.3381435955, longi: 8.24766552022,
    name: "Meschede",
    plz: "59872"
  },
  {
    lati: 51.0728576487, longi: 8.78414883188,
    name: "Frankenberg",
    plz: "35066"
  },
  {
    lati: 54.4049637237, longi: 9.48829153809,
    name: "Kropp u.a.",
    plz: "24848"
  },
  {
    lati: 53.5751638582, longi: 11.6917138191,
    name: "Crivitz, Friedrichsruhe u.a.",
    plz: "19089"
  },
  {
    lati: 52.4731509336, longi: 13.0078317256,
    name: "Potsdam",
    plz: "14476"
  },
  {
    lati: 52.478491535, longi: 13.3790715758,
    name: "Berlin Tempelhof",
    plz: "12101"
  },
  {
    lati: 52.4441588287, longi: 13.7029390265,
    name: "Berlin Rahnsdorf",
    plz: "12589"
  },
  {
    lati: 51.5808599031, longi: 8.18658859597,
    name: "Bad Sassendorf",
    plz: "59505"
  },
  {
    lati: 48.5365412789, longi: 9.19577637705,
    name: "Reutlingen",
    plz: "72768"
  },
  {
    lati: 50.9233658344, longi: 10.0854946938,
    name: "Berka/ Werra",
    plz: "99837"
  },
  {
    lati: 50.4630886291, longi: 10.2746771901,
    name: "Oberstreu",
    plz: "97640"
  },
  {
    lati: 48.0824743195, longi: 11.8196624448,
    name: "Zorneding",
    plz: "85604"
  },
  {
    lati: 51.8304720478, longi: 6.70708289886,
    name: "Rhede",
    plz: "46414"
  },
  {
    lati: 51.2846807961, longi: 7.68837531122,
    name: "Altena",
    plz: "58762"
  },
  {
    lati: 48.6390541007, longi: 8.09969177456,
    name: "Sasbach",
    plz: "77880"
  },
  {
    lati: 49.1458426365, longi: 9.11223094353,
    name: "Leingarten",
    plz: "74211"
  },
  {
    lati: 52.4081096107, longi: 10.950537851,
    name: "Velpke",
    plz: "38458"
  },
  {
    lati: 51.534078438, longi: 9.93347507357,
    name: "Göttingen",
    plz: "37073"
  },
  {
    lati: 49.5471238471, longi: 10.5398668007,
    name: "Dietersheim",
    plz: "91463"
  },
  {
    lati: 48.1621588931, longi: 11.6223313744,
    name: "München",
    plz: "81925"
  },
  {
    lati: 50.8948959788, longi: 13.548234595,
    name: "Klingenberg",
    plz: "01774"
  },
  {
    lati: 47.9904471409, longi: 11.6918101366,
    name: "Brunnthal",
    plz: "85649"
  },
  {
    lati: 53.2913611537, longi: 10.7281858411,
    name: "Bleckede",
    plz: "21354"
  },
  {
    lati: 48.0828450157, longi: 9.61057058991,
    name: "Bad Buchau",
    plz: "88422"
  },
  {
    lati: 51.0540673033, longi: 6.58068187802,
    name: "Grevenbroich",
    plz: "41517"
  },
  {
    lati: 48.4494312079, longi: 7.97204242605,
    name: "Ortenberg",
    plz: "77799"
  },
  {
    lati: 53.2497010086, longi: 7.64410783276,
    name: "Filsum",
    plz: "26849"
  },
  {
    lati: 47.8884125188, longi: 9.09935300028,
    name: "Hohenfels",
    plz: "78355"
  },
  {
    lati: 47.7984120387, longi: 9.10270123741,
    name: "Sipplingen",
    plz: "78354"
  },
  {
    lati: 49.2469941773, longi: 9.17043749135,
    name: "Offenau",
    plz: "74254"
  },
  {
    lati: 48.6074815424, longi: 9.21657769425,
    name: "Schlaitdorf",
    plz: "72667"
  },
  {
    lati: 50.2867484105, longi: 8.68565420293,
    name: "Rosbach v.d. Höhe",
    plz: "61191"
  },
  {
    lati: 53.2904600851, longi: 12.2031617369,
    name: "Meyenburg, Kümmernitztal u.a.",
    plz: "16945"
  },
  {
    lati: 51.0251269323, longi: 12.2657406599,
    name: "Elsteraue",
    plz: "06729"
  },
  {
    lati: 54.171926391, longi: 13.4100558583,
    name: "Greifswald",
    plz: "17493"
  },
  {
    lati: 48.7771794288, longi: 13.4411066369,
    name: "Perlesreut",
    plz: "94157"
  },
  {
    lati: 50.995517355, longi: 13.8475594497,
    name: "Dresden",
    plz: "01259"
  },
  {
    lati: 51.4264744968, longi: 11.986976024,
    name: "Halle/ Saale",
    plz: "06132"
  },
  {
    lati: 52.6142314714, longi: 12.3771024804,
    name: "Milower Land, Schollene, Nennhausen u.a.",
    plz: "14715"
  },
  {
    lati: 48.6567382444, longi: 7.9353834999,
    name: "Rheinau",
    plz: "77866"
  },
  {
    lati: 50.9121812086, longi: 6.91144640863,
    name: "Köln",
    plz: "50937"
  },
  {
    lati: 49.7100992491, longi: 7.67146195875,
    name: "Meisenheim",
    plz: "55590"
  },
  {
    lati: 53.8404932476, longi: 13.4722750382,
    name: "Krien u.a.",
    plz: "17391"
  },
  {
    lati: 49.2943931101, longi: 7.47460373685,
    name: "Battweiler u.a.",
    plz: "66484"
  },
  {
    lati: 51.3703281226, longi: 7.61948608665,
    name: "Iserlohn",
    plz: "58642"
  },
  {
    lati: 49.3019789719, longi: 7.73204316625,
    name: "Heltersberg",
    plz: "67716"
  },
  {
    lati: 50.9111676327, longi: 11.1443737869,
    name: "Rockhausen, Klettbach",
    plz: "99102"
  },
  {
    lati: 49.3827718131, longi: 11.2332723772,
    name: "Nürnberg-Feucht, Feuchter Forst",
    plz: "90537"
  },
  {
    lati: 51.1967259719, longi: 11.5335617121,
    name: "Bad Bibra, Finne u.a.",
    plz: "06647"
  },
  {
    lati: 53.261748604, longi: 7.92264729326,
    name: "Westerstede",
    plz: "26655"
  },
  {
    lati: 54.5284442992, longi: 9.0891544398,
    name: "Horstedt",
    plz: "25860"
  },
  {
    lati: 52.4661681416, longi: 9.19265569061,
    name: "Rehburg-Loccum",
    plz: "31547"
  },
  {
    lati: 54.3429343852, longi: 9.21868748554,
    name: "Süderstapel",
    plz: "25879"
  },
  {
    lati: 51.6282989214, longi: 10.6458050617,
    name: "Zorge",
    plz: "37449"
  },
  {
    lati: 53.8915685571, longi: 10.7508258363,
    name: "Lübeck Schlutup/St. Gertrud",
    plz: "23568"
  },
  {
    lati: 52.2607786859, longi: 10.9099360945,
    name: "Süpplingenburg",
    plz: "38376"
  },
  {
    lati: 47.9997567052, longi: 10.3596376287,
    name: "Sontheim",
    plz: "87776"
  },
  {
    lati: 48.5587254856, longi: 9.60784697135,
    name: "Wiesensteig",
    plz: "73349"
  },
  {
    lati: 54.3324536847, longi: 9.85477263698,
    name: "Bredenbek",
    plz: "24796"
  },
  {
    lati: 53.5481982916, longi: 10.1351283768,
    name: "Hamburg",
    plz: "22117"
  },
  {
    lati: 47.7968328495, longi: 12.8462420421,
    name: "Anger",
    plz: "83454"
  },
  {
    lati: 50.7737681586, longi: 6.03634173683,
    name: "Aachen",
    plz: "52074"
  },
  {
    lati: 50.8327613491, longi: 6.45274394569,
    name: "Düren",
    plz: "52353"
  },
  {
    lati: 52.4877728628, longi: 6.88437920724,
    name: "Uelsen, Halle, Gölenkamp, Getelo",
    plz: "49843"
  },
  {
    lati: 49.539483163, longi: 8.78724833607,
    name: "Abtsteinach",
    plz: "69518"
  },
  {
    lati: 53.3822131991, longi: 13.1571397612,
    name: "Möllenbeck",
    plz: "17237"
  },
  {
    lati: 47.9317620091, longi: 9.16684591305,
    name: "Wald",
    plz: "88639"
  },
  {
    lati: 51.2751378364, longi: 6.62867132093,
    name: "Meerbusch",
    plz: "40670"
  },
  {
    lati: 53.5792321719, longi: 10.0310246405,
    name: "Hamburg",
    plz: "22083"
  },
  {
    lati: 47.9009064718, longi: 10.4056675387,
    name: "Ronsberg",
    plz: "87671"
  },
  {
    lati: 52.4093771653, longi: 10.5204202297,
    name: "Rötgesbüttel",
    plz: "38531"
  },
  {
    lati: 49.8001982476, longi: 11.7474269034,
    name: "Vorbach",
    plz: "95519"
  },
  {
    lati: 51.0423075715, longi: 8.6643966441,
    name: "Allendorf",
    plz: "35108"
  },
  {
    lati: 49.5208413286, longi: 7.98963069563,
    name: "Ramsen",
    plz: "67305"
  },
  {
    lati: 49.3493252147, longi: 11.9429439513,
    name: "Ensdorf",
    plz: "92266"
  },
  {
    lati: 51.6954348257, longi: 6.1236905699,
    name: "Goch",
    plz: "47574"
  },
  {
    lati: 50.1448956112, longi: 6.16516279622,
    name: "Lützkampen",
    plz: "54617"
  },
  {
    lati: 50.0533285346, longi: 6.71426732849,
    name: "Bettenfeld, Niederöfflingen u.a.",
    plz: "54533"
  },
  {
    lati: 54.5377445964, longi: 9.70555017084,
    name: "Brodersby, Goltoft",
    plz: "24864"
  },
  {
    lati: 48.6169203892, longi: 9.76760086617,
    name: "Bad Überkingen",
    plz: "73337"
  },
  {
    lati: 48.5255525419, longi: 9.45459489119,
    name: "Grabenstetten",
    plz: "72582"
  },
  {
    lati: 53.5575193768, longi: 10.0116793448,
    name: "Hamburg",
    plz: "20099"
  },
  {
    lati: 49.4180057654, longi: 8.62466810237,
    name: "Heidelberg",
    plz: "69123"
  },
  {
    lati: 51.4470540814, longi: 8.83993013443,
    name: "Marsberg",
    plz: "34431"
  },
  {
    lati: 48.0831266002, longi: 11.5077152961,
    name: "München",
    plz: "81477"
  },
  {
    lati: 52.3815920273, longi: 10.8380691504,
    name: "Wolfsburg",
    plz: "38446"
  },
  {
    lati: 52.7062804784, longi: 9.16164738913,
    name: "Balge",
    plz: "31609"
  },
  {
    lati: 53.1673356421, longi: 8.65365996265,
    name: "Bremen",
    plz: "28759"
  },
  {
    lati: 49.6245441479, longi: 10.8757848712,
    name: "Großenseebach",
    plz: "91091"
  },
  {
    lati: 52.6202237155, longi: 11.1226296903,
    name: "Klötze, Apenburg-Winterfeld",
    plz: "38486"
  },
  {
    lati: 49.0130350042, longi: 11.4958946774,
    name: "Beilngries",
    plz: "92339"
  },
  {
    lati: 50.02628862, longi: 9.56370136374,
    name: "Lohr a. Main",
    plz: "97816"
  },
  {
    lati: 49.065317842, longi: 9.00181398982,
    name: "Güglingen",
    plz: "74363"
  },
  {
    lati: 47.7372680692, longi: 9.07541955223,
    name: "Allensbach",
    plz: "78476"
  },
  {
    lati: 48.5981150693, longi: 9.10411363584,
    name: "Dettenhausen",
    plz: "72135"
  },
  {
    lati: 49.3528043052, longi: 7.70452870081,
    name: "Schopp",
    plz: "67707"
  },
  {
    lati: 48.1397759217, longi: 8.25810858235,
    name: "Triberg im Schwarzwald",
    plz: "78098"
  },
  {
    lati: 48.7164185903, longi: 12.038480004,
    name: "Rottenburg a.d. Laaber",
    plz: "84056"
  },
  {
    lati: 50.4287119348, longi: 12.4950066259,
    name: "Muldenhammer",
    plz: "08262"
  },
  {
    lati: 53.5066901824, longi: 12.7443157637,
    name: "Waren/ Müritz",
    plz: "17192"
  },
  {
    lati: 52.5762599359, longi: 13.2733558024,
    name: "Berlin Tegel",
    plz: "13507"
  },
  {
    lati: 50.5850005206, longi: 7.57670879562,
    name: "Dürrholz",
    plz: "56307"
  },
  {
    lati: 50.7556973339, longi: 7.6797087703,
    name: "Hamm (Sieg)",
    plz: "57577"
  },
  {
    lati: 47.9382124358, longi: 7.75942594071,
    name: "Pfaffenweiler",
    plz: "79292"
  },
  {
    lati: 49.6146794105, longi: 6.769543744,
    name: "Schillingen",
    plz: "54429"
  },
  {
    lati: 53.591402722, longi: 9.97972014494,
    name: "Hamburg",
    plz: "20251"
  },
  {
    lati: 48.0782004002, longi: 11.6437829818,
    name: "Neubiberg",
    plz: "85579"
  },
  {
    lati: 50.4200013338, longi: 11.8170997635,
    name: "Hirschberg",
    plz: "07927"
  },
  {
    lati: 49.6396774881, longi: 11.0633083159,
    name: "Langensendelbach",
    plz: "91094"
  },
  {
    lati: 51.9285122683, longi: 11.0251374455,
    name: "Halberstadt, Groß Quenstedt",
    plz: "38822"
  },
  {
    lati: 52.5488240164, longi: 10.2956601423,
    name: "Langlingen",
    plz: "29364"
  },
  {
    lati: 50.3431526799, longi: 12.2258687207,
    name: "Adorf",
    plz: "08626"
  },
  {
    lati: 52.450665101, longi: 8.94795922401,
    name: "Raddestorf",
    plz: "31604"
  },
  {
    lati: 50.7798001293, longi: 10.5097884798,
    name: "Floh-Seligenthal",
    plz: "98593"
  },
  {
    lati: 54.2957488508, longi: 13.3103467749,
    name: "Garz/ Rügen",
    plz: "18574"
  },
  {
    lati: 52.5234132733, longi: 13.5885923443,
    name: "Berlin",
    plz: "12619"
  },
  {
    lati: 51.2901354863, longi: 13.9033237393,
    name: "Königsbrück u.a.",
    plz: "01936"
  },
  {
    lati: 49.9238790442, longi: 9.78209856991,
    name: "Himmelstadt",
    plz: "97267"
  },
  {
    lati: 51.5936190016, longi: 6.31517817659,
    name: "Kevelaer-Winnekendonk",
    plz: "47626"
  },
  {
    lati: 52.5389090203, longi: 7.28209585664,
    name: "Lingen",
    plz: "49808"
  },
  {
    lati: 52.3860290967, longi: 9.57477667322,
    name: "Seelze",
    plz: "30926"
  },
  {
    lati: 52.5266789912, longi: 8.24046268126,
    name: "Damme",
    plz: "49401"
  },
  {
    lati: 48.72012757, longi: 9.89112899909,
    name: "Lauterstein",
    plz: "73111"
  },
  {
    lati: 51.2261705327, longi: 11.754003114,
    name: "Freyburg, Balgstädt",
    plz: "06632"
  },
  {
    lati: 48.1501438324, longi: 11.9986911832,
    name: "Hohenlinden",
    plz: "85664"
  },
  {
    lati: 48.8745674983, longi: 12.2835640245,
    name: "Aufhausen",
    plz: "93089"
  },
  {
    lati: 51.6887227087, longi: 12.3238529815,
    name: "Raguhn-Jeßnitz",
    plz: "06800"
  },
  {
    lati: 49.8821758799, longi: 6.79692538881,
    name: "Hetzerath, Dierscheid, Heckenmünster",
    plz: "54523"
  },
  {
    lati: 51.2338758175, longi: 6.78041042321,
    name: "Düsseldorf",
    plz: "40479"
  },
  {
    lati: 51.1835411623, longi: 7.18075000817,
    name: "Remscheid",
    plz: "42853"
  },
  {
    lati: 53.5883921644, longi: 10.0936737081,
    name: "Hamburg",
    plz: "22047"
  },
  {
    lati: 52.5003054575, longi: 13.3813302792,
    name: "Berlin Kreuzberg",
    plz: "10963"
  },
  {
    lati: 54.0550976699, longi: 13.7825186259,
    name: "Wolgast",
    plz: "17438"
  },
  {
    lati: 47.7367797312, longi: 11.9479528936,
    name: "Fischbachau",
    plz: "83730"
  },
  {
    lati: 53.7829996351, longi: 10.7297946588,
    name: "Groß Grönau",
    plz: "23627"
  },
  {
    lati: 48.5025273612, longi: 13.1448474597,
    name: "Haarbach",
    plz: "94542"
  },
  {
    lati: 52.4834169981, longi: 13.5290339713,
    name: "Berlin Karlshorst",
    plz: "10318"
  },
  {
    lati: 51.0241951018, longi: 11.0357323991,
    name: "Erfurt",
    plz: "99087"
  },
  {
    lati: 50.9180776202, longi: 12.9605366323,
    name: "Lichtenau",
    plz: "09244"
  },
  {
    lati: 51.6245368335, longi: 7.86302886702,
    name: "Hamm",
    plz: "59069"
  },
  {
    lati: 53.3091014951, longi: 7.76949161914,
    name: "Uplengen",
    plz: "26670"
  },
  {
    lati: 51.2715547941, longi: 7.25297482282,
    name: "Wuppertal",
    plz: "42389"
  },
  {
    lati: 54.422431436, longi: 9.69217395047,
    name: "Ascheffel",
    plz: "24358"
  },
  {
    lati: 50.3857317408, longi: 9.31259563001,
    name: "Birstein",
    plz: "63633"
  },
  {
    lati: 48.8797544988, longi: 9.53944607758,
    name: "Rudersberg",
    plz: "73635"
  },
  {
    lati: 54.1737528145, longi: 10.0459297166,
    name: "Bordesholm",
    plz: "24582"
  },
  {
    lati: 54.3426176881, longi: 10.0265699546,
    name: "Kiel",
    plz: "24107"
  },
  {
    lati: 50.8715822413, longi: 12.4213907659,
    name: "Gößnitz",
    plz: "04639"
  },
  {
    lati: 53.7165261943, longi: 11.6844120253,
    name: "Brüel",
    plz: "19412"
  },
  {
    lati: 48.5501312958, longi: 11.7170821048,
    name: "Au in der Hallertau",
    plz: "84072"
  },
  {
    lati: 51.6783655901, longi: 7.6261754801,
    name: "Werne",
    plz: "59368"
  },
  {
    lati: 49.4905508567, longi: 8.22187716247,
    name: "Erpolzheim",
    plz: "67167"
  },
  {
    lati: 51.719171395, longi: 8.68629426153,
    name: "Paderborn",
    plz: "33106"
  },
  {
    lati: 49.3278020046, longi: 7.0866112786,
    name: "Friedrichsthal",
    plz: "66299"
  },
  {
    lati: 51.4481439188, longi: 7.07537257736,
    name: "Essen",
    plz: "45276"
  },
  {
    lati: 50.5912162262, longi: 6.64780331303,
    name: "Mechernich",
    plz: "53894"
  },
  {
    lati: 49.7947773532, longi: 11.0036475816,
    name: "Altendorf",
    plz: "96146"
  },
  {
    lati: 49.4565417639, longi: 11.0424416243,
    name: "Nürnberg",
    plz: "90429"
  },
  {
    lati: 54.5364343727, longi: 9.36695655423,
    name: "Silberstedt, Schwittschau",
    plz: "24887"
  },
  {
    lati: 49.210470331, longi: 9.50162487498,
    name: "Öhringen",
    plz: "74613"
  },
  {
    lati: 50.0823229402, longi: 9.20123558313,
    name: "Krombach",
    plz: "63829"
  },
  {
    lati: 48.9150946667, longi: 12.7184307798,
    name: "Bogen",
    plz: "94327"
  },
  {
    lati: 53.3412467517, longi: 12.4956427844,
    name: "Wredenhagen",
    plz: "17209"
  },
  {
    lati: 52.5301100992, longi: 13.3499266238,
    name: "Berlin Moabit",
    plz: "10559"
  },
  {
    lati: 50.8125077778, longi: 9.92476550406,
    name: "Hohenroda",
    plz: "36284"
  },
  {
    lati: 54.3178635095, longi: 10.0496597187,
    name: "Melsdorf",
    plz: "24109"
  },
  {
    lati: 52.7041932853, longi: 10.2104297061,
    name: "Habighorst",
    plz: "29359"
  },
  {
    lati: 47.6082298726, longi: 10.6440184518,
    name: "Hopferau",
    plz: "87659"
  },
  {
    lati: 48.392662729, longi: 10.8081926567,
    name: "Neusäß",
    plz: "86356"
  },
  {
    lati: 50.5087606398, longi: 10.9314195813,
    name: "Biberau, Masserberg",
    plz: "98666"
  },
  {
    lati: 51.505960846, longi: 7.09506949706,
    name: "Gelsenkirchen",
    plz: "45879"
  },
  {
    lati: 53.7274826621, longi: 7.40158535127,
    name: "Baltrum",
    plz: "26579"
  },
  {
    lati: 49.8633575102, longi: 10.2413986818,
    name: "Volkach",
    plz: "97332"
  },
  {
    lati: 52.9834834086, longi: 12.0466705916,
    name: "Plattenburg",
    plz: "19339"
  },
  {
    lati: 49.0035292871, longi: 12.067180837,
    name: "Regensburg",
    plz: "93051"
  },
  {
    lati: 50.6057428513, longi: 12.1724800861,
    name: "Elsterberg",
    plz: "07985"
  },
  {
    lati: 47.9585731176, longi: 12.5029138831,
    name: "Seeon-Seebruck",
    plz: "83376"
  },
  {
    lati: 51.0079143927, longi: 12.6013018192,
    name: "Kohren-Sahlis",
    plz: "04655"
  },
  {
    lati: 48.4985759776, longi: 12.9508877375,
    name: "Dietersburg",
    plz: "84378"
  },
  {
    lati: 47.8220469812, longi: 10.7198464531,
    name: "Bidingen",
    plz: "87651"
  },
  {
    lati: 48.3906941729, longi: 10.8557319447,
    name: "Augsburg",
    plz: "86156"
  },
  {
    lati: 53.5731666481, longi: 10.3339237553,
    name: "Witzhave",
    plz: "22969"
  },
  {
    lati: 51.3048470377, longi: 10.6677429368,
    name: "Ebeleben",
    plz: "99713"
  },
  {
    lati: 51.3912220278, longi: 13.4252056482,
    name: "Gröditz, Wülknitz, Röderaue",
    plz: "01609"
  },
  {
    lati: 49.8556143055, longi: 7.87835609765,
    name: "Bad Kreuznach",
    plz: "55545"
  },
  {
    lati: 52.8222764508, longi: 8.95495370372,
    name: "Bruchhausen-Vilsen, Süstedt",
    plz: "27305"
  },
  {
    lati: 49.153473984, longi: 8.97868970145,
    name: "Gemmingen",
    plz: "75050"
  },
  {
    lati: 49.1385163973, longi: 9.03000789432,
    name: "Schwaigern",
    plz: "74193"
  },
  {
    lati: 50.9553133042, longi: 11.035101766,
    name: "Erfurt",
    plz: "99096"
  },
  {
    lati: 49.445391058, longi: 11.7659195289,
    name: "Ammerthal",
    plz: "92260"
  },
  {
    lati: 48.7959122999, longi: 10.4643238269,
    name: "Ederheim",
    plz: "86739"
  },
  {
    lati: 49.8846870984, longi: 11.4993273673,
    name: "Hummeltal",
    plz: "95503"
  },
  {
    lati: 49.8486603093, longi: 6.91007268191,
    name: "Neumagen-Dhron",
    plz: "54347"
  },
  {
    lati: 49.9116993758, longi: 8.1401157995,
    name: "Stadecken-Elsheim",
    plz: "55271"
  },
  {
    lati: 49.4867709461, longi: 8.47248182265,
    name: "Mannheim",
    plz: "68161"
  },
  {
    lati: 50.0688726995, longi: 8.64050283772,
    name: "Frankfurt am Main",
    plz: "60528"
  },
  {
    lati: 51.3275653692, longi: 8.94784956015,
    name: "Twistetal",
    plz: "34477"
  },
  {
    lati: 48.6848125445, longi: 10.0385625322,
    name: "Steinheim am Albuch",
    plz: "89555"
  },
  {
    lati: 47.9788222336, longi: 10.1630742166,
    name: "Memmingen",
    plz: "87700"
  },
  {
    lati: 47.6627289689, longi: 9.50413419012,
    name: "Friedrichshafen",
    plz: "88046"
  },
  {
    lati: 51.53769316, longi: 7.03242002953,
    name: "Gelsenkirchen",
    plz: "45899"
  },
  {
    lati: 51.3576134367, longi: 12.4686427819,
    name: "Leipzig",
    plz: "04329"
  },
  {
    lati: 49.72029411, longi: 12.0034925957,
    name: "Schwarzenbach",
    plz: "92720"
  },
  {
    lati: 48.9556889117, longi: 8.23346033658,
    name: "Au am Rhein",
    plz: "76474"
  },
  {
    lati: 48.4656425339, longi: 13.3171981671,
    name: "Ruhstorf a.d. Rott",
    plz: "94099"
  },
  {
    lati: 47.6146389238, longi: 11.0206886067,
    name: "Unterammergau",
    plz: "82497"
  },
  {
    lati: 50.9572708482, longi: 7.68746335803,
    name: "Reichshof",
    plz: "51580"
  },
  {
    lati: 53.4004452368, longi: 7.40495485539,
    name: "Ihlow",
    plz: "26632"
  },
  {
    lati: 52.3785263261, longi: 12.1720845599,
    name: "Genthin, Hohenseeden, Zabakuck u.a.",
    plz: "39307"
  },
  {
    lati: 52.7064004, longi: 8.97198715186,
    name: "Siedenburg, Staffhorst",
    plz: "27254"
  },
  {
    lati: 52.4462759993, longi: 13.202604737,
    name: "Berlin Nikolassee",
    plz: "14129"
  },
  {
    lati: 48.6395866647, longi: 9.65162729164,
    name: "Gammelshausen",
    plz: "73108"
  },
  {
    lati: 49.7332883486, longi: 9.82275179604,
    name: "Kist, Irtenberger Wald",
    plz: "97270"
  },
  {
    lati: 52.5139059563, longi: 13.5149923965,
    name: "Berlin Friedrichsfelde",
    plz: "10315"
  },
  {
    lati: 50.9485422571, longi: 13.7566062199,
    name: "Kreischa",
    plz: "01731"
  },
  {
    lati: 50.5482026756, longi: 8.56935711993,
    name: "Wetzlar",
    plz: "35581"
  },
  {
    lati: 51.277927496, longi: 9.62020948226,
    name: "Kaufungen",
    plz: "34260"
  },
  {
    lati: 50.5594128579, longi: 9.6294225277,
    name: "Fulda",
    plz: "36041"
  },
  {
    lati: 53.7246454628, longi: 11.3966426427,
    name: "Lübstorf",
    plz: "19069"
  },
  {
    lati: 51.9454327796, longi: 6.84748094731,
    name: "Südlohn",
    plz: "46354"
  },
  {
    lati: 51.4299161661, longi: 7.15820636127,
    name: "Bochum",
    plz: "44879"
  },
  {
    lati: 49.4272520721, longi: 7.28496195329,
    name: "Breitenbach",
    plz: "66916"
  },
  {
    lati: 48.1528975345, longi: 10.3210265813,
    name: "Kirchhaslach",
    plz: "87755"
  },
  {
    lati: 48.7886374532, longi: 9.06282023891,
    name: "Gerlingen",
    plz: "70839"
  },
  {
    lati: 49.406471561, longi: 12.0396151187,
    name: "Fensterbach",
    plz: "92269"
  },
  {
    lati: 54.2200859654, longi: 11.0744678682,
    name: "Dahme",
    plz: "23747"
  },
  {
    lati: 53.0932485398, longi: 14.2590323402,
    name: "Schwedt",
    plz: "16303"
  },
  {
    lati: 50.5164285368, longi: 6.55267370592,
    name: "Kall",
    plz: "53925"
  },
  {
    lati: 49.2635913393, longi: 6.91852818086,
    name: "Saarbrücken",
    plz: "66126"
  },
  {
    lati: 50.1003279787, longi: 7.5869825891,
    name: "Leiningen",
    plz: "56291"
  },
  {
    lati: 49.5105982482, longi: 8.14639561472,
    name: "Weisenheim am Berg",
    plz: "67273"
  },
  {
    lati: 49.144629766, longi: 9.32084259582,
    name: "Ellhofen",
    plz: "74248"
  },
  {
    lati: 48.5000296642, longi: 9.11072298868,
    name: "Kusterdingen",
    plz: "72127"
  },
  {
    lati: 51.7654407292, longi: 8.54091642259,
    name: "Delbrück",
    plz: "33129"
  },
  {
    lati: 52.03291695, longi: 8.50892581094,
    name: "Bielefeld",
    plz: "33615"
  },
  {
    lati: 49.5724828544, longi: 8.31148479011,
    name: "Beindersheim",
    plz: "67259"
  },
  {
    lati: 48.5401878473, longi: 9.78738256273,
    name: "Nellingen",
    plz: "89191"
  },
  {
    lati: 53.7824058446, longi: 10.0258104511,
    name: "Henstedt-Ulzburg",
    plz: "24558"
  },
  {
    lati: 53.6072735618, longi: 10.0120557342,
    name: "Hamburg",
    plz: "22297"
  },
  {
    lati: 47.6839644786, longi: 7.83261239125,
    name: "Hausen im Wiesental",
    plz: "79688"
  },
  {
    lati: 51.6274117164, longi: 9.53539801348,
    name: "Oberweser",
    plz: "34399"
  },
  {
    lati: 51.9470627029, longi: 7.02172258273,
    name: "Gescher",
    plz: "48712"
  },
  {
    lati: 50.7534201875, longi: 7.11840248846,
    name: "Bonn",
    plz: "53225"
  },
  {
    lati: 52.8528983423, longi: 7.52824641111,
    name: "Sögel u.a.",
    plz: "49751"
  },
  {
    lati: 49.128884251, longi: 7.55835394818,
    name: "Vinningen, Trulben, Ruppertsweiler u.a.",
    plz: "66957"
  },
  {
    lati: 52.8334929092, longi: 7.77830333926,
    name: "Lindern (Oldenburg)",
    plz: "49699"
  },
  {
    lati: 48.7651632103, longi: 11.6761164058,
    name: "Münchsmünster",
    plz: "85126"
  },
  {
    lati: 50.5665076776, longi: 12.9493695511,
    name: "Schlettau",
    plz: "09487"
  },
  {
    lati: 51.1609808883, longi: 12.1146533133,
    name: "Hohenmölsen",
    plz: "06679"
  },
  {
    lati: 52.2383840439, longi: 7.95995635518,
    name: "Hasbergen",
    plz: "49205"
  },
  {
    lati: 50.3060263717, longi: 8.58428220695,
    name: "Wehrheim",
    plz: "61273"
  },
  {
    lati: 48.2433885008, longi: 8.77423687687,
    name: "Dormettingen",
    plz: "72358"
  },
  {
    lati: 51.4434990976, longi: 6.88958720542,
    name: "Mülheim an der Ruhr",
    plz: "45473"
  },
  {
    lati: 50.9952148023, longi: 7.00350045253,
    name: "Köln",
    plz: "51061"
  },
  {
    lati: 51.5133108054, longi: 7.16192336384,
    name: "Herne",
    plz: "44651"
  },
  {
    lati: 51.4517950569, longi: 6.69854865436,
    name: "Duisburg",
    plz: "47198"
  },
  {
    lati: 47.611243969, longi: 10.8473342755,
    name: "Halblech",
    plz: "87642"
  },
  {
    lati: 50.5915721266, longi: 9.68276411317,
    name: "Fulda",
    plz: "36039"
  },
  {
    lati: 48.3242674048, longi: 9.8631438479,
    name: "Erbach",
    plz: "89155"
  },
  {
    lati: 52.6759101105, longi: 9.37927854426,
    name: "Steimbke",
    plz: "31634"
  },
  {
    lati: 50.6439907826, longi: 9.37613739332,
    name: "Lauterbach",
    plz: "36341"
  },
  {
    lati: 50.4007828515, longi: 11.3991225577,
    name: "Teuschnitz",
    plz: "96358"
  },
  {
    lati: 49.0426848369, longi: 13.0561909562,
    name: "Böbrach",
    plz: "94255"
  },
  {
    lati: 53.76039871, longi: 13.2954779141,
    name: "Burow",
    plz: "17089"
  },
  {
    lati: 52.1644783449, longi: 9.26657269798,
    name: "Hessisch Oldendorf",
    plz: "31840"
  },
  {
    lati: 51.3792726452, longi: 12.4096581175,
    name: "Leipzig",
    plz: "04357"
  },
  {
    lati: 47.7802214653, longi: 12.1498664556,
    name: "Neubeuern",
    plz: "83115"
  },
  {
    lati: 48.1740308598, longi: 11.6067696143,
    name: "München",
    plz: "80805"
  },
  {
    lati: 48.3935897195, longi: 11.7063767355,
    name: "Freising",
    plz: "85354"
  },
  {
    lati: 48.7678351847, longi: 9.83613028216,
    name: "Stadt Schwäbisch Gmünd",
    plz: "73529"
  },
  {
    lati: 53.6820223658, longi: 9.26800391638,
    name: "Großenwörden",
    plz: "21712"
  },
  {
    lati: 48.8774611099, longi: 9.26627919154,
    name: "Remseck am Neckar",
    plz: "71686"
  },
  {
    lati: 53.1128733472, longi: 10.1872072331,
    name: "Oldendorf (Luhe), Amelinghausen, Rehlingen",
    plz: "21385"
  },
  {
    lati: 50.3590185328, longi: 6.42309623896,
    name: "Hallschlag",
    plz: "54611"
  },
  {
    lati: 49.1930157923, longi: 7.70442650295,
    name: "Vinningen, Trulben, Ruppertsweiler u.a.",
    plz: "66957"
  },
  {
    lati: 50.192586622, longi: 8.46181431096,
    name: "Königstein im Taunus",
    plz: "61462"
  },
  {
    lati: 48.9236407835, longi: 8.67326986438,
    name: "Ispringen",
    plz: "75228"
  },
  {
    lati: 48.1897593806, longi: 8.34370453393,
    name: "Schramberg",
    plz: "78144"
  },
  {
    lati: 50.2973426824, longi: 8.05801318439,
    name: "Hahnstätten u.a.",
    plz: "65623"
  },
  {
    lati: 49.4413890332, longi: 7.43474344564,
    name: "Herschweiler-Pettersheim",
    plz: "66909"
  },
  {
    lati: 49.9976955644, longi: 7.53157977386,
    name: "Simmern/Hunsrück u.a.",
    plz: "55469"
  },
  {
    lati: 47.5523326984, longi: 7.68340953643,
    name: "Grenzach-Wyhlen",
    plz: "79639"
  },
  {
    lati: 53.1694111849, longi: 10.8380031555,
    name: "Nahrendorf",
    plz: "21369"
  },
  {
    lati: 51.3195638066, longi: 10.301717521,
    name: "Dingelstädt",
    plz: "37351"
  },
  {
    lati: 54.314467324, longi: 10.5615093514,
    name: "Lütjenburg",
    plz: "24321"
  },
  {
    lati: 48.6523557642, longi: 10.4987849135,
    name: "Finningen",
    plz: "89435"
  },
  {
    lati: 48.8110083199, longi: 12.8228335967,
    name: "Stephansposching",
    plz: "94569"
  },
  {
    lati: 53.6194847211, longi: 11.1803296687,
    name: "Lützow",
    plz: "19209"
  },
  {
    lati: 48.6838524233, longi: 11.6199707345,
    name: "Geisenfeld",
    plz: "85290"
  },
  {
    lati: 48.3784338917, longi: 11.8707198101,
    name: "Eitting",
    plz: "85462"
  },
  {
    lati: 50.7938404588, longi: 6.09624837772,
    name: "Aachen",
    plz: "52070"
  },
  {
    lati: 49.4405599029, longi: 7.76679770829,
    name: "Kaiserslautern",
    plz: "67655"
  },
  {
    lati: 49.4576733698, longi: 8.41019404194,
    name: "Ludwigshafen am Rhein",
    plz: "67067"
  },
  {
    lati: 48.2060462571, longi: 9.7024537711,
    name: "Unterstadion",
    plz: "89619"
  },
  {
    lati: 48.1378785508, longi: 11.7999919633,
    name: "Parsdorf/Hergolding",
    plz: "85599"
  },
  {
    lati: 49.7796543608, longi: 10.0002749034,
    name: "Gerbrunn",
    plz: "97218"
  },
  {
    lati: 50.5188856931, longi: 12.7812763674,
    name: "Schwarzenberg/Erzgeb.",
    plz: "08340"
  },
  {
    lati: 51.0841256181, longi: 10.9364101363,
    name: "Gebesee",
    plz: "99189"
  },
  {
    lati: 52.560051516, longi: 10.3691162299,
    name: "Langlingen",
    plz: "29364"
  },
  {
    lati: 49.3171185193, longi: 12.0833666736,
    name: "Schwandorf",
    plz: "92421"
  },
  {
    lati: 48.8898562167, longi: 12.0120930093,
    name: "Teugn",
    plz: "93356"
  },
  {
    lati: 48.7538434044, longi: 13.7655745899,
    name: "Neureichenau",
    plz: "94089"
  },
  {
    lati: 50.6716302877, longi: 7.42270106112,
    name: "Asbach, Buchholz",
    plz: "53567"
  },
  {
    lati: 48.3101779872, longi: 11.1822926311,
    name: "Odelzhausen",
    plz: "85235"
  },
  {
    lati: 53.4022099106, longi: 8.35218113319,
    name: "Stadland",
    plz: "26936"
  },
  {
    lati: 49.0231461786, longi: 8.7771896841,
    name: "Knittlingen",
    plz: "75438"
  },
  {
    lati: 51.2492662584, longi: 9.40975583962,
    name: "Baunatal",
    plz: "34225"
  },
  {
    lati: 53.3586365373, longi: 10.0471686574,
    name: "Seevetal",
    plz: "21220"
  },
  {
    lati: 52.186510437, longi: 10.8470814912,
    name: "Königslutter am Elm",
    plz: "38154"
  },
  {
    lati: 53.4751519935, longi: 11.4112430031,
    name: "Rastow",
    plz: "19077"
  },
  {
    lati: 49.1775003074, longi: 8.64310220219,
    name: "Ubstadt-Weiher",
    plz: "76698"
  },
  {
    lati: 49.3962683857, longi: 8.62606138247,
    name: "Eppelheim",
    plz: "69214"
  },
  {
    lati: 50.3212236469, longi: 9.47400072805,
    name: "Steinau an der Straße",
    plz: "36396"
  },
  {
    lati: 54.5955333685, longi: 9.54574422305,
    name: "Stolk",
    plz: "24890"
  },
  {
    lati: 50.7050374895, longi: 12.4839211693,
    name: "Zwickau",
    plz: "08056"
  },
  {
    lati: 50.4385241461, longi: 9.3989892678,
    name: "Freiensteinau",
    plz: "36399"
  },
  {
    lati: 51.6686017315, longi: 12.2374339613,
    name: "Bitterfeld-Wolfen",
    plz: "06766"
  },
  {
    lati: 52.4110113241, longi: 13.4978200419,
    name: "Berlin Rudow",
    plz: "12355"
  },
  {
    lati: 51.7685165407, longi: 14.0630902152,
    name: "Vetschau",
    plz: "03226"
  },
  {
    lati: 50.8489601498, longi: 11.3635066889,
    name: "Blankenhain",
    plz: "99444"
  },
  {
    lati: 49.5599246854, longi: 9.08427960008,
    name: "Eberbach",
    plz: "69412"
  },
  {
    lati: 47.8264237511, longi: 9.17493782859,
    name: "Owingen",
    plz: "88696"
  },
  {
    lati: 48.6883940868, longi: 9.19923294366,
    name: "Stuttgart/Leinfelden-Echterdingen",
    plz: "70629"
  },
  {
    lati: 54.0865800276, longi: 9.95355003878,
    name: "Neumünster",
    plz: "24537"
  },
  {
    lati: 49.862046897, longi: 9.71587126274,
    name: "Birkenfeld",
    plz: "97834"
  },
  {
    lati: 49.896810161, longi: 10.7193677443,
    name: "Lisberg",
    plz: "96170"
  },
  {
    lati: 50.184492386, longi: 10.3847496721,
    name: "Stadtlauringen",
    plz: "97488"
  },
  {
    lati: 51.5693367678, longi: 10.6412817526,
    name: "Ellrich",
    plz: "99755"
  },
  {
    lati: 48.2405791089, longi: 11.7047598909,
    name: "Ismaning",
    plz: "85737"
  },
  {
    lati: 51.7147742403, longi: 7.83608962217,
    name: "Hamm",
    plz: "59073"
  },
  {
    lati: 47.9491189336, longi: 7.86562033638,
    name: "Freiburg im Breisgau",
    plz: "79100"
  },
  {
    lati: 51.6731013922, longi: 6.43379818461,
    name: "Xanten",
    plz: "46509"
  },
  {
    lati: 49.7002644301, longi: 9.39276724709,
    name: "Neunkirchen",
    plz: "63930"
  },
  {
    lati: 48.5282362834, longi: 8.54503741756,
    name: "Pfalzgrafenweiler",
    plz: "72285"
  },
  {
    lati: 54.1960073996, longi: 11.053119018,
    name: "Kellenhusen",
    plz: "23746"
  },
  {
    lati: 48.7820835239, longi: 10.6884551135,
    name: "Harburg",
    plz: "86655"
  },
  {
    lati: 49.7336537283, longi: 12.0573226987,
    name: "Parkstein",
    plz: "92711"
  },
  {
    lati: 48.4519266648, longi: 10.2034691491,
    name: "Leipheim",
    plz: "89340"
  },
  {
    lati: 49.3649067947, longi: 11.6885561557,
    name: "Kastl",
    plz: "92280"
  },
  {
    lati: 53.1191769141, longi: 10.8820381422,
    name: "Göhrde",
    plz: "29473"
  },
  {
    lati: 48.553632086, longi: 10.2806147375,
    name: "Sontheim an der Brenz",
    plz: "89567"
  },
  {
    lati: 54.4601951995, longi: 9.82576207082,
    name: "Eckernförde",
    plz: "24340"
  },
  {
    lati: 50.8016655852, longi: 6.62914107356,
    name: "Nörvenich",
    plz: "52388"
  },
  {
    lati: 51.2164622196, longi: 6.6629811517,
    name: "Neuss",
    plz: "41462"
  },
  {
    lati: 50.4600753212, longi: 8.15130111518,
    name: "Beselich",
    plz: "65614"
  },
  {
    lati: 48.9992067903, longi: 8.32483040507,
    name: "Karlsruhe",
    plz: "76189"
  },
  {
    lati: 52.7697758133, longi: 14.0288330548,
    name: "Bad Freienwalde u.a.",
    plz: "16259"
  },
  {
    lati: 51.1699099935, longi: 10.1713216687,
    name: "Wanfried",
    plz: "37281"
  },
  {
    lati: 48.8562621289, longi: 12.9119956299,
    name: "Metten",
    plz: "94526"
  },
  {
    lati: 51.4510395642, longi: 11.9807371565,
    name: "Halle/ Saale",
    plz: "06130"
  },
  {
    lati: 53.1423817192, longi: 11.1643289107,
    name: "Damnatz",
    plz: "29472"
  },
  {
    lati: 49.8468725175, longi: 11.6236496512,
    name: "Creußen",
    plz: "95473"
  },
  {
    lati: 48.2338581538, longi: 8.55816622762,
    name: "Bösingen",
    plz: "78662"
  },
  {
    lati: 49.4510800353, longi: 8.25067248535,
    name: "Gönnheim",
    plz: "67161"
  },
  {
    lati: 48.2794060935, longi: 8.41211972115,
    name: "Aichhalden",
    plz: "78733"
  },
  {
    lati: 50.1891937756, longi: 7.65911928198,
    name: "Bornich, Patersberg",
    plz: "56348"
  },
  {
    lati: 50.5776905871, longi: 7.71184951444,
    name: "Freilingen, Freirachdorf u.a.",
    plz: "56244"
  },
  {
    lati: 50.6683380289, longi: 13.3402276977,
    name: "Olbernhau, Pfaffroda, Heidersdorf",
    plz: "09526"
  },
  {
    lati: 52.3904014941, longi: 13.4171264811,
    name: "Berlin",
    plz: "12309"
  },
  {
    lati: 49.3560721947, longi: 7.7726139939,
    name: "Trippstadt u.a.",
    plz: "67705"
  },
  {
    lati: 51.2951636511, longi: 9.52485321063,
    name: "Kassel",
    plz: "34123"
  },
  {
    lati: 48.6664816888, longi: 9.56793480436,
    name: "Hattenhofen",
    plz: "73110"
  },
  {
    lati: 49.5341577033, longi: 11.3425724005,
    name: "Neunkirchen am Sand",
    plz: "91233"
  },
  {
    lati: 49.8068968951, longi: 11.5963276277,
    name: "Schnabelwaid",
    plz: "91289"
  },
  {
    lati: 47.6968711239, longi: 10.4990586832,
    name: "Görisried",
    plz: "87657"
  },
  {
    lati: 48.1795467301, longi: 9.89573861434,
    name: "Mietingen",
    plz: "88487"
  },
  {
    lati: 48.1194912843, longi: 8.82029891124,
    name: "Bubsheim",
    plz: "78585"
  },
  {
    lati: 49.2222253124, longi: 12.7569270035,
    name: "Runding",
    plz: "93486"
  },
  {
    lati: 51.1417418413, longi: 9.58501959839,
    name: "Melsungen",
    plz: "34212"
  },
  {
    lati: 53.5597062336, longi: 9.96259181432,
    name: "Hamburg",
    plz: "22767"
  },
  {
    lati: 49.9728748731, longi: 10.516555914,
    name: "Knetzgau",
    plz: "97478"
  },
  {
    lati: 47.8442213578, longi: 11.1455481036,
    name: "Weilheim i. OB",
    plz: "82362"
  },
  {
    lati: 50.6374776782, longi: 11.5697468103,
    name: "Ranis",
    plz: "07389"
  },
  {
    lati: 51.441399032, longi: 7.80221422905,
    name: "Menden",
    plz: "58708"
  },
  {
    lati: 49.2137111696, longi: 8.37170498334,
    name: "Germersheim",
    plz: "76726"
  },
  {
    lati: 48.825611555, longi: 12.1587365583,
    name: "Schierling",
    plz: "84069"
  },
  {
    lati: 49.6355931395, longi: 6.83193757529,
    name: "Kell am See",
    plz: "54427"
  },
  {
    lati: 48.0450376567, longi: 9.32417316202,
    name: "Mengen",
    plz: "88512"
  },
  {
    lati: 53.6901927575, longi: 9.51635283121,
    name: "Kollmar, Pagensand",
    plz: "25377"
  },
  {
    lati: 53.5945818348, longi: 9.53707288256,
    name: "Hollern-Twielenfleth",
    plz: "21723"
  },
  {
    lati: 49.8917604478, longi: 12.4752408888,
    name: "Mähring",
    plz: "95695"
  },
  {
    lati: 48.5718146046, longi: 11.2337047502,
    name: "Schrobenhausen",
    plz: "86529"
  },
  {
    lati: 49.4932313504, longi: 11.1298456034,
    name: "Nürnberg",
    plz: "90411"
  },
  {
    lati: 49.1286780512, longi: 7.80517076043,
    name: "Bad Bergzabern u.a.",
    plz: "76887"
  },
  {
    lati: 47.9969642695, longi: 7.83468753597,
    name: "Freiburg im Breisgau",
    plz: "79115"
  },
  {
    lati: 49.4625316401, longi: 7.91058253145,
    name: "Waldleiningen, Fischbach",
    plz: "67693"
  },
  {
    lati: 50.7419563781, longi: 7.97358727046,
    name: "Daaden",
    plz: "57567"
  },
  {
    lati: 51.4465703288, longi: 6.9763100365,
    name: "Essen",
    plz: "45145"
  },
  {
    lati: 50.0844736758, longi: 7.05086916559,
    name: "Bad Bertrich",
    plz: "56864"
  },
  {
    lati: 49.6364291003, longi: 7.70095382029,
    name: "Nußbach",
    plz: "67759"
  },
  {
    lati: 47.9910353899, longi: 7.78369087322,
    name: "Freiburg im Breisgau",
    plz: "79111"
  },
  {
    lati: 47.9310238413, longi: 7.81058860612,
    name: "Sölden",
    plz: "79294"
  },
  {
    lati: 50.1105224803, longi: 8.23602533913,
    name: "Wiesbaden",
    plz: "65193"
  },
  {
    lati: 53.6596628282, longi: 8.65778189167,
    name: "Geestland",
    plz: "27607"
  },
  {
    lati: 50.5276896994, longi: 7.04527683882,
    name: "Dernau",
    plz: "53507"
  },
  {
    lati: 49.5114187721, longi: 7.7901751126,
    name: "Otterberg",
    plz: "67697"
  },
  {
    lati: 50.2661954116, longi: 8.44768909276,
    name: "Schmitten",
    plz: "61389"
  },
  {
    lati: 48.9916446202, longi: 8.54221467418,
    name: "Pfinztal",
    plz: "76327"
  },
  {
    lati: 48.0603258718, longi: 8.57182498436,
    name: "Villingen-Schwenningen",
    plz: "78056"
  },
  {
    lati: 52.0778696695, longi: 7.97275909205,
    name: "Glandorf",
    plz: "49219"
  },
  {
    lati: 49.2810186472, longi: 8.07363221207,
    name: "Venningen",
    plz: "67482"
  },
  {
    lati: 48.8706400884, longi: 8.60824759829,
    name: "Birkenfeld",
    plz: "75217"
  },
  {
    lati: 47.9295725043, longi: 8.87587342959,
    name: "Emmingen-Liptingen",
    plz: "78576"
  },
  {
    lati: 50.41408114, longi: 11.6767718274,
    name: "Blankenstein, Blankenberg u.a.",
    plz: "07366"
  },
  {
    lati: 50.9410549525, longi: 10.7044747785,
    name: "Gotha",
    plz: "99867"
  },
  {
    lati: 48.8759991779, longi: 10.7999107966,
    name: "Otting",
    plz: "86700"
  },
  {
    lati: 52.6870438427, longi: 11.0133066595,
    name: "Beetzendorf, Rohrberg, Jübar",
    plz: "38489"
  },
  {
    lati: 51.0288396352, longi: 13.5046871674,
    name: "Wilsdruff",
    plz: "01723"
  },
  {
    lati: 51.2724041099, longi: 9.79394069514,
    name: "Großalmerode",
    plz: "37247"
  },
  {
    lati: 51.2708457628, longi: 9.95006045062,
    name: "Bad Sooden-Allendorf",
    plz: "37242"
  },
  {
    lati: 48.9937307079, longi: 9.45902202179,
    name: "Oppenweiler",
    plz: "71570"
  },
  {
    lati: 54.0352702109, longi: 10.0906496974,
    name: "Groß Kummerfeld",
    plz: "24626"
  },
  {
    lati: 48.2078963678, longi: 12.539184767,
    name: "Polling",
    plz: "84570"
  },
  {
    lati: 51.20080489, longi: 13.1311744119,
    name: "Ostrau",
    plz: "04749"
  },
  {
    lati: 49.9757010127, longi: 9.16477531268,
    name: "Aschaffenburg",
    plz: "63739"
  },
  {
    lati: 50.5049303764, longi: 8.06085255997,
    name: "Elbtal",
    plz: "65627"
  },
  {
    lati: 49.6418629223, longi: 8.56294462179,
    name: "Lorsch",
    plz: "64653"
  },
  {
    lati: 50.6131060365, longi: 7.01511176561,
    name: "Meckenheim",
    plz: "53340"
  },
  {
    lati: 51.6064403023, longi: 7.02982317935,
    name: "Gelsenkirchen",
    plz: "45896"
  },
  {
    lati: 51.6423469389, longi: 6.76546824656,
    name: "Hünxe",
    plz: "46569"
  },
  {
    lati: 51.2466412438, longi: 6.40632621997,
    name: "Viersen",
    plz: "41748"
  },
  {
    lati: 49.3190955867, longi: 10.309692151,
    name: "Buch a. Wald",
    plz: "91592"
  },
  {
    lati: 51.3254215468, longi: 12.2681658836,
    name: "Leipzig",
    plz: "04205"
  },
  {
    lati: 49.1703488541, longi: 12.8790186447,
    name: "Kötzting",
    plz: "93444"
  },
  {
    lati: 48.8843259788, longi: 9.76394472616,
    name: "Spraitbach",
    plz: "73565"
  },
  {
    lati: 50.0402888851, longi: 11.3454196843,
    name: "Kasendorf",
    plz: "95359"
  },
  {
    lati: 53.652727874, longi: 13.5292922014,
    name: "Friedland",
    plz: "17098"
  },
  {
    lati: 52.4762308132, longi: 13.4226271191,
    name: "Berlin Neukölln",
    plz: "12049"
  },
  {
    lati: 52.5388675616, longi: 13.5653313342,
    name: "Berlin",
    plz: "12685"
  },
  {
    lati: 50.7825538943, longi: 7.75734553442,
    name: "Wissen, Hövels u.a.",
    plz: "57537"
  },
  {
    lati: 50.8136366725, longi: 7.05025568237,
    name: "Niederkassel",
    plz: "53859"
  },
  {
    lati: 49.273739791, longi: 7.13210548843,
    name: "Sankt Ingbert",
    plz: "66386"
  },
  {
    lati: 50.1323839462, longi: 7.24435038788,
    name: "Ediger-Eller",
    plz: "56814"
  },
  {
    lati: 49.501869079, longi: 6.55208618709,
    name: "Mettlach",
    plz: "66693"
  },
  {
    lati: 52.371409942, longi: 8.62482144884,
    name: "Espelkamp",
    plz: "32339"
  },
  {
    lati: 47.5959346045, longi: 11.4292074575,
    name: "Jachenau",
    plz: "83676"
  },
  {
    lati: 48.8136457496, longi: 11.3640091605,
    name: "Gaimersheim",
    plz: "85080"
  },
  {
    lati: 52.4575431768, longi: 10.8188138464,
    name: "Wolfsburg",
    plz: "38448"
  },
  {
    lati: 48.0238427472, longi: 10.2978665499,
    name: "Westerheim",
    plz: "87784"
  },
  {
    lati: 51.827249807, longi: 10.2839501914,
    name: "Wildemann",
    plz: "38709"
  },
  {
    lati: 48.4435766868, longi: 8.66083069998,
    name: "Horb am Neckar",
    plz: "72160"
  },
  {
    lati: 48.1855541601, longi: 10.9893194865,
    name: "Egling a.d. Paar",
    plz: "86492"
  },
  {
    lati: 49.8275126032, longi: 11.8353185686,
    name: "Neustadt a. Kulm",
    plz: "95514"
  },
  {
    lati: 53.8561537278, longi: 10.657451517,
    name: "Lübeck",
    plz: "23558"
  },
  {
    lati: 53.0590174761, longi: 10.9046429506,
    name: "Zernien",
    plz: "29499"
  },
  {
    lati: 51.8898465236, longi: 10.1866086263,
    name: "Seesen",
    plz: "38723"
  },
  {
    lati: 54.0980095905, longi: 9.68202032868,
    name: "Hohenwestedt",
    plz: "24594"
  },
  {
    lati: 53.9115749324, longi: 13.6680768181,
    name: "Klein Bünzow",
    plz: "17390"
  },
  {
    lati: 50.9080344301, longi: 6.92479466744,
    name: "Köln",
    plz: "50939"
  },
  {
    lati: 49.9327907261, longi: 6.98197452392,
    name: "Maring-Noviand",
    plz: "54484"
  },
  {
    lati: 49.6729615374, longi: 9.50739279461,
    name: "Külsheim",
    plz: "97900"
  },
  {
    lati: 51.6270741363, longi: 9.50553898919,
    name: "Oberweser",
    plz: "34399"
  },
  {
    lati: 50.0101386671, longi: 9.00872097015,
    name: "Mainhausen",
    plz: "63533"
  },
  {
    lati: 50.6270233968, longi: 8.07423652612,
    name: "Rennerod, Zehnhausen, Nister-Möhrendorf, Waigandshain",
    plz: "56477"
  },
  {
    lati: 50.3814739157, longi: 8.45182937054,
    name: "Grävenwiesbach",
    plz: "61279"
  },
  {
    lati: 49.5342560677, longi: 8.49518756459,
    name: "Mannheim",
    plz: "68305"
  },
  {
    lati: 51.0759701471, longi: 7.5369023272,
    name: "Marienheide",
    plz: "51709"
  },
  {
    lati: 47.8537339873, longi: 7.82065582554,
    name: "Münstertal",
    plz: "79244"
  },
  {
    lati: 48.7081851483, longi: 9.65519193885,
    name: "Göppingen",
    plz: "73033"
  },
  {
    lati: 48.2829854503, longi: 11.263001919,
    name: "Sulzemoos",
    plz: "85254"
  },
  {
    lati: 51.8871266684, longi: 12.1506862494,
    name: "Dessau-Roßlau",
    plz: "06862"
  },
  {
    lati: 48.7701678534, longi: 10.8083111983,
    name: "Kaisheim",
    plz: "86687"
  },
  {
    lati: 48.8309541554, longi: 9.16194157857,
    name: "Stuttgart",
    plz: "70435"
  },
  {
    lati: 49.4771329233, longi: 8.61537948842,
    name: "Ladenburg",
    plz: "68526"
  },
  {
    lati: 49.3111122161, longi: 6.74750210814,
    name: "Saarlouis",
    plz: "66740"
  },
  {
    lati: 51.2446144485, longi: 7.06353833127,
    name: "Wuppertal",
    plz: "42327"
  },
  {
    lati: 53.7138943226, longi: 7.24128612232,
    name: "Norderney",
    plz: "26548"
  },
  {
    lati: 49.289358008, longi: 8.01298498182,
    name: "Edesheim",
    plz: "67483"
  },
  {
    lati: 50.7180809457, longi: 9.89718528689,
    name: "Rasdorf",
    plz: "36169"
  },
  {
    lati: 53.9200004948, longi: 13.0276825504,
    name: "Demmin",
    plz: "17109"
  },
  {
    lati: 49.4631564637, longi: 10.4047541933,
    name: "Illesheim",
    plz: "91471"
  },
  {
    lati: 53.4863343336, longi: 13.3256862472,
    name: "Cölpin",
    plz: "17094"
  },
  {
    lati: 47.8690927621, longi: 11.5754195873,
    name: "Dietramszell",
    plz: "83623"
  },
  {
    lati: 50.8639538605, longi: 10.5024119009,
    name: "Tabarz/ Thür. Wald",
    plz: "99891"
  },
  {
    lati: 47.6070215072, longi: 7.86290070955,
    name: "Schwörstadt",
    plz: "79739"
  },
  {
    lati: 47.8107950872, longi: 9.03573992013,
    name: "Bodman-Ludwigshafen",
    plz: "78351"
  },
  {
    lati: 48.696922712, longi: 9.75354106383,
    name: "Salach",
    plz: "73084"
  },
  {
    lati: 48.6377990818, longi: 9.79373828887,
    name: "Kuchen",
    plz: "73329"
  },
  {
    lati: 48.725724851, longi: 11.9077607079,
    name: "Wildenberg",
    plz: "93359"
  },
  {
    lati: 49.4507786585, longi: 11.8454691738,
    name: "Amberg",
    plz: "92224"
  },
  {
    lati: 50.4356621873, longi: 7.90606073263,
    name: "Nentershausen, Hübingen, Niederelbert u.a.",
    plz: "56412"
  },
  {
    lati: 49.714630222, longi: 8.4779191048,
    name: "Groß-Rohrheim",
    plz: "68649"
  },
  {
    lati: 49.2682612083, longi: 11.024942821,
    name: "Büchenbach",
    plz: "91186"
  },
  {
    lati: 53.8996012825, longi: 11.3392231966,
    name: "Barnekow, Gägelow u.a.",
    plz: "23968"
  },
  {
    lati: 47.7868858748, longi: 11.3230301478,
    name: "Iffeldorf",
    plz: "82393"
  },
  {
    lati: 52.2879400168, longi: 11.4066019708,
    name: "Haldensleben",
    plz: "39340"
  },
  {
    lati: 51.4826720603, longi: 11.6737451682,
    name: "Seegebiet Mansfelder Land",
    plz: "06317"
  },
  {
    lati: 48.5134871635, longi: 9.15215452684,
    name: "Wannweil",
    plz: "72827"
  },
  {
    lati: 52.6929770227, longi: 13.1219067318,
    name: "Velten, Oberkrämer",
    plz: "16727"
  },
  {
    lati: 54.1750590866, longi: 13.8115497962,
    name: "Karlshagen",
    plz: "17449"
  },
  {
    lati: 54.1832541098, longi: 13.3617419164,
    name: "Greifswald",
    plz: "17493"
  },
  {
    lati: 48.424233083, longi: 10.1387064469,
    name: "Nersingen",
    plz: "89278"
  },
  {
    lati: 53.7968652659, longi: 10.1314860257,
    name: "Nahe",
    plz: "23866"
  },
  {
    lati: 54.6635803894, longi: 9.66945245945,
    name: "Struxdorf, Schnarup-Thumby",
    plz: "24891"
  },
  {
    lati: 48.7809403095, longi: 10.1004766555,
    name: "Oberkochen",
    plz: "73447"
  },
  {
    lati: 53.6413160244, longi: 10.0803378135,
    name: "Hamburg",
    plz: "22391"
  },
  {
    lati: 50.9896277881, longi: 10.300134573,
    name: "Eisenach",
    plz: "99817"
  },
  {
    lati: 53.7476312862, longi: 12.5634053832,
    name: "Dahmen, Groß Wokern, Teterow",
    plz: "17166"
  },
  {
    lati: 50.7830152569, longi: 8.29219642687,
    name: "Dillenburg",
    plz: "35684"
  },
  {
    lati: 49.091397874, longi: 8.47619016128,
    name: "Stutensee",
    plz: "76297"
  },
  {
    lati: 48.2388856189, longi: 9.09276111667,
    name: "Bitz",
    plz: "72475"
  },
  {
    lati: 48.6945196305, longi: 9.46786929729,
    name: "Hochdorf",
    plz: "73269"
  },
  {
    lati: 50.9748887951, longi: 11.2394477116,
    name: "Weimar",
    plz: "99428"
  },
  {
    lati: 47.7157367962, longi: 11.2033404103,
    name: "Spatzenhausen",
    plz: "82447"
  },
  {
    lati: 47.8635784593, longi: 11.2726402798,
    name: "Bernried",
    plz: "82347"
  },
  {
    lati: 50.9420141462, longi: 6.87718409671,
    name: "Köln",
    plz: "50933"
  },
  {
    lati: 50.2703578562, longi: 7.9547596986,
    name: "Katzenelnbogen",
    plz: "56368"
  },
  {
    lati: 49.7405131885, longi: 7.99885652522,
    name: "Albig",
    plz: "55234"
  },
  {
    lati: 53.5177737761, longi: 7.54071891106,
    name: "Aurich",
    plz: "26607"
  },
  {
    lati: 50.9958402881, longi: 9.22099792218,
    name: "Neuental",
    plz: "34599"
  },
  {
    lati: 53.1959900809, longi: 8.61445078601,
    name: "Bremen",
    plz: "28755"
  },
  {
    lati: 47.7280669559, longi: 9.34181260373,
    name: "Bermatingen",
    plz: "88697"
  },
  {
    lati: 50.3179144246, longi: 8.05322985447,
    name: "Burgschwalbach",
    plz: "65558"
  },
  {
    lati: 49.7457958541, longi: 8.10355861097,
    name: "Alzey",
    plz: "55232"
  },
  {
    lati: 49.105258881, longi: 13.1842309996,
    name: "Bayerisch Eisenstein",
    plz: "94252"
  },
  {
    lati: 51.0601283152, longi: 13.8462498359,
    name: "Dresden",
    plz: "01324"
  },
  {
    lati: 48.5235346508, longi: 11.294565638,
    name: "Aresing",
    plz: "86561"
  },
  {
    lati: 49.2966624294, longi: 10.397385457,
    name: "Leutershausen",
    plz: "91578"
  },
  {
    lati: 49.9252938972, longi: 10.631820875,
    name: "Oberaurach",
    plz: "97514"
  },
  {
    lati: 53.3573569434, longi: 11.7588920376,
    name: "Spornitz",
    plz: "19372"
  },
  {
    lati: 51.6232006518, longi: 7.39942195662,
    name: "Waltrop",
    plz: "45731"
  },
  {
    lati: 50.6159062895, longi: 12.6640718026,
    name: "Schlema",
    plz: "08301"
  },
  {
    lati: 49.8337634266, longi: 9.2014278399,
    name: "Elsenfeld",
    plz: "63820"
  },
  {
    lati: 48.8090472091, longi: 13.0962962476,
    name: "Auerbach",
    plz: "94530"
  },
  {
    lati: 49.0773289403, longi: 9.99572336085,
    name: "Frankenhardt",
    plz: "74586"
  },
  {
    lati: 50.1492909245, longi: 10.1351863143,
    name: "Oerlenbach",
    plz: "97714"
  },
  {
    lati: 51.490823095, longi: 7.56060007651,
    name: "Dortmund",
    plz: "44287"
  },
  {
    lati: 50.0754851415, longi: 7.84418191804,
    name: "Lorch",
    plz: "65391"
  },
  {
    lati: 49.336487922, longi: 8.09100605267,
    name: "Neustadt an der Weinstraße",
    plz: "67434"
  },
  {
    lati: 54.3078172652, longi: 8.88853903903,
    name: "Tönning",
    plz: "25832"
  },
  {
    lati: 52.4584223792, longi: 13.6355210508,
    name: "Berlin Wiesengrund",
    plz: "12587"
  },
  {
    lati: 50.1775112039, longi: 11.515229642,
    name: "Stadtsteinach",
    plz: "95346"
  },
  {
    lati: 48.2438985678, longi: 11.8336967073,
    name: "Neuching",
    plz: "85467"
  },
  {
    lati: 49.3742977021, longi: 6.62494762357,
    name: "Rehlingen-Siersburg",
    plz: "66780"
  },
  {
    lati: 50.8738680383, longi: 6.85887045324,
    name: "Hürth",
    plz: "50354"
  },
  {
    lati: 49.1118341627, longi: 9.11215732249,
    name: "Nordheim",
    plz: "74226"
  },
  {
    lati: 48.9328550732, longi: 9.15640141028,
    name: "Bietigheim-Bissingen",
    plz: "74321"
  },
  {
    lati: 53.5587606877, longi: 9.99411274837,
    name: "Hamburg",
    plz: "20354"
  },
  {
    lati: 48.8720743484, longi: 12.87152229,
    name: "Offenberg",
    plz: "94560"
  },
  {
    lati: 52.5094794322, longi: 13.3147161194,
    name: "Berlin Charlottenburg",
    plz: "10625"
  },
  {
    lati: 52.491385575, longi: 13.8525048285,
    name: "Rüdersdorf",
    plz: "15378"
  },
  {
    lati: 51.5751631101, longi: 6.79772586185,
    name: "Dinslaken",
    plz: "46539"
  },
  {
    lati: 51.2732678248, longi: 7.10400489393,
    name: "Wuppertal",
    plz: "42113"
  },
  {
    lati: 51.4130744439, longi: 7.41747584221,
    name: "Herdecke",
    plz: "58313"
  },
  {
    lati: 54.182043676, longi: 7.88452944246,
    name: "Helgoland",
    plz: "27498"
  },
  {
    lati: 53.3975906487, longi: 9.23347853879,
    name: "Selsingen",
    plz: "27446"
  },
  {
    lati: 52.238265295, longi: 9.7627030359,
    name: "Pattensen",
    plz: "30982"
  },
  {
    lati: 53.1462621248, longi: 10.1349479897,
    name: "Soderstorf",
    plz: "21388"
  },
  {
    lati: 49.6962646484, longi: 11.185694963,
    name: "Leutenbach",
    plz: "91359"
  },
  {
    lati: 48.8354676116, longi: 13.2232010575,
    name: "Schöfweg",
    plz: "94572"
  },
  {
    lati: 50.904031581, longi: 14.7761593223,
    name: "Zittau u.a.",
    plz: "02763"
  },
  {
    lati: 51.6154151022, longi: 6.37510002981,
    name: "Sonsbeck",
    plz: "47665"
  },
  {
    lati: 50.1071434948, longi: 8.66640342811,
    name: "Frankfurt am Main",
    plz: "60329"
  },
  {
    lati: 49.2502247569, longi: 7.50815337666,
    name: "Rieschweiler-Mühlbach",
    plz: "66509"
  },
  {
    lati: 49.9415904324, longi: 7.73590394367,
    name: "Seibersbach",
    plz: "55444"
  },
  {
    lati: 49.8594949505, longi: 11.9282521394,
    name: "Kemnath",
    plz: "95478"
  },
  {
    lati: 51.3305132805, longi: 12.4039192773,
    name: "Leipzig",
    plz: "04317"
  },
  {
    lati: 51.0457623048, longi: 6.87601361994,
    name: "Köln",
    plz: "50769"
  },
  {
    lati: 49.9285765575, longi: 6.8443246419,
    name: "Salmtal",
    plz: "54528"
  },
  {
    lati: 51.8938566367, longi: 11.5300080078,
    name: "Staßfurt",
    plz: "39446"
  },
  {
    lati: 47.5963368443, longi: 11.5343947133,
    name: "Lenggries",
    plz: "83661"
  },
  {
    lati: 49.1218074974, longi: 12.5413240636,
    name: "Michelsneukirchen",
    plz: "93185"
  },
  {
    lati: 50.9024765113, longi: 14.1938716414,
    name: "Bad Schandau",
    plz: "01814"
  },
  {
    lati: 53.5334289018, longi: 12.3749132859,
    name: "Nossentiner Hütte",
    plz: "17214"
  },
  {
    lati: 48.9699370823, longi: 13.1223243702,
    name: "Regen",
    plz: "94209"
  },
  {
    lati: 49.9013085947, longi: 10.8026881412,
    name: "Bischberg",
    plz: "96120"
  },
  {
    lati: 51.675336431, longi: 11.5806853942,
    name: "Arnstein",
    plz: "06456"
  },
  {
    lati: 47.8872082266, longi: 10.1068559225,
    name: "Lautrach",
    plz: "87763"
  },
  {
    lati: 47.6223074338, longi: 8.12163462821,
    name: "Albbruck",
    plz: "79774"
  },
  {
    lati: 49.7348252018, longi: 6.59541924475,
    name: "Trier",
    plz: "54294"
  },
  {
    lati: 49.8609094571, longi: 6.98040199775,
    name: "Wintrich",
    plz: "54487"
  },
  {
    lati: 51.4847207518, longi: 7.00836174631,
    name: "Essen",
    plz: "45326"
  },
  {
    lati: 49.3189667783, longi: 8.89789635906,
    name: "Neidenstein",
    plz: "74933"
  },
  {
    lati: 49.1431654486, longi: 8.31822450474,
    name: "Kuhardt",
    plz: "76773"
  },
  {
    lati: 49.1567443988, longi: 9.43854782423,
    name: "Bretzfeld",
    plz: "74626"
  },
  {
    lati: 48.2607116667, longi: 10.8853998354,
    name: "Königsbrunn",
    plz: "86343"
  },
  {
    lati: 49.0031989121, longi: 12.9680842527,
    name: "Patersdorf",
    plz: "94265"
  },
  {
    lati: 50.5214464907, longi: 12.1177425327,
    name: "Plauen",
    plz: "08525"
  },
  {
    lati: 51.5613802685, longi: 13.2319159091,
    name: "Falkenberg/ Elster",
    plz: "04895"
  },
  {
    lati: 51.0260809883, longi: 13.8575688116,
    name: "Dresden",
    plz: "01326"
  },
  {
    lati: 50.1910823757, longi: 11.3538206248,
    name: "Weißenbrunn",
    plz: "96369"
  },
  {
    lati: 51.8501449349, longi: 6.24842232228,
    name: "Emmerich am Rhein",
    plz: "46446"
  },
  {
    lati: 51.5965475749, longi: 6.48874419493,
    name: "Alpen",
    plz: "46519"
  },
  {
    lati: 51.3291855191, longi: 6.64621409416,
    name: "Krefeld",
    plz: "47809"
  },
  {
    lati: 49.6681324702, longi: 7.03732662952,
    name: "Neuhütten",
    plz: "54422"
  },
  {
    lati: 51.5530914454, longi: 7.3665538577,
    name: "Dortmund",
    plz: "44357"
  },
  {
    lati: 48.7347772142, longi: 9.64314828843,
    name: "Rechberghausen",
    plz: "73098"
  },
  {
    lati: 48.7233731092, longi: 9.31054485941,
    name: "Esslingen am Neckar",
    plz: "73734"
  },
  {
    lati: 48.2684046164, longi: 9.38391718244,
    name: "Pfronstetten",
    plz: "72539"
  },
  {
    lati: 49.4274439334, longi: 11.0918009458,
    name: "Nürnberg",
    plz: "90461"
  },
  {
    lati: 52.6570218908, longi: 11.7061242815,
    name: "Bismark, Rochau",
    plz: "39579"
  },
  {
    lati: 48.6182300721, longi: 10.2415038657,
    name: "Giengen an der Brenz",
    plz: "89537"
  },
  {
    lati: 52.6196289711, longi: 10.7098674424,
    name: "Schönewörde",
    plz: "29396"
  },
  {
    lati: 48.0530164146, longi: 12.1036159639,
    name: "Pfaffing",
    plz: "83539"
  },
  {
    lati: 51.186449425, longi: 14.2048138152,
    name: "Burkau",
    plz: "01906"
  },
  {
    lati: 50.4521339787, longi: 7.1850553022,
    name: "Niederzissen",
    plz: "56651"
  },
  {
    lati: 47.656592975, longi: 11.3788628403,
    name: "Kochel a. See",
    plz: "82431"
  },
  {
    lati: 50.0968988509, longi: 11.6535337755,
    name: "Marktschorgast",
    plz: "95509"
  },
  {
    lati: 50.0118663835, longi: 7.648688526,
    name: "Rheinböllen",
    plz: "55494"
  },
  {
    lati: 49.2977095604, longi: 8.57216977998,
    name: "Reilingen",
    plz: "68799"
  },
  {
    lati: 51.3277204529, longi: 9.33331412544,
    name: "Habichtswald",
    plz: "34317"
  },
  {
    lati: 53.862844293, longi: 10.2585590629,
    name: "Leezen",
    plz: "23816"
  },
  {
    lati: 48.2271260674, longi: 10.9467127553,
    name: "Schmiechen",
    plz: "86511"
  },
  {
    lati: 48.7334108573, longi: 9.02106165723,
    name: "Sindelfingen",
    plz: "71063"
  },
  {
    lati: 51.4398403366, longi: 12.1025538254,
    name: "Kabelsketal",
    plz: "06184"
  },
  {
    lati: 47.9040913698, longi: 12.301311211,
    name: "Bad Endorf",
    plz: "83093"
  },
  {
    lati: 51.2860955818, longi: 12.729287858,
    name: "Trebsen/Mulde",
    plz: "04687"
  },
  {
    lati: 53.6529778881, longi: 10.1637626681,
    name: "Hamburg",
    plz: "22359"
  },
  {
    lati: 53.564015172, longi: 9.87210605868,
    name: "Hamburg",
    plz: "22609"
  },
  {
    lati: 49.5705696696, longi: 11.4243796539,
    name: "Kirchensittenbach",
    plz: "91241"
  },
  {
    lati: 50.8633876811, longi: 13.8427544994,
    name: "Liebstadt",
    plz: "01825"
  },
  {
    lati: 51.190957472, longi: 14.4141121104,
    name: "Bautzen",
    plz: "02625"
  },
  {
    lati: 52.2359642156, longi: 7.37616731326,
    name: "Neuenkirchen",
    plz: "48485"
  },
  {
    lati: 49.6729533479, longi: 6.51295083231,
    name: "Tawern",
    plz: "54456"
  },
  {
    lati: 47.9874229183, longi: 7.86176621899,
    name: "Freiburg im Breisgau",
    plz: "79102"
  },
  {
    lati: 48.5819054695, longi: 8.19426832586,
    name: "Seebach",
    plz: "77889"
  },
  {
    lati: 48.2317029137, longi: 7.70918453303,
    name: "Rheinhausen",
    plz: "79365"
  },
  {
    lati: 51.8963323991, longi: 9.53393029186,
    name: "Golmbach",
    plz: "37640"
  },
  {
    lati: 50.8863270771, longi: 12.8641170939,
    name: "Chemnitz",
    plz: "09228"
  },
  {
    lati: 48.5244135016, longi: 12.1981450786,
    name: "Landshut",
    plz: "84036"
  },
  {
    lati: 51.2723892889, longi: 14.1335925257,
    name: "Elstra, Oßling u.a.",
    plz: "01920"
  },
  {
    lati: 50.3865154726, longi: 8.06663487543,
    name: "Limburg",
    plz: "65549"
  },
  {
    lati: 49.3669899414, longi: 7.67568332391,
    name: "Krickenbach",
    plz: "67706"
  },
  {
    lati: 49.2002800694, longi: 8.98951913283,
    name: "Kirchardt",
    plz: "74912"
  },
  {
    lati: 49.8160132398, longi: 9.91547541448,
    name: "Würzburg",
    plz: "97080"
  },
  {
    lati: 54.5576371672, longi: 9.90501822658,
    name: "Holzdorf",
    plz: "24364"
  },
  {
    lati: 53.2207285108, longi: 10.6420437833,
    name: "Thomasburg",
    plz: "21401"
  },
  {
    lati: 52.3752739982, longi: 10.7634905937,
    name: "Wolfsburg",
    plz: "38444"
  },
  {
    lati: 50.1576326524, longi: 6.31770895214,
    name: "Pronsfeld",
    plz: "54597"
  },
  {
    lati: 52.782158416, longi: 9.23912611986,
    name: "Eystrup, Hassel u.a.",
    plz: "27324"
  },
  {
    lati: 50.1415998871, longi: 8.61501527959,
    name: "Frankfurt am Main",
    plz: "60488"
  },
  {
    lati: 53.3396425249, longi: 14.3107836672,
    name: "Krackow, Nadrensee",
    plz: "17329"
  },
  {
    lati: 48.5281781514, longi: 12.0903330699,
    name: "Landshut",
    plz: "84034"
  },
  {
    lati: 50.6208530433, longi: 13.110072482,
    name: "Großrückerswalde",
    plz: "09518"
  },
  {
    lati: 52.5088683756, longi: 13.3273754995,
    name: "Berlin Charlottenburg",
    plz: "10623"
  },
  {
    lati: 50.9272050736, longi: 11.0599411902,
    name: "Erfurt",
    plz: "99097"
  },
  {
    lati: 48.9275938405, longi: 11.4693775804,
    name: "Denkendorf",
    plz: "85095"
  },
  {
    lati: 49.9122512163, longi: 11.5076355406,
    name: "Mistelbach",
    plz: "95511"
  },
  {
    lati: 50.3687864819, longi: 7.27340207028,
    name: "Mendig",
    plz: "56743"
  },
  {
    lati: 51.1973121409, longi: 6.84407113711,
    name: "Düsseldorf",
    plz: "40229"
  },
  {
    lati: 49.6437646869, longi: 8.16678034879,
    name: "Offstein",
    plz: "67591"
  },
  {
    lati: 49.4216397785, longi: 8.31464873736,
    name: "Dannstadt-Schauernheim",
    plz: "67125"
  },
  {
    lati: 49.4313440194, longi: 8.53131609595,
    name: "Mannheim",
    plz: "68219"
  },
  {
    lati: 53.7310435378, longi: 14.049079966,
    name: "Ueckermünde",
    plz: "17373"
  },
  {
    lati: 48.3320689475, longi: 9.7732319745,
    name: "Altheim",
    plz: "89605"
  },
  {
    lati: 49.4828580504, longi: 9.91209861475,
    name: "Weikersheim",
    plz: "97990"
  },
  {
    lati: 53.2789371059, longi: 9.71156335612,
    name: "Tostedt, Kakenstorf u.a.",
    plz: "21255"
  },
  {
    lati: 48.5466746019, longi: 10.2247343808,
    name: "Niederstotzingen",
    plz: "89168"
  },
  {
    lati: 48.4068803055, longi: 10.2859571423,
    name: "Kötz",
    plz: "89359"
  },
  {
    lati: 48.4270594105, longi: 12.8194049076,
    name: "Hebertsfelden",
    plz: "84332"
  },
  {
    lati: 51.1273897267, longi: 12.0022171563,
    name: "Teuchern",
    plz: "06682"
  },
  {
    lati: 48.4558701148, longi: 11.24431677,
    name: "Schiltberg",
    plz: "86576"
  },
  {
    lati: 49.6118821252, longi: 11.7984864387,
    name: "Vilseck",
    plz: "92249"
  },
  {
    lati: 50.6389519704, longi: 11.450528621,
    name: "Kamsdorf",
    plz: "07334"
  },
  {
    lati: 48.5681977327, longi: 8.68623790595,
    name: "Rohrdorf",
    plz: "72229"
  },
  {
    lati: 49.6780471653, longi: 6.72578594731,
    name: "Pluwig",
    plz: "54316"
  },
  {
    lati: 49.868127404, longi: 11.7861688463,
    name: "Speichersdorf",
    plz: "95469"
  },
  {
    lati: 48.4630116633, longi: 7.87272900715,
    name: "Schutterwald",
    plz: "77746"
  },
  {
    lati: 49.3691357882, longi: 7.99897954735,
    name: "Esthal",
    plz: "67472"
  },
  {
    lati: 51.8066273315, longi: 9.10318843551,
    name: "Nieheim",
    plz: "33039"
  },
  {
    lati: 53.5612506015, longi: 9.20080096075,
    name: "Estorf",
    plz: "21727"
  },
  {
    lati: 47.7370265588, longi: 9.24636102255,
    name: "Uhldingen-Mühlhofen",
    plz: "88690"
  },
  {
    lati: 51.4081920566, longi: 10.5792810254,
    name: "Sollstedt",
    plz: "99759"
  },
  {
    lati: 49.9206679662, longi: 10.1333663627,
    name: "Schwanfeld",
    plz: "97523"
  },
  {
    lati: 48.8301765829, longi: 12.5302467023,
    name: "Feldkirchen",
    plz: "94351"
  },
  {
    lati: 51.4670366982, longi: 6.85933332244,
    name: "Oberhausen",
    plz: "46045"
  },
  {
    lati: 48.9295100674, longi: 13.3423410502,
    name: "Spiegelau",
    plz: "94518"
  },
  {
    lati: 48.8112682704, longi: 13.4594869517,
    name: "Ringelai",
    plz: "94160"
  },
  {
    lati: 50.8738713504, longi: 14.7622448861,
    name: "Olbersdorf",
    plz: "02785"
  },
  {
    lati: 49.1457435559, longi: 12.774625919,
    name: "Miltach",
    plz: "93468"
  },
  {
    lati: 50.7968327309, longi: 12.8841408005,
    name: "Chemnitz",
    plz: "09122"
  },
  {
    lati: 54.1604268755, longi: 9.00927626077,
    name: "Wöhrden",
    plz: "25797"
  },
  {
    lati: 49.445856544, longi: 9.41120962557,
    name: "Osterburken",
    plz: "74706"
  },
  {
    lati: 47.7437099471, longi: 10.2791630462,
    name: "Kempten (Allgäu)",
    plz: "87439"
  },
  {
    lati: 53.5908866642, longi: 10.3244065391,
    name: "Hamfelde, Kasseburg, Köthel, Rausdorf, Schönberg",
    plz: "22929"
  },
  {
    lati: 48.4243607157, longi: 11.0473282668,
    name: "Obergriesbach",
    plz: "86573"
  },
  {
    lati: 49.8284625193, longi: 8.82527847926,
    name: "Reinheim",
    plz: "64354"
  },
  {
    lati: 49.8007319248, longi: 8.34825336245,
    name: "Guntersblum",
    plz: "67583"
  },
  {
    lati: 49.7331931974, longi: 12.1705280266,
    name: "Neustadt a.d. Waldnaab",
    plz: "92660"
  },
  {
    lati: 49.3177309231, longi: 9.57181391618,
    name: "Forchtenberg",
    plz: "74670"
  },
  {
    lati: 48.2417109597, longi: 9.61106870693,
    name: "Untermarchtal",
    plz: "89617"
  },
  {
    lati: 49.7865484335, longi: 9.53257311065,
    name: "Kreuzwertheim",
    plz: "97892"
  },
  {
    lati: 47.5840495792, longi: 10.4119193347,
    name: "Wertach",
    plz: "87497"
  },
  {
    lati: 48.9737187468, longi: 12.9107591726,
    name: "Achslach",
    plz: "94250"
  },
  {
    lati: 48.4931424092, longi: 12.8402334989,
    name: "Schönau",
    plz: "84337"
  },
  {
    lati: 50.6662344083, longi: 11.4517299535,
    name: "Unterwellenborn",
    plz: "07333"
  },
  {
    lati: 49.2364529783, longi: 7.47868403226,
    name: "Dellfeld",
    plz: "66503"
  },
  {
    lati: 50.0389732822, longi: 8.24168226716,
    name: "Wiesbaden",
    plz: "65203"
  },
  {
    lati: 50.5503076492, longi: 8.41532645698,
    name: "Solms",
    plz: "35606"
  },
  {
    lati: 48.2507395244, longi: 12.2653978192,
    name: "Schwindegg",
    plz: "84419"
  },
  {
    lati: 50.6545727048, longi: 12.3538741689,
    name: "Neumark",
    plz: "08496"
  },
  {
    lati: 50.6035456008, longi: 10.6508991184,
    name: "Suhl",
    plz: "98529"
  },
  {
    lati: 48.9332333422, longi: 10.7296199549,
    name: "Polsingen",
    plz: "91805"
  },
  {
    lati: 51.5331636142, longi: 7.15067869468,
    name: "Herne",
    plz: "44649"
  },
  {
    lati: 50.5169611571, longi: 7.4254255662,
    name: "Niederbreitbach",
    plz: "56589"
  },
  {
    lati: 50.012075313, longi: 8.31124782072,
    name: "Wiesbaden",
    plz: "55246"
  },
  {
    lati: 50.2594175434, longi: 8.35010203322,
    name: "Waldems",
    plz: "65529"
  },
  {
    lati: 51.9243682256, longi: 8.12096937327,
    name: "Beelen",
    plz: "48361"
  },
  {
    lati: 49.3225580115, longi: 8.70209863218,
    name: "Nußloch",
    plz: "69226"
  },
  {
    lati: 53.962575546, longi: 11.5661634116,
    name: "Neuburg-Steinhausen, Hornstorf",
    plz: "23974"
  },
  {
    lati: 48.3002171199, longi: 10.9139430762,
    name: "Augsburg",
    plz: "86179"
  },
  {
    lati: 54.0887216485, longi: 13.6214043208,
    name: "Lubmin",
    plz: "17509"
  },
  {
    lati: 54.3029276487, longi: 9.76555187616,
    name: "Schacht-Audorf",
    plz: "24790"
  },
  {
    lati: 48.0822796789, longi: 12.5082060485,
    name: "Tacherting",
    plz: "83342"
  },
  {
    lati: 54.0262299185, longi: 9.0416908913,
    name: "Barlt, Busenwurth",
    plz: "25719"
  },
  {
    lati: 49.6999551358, longi: 8.24144443197,
    name: "Westhofen, Bermersheim",
    plz: "67593"
  },
  {
    lati: 49.2534967537, longi: 6.9744131628,
    name: "Saarbrücken",
    plz: "66113"
  },
  {
    lati: 53.5780828286, longi: 7.3784571165,
    name: "Großheide",
    plz: "26532"
  },
  {
    lati: 50.196517792, longi: 7.34957843637,
    name: "Müden",
    plz: "56254"
  },
  {
    lati: 51.3364092977, longi: 6.57692899536,
    name: "Krefeld",
    plz: "47799"
  },
  {
    lati: 52.2138007466, longi: 10.5248685807,
    name: "Braunschweig",
    plz: "38124"
  },
  {
    lati: 49.9961359284, longi: 11.6022942206,
    name: "Bindlach",
    plz: "95463"
  },
  {
    lati: 50.9874179836, longi: 11.9394519897,
    name: "Crossen, Heideland u.a.",
    plz: "07613"
  },
  {
    lati: 54.3284459966, longi: 13.0667295708,
    name: "Stralsund",
    plz: "18435"
  },
  {
    lati: 53.664783624, longi: 10.0721710715,
    name: "Hamburg",
    plz: "22399"
  },
  {
    lati: 52.4180424986, longi: 10.6979040983,
    name: "Wolfsburg",
    plz: "38442"
  },
  {
    lati: 49.6088734378, longi: 7.97561244023,
    name: "Steinbach, Weitersweiler, Bennhausen, Mörsfeld, Würzweiler, Ruppertsecke",
    plz: "67808"
  },
  {
    lati: 51.8611765873, longi: 7.50057681879,
    name: "Senden",
    plz: "48308"
  },
  {
    lati: 49.9687817705, longi: 7.19796366087,
    name: "Enkirch u.a.",
    plz: "56850"
  },
  {
    lati: 50.6399532609, longi: 7.35819105709,
    name: "Windhagen",
    plz: "53578"
  },
  {
    lati: 53.1042009088, longi: 8.22119091178,
    name: "Oldenburg (Oldenburg)",
    plz: "26133"
  },
  {
    lati: 49.1069835331, longi: 9.90514680078,
    name: "Vellberg",
    plz: "74541"
  },
  {
    lati: 52.0727558172, longi: 10.7384935953,
    name: "Roklum",
    plz: "38325"
  },
  {
    lati: 51.1891265226, longi: 12.2349712322,
    name: "Pegau, Elstertrebnitz",
    plz: "04523"
  },
  {
    lati: 53.6130651385, longi: 9.29767522292,
    name: "Himmelpforten",
    plz: "21709"
  },
  {
    lati: 53.8477190301, longi: 9.38824012937,
    name: "Wewelsfleth",
    plz: "25599"
  },
  {
    lati: 48.8384198146, longi: 9.32757258445,
    name: "Waiblingen",
    plz: "71334"
  },
  {
    lati: 48.0060415941, longi: 12.6491230232,
    name: "Palling",
    plz: "83349"
  },
  {
    lati: 48.2802021012, longi: 12.1657456254,
    name: "Dorfen",
    plz: "84405"
  },
  {
    lati: 48.2104817205, longi: 11.575075868,
    name: "München",
    plz: "80937"
  },
  {
    lati: 52.5871698978, longi: 10.5362420928,
    name: "Wesendorf",
    plz: "29392"
  },
  {
    lati: 48.9870204457, longi: 13.3237307085,
    name: "Frauenau",
    plz: "94258"
  },
  {
    lati: 50.9598908975, longi: 14.859193841,
    name: "Zittau",
    plz: "02788"
  },
  {
    lati: 49.8050034107, longi: 9.98812003478,
    name: "Würzburg",
    plz: "97076"
  },
  {
    lati: 47.5768238948, longi: 9.61491818499,
    name: "Nonnenhorn",
    plz: "88149"
  },
  {
    lati: 49.7602430196, longi: 9.7870802452,
    name: "Waldbrunn, Irtenberger Wald",
    plz: "97295"
  },
  {
    lati: 52.5791993246, longi: 6.95802903869,
    name: "Hoogstede",
    plz: "49846"
  },
  {
    lati: 51.1118182746, longi: 7.21791655529,
    name: "Wermelskirchen",
    plz: "42929"
  },
  {
    lati: 49.9267702908, longi: 7.87228591865,
    name: "Guldental",
    plz: "55452"
  },
  {
    lati: 50.3533864357, longi: 7.59492806951,
    name: "Koblenz",
    plz: "56068"
  },
  {
    lati: 49.3433828686, longi: 8.71009452372,
    name: "Leimen",
    plz: "69181"
  },
  {
    lati: 50.0473451258, longi: 8.80971449438,
    name: "Heusenstamm",
    plz: "63150"
  },
  {
    lati: 54.5387291701, longi: 9.63775804196,
    name: "Schaalby, Geelbek",
    plz: "24882"
  },
  {
    lati: 51.3805881931, longi: 10.3061098429,
    name: "Leinefelde-Worbis, Wingerode, Hausen",
    plz: "37327"
  },
  {
    lati: 48.9955581558, longi: 11.8355170511,
    name: "Painten",
    plz: "93351"
  },
  {
    lati: 54.1721986201, longi: 12.0792350671,
    name: "Rostock",
    plz: "18119"
  },
  {
    lati: 50.5774061873, longi: 13.0550044697,
    name: "Annaberg-Buchholz, Mildenau",
    plz: "09456"
  },
  {
    lati: 50.9080612554, longi: 9.56859176234,
    name: "Neuenstein (Hessen)",
    plz: "36286"
  },
  {
    lati: 48.6049769273, longi: 8.67630261424,
    name: "Ebhausen",
    plz: "72224"
  },
  {
    lati: 51.2370315219, longi: 6.81057891223,
    name: "Düsseldorf",
    plz: "40237"
  },
  {
    lati: 49.7818185378, longi: 7.9594144585,
    name: "Gau-Bickelheim",
    plz: "55599"
  },
  {
    lati: 52.2982036904, longi: 8.06680410005,
    name: "Osnabrück",
    plz: "49088"
  },
  {
    lati: 49.8384167967, longi: 8.46921178463,
    name: "Riedstadt",
    plz: "64560"
  },
  {
    lati: 50.1202856914, longi: 8.49218045854,
    name: "Liederbach am Taunus",
    plz: "65835"
  },
  {
    lati: 48.09120705, longi: 9.78850943548,
    name: "Biberach an der Riß",
    plz: "88400"
  },
  {
    lati: 49.8420775604, longi: 10.1972339135,
    name: "Sommerach",
    plz: "97334"
  },
  {
    lati: 50.7467800296, longi: 12.1589059955,
    name: "Berga/Elster",
    plz: "07980"
  },
  {
    lati: 50.5501553832, longi: 12.1351403116,
    name: "Jößnitz",
    plz: "08547"
  },
  {
    lati: 50.3337206211, longi: 10.894852013,
    name: "Meeder",
    plz: "96484"
  },
  {
    lati: 51.0637753767, longi: 11.2988693748,
    name: "Berlstedt",
    plz: "99439"
  },
  {
    lati: 49.759218684, longi: 7.71301027646,
    name: "Odernheim am Glan",
    plz: "55571"
  },
  {
    lati: 50.2928305432, longi: 8.50609044804,
    name: "Neu-Anspach",
    plz: "61267"
  },
  {
    lati: 48.4146588879, longi: 8.10573193574,
    name: "Nordrach",
    plz: "77787"
  },
  {
    lati: 49.5915003182, longi: 6.68885842815,
    name: "Zerf",
    plz: "54314"
  },
  {
    lati: 53.9173333089, longi: 9.01299034185,
    name: "Neufeld, Schmedeswurth",
    plz: "25724"
  },
  {
    lati: 54.042975597, longi: 9.12697779598,
    name: "Windbergen",
    plz: "25729"
  },
  {
    lati: 54.6865031701, longi: 8.92049617618,
    name: "Langenhorn, Ockholm u.a.",
    plz: "25842"
  },
  {
    lati: 51.9558332829, longi: 9.62205711789,
    name: "Lüerdissen",
    plz: "37635"
  },
  {
    lati: 54.397412613, longi: 9.96292429439,
    name: "Gettorf u.a.",
    plz: "24214"
  },
  {
    lati: 48.2498236263, longi: 7.76912854936,
    name: "Ringsheim",
    plz: "77975"
  },
  {
    lati: 50.1122960027, longi: 8.65271630526,
    name: "Frankfurt",
    plz: "60308"
  },
  {
    lati: 54.8710201889, longi: 8.76934668092,
    name: "Neukirchen, Aventoft",
    plz: "25927"
  },
  {
    lati: 51.0703459904, longi: 7.00095382356,
    name: "Leverkusen",
    plz: "51379"
  },
  {
    lati: 48.1186682118, longi: 11.6608812308,
    name: "München",
    plz: "81825"
  },
  {
    lati: 47.9038737489, longi: 9.46880077057,
    name: "Unterwaldhausen",
    plz: "88379"
  },
  {
    lati: 52.4464794755, longi: 13.3994223999,
    name: "Berlin Mariendorf",
    plz: "12109"
  },
  {
    lati: 49.8008793685, longi: 9.59751043815,
    name: "Triefenstein",
    plz: "97855"
  },
  {
    lati: 49.6170290189, longi: 9.76515062326,
    name: "Grünsfeld",
    plz: "97947"
  },
  {
    lati: 52.8690412284, longi: 10.7768271344,
    name: "Soltendiek",
    plz: "29594"
  },
  {
    lati: 48.5256198124, longi: 10.3536535213,
    name: "Gundelfingen a.d. Donau",
    plz: "89423"
  },
  {
    lati: 51.5782499762, longi: 6.73916882573,
    name: "Dinslaken",
    plz: "46537"
  },
  {
    lati: 47.8757965246, longi: 7.65768858077,
    name: "Heitersheim",
    plz: "79423"
  },
  {
    lati: 48.2237864738, longi: 10.0512764483,
    name: "Dietenheim",
    plz: "89165"
  },
  {
    lati: 53.8426758137, longi: 10.8865597953,
    name: "Schönberg",
    plz: "23923"
  },
  {
    lati: 48.1907961544, longi: 10.2654378791,
    name: "Kettershausen",
    plz: "86498"
  },
  {
    lati: 53.9898479345, longi: 13.5474037372,
    name: "Karlsburg",
    plz: "17495"
  },
  {
    lati: 47.9538994662, longi: 7.83387863609,
    name: "Au (Breisgau)",
    plz: "79280"
  },
  {
    lati: 49.2696910781, longi: 9.08795229276,
    name: "Siegelsbach",
    plz: "74936"
  },
  {
    lati: 54.4375768775, longi: 11.0867749272,
    name: "Fehmarn",
    plz: "23769"
  },
  {
    lati: 48.2232407839, longi: 11.1136194824,
    name: "Hattenhofen",
    plz: "82285"
  },
  {
    lati: 54.1246988019, longi: 10.8202135844,
    name: "Neustadt",
    plz: "23730"
  },
  {
    lati: 51.0246890753, longi: 7.36279638388,
    name: "Lindlar",
    plz: "51789"
  },
  {
    lati: 49.6092511699, longi: 8.14808961555,
    name: "Kindenheim",
    plz: "67271"
  },
  {
    lati: 49.312284382, longi: 11.8176475526,
    name: "Hohenburg",
    plz: "92277"
  },
  {
    lati: 52.9860702597, longi: 10.1086797327,
    name: "Munster",
    plz: "29633"
  },
  {
    lati: 49.7333782746, longi: 8.94578202336,
    name: "Brombachtal",
    plz: "64753"
  },
  {
    lati: 53.1096349666, longi: 9.1097642027,
    name: "Ottersberg",
    plz: "28870"
  },
  {
    lati: 48.6300932811, longi: 9.13071844788,
    name: "Waldenbuch",
    plz: "71111"
  },
  {
    lati: 50.6349994617, longi: 6.81803881245,
    name: "Euskirchen",
    plz: "53881"
  },
  {
    lati: 49.4183180724, longi: 12.625980393,
    name: "Treffelstein",
    plz: "93492"
  },
  {
    lati: 48.8733256159, longi: 12.792935633,
    name: "Niederwinkling",
    plz: "94559"
  },
  {
    lati: 53.341040562, longi: 13.0704279409,
    name: "Neustrelitz",
    plz: "17235"
  },
  {
    lati: 54.1616780393, longi: 10.4336216381,
    name: "Plön",
    plz: "24306"
  },
  {
    lati: 48.9272459881, longi: 8.40613088533,
    name: "Ettlingen",
    plz: "76275"
  },
  {
    lati: 51.8973934134, longi: 8.37688964408,
    name: "Gütersloh",
    plz: "33330"
  },
  {
    lati: 48.505937435, longi: 11.1809774004,
    name: "Kühbach",
    plz: "86556"
  },
  {
    lati: 48.6367306388, longi: 11.2499535964,
    name: "Berg im Gau",
    plz: "86562"
  },
  {
    lati: 48.2259196134, longi: 11.070433653,
    name: "Althegnenberg",
    plz: "82278"
  },
  {
    lati: 51.4196638272, longi: 6.75205251347,
    name: "Duisburg",
    plz: "47053"
  },
  {
    lati: 53.3745196534, longi: 12.6231222879,
    name: "Röbel/ Müritz",
    plz: "17207"
  },
  {
    lati: 48.1784436267, longi: 9.08054418185,
    name: "Straßberg",
    plz: "72479"
  },
  {
    lati: 49.7799552074, longi: 8.25240555648,
    name: "Hillesheim",
    plz: "67586"
  },
  {
    lati: 50.9507192851, longi: 13.6660840711,
    name: "Rabenau",
    plz: "01734"
  },
  {
    lati: 53.3564155436, longi: 8.25331450982,
    name: "Jade",
    plz: "26349"
  },
  {
    lati: 50.1199304747, longi: 9.72989535722,
    name: "Gräfendorf",
    plz: "97782"
  },
  {
    lati: 48.6908451136, longi: 11.0384430072,
    name: "Burgheim",
    plz: "86666"
  },
  {
    lati: 50.784319937, longi: 6.21466894908,
    name: "Stolberg (Rhld.)",
    plz: "52222"
  },
  {
    lati: 51.2419162217, longi: 7.12604359095,
    name: "Wuppertal",
    plz: "42117"
  },
  {
    lati: 49.0418733724, longi: 9.02798341968,
    name: "Cleebronn",
    plz: "74389"
  },
  {
    lati: 48.8746401299, longi: 11.3158820653,
    name: "Hitzhofen",
    plz: "85122"
  },
  {
    lati: 50.0955193009, longi: 6.56237281569,
    name: "Badem, Gindorf, Neidenbach",
    plz: "54657"
  },
  {
    lati: 49.6785284642, longi: 7.1669710657,
    name: "Brücken, Oberbrombach u.a.",
    plz: "55767"
  },
  {
    lati: 48.4828882651, longi: 8.18032968439,
    name: "Oppenau",
    plz: "77728"
  },
  {
    lati: 53.8326977896, longi: 9.10993197317,
    name: "Balje",
    plz: "21730"
  },
  {
    lati: 49.2284994851, longi: 8.25196584321,
    name: "Zeiskam",
    plz: "67378"
  },
  {
    lati: 49.3624736983, longi: 8.32166804747,
    name: "Böhl-Iggelheim",
    plz: "67459"
  },
  {
    lati: 53.588838255, longi: 9.70429554269,
    name: "Wedel",
    plz: "22880"
  },
  {
    lati: 50.2733674771, longi: 10.0692835335,
    name: "Bad Bocklet",
    plz: "97708"
  },
  {
    lati: 48.8068137622, longi: 9.6106502253,
    name: "Plüderhausen",
    plz: "73655"
  },
  {
    lati: 50.9995611893, longi: 13.7851733363,
    name: "Dresden",
    plz: "01239"
  },
  {
    lati: 51.047837673, longi: 14.8267756981,
    name: "Bernstadt",
    plz: "02748"
  },
  {
    lati: 50.8197287587, longi: 8.70408882506,
    name: "Marburg",
    plz: "35041"
  },
  {
    lati: 48.4512520059, longi: 7.81007912598,
    name: "Neuried",
    plz: "77743"
  },
  {
    lati: 50.6025578781, longi: 7.83023524306,
    name: "Malberg, Norken, Höchstenbach u.a.",
    plz: "57629"
  },
  {
    lati: 50.0923306102, longi: 8.27643723917,
    name: "Wiesbaden",
    plz: "65191"
  },
  {
    lati: 51.3550803025, longi: 12.6886367846,
    name: "Bennewitz, Machern",
    plz: "04828"
  },
  {
    lati: 49.944730294, longi: 12.2773094838,
    name: "Mitterteich",
    plz: "95666"
  },
  {
    lati: 49.9499069256, longi: 8.01266925183,
    name: "Gau-Algesheim",
    plz: "55435"
  },
  {
    lati: 49.4515922236, longi: 8.67940571607,
    name: "Dossenheim",
    plz: "69221"
  },
  {
    lati: 50.5790780638, longi: 8.72193448984,
    name: "Gießen",
    plz: "35394"
  },
  {
    lati: 51.6011851176, longi: 6.90899589971,
    name: "Bottrop",
    plz: "46244"
  },
  {
    lati: 49.2290488579, longi: 6.9152420597,
    name: "Saarbrücken",
    plz: "66128"
  },
  {
    lati: 50.864616544, longi: 9.21391686995,
    name: "Willingshausen",
    plz: "34628"
  },
  {
    lati: 51.912196631, longi: 9.58458086591,
    name: "Holenberg",
    plz: "37642"
  },
  {
    lati: 52.3370984799, longi: 9.77565366086,
    name: "Hannover",
    plz: "30519"
  },
  {
    lati: 50.7600274013, longi: 11.095049282,
    name: "Stadtilm, Ilmtal",
    plz: "99326"
  },
  {
    lati: 51.3769716083, longi: 12.3041192502,
    name: "Leipzig",
    plz: "04159"
  },
  {
    lati: 50.9398512201, longi: 14.2909562366,
    name: "Sebnitz",
    plz: "01855"
  },
  {
    lati: 51.9589026393, longi: 14.5703832601,
    name: "Guben, Schenkendöbern",
    plz: "03172"
  },
  {
    lati: 48.7846288822, longi: 9.21354654116,
    name: "Stuttgart",
    plz: "70188"
  },
  {
    lati: 53.510462015, longi: 13.0764049638,
    name: "Penzlin",
    plz: "17217"
  },
  {
    lati: 48.0881572014, longi: 12.2954641363,
    name: "Babensham",
    plz: "83547"
  },
  {
    lati: 53.1038673222, longi: 10.5316221788,
    name: "Jelmstorf",
    plz: "29585"
  },
  {
    lati: 53.9865337669, longi: 11.1997708258,
    name: "Boltenhagen",
    plz: "23946"
  },
  {
    lati: 50.0360803251, longi: 9.35164537979,
    name: "Heigenbrücken",
    plz: "63869"
  },
  {
    lati: 48.8716645817, longi: 9.41720277418,
    name: "Winnenden",
    plz: "71364"
  },
  {
    lati: 50.0224157204, longi: 9.4533241376,
    name: "Wiesthal",
    plz: "97859"
  },
  {
    lati: 54.8073462208, longi: 9.53319741887,
    name: "Wees",
    plz: "24999"
  },
  {
    lati: 52.1433972325, longi: 10.2158953612,
    name: "Burgdorf",
    plz: "38272"
  },
  {
    lati: 49.503824614, longi: 8.66240474109,
    name: "Hirschberg an der Bergstraße",
    plz: "69493"
  },
  {
    lati: 50.5857560425, longi: 8.85843646267,
    name: "Reiskirchen",
    plz: "35447"
  },
  {
    lati: 49.428467608, longi: 8.48236385336,
    name: "Altrip",
    plz: "67122"
  },
  {
    lati: 50.0848619427, longi: 6.37417971868,
    name: "Waxweiler",
    plz: "54649"
  },
  {
    lati: 51.4103062746, longi: 9.35649199561,
    name: "Calden",
    plz: "34379"
  },
  {
    lati: 50.9956028254, longi: 9.5125678688,
    name: "Knüllwald",
    plz: "34593"
  },
  {
    lati: 48.2471139395, longi: 7.96758736723,
    name: "Schuttertal",
    plz: "77978"
  },
  {
    lati: 51.486356384, longi: 7.58957737492,
    name: "Dortmund",
    plz: "44289"
  },
  {
    lati: 53.5662202263, longi: 9.94594210402,
    name: "Hamburg",
    plz: "22769"
  },
  {
    lati: 53.2722497846, longi: 10.1017079042,
    name: "Toppenstedt",
    plz: "21442"
  },
  {
    lati: 54.4972801214, longi: 9.62585764033,
    name: "Fahrdorf",
    plz: "24857"
  },
  {
    lati: 47.9889071072, longi: 10.7020078728,
    name: "Jengen",
    plz: "86860"
  },
  {
    lati: 47.661352669, longi: 10.5494752078,
    name: "Rückholz",
    plz: "87494"
  },
  {
    lati: 51.3786692845, longi: 6.57519045841,
    name: "Krefeld",
    plz: "47802"
  },
  {
    lati: 48.1908033785, longi: 11.661852064,
    name: "Unterföhring",
    plz: "85774"
  },
  {
    lati: 53.5568192458, longi: 7.88037101307,
    name: "Jever",
    plz: "26441"
  },
  {
    lati: 49.069017821, longi: 9.76745451914,
    name: "Michelbach an der Bilz",
    plz: "74544"
  },
  {
    lati: 48.9122559105, longi: 9.96180614269,
    name: "Abtsgmünd",
    plz: "73453"
  },
  {
    lati: 48.1342654843, longi: 11.2050794336,
    name: "Schöngeising",
    plz: "82296"
  },
  {
    lati: 48.1906211735, longi: 9.97841865083,
    name: "Schwendi",
    plz: "88477"
  },
  {
    lati: 48.9133536598, longi: 12.9075414,
    name: "Bernried",
    plz: "94505"
  },
  {
    lati: 49.4588120382, longi: 10.8495468391,
    name: "Cadolzburg",
    plz: "90556"
  },
  {
    lati: 48.8594185378, longi: 10.3436391859,
    name: "Bopfingen",
    plz: "73441"
  },
  {
    lati: 51.5213337568, longi: 14.0228649039,
    name: "Senftenberg",
    plz: "01968"
  },
  {
    lati: 51.2941288423, longi: 6.8975483701,
    name: "Ratingen",
    plz: "40882"
  },
  {
    lati: 48.0138279531, longi: 7.85391393961,
    name: "Freiburg im Breisgau",
    plz: "79106"
  },
  {
    lati: 52.2680827655, longi: 8.1228122801,
    name: "Osnabrück",
    plz: "49086"
  },
  {
    lati: 49.2730946492, longi: 8.27381640089,
    name: "Freisbach",
    plz: "67361"
  },
  {
    lati: 49.0121642725, longi: 8.38947467485,
    name: "Karlsruhe",
    plz: "76133"
  },
  {
    lati: 49.4001355784, longi: 8.52019973692,
    name: "Brühl",
    plz: "68782"
  },
  {
    lati: 47.6162365437, longi: 9.56255955547,
    name: "Langenargen",
    plz: "88085"
  },
  {
    lati: 51.9738193722, longi: 9.99825892273,
    name: "Lamspringe",
    plz: "31195"
  },
  {
    lati: 52.5784882218, longi: 10.2040457581,
    name: "Wienhausen",
    plz: "29342"
  },
  {
    lati: 49.2907872239, longi: 12.4179550708,
    name: "Neukirchen-Balbini",
    plz: "92445"
  },
  {
    lati: 51.9641095368, longi: 13.0874410241,
    name: "Jüterbog",
    plz: "14913"
  },
  {
    lati: 52.3454864021, longi: 14.4673927998,
    name: "Frankfurt/ Oder",
    plz: "15234"
  },
  {
    lati: 53.0444385983, longi: 11.0561613549,
    name: "Jameln",
    plz: "29479"
  },
  {
    lati: 50.9231864788, longi: 6.96359112641,
    name: "Köln",
    plz: "50678"
  },
  {
    lati: 51.239439427, longi: 6.78550899883,
    name: "Düsseldorf",
    plz: "40477"
  },
  {
    lati: 48.6340091251, longi: 8.03733147748,
    name: "Achern (Abweichung Exklaven)",
    plz: "77855"
  },
  {
    lati: 50.0590565799, longi: 8.12549673894,
    name: "Eltville am Rhein",
    plz: "65344"
  },
  {
    lati: 50.3535869205, longi: 8.86077717366,
    name: "Reichelsheim (Wetterau)",
    plz: "61203"
  },
  {
    lati: 48.5219745819, longi: 9.00607221924,
    name: "Tübingen",
    plz: "72070"
  },
  {
    lati: 49.3974118519, longi: 9.01940184361,
    name: "Neunkirchen",
    plz: "74867"
  },
  {
    lati: 50.809529498, longi: 12.9097784375,
    name: "Chemnitz",
    plz: "09120"
  },
  {
    lati: 48.7600165031, longi: 12.9665370054,
    name: "Moos",
    plz: "94554"
  },
  {
    lati: 48.8262903359, longi: 11.3150086564,
    name: "Eitensheim",
    plz: "85117"
  },
  {
    lati: 49.59814138, longi: 7.99981611334,
    name: "Dreisen, Standenbühl",
    plz: "67816"
  },
  {
    lati: 50.615576144, longi: 8.07834650676,
    name: "Oberrod u.a.",
    plz: "56479"
  },
  {
    lati: 51.6022253621, longi: 7.46592973359,
    name: "Lünen",
    plz: "44536"
  },
  {
    lati: 50.2889373496, longi: 7.71280632201,
    name: "Dausenau, Nievern",
    plz: "56132"
  },
  {
    lati: 47.8398495792, longi: 8.9336536094,
    name: "Orsingen-Nenzingen",
    plz: "78359"
  },
  {
    lati: 50.0161655794, longi: 9.06515403591,
    name: "Kleinostheim",
    plz: "63801"
  },
  {
    lati: 49.2101825674, longi: 9.19453168504,
    name: "Untereisesheim",
    plz: "74257"
  },
  {
    lati: 52.3201190553, longi: 9.96882846823,
    name: "Sehnde",
    plz: "31319"
  },
  {
    lati: 48.8540378446, longi: 11.6365687535,
    name: "Mindelstetten",
    plz: "93349"
  },
  {
    lati: 51.2311333084, longi: 6.82484308751,
    name: "Düsseldorf",
    plz: "40235"
  },
  {
    lati: 50.5630704175, longi: 6.20863299366,
    name: "Monschau",
    plz: "52156"
  },
  {
    lati: 50.8006751019, longi: 6.47102725863,
    name: "Düren",
    plz: "52349"
  },
  {
    lati: 50.7558046124, longi: 7.32946292211,
    name: "Hennef (Sieg)",
    plz: "53773"
  },
  {
    lati: 50.8625796711, longi: 6.66409696759,
    name: "Kerpen",
    plz: "50171"
  },
  {
    lati: 53.3058376529, longi: 9.93511068661,
    name: "Jesteburg",
    plz: "21266"
  },
  {
    lati: 50.4900367245, longi: 10.1559101885,
    name: "Nordheim v.d. Rhön",
    plz: "97647"
  },
  {
    lati: 48.244860763, longi: 10.3791894106,
    name: "Krumbach (Schwaben)",
    plz: "86381"
  },
  {
    lati: 52.3091241512, longi: 10.4749995002,
    name: "Braunschweig",
    plz: "38112"
  },
  {
    lati: 53.3862936905, longi: 10.5241771949,
    name: "Lauenburg/Elbe",
    plz: "21481"
  },
  {
    lati: 50.9645788555, longi: 12.7530339081,
    name: "Lunzenau",
    plz: "09328"
  },
  {
    lati: 51.0196232554, longi: 11.8144036518,
    name: "Schkölen",
    plz: "07619"
  },
  {
    lati: 53.6474763383, longi: 10.0083846281,
    name: "Hamburg",
    plz: "22415"
  },
  {
    lati: 51.4485132948, longi: 7.04156031247,
    name: "Essen",
    plz: "45138"
  },
  {
    lati: 50.9242652045, longi: 6.85667119406,
    name: "Köln",
    plz: "50858"
  },
  {
    lati: 50.820886199, longi: 6.88665798344,
    name: "Brühl",
    plz: "50321"
  },
  {
    lati: 47.9818380773, longi: 9.10336597087,
    name: "Meßkirch, Sauldorf",
    plz: "88605"
  },
  {
    lati: 49.8383427943, longi: 12.0427354536,
    name: "Erbendorf",
    plz: "92681"
  },
  {
    lati: 52.3249140793, longi: 14.0269599865,
    name: "Briesen, Rauen u.a.",
    plz: "15518"
  },
  {
    lati: 49.0475371628, longi: 9.85967529758,
    name: "Obersontheim",
    plz: "74423"
  },
  {
    lati: 52.3868358261, longi: 9.8251217721,
    name: "Hannover",
    plz: "30627"
  },
  {
    lati: 50.938339328, longi: 9.87785695212,
    name: "Ronshausen",
    plz: "36217"
  },
  {
    lati: 48.3869847624, longi: 9.9190202132,
    name: "Ulm",
    plz: "89081"
  },
  {
    lati: 51.0898665344, longi: 10.0733559275,
    name: "Ringgau",
    plz: "37296"
  },
  {
    lati: 48.085863577, longi: 10.2073327757,
    name: "Boos",
    plz: "87737"
  },
  {
    lati: 48.3135776299, longi: 12.380958738,
    name: "Oberbergkirchen",
    plz: "84564"
  },
  {
    lati: 48.1409458003, longi: 11.4641546085,
    name: "München",
    plz: "81241"
  },
  {
    lati: 49.8987887374, longi: 11.5394623683,
    name: "Gesees",
    plz: "95494"
  },
  {
    lati: 49.4711976175, longi: 8.44715823967,
    name: "Ludwigshafen am Rhein",
    plz: "67061"
  },
  {
    lati: 52.1387072099, longi: 8.55375514832,
    name: "Enger",
    plz: "32130"
  },
  {
    lati: 47.8094661093, longi: 9.48223060584,
    name: "Horgenzell",
    plz: "88263"
  },
  {
    lati: 50.8685151145, longi: 7.70590302105,
    name: "Morsbach",
    plz: "51597"
  },
  {
    lati: 48.1575603725, longi: 7.74609915093,
    name: "Riegel",
    plz: "79359"
  },
  {
    lati: 48.3603833269, longi: 13.1917636144,
    name: "Rotthalmünster",
    plz: "94094"
  },
  {
    lati: 51.0374308216, longi: 13.3515902428,
    name: "Reinsberg",
    plz: "09634"
  },
  {
    lati: 47.9570186708, longi: 11.773532831,
    name: "Aying",
    plz: "85653"
  },
  {
    lati: 50.2716191097, longi: 8.00662653038,
    name: "Schönborn",
    plz: "56370"
  },
  {
    lati: 49.3759458749, longi: 8.09231378699,
    name: "Lindenberg",
    plz: "67473"
  },
  {
    lati: 54.7634773305, longi: 9.01313404273,
    name: "Leck",
    plz: "25917"
  },
  {
    lati: 49.5576029135, longi: 9.07486024469,
    name: "Hesseneck",
    plz: "64754"
  },
  {
    lati: 50.0170033807, longi: 10.288474331,
    name: "Gochsheim",
    plz: "97469"
  },
  {
    lati: 49.9870659409, longi: 9.9611383593,
    name: "Arnstein",
    plz: "97450"
  },
  {
    lati: 48.8466216697, longi: 13.4004943227,
    name: "Grafenau",
    plz: "94481"
  },
  {
    lati: 53.903078708, longi: 13.9426411247,
    name: "Usedom u.a.",
    plz: "17406"
  },
  {
    lati: 52.1664841731, longi: 14.5574819772,
    name: "Eisenhüttenstadt",
    plz: "15890"
  },
  {
    lati: 48.9480779354, longi: 12.7312816887,
    name: "Hunderdorf",
    plz: "94336"
  },
  {
    lati: 52.6609838395, longi: 13.2633753335,
    name: "Hohen Neuendorf",
    plz: "16540"
  },
  {
    lati: 51.7398412755, longi: 13.4146344416,
    name: "Schlieben",
    plz: "04936"
  },
  {
    lati: 54.1017730016, longi: 13.8037001091,
    name: "Kröslin, Krummin, Lassan u.a.",
    plz: "17440"
  },
  {
    lati: 50.1176361837, longi: 7.37494585384,
    name: "Kastellaun",
    plz: "56288"
  },
  {
    lati: 49.669970783, longi: 6.67241242865,
    name: "Pellingen",
    plz: "54331"
  },
  {
    lati: 54.3743883012, longi: 9.31482628954,
    name: "Bergenhusen",
    plz: "24861"
  },
  {
    lati: 50.8291832155, longi: 10.3603879085,
    name: "Bad Liebenstein",
    plz: "36448"
  },
  {
    lati: 48.1413114691, longi: 11.5060446018,
    name: "München",
    plz: "80687"
  },
  {
    lati: 52.7523831618, longi: 7.60762403099,
    name: "Lähden",
    plz: "49774"
  },
  {
    lati: 49.434160186, longi: 8.69615782977,
    name: "Heidelberg",
    plz: "69121"
  },
  {
    lati: 53.685713983, longi: 9.14538720118,
    name: "Hemmoor",
    plz: "21745"
  },
  {
    lati: 52.1661871304, longi: 9.79363394978,
    name: "Nordstemmen",
    plz: "31171"
  },
  {
    lati: 48.8293473632, longi: 8.65519358429,
    name: "Engelsbrand",
    plz: "75331"
  },
  {
    lati: 49.454180952, longi: 8.42637773058,
    name: "Ludwigshafen am Rhein",
    plz: "67065"
  },
  {
    lati: 50.1238876381, longi: 11.5657246249,
    name: "Ludwigschorgast",
    plz: "95364"
  },
  {
    lati: 49.4892803427, longi: 10.7824794958,
    name: "Langenzenn",
    plz: "90579"
  },
  {
    lati: 49.303753951, longi: 7.96487730227,
    name: "Rhodt u.a.",
    plz: "76835"
  },
  {
    lati: 48.7151015898, longi: 8.00308318863,
    name: "Lichtenau",
    plz: "77839"
  },
  {
    lati: 51.4849290506, longi: 8.12342923623,
    name: "Möhnesee",
    plz: "59519"
  },
  {
    lati: 51.4577850615, longi: 10.5909712339,
    name: "Bleicherode",
    plz: "99752"
  },
  {
    lati: 54.3689017535, longi: 8.93486481506,
    name: "Oldenswort",
    plz: "25870"
  },
  {
    lati: 53.8003722232, longi: 11.6696182211,
    name: "Warin",
    plz: "19417"
  },
  {
    lati: 48.4970770358, longi: 11.7246885239,
    name: "Attenkirchen",
    plz: "85395"
  },
  {
    lati: 49.5028759824, longi: 11.3454359855,
    name: "Ottensoos",
    plz: "91242"
  },
  {
    lati: 50.760077667, longi: 13.6435353635,
    name: "Hermsdorf",
    plz: "01776"
  },
  {
    lati: 52.3294160835, longi: 9.8420589754,
    name: "Hannover",
    plz: "30539"
  },
  {
    lati: 54.6738213669, longi: 9.95398092907,
    name: "Kappeln",
    plz: "24376"
  },
  {
    lati: 53.6102778485, longi: 7.52541360016,
    name: "Ochtersum",
    plz: "26489"
  },
  {
    lati: 49.049877708, longi: 8.38236897122,
    name: "Karlsruhe",
    plz: "76149"
  },
  {
    lati: 50.5730198912, longi: 7.08275116568,
    name: "Grafschaft",
    plz: "53501"
  },
  {
    lati: 49.1186486644, longi: 11.8213353527,
    name: "Beratzhausen",
    plz: "93176"
  },
  {
    lati: 51.4989319564, longi: 11.9672122324,
    name: "Halle/ Saale",
    plz: "06114"
  },
  {
    lati: 49.5849016782, longi: 10.1587779012,
    name: "Ippesheim",
    plz: "97258"
  },
  {
    lati: 52.5027168185, longi: 13.6167049866,
    name: "Berlin Mahlsdorf",
    plz: "12623"
  },
  {
    lati: 52.3248888658, longi: 13.4104856591,
    name: "Blankenfelde-Mahlow",
    plz: "15827"
  },
  {
    lati: 50.3453961427, longi: 8.0361790061,
    name: "Burgschwalbach",
    plz: "65558"
  },
  {
    lati: 52.664380856, longi: 7.61351417126,
    name: "Herzlake, Dohren",
    plz: "49770"
  },
  {
    lati: 49.4805154538, longi: 8.35706595738,
    name: "Ludwigshafen am Rhein",
    plz: "67071"
  },
  {
    lati: 53.5518599522, longi: 8.3368834141,
    name: "Butjadingen",
    plz: "26969"
  },
  {
    lati: 48.514685101, longi: 9.94939387463,
    name: "Westerstetten",
    plz: "89198"
  },
  {
    lati: 50.0424864714, longi: 10.1449653879,
    name: "Geldersheim",
    plz: "97505"
  },
  {
    lati: 50.1828596379, longi: 11.8597104931,
    name: "Weißdorf",
    plz: "95237"
  },
  {
    lati: 50.2103332537, longi: 11.9260490001,
    name: "Schwarzenbach a.d. Saale",
    plz: "95126"
  },
  {
    lati: 53.1333830847, longi: 12.1339821447,
    name: "Pritzwalk, Groß Pankow",
    plz: "16928"
  },
  {
    lati: 53.6577807744, longi: 11.4344908548,
    name: "Schwerin",
    plz: "19055"
  },
  {
    lati: 49.3857844218, longi: 10.8850614724,
    name: "Roßtal",
    plz: "90574"
  },
  {
    lati: 47.7700616281, longi: 9.29693532827,
    name: "Salem",
    plz: "88682"
  },
  {
    lati: 51.9496301394, longi: 9.39776101605,
    name: "Ottenstein",
    plz: "31868"
  },
  {
    lati: 53.5465159074, longi: 9.97845643964,
    name: "Hamburg",
    plz: "20459"
  },
  {
    lati: 48.070566097, longi: 10.6889156722,
    name: "Amberg",
    plz: "86854"
  },
  {
    lati: 54.2952731918, longi: 10.8583623661,
    name: "Oldenburg in Holstein",
    plz: "23758"
  },
  {
    lati: 51.4529484288, longi: 7.15710952631,
    name: "Bochum",
    plz: "44869"
  },
  {
    lati: 51.8610188558, longi: 11.1019784484,
    name: "Harsleben",
    plz: "38829"
  },
  {
    lati: 48.9196883784, longi: 13.5721738941,
    name: "Mauth",
    plz: "94151"
  },
  {
    lati: 50.8082697245, longi: 13.5777333757,
    name: "Hartmannsdorf-Reichenau",
    plz: "01762"
  },
  {
    lati: 48.217146401, longi: 11.5568127144,
    name: "München",
    plz: "80933"
  },
  {
    lati: 51.5526274798, longi: 7.00434861833,
    name: "Gladbeck",
    plz: "45968"
  },
  {
    lati: 48.7608370381, longi: 8.14382090797,
    name: "Sinzheim",
    plz: "76547"
  },
  {
    lati: 49.9921506159, longi: 9.08778861643,
    name: "Mainaschaff",
    plz: "63814"
  },
  {
    lati: 51.2078282258, longi: 8.67537959102,
    name: "Medebach",
    plz: "59964"
  },
  {
    lati: 49.3507748498, longi: 10.0016847914,
    name: "Schrozberg",
    plz: "74575"
  },
  {
    lati: 51.2298397287, longi: 12.7588580304,
    name: "Grimma",
    plz: "04668"
  },
  {
    lati: 48.4425576639, longi: 10.7118059527,
    name: "Bonstetten",
    plz: "86486"
  },
  {
    lati: 47.6486339669, longi: 10.4392624191,
    name: "Oy-Mittelberg",
    plz: "87466"
  },
  {
    lati: 51.4499543304, longi: 11.9442621351,
    name: "Halle/ Saale",
    plz: "06128"
  },
  {
    lati: 48.7848664938, longi: 12.068442144,
    name: "Herrngiersdorf",
    plz: "84097"
  },
  {
    lati: 52.3701017852, longi: 13.2029010432,
    name: "Kleinmachnow",
    plz: "14532"
  },
  {
    lati: 50.9666623459, longi: 6.619842408,
    name: "Bergheim",
    plz: "50126"
  },
  {
    lati: 49.5649504025, longi: 7.85114471659,
    name: "Winnweiler",
    plz: "67722"
  },
  {
    lati: 50.7197136729, longi: 8.29521648638,
    name: "Dillenburg",
    plz: "35687"
  },
  {
    lati: 52.0765067422, longi: 8.79644070274,
    name: "Bad Salzuflen",
    plz: "32108"
  },
  {
    lati: 52.1004638154, longi: 9.38114940048,
    name: "Hameln",
    plz: "31789"
  },
  {
    lati: 53.9932228338, longi: 9.62400096715,
    name: "Hohenlockstedt",
    plz: "25551"
  },
  {
    lati: 51.4298352076, longi: 9.92880207028,
    name: "Friedland",
    plz: "37133"
  },
  {
    lati: 53.6700768498, longi: 10.1192336884,
    name: "Hamburg",
    plz: "22395"
  },
  {
    lati: 49.6751795731, longi: 10.3153694668,
    name: "Iphofen",
    plz: "97346"
  },
  {
    lati: 48.9397083656, longi: 11.0036423762,
    name: "Pappenheim",
    plz: "91788"
  },
  {
    lati: 51.6046212255, longi: 9.09393344152,
    name: "Willebadessen",
    plz: "34439"
  },
  {
    lati: 49.945109734, longi: 9.16679048025,
    name: "Aschaffenburg",
    plz: "63743"
  },
  {
    lati: 53.1565287276, longi: 13.2050476653,
    name: "Fürstenberg",
    plz: "16798"
  },
  {
    lati: 53.5886692432, longi: 12.0921360399,
    name: "Goldberg",
    plz: "19399"
  },
  {
    lati: 50.0956474146, longi: 12.0765997687,
    name: "Höchstädt",
    plz: "95186"
  },
  {
    lati: 54.1799129613, longi: 12.1952971077,
    name: "Rostock",
    plz: "18146"
  },
  {
    lati: 47.8345730049, longi: 11.4014314241,
    name: "Eurasburg",
    plz: "82547"
  },
  {
    lati: 52.5184683441, longi: 13.3195096382,
    name: "Berlin Charlottenburg",
    plz: "10587"
  },
  {
    lati: 51.3368742447, longi: 7.02303158913,
    name: "Velbert",
    plz: "42549"
  },
  {
    lati: 49.740027453, longi: 6.52031162539,
    name: "Langsur",
    plz: "54308"
  },
  {
    lati: 48.8110710851, longi: 9.15406906976,
    name: "Stuttgart",
    plz: "70469"
  },
  {
    lati: 49.2677390654, longi: 12.7519331915,
    name: "Weiding",
    plz: "93495"
  },
  {
    lati: 48.1305790371, longi: 11.5995100461,
    name: "München",
    plz: "81667"
  },
  {
    lati: 48.9901141367, longi: 9.39042412102,
    name: "Aspach",
    plz: "71546"
  },
  {
    lati: 48.7721858874, longi: 13.07526436,
    name: "Hengersberg",
    plz: "94491"
  },
  {
    lati: 51.3614993906, longi: 13.229872569,
    name: "Strehla",
    plz: "01616"
  },
  {
    lati: 52.328175834, longi: 14.5414731455,
    name: "Frankfurt/ Oder",
    plz: "15232"
  },
  {
    lati: 52.4933177004, longi: 11.1729389684,
    name: "Gardelegen",
    plz: "39649"
  },
  {
    lati: 49.7422393504, longi: 11.2013757451,
    name: "Pretzfeld",
    plz: "91362"
  },
  {
    lati: 49.5097412509, longi: 7.67531614807,
    name: "Katzweiler",
    plz: "67734"
  },
  {
    lati: 50.6173168995, longi: 8.50340178032,
    name: "Wetzlar",
    plz: "35585"
  },
  {
    lati: 47.7116118866, longi: 8.93903488017,
    name: "Moos",
    plz: "78345"
  },
  {
    lati: 49.9268634581, longi: 8.84507978343,
    name: "Münster",
    plz: "64839"
  },
  {
    lati: 52.1064049623, longi: 10.9084635529,
    name: "Söllingen",
    plz: "38387"
  },
  {
    lati: 48.2440301288, longi: 10.9828755302,
    name: "Merching",
    plz: "86504"
  },
  {
    lati: 51.4976316347, longi: 7.11957078695,
    name: "Gelsenkirchen",
    plz: "45886"
  },
  {
    lati: 52.7854149034, longi: 7.43744760396,
    name: "Stavern",
    plz: "49777"
  },
  {
    lati: 49.6572588492, longi: 6.9472896184,
    name: "Deuselbach, Hermeskeil, Rorodt",
    plz: "54411"
  },
  {
    lati: 53.5189456813, longi: 11.081978632,
    name: "Wittenburg u.a.",
    plz: "19243"
  },
  {
    lati: 52.1297436678, longi: 10.6441850049,
    name: "Wittmar",
    plz: "38329"
  },
  {
    lati: 53.9914178851, longi: 10.6663291758,
    name: "Sarkwitz",
    plz: "23629"
  },
  {
    lati: 53.4670868542, longi: 9.97512414834,
    name: "Hamburg",
    plz: "21079"
  },
  {
    lati: 53.4356933701, longi: 9.95046378598,
    name: "Hamburg",
    plz: "21077"
  },
  {
    lati: 53.6264732827, longi: 9.93680116724,
    name: "Hamburg",
    plz: "22459"
  },
  {
    lati: 54.319168461, longi: 10.1791023679,
    name: "Kiel",
    plz: "24148"
  },
  {
    lati: 52.5158848275, longi: 10.2195913513,
    name: "Bröckel",
    plz: "29356"
  },
  {
    lati: 48.2391567157, longi: 12.9577373735,
    name: "Kirchdorf a. Inn",
    plz: "84375"
  },
  {
    lati: 51.364728973, longi: 9.39900784165,
    name: "Ahnatal",
    plz: "34292"
  },
  {
    lati: 54.8318908148, longi: 9.56695216357,
    name: "Glücksburg, Munkbrarup",
    plz: "24960"
  },
  {
    lati: 52.1899178996, longi: 7.03423396155,
    name: "Gronau",
    plz: "48599"
  },
  {
    lati: 47.6793395212, longi: 8.19605764782,
    name: "Weilheim",
    plz: "79809"
  },
  {
    lati: 51.631479694, longi: 7.51268701584,
    name: "Lünen",
    plz: "44534"
  },
  {
    lati: 49.7605136835, longi: 7.66289691073,
    name: "Staudernheim",
    plz: "55568"
  },
  {
    lati: 53.5897470985, longi: 8.61766483959,
    name: "Bremerhaven",
    plz: "27578"
  },
  {
    lati: 50.8527407249, longi: 12.0724566041,
    name: "Gera",
    plz: "07551"
  },
  {
    lati: 47.6451846449, longi: 10.6120822747,
    name: "Seeg",
    plz: "87637"
  },
  {
    lati: 53.3507616073, longi: 9.61237324051,
    name: "Halvesbostel",
    plz: "21646"
  },
  {
    lati: 52.3260889264, longi: 12.9385204326,
    name: "Schwielowswee",
    plz: "14548"
  },
  {
    lati: 48.2274695682, longi: 9.44350021862,
    name: "Zwiefalten",
    plz: "88529"
  },
  {
    lati: 48.7160633453, longi: 9.4672344107,
    name: "Reichenbach an der Fils",
    plz: "73262"
  },
  {
    lati: 52.0050637643, longi: 9.56795266057,
    name: "Halle",
    plz: "37620"
  },
  {
    lati: 49.3006927142, longi: 9.07684594062,
    name: "Hüffenhardt",
    plz: "74928"
  },
  {
    lati: 48.662694317, longi: 9.11981078356,
    name: "Steinenbronn",
    plz: "71144"
  },
  {
    lati: 49.1584048599, longi: 9.22855508645,
    name: "Heilbronn",
    plz: "74076"
  },
  {
    lati: 50.1711005444, longi: 7.96783492142,
    name: "Heidenrod",
    plz: "65321"
  },
  {
    lati: 50.8424213177, longi: 7.99278992224,
    name: "Siegen",
    plz: "57080"
  },
  {
    lati: 49.5751220923, longi: 7.49477690858,
    name: "Rammelsbach u.a.",
    plz: "66887"
  },
  {
    lati: 49.656529389, longi: 7.55700495086,
    name: "Grumbach",
    plz: "67745"
  },
  {
    lati: 51.9439529861, longi: 7.61161183272,
    name: "Münster",
    plz: "48151"
  },
  {
    lati: 49.4241239239, longi: 7.4017770411,
    name: "Altenkirchen",
    plz: "66903"
  },
  {
    lati: 51.597501098, longi: 7.54058220901,
    name: "Lünen",
    plz: "44532"
  },
  {
    lati: 51.5221130009, longi: 6.90912249327,
    name: "Bottrop",
    plz: "46242"
  },
  {
    lati: 48.4829118954, longi: 10.8370517822,
    name: "Langweid a. Lech",
    plz: "86462"
  },
  {
    lati: 48.0499555869, longi: 10.3457478037,
    name: "Erkheim",
    plz: "87746"
  },
  {
    lati: 49.8229140101, longi: 10.5853924202,
    name: "Burgwindheim",
    plz: "96154"
  },
  {
    lati: 52.3976753923, longi: 10.2723856431,
    name: "Edemissen",
    plz: "31234"
  },
  {
    lati: 50.8747047307, longi: 12.0397812316,
    name: "Gera",
    plz: "07548"
  },
  {
    lati: 52.2465466869, longi: 12.3485656048,
    name: "Ziesar",
    plz: "14793"
  },
  {
    lati: 50.8827295035, longi: 12.7946889225,
    name: "Hartmannsdorf",
    plz: "09232"
  },
  {
    lati: 50.1439967664, longi: 7.16718750786,
    name: "Cochem",
    plz: "56812"
  },
  {
    lati: 52.1480668113, longi: 7.20734246971,
    name: "Metelen",
    plz: "48629"
  },
  {
    lati: 50.7008987558, longi: 7.25879019215,
    name: "Königswinter",
    plz: "53639"
  },
  {
    lati: 48.2493798208, longi: 12.5183687507,
    name: "Mühldorf a. Inn",
    plz: "84453"
  },
  {
    lati: 49.3021238009, longi: 9.11948662961,
    name: "Haßmersheim",
    plz: "74855"
  },
  {
    lati: 48.4799614171, longi: 9.1921813352,
    name: "Reutlingen",
    plz: "72762"
  },
  {
    lati: 51.4036296787, longi: 6.98430916317,
    name: "Essen",
    plz: "45133"
  },
  {
    lati: 47.790757448, longi: 10.1773245058,
    name: "Altusried",
    plz: "87452"
  },
  {
    lati: 52.0904241633, longi: 10.6926842429,
    name: "Semmenstedt",
    plz: "38327"
  },
  {
    lati: 49.4340235048, longi: 11.6170382025,
    name: "Birgland",
    plz: "92262"
  },
  {
    lati: 50.2123336085, longi: 7.69625726469,
    name: "Bornich, Patersberg",
    plz: "56348"
  },
  {
    lati: 49.4799248218, longi: 8.48996820524,
    name: "Mannheim",
    plz: "68165"
  },
  {
    lati: 47.8895253385, longi: 11.9336748668,
    name: "Bruckmühl",
    plz: "83052"
  },
  {
    lati: 51.3195847758, longi: 6.56811877743,
    name: "Krefeld",
    plz: "47805"
  },
  {
    lati: 52.7155393293, longi: 9.30721639427,
    name: "Heemsen",
    plz: "31622"
  },
  {
    lati: 53.8270631222, longi: 9.44909833386,
    name: "Borsfleth",
    plz: "25376"
  },
  {
    lati: 52.516920591, longi: 10.3945106902,
    name: "Müden (Aller)",
    plz: "38539"
  },
  {
    lati: 54.0940627899, longi: 10.5092884375,
    name: "Bosau",
    plz: "23715"
  },
  {
    lati: 50.3792208295, longi: 8.12581097427,
    name: "Limburg",
    plz: "65551"
  },
  {
    lati: 51.4577057966, longi: 7.38051349462,
    name: "Witten",
    plz: "58454"
  },
  {
    lati: 49.2858387024, longi: 7.66348419877,
    name: "Waldfischbach-Burgalben",
    plz: "67714"
  },
  {
    lati: 51.5532940931, longi: 7.69835161783,
    name: "Unna",
    plz: "59425"
  },
  {
    lati: 48.2843859675, longi: 8.71678665107,
    name: "Rosenfeld",
    plz: "72348"
  },
  {
    lati: 48.0202384334, longi: 11.7385217198,
    name: "Höhenkirchen-Siegertsbrunn",
    plz: "85635"
  },
  {
    lati: 48.8947005975, longi: 10.9161238881,
    name: "Langenaltheim",
    plz: "91799"
  },
  {
    lati: 53.2764677396, longi: 7.32631400348,
    name: "Jemgum",
    plz: "26844"
  },
  {
    lati: 49.4771394011, longi: 7.43817682701,
    name: "Glan-Münchweiler",
    plz: "66907"
  },
  {
    lati: 54.4640106857, longi: 8.55299004737,
    name: "Süderoog, Pellworm",
    plz: "25849"
  },
  {
    lati: 48.7575860146, longi: 9.14625379897,
    name: "Stuttgart",
    plz: "70199"
  },
  {
    lati: 50.7751291082, longi: 7.86826471295,
    name: "Betzdorf",
    plz: "57518"
  },
  {
    lati: 53.29778455, longi: 11.08777405,
    name: "Lübtheen",
    plz: "19249"
  },
  {
    lati: 52.5414768552, longi: 13.5902960669,
    name: "Berlin",
    plz: "12629"
  },
  {
    lati: 48.7026440226, longi: 9.71599088255,
    name: "Eislingen/Fils",
    plz: "73054"
  },
  {
    lati: 53.4604156239, longi: 9.85771854642,
    name: "Hamburg",
    plz: "21149"
  },
  {
    lati: 49.8246479722, longi: 9.96387294902,
    name: "Würzburg",
    plz: "97078"
  },
  {
    lati: 49.1027856476, longi: 13.0343455202,
    name: "Drachselsried",
    plz: "94256"
  },
  {
    lati: 50.1998464323, longi: 8.98843609121,
    name: "Neuberg",
    plz: "63543"
  },
  {
    lati: 52.6903835363, longi: 9.21743617147,
    name: "Drakenburg",
    plz: "31623"
  },
  {
    lati: 49.5942902355, longi: 8.74082228869,
    name: "Mörlenbach",
    plz: "69509"
  },
  {
    lati: 48.2037514274, longi: 8.83588255122,
    name: "Hausen am Tann",
    plz: "72361"
  },
  {
    lati: 49.8450825474, longi: 7.37836568211,
    name: "Bundenbach",
    plz: "55626"
  },
  {
    lati: 54.3675638072, longi: 10.2188922018,
    name: "Heikendorf",
    plz: "24226"
  },
  {
    lati: 49.1406609342, longi: 12.4162172605,
    name: "Zell",
    plz: "93199"
  },
  {
    lati: 53.2388745966, longi: 12.9859539024,
    name: "Wesenberg",
    plz: "17255"
  },
  {
    lati: 47.9107070351, longi: 12.5944611783,
    name: "Nußdorf",
    plz: "83365"
  },
  {
    lati: 48.6388355421, longi: 9.66302480939,
    name: "Eschenbach",
    plz: "73107"
  },
  {
    lati: 47.8741263508, longi: 10.6715037138,
    name: "Mauerstetten",
    plz: "87665"
  },
  {
    lati: 48.1631648746, longi: 11.5371388473,
    name: "München",
    plz: "80637"
  },
  {
    lati: 50.7277175462, longi: 13.1342794686,
    name: "Börnichen, Gornau",
    plz: "09437"
  },
  {
    lati: 50.6817696853, longi: 13.4922859346,
    name: "Neuhausen/Erzgeb.",
    plz: "09544"
  },
  {
    lati: 50.3771389562, longi: 7.420178726,
    name: "Saffig",
    plz: "56648"
  },
  {
    lati: 49.8215646436, longi: 7.52529110753,
    name: "Simmertal",
    plz: "55618"
  },
  {
    lati: 50.1357638063, longi: 7.05692758174,
    name: "Gevenich",
    plz: "56825"
  },
  {
    lati: 49.366570473, longi: 7.17979763325,
    name: "Neunkirchen",
    plz: "66540"
  },
  {
    lati: 51.2183540503, longi: 6.27146365537,
    name: "Schwalmtal",
    plz: "41366"
  },
  {
    lati: 52.0607346888, longi: 8.61266701489,
    name: "Bielefeld",
    plz: "33729"
  },
  {
    lati: 52.3844190254, longi: 9.74467025886,
    name: "Hannover",
    plz: "30161"
  },
  {
    lati: 52.7946065729, longi: 9.96063522108,
    name: "Bergen, Lohheide u.a.",
    plz: "29303"
  },
  {
    lati: 50.3659141643, longi: 10.0900980488,
    name: "Schönau a.d. Brend",
    plz: "97659"
  },
  {
    lati: 53.6060055192, longi: 10.0675448242,
    name: "Hamburg",
    plz: "22177"
  },
  {
    lati: 49.8113872224, longi: 10.1309447954,
    name: "Dettelbach",
    plz: "97337"
  },
  {
    lati: 48.1763735925, longi: 11.6343397537,
    name: "München",
    plz: "81927"
  },
  {
    lati: 50.3953251716, longi: 11.0442363428,
    name: "Frankenblick, Schalkau, Bachfeld",
    plz: "96528"
  },
  {
    lati: 49.4492214592, longi: 12.0695564076,
    name: "Schmidgaden",
    plz: "92546"
  },
  {
    lati: 49.1284128792, longi: 12.3516248772,
    name: "Wald",
    plz: "93192"
  },
  {
    lati: 48.7811922252, longi: 9.14590965601,
    name: "Stuttgart",
    plz: "70193"
  },
  {
    lati: 53.4128272962, longi: 11.2322580213,
    name: "Hagenow u.a.",
    plz: "19230"
  },
  {
    lati: 48.3034149414, longi: 11.234317178,
    name: "Sulzemoos",
    plz: "85259"
  },
  {
    lati: 51.4199706977, longi: 7.03784774582,
    name: "Essen",
    plz: "45134"
  },
  {
    lati: 52.698664495, longi: 9.06861617447,
    name: "Wietzen",
    plz: "31613"
  },
  {
    lati: 49.112857299, longi: 9.18433132607,
    name: "Heilbronn",
    plz: "74081"
  },
  {
    lati: 52.4258577327, longi: 10.767657261,
    name: "Wolfsburg",
    plz: "38440"
  },
  {
    lati: 50.694111584, longi: 13.0879184566,
    name: "Großolbersdorf",
    plz: "09432"
  },
  {
    lati: 51.9911964318, longi: 14.0937679468,
    name: "Straupitz",
    plz: "15913"
  },
  {
    lati: 53.7689078706, longi: 10.1748687787,
    name: "Bargfeld-Stegen",
    plz: "23863"
  },
  {
    lati: 54.4703737862, longi: 13.1722989875,
    name: "Gingst",
    plz: "18569"
  },
  {
    lati: 54.4181697968, longi: 13.5916444952,
    name: "Binz",
    plz: "18609"
  },
  {
    lati: 49.5180439074, longi: 7.16468337889,
    name: "Namborn",
    plz: "66640"
  },
  {
    lati: 50.2950835882, longi: 7.29730477927,
    name: "Polch",
    plz: "56751"
  },
  {
    lati: 48.6318909908, longi: 9.56465789229,
    name: "Aichelberg",
    plz: "73101"
  },
  {
    lati: 49.087855849, longi: 8.19646161752,
    name: "Kandel",
    plz: "76870"
  },
  {
    lati: 53.1527034365, longi: 8.20706266093,
    name: "Oldenburg (Oldenburg)",
    plz: "26121"
  },
  {
    lati: 49.5597979589, longi: 7.55763453103,
    name: "Eßweiler",
    plz: "67754"
  },
  {
    lati: 50.8105343525, longi: 8.78144091878,
    name: "Marburg",
    plz: "35039"
  },
  {
    lati: 50.1693688718, longi: 10.2038330802,
    name: "Rannungen",
    plz: "97517"
  },
  {
    lati: 49.9789061532, longi: 11.8692898089,
    name: "Mehlmeisel",
    plz: "95694"
  },
  {
    lati: 51.0873567104, longi: 14.6640679609,
    name: "Löbau, Kottmar u.a.",
    plz: "02708"
  },
  {
    lati: 48.7868502833, longi: 11.4448636491,
    name: "Ingolstadt",
    plz: "85055"
  },
  {
    lati: 48.8989456559, longi: 12.4577655732,
    name: "Rain",
    plz: "94369"
  },
  {
    lati: 48.1904864319, longi: 10.6455183825,
    name: "Scherstetten",
    plz: "86872"
  },
  {
    lati: 49.80249603, longi: 9.39620398922,
    name: "Stadtprozelten",
    plz: "97909"
  },
  {
    lati: 51.6617941457, longi: 6.61374639484,
    name: "Wesel",
    plz: "46483"
  },
  {
    lati: 50.6108649899, longi: 7.41894848628,
    name: "Neustadt (Wied)",
    plz: "53577"
  },
  {
    lati: 49.3956651192, longi: 8.18039635165,
    name: "Ruppertsberg",
    plz: "67152"
  },
  {
    lati: 48.9859549182, longi: 8.2468486937,
    name: "Neuburg am Rhein",
    plz: "76776"
  },
  {
    lati: 53.890504262, longi: 10.6816991607,
    name: "Lübeck St. Lorenz Nord",
    plz: "23554"
  },
  {
    lati: 49.4395719395, longi: 11.0687957979,
    name: "Nürnberg",
    plz: "90443"
  },
  {
    lati: 47.6229479834, longi: 11.1234263914,
    name: "Schwaigen",
    plz: "82445"
  },
  {
    lati: 48.6300500639, longi: 10.4401328753,
    name: "Wittislingen",
    plz: "89426"
  },
  {
    lati: 47.9964470455, longi: 10.4973645248,
    name: "Apfeltrach",
    plz: "87742"
  },
  {
    lati: 47.7611209022, longi: 11.6245127373,
    name: "Reichersbeuern",
    plz: "83677"
  },
  {
    lati: 54.1656921126, longi: 10.7053860615,
    name: "Kasseedorf",
    plz: "23717"
  },
  {
    lati: 52.2034916976, longi: 10.3040920362,
    name: "Lengede",
    plz: "38268"
  },
  {
    lati: 52.4525961831, longi: 13.5259308216,
    name: "Berlin Niederschöneweide",
    plz: "12439"
  },
  {
    lati: 48.8279101147, longi: 11.4604867712,
    name: "Hepberg",
    plz: "85120"
  },
  {
    lati: 51.5465337328, longi: 6.81864090385,
    name: "Oberhausen",
    plz: "46147"
  },
  {
    lati: 52.1447813949, longi: 7.36936150595,
    name: "Steinfurt",
    plz: "48565"
  },
  {
    lati: 52.0056850726, longi: 9.12706417194,
    name: "Barntrup",
    plz: "32683"
  },
  {
    lati: 49.8173716128, longi: 8.74954161293,
    name: "Ober-Ramstadt",
    plz: "64372"
  },
  {
    lati: 48.4427732739, longi: 9.6609088063,
    name: "Heroldstatt",
    plz: "72535"
  },
  {
    lati: 48.0234288977, longi: 9.38206156147,
    name: "Hohentengen",
    plz: "88367"
  },
  {
    lati: 50.5096518592, longi: 9.49186068884,
    name: "Hosenfeld",
    plz: "36154"
  },
  {
    lati: 54.3052490692, longi: 9.92764423693,
    name: "Felde",
    plz: "24242"
  },
  {
    lati: 51.028506764, longi: 7.67975378344,
    name: "Bergneustadt",
    plz: "51702"
  },
  {
    lati: 51.3546699863, longi: 7.1220594684,
    name: "Velbert",
    plz: "42555"
  },
  {
    lati: 49.929781578, longi: 11.8525603869,
    name: "Immenreuth",
    plz: "95505"
  },
  {
    lati: 50.1447139679, longi: 8.93201870088,
    name: "Hanau",
    plz: "63452"
  },
  {
    lati: 51.1634882212, longi: 9.07563123011,
    name: "Edertal",
    plz: "34549"
  },
  {
    lati: 47.9262373183, longi: 12.1197422952,
    name: "Schechen",
    plz: "83135"
  },
  {
    lati: 51.321906442, longi: 12.3374783367,
    name: "Leipzig",
    plz: "04229"
  },
  {
    lati: 54.0744011994, longi: 13.3517186289,
    name: "Neuenkirchen",
    plz: "17498"
  },
  {
    lati: 52.487331107, longi: 13.3509604746,
    name: "Berlin-West",
    plz: "10823"
  },
  {
    lati: 54.1138916241, longi: 10.1298761183,
    name: "Schillsdorf",
    plz: "24637"
  },
  {
    lati: 54.1979946041, longi: 10.7664363508,
    name: "Schönwalde am Bungsberg",
    plz: "23744"
  },
  {
    lati: 47.7757460851, longi: 10.4153364562,
    name: "Wildpoldsried",
    plz: "87499"
  },
  {
    lati: 50.7646958516, longi: 7.45219279946,
    name: "Eitorf",
    plz: "53783"
  },
  {
    lati: 51.1797266783, longi: 10.0410807262,
    name: "Eschwege",
    plz: "37269"
  },
  {
    lati: 49.5884148332, longi: 12.264015594,
    name: "Leuchtenberg",
    plz: "92705"
  },
  {
    lati: 52.1985136685, longi: 10.676392279,
    name: "Sickte, Dettum u.a.",
    plz: "38173"
  },
  {
    lati: 54.4340099837, longi: 13.4368401815,
    name: "Bergen/ Rügen",
    plz: "18528"
  },
  {
    lati: 50.6770709232, longi: 11.1277449454,
    name: "Königsee-Rottenbach u.a.",
    plz: "07426"
  },
  {
    lati: 49.8899839018, longi: 11.3392667186,
    name: "Plankenfels",
    plz: "95515"
  },
  {
    lati: 49.4724946354, longi: 11.1319154603,
    name: "Nürnberg",
    plz: "90491"
  },
  {
    lati: 48.0102743852, longi: 11.3325258651,
    name: "Starnberg",
    plz: "82319"
  },
  {
    lati: 51.3554309511, longi: 11.3979357786,
    name: "Querfurt, Obhausen, Mücheln u.a.",
    plz: "06268"
  },
  {
    lati: 47.8105263895, longi: 7.63768851314,
    name: "Müllheim",
    plz: "79379"
  },
  {
    lati: 49.1952882911, longi: 8.92779685646,
    name: "Ittlingen",
    plz: "74930"
  },
  {
    lati: 50.1682667033, longi: 9.21702649429,
    name: "Linsengericht",
    plz: "63589"
  },
  {
    lati: 53.0424980306, longi: 8.32165967177,
    name: "Hatten",
    plz: "26209"
  },
  {
    lati: 49.8090487328, longi: 8.69220027191,
    name: "Mühltal",
    plz: "64367"
  },
  {
    lati: 52.9887219417, longi: 8.85077324773,
    name: "Weyhe",
    plz: "28844"
  },
  {
    lati: 54.3019409246, longi: 10.1773636788,
    name: "Kiel",
    plz: "24147"
  },
  {
    lati: 50.2026427976, longi: 10.058979743,
    name: "Bad Kissingen",
    plz: "97688"
  },
  {
    lati: 51.5576612719, longi: 6.72692967384,
    name: "Dinslaken",
    plz: "46535"
  },
  {
    lati: 51.4291567772, longi: 7.00296922954,
    name: "Essen",
    plz: "45131"
  },
  {
    lati: 48.2812352928, longi: 8.09036773325,
    name: "Fischerbach, Haslach, Hofstetten",
    plz: "77716"
  },
  {
    lati: 50.0973802986, longi: 8.59765015181,
    name: "Frankfurt am Main",
    plz: "65933"
  },
  {
    lati: 51.9479501645, longi: 8.6757299435,
    name: "Oerlinghausen",
    plz: "33813"
  },
  {
    lati: 51.5986560718, longi: 11.0003448803,
    name: "Ballenstedt, Harzgerode",
    plz: "06493"
  },
  {
    lati: 48.126388872, longi: 11.5583850029,
    name: "München",
    plz: "80337"
  },
  {
    lati: 48.7834456224, longi: 10.8417686308,
    name: "Buchdorf",
    plz: "86675"
  },
  {
    lati: 49.6195666254, longi: 10.9237074975,
    name: "Heßdorf",
    plz: "91093"
  },
  {
    lati: 50.0795700613, longi: 8.58012072529,
    name: "Frankfurt am Main",
    plz: "60529"
  },
  {
    lati: 49.6242678637, longi: 11.0151649596,
    name: "Bubenreuth",
    plz: "91088"
  },
  {
    lati: 51.0358348077, longi: 6.98569052214,
    name: "Leverkusen",
    plz: "51373"
  },
  {
    lati: 51.3129003752, longi: 6.52359301402,
    name: "Krefeld",
    plz: "47804"
  },
  {
    lati: 47.8395248779, longi: 12.2375450619,
    name: "Riedering",
    plz: "83083"
  },
  {
    lati: 47.8936435411, longi: 12.3844441781,
    name: "Breitbrunn a. Chiemsee",
    plz: "83254"
  },
  {
    lati: 48.7079600433, longi: 12.6319215851,
    name: "Pilsting",
    plz: "94431"
  },
  {
    lati: 49.6460948424, longi: 9.29293449835,
    name: "Schneeberg",
    plz: "63936"
  },
  {
    lati: 48.9265100582, longi: 9.38455364244,
    name: "Burgstetten",
    plz: "71576"
  },
  {
    lati: 51.3701428963, longi: 9.02575223617,
    name: "Bad Arolsen",
    plz: "34454"
  },
  {
    lati: 48.8480548208, longi: 9.10229456918,
    name: "Korntal-Münchingen",
    plz: "70825"
  },
  {
    lati: 47.6870082806, longi: 9.14789861354,
    name: "Konstanz",
    plz: "78467"
  },
  {
    lati: 48.6574631905, longi: 9.77560757987,
    name: "Gingen an der Fils",
    plz: "73333"
  },
  {
    lati: 52.1289849338, longi: 9.90070927471,
    name: "Hildesheim",
    plz: "31139"
  },
  {
    lati: 50.2884521812, longi: 9.90044986742,
    name: "Geroda",
    plz: "97779"
  },
  {
    lati: 50.6918737931, longi: 10.897884866,
    name: "Ilmenau",
    plz: "98693"
  },
  {
    lati: 50.0981068443, longi: 8.04939988868,
    name: "Schlangenbad",
    plz: "65388"
  },
  {
    lati: 52.2413437052, longi: 8.78933128972,
    name: "Bad Oeynhausen",
    plz: "32549"
  },
  {
    lati: 52.5213102309, longi: 13.4098052903,
    name: "Berlin Mitte",
    plz: "10178"
  },
  {
    lati: 50.5440492964, longi: 8.45532425784,
    name: "Wetzlar",
    plz: "35579"
  },
  {
    lati: 48.6532073758, longi: 9.67634963783,
    name: "Eschenbach",
    plz: "73107"
  },
  {
    lati: 49.4264383904, longi: 11.0550039827,
    name: "Nürnberg",
    plz: "90441"
  },
  {
    lati: 49.9323005958, longi: 11.6104115464,
    name: "Bayreuth",
    plz: "95448"
  },
  {
    lati: 50.1339530338, longi: 10.6305038275,
    name: "Burgpreppach",
    plz: "97496"
  },
  {
    lati: 50.2630966211, longi: 8.63337840818,
    name: "Friedrichsdorf",
    plz: "61381"
  },
  {
    lati: 48.4833848249, longi: 8.86541557608,
    name: "Neustetten",
    plz: "72149"
  },
  {
    lati: 51.3903336031, longi: 12.4450676637,
    name: "Leipzig",
    plz: "04349"
  },
  {
    lati: 50.5153359211, longi: 12.8631841344,
    name: "Raschau",
    plz: "08352"
  },
  {
    lati: 49.418980693, longi: 12.2875362825,
    name: "Altendorf",
    plz: "92540"
  },
  {
    lati: 48.8054478335, longi: 11.2216677434,
    name: "Nassenfels",
    plz: "85128"
  },
  {
    lati: 50.1212143272, longi: 11.4325745377,
    name: "Kulmbach",
    plz: "95326"
  },
  {
    lati: 52.1187148444, longi: 11.6364784932,
    name: "Magdeburg",
    plz: "39104"
  },
  {
    lati: 51.1140314426, longi: 12.9682293332,
    name: "Hartha",
    plz: "04746"
  },
  {
    lati: 51.1857976894, longi: 6.54934641774,
    name: "Korschenbroich",
    plz: "41352"
  },
  {
    lati: 51.2082244613, longi: 7.01012060155,
    name: "Haan",
    plz: "42781"
  },
  {
    lati: 48.7950219761, longi: 8.11593703836,
    name: "Hügelsheim",
    plz: "76549"
  },
  {
    lati: 54.2161772223, longi: 9.40257106177,
    name: "Wrohm",
    plz: "25799"
  },
  {
    lati: 53.0655381783, longi: 8.86606886715,
    name: "Bremen",
    plz: "28207"
  },
  {
    lati: 53.9064737027, longi: 9.63469443363,
    name: "Westermoor",
    plz: "25597"
  },
  {
    lati: 53.1169798444, longi: 9.99403279275,
    name: "Bispingen",
    plz: "29646"
  },
  {
    lati: 52.3819109092, longi: 10.0172519399,
    name: "Lehrte",
    plz: "31275"
  },
  {
    lati: 49.4515916225, longi: 10.1491236847,
    name: "Adelshofen",
    plz: "91587"
  },
  {
    lati: 47.8556545201, longi: 9.01641646519,
    name: "Stockach",
    plz: "78333"
  },
  {
    lati: 53.7264724603, longi: 9.22223796233,
    name: "Osten",
    plz: "21756"
  },
  {
    lati: 49.9786775032, longi: 9.4599806982,
    name: "Neuhütten",
    plz: "97843"
  },
  {
    lati: 52.0923324225, longi: 9.9449600458,
    name: "Diekholzen",
    plz: "31199"
  },
  {
    lati: 49.4502314359, longi: 11.090383201,
    name: "Nürnberg",
    plz: "90402"
  },
  {
    lati: 50.8672557181, longi: 13.1893208133,
    name: "Oederan",
    plz: "09569"
  },
  {
    lati: 52.5294389136, longi: 13.469377763,
    name: "Berlin Lichtenberg",
    plz: "10369"
  },
  {
    lati: 52.502512814, longi: 13.5876517691,
    name: "Berlin Kaulsdorf",
    plz: "12621"
  },
  {
    lati: 51.1248229364, longi: 13.8221422813,
    name: "Dresden",
    plz: "01465"
  },
  {
    lati: 50.7084522072, longi: 7.11252671601,
    name: "Bonn",
    plz: "53129"
  },
  {
    lati: 53.3950984007, longi: 7.60485320754,
    name: "Großefehn",
    plz: "26629"
  },
  {
    lati: 50.5415378439, longi: 7.80845729584,
    name: "Freilingen, Freirachdorf u.a.",
    plz: "56244"
  },
  {
    lati: 50.1707760606, longi: 8.57048764645,
    name: "Steinbach (Taunus)",
    plz: "61449"
  },
  {
    lati: 49.8298976616, longi: 7.44052021744,
    name: "Hennweiler",
    plz: "55619"
  },
  {
    lati: 48.2062036729, longi: 12.6144069677,
    name: "Tüßling",
    plz: "84577"
  },
  {
    lati: 54.1864981473, longi: 13.0244703954,
    name: "Wittenhagen",
    plz: "18510"
  },
  {
    lati: 53.2518983201, longi: 12.0480201795,
    name: "Triglitz, Putlitz",
    plz: "16949"
  },
  {
    lati: 50.8032052575, longi: 8.75200400632,
    name: "Marburg",
    plz: "35037"
  },
  {
    lati: 50.3816224408, longi: 7.32463491633,
    name: "Kruft",
    plz: "56642"
  },
  {
    lati: 52.1623961936, longi: 9.99528517474,
    name: "Hildesheim",
    plz: "31135"
  },
  {
    lati: 48.6587801856, longi: 10.288743319,
    name: "Syrgenstein",
    plz: "89428"
  },
  {
    lati: 53.2261499148, longi: 9.74823615562,
    name: "Otter",
    plz: "21259"
  },
  {
    lati: 51.3934153044, longi: 9.47006265555,
    name: "Espenau",
    plz: "34314"
  },
  {
    lati: 48.4481266156, longi: 12.1201415817,
    name: "Vilsheim",
    plz: "84186"
  },
  {
    lati: 51.6517809518, longi: 12.2946715925,
    name: "Bitterfeld-Wolfen",
    plz: "06803"
  },
  {
    lati: 49.2034578422, longi: 12.8795575058,
    name: "Grafenwiesen",
    plz: "93479"
  },
  {
    lati: 54.5497548723, longi: 13.371139345,
    name: "Gingst",
    plz: "18569"
  },
  {
    lati: 48.1189941194, longi: 11.4851917732,
    name: "München",
    plz: "81375"
  },
  {
    lati: 48.0827777126, longi: 11.7066050453,
    name: "Putzbrunn",
    plz: "85640"
  },
  {
    lati: 48.328755741, longi: 11.8276994554,
    name: "Oberding",
    plz: "85445"
  },
  {
    lati: 52.1186553721, longi: 9.70502200929,
    name: "Elze",
    plz: "31008"
  },
  {
    lati: 50.9772820479, longi: 9.81654559389,
    name: "Bebra",
    plz: "36179"
  },
  {
    lati: 51.5115939173, longi: 6.8352244058,
    name: "Oberhausen",
    plz: "46149"
  },
  {
    lati: 50.4758013278, longi: 7.31365666422,
    name: "Brohl-Lützing",
    plz: "56656"
  },
  {
    lati: 47.7542152494, longi: 7.71992363358,
    name: "Malsburg-Marzell",
    plz: "79429"
  },
  {
    lati: 51.853454144, longi: 7.79060384692,
    name: "Sendenhorst",
    plz: "48324"
  },
  {
    lati: 52.1641407782, longi: 8.30684666783,
    name: "Melle",
    plz: "49326"
  },
  {
    lati: 51.9001662101, longi: 8.23194137669,
    name: "Herzebrock-Clarholz",
    plz: "33442"
  },
  {
    lati: 51.3718378272, longi: 7.47878258129,
    name: "Hagen",
    plz: "58097"
  },
  {
    lati: 51.9232699475, longi: 7.72301370192,
    name: "Münster",
    plz: "48167"
  },
  {
    lati: 48.1205617164, longi: 7.73998004921,
    name: "Bahlingen",
    plz: "79353"
  },
  {
    lati: 49.5084802184, longi: 8.41831076738,
    name: "Ludwigshafen am Rhein",
    plz: "67063"
  },
  {
    lati: 49.587644197, longi: 11.0122133201,
    name: "Erlangen",
    plz: "91052"
  },
  {
    lati: 51.7440243002, longi: 7.00019411844,
    name: "Dorsten",
    plz: "46286"
  },
  {
    lati: 53.2814655276, longi: 12.8437234303,
    name: "Mirow",
    plz: "17252"
  },
  {
    lati: 48.4414385251, longi: 12.9576190625,
    name: "Pfarrkirchen",
    plz: "84347"
  },
  {
    lati: 50.709123196, longi: 12.0526815944,
    name: "Hohenleuben",
    plz: "07958"
  },
  {
    lati: 49.6839776472, longi: 11.8007727559,
    name: "Grafenwöhr",
    plz: "92655"
  },
  {
    lati: 51.5406333164, longi: 9.99284885687,
    name: "Göttingen",
    plz: "37075"
  },
  {
    lati: 50.1466682143, longi: 10.034289861,
    name: "Euerdorf",
    plz: "97717"
  },
  {
    lati: 52.4939072594, longi: 13.3030621791,
    name: "Berlin Wilmersdorf",
    plz: "10709"
  },
  {
    lati: 49.6649185133, longi: 7.97718878175,
    name: "Kirchheimbolanden",
    plz: "67292"
  },
  {
    lati: 47.6000963583, longi: 9.77050104096,
    name: "Sigmarszell",
    plz: "88138"
  },
  {
    lati: 48.2656967663, longi: 9.83088407471,
    name: "Ehingen (Donau), Lauterach",
    plz: "89584"
  },
  {
    lati: 49.5322747145, longi: 11.6483647857,
    name: "Neukirchen b. Sulzbach-Rosen",
    plz: "92259"
  },
  {
    lati: 49.4580684216, longi: 11.0984985254,
    name: "Nürnberg",
    plz: "90489"
  },
  {
    lati: 48.8908539255, longi: 8.6842627214,
    name: "Pforzheim",
    plz: "75172"
  },
  {
    lati: 49.4459650359, longi: 6.85046693116,
    name: "Schmelz",
    plz: "66839"
  },
  {
    lati: 51.0871172991, longi: 7.11612603382,
    name: "Burscheid",
    plz: "51399"
  },
  {
    lati: 54.5686066578, longi: 8.54536771994,
    name: "Hallig Hooge",
    plz: "25859"
  },
  {
    lati: 50.6640454944, longi: 12.5215659356,
    name: "Wilkau-Haßlau",
    plz: "08112"
  },
  {
    lati: 49.2656978517, longi: 10.0605422872,
    name: "Rot am See",
    plz: "74585"
  },
  {
    lati: 53.0293908844, longi: 10.248495348,
    name: "Wriedel",
    plz: "29565"
  },
  {
    lati: 52.381693559, longi: 13.006115642,
    name: "Potsdam",
    plz: "14471"
  },
  {
    lati: 49.4561760518, longi: 10.9940068682,
    name: "Fürth",
    plz: "90763"
  },
  {
    lati: 53.5254172379, longi: 10.3642443185,
    name: "Aumühle",
    plz: "21521"
  },
  {
    lati: 53.3304136659, longi: 10.3035590962,
    name: "Winsen",
    plz: "21423"
  },
  {
    lati: 53.2044454527, longi: 10.5512582323,
    name: "Barendorf, Vastorf",
    plz: "21397"
  },
  {
    lati: 48.2166782471, longi: 12.0003314105,
    name: "Buch a. Buchrain",
    plz: "85656"
  },
  {
    lati: 50.7979134711, longi: 10.7408270251,
    name: "Luisenthal, Ohrdruf, Wolfis",
    plz: "99885"
  },
  {
    lati: 48.3376310612, longi: 13.3035624123,
    name: "Bad Füssing",
    plz: "94072"
  },
  {
    lati: 50.0010366543, longi: 8.20239992105,
    name: "Mainz",
    plz: "55124"
  },
  {
    lati: 51.2565288405, longi: 7.10973520204,
    name: "Wuppertal",
    plz: "42115"
  },
  {
    lati: 51.0363650289, longi: 8.98585945098,
    name: "Haina",
    plz: "35114"
  },
  {
    lati: 51.5931866307, longi: 9.93839007142,
    name: "Bovenden",
    plz: "37120"
  },
  {
    lati: 53.5717752826, longi: 9.95820699567,
    name: "Hamburg",
    plz: "20259"
  },
  {
    lati: 51.2101481322, longi: 7.86069623785,
    name: "Plettenberg",
    plz: "58840"
  },
  {
    lati: 47.6730676127, longi: 7.87199693685,
    name: "Schopfheim",
    plz: "79650"
  },
  {
    lati: 48.7177656936, longi: 8.24382798168,
    name: "Baden-Baden",
    plz: "76534"
  },
  {
    lati: 50.7767942816, longi: 8.57905286395,
    name: "Gladenbach",
    plz: "35075"
  },
  {
    lati: 50.3059594014, longi: 7.38461739303,
    name: "Lonnig",
    plz: "56295"
  },
  {
    lati: 51.6009269949, longi: 6.65206483715,
    name: "Voerde (Niederrhein)",
    plz: "46562"
  },
  {
    lati: 54.1149579102, longi: 9.40726765093,
    name: "Hanerau-Hademarschen, Seefeld u.a.",
    plz: "25557"
  },
  {
    lati: 53.2018912945, longi: 9.5986086876,
    name: "Fintel, Lauenbrück u.a.",
    plz: "27389"
  },
  {
    lati: 49.1312214129, longi: 12.647424363,
    name: "Traitsching",
    plz: "93455"
  },
  {
    lati: 48.4701495326, longi: 12.7097225074,
    name: "Falkenberg",
    plz: "84326"
  },
  {
    lati: 48.3690942802, longi: 12.2686981817,
    name: "Velden",
    plz: "84149"
  },
  {
    lati: 47.9745164986, longi: 12.3768374534,
    name: "Pittenhart",
    plz: "83132"
  },
  {
    lati: 53.6472353217, longi: 9.67242553902,
    name: "Heist",
    plz: "25492"
  },
  {
    lati: 47.7297124121, longi: 10.7678197038,
    name: "Bernbeuren",
    plz: "86975"
  },
  {
    lati: 49.4926417179, longi: 10.9412074486,
    name: "Fürth",
    plz: "90768"
  },
  {
    lati: 53.6652951574, longi: 10.2826165858,
    name: "Großhansdorf",
    plz: "22927"
  },
  {
    lati: 50.3928375989, longi: 10.5649412626,
    name: "Römhild",
    plz: "98630"
  },
  {
    lati: 51.4067011116, longi: 6.71898278041,
    name: "Duisburg",
    plz: "47226"
  },
  {
    lati: 51.0224001079, longi: 7.55206355856,
    name: "Gummersbach",
    plz: "51643"
  },
  {
    lati: 51.7730395139, longi: 7.63973274038,
    name: "Ascheberg",
    plz: "59387"
  },
  {
    lati: 52.5122826486, longi: 13.0712251363,
    name: "Dallgow-Döberitz",
    plz: "14624"
  },
  {
    lati: 52.0215921887, longi: 8.56813277498,
    name: "Bielefeld",
    plz: "33607"
  },
  {
    lati: 48.028623266, longi: 8.60909927755,
    name: "Tuningen",
    plz: "78609"
  },
  {
    lati: 49.4489726602, longi: 10.6531869857,
    name: "Neuhof a.d.Zenn",
    plz: "90616"
  },
  {
    lati: 52.5507682098, longi: 9.16257016392,
    name: "Landesbergen",
    plz: "31628"
  },
  {
    lati: 49.6099115353, longi: 11.066026349,
    name: "Uttenreuth, Marloffstein",
    plz: "91080"
  },
  {
    lati: 50.5367171247, longi: 11.5995874742,
    name: "Remptendorf",
    plz: "07368"
  },
  {
    lati: 47.9143688393, longi: 11.6741415033,
    name: "Otterfing",
    plz: "83624"
  },
  {
    lati: 47.7707083423, longi: 12.8978116411,
    name: "Piding",
    plz: "83451"
  },
  {
    lati: 50.0236390907, longi: 10.6080398394,
    name: "Zeil a. Main",
    plz: "97475"
  },
  {
    lati: 48.5118603057, longi: 10.9503719251,
    name: "Aindling",
    plz: "86447"
  },
  {
    lati: 48.1740891019, longi: 9.54089313092,
    name: "Unlingen",
    plz: "88527"
  },
  {
    lati: 48.365224253, longi: 8.63917644609,
    name: "Sulz am Neckar",
    plz: "72172"
  },
  {
    lati: 49.7823170868, longi: 9.93805660025,
    name: "Würzburg",
    plz: "97072"
  },
  {
    lati: 51.8811662285, longi: 9.49721536349,
    name: "Bevern",
    plz: "37639"
  },
  {
    lati: 48.6179015016, longi: 8.96231673923,
    name: "Hildrizhausen",
    plz: "71157"
  },
  {
    lati: 50.6340050941, longi: 6.26122056317,
    name: "Simmerath",
    plz: "52152"
  },
  {
    lati: 49.8475005394, longi: 6.45227062936,
    name: "Irrel",
    plz: "54666"
  },
  {
    lati: 51.08514344, longi: 6.60076814524,
    name: "Grevenbroich",
    plz: "41515"
  },
  {
    lati: 51.1898723987, longi: 6.8206850837,
    name: "Düsseldorf",
    plz: "40591"
  },
  {
    lati: 50.9495621879, longi: 7.12919514257,
    name: "Bergisch Gladbach",
    plz: "51427"
  },
  {
    lati: 49.9646759292, longi: 7.23968449426,
    name: "Briedel",
    plz: "56867"
  },
  {
    lati: 47.9802428078, longi: 12.7182203161,
    name: "Taching a. See",
    plz: "83373"
  },
  {
    lati: 50.328184918, longi: 11.9909519455,
    name: "Gattendorf",
    plz: "95185"
  },
  {
    lati: 48.1983656031, longi: 12.3554934607,
    name: "Aschau a. Inn",
    plz: "84544"
  },
  {
    lati: 49.8896825642, longi: 11.4453177075,
    name: "Glashütten",
    plz: "95496"
  },
  {
    lati: 48.4048556121, longi: 11.6269161628,
    name: "Kranzberg",
    plz: "85402"
  },
  {
    lati: 48.6346682462, longi: 10.3350205283,
    name: "Bachhagel",
    plz: "89429"
  },
  {
    lati: 53.6561168732, longi: 7.41837894564,
    name: "Dornum",
    plz: "26553"
  },
  {
    lati: 53.5875855304, longi: 9.76094558216,
    name: "Hamburg",
    plz: "22559"
  },
  {
    lati: 53.6062385666, longi: 10.1698479551,
    name: "Hamburg",
    plz: "22143"
  },
  {
    lati: 52.1078867246, longi: 10.6556037668,
    name: "Remlingen",
    plz: "38319"
  },
  {
    lati: 48.1695491022, longi: 10.8293762457,
    name: "Untermeitingen",
    plz: "86836"
  },
  {
    lati: 47.7887816123, longi: 7.58910796512,
    name: "Auggen",
    plz: "79424"
  },
  {
    lati: 53.601593376, longi: 9.82630208105,
    name: "Schenefeld",
    plz: "22869"
  },
  {
    lati: 51.6200376264, longi: 7.19256249663,
    name: "Recklinghausen",
    plz: "45657"
  },
  {
    lati: 52.146477114, longi: 10.2778862911,
    name: "Salzgitter",
    plz: "38228"
  },
  {
    lati: 51.36808729, longi: 7.77736208464,
    name: "Hemer",
    plz: "58675"
  },
  {
    lati: 48.7725737518, longi: 12.2222638488,
    name: "Mallersdorf-Pfaffenberg",
    plz: "84066"
  },
  {
    lati: 50.670404008, longi: 12.6727336111,
    name: "Hartenstein",
    plz: "08118"
  },
  {
    lati: 48.1864972957, longi: 11.5192608305,
    name: "München",
    plz: "80993"
  },
  {
    lati: 48.955292249, longi: 12.8113892535,
    name: "Perasdorf",
    plz: "94366"
  },
  {
    lati: 51.2468871731, longi: 6.41776012173,
    name: "Viersen",
    plz: "41747"
  },
  {
    lati: 51.4590263653, longi: 6.89750077665,
    name: "Mülheim an der Ruhr",
    plz: "45475"
  },
  {
    lati: 50.6199363047, longi: 7.98116179122,
    name: "Höhn",
    plz: "56462"
  },
  {
    lati: 54.6227202591, longi: 9.56691660319,
    name: "Böklund u.a.",
    plz: "24860"
  },
  {
    lati: 49.9805811666, longi: 9.11703231829,
    name: "Aschaffenburg",
    plz: "63741"
  },
  {
    lati: 50.1246784822, longi: 8.66377057311,
    name: "Frankfurt am Main",
    plz: "60323"
  },
  {
    lati: 48.0679429192, longi: 10.0518593384,
    name: "Kirchdorf an der Iller",
    plz: "88457"
  },
  {
    lati: 50.5462219888, longi: 10.3725749664,
    name: "Meiningen",
    plz: "98617"
  },
  {
    lati: 52.7163378732, longi: 10.5756981247,
    name: "Hankensbüttel, Obernholz, Dedelstorf",
    plz: "29386"
  },
  {
    lati: 50.028954425, longi: 9.1299985096,
    name: "Johannesberg",
    plz: "63867"
  },
  {
    lati: 54.1305181025, longi: 9.82243143531,
    name: "Gnutz",
    plz: "24622"
  },
  {
    lati: 52.9945157176, longi: 11.0499584746,
    name: "Küsten",
    plz: "29482"
  },
  {
    lati: 50.8272111881, longi: 6.150709614,
    name: "Würselen",
    plz: "52146"
  },
  {
    lati: 51.3877956582, longi: 6.70658966139,
    name: "Duisburg",
    plz: "47229"
  },
  {
    lati: 51.5806927347, longi: 7.05507302506,
    name: "Gelsenkirchen",
    plz: "45894"
  },
  {
    lati: 49.8697528191, longi: 7.3327754446,
    name: "Rhaunen",
    plz: "55624"
  },
  {
    lati: 49.9625110026, longi: 8.31238493236,
    name: "Mainz",
    plz: "55130"
  },
  {
    lati: 49.4665538302, longi: 7.66693066678,
    name: "Rodenbach",
    plz: "67688"
  },
  {
    lati: 49.3857133091, longi: 7.3442701343,
    name: "Waldmohr",
    plz: "66914"
  },
  {
    lati: 50.3966482473, longi: 7.36515370659,
    name: "Kretz",
    plz: "56630"
  },
  {
    lati: 51.4656140423, longi: 7.42157868324,
    name: "Dortmund",
    plz: "44227"
  },
  {
    lati: 51.2422106552, longi: 6.40762825862,
    name: "Viersen",
    plz: "41748"
  },
  {
    lati: 53.8523400097, longi: 10.7215532029,
    name: "Lübeck",
    plz: "23564"
  },
  {
    lati: 47.93043376, longi: 10.937083766,
    name: "Reichling",
    plz: "86934"
  },
  {
    lati: 51.232509375, longi: 10.4620651384,
    name: "Mühlhausen, Unstruttal",
    plz: "99974"
  },
  {
    lati: 52.2821374301, longi: 10.5030292343,
    name: "Braunschweig",
    plz: "38114"
  },
  {
    lati: 48.8671317196, longi: 9.0237267093,
    name: "Hemmingen",
    plz: "71282"
  },
  {
    lati: 54.3691008029, longi: 9.82152198653,
    name: "Sehestedt",
    plz: "24814"
  },
  {
    lati: 49.1453475438, longi: 12.1281823884,
    name: "Regenstauf",
    plz: "93128"
  },
  {
    lati: 48.1760675717, longi: 12.3221116619,
    name: "Gars am Inn",
    plz: "83546"
  },
  {
    lati: 49.0664460391, longi: 12.6432995266,
    name: "Stallwang",
    plz: "94375"
  },
  {
    lati: 50.5958042184, longi: 12.6984456005,
    name: "Aue",
    plz: "08280"
  },
  {
    lati: 47.6883167581, longi: 12.7969435668,
    name: "Schneizlreuth",
    plz: "83458"
  },
  {
    lati: 50.8229124202, longi: 12.8317977814,
    name: "Chemnitz",
    plz: "09117"
  },
  {
    lati: 49.8282939361, longi: 10.3661451296,
    name: "Prichsenstadt",
    plz: "97357"
  },
  {
    lati: 51.8432765163, longi: 11.5826605715,
    name: "Staßfurt",
    plz: "39418"
  },
  {
    lati: 52.0066507346, longi: 9.69740937942,
    name: "Duingen",
    plz: "31089"
  },
  {
    lati: 54.4505214561, longi: 9.55181081999,
    name: "Jagel, Lottorf",
    plz: "24878"
  },
  {
    lati: 48.1753510899, longi: 7.92046798017,
    name: "Freiamt",
    plz: "79348"
  },
  {
    lati: 50.3263002218, longi: 7.1917997244,
    name: "Mayen",
    plz: "56727"
  },
  {
    lati: 48.0402435633, longi: 7.82828004011,
    name: "Freiburg im Breisgau",
    plz: "79108"
  },
  {
    lati: 50.7205897289, longi: 8.22744927997,
    name: "Dillenburg",
    plz: "35686"
  },
  {
    lati: 50.2826186632, longi: 8.83526688437,
    name: "Niddatal",
    plz: "61194"
  },
  {
    lati: 52.3033507528, longi: 10.2052672057,
    name: "Peine",
    plz: "31226"
  },
  {
    lati: 54.2474277014, longi: 9.62208976718,
    name: "Schülp bei Rendsburg",
    plz: "24813"
  },
  {
    lati: 50.7486063095, longi: 10.7868716811,
    name: "Gräfenroda",
    plz: "99330"
  },
  {
    lati: 49.9641915955, longi: 10.4064481448,
    name: "Donnersdorf",
    plz: "97499"
  },
  {
    lati: 47.9882201395, longi: 12.1263834729,
    name: "Rott a. Inn",
    plz: "83543"
  },
  {
    lati: 49.0181325486, longi: 11.9422275987,
    name: "Nittendorf",
    plz: "93152"
  },
  {
    lati: 51.2900748893, longi: 13.5387187391,
    name: "Großenhain",
    plz: "01558"
  },
  {
    lati: 51.3317362357, longi: 6.55816518842,
    name: "Krefeld",
    plz: "47798"
  },
  {
    lati: 49.3883194147, longi: 6.80207755917,
    name: "Nalbach",
    plz: "66809"
  },
  {
    lati: 48.054306868, longi: 9.15115010579,
    name: "Inzigkofen",
    plz: "72514"
  },
  {
    lati: 48.7804664804, longi: 11.5433772889,
    name: "Großmehring",
    plz: "85098"
  },
  {
    lati: 49.6569587124, longi: 10.5912497126,
    name: "Münchsteinach",
    plz: "91481"
  },
  {
    lati: 48.3525080212, longi: 12.3257368163,
    name: "Wurmsham",
    plz: "84189"
  },
  {
    lati: 49.7422395101, longi: 12.3581474704,
    name: "Flossenbürg",
    plz: "92696"
  },
  {
    lati: 52.566405993, longi: 14.5311415275,
    name: "Golzow, Zechin u.a.",
    plz: "15328"
  },
  {
    lati: 49.52439802, longi: 7.09463270398,
    name: "Oberthal",
    plz: "66649"
  },
  {
    lati: 50.3450207386, longi: 6.58420706705,
    name: "Jünkerath",
    plz: "54584"
  },
  {
    lati: 50.1793118893, longi: 11.2132538101,
    name: "Redwitz a.d. Rodach",
    plz: "96257"
  },
  {
    lati: 48.1554427365, longi: 11.849723435,
    name: "Anzing",
    plz: "85646"
  },
  {
    lati: 49.9075391059, longi: 7.98719264522,
    name: "Aspisheim, Grolsheim",
    plz: "55459"
  },
  {
    lati: 49.9273942807, longi: 8.67373098143,
    name: "Darmstadt",
    plz: "64291"
  },
  {
    lati: 48.9602600461, longi: 9.96834272993,
    name: "Adelmannsfelden",
    plz: "73486"
  },
  {
    lati: 48.8467769544, longi: 10.9477640982,
    name: "Rögling",
    plz: "86703"
  },
  {
    lati: 47.6944686692, longi: 9.06261528017,
    name: "Reichenau",
    plz: "78479"
  },
  {
    lati: 54.359023504, longi: 9.16706272972,
    name: "Drage, Seeth",
    plz: "25878"
  },
  {
    lati: 48.745763771, longi: 9.98094875106,
    name: "Bartholomä",
    plz: "73566"
  },
  {
    lati: 52.4824624871, longi: 13.3288916809,
    name: "Berlin Wilhelmsdorf",
    plz: "10715"
  },
  {
    lati: 54.0883480947, longi: 13.4494571327,
    name: "Greifswald",
    plz: "17493"
  },
  {
    lati: 51.5089916538, longi: 7.22851014599,
    name: "Bochum",
    plz: "44807"
  },
  {
    lati: 49.9036941928, longi: 7.88176966255,
    name: "Langenlonsheim",
    plz: "55450"
  },
  {
    lati: 49.5475436168, longi: 7.93766997329,
    name: "Sippersfeld",
    plz: "67729"
  },
  {
    lati: 48.931787934, longi: 8.20642121731,
    name: "Elchesheim-Illingen",
    plz: "76477"
  },
  {
    lati: 53.0557832045, longi: 8.73663137678,
    name: "Bremen",
    plz: "28259"
  },
  {
    lati: 49.0683660225, longi: 8.79209164539,
    name: "Oberderdingen",
    plz: "75038"
  },
  {
    lati: 49.5668711004, longi: 8.20924415692,
    name: "Kindenheim",
    plz: "67271"
  },
  {
    lati: 49.7791232077, longi: 8.29122041071,
    name: "Wintersheim",
    plz: "67587"
  },
  {
    lati: 54.362055949, longi: 12.6651395502,
    name: "Barth",
    plz: "18356"
  },
  {
    lati: 48.3188828637, longi: 13.0733463063,
    name: "Stubenberg",
    plz: "94166"
  },
  {
    lati: 49.0516356761, longi: 12.1185493804,
    name: "Regensburg",
    plz: "93057"
  },
  {
    lati: 48.3696950999, longi: 13.1245637262,
    name: "Kößlarn",
    plz: "94149"
  },
  {
    lati: 48.4633793188, longi: 13.1965292218,
    name: "Griesbach i. Rottal",
    plz: "94086"
  },
  {
    lati: 52.5168816758, longi: 13.3873702411,
    name: "Berlin Mitte",
    plz: "10117"
  },
  {
    lati: 52.7158140374, longi: 9.23466102564,
    name: "Rohrsen",
    plz: "31627"
  },
  {
    lati: 50.1913951169, longi: 8.74825031859,
    name: "Bad Vilbel",
    plz: "61118"
  },
  {
    lati: 49.4028079041, longi: 9.93709883411,
    name: "Niederstetten",
    plz: "97996"
  },
  {
    lati: 50.1928732991, longi: 10.6796934826,
    name: "Maroldsweisach",
    plz: "96126"
  },
  {
    lati: 48.2730012792, longi: 10.7893875071,
    name: "Bobingen",
    plz: "86399"
  },
  {
    lati: 47.6484692701, longi: 10.2654382022,
    name: "Waltenhofen",
    plz: "87448"
  },
  {
    lati: 52.0035497799, longi: 10.4093244068,
    name: "Liebenburg",
    plz: "38704"
  },
  {
    lati: 51.4973066808, longi: 7.04606738285,
    name: "Essen",
    plz: "45327"
  },
  {
    lati: 49.4406134057, longi: 7.95287400711,
    name: "Frankenstein, Neidenfels, Frankeneck",
    plz: "67468"
  },
  {
    lati: 48.6012957457, longi: 9.16784488868,
    name: "Altenriet",
    plz: "72657"
  },
  {
    lati: 50.1000098976, longi: 8.64541091286,
    name: "Frankfurt am Main",
    plz: "60327"
  },
  {
    lati: 50.119576687, longi: 8.7949891521,
    name: "Offenbach am Main",
    plz: "63075"
  },
  {
    lati: 51.0272579179, longi: 13.8266134562,
    name: "Dresden",
    plz: "01279"
  },
  {
    lati: 51.0071311635, longi: 14.5993983408,
    name: "Ebersbach-Neugersdorf",
    plz: "02730"
  },
  {
    lati: 51.5235038735, longi: 12.1090780235,
    name: "Landsberg",
    plz: "06188"
  },
  {
    lati: 51.5059364702, longi: 9.94486879987,
    name: "Göttingen",
    plz: "37083"
  },
  {
    lati: 51.9097365809, longi: 10.5017284711,
    name: "Goslar",
    plz: "38644"
  },
  {
    lati: 47.8548731228, longi: 12.8181335975,
    name: "Teisendorf",
    plz: "83317"
  },
  {
    lati: 52.9980779983, longi: 12.6983937027,
    name: "Fehrbellin, Temnitzquell, Märkisch Linden u.a.",
    plz: "16818"
  },
  {
    lati: 48.3697824643, longi: 11.3597671605,
    name: "Markt Indersdorf",
    plz: "85229"
  },
  {
    lati: 48.1553073317, longi: 11.565983869,
    name: "München",
    plz: "80798"
  },
  {
    lati: 48.00242271, longi: 10.967548394,
    name: "Hofstetten",
    plz: "86928"
  },
  {
    lati: 48.2643174281, longi: 9.90194685838,
    name: "Achstetten",
    plz: "88480"
  },
  {
    lati: 51.110996962, longi: 6.95714884572,
    name: "Langenfeld",
    plz: "40764"
  },
  {
    lati: 50.232933302, longi: 8.25027480823,
    name: "Hünstetten, Idstein",
    plz: "65510"
  },
  {
    lati: 49.3189065581, longi: 8.99600024493,
    name: "Helmstadt-Bargen",
    plz: "74921"
  },
  {
    lati: 50.494116543, longi: 12.115282469,
    name: "Plauen",
    plz: "08523"
  },
  {
    lati: 52.6305062048, longi: 13.6902649804,
    name: "Werneuchen",
    plz: "16356"
  },
  {
    lati: 47.6247242705, longi: 10.7082987057,
    name: "Rieden am Forggensee",
    plz: "87669"
  },
  {
    lati: 48.046703516, longi: 12.7401553748,
    name: "Tittmoning",
    plz: "84529"
  },
  {
    lati: 52.2273377099, longi: 12.9474921628,
    name: "Beelitz",
    plz: "14547"
  },
  {
    lati: 48.191107245, longi: 12.0585767607,
    name: "Isen",
    plz: "84424"
  },
  {
    lati: 49.0046598381, longi: 12.4218403789,
    name: "Wörth an der Donau",
    plz: "93086"
  },
  {
    lati: 49.5833878722, longi: 11.2181191671,
    name: "Eckental",
    plz: "90542"
  },
  {
    lati: 48.0993768184, longi: 11.531807662,
    name: "München",
    plz: "81379"
  },
  {
    lati: 50.9004889232, longi: 8.03143989166,
    name: "Siegen",
    plz: "57076"
  },
  {
    lati: 47.9977678756, longi: 7.71885717277,
    name: "Freiburg im Breisgau",
    plz: "79112"
  },
  {
    lati: 50.0068624255, longi: 7.16247852923,
    name: "Briedel",
    plz: "56867"
  },
  {
    lati: 53.76242349, longi: 13.5722018549,
    name: "Spantekow",
    plz: "17392"
  },
  {
    lati: 50.6866751527, longi: 9.08904495934,
    name: "Gemünden (Felda)",
    plz: "35329"
  },
  {
    lati: 52.4465485903, longi: 10.9389275455,
    name: "Grafhorst",
    plz: "38462"
  },
  {
    lati: 49.1707085205, longi: 10.5563651138,
    name: "Bechhofen",
    plz: "91572"
  },
  {
    lati: 52.448885891, longi: 9.74734472316,
    name: "Langenhagen",
    plz: "30853"
  },
  {
    lati: 48.1357148404, longi: 9.78380372175,
    name: "Warthausen",
    plz: "88447"
  },
  {
    lati: 51.3164454119, longi: 6.45247418104,
    name: "Tönisvorst",
    plz: "47918"
  },
  {
    lati: 48.3138147924, longi: 13.1231394743,
    name: "Ering",
    plz: "94140"
  },
  {
    lati: 50.8449827535, longi: 12.4772444846,
    name: "Meerane",
    plz: "08393"
  },
  {
    lati: 53.9442243133, longi: 14.0647200682,
    name: "Benz, Heringsdorf u.a.",
    plz: "17429"
  },
  {
    lati: 51.2841707202, longi: 7.28976541552,
    name: "Schwelm",
    plz: "58332"
  },
  {
    lati: 54.3337971413, longi: 9.40388071224,
    name: "Meggerdorf, Friedrichsholm, Friedrichsgraben u.a.",
    plz: "24799"
  },
  {
    lati: 53.3597158311, longi: 10.2234404921,
    name: "Winsen",
    plz: "21423"
  },
  {
    lati: 54.3104024, longi: 10.1489654626,
    name: "Kiel",
    plz: "24143"
  },
  {
    lati: 53.3210828218, longi: 10.3814025314,
    name: "Bardowick, Wittorf, Barum",
    plz: "21357"
  },
  {
    lati: 47.9546668871, longi: 10.3997667813,
    name: "Markt Rettenbach",
    plz: "87733"
  },
  {
    lati: 48.7094499591, longi: 10.8686885279,
    name: "Genderkingen",
    plz: "86682"
  },
  {
    lati: 49.3142125541, longi: 11.001117813,
    name: "Schwabach",
    plz: "91126"
  },
  {
    lati: 47.7500706013, longi: 11.0156563704,
    name: "Böbing",
    plz: "82389"
  },
  {
    lati: 48.1142236337, longi: 11.5478075089,
    name: "München",
    plz: "81371"
  },
  {
    lati: 50.3271809381, longi: 8.750264845,
    name: "Friedberg (Hessen)",
    plz: "61169"
  },
  {
    lati: 50.1641935, longi: 8.66892113026,
    name: "Frankfurt am Main",
    plz: "60433"
  },
  {
    lati: 48.8139531782, longi: 8.86749223839,
    name: "Heimsheim",
    plz: "71296"
  },
  {
    lati: 48.9238653122, longi: 8.47000502848,
    name: "Waldbronn",
    plz: "76337"
  },
  {
    lati: 50.1024390468, longi: 12.1880085182,
    name: "Hohenberg a.d. Eger",
    plz: "95691"
  },
  {
    lati: 51.3057362537, longi: 6.68695462443,
    name: "Meerbusch",
    plz: "40668"
  },
  {
    lati: 54.5892470783, longi: 9.06821391285,
    name: "Ahrenshöft",
    plz: "25853"
  },
  {
    lati: 51.4752427231, longi: 10.0636687189,
    name: "Gleichen",
    plz: "37130"
  },
  {
    lati: 48.9003557354, longi: 10.0924449459,
    name: "Hüttlingen",
    plz: "73460"
  },
  {
    lati: 51.1417838016, longi: 13.1262126472,
    name: "Döbeln, Großweitzschen u.a.",
    plz: "04720"
  },
  {
    lati: 48.1677018854, longi: 12.6215482349,
    name: "Unterneukirchen",
    plz: "84579"
  },
  {
    lati: 48.8051277904, longi: 13.159675398,
    name: "Grattersdorf",
    plz: "94541"
  },
  {
    lati: 51.096961008, longi: 13.7234858405,
    name: "Dresden",
    plz: "01129"
  },
  {
    lati: 47.9948616166, longi: 7.84848760138,
    name: "Freiburg im Breisgau",
    plz: "79098"
  },
  {
    lati: 52.5436612472, longi: 12.3690692525,
    name: "Premnitz",
    plz: "14727"
  },
  {
    lati: 48.2971211855, longi: 11.4803910551,
    name: "Hebertshausen",
    plz: "85241"
  },
  {
    lati: 51.4302087416, longi: 7.37145951003,
    name: "Witten",
    plz: "58453"
  },
  {
    lati: 48.0232723268, longi: 7.68372996775,
    name: "Merdingen",
    plz: "79291"
  },
  {
    lati: 49.6184282956, longi: 8.28388373745,
    name: "Worms",
    plz: "67551"
  },
  {
    lati: 47.6237011683, longi: 8.32899456714,
    name: "Lauchringen",
    plz: "79787"
  },
  {
    lati: 0, longi: 0,
    name: "Stemwede",
    plz: "32351"
  },
  {
    lati: 48.1442575382, longi: 7.987290264,
    name: "Gutach im Breisgau",
    plz: "79261"
  },
  {
    lati: 50.9493980046, longi: 11.5939827182,
    name: "Jena",
    plz: "07743"
  },
  {
    lati: 48.9599037058, longi: 10.6434470654,
    name: "Hainsfarth",
    plz: "86744"
  },
  {
    lati: 52.5823843505, longi: 13.399864986,
    name: "Berlin Niederschönhausen",
    plz: "13156"
  },
  {
    lati: 51.6677298634, longi: 9.33890405612,
    name: "Beverungen",
    plz: "37688"
  },
  {
    lati: 50.6051179367, longi: 7.59102116927,
    name: "Puderbach",
    plz: "56305"
  },
  {
    lati: 48.0070127196, longi: 8.47144468717,
    name: "Brigachtal",
    plz: "78086"
  },
  {
    lati: 50.1625142349, longi: 8.88849457275,
    name: "Hanau",
    plz: "63454"
  },
  {
    lati: 50.9270944037, longi: 8.34769199154,
    name: "Bad Laasphe",
    plz: "57334"
  },
  {
    lati: 48.0908138698, longi: 10.413504318,
    name: "Oberrieden",
    plz: "87769"
  },
  {
    lati: 52.6818829816, longi: 13.3231043754,
    name: "Hohen Neuendorf OT Bergfelde",
    plz: "16562"
  },
  {
    lati: 52.5402240091, longi: 13.4957364971,
    name: "Berlin Alt-Hohenschönhausen",
    plz: "13055"
  },
  {
    lati: 52.4156294445, longi: 13.6618723997,
    name: "Berlin Köpenick",
    plz: "12559"
  },
  {
    lati: 52.2294823008, longi: 13.6432496516,
    name: "Bestensee",
    plz: "15741"
  },
  {
    lati: 48.1228933092, longi: 11.5305496758,
    name: "München",
    plz: "81373"
  },
  {
    lati: 52.5121132336, longi: 13.4164054689,
    name: "Berlin Mitte",
    plz: "10179"
  },
  {
    lati: 49.9480354207, longi: 7.39527720179,
    name: "Kirchberg u.a.",
    plz: "55481"
  },
  {
    lati: 49.166138628, longi: 8.33991255611,
    name: "Hördt",
    plz: "76771"
  },
  {
    lati: 51.8229522601, longi: 9.88903831855,
    name: "Einbeck, Kreiensen",
    plz: "37574"
  },
  {
    lati: 48.1644687312, longi: 11.580243666,
    name: "München",
    plz: "80803"
  },
  {
    lati: 49.4792747887, longi: 11.8146813802,
    name: "Poppenricht",
    plz: "92284"
  },
  {
    lati: 48.2313345955, longi: 10.7468331964,
    name: "Großaitingen",
    plz: "86845"
  },
  {
    lati: 52.5390980668, longi: 10.8517628441,
    name: "Bergfeld",
    plz: "38467"
  },
  {
    lati: 48.4224818959, longi: 9.01942556575,
    name: "Ofterdingen",
    plz: "72131"
  },
  {
    lati: 47.8198543222, longi: 9.22563387135,
    name: "Überlingen",
    plz: "88662"
  },
  {
    lati: 51.5118416225, longi: 7.00925436667,
    name: "Essen",
    plz: "45329"
  },
  {
    lati: 49.2746522392, longi: 7.03033158987,
    name: "Saarbrücken",
    plz: "66125"
  },
  {
    lati: 51.2551085453, longi: 7.14567489174,
    name: "Wuppertal",
    plz: "42103"
  },
  {
    lati: 53.4198865612, longi: 7.20998026095,
    name: "Hinte",
    plz: "26759"
  },
  {
    lati: 53.0375246524, longi: 8.93364994673,
    name: "Bremen",
    plz: "28307"
  },
  {
    lati: 54.2051791286, longi: 9.54590679282,
    name: "Breiholz, Tackesdorf",
    plz: "24797"
  },
  {
    lati: 52.7270328124, longi: 12.6189015506,
    name: "Friesack",
    plz: "14662"
  },
  {
    lati: 48.0988239573, longi: 12.043054649,
    name: "Steinhöring",
    plz: "85643"
  },
  {
    lati: 48.1607059694, longi: 11.4418473581,
    name: "München",
    plz: "81245"
  },
  {
    lati: 47.8858694109, longi: 11.7630323375,
    name: "Valley",
    plz: "83626"
  },
  {
    lati: 52.5547330651, longi: 10.5182641165,
    name: "Wagenhoff, Ringelah",
    plz: "38559"
  },
  {
    lati: 52.477099033, longi: 13.4324478379,
    name: "Berlin Neukölln",
    plz: "12053"
  },
  {
    lati: 49.4158928097, longi: 8.1807121823,
    name: "Deidesheim",
    plz: "67146"
  },
  {
    lati: 49.6270947092, longi: 10.0010057519,
    name: "Gaukönigshofen",
    plz: "97253"
  },
  {
    lati: 51.1348942757, longi: 9.85773430775,
    name: "Waldkappel",
    plz: "37284"
  },
  {
    lati: 53.5541747189, longi: 8.11361258192,
    name: "Wilhelmshaven",
    plz: "26386"
  },
  {
    lati: 49.3030651396, longi: 8.16744368145,
    name: "Kirrweiler (Pfalz)",
    plz: "67489"
  },
  {
    lati: 47.6435646099, longi: 9.84881417133,
    name: "Hergatz",
    plz: "88145"
  },
  {
    lati: 50.3266513468, longi: 10.2127456215,
    name: "Bad Neustadt an der Saale",
    plz: "97616"
  },
  {
    lati: 50.3115900368, longi: 10.444054335,
    name: "Sulzfeld",
    plz: "97633"
  },
  {
    lati: 51.3320971981, longi: 12.5057926686,
    name: "Leipzig",
    plz: "04319"
  },
  {
    lati: 53.1582621313, longi: 10.9931763117,
    name: "Hitzacker (Elbe)",
    plz: "29456"
  },
  {
    lati: 50.0037777707, longi: 9.08444333584,
    name: "Kleinostheim",
    plz: "63801"
  },
  {
    lati: 54.4655298324, longi: 8.72647346346,
    name: "Nordstrand, Elisabeth-Sophien-Koog, Südfall",
    plz: "25845"
  },
  {
    lati: 50.3260536058, longi: 6.75617239397,
    name: "Üxheim",
    plz: "54579"
  },
  {
    lati: 51.4961354057, longi: 7.8693175892,
    name: "Wickede (Ruhr)",
    plz: "58739"
  },
  {
    lati: 54.9358432877, longi: 8.33673667636,
    name: "Wenningstedt-Braderup (Sylt)",
    plz: "25996"
  },
  {
    lati: 52.2858230146, longi: 12.5178941214,
    name: "Beetzsee, Wollin, Wenzlow, Golzow u.a",
    plz: "14778"
  },
  {
    lati: 53.9454229498, longi: 10.877092503,
    name: "Lübeck",
    plz: "23570"
  },
  {
    lati: 48.856332671, longi: 11.0116849929,
    name: "Mörnsheim",
    plz: "91804"
  },
  {
    lati: 52.4211568487, longi: 13.2765665063,
    name: "Berlin Zehlendorf",
    plz: "14167"
  },
  {
    lati: 48.1226814984, longi: 11.4366345014,
    name: "Gräfelfing",
    plz: "82166"
  },
  {
    lati: 48.1907763773, longi: 11.4520790541,
    name: "München",
    plz: "80999"
  },
  {
    lati: 53.5491405806, longi: 10.7735525672,
    name: "Gudow",
    plz: "23899"
  },
  {
    lati: 50.2383306353, longi: 7.97973471577,
    name: "Schönborn",
    plz: "56370"
  },
  {
    lati: 49.2161962589, longi: 7.7066194745,
    name: "Münchweiler an der Rodalb",
    plz: "66981"
  },
  {
    lati: 51.4146686107, longi: 8.11563701604,
    name: "Arnsberg",
    plz: "59823"
  },
  {
    lati: 50.2170507284, longi: 6.64734260779,
    name: "Gerolstein",
    plz: "54568"
  },
  {
    lati: 53.6051178142, longi: 9.45900764836,
    name: "Stade",
    plz: "21682"
  },
  {
    lati: 48.8273192228, longi: 9.96541865425,
    name: "Mögglingen",
    plz: "73563"
  },
  {
    lati: 50.848473657, longi: 10.1108428413,
    name: "Krayenberggemeinde, Frauensee",
    plz: "36460"
  },
  {
    lati: 53.5082389345, longi: 10.1966705978,
    name: "Hamburg",
    plz: "21031"
  },
  {
    lati: 50.4984371711, longi: 12.2326063342,
    name: "Neuensalz",
    plz: "08541"
  },
  {
    lati: 51.6230624521, longi: 7.63070900036,
    name: "Bergkamen",
    plz: "59192"
  },
  {
    lati: 49.3012001381, longi: 8.63267269792,
    name: "Walldorf",
    plz: "69190"
  },
  {
    lati: 50.1467710685, longi: 8.71694265857,
    name: "Frankfurt am Main",
    plz: "60389"
  },
  {
    lati: 48.7225090771, longi: 8.84608028803,
    name: "Ostelsheim",
    plz: "75395"
  },
  {
    lati: 49.9151573759, longi: 9.19055479019,
    name: "Sulzbach am Main",
    plz: "63834"
  },
  {
    lati: 52.3513744664, longi: 10.2125646604,
    name: "Peine",
    plz: "31228"
  },
  {
    lati: 47.7413520176, longi: 10.7116294145,
    name: "Stötten am Auerberg",
    plz: "87675"
  },
  {
    lati: 49.0305188497, longi: 10.4532253388,
    name: "Weiltingen",
    plz: "91744"
  },
  {
    lati: 49.7786937611, longi: 6.93688453511,
    name: "Malborn",
    plz: "54426"
  },
  {
    lati: 50.9922677667, longi: 6.93353086333,
    name: "Köln",
    plz: "50737"
  },
  {
    lati: 50.4128020501, longi: 7.7054459205,
    name: "Hillscheid",
    plz: "56204"
  },
  {
    lati: 47.4423551967, longi: 10.1751060785,
    name: "Fischen im Allgäu",
    plz: "87538"
  },
  {
    lati: 48.1191506351, longi: 12.1633610951,
    name: "Rechtmehring",
    plz: "83562"
  },
  {
    lati: 54.0835162013, longi: 12.1659247622,
    name: "Rostock",
    plz: "18055"
  },
  {
    lati: 50.4812404876, longi: 12.2965826345,
    name: "Bergen",
    plz: "08239"
  },
  {
    lati: 50.3600712051, longi: 12.4135995928,
    name: "Klingenthal",
    plz: "08267"
  },
  {
    lati: 51.0785777152, longi: 12.595365882,
    name: "Frohburg",
    plz: "04654"
  },
  {
    lati: 49.5614323557, longi: 8.45613612782,
    name: "Mannheim",
    plz: "68307"
  },
  {
    lati: 48.3916053839, longi: 10.4623789074,
    name: "Jettingen-Scheppach",
    plz: "89343"
  },
  {
    lati: 50.961120659, longi: 14.0078337427,
    name: "Wehlen",
    plz: "01829"
  },
  {
    lati: 54.4411997522, longi: 9.42615290463,
    name: "Groß Rheide",
    plz: "24872"
  },
  {
    lati: 49.0118781761, longi: 9.51105650669,
    name: "Sulzbach an der Murr",
    plz: "71560"
  },
  {
    lati: 51.265968323, longi: 11.517486589,
    name: "Nebra, Kaiserpfalz",
    plz: "06642"
  },
  {
    lati: 48.6561464464, longi: 11.5004300829,
    name: "Reichertshofen",
    plz: "85084"
  },
  {
    lati: 48.4701946481, longi: 11.8353464543,
    name: "Haag a.d. Amper",
    plz: "85410"
  },
  {
    lati: 50.8880074683, longi: 12.7327905584,
    name: "Niederfrohna",
    plz: "09243"
  },
  {
    lati: 51.1477024756, longi: 7.33139560852,
    name: "Hückeswagen",
    plz: "42499"
  },
  {
    lati: 53.7670857758, longi: 8.81253780017,
    name: "Nordleda",
    plz: "21765"
  },
  {
    lati: 48.6296395643, longi: 10.1565427056,
    name: "Herbrechtingen",
    plz: "89542"
  },
  {
    lati: 52.5072463512, longi: 13.2879408356,
    name: "Berlin Charlottenburg",
    plz: "14057"
  },
  {
    lati: 54.7240728696, longi: 9.66097517825,
    name: "Sörup",
    plz: "24966"
  },
  {
    lati: 53.8273952035, longi: 9.28101355025,
    name: "Freiburg (Elbe)",
    plz: "21729"
  },
  {
    lati: 54.2445531151, longi: 9.07930667969,
    name: "Weddingstedt",
    plz: "25795"
  },
  {
    lati: 52.3954052573, longi: 9.27261909275,
    name: "Auhagen, Sachsenhagen",
    plz: "31553"
  },
  {
    lati: 49.2783474002, longi: 6.81722819765,
    name: "Bous",
    plz: "66359"
  },
  {
    lati: 51.5875439732, longi: 6.96650856835,
    name: "Gladbeck",
    plz: "45966"
  },
  {
    lati: 52.0396895914, longi: 8.17153871659,
    name: "Versmold",
    plz: "33775"
  },
  {
    lati: 50.61473662, longi: 8.70389059643,
    name: "Gießen",
    plz: "35396"
  },
  {
    lati: 49.7200371361, longi: 11.0633894038,
    name: "Forchheim",
    plz: "91301"
  },
  {
    lati: 50.9485460749, longi: 11.1654583604,
    name: "Udestedt, Mönchenholzhausen u.a.",
    plz: "99198"
  },
  {
    lati: 50.3229031243, longi: 11.6866217645,
    name: "Naila",
    plz: "95119"
  },
  {
    lati: 48.3040508073, longi: 10.3766306431,
    name: "Neuburg a.d. Kammel",
    plz: "86476"
  },
  {
    lati: 47.8476043745, longi: 10.5418277819,
    name: "Aitrang",
    plz: "87648"
  },
  {
    lati: 48.2554970292, longi: 11.3599879384,
    name: "Bergkirchen",
    plz: "85232"
  },
  {
    lati: 47.4098525474, longi: 10.9891754684,
    name: "Garmisch-Partenkirchen (Schneefernerhaus)",
    plz: "82475"
  },
  {
    lati: 49.4461983263, longi: 11.1312174469,
    name: "Nürnberg",
    plz: "90480"
  },
  {
    lati: 51.8509160665, longi: 12.2703559957,
    name: "Dessau-Roßlau",
    plz: "06844"
  },
  {
    lati: 48.3402914031, longi: 9.36001254801,
    name: "Hohenstein",
    plz: "72531"
  },
  {
    lati: 54.6043774647, longi: 9.72646942676,
    name: "Steinfeld",
    plz: "24888"
  },
  {
    lati: 53.7010179611, longi: 9.79046117243,
    name: "Kummerfeld",
    plz: "25495"
  },
  {
    lati: 49.760209951, longi: 9.82606411392,
    name: "Eisingen",
    plz: "97249"
  },
  {
    lati: 52.1211925748, longi: 8.20644360716,
    name: "Dissen",
    plz: "49201"
  },
  {
    lati: 50.5142431123, longi: 8.19050814746,
    name: "Merenberg",
    plz: "35799"
  },
  {
    lati: 50.9114555471, longi: 8.89702188232,
    name: "Rauschenberg",
    plz: "35282"
  },
  {
    lati: 53.7729785341, longi: 8.89428380566,
    name: "Neuenkirchen",
    plz: "21763"
  },
  {
    lati: 50.7655759957, longi: 8.47885126313,
    name: "Bad Endbach",
    plz: "35080"
  },
  {
    lati: 52.7629951189, longi: 9.54873653199,
    name: "Hodenhagen u.a.",
    plz: "29693"
  },
  {
    lati: 48.611693912, longi: 9.44369461886,
    name: "Dettingen unter Teck",
    plz: "73265"
  },
  {
    lati: 49.9989655512, longi: 11.8407711638,
    name: "Fichtelberg",
    plz: "95686"
  },
  {
    lati: 51.3838607508, longi: 6.86226934248,
    name: "Mülheim an der Ruhr",
    plz: "45481"
  },
  {
    lati: 49.466987665, longi: 7.1833386058,
    name: "Sankt Wendel",
    plz: "66606"
  },
  {
    lati: 52.3436627306, longi: 7.21085874824,
    name: "Engden, Isterberg, Schüttorf u.a.",
    plz: "48465"
  },
  {
    lati: 51.5805438451, longi: 10.6275220403,
    name: "Walkenried",
    plz: "37445"
  },
  {
    lati: 48.4192454074, longi: 10.9058454175,
    name: "Augsburg",
    plz: "86169"
  },
  {
    lati: 49.1898118425, longi: 7.76505189995,
    name: "Hinterweidenthal",
    plz: "66999"
  },
  {
    lati: 47.7385040092, longi: 7.928262712,
    name: "Häg-Ehrsberg",
    plz: "79685"
  },
  {
    lati: 52.4605748652, longi: 10.1700590686,
    name: "Uetze",
    plz: "31311"
  },
  {
    lati: 49.5060999763, longi: 11.2413134686,
    name: "Rückersdorf",
    plz: "90607"
  },
  {
    lati: 52.9565333885, longi: 11.4119562986,
    name: "Prezelle",
    plz: "29491"
  },
  {
    lati: 47.6251409583, longi: 11.7299579671,
    name: "Kreuth",
    plz: "83708"
  },
  {
    lati: 54.127143595, longi: 12.6499029437,
    name: "Bad Sülze",
    plz: "18334"
  },
  {
    lati: 54.1792517332, longi: 13.8107165517,
    name: "Karlshagen",
    plz: "17449"
  },
  {
    lati: 49.945776933, longi: 6.38653591736,
    name: "Bettingen",
    plz: "54646"
  },
  {
    lati: 51.1015747069, longi: 6.84754759702,
    name: "Dormagen",
    plz: "41539"
  },
  {
    lati: 51.4366554797, longi: 7.04118760919,
    name: "Essen",
    plz: "45136"
  },
  {
    lati: 49.9000299749, longi: 7.80823016184,
    name: "Guldental",
    plz: "55452"
  },
  {
    lati: 49.0941922984, longi: 9.30707256181,
    name: "Untergruppenbach",
    plz: "74199"
  },
  {
    lati: 49.0454029364, longi: 9.55080614791,
    name: "Großerlach",
    plz: "71577"
  },
  {
    lati: 50.1369294675, longi: 9.04117676775,
    name: "Rodenbach",
    plz: "63517"
  },
  {
    lati: 49.0026989009, longi: 9.22190106369,
    name: "Mundelsheim",
    plz: "74395"
  },
  {
    lati: 54.6348869044, longi: 8.72427966803,
    name: "Habel, Gröde",
    plz: "25869"
  },
  {
    lati: 51.1778635086, longi: 8.90731467574,
    name: "Vöhl",
    plz: "34516"
  },
  {
    lati: 51.3347101076, longi: 9.90925201976,
    name: "Witzenhausen",
    plz: "37214"
  },
  {
    lati: 53.5756208595, longi: 10.0437953237,
    name: "Hamburg",
    plz: "22081"
  },
  {
    lati: 48.5822877426, longi: 9.12097739918,
    name: "Walddorfhäslach",
    plz: "72141"
  },
  {
    lati: 48.6586410764, longi: 9.21670854442,
    name: "Filderstadt",
    plz: "70794"
  },
  {
    lati: 48.1199953619, longi: 9.27352854235,
    name: "Bingen",
    plz: "72511"
  },
  {
    lati: 54.4677318961, longi: 9.34912988807,
    name: "Hollingstedt",
    plz: "24876"
  },
  {
    lati: 53.1636196108, longi: 9.49068365669,
    name: "Scheeßel",
    plz: "27383"
  },
  {
    lati: 47.9457032528, longi: 9.45902556155,
    name: "Hoßkirch",
    plz: "88374"
  },
  {
    lati: 48.6930592317, longi: 8.82685625405,
    name: "Gechingen",
    plz: "75391"
  },
  {
    lati: 52.4784014502, longi: 10.3310531713,
    name: "Meinersen",
    plz: "38536"
  },
  {
    lati: 49.4652868817, longi: 11.4912908602,
    name: "Happurg",
    plz: "91230"
  },
  {
    lati: 52.041915966, longi: 11.8130811953,
    name: "Schönebeck (Elbe)",
    plz: "39217"
  },
  {
    lati: 49.2380831801, longi: 8.87969947072,
    name: "Sinsheim",
    plz: "74889"
  },
  {
    lati: 51.8948710555, longi: 7.57620212876,
    name: "Münster",
    plz: "48163"
  },
  {
    lati: 51.9647174548, longi: 7.65357060116,
    name: "Münster",
    plz: "48145"
  },
  {
    lati: 51.1119568418, longi: 7.05850048126,
    name: "Leichlingen",
    plz: "42799"
  },
  {
    lati: 50.5197207163, longi: 7.01802010706,
    name: "Mayschoß",
    plz: "53508"
  },
  {
    lati: 49.5419304048, longi: 7.34438881255,
    name: "Pfeffelbach",
    plz: "66871"
  },
  {
    lati: 52.8849725396, longi: 10.181481254,
    name: "Faßberg",
    plz: "29328"
  },
  {
    lati: 48.3631176776, longi: 11.5630207514,
    name: "Fahrenzhausen",
    plz: "85777"
  },
  {
    lati: 54.0286465393, longi: 10.6775320442,
    name: "Scharbeutz, Süsel",
    plz: "23684"
  },
  {
    lati: 49.6271227691, longi: 10.8152144106,
    name: "Weisendorf",
    plz: "91085"
  },
  {
    lati: 48.1386452756, longi: 10.2467276938,
    name: "Babenhausen",
    plz: "87727"
  },
  {
    lati: 48.2849318297, longi: 10.5462143831,
    name: "Ziemetshausen",
    plz: "86473"
  },
  {
    lati: 50.0353806298, longi: 9.62034759655,
    name: "Neuendorf",
    plz: "97788"
  },
  {
    lati: 49.2061307181, longi: 9.79013347573,
    name: "Braunsbach",
    plz: "74542"
  },
  {
    lati: 49.090597005, longi: 9.46396928294,
    name: "Wüstenrot, Beilstein-Stocksberg",
    plz: "71543"
  },
  {
    lati: 48.6376925154, longi: 11.0978380607,
    name: "Ehekirchen",
    plz: "86676"
  },
  {
    lati: 50.7102136893, longi: 12.9519030122,
    name: "Gelenau/Erzgeb.",
    plz: "09423"
  },
  {
    lati: 51.7588490643, longi: 14.3109068942,
    name: "Cottbus",
    plz: "03046"
  },
  {
    lati: 48.0888251281, longi: 12.5998491034,
    name: "Feichten a.d. Alz",
    plz: "84550"
  },
  {
    lati: 50.3941371656, longi: 12.1302318621,
    name: "Oelsnitz",
    plz: "08606"
  },
  {
    lati: 51.0387487582, longi: 14.9065755986,
    name: "Ostritz, Schönau-Berzdorf",
    plz: "02899"
  },
  {
    lati: 48.8574521754, longi: 10.5111614425,
    name: "Nördlingen",
    plz: "86720"
  },
  {
    lati: 48.6193902345, longi: 10.9058359559,
    name: "Münster",
    plz: "86692"
  },
  {
    lati: 49.6049337237, longi: 11.5059760166,
    name: "Velden/Hartenstein",
    plz: "91235"
  },
  {
    lati: 51.9856111853, longi: 9.82532408918,
    name: "Alfeld (Leine)",
    plz: "31061"
  },
  {
    lati: 48.4385218181, longi: 9.9747215734,
    name: "Ulm",
    plz: "89081"
  },
  {
    lati: 52.6782648596, longi: 10.100564221,
    name: "Celle, Wittbeck",
    plz: "29229"
  },
  {
    lati: 48.9351824063, longi: 10.0528577402,
    name: "Neuler",
    plz: "73491"
  },
  {
    lati: 50.8778003479, longi: 8.43493068525,
    name: "Breidenbach",
    plz: "35236"
  },
  {
    lati: 51.3850642432, longi: 6.80219735948,
    name: "Duisburg",
    plz: "47279"
  },
  {
    lati: 53.4579820131, longi: 8.3485206987,
    name: "Stadland",
    plz: "26937"
  },
  {
    lati: 49.3280303504, longi: 8.53030430619,
    name: "Hockenheim",
    plz: "68766"
  },
  {
    lati: 48.4026735429, longi: 7.97632157832,
    name: "Berghaupten",
    plz: "77791"
  },
  {
    lati: 49.7795305809, longi: 8.20078072893,
    name: "Gau-Odernheim",
    plz: "55239"
  },
  {
    lati: 47.8292698478, longi: 7.72209393473,
    name: "Sulzburg",
    plz: "79295"
  },
  {
    lati: 51.6654254625, longi: 10.0971657451,
    name: "Katlenburg-Lindau",
    plz: "37191"
  },
  {
    lati: 53.2472401612, longi: 9.84240231609,
    name: "Handeloh",
    plz: "21256"
  },
  {
    lati: 49.0752657737, longi: 10.3103761122,
    name: "Dinkelsbühl",
    plz: "91550"
  },
  {
    lati: 52.4981247842, longi: 13.2904669367,
    name: "Berlin Halensee",
    plz: "10711"
  },
  {
    lati: 51.2798346849, longi: 14.1086093643,
    name: "Kamenz",
    plz: "01917"
  },
  {
    lati: 48.1652431305, longi: 11.3149578792,
    name: "Eichenau",
    plz: "82223"
  },
  {
    lati: 47.6112758692, longi: 8.16789127271,
    name: "Dogern",
    plz: "79804"
  },
  {
    lati: 51.3745667796, longi: 7.52847897593,
    name: "Hagen",
    plz: "58093"
  },
  {
    lati: 54.6524029167, longi: 8.34362067506,
    name: "Amrum",
    plz: "25946"
  },
  {
    lati: 50.0197825759, longi: 9.77471574978,
    name: "Gössenheim",
    plz: "97780"
  },
  {
    lati: 49.1646742276, longi: 9.85258201468,
    name: "Wolpertshausen",
    plz: "74549"
  },
  {
    lati: 50.1651383604, longi: 11.1776916856,
    name: "Marktzeuln",
    plz: "96275"
  },
  {
    lati: 47.6525611269, longi: 11.8238834437,
    name: "Rottach-Egern",
    plz: "83700"
  },
  {
    lati: 49.0142544666, longi: 10.2193208419,
    name: "Ellenberg",
    plz: "73488"
  },
  {
    lati: 50.8829681626, longi: 13.8994636932,
    name: "Bahretal",
    plz: "01819"
  },
  {
    lati: 51.1652221645, longi: 7.20596431143,
    name: "Remscheid",
    plz: "42859"
  },
  {
    lati: 51.2193954598, longi: 6.61100161292,
    name: "Kaarst",
    plz: "41564"
  },
  {
    lati: 53.038141235, longi: 11.370752647,
    name: "Gorleben",
    plz: "29475"
  },
  {
    lati: 50.2267826636, longi: 7.63365283378,
    name: "Kamp-Bornhofen-Filsen",
    plz: "56341"
  },
  {
    lati: 50.1555040917, longi: 8.17175914257,
    name: "Taunusstein",
    plz: "65232"
  },
  {
    lati: 49.5662234915, longi: 8.16119701849,
    name: "Grünstadt",
    plz: "67269"
  },
  {
    lati: 53.2229254309, longi: 8.38855230332,
    name: "Elsfleth",
    plz: "26931"
  },
  {
    lati: 48.8148133615, longi: 8.69330611073,
    name: "Unterreichenbach",
    plz: "75399"
  },
  {
    lati: 48.8079346267, longi: 8.76547733132,
    name: "Neuhausen",
    plz: "75242"
  },
  {
    lati: 51.9099018939, longi: 8.88909047306,
    name: "Detmold",
    plz: "32760"
  },
  {
    lati: 52.625511139, longi: 10.6427756123,
    name: "Schönewörde",
    plz: "29396"
  },
  {
    lati: 47.9331599758, longi: 10.822723409,
    name: "Fuchstal",
    plz: "86925"
  },
  {
    lati: 48.7914078719, longi: 9.20300740177,
    name: "Stuttgart",
    plz: "70190"
  },
  {
    lati: 50.5861262054, longi: 11.9837092871,
    name: "Pausa-Mühltroff",
    plz: "07952"
  },
  {
    lati: 47.9469676043, longi: 12.0147821008,
    name: "Tuntenhausen",
    plz: "83104"
  },
  {
    lati: 50.8557419308, longi: 13.0863443224,
    name: "Flöha",
    plz: "09557"
  },
  {
    lati: 47.8077307531, longi: 9.63778602853,
    name: "Weingarten",
    plz: "88250"
  },
  {
    lati: 50.455521405, longi: 10.4601962257,
    name: "Grabfeld",
    plz: "98631"
  },
  {
    lati: 49.6842499607, longi: 10.8489347079,
    name: "Gremsdorf",
    plz: "91350"
  },
  {
    lati: 49.9305795165, longi: 10.8731776161,
    name: "Hallstadt",
    plz: "96103"
  },
  {
    lati: 52.2929920363, longi: 14.5159354038,
    name: "Treplin, Jacobsdorf, Frankfurt (Oder)",
    plz: "15236"
  },
  {
    lati: 51.0739579183, longi: 7.04148645948,
    name: "Leverkusen",
    plz: "51381"
  },
  {
    lati: 49.8057053657, longi: 7.91005953689,
    name: "Hackenheim",
    plz: "55546"
  },
  {
    lati: 50.6627323249, longi: 8.76365819369,
    name: "Staufenberg",
    plz: "35460"
  },
  {
    lati: 49.6895266947, longi: 8.33830013273,
    name: "Worms",
    plz: "67550"
  },
  {
    lati: 50.6934841499, longi: 12.4685985087,
    name: "Zwickau",
    plz: "08062"
  },
  {
    lati: 47.8454527673, longi: 12.5504576015,
    name: "Grabenstätt",
    plz: "83355"
  },
  {
    lati: 48.67460994, longi: 9.46434160629,
    name: "Notzingen",
    plz: "73274"
  },
  {
    lati: 52.5502872327, longi: 13.3823901509,
    name: "Berlin Gesundbrunnen",
    plz: "13357"
  },
  {
    lati: 52.0134093822, longi: 7.59982296835,
    name: "Münster",
    plz: "48159"
  },
  {
    lati: 50.0643429806, longi: 9.1566448148,
    name: "Mömbris",
    plz: "63776"
  },
  {
    lati: 52.8832350995, longi: 9.77561073017,
    name: "Bad Fallingbostel, Osterheide",
    plz: "29683"
  },
  {
    lati: 47.7807124929, longi: 9.62431431086,
    name: "Ravensburg",
    plz: "88212"
  },
  {
    lati: 51.2439211472, longi: 10.663740688,
    name: "Schlotheim",
    plz: "99994"
  },
  {
    lati: 50.6299596977, longi: 10.8621939749,
    name: "Stützerbach",
    plz: "98714"
  },
  {
    lati: 52.1225551833, longi: 10.3600280939,
    name: "Salzgitter",
    plz: "38229"
  },
  {
    lati: 47.9135302511, longi: 10.4907523378,
    name: "Eggenthal",
    plz: "87653"
  },
  {
    lati: 50.7713999929, longi: 7.18978905906,
    name: "Sankt Augustin",
    plz: "53757"
  },
  {
    lati: 51.8589578063, longi: 8.02546359726,
    name: "Ennigerloh",
    plz: "59320"
  },
  {
    lati: 52.3003939976, longi: 7.92888101557,
    name: "Lotte",
    plz: "49504"
  },
  {
    lati: 50.0065377615, longi: 9.1755957076,
    name: "Goldbach",
    plz: "63773"
  },
  {
    lati: 51.2171156731, longi: 9.40168298063,
    name: "Edermünde",
    plz: "34295"
  },
  {
    lati: 51.0919642082, longi: 11.0955712991,
    name: "Großrudestedt, Schloßvippach u.a.",
    plz: "99195"
  },
  {
    lati: 53.0939326663, longi: 11.1045220043,
    name: "Dannenberg",
    plz: "29451"
  },
  {
    lati: 49.6623731982, longi: 11.2444231981,
    name: "Gräfenberg",
    plz: "91322"
  },
  {
    lati: 49.7452335886, longi: 10.1380437732,
    name: "Kitzingen",
    plz: "97318"
  },
  {
    lati: 53.2264071684, longi: 10.2752914516,
    name: "Kirchgellersen, Westergellersen, Südergellersen",
    plz: "21394"
  },
  {
    lati: 48.1792776973, longi: 12.4467468813,
    name: "Kraiburg a. Inn",
    plz: "84559"
  },
  {
    lati: 52.0769256024, longi: 10.8975759692,
    name: "Jerxheim",
    plz: "38381"
  },
  {
    lati: 52.965678054, longi: 8.41976332027,
    name: "Dötlingen",
    plz: "27801"
  },
  {
    lati: 48.0847646478, longi: 8.89198383095,
    name: "Renquishausen",
    plz: "78603"
  },
  {
    lati: 49.9069395356, longi: 12.184566553,
    name: "Wiesau",
    plz: "95676"
  },
  {
    lati: 52.6019693708, longi: 13.345548217,
    name: "Berlin Märkisches Viertel",
    plz: "13435"
  },
  {
    lati: 54.1720266123, longi: 13.8096228733,
    name: "Karlshagen",
    plz: "17449"
  },
  {
    lati: 51.5111399128, longi: 13.8864791417,
    name: "Schipkau",
    plz: "01993"
  },
  {
    lati: 52.3112863331, longi: 8.50476707626,
    name: "Preußisch Oldendorf",
    plz: "32361"
  },
  {
    lati: 53.1627879377, longi: 9.32771669819,
    name: "Rotenburg",
    plz: "27356"
  },
  {
    lati: 48.5603565547, longi: 9.08275640438,
    name: "Tübingen",
    plz: "72074"
  },
  {
    lati: 50.4186776845, longi: 7.62172045771,
    name: "Weitersburg",
    plz: "56191"
  },
  {
    lati: 49.3128419573, longi: 7.1510963473,
    name: "Spiesen-Elversberg",
    plz: "66583"
  },
  {
    lati: 51.1402970658, longi: 13.913416323,
    name: "Radeberg, Wachau",
    plz: "01454"
  },
  {
    lati: 48.0324758897, longi: 7.7619438975,
    name: "Umkirch",
    plz: "79224"
  },
  {
    lati: 50.2549255054, longi: 7.00113383834,
    name: "Uersfeld",
    plz: "56767"
  },
  {
    lati: 49.3775694119, longi: 11.0337557882,
    name: "Nürnberg",
    plz: "90453"
  },
  {
    lati: 50.9763755894, longi: 11.0269329473,
    name: "Erfurt",
    plz: "99084"
  },
  {
    lati: 51.2080467121, longi: 11.2704934997,
    name: "Kölleda",
    plz: "99625"
  },
  {
    lati: 48.1105384601, longi: 11.9506456485,
    name: "Ebersberg",
    plz: "85560"
  },
  {
    lati: 53.9822953075, longi: 10.7781469992,
    name: "Timmendorfer Strand",
    plz: "23669"
  },
  {
    lati: 53.5800853786, longi: 10.4076484467,
    name: "Kuddewörde",
    plz: "22958"
  },
  {
    lati: 48.7768798254, longi: 11.9683193332,
    name: "Rohr i. NB",
    plz: "93352"
  },
  {
    lati: 48.39203914, longi: 12.4534265505,
    name: "Egglkofen",
    plz: "84546"
  },
  {
    lati: 52.0142091714, longi: 7.1944427893,
    name: "Rosendahl",
    plz: "48720"
  },
  {
    lati: 52.8591971709, longi: 13.3985940914,
    name: "Liebenwalde",
    plz: "16559"
  },
  {
    lati: 51.2745601887, longi: 14.4186031425,
    name: "Weißenberg, Hochkirch u.a.",
    plz: "02627"
  },
  {
    lati: 50.3994356154, longi: 10.0060217018,
    name: "Bischofsheim a.d. Rhön",
    plz: "97653"
  },
  {
    lati: 49.1696845198, longi: 9.6402328244,
    name: "Waldenburg",
    plz: "74638"
  },
  {
    lati: 49.3034132481, longi: 9.96625077175,
    name: "Blaufelden",
    plz: "74572"
  },
  {
    lati: 51.4660391729, longi: 10.6977555261,
    name: "Werther Hohenstein Wolkramshausen",
    plz: "99735"
  },
  {
    lati: 52.073465845, longi: 11.8360704949,
    name: "Gommern, Dannigkow",
    plz: "39245"
  },
  {
    lati: 50.5496507666, longi: 7.71179023603,
    name: "Selters (Westerwald)",
    plz: "56242"
  },
  {
    lati: 50.1139986314, longi: 8.62572785961,
    name: "Frankfurt am Main",
    plz: "60486"
  },
  {
    lati: 50.1274344087, longi: 8.64023549583,
    name: "Frankfurt am Main",
    plz: "60487"
  },
  {
    lati: 49.086512426, longi: 8.28282941783,
    name: "Jockgrim",
    plz: "76751"
  },
  {
    lati: 49.9215718145, longi: 12.1318657213,
    name: "Fuchsmühl",
    plz: "95689"
  },
  {
    lati: 49.0996314094, longi: 9.22626274329,
    name: "Flein",
    plz: "74223"
  },
  {
    lati: 49.9091693021, longi: 9.88731443687,
    name: "Retzstadt",
    plz: "97282"
  },
  {
    lati: 50.7099056807, longi: 10.1347499491,
    name: "Dermbach, Wiesenthal",
    plz: "36466"
  },
  {
    lati: 53.5847615449, longi: 11.2740120143,
    name: "Wittenförden u.a.",
    plz: "19073"
  },
  {
    lati: 49.7742819759, longi: 7.83572784194,
    name: "Norheim u.a.",
    plz: "55585"
  },
  {
    lati: 49.5871205888, longi: 7.19262246502,
    name: "Brücken, Oberbrombach u.a.",
    plz: "55767"
  },
  {
    lati: 51.6610072402, longi: 7.23938442483,
    name: "Oer-Erkenschwick",
    plz: "45739"
  },
  {
    lati: 52.2603159428, longi: 7.48021571957,
    name: "Rheine",
    plz: "48432"
  },
  {
    lati: 48.1252278717, longi: 8.90650362314,
    name: "Nusplingen",
    plz: "72362"
  },
  {
    lati: 54.4652293415, longi: 11.1484649455,
    name: "Fehmarn",
    plz: "23769"
  },
  {
    lati: 50.1677601451, longi: 12.1145331009,
    name: "Selb",
    plz: "95100"
  },
  {
    lati: 48.2605735287, longi: 12.0467222243,
    name: "Lengdorf",
    plz: "84435"
  },
  {
    lati: 48.9396168702, longi: 12.1822788475,
    name: "Köfering",
    plz: "93096"
  },
  {
    lati: 48.2059969809, longi: 11.6164164785,
    name: "München",
    plz: "80939"
  },
  {
    lati: 50.6318686565, longi: 6.48960522009,
    name: "Heimbach",
    plz: "52396"
  },
  {
    lati: 50.5835402492, longi: 12.3774290984,
    name: "Lengenfeld",
    plz: "08485"
  },
  {
    lati: 50.1049216913, longi: 8.69644108367,
    name: "Frankfurt am Main",
    plz: "60594"
  },
  {
    lati: 48.4546218531, longi: 11.4780232147,
    name: "Reichertshausen",
    plz: "85293"
  },
  {
    lati: 48.717856731, longi: 9.26139394649,
    name: "Ostfildern",
    plz: "73760"
  },
  {
    lati: 48.5859927446, longi: 9.3606678696,
    name: "Frickenhausen",
    plz: "72636"
  },
  {
    lati: 49.5819807549, longi: 10.0115751785,
    name: "Gelchsheim, Sonderhofen",
    plz: "97255"
  },
  {
    lati: 52.1701243147, longi: 10.0954635066,
    name: "Schellerten",
    plz: "31174"
  },
  {
    lati: 54.7107493332, longi: 9.51106355021,
    name: "Großsolt",
    plz: "24991"
  },
  {
    lati: 52.1003545533, longi: 7.48550299545,
    name: "Nordwalde",
    plz: "48356"
  },
  {
    lati: 52.5473628112, longi: 8.38427325639,
    name: "Lembruch",
    plz: "49459"
  },
  {
    lati: 51.4080926793, longi: 8.60399035181,
    name: "Brilon",
    plz: "59929"
  },
  {
    lati: 48.1649164665, longi: 8.55030481143,
    name: "Zimmern ob Rottweil",
    plz: "78658"
  },
  {
    lati: 53.5925724109, longi: 6.7224865767,
    name: "Borkum",
    plz: "26757"
  },
  {
    lati: 48.1451898327, longi: 11.5687437028,
    name: "München",
    plz: "80333"
  },
  {
    lati: 49.3739946647, longi: 9.71726985584,
    name: "Dörzbach",
    plz: "74677"
  },
  {
    lati: 49.980777266, longi: 11.0501720062,
    name: "Scheßlitz",
    plz: "96110"
  },
  {
    lati: 54.0335090112, longi: 10.7450893989,
    name: "Scharbeutz",
    plz: "23683"
  },
  {
    lati: 49.5254789883, longi: 10.8267867851,
    name: "Puschendorf",
    plz: "90617"
  },
  {
    lati: 51.503430195, longi: 13.7537889725,
    name: "Lauchhammer",
    plz: "01979"
  },
  {
    lati: 53.5501624143, longi: 9.94695844236,
    name: "Hamburg",
    plz: "22767"
  },
  {
    lati: 48.2547709742, longi: 11.0993177508,
    name: "Mittelstetten",
    plz: "82293"
  },
  {
    lati: 48.8649839496, longi: 10.5716087368,
    name: "Deiningen",
    plz: "86738"
  },
  {
    lati: 48.4075257994, longi: 12.4115167355,
    name: "Bodenkirchen",
    plz: "84155"
  },
  {
    lati: 53.0206119354, longi: 9.04000014082,
    name: "Achim",
    plz: "28832"
  },
  {
    lati: 50.5444856422, longi: 8.51204370397,
    name: "Wetzlar",
    plz: "35578"
  },
  {
    lati: 49.9918373604, longi: 7.08459543713,
    name: "Kröv",
    plz: "54536"
  },
  {
    lati: 49.7633523609, longi: 9.18158978025,
    name: "Klingenberg a. Main",
    plz: "63911"
  },
  {
    lati: 48.1188638401, longi: 12.6829421756,
    name: "Halsbach",
    plz: "84553"
  },
  {
    lati: 49.3095358279, longi: 12.8346418298,
    name: "Furth i. Wald",
    plz: "93437"
  },
  {
    lati: 53.6076208316, longi: 11.4740551635,
    name: "Schwerin",
    plz: "19063"
  },
  {
    lati: 48.0484662791, longi: 11.7173778626,
    name: "Hohenbrunn",
    plz: "85662"
  },
  {
    lati: 48.3786639839, longi: 11.7695603856,
    name: "Freising",
    plz: "85356"
  },
  {
    lati: 48.6845255438, longi: 10.6895346321,
    name: "Tapfheim",
    plz: "86660"
  },
  {
    lati: 52.366622858, longi: 10.9126691301,
    name: "Groß Twülpstedt",
    plz: "38464"
  },
  {
    lati: 48.9642933843, longi: 10.4152783409,
    name: "Tannhausen",
    plz: "73497"
  },
  {
    lati: 49.904925751, longi: 9.67853546961,
    name: "Urspringen",
    plz: "97857"
  },
  {
    lati: 52.6218059238, longi: 13.3949538531,
    name: "Berlin Blankenfelde",
    plz: "13159"
  },
  {
    lati: 51.5796597319, longi: 7.23357041767,
    name: "Recklinghausen",
    plz: "45663"
  },
  {
    lati: 54.6566668083, longi: 9.5158434879,
    name: "Havetoft",
    plz: "24873"
  },
  {
    lati: 49.5471186675, longi: 7.84854817406,
    name: "Lohnsfeld",
    plz: "67727"
  },
  {
    lati: 53.0592925768, longi: 8.81542738524,
    name: "Bremen",
    plz: "28201"
  },
  {
    lati: 52.3657955061, longi: 9.75516363436,
    name: "Hannover",
    plz: "30171"
  },
  {
    lati: 54.07529489, longi: 10.0659284468,
    name: "Bönebüttel",
    plz: "24620"
  },
  {
    lati: 48.1007162239, longi: 11.5753997976,
    name: "München",
    plz: "81547"
  },
  {
    lati: 51.5975200552, longi: 11.3649205714,
    name: "Mansfeld",
    plz: "06343"
  },
  {
    lati: 52.463198397, longi: 10.7178599506,
    name: "Weyhausen",
    plz: "38554"
  },
  {
    lati: 50.9543133519, longi: 9.33261805069,
    name: "Frielendorf",
    plz: "34621"
  },
  {
    lati: 52.9979062235, longi: 8.72636503085,
    name: "Stuhr",
    plz: "28816"
  },
  {
    lati: 51.4288833335, longi: 6.25941001732,
    name: "Straelen",
    plz: "47638"
  },
  {
    lati: 49.2592515307, longi: 7.87627129045,
    name: "Annweiler am Trifels",
    plz: "76855"
  },
  {
    lati: 49.6907208937, longi: 8.19615894572,
    name: "Gundersheim",
    plz: "67598"
  },
  {
    lati: 50.3033258736, longi: 7.66251428156,
    name: "Lahnstein (ohne Exklave)",
    plz: "56112"
  },
  {
    lati: 48.2962221171, longi: 7.74929236785,
    name: "Kappel-Grafenhausen",
    plz: "77966"
  },
  {
    lati: 49.9568997081, longi: 10.8669838113,
    name: "Kemmern",
    plz: "96164"
  },
  {
    lati: 49.445444133, longi: 11.021704442,
    name: "Nürnberg",
    plz: "90431"
  },
  {
    lati: 49.3046627913, longi: 11.1366602881,
    name: "Schwanstetten",
    plz: "90596"
  },
  {
    lati: 52.6534830918, longi: 10.2565093297,
    name: "Beedenbostel",
    plz: "29355"
  },
  {
    lati: 49.910437908, longi: 10.3977636336,
    name: "Dingolshausen",
    plz: "97497"
  },
  {
    lati: 49.4090918365, longi: 11.8897794981,
    name: "Kümmersbruck",
    plz: "92245"
  },
  {
    lati: 53.2439949078, longi: 10.4568595052,
    name: "Lüneburg",
    plz: "21337"
  },
  {
    lati: 52.7721569581, longi: 13.5054320826,
    name: "Wandlitz",
    plz: "16348"
  },
  {
    lati: 51.596557905, longi: 11.040744107,
    name: "Südharz, Berga",
    plz: "06536"
  },
  {
    lati: 50.2822771424, longi: 6.99735255225,
    name: "Retterath",
    plz: "56769"
  },
  {
    lati: 53.1287003216, longi: 8.71926928905,
    name: "Bremen",
    plz: "28237"
  },
  {
    lati: 51.9150111305, longi: 9.69196595569,
    name: "Holzen, Eschershausen, Eimen",
    plz: "37632"
  },
  {
    lati: 48.810853933, longi: 9.24101109785,
    name: "Stuttgart",
    plz: "70374"
  },
  {
    lati: 50.6793324085, longi: 12.1174368517,
    name: "Berga/Elster",
    plz: "07980"
  },
  {
    lati: 51.2435445325, longi: 7.1571177029,
    name: "Wuppertal",
    plz: "42119"
  },
  {
    lati: 48.1843409283, longi: 11.5853732021,
    name: "München",
    plz: "80807"
  },
  {
    lati: 48.8002402106, longi: 9.28425982281,
    name: "Fellbach",
    plz: "70734"
  },
  {
    lati: 54.0530686672, longi: 12.8977887031,
    name: "Glewitz",
    plz: "18513"
  },
  {
    lati: 48.1747669378, longi: 11.9055690016,
    name: "Forstinning",
    plz: "85661"
  },
  {
    lati: 48.8786173812, longi: 9.62401321492,
    name: "Welzheim",
    plz: "73642"
  },
  {
    lati: 52.8888451296, longi: 10.9465456177,
    name: "Bergen",
    plz: "29468"
  },
  {
    lati: 51.2669203122, longi: 7.4529727593,
    name: "Breckerfeld",
    plz: "58339"
  },
  {
    lati: 49.6803564168, longi: 7.64725503444,
    name: "Odenbach",
    plz: "67748"
  },
  {
    lati: 48.2206118952, longi: 8.49632840649,
    name: "Dunningen",
    plz: "78655"
  },
  {
    lati: 52.793553258, longi: 8.58761268226,
    name: "Twistringen",
    plz: "27239"
  },
  {
    lati: 51.0533442511, longi: 9.83382872556,
    name: "Cornberg",
    plz: "36219"
  },
  {
    lati: 52.477751071, longi: 10.7357038925,
    name: "Tappenbeck",
    plz: "38479"
  },
  {
    lati: 49.1439955317, longi: 10.4220741923,
    name: "Dentlein a. Forst",
    plz: "91599"
  },
  {
    lati: 49.42524335, longi: 8.13657008657,
    name: "Wachenheim an der Weinstraße",
    plz: "67157"
  },
  {
    lati: 48.7627753494, longi: 9.17501770372,
    name: "Stuttgart",
    plz: "70180"
  },
  {
    lati: 49.5030614743, longi: 10.9964875004,
    name: "Fürth",
    plz: "90765"
  },
  {
    lati: 50.984645191, longi: 11.7157836774,
    name: "Neuengönna u.a.",
    plz: "07778"
  },
  {
    lati: 47.5772427131, longi: 9.63941051209,
    name: "Wasserburg (Bodensee)",
    plz: "88142"
  },
  {
    lati: 50.4715430893, longi: 12.1064354782,
    name: "Plauen",
    plz: "08523"
  },
  {
    lati: 49.8106543872, longi: 10.8703215041,
    name: "Frensdorf",
    plz: "96158"
  },
  {
    lati: 49.4230917887, longi: 10.8531496306,
    name: "Ammerndorf",
    plz: "90614"
  },
  {
    lati: 52.5421878868, longi: 6.83326500175,
    name: "Wilsum",
    plz: "49849"
  },
  {
    lati: 47.9650809497, longi: 7.82542269953,
    name: "Merzhausen",
    plz: "79249"
  },
  {
    lati: 49.2090913529, longi: 7.84952347304,
    name: "Wilgartswiesen",
    plz: "76848"
  },
  {
    lati: 49.4163519707, longi: 8.9363534656,
    name: "Schönbrunn",
    plz: "69436"
  },
  {
    lati: 49.3250731831, longi: 9.15231834812,
    name: "Neckarzimmern",
    plz: "74865"
  },
  {
    lati: 48.7216340797, longi: 9.56603115167,
    name: "Uhingen",
    plz: "73066"
  },
  {
    lati: 54.214753848, longi: 10.1581142623,
    name: "Kirchbarkau",
    plz: "24245"
  },
  {
    lati: 54.6280483918, longi: 13.3185927058,
    name: "Dranske",
    plz: "18556"
  },
  {
    lati: 48.890405333, longi: 9.80573152743,
    name: "Ruppertshofen",
    plz: "73577"
  },
  {
    lati: 49.6235351037, longi: 9.25560266511,
    name: "Schneeberg",
    plz: "63936"
  },
  {
    lati: 52.5056218886, longi: 9.13572717421,
    name: "Leese",
    plz: "31633"
  },
  {
    lati: 49.3194697966, longi: 6.65835086012,
    name: "Wallerfangen",
    plz: "66798"
  },
  {
    lati: 48.3419766433, longi: 13.0110123105,
    name: "Wittibreut",
    plz: "84384"
  },
  {
    lati: 54.1634140143, longi: 12.4938809085,
    name: "Marlow",
    plz: "18337"
  },
  {
    lati: 48.0339240809, longi: 8.87050931633,
    name: "Mühlheim an der Donau",
    plz: "78570"
  },
  {
    lati: 52.7192866951, longi: 13.1946682668,
    name: "Leegebruch",
    plz: "16767"
  },
  {
    lati: 50.3810644574, longi: 11.9129925973,
    name: "Feilitzsch",
    plz: "95183"
  },
  {
    lati: 53.0290405539, longi: 10.5723781888,
    name: "Emmendorf",
    plz: "29579"
  },
  {
    lati: 48.218467505, longi: 10.8637268352,
    name: "Oberottmarshausen",
    plz: "86507"
  },
  {
    lati: 49.5446311327, longi: 8.24065748772,
    name: "Gerolsheim",
    plz: "67229"
  },
  {
    lati: 47.6847504762, longi: 11.3032596477,
    name: "Großweil",
    plz: "82439"
  },
  {
    lati: 48.7463063408, longi: 11.4641213885,
    name: "Ingolstadt",
    plz: "85053"
  },
  {
    lati: 49.9668452833, longi: 6.53190587305,
    name: "Bitburg",
    plz: "54634"
  },
  {
    lati: 50.9533231775, longi: 6.83286028012,
    name: "Köln",
    plz: "50859"
  },
  {
    lati: 48.0370617009, longi: 12.0492819811,
    name: "Frauenneuharting",
    plz: "83553"
  },
  {
    lati: 49.0341260843, longi: 12.8767224465,
    name: "Kollnburg",
    plz: "94262"
  },
  {
    lati: 48.5756293803, longi: 9.27756195806,
    name: "Bempflingen",
    plz: "72658"
  },
  {
    lati: 51.2962326096, longi: 13.6338477979,
    name: "Großenhain, Ebersbach u.a.",
    plz: "01561"
  },
  {
    lati: 52.4887064098, longi: 10.8834846822,
    name: "Rühen",
    plz: "38471"
  },
  {
    lati: 48.1339869769, longi: 7.68213090129,
    name: "Endingen am Kaiserstuhl",
    plz: "79346"
  },
  {
    lati: 54.1153969473, longi: 13.8179526339,
    name: "Karlshagen",
    plz: "17449"
  },
  {
    lati: 52.7022003656, longi: 13.3064434379,
    name: "Birkenwerder",
    plz: "16547"
  },
  {
    lati: 54.7689670852, longi: 9.40636372835,
    name: "Flensburg",
    plz: "24941"
  },
  {
    lati: 50.2384511623, longi: 7.09895920782,
    name: "Kaisersesch",
    plz: "56759"
  },
  {
    lati: 49.4985156862, longi: 7.46296425519,
    name: "Herschweiler-Pettersheim",
    plz: "66909"
  },
  {
    lati: 47.9379623852, longi: 8.18999237849,
    name: "Titisee-Neustadt",
    plz: "79822"
  },
  {
    lati: 52.9996756123, longi: 9.18283518971,
    name: "Langwedel",
    plz: "27299"
  },
  {
    lati: 48.9588084701, longi: 9.21117191356,
    name: "Pleidelsheim",
    plz: "74385"
  },
  {
    lati: 50.1294381434, longi: 9.97451014372,
    name: "Elfershausen",
    plz: "97725"
  },
  {
    lati: 47.9204464349, longi: 11.1724776379,
    name: "Pähl",
    plz: "82396"
  },
  {
    lati: 53.7604828661, longi: 10.9616259913,
    name: "Rehna, Carlow u.a.",
    plz: "19217"
  },
  {
    lati: 50.8445924169, longi: 11.7508652602,
    name: "Stadtroda u.a.",
    plz: "07646"
  },
  {
    lati: 52.5903125884, longi: 14.069088855,
    name: "Oberbarnim, Märkische Höhe u.a.",
    plz: "15377"
  },
  {
    lati: 51.1907669283, longi: 14.0039306503,
    name: "Pulsnitz",
    plz: "01896"
  },
  {
    lati: 50.8686117186, longi: 6.17551873493,
    name: "Alsdorf",
    plz: "52477"
  },
  {
    lati: 51.1813047656, longi: 6.95447673148,
    name: "Hilden",
    plz: "40724"
  },
  {
    lati: 51.19965554, longi: 8.00958742882,
    name: "Finnentrop",
    plz: "57413"
  },
  {
    lati: 54.2664848166, longi: 9.33067528263,
    name: "Pahlen",
    plz: "25794"
  },
  {
    lati: 48.1987026592, longi: 8.75559934957,
    name: "Schömberg",
    plz: "72355"
  },
  {
    lati: 50.6786355811, longi: 8.2859675169,
    name: "Herborn",
    plz: "35745"
  },
  {
    lati: 48.2063637122, longi: 9.65031509233,
    name: "Emerkingen",
    plz: "89607"
  },
  {
    lati: 53.8994984985, longi: 10.1586806791,
    name: "Bark",
    plz: "23826"
  },
  {
    lati: 49.9559054241, longi: 10.1412055715,
    name: "Waigolshausen",
    plz: "97534"
  },
  {
    lati: 48.8285418379, longi: 10.5222806633,
    name: "Reimlingen",
    plz: "86756"
  },
  {
    lati: 48.9639701128, longi: 10.1812299738,
    name: "Ellwangen (Jagst)",
    plz: "73479"
  },
  {
    lati: 48.9823392651, longi: 8.20458903181,
    name: "Berg",
    plz: "76768"
  },
  {
    lati: 52.2017289642, longi: 9.43574853909,
    name: "Bad Münder am Deister",
    plz: "31848"
  },
  {
    lati: 47.719163954, longi: 11.3431742824,
    name: "Sindelsdorf",
    plz: "82404"
  },
  {
    lati: 52.5740475031, longi: 13.3224936135,
    name: "Berlin Reinickendorf",
    plz: "13403"
  },
  {
    lati: 52.4547930675, longi: 13.3435289929,
    name: "Berlin Steglitz",
    plz: "12169"
  },
  {
    lati: 53.2958779644, longi: 14.2155444977,
    name: "Penkun u.a.",
    plz: "17328"
  },
  {
    lati: 52.277506775, longi: 7.7080013259,
    name: "Ibbenbüren",
    plz: "49479"
  },
  {
    lati: 48.15379077, longi: 10.7136112798,
    name: "Hiltenfingen",
    plz: "86856"
  },
  {
    lati: 48.9114856482, longi: 10.7798911433,
    name: "Wolferstadt",
    plz: "86709"
  },
  {
    lati: 48.864834299, longi: 12.444057363,
    name: "Perkam",
    plz: "94368"
  },
  {
    lati: 49.1599854748, longi: 13.1080192572,
    name: "Lohberg",
    plz: "93470"
  },
  {
    lati: 50.0374865595, longi: 12.0116158156,
    name: "Wunsiedel",
    plz: "95632"
  },
  {
    lati: 50.4707975273, longi: 8.60145461929,
    name: "Langgöns",
    plz: "35428"
  },
  {
    lati: 53.4554563198, longi: 7.83964267184,
    name: "Friedeburg",
    plz: "26446"
  },
  {
    lati: 51.4396607458, longi: 6.98399297927,
    name: "Essen",
    plz: "45147"
  },
  {
    lati: 50.7324139405, longi: 6.49090032163,
    name: "Kreuzau",
    plz: "52372"
  },
  {
    lati: 47.8454873214, longi: 10.3672829056,
    name: "Untrasried",
    plz: "87496"
  },
  {
    lati: 54.5831736118, longi: 9.93475794306,
    name: "Damp",
    plz: "24351"
  },
  {
    lati: 49.2278922688, longi: 12.0036263249,
    name: "Burglengenfeld",
    plz: "93133"
  },
  {
    lati: 50.7765196622, longi: 13.7280718513,
    name: "Altenberg",
    plz: "01773"
  },
  {
    lati: 52.8376183005, longi: 13.7348777492,
    name: "Eberswalde",
    plz: "16227"
  },
  {
    lati: 52.1701578106, longi: 12.3312648051,
    name: "Görzke",
    plz: "14828"
  },
  {
    lati: 47.8505596914, longi: 10.4387167043,
    name: "Obergünzburg",
    plz: "87634"
  },
  {
    lati: 54.105708527, longi: 11.8820953457,
    name: "Bad Doberan, Bartenshagen-Parkentin u.a.",
    plz: "18209"
  },
  {
    lati: 48.4054789166, longi: 11.8064295866,
    name: "Marzling",
    plz: "85417"
  },
  {
    lati: 52.2341069288, longi: 11.2750679363,
    name: "Erxleben, Nordgermersleben u.a.",
    plz: "39343"
  },
  {
    lati: 50.5520430358, longi: 11.3804744483,
    name: "Probstzella",
    plz: "07330"
  },
  {
    lati: 53.5830757642, longi: 9.80717632789,
    name: "Hamburg",
    plz: "22589"
  },
  {
    lati: 52.092850607, longi: 10.2143253744,
    name: "Baddeckenstedt",
    plz: "38271"
  },
  {
    lati: 50.1155337861, longi: 8.68287721709,
    name: "Frankfurt am Main",
    plz: "60313"
  },
  {
    lati: 49.6286340592, longi: 8.36632144678,
    name: "Worms",
    plz: "67547"
  },
  {
    lati: 49.0925385284, longi: 8.13745008229,
    name: "Steinweiler",
    plz: "76872"
  },
  {
    lati: 50.8638935121, longi: 6.09832655817,
    name: "Herzogenrath",
    plz: "52134"
  },
  {
    lati: 50.8982088608, longi: 6.26125290769,
    name: "Aldenhoven",
    plz: "52457"
  },
  {
    lati: 50.928245742, longi: 6.36875082918,
    name: "Jülich",
    plz: "52428"
  },
  {
    lati: 51.2925323997, longi: 6.36457911617,
    name: "Viersen",
    plz: "41749"
  },
  {
    lati: 51.4429053754, longi: 6.55024842871,
    name: "Neukirchen-Vluyn",
    plz: "47506"
  },
  {
    lati: 51.1685182766, longi: 6.76153148252,
    name: "Neuss",
    plz: "41468"
  },
  {
    lati: 48.0491105648, longi: 8.69674720257,
    name: "Gunningen",
    plz: "78594"
  },
  {
    lati: 50.1106811751, longi: 8.68262523672,
    name: "Frankfurt am Main",
    plz: "60311"
  },
  {
    lati: 48.0198413339, longi: 8.93253366981,
    name: "Fridingen an der Donau",
    plz: "78567"
  },
  {
    lati: 54.5833316876, longi: 9.16571958983,
    name: "Viöl",
    plz: "25884"
  },
  {
    lati: 49.8735441207, longi: 9.13757275915,
    name: "Großwallstadt",
    plz: "63868"
  },
  {
    lati: 48.4230999205, longi: 9.26896748636,
    name: "Lichtenstein",
    plz: "72805"
  },
  {
    lati: 48.3050929712, longi: 7.94283141846,
    name: "Seelbach",
    plz: "77960"
  },
  {
    lati: 49.3053396424, longi: 7.98385112464,
    name: "Edenkoben",
    plz: "67480"
  },
  {
    lati: 52.3269289133, longi: 8.14140880981,
    name: "Belm",
    plz: "49191"
  },
  {
    lati: 50.7620289774, longi: 10.1450172152,
    name: "Stadtlengsfeld, Weilar, Urnshausen",
    plz: "36457"
  },
  {
    lati: 51.5905191451, longi: 10.592604462,
    name: "Walkenried",
    plz: "37445"
  },
  {
    lati: 49.7286008253, longi: 10.2514508025,
    name: "Rödelsee",
    plz: "97348"
  },
  {
    lati: 51.2233739941, longi: 6.79652521184,
    name: "Düsseldorf",
    plz: "40211"
  },
  {
    lati: 50.7873747276, longi: 12.9392596791,
    name: "Chemnitz",
    plz: "09125"
  },
  {
    lati: 48.0194273203, longi: 12.0360444634,
    name: "in der Gde Frauenneuharting",
    plz: "83565"
  },
  {
    lati: 48.6216920648, longi: 12.2170927796,
    name: "Essenbach",
    plz: "84051"
  },
  {
    lati: 48.7985643985, longi: 13.6658706637,
    name: "Grainet",
    plz: "94143"
  },
  {
    lati: 51.201493559, longi: 6.41984519554,
    name: "Mönchengladbach",
    plz: "41061"
  },
  {
    lati: 51.7217668179, longi: 9.03822555309,
    name: "Bad Driburg",
    plz: "33014"
  },
  {
    lati: 53.0691149019, longi: 8.84463820043,
    name: "Bremen",
    plz: "28205"
  },
  {
    lati: 49.115095891, longi: 9.6466853196,
    name: "Michelfeld",
    plz: "74545"
  },
  {
    lati: 51.0687472187, longi: 9.95109628822,
    name: "Sontra",
    plz: "36205"
  },
  {
    lati: 47.7339269122, longi: 9.47069290733,
    name: "Oberteuringen",
    plz: "88094"
  },
  {
    lati: 47.936645706, longi: 9.53092611333,
    name: "Altshausen",
    plz: "88361"
  },
  {
    lati: 54.3538214079, longi: 12.5286784155,
    name: "Saal",
    plz: "18317"
  },
  {
    lati: 53.2401004401, longi: 10.5720505453,
    name: "Reinstorf",
    plz: "21400"
  },
  {
    lati: 52.5985331147, longi: 13.3587332226,
    name: "Berlin Märkisches Viertel",
    plz: "13439"
  },
  {
    lati: 52.5681011691, longi: 13.371647429,
    name: "Berlin-West",
    plz: "13409"
  },
  {
    lati: 50.7647436844, longi: 13.8392776643,
    name: "Altenberg",
    plz: "01778"
  },
  {
    lati: 49.7124858301, longi: 7.02128663738,
    name: "Malborn",
    plz: "54426"
  },
  {
    lati: 49.4309708069, longi: 11.5315493015,
    name: "Alfeld",
    plz: "91236"
  },
  {
    lati: 48.1326329007, longi: 11.5541710562,
    name: "München",
    plz: "80336"
  },
  {
    lati: 49.5813340003, longi: 7.55647364543,
    name: "Hinzweiler",
    plz: "67756"
  },
  {
    lati: 52.5634403945, longi: 7.94330549565,
    name: "Bersenbrück",
    plz: "49593"
  },
  {
    lati: 47.7822588985, longi: 9.76684086914,
    name: "Vogt",
    plz: "88267"
  },
  {
    lati: 53.5185445711, longi: 10.0958402765,
    name: "Hamburg, Oststeinbek",
    plz: "22113"
  },
  {
    lati: 53.6931880595, longi: 10.1921783662,
    name: "Ammersbek",
    plz: "22949"
  },
  {
    lati: 48.5455000026, longi: 9.04416768629,
    name: "Tübingen",
    plz: "72076"
  },
  {
    lati: 54.0726187503, longi: 13.1285169157,
    name: "Rakow",
    plz: "18516"
  },
  {
    lati: 52.089428607, longi: 13.1516343094,
    name: "Luckenwalde",
    plz: "14943"
  },
  {
    lati: 52.3976413206, longi: 13.5643011508,
    name: "Berlin Bohnsdorf",
    plz: "12526"
  },
  {
    lati: 49.7945091588, longi: 9.93508953327,
    name: "Würzburg",
    plz: "97070"
  },
  {
    lati: 49.6921456627, longi: 11.1106221781,
    name: "Pinzberg",
    plz: "91361"
  },
  {
    lati: 49.5510742671, longi: 11.9416907203,
    name: "Hirschau",
    plz: "92242"
  },
  {
    lati: 50.6861269342, longi: 12.0104535003,
    name: "Zeulenroda-Triebes, Weißendorf",
    plz: "07950"
  },
  {
    lati: 50.1227360006, longi: 12.0178085112,
    name: "Marktleuthen",
    plz: "95168"
  },
  {
    lati: 48.999892885, longi: 12.2689340701,
    name: "Barbing",
    plz: "93092"
  },
  {
    lati: 47.9885208271, longi: 10.7874906667,
    name: "Waal",
    plz: "86875"
  },
  {
    lati: 52.5641782329, longi: 13.4218558762,
    name: "Berlin Pankow",
    plz: "13189"
  },
  {
    lati: 48.573261456, longi: 13.6141906646,
    name: "Obernzell",
    plz: "94130"
  },
  {
    lati: 49.9842453752, longi: 8.16835325849,
    name: "Mainz",
    plz: "55126"
  },
  {
    lati: 53.1320074148, longi: 8.87289320066,
    name: "Bremen",
    plz: "28357"
  },
  {
    lati: 48.8429455822, longi: 9.22348371275,
    name: "Stuttgart",
    plz: "70378"
  },
  {
    lati: 48.5690372903, longi: 9.30609895028,
    name: "Grafenberg",
    plz: "72661"
  },
  {
    lati: 54.3559240779, longi: 9.47847057839,
    name: "Tetenhusen",
    plz: "24817"
  },
  {
    lati: 48.6418497503, longi: 11.7653918878,
    name: "Mainburg",
    plz: "84048"
  },
  {
    lati: 51.3397940855, longi: 9.42092892606,
    name: "Kassel",
    plz: "34128"
  },
  {
    lati: 49.3099548006, longi: 9.46651669915,
    name: "Jagsthausen",
    plz: "74249"
  },
  {
    lati: 48.9814643828, longi: 12.2069001388,
    name: "Neutraubling",
    plz: "93073"
  },
  {
    lati: 47.8628348462, longi: 11.0237690687,
    name: "Wessobrunn",
    plz: "82405"
  },
  {
    lati: 48.8390991104, longi: 9.19373864408,
    name: "Stuttgart",
    plz: "70437"
  },
  {
    lati: 49.3446123846, longi: 9.24788641565,
    name: "Billigheim",
    plz: "74842"
  },
  {
    lati: 54.6336200787, longi: 9.7961298784,
    name: "Süderbrarup",
    plz: "24392"
  },
  {
    lati: 53.6183171607, longi: 10.7001315022,
    name: "Mölln",
    plz: "23879"
  },
  {
    lati: 50.2945230657, longi: 11.7987547431,
    name: "Leupoldsgrün",
    plz: "95191"
  },
  {
    lati: 53.7461648582, longi: 7.52865449012,
    name: "Langeoog",
    plz: "26465"
  },
  {
    lati: 48.6799275726, longi: 8.08646827872,
    name: "Ottersweier",
    plz: "77833"
  },
  {
    lati: 48.3523977697, longi: 8.97651066905,
    name: "Hechingen",
    plz: "72379"
  },
  {
    lati: 50.9914115063, longi: 9.12974918765,
    name: "Jesberg",
    plz: "34632"
  },
  {
    lati: 54.0855574141, longi: 9.29690886512,
    name: "Schafstedt, Weidenhof, Bornholt",
    plz: "25725"
  },
  {
    lati: 48.4454107342, longi: 11.1236394838,
    name: "Aichach",
    plz: "86551"
  },
  {
    lati: 50.8900782399, longi: 9.97539363656,
    name: "Heringen",
    plz: "36266"
  },
  {
    lati: 48.2690622134, longi: 12.5807771943,
    name: "Töging a. Inn",
    plz: "84513"
  },
  {
    lati: 50.4955092415, longi: 12.9807338966,
    name: "Sehma",
    plz: "09465"
  },
  {
    lati: 51.3248552939, longi: 12.6260617411,
    name: "Brandis",
    plz: "04821"
  },
  {
    lati: 48.4874869215, longi: 11.5125127698,
    name: "Ilmmünster",
    plz: "85304"
  },
  {
    lati: 48.6055030422, longi: 10.8216214051,
    name: "Nordendorf",
    plz: "86695"
  },
  {
    lati: 50.8527215448, longi: 9.4643655877,
    name: "Oberaula",
    plz: "36280"
  },
  {
    lati: 54.1533255436, longi: 9.48755529091,
    name: "Lütjenwestedt, Tackesdorf",
    plz: "25585"
  },
  {
    lati: 49.2541794205, longi: 9.50844232692,
    name: "Zweiflingen",
    plz: "74639"
  },
  {
    lati: 51.6210996048, longi: 6.18234616729,
    name: "Weeze",
    plz: "47652"
  },
  {
    lati: 47.7036784311, longi: 7.65407142732,
    name: "Kandern",
    plz: "79400"
  },
  {
    lati: 53.5594235122, longi: 8.60480446592,
    name: "Bremerhaven",
    plz: "27576"
  },
  {
    lati: 49.0734051867, longi: 12.0589262871,
    name: "Lappersdorf",
    plz: "93138"
  },
  {
    lati: 52.4969350206, longi: 13.4265362154,
    name: "Berlin Kreuzberg",
    plz: "10999"
  },
  {
    lati: 51.7778375037, longi: 14.4405562997,
    name: "Cottbus",
    plz: "03052"
  },
  {
    lati: 54.0754646789, longi: 9.78841764647,
    name: "Aukrug, Wiedenborstel",
    plz: "24613"
  },
  {
    lati: 49.8741056245, longi: 9.90283505277,
    name: "Güntersleben",
    plz: "97261"
  },
  {
    lati: 54.2544040692, longi: 10.021915338,
    name: "Rumohr",
    plz: "24254"
  },
  {
    lati: 54.2872984204, longi: 10.086148647,
    name: "Kiel",
    plz: "24113"
  },
  {
    lati: 50.5623193267, longi: 12.7343206008,
    name: "Lauter-Bernsbach",
    plz: "08315"
  },
  {
    lati: 49.4437739711, longi: 8.80303027026,
    name: "Schönau",
    plz: "69250"
  },
  {
    lati: 49.3509549665, longi: 8.88719187791,
    name: "Spechbach",
    plz: "74937"
  },
  {
    lati: 50.7188554237, longi: 8.61149084771,
    name: "Lohra",
    plz: "35102"
  },
  {
    lati: 52.1049116833, longi: 9.36299043839,
    name: "Hameln",
    plz: "31785"
  },
  {
    lati: 52.175427524, longi: 9.11154274,
    name: "Rinteln",
    plz: "31737"
  },
  {
    lati: 48.4041347551, longi: 7.79053486616,
    name: "Meißenheim",
    plz: "77974"
  },
  {
    lati: 52.795927756, longi: 7.20228330347,
    name: "Haren",
    plz: "49733"
  },
  {
    lati: 54.2466194961, longi: 13.9172833921,
    name: "Kröslin, Krummin, Lassan u.a.",
    plz: "17440"
  },
  {
    lati: 52.286802342, longi: 7.45973426445,
    name: "Rheine",
    plz: "48429"
  },
  {
    lati: 49.5062028293, longi: 11.7366536888,
    name: "Sulzbach-Rosenberg",
    plz: "92237"
  },
  {
    lati: 48.7451869986, longi: 9.16862959994,
    name: "Stuttgart",
    plz: "70597"
  },
  {
    lati: 53.0019700688, longi: 10.9568277283,
    name: "Waddeweitz",
    plz: "29496"
  },
  {
    lati: 53.1564390348, longi: 10.3861978679,
    name: "Melbeck, Barnstedt",
    plz: "21406"
  },
  {
    lati: 50.1982136456, longi: 9.9272405159,
    name: "Oberthulba",
    plz: "97723"
  },
  {
    lati: 48.1258399703, longi: 12.5219672625,
    name: "Engelsberg",
    plz: "84549"
  },
  {
    lati: 51.3487615897, longi: 6.54617410853,
    name: "Krefeld",
    plz: "47803"
  },
  {
    lati: 54.1767545312, longi: 13.3500417995,
    name: "Greifswald",
    plz: "17493"
  },
  {
    lati: 48.8786222499, longi: 13.6732910054,
    name: "Philippsreut",
    plz: "94158"
  },
  {
    lati: 51.6319831704, longi: 14.6562340663,
    name: "Döbern",
    plz: "03159"
  },
  {
    lati: 51.4517593802, longi: 14.0980560797,
    name: "Lauta",
    plz: "02991"
  },
  {
    lati: 51.4100296396, longi: 9.10362875263,
    name: "Volkmarsen",
    plz: "34471"
  },
  {
    lati: 54.6635746463, longi: 9.1274678609,
    name: "Joldelund",
    plz: "25862"
  },
  {
    lati: 50.0593795633, longi: 6.8946125394,
    name: "Bettenfeld, Niederöfflingen u.a.",
    plz: "54533"
  },
  {
    lati: 53.6235581058, longi: 10.0497779237,
    name: "Hamburg",
    plz: "22337"
  },
  {
    lati: 51.9959590357, longi: 12.11321507,
    name: "Güterglück, Lübs, Deetz, Jütrichau etc",
    plz: "39264"
  },
  {
    lati: 49.3985513226, longi: 9.06951262698,
    name: "Neckargerach",
    plz: "69437"
  },
  {
    lati: 53.953577602, longi: 10.4961457404,
    name: "Pronstorf",
    plz: "23820"
  },
  {
    lati: 50.7329184829, longi: 12.9187985111,
    name: "Burkhardtsdorf",
    plz: "09235"
  },
  {
    lati: 50.9352781825, longi: 13.5529574141,
    name: "Dorfhain",
    plz: "01738"
  },
  {
    lati: 51.4476271035, longi: 7.26815286695,
    name: "Bochum",
    plz: "44801"
  },
  {
    lati: 52.9071948513, longi: 8.56778010419,
    name: "Harpstedt, Groß Ippener, Colnrade u.a.",
    plz: "27243"
  },
  {
    lati: 53.848270221, longi: 8.62133712301,
    name: "Cuxhaven",
    plz: "27476"
  },
  {
    lati: 51.3078208457, longi: 11.9338471051,
    name: "Frankleben",
    plz: "06259"
  },
  {
    lati: 51.5288619117, longi: 11.4703086869,
    name: "Hergisdorf",
    plz: "06313"
  },
  {
    lati: 51.8949446654, longi: 11.6455359354,
    name: "Staßfurt",
    plz: "39443"
  },
  {
    lati: 52.5851525129, longi: 11.7839001589,
    name: "Stendal",
    plz: "39576"
  },
  {
    lati: 48.8196902672, longi: 11.6876295264,
    name: "Pförring",
    plz: "85104"
  },
  {
    lati: 51.384298874, longi: 7.71643984226,
    name: "Iserlohn",
    plz: "58636"
  },
  {
    lati: 47.6569769528, longi: 8.36259095844,
    name: "Wutöschingen",
    plz: "79793"
  },
  {
    lati: 48.8769070925, longi: 9.07722446454,
    name: "Schwieberdingen",
    plz: "71701"
  },
  {
    lati: 48.6607852578, longi: 9.17801238023,
    name: "Leinfelden-Echterdingen",
    plz: "70771"
  },
  {
    lati: 54.3440611833, longi: 8.77671208941,
    name: "Garding, Osterhever, Poppenbüll u.a.",
    plz: "25836"
  },
  {
    lati: 49.6249353246, longi: 10.6374887335,
    name: "Gutenstetten",
    plz: "91468"
  },
  {
    lati: 53.2161337021, longi: 11.3855071386,
    name: "Neu Kaliß",
    plz: "19294"
  },
  {
    lati: 48.4834956051, longi: 9.27768082523,
    name: "Eningen",
    plz: "72800"
  },
  {
    lati: 53.4570679434, longi: 9.97184434282,
    name: "Hamburg",
    plz: "21073"
  },
  {
    lati: 50.8354114922, longi: 12.9247101199,
    name: "Chemnitz",
    plz: "09111"
  },
  {
    lati: 48.5803873147, longi: 9.50361951929,
    name: "Bissingen an der Teck",
    plz: "73266"
  },
  {
    lati: 49.5448181061, longi: 7.59332943087,
    name: "Rothselberg u.a.",
    plz: "67753"
  },
  {
    lati: 50.7418181044, longi: 8.36200488195,
    name: "Dillenburg",
    plz: "35688"
  },
  {
    lati: 49.2478003123, longi: 8.72866308582,
    name: "Mühlhausen",
    plz: "69242"
  },
  {
    lati: 48.5546318901, longi: 8.84232490396,
    name: "Gäufelden",
    plz: "71126"
  },
  {
    lati: 49.968569962, longi: 7.0064358118,
    name: "Zeltingen-Rachtig, Erden, Lösnich u.a.",
    plz: "54492"
  },
  {
    lati: 47.9314982358, longi: 12.1764383158,
    name: "Vogtareuth",
    plz: "83569"
  },
  {
    lati: 47.7941861858, longi: 9.69597172598,
    name: "Schlier",
    plz: "88281"
  },
  {
    lati: 49.5258243045, longi: 11.2660514576,
    name: "Lauf an der Pegnitz",
    plz: "91207"
  },
  {
    lati: 47.9590086188, longi: 11.379880682,
    name: "Berg",
    plz: "82335"
  },
  {
    lati: 48.8384777774, longi: 10.7712313703,
    name: "Fünfstetten",
    plz: "86681"
  },
  {
    lati: 48.1602078211, longi: 10.889424436,
    name: "Scheuring",
    plz: "86937"
  },
  {
    lati: 52.4761646385, longi: 13.3607712186,
    name: "Berlin Schöneberg",
    plz: "10829"
  },
  {
    lati: 52.2047360662, longi: 8.07509626785,
    name: "Georgsmarienhütte",
    plz: "49124"
  },
  {
    lati: 49.3570765225, longi: 7.0684661832,
    name: "Merchweiler",
    plz: "66589"
  },
  {
    lati: 49.4944429778, longi: 8.45319813994,
    name: "Mannheim",
    plz: "68159"
  },
  {
    lati: 50.4443153607, longi: 11.8260480117,
    name: "Gefell",
    plz: "07926"
  },
  {
    lati: 51.624980639, longi: 11.003755474,
    name: "Ballenstedt, Harzgerode",
    plz: "06493"
  },
  {
    lati: 51.8632196956, longi: 11.2827308619,
    name: "Hedersleben",
    plz: "06458"
  },
  {
    lati: 49.2527731416, longi: 10.8432705076,
    name: "Windsbach",
    plz: "91575"
  },
  {
    lati: 48.3622798396, longi: 11.0034399739,
    name: "Friedberg",
    plz: "86316"
  },
  {
    lati: 50.9230541008, longi: 12.12847438,
    name: "Brahmenau",
    plz: "07554"
  },
  {
    lati: 51.317721487, longi: 12.4250218736,
    name: "Leipzig",
    plz: "04299"
  },
  {
    lati: 52.9513731718, longi: 12.9811899783,
    name: "Lindow u.a.",
    plz: "16835"
  },
  {
    lati: 47.7644107821, longi: 8.98319444767,
    name: "Radolfzell am Bodensee",
    plz: "78315"
  },
  {
    lati: 49.0836638888, longi: 9.06522296568,
    name: "Brackenheim",
    plz: "74336"
  },
  {
    lati: 50.0239970095, longi: 8.28543754939,
    name: "Mainz-Kastel",
    plz: "55252"
  },
  {
    lati: 50.9503218737, longi: 6.94859162842,
    name: "Köln",
    plz: "50670"
  },
  {
    lati: 52.6184027365, longi: 12.3221997295,
    name: "Rathenow",
    plz: "14712"
  },
  {
    lati: 49.0495785447, longi: 12.3483155815,
    name: "Wiesent",
    plz: "93109"
  },
  {
    lati: 50.4479861761, longi: 11.5243781142,
    name: "Wurzbach",
    plz: "07343"
  },
  {
    lati: 52.2635805834, longi: 10.523661309,
    name: "Braunschweig",
    plz: "38100"
  },
  {
    lati: 47.6717719142, longi: 7.56721931575,
    name: "Efringen-Kirchen",
    plz: "79588"
  },
  {
    lati: 52.465292312, longi: 13.346205557,
    name: "Berlin Schöneberg",
    plz: "12157"
  },
  {
    lati: 51.4113172771, longi: 7.82958640671,
    name: "Menden",
    plz: "58710"
  },
  {
    lati: 53.5300473414, longi: 10.1490654183,
    name: "Hamburg",
    plz: "22115"
  },
  {
    lati: 48.8598790956, longi: 9.70442871971,
    name: "Alfdorf, Schillinghof",
    plz: "73553"
  },
  {
    lati: 53.9965680782, longi: 9.83141689524,
    name: "Brokstedt",
    plz: "24616"
  },
  {
    lati: 51.1376918676, longi: 13.5769157633,
    name: "Coswig",
    plz: "01640"
  },
  {
    lati: 51.1952302309, longi: 6.43347297337,
    name: "Mönchengladbach",
    plz: "41061"
  },
  {
    lati: 49.5821225609, longi: 6.9750310505,
    name: "Nonnweiler",
    plz: "66620"
  },
  {
    lati: 50.8826429048, longi: 7.98831065826,
    name: "Siegen",
    plz: "57072"
  },
  {
    lati: 49.9431043486, longi: 7.97662977133,
    name: "Ockenheim",
    plz: "55437"
  },
  {
    lati: 47.9914629078, longi: 10.003166177,
    name: "Rot an der Rot",
    plz: "88430"
  },
  {
    lati: 49.372773876, longi: 11.5652264926,
    name: "Lauterhofen",
    plz: "92283"
  },
  {
    lati: 52.5666739176, longi: 13.9004382733,
    name: "Strausberg",
    plz: "15344"
  },
  {
    lati: 49.6629547948, longi: 11.3159121174,
    name: "Hiltpoltstein",
    plz: "91355"
  },
  {
    lati: 49.4978491107, longi: 11.3927592923,
    name: "Henfenfeld",
    plz: "91239"
  },
  {
    lati: 54.4227303196, longi: 12.5336928611,
    name: "Prerow a. Darß",
    plz: "18375"
  },
  {
    lati: 54.3056952643, longi: 9.49532446549,
    name: "Hohn",
    plz: "24806"
  },
  {
    lati: 49.740400786, longi: 9.95579146088,
    name: "Würzburg",
    plz: "97084"
  },
  {
    lati: 49.7050525275, longi: 10.21229458,
    name: "Mainbernheim",
    plz: "97350"
  },
  {
    lati: 54.1376683513, longi: 11.9524310054,
    name: "Retschow, Admannshagen-Bargeshagen u.a.",
    plz: "18211"
  },
  {
    lati: 51.9097075622, longi: 10.433161563,
    name: "Goslar",
    plz: "38640"
  },
  {
    lati: 52.5838748547, longi: 13.2404764518,
    name: "Berlin Tegel",
    plz: "13505"
  },
  {
    lati: 48.814328594, longi: 11.2864358189,
    name: "Buxheim",
    plz: "85114"
  },
  {
    lati: 49.1180941542, longi: 11.4667017093,
    name: "Berching",
    plz: "92334"
  },
  {
    lati: 48.8865033323, longi: 8.23922608381,
    name: "Ötigheim",
    plz: "76470"
  },
  {
    lati: 48.7284982267, longi: 9.02112249112,
    name: "Sindelfingen",
    plz: "71067"
  },
  {
    lati: 52.1470088435, longi: 8.837812552,
    name: "Vlotho",
    plz: "32602"
  },
  {
    lati: 48.3954764195, longi: 9.0739594831,
    name: "Mössingen",
    plz: "72116"
  },
  {
    lati: 49.3288168863, longi: 9.42512280397,
    name: "Widdern",
    plz: "74259"
  },
  {
    lati: 51.7308095419, longi: 10.3066055115,
    name: "Osterode am Harz",
    plz: "37520"
  },
  {
    lati: 53.6112487221, longi: 10.13718964,
    name: "Hamburg",
    plz: "22147"
  },
  {
    lati: 48.0103245527, longi: 7.85907979896,
    name: "Freiburg im Breisgau",
    plz: "79108"
  },
  {
    lati: 51.7606125497, longi: 8.04687348931,
    name: "Beckum",
    plz: "59269"
  },
  {
    lati: 49.3832561174, longi: 8.37104845182,
    name: "Schifferstadt",
    plz: "67105"
  },
  {
    lati: 49.37440395, longi: 8.46891229015,
    name: "Otterstadt",
    plz: "67166"
  },
  {
    lati: 50.5324270057, longi: 8.66844903378,
    name: "Linden",
    plz: "35440"
  },
  {
    lati: 49.6194860562, longi: 8.76612367939,
    name: "Rimbach",
    plz: "64668"
  },
  {
    lati: 51.7741586025, longi: 7.41592262151,
    name: "Lüdinghausen",
    plz: "59348"
  },
  {
    lati: 50.8733112233, longi: 11.8567891076,
    name: "Hermsdorf",
    plz: "07629"
  },
  {
    lati: 50.9473802751, longi: 9.06594718266,
    name: "Gilserberg",
    plz: "34630"
  },
  {
    lati: 50.5762405282, longi: 9.18501188834,
    name: "Ulrichstein",
    plz: "35327"
  },
  {
    lati: 52.2720632162, longi: 14.0348999467,
    name: "Bad Saarow-Pieskow",
    plz: "15526"
  },
  {
    lati: 53.6279267824, longi: 9.99931718824,
    name: "Hamburg",
    plz: "22335"
  },
  {
    lati: 48.0436042498, longi: 10.9395280373,
    name: "Schwifting",
    plz: "86940"
  },
  {
    lati: 50.6682611376, longi: 7.12339387391,
    name: "Bonn",
    plz: "53177"
  },
  {
    lati: 53.0668842179, longi: 7.25400979672,
    name: "Rhede (Ems)",
    plz: "26899"
  },
  {
    lati: 52.9667003999, longi: 7.34105960849,
    name: "Dörpen, Lehe, u.a.",
    plz: "26892"
  },
  {
    lati: 50.4921113213, longi: 6.66322028763,
    name: "Nettersheim",
    plz: "53947"
  },
  {
    lati: 49.5554082401, longi: 9.71492604949,
    name: "Lauda-Königshofen",
    plz: "97922"
  },
  {
    lati: 48.0801767574, longi: 10.7504384421,
    name: "Lamerdingen",
    plz: "86862"
  },
  {
    lati: 49.7732563935, longi: 10.8163495598,
    name: "Pommersfelden",
    plz: "96178"
  },
  {
    lati: 53.6271946244, longi: 13.368365798,
    name: "Sponholz, Neunkirchen u.a.",
    plz: "17039"
  },
  {
    lati: 52.3055101788, longi: 13.3838228934,
    name: "Blankenfelde-Mahlow u.a.",
    plz: "15831"
  },
  {
    lati: 51.1621026299, longi: 13.6662993757,
    name: "Moritzburg",
    plz: "01468"
  },
  {
    lati: 52.247027166, longi: 13.7696732731,
    name: "Heidesee",
    plz: "15754"
  },
  {
    lati: 48.8109468137, longi: 8.93600699105,
    name: "Rutesheim",
    plz: "71277"
  },
  {
    lati: 51.1814938595, longi: 9.36297842998,
    name: "Gudensberg",
    plz: "34281"
  },
  {
    lati: 48.166742511, longi: 9.4578375276,
    name: "Riedlingen",
    plz: "88499"
  },
  {
    lati: 48.025128192, longi: 10.247001268,
    name: "Holzgünz",
    plz: "87752"
  },
  {
    lati: 48.1586935411, longi: 11.6374845071,
    name: "München",
    plz: "81927"
  },
  {
    lati: 48.4846689627, longi: 9.89117734749,
    name: "Dornstadt",
    plz: "89160"
  },
  {
    lati: 51.6338200649, longi: 9.95502185129,
    name: "Nörten-Hardenberg",
    plz: "37176"
  },
  {
    lati: 48.8945751518, longi: 12.2298684069,
    name: "Hagelstadt",
    plz: "93095"
  },
  {
    lati: 50.0410195451, longi: 10.7012819575,
    name: "Kirchlauter",
    plz: "96166"
  },
  {
    lati: 51.9174294596, longi: 7.15264436385,
    name: "Coesfeld",
    plz: "48653"
  },
  {
    lati: 49.4328223898, longi: 7.37506104459,
    name: "Brücken (Pfalz)",
    plz: "66904"
  },
  {
    lati: 48.8385868323, longi: 9.02348576868,
    name: "Ditzingen",
    plz: "71254"
  },
  {
    lati: 48.5404543904, longi: 8.72108800329,
    name: "Nagold",
    plz: "72202"
  },
  {
    lati: 49.1835114414, longi: 10.0925576065,
    name: "Satteldorf",
    plz: "74589"
  },
  {
    lati: 52.3745294078, longi: 9.73727236921,
    name: "Hannover",
    plz: "30159"
  },
  {
    lati: 50.0099008603, longi: 9.32693292269,
    name: "Laufach",
    plz: "63846"
  },
  {
    lati: 48.6550214037, longi: 9.28899463768,
    name: "Wolfschlugen",
    plz: "72649"
  },
  {
    lati: 48.4826276103, longi: 9.42363780505,
    name: "Bad Urach",
    plz: "72574"
  },
  {
    lati: 53.0980031512, longi: 8.93492887895,
    name: "Bremen",
    plz: "28355"
  },
  {
    lati: 48.0786923424, longi: 12.3939092602,
    name: "Schnaitsee",
    plz: "83530"
  },
  {
    lati: 49.6992861183, longi: 7.9276850604,
    name: "Kriegsfeld",
    plz: "67819"
  },
  {
    lati: 52.9443244263, longi: 8.23289596333,
    name: "Großenkneten",
    plz: "26197"
  },
  {
    lati: 51.5232108492, longi: 14.5222314351,
    name: "Schleife",
    plz: "02959"
  },
  {
    lati: 52.6081870663, longi: 10.2940209591,
    name: "Ahnsbeck",
    plz: "29353"
  },
  {
    lati: 47.7910628417, longi: 10.3588112533,
    name: "Haldenwang",
    plz: "87490"
  },
  {
    lati: 47.6757509918, longi: 10.8130375975,
    name: "Prem",
    plz: "86984"
  },
  {
    lati: 48.0989049726, longi: 10.8721043587,
    name: "Kaufering",
    plz: "86916"
  },
  {
    lati: 52.5661096437, longi: 7.51946439199,
    name: "Lengerich u.a.",
    plz: "49838"
  },
  {
    lati: 53.1373599366, longi: 8.20871974221,
    name: "Oldenburg",
    plz: "26122"
  },
  {
    lati: 49.3690459604, longi: 8.52614838196,
    name: "Ketsch",
    plz: "68775"
  },
  {
    lati: 48.573007693, longi: 11.0677699672,
    name: "Pöttmes",
    plz: "86554"
  },
  {
    lati: 50.2370816981, longi: 11.1486500364,
    name: "Sonnefeld",
    plz: "96242"
  },
  {
    lati: 49.6105704282, longi: 11.6411388884,
    name: "Königstein",
    plz: "92281"
  },
  {
    lati: 50.3528535894, longi: 6.86037578245,
    name: "Barweiler, Bauler, Hoffeld, Pomster, Wiesemscheid, Wirft",
    plz: "53534"
  },
  {
    lati: 51.3400529169, longi: 12.4257774863,
    name: "Leipzig",
    plz: "04318"
  },
  {
    lati: 50.7544497252, longi: 12.8271416721,
    name: "Jahnsdorf",
    plz: "09387"
  },
  {
    lati: 48.6698358935, longi: 9.39053553698,
    name: "Wendlingen am Neckar",
    plz: "73240"
  },
  {
    lati: 52.5520399712, longi: 13.4116904074,
    name: "Berlin Prenzlauer Berg",
    plz: "10439"
  },
  {
    lati: 52.442476547, longi: 13.5048022231,
    name: "Berlin",
    plz: "12487"
  },
  {
    lati: 47.6925137914, longi: 9.80613084618,
    name: "Wangen im Allgäu",
    plz: "88239"
  },
  {
    lati: 47.6579759571, longi: 7.65468850189,
    name: "Wittlingen",
    plz: "79599"
  },
  {
    lati: 50.3711152756, longi: 7.96318066953,
    name: "Altendiez",
    plz: "65624"
  },
  {
    lati: 50.1953421222, longi: 10.2719037257,
    name: "Maßbach",
    plz: "97711"
  },
  {
    lati: 54.1594162175, longi: 8.86681117899,
    name: "Büsum",
    plz: "25761"
  },
  {
    lati: 50.6205827807, longi: 8.3767630118,
    name: "Ehringshausen",
    plz: "35630"
  },
  {
    lati: 48.826415308, longi: 9.30841535503,
    name: "Waiblingen",
    plz: "71332"
  },
  {
    lati: 49.9900561075, longi: 11.1755913172,
    name: "Stadelhofen",
    plz: "96187"
  },
  {
    lati: 49.1019536637, longi: 10.4539475316,
    name: "Langfurth",
    plz: "91731"
  },
  {
    lati: 48.7483790912, longi: 8.81434154252,
    name: "Simmozheim",
    plz: "75397"
  },
  {
    lati: 52.5718612076, longi: 9.25645760859,
    name: "Husum",
    plz: "31632"
  },
  {
    lati: 51.0208510372, longi: 13.7281700576,
    name: "Dresden",
    plz: "01069"
  },
  {
    lati: 50.9643402799, longi: 6.95376312121,
    name: "Köln",
    plz: "50733"
  },
  {
    lati: 48.0872760811, longi: 7.81992758484,
    name: "Reute",
    plz: "79276"
  },
  {
    lati: 51.9474796315, longi: 7.95835025349,
    name: "Warendorf",
    plz: "48231"
  },
  {
    lati: 49.8649681727, longi: 9.34342047627,
    name: "Dammbach",
    plz: "63874"
  },
  {
    lati: 48.575589923, longi: 9.40498699902,
    name: "Beuren",
    plz: "72660"
  },
  {
    lati: 52.9501047757, longi: 9.02108155939,
    name: "Thedinghausen, Emtinghausen",
    plz: "27321"
  },
  {
    lati: 52.0057500913, longi: 8.5488988471,
    name: "Bielefeld",
    plz: "33604"
  },
  {
    lati: 50.0102153668, longi: 8.71648555382,
    name: "Dreieich",
    plz: "63303"
  },
  {
    lati: 50.1255467233, longi: 8.6762630552,
    name: "Frankfurt am Main",
    plz: "60322"
  },
  {
    lati: 48.4140573162, longi: 8.29617750076,
    name: "Bad Rippoldsau-Schapbach",
    plz: "77776"
  },
  {
    lati: 49.4386887577, longi: 9.16127797854,
    name: "Fahrenbach",
    plz: "74864"
  },
  {
    lati: 51.1649163878, longi: 6.69357229413,
    name: "Neuss",
    plz: "41466"
  },
  {
    lati: 50.882416344, longi: 7.08045151728,
    name: "Köln",
    plz: "51145"
  },
  {
    lati: 49.6203767858, longi: 7.27156371009,
    name: "Berglangenbach, Ruschberg u.a.",
    plz: "55776"
  },
  {
    lati: 49.9404814021, longi: 8.26050256398,
    name: "Mainz Ebersheim, Hechtsheim",
    plz: "55129"
  },
  {
    lati: 53.2864400321, longi: 10.4547087074,
    name: "Adendorf",
    plz: "21365"
  },
  {
    lati: 50.9108800295, longi: 12.8556961356,
    name: "Taura b. Burgstädt",
    plz: "09249"
  },
  {
    lati: 51.4535274999, longi: 8.29879099924,
    name: "Warstein",
    plz: "59581"
  },
  {
    lati: 54.2137244617, longi: 8.9129574146,
    name: "Wesselburen",
    plz: "25764"
  },
  {
    lati: 50.4385036353, longi: 7.68007723095,
    name: "Höhr-Grenzhausen",
    plz: "56203"
  },
  {
    lati: 50.237406454, longi: 6.30500533623,
    name: "Bleialf",
    plz: "54608"
  },
  {
    lati: 50.2834543108, longi: 6.66161402039,
    name: "Hillesheim",
    plz: "54576"
  },
  {
    lati: 50.7092120337, longi: 9.6909173148,
    name: "Burghaun",
    plz: "36151"
  },
  {
    lati: 54.2617180429, longi: 9.52409572038,
    name: "Elsdorf-Westermühlen",
    plz: "24800"
  },
  {
    lati: 48.0011794036, longi: 10.2689396772,
    name: "Ungerhausen",
    plz: "87781"
  },
  {
    lati: 51.9047607955, longi: 10.4830856013,
    name: "Goslar",
    plz: "38642"
  },
  {
    lati: 52.1848842554, longi: 10.578513405,
    name: "Wolfenbüttel",
    plz: "38302"
  },
  {
    lati: 49.4669718559, longi: 11.0903356917,
    name: "Nürnberg",
    plz: "90409"
  },
  {
    lati: 54.5947729705, longi: 9.65621964803,
    name: "Tolk, Twedt",
    plz: "24894"
  },
  {
    lati: 49.7590031953, longi: 8.65999804057,
    name: "Seeheim-Jugenheim",
    plz: "64342"
  },
  {
    lati: 50.7748081652, longi: 8.34889191261,
    name: "Dillenburg",
    plz: "35690"
  },
  {
    lati: 48.0647054675, longi: 8.51885190337,
    name: "Villingen-Schwenningen",
    plz: "78054"
  },
  {
    lati: 54.1853647631, longi: 7.91193980285,
    name: "Helgoland",
    plz: "27498"
  },
  {
    lati: 51.5561523414, longi: 7.90889565272,
    name: "Werl",
    plz: "59457"
  },
  {
    lati: 51.0677163201, longi: 6.33900074728,
    name: "Erkelenz",
    plz: "41812"
  },
  {
    lati: 51.328984588, longi: 6.96540869017,
    name: "Heiligenhaus",
    plz: "42579"
  },
  {
    lati: 51.5134865478, longi: 9.09230506928,
    name: "Warburg",
    plz: "34414"
  },
  {
    lati: 54.1547872428, longi: 9.57804083371,
    name: "Todenbüttel",
    plz: "24819"
  },
  {
    lati: 50.9814347769, longi: 7.98589611196,
    name: "Kreuztal",
    plz: "57223"
  },
  {
    lati: 49.6244597044, longi: 7.61104269114,
    name: "Lauterecken u.a.",
    plz: "67742"
  },
  {
    lati: 53.5985056355, longi: 9.9307523268,
    name: "Hamburg",
    plz: "22527"
  },
  {
    lati: 47.8438253933, longi: 9.66511868583,
    name: "Baienfurt",
    plz: "88255"
  },
  {
    lati: 49.4293461944, longi: 9.67977177028,
    name: "Assamstadt",
    plz: "97959"
  },
  {
    lati: 48.7239048737, longi: 10.7722180442,
    name: "Donauwörth",
    plz: "86609"
  },
  {
    lati: 51.5732443534, longi: 13.0859969069,
    name: "Arzberg, Beilrode",
    plz: "04886"
  },
  {
    lati: 51.0266923878, longi: 13.7005453553,
    name: "Dresden",
    plz: "01187"
  },
  {
    lati: 53.6707257244, longi: 11.278323216,
    name: "Brüsewitz",
    plz: "19071"
  },
  {
    lati: 50.4644114545, longi: 7.72679377355,
    name: "Ransbach-Baumbach",
    plz: "56235"
  },
  {
    lati: 52.2572623193, longi: 9.09744215419,
    name: "Ahnsen",
    plz: "31708"
  },
  {
    lati: 51.2227333072, longi: 9.58272200791,
    name: "Söhrewald",
    plz: "34320"
  },
  {
    lati: 53.6968496249, longi: 9.55737017585,
    name: "Seestermühe",
    plz: "25371"
  },
  {
    lati: 49.1305594805, longi: 11.1215660058,
    name: "Heideck",
    plz: "91180"
  },
  {
    lati: 48.1430852273, longi: 11.2790016424,
    name: "Alling",
    plz: "82239"
  },
  {
    lati: 53.2108354202, longi: 10.9052236629,
    name: "Neu Darchau",
    plz: "29490"
  },
  {
    lati: 49.9412216101, longi: 11.967785581,
    name: "Neusorg",
    plz: "95700"
  },
  {
    lati: 49.0313245587, longi: 12.0849379972,
    name: "Regensburg",
    plz: "93059"
  },
  {
    lati: 52.5233700518, longi: 13.3595527868,
    name: "Berlin Moabit",
    plz: "10557"
  },
  {
    lati: 48.0988774868, longi: 11.0449124681,
    name: "Eresing",
    plz: "86941"
  },
  {
    lati: 48.1815521854, longi: 11.2946503106,
    name: "Emmering",
    plz: "82275"
  },
  {
    lati: 50.0506490525, longi: 7.72878833596,
    name: "Bacharach, Breitscheid",
    plz: "55422"
  },
  {
    lati: 51.253156107, longi: 7.74551445477,
    name: "Werdohl",
    plz: "58791"
  },
  {
    lati: 50.157377827, longi: 8.76327158978,
    name: "Frankfurt am Main",
    plz: "60388"
  },
  {
    lati: 48.6479984418, longi: 9.57507349351,
    name: "Zell unter Aichelberg",
    plz: "73119"
  },
  {
    lati: 52.9702513662, longi: 10.5677418173,
    name: "Uelzen",
    plz: "29525"
  },
  {
    lati: 51.5221304533, longi: 9.97199684615,
    name: "Göttingen",
    plz: "37085"
  },
  {
    lati: 52.3853861473, longi: 9.71171169058,
    name: "Hannover",
    plz: "30167"
  },
  {
    lati: 51.6118526943, longi: 11.0124685613,
    name: "Ballenstedt, Harzgerode",
    plz: "06493"
  },
  {
    lati: 51.1524867842, longi: 13.7873379518,
    name: "Dresden",
    plz: "01108"
  },
  {
    lati: 51.3660722216, longi: 6.94415110772,
    name: "Essen",
    plz: "45219"
  },
  {
    lati: 52.7924712543, longi: 7.90149593632,
    name: "Lastrup",
    plz: "49688"
  },
  {
    lati: 51.5371580266, longi: 7.59897195588,
    name: "Dortmund",
    plz: "44319"
  },
  {
    lati: 48.9632778188, longi: 8.80002813507,
    name: "Ötisheim",
    plz: "75443"
  },
  {
    lati: 50.0043538544, longi: 8.23604403012,
    name: "Mainz",
    plz: "55122"
  },
  {
    lati: 52.879740958, longi: 8.40364894519,
    name: "Wildeshausen",
    plz: "27793"
  },
  {
    lati: 50.6958233212, longi: 11.5191129471,
    name: "Krölpa",
    plz: "07387"
  },
  {
    lati: 49.8019915412, longi: 9.48257641652,
    name: "Hasloch",
    plz: "97907"
  },
  {
    lati: 48.5610885241, longi: 12.7195969181,
    name: "Simbach",
    plz: "94436"
  },
  {
    lati: 52.2489813785, longi: 13.5500079422,
    name: "Mittenwalde",
    plz: "15749"
  },
  {
    lati: 53.6711275152, longi: 14.2218640908,
    name: "Vogelsang-Warsin, Meiersberg, Mönkebude u.a.",
    plz: "17375"
  },
  {
    lati: 49.6538210455, longi: 11.1709595917,
    name: "Igensdorf",
    plz: "91338"
  },
  {
    lati: 47.5118307056, longi: 11.2398731446,
    name: "Krün",
    plz: "82494"
  },
  {
    lati: 48.4320784619, longi: 11.3270374392,
    name: "Hilgertshausen-Tandern",
    plz: "86567"
  },
  {
    lati: 50.6565631576, longi: 10.1324635086,
    name: "Kaltennordheim",
    plz: "36452"
  },
  {
    lati: 50.1038030805, longi: 9.87425735312,
    name: "Hammelburg",
    plz: "97762"
  },
  {
    lati: 54.4025133206, longi: 9.61275779985,
    name: "Owschlag u.a.",
    plz: "24811"
  },
  {
    lati: 48.4254567497, longi: 10.7183227345,
    name: "Adelsried",
    plz: "86477"
  },
  {
    lati: 49.9797033795, longi: 11.5233683936,
    name: "Heinersreuth",
    plz: "95500"
  },
  {
    lati: 48.5573145278, longi: 11.8706760137,
    name: "Hörgertshausen",
    plz: "85413"
  },
  {
    lati: 51.6271852, longi: 6.27201197056,
    name: "Kevelaer-Kervenheim",
    plz: "47627"
  },
  {
    lati: 49.5080802734, longi: 6.73190925735,
    name: "Losheim",
    plz: "66679"
  },
  {
    lati: 51.1425080114, longi: 6.87244967589,
    name: "Düsseldorf",
    plz: "40593"
  },
  {
    lati: 50.4153056702, longi: 7.45562311739,
    name: "Weißenthurm",
    plz: "56575"
  },
  {
    lati: 50.0191298707, longi: 7.89140325914,
    name: "Rüdesheim am Rhein",
    plz: "65385"
  },
  {
    lati: 49.0556761182, longi: 10.7099899811,
    name: "Gnotzheim",
    plz: "91728"
  },
  {
    lati: 54.0816028025, longi: 13.8034595292,
    name: "Karlshagen",
    plz: "17449"
  },
  {
    lati: 50.7073271658, longi: 12.88554925,
    name: "Gornsdorf",
    plz: "09390"
  },
  {
    lati: 51.915889964, longi: 12.6932635979,
    name: "Wittenberg",
    plz: "06888"
  },
  {
    lati: 49.9522849457, longi: 11.4579517385,
    name: "Eckersdorf",
    plz: "95488"
  },
  {
    lati: 48.3231533257, longi: 11.6636927792,
    name: "Neufahrn b. Freising",
    plz: "85375"
  },
  {
    lati: 48.1338013027, longi: 11.6883071515,
    name: "München",
    plz: "81829"
  },
  {
    lati: 49.287426753, longi: 10.8084092365,
    name: "Neuendettelsau",
    plz: "91564"
  },
  {
    lati: 48.4659570533, longi: 10.4988554099,
    name: "Winterbach",
    plz: "89368"
  },
  {
    lati: 49.9894695859, longi: 6.69297222024,
    name: "Spangdahlem",
    plz: "54529"
  },
  {
    lati: 48.0756610247, longi: 8.96326880179,
    name: "Irndorf",
    plz: "78597"
  },
  {
    lati: 54.3023981532, longi: 12.3598608073,
    name: "Dierhagen",
    plz: "18347"
  },
  {
    lati: 47.7218479584, longi: 12.3830678328,
    name: "Schleching",
    plz: "83259"
  },
  {
    lati: 53.87248695, longi: 10.7372981683,
    name: "Lübeck",
    plz: "23566"
  },
  {
    lati: 50.1029010394, longi: 10.1453290098,
    name: "Poppenhausen",
    plz: "97490"
  },
  {
    lati: 54.0544880731, longi: 10.3142607336,
    name: "Tensfeld",
    plz: "23824"
  },
  {
    lati: 50.9017506128, longi: 11.0355833841,
    name: "Rockhausen, Klettbach",
    plz: "99102"
  },
  {
    lati: 49.5422084797, longi: 11.1158071757,
    name: "Heroldsberg",
    plz: "90562"
  },
  {
    lati: 50.1106153859, longi: 8.67282325259,
    name: "Frankfurt am Main (Taunusturm)",
    plz: "60310"
  },
  {
    lati: 48.4306883085, longi: 8.81572950919,
    name: "Starzach",
    plz: "72181"
  },
  {
    lati: 48.2235768436, longi: 8.79733367913,
    name: "Dotternhausen",
    plz: "72359"
  },
  {
    lati: 47.9598401294, longi: 8.49355864583,
    name: "Donaueschingen",
    plz: "78166"
  },
  {
    lati: 49.3209462465, longi: 8.38057716168,
    name: "Dudenhofen",
    plz: "67373"
  },
  {
    lati: 52.5551611999, longi: 9.46512199219,
    name: "Neustadt am Rübenberge",
    plz: "31535"
  },
  {
    lati: 49.3151179632, longi: 7.98962036655,
    name: "Gommersheim",
    plz: "67377"
  },
  {
    lati: 49.9988844911, longi: 6.75038512517,
    name: "Landscheid",
    plz: "54526"
  },
  {
    lati: 50.4333880492, longi: 6.89024803247,
    name: "Reifferscheid, Kaltenborn, Wershofen u.a.",
    plz: "53520"
  },
  {
    lati: 52.0021829727, longi: 6.93515136441,
    name: "Stadtlohn",
    plz: "48703"
  },
  {
    lati: 50.529675259, longi: 7.12512291529,
    name: "Bad Neuenahr-Ahrweiler",
    plz: "53474"
  },
  {
    lati: 51.6837871388, longi: 7.48568796834,
    name: "Selm",
    plz: "59379"
  },
  {
    lati: 53.5732028927, longi: 10.0148272009,
    name: "Hamburg",
    plz: "22085"
  },
  {
    lati: 49.0216029569, longi: 9.65425443007,
    name: "Oberrot",
    plz: "74420"
  },
  {
    lati: 50.5278737593, longi: 12.681870757,
    name: "Bockau",
    plz: "08324"
  },
  {
    lati: 54.1124327633, longi: 12.7806409132,
    name: "Tribsees",
    plz: "18465"
  },
  {
    lati: 51.1702385472, longi: 7.15800470999,
    name: "Remscheid",
    plz: "42857"
  },
  {
    lati: 50.5253001529, longi: 8.48864940647,
    name: "Wetzlar",
    plz: "35580"
  },
  {
    lati: 50.0850962036, longi: 8.71526723218,
    name: "Frankfurt am Main",
    plz: "60599"
  },
  {
    lati: 49.420251577, longi: 8.7579449044,
    name: "Heidelberg",
    plz: "69118"
  },
  {
    lati: 52.293846086, longi: 13.6997026289,
    name: "Königs Wusterhausen",
    plz: "15712"
  },
  {
    lati: 53.9915124096, longi: 13.1651701878,
    name: "Loitz",
    plz: "17121"
  },
  {
    lati: 52.4496641358, longi: 13.2572192796,
    name: "Berlin Zehlendorf",
    plz: "14169"
  },
  {
    lati: 52.4556631713, longi: 13.3148371693,
    name: "Berlin Steglitz",
    plz: "12165"
  },
  {
    lati: 50.9344406064, longi: 8.92648308486,
    name: "Wohratal",
    plz: "35288"
  },
  {
    lati: 49.2247879047, longi: 10.0859442463,
    name: "Wallhausen",
    plz: "74599"
  },
  {
    lati: 49.4008897393, longi: 10.2939183197,
    name: "Windelsbach",
    plz: "91635"
  },
  {
    lati: 48.5940767512, longi: 11.397317568,
    name: "Hohenwart",
    plz: "86558"
  },
  {
    lati: 49.5600830822, longi: 11.4831576739,
    name: "Vorra",
    plz: "91247"
  },
  {
    lati: 51.4785915087, longi: 11.8964009835,
    name: "Halle/ Saale",
    plz: "06126"
  },
  {
    lati: 48.1627132055, longi: 11.5576113633,
    name: "München",
    plz: "80797"
  },
  {
    lati: 52.1070345844, longi: 8.61573357567,
    name: "Herford",
    plz: "32051"
  },
  {
    lati: 50.1156239494, longi: 8.65873090487,
    name: "Frankfurt am Main",
    plz: "60325"
  },
  {
    lati: 53.3879890337, longi: 8.11723337886,
    name: "Varel",
    plz: "26316"
  },
  {
    lati: 49.5428499055, longi: 8.29775354755,
    name: "Heßheim",
    plz: "67258"
  },
  {
    lati: 51.0002209115, longi: 6.78416448159,
    name: "Pulheim",
    plz: "50259"
  },
  {
    lati: 48.8688464083, longi: 10.2589180458,
    name: "Lauchheim",
    plz: "73466"
  },
  {
    lati: 51.1709711185, longi: 10.2692438447,
    name: "Südeichsfeld",
    plz: "99988"
  },
  {
    lati: 52.7854033801, longi: 10.4942741293,
    name: "Sprakensehl",
    plz: "29365"
  },
  {
    lati: 50.8685300447, longi: 13.3869832563,
    name: "Weißenborn, Oberschöna",
    plz: "09600"
  },
  {
    lati: 47.6160419351, longi: 7.97213415852,
    name: "Rickenbach",
    plz: "79736"
  },
  {
    lati: 50.1248028245, longi: 8.71368401694,
    name: "Frankfurt am Main",
    plz: "60385"
  },
  {
    lati: 50.1206734125, longi: 10.8396955871,
    name: "Untermerzbach",
    plz: "96190"
  },
  {
    lati: 50.1760017064, longi: 6.61353687753,
    name: "Birresborn",
    plz: "54574"
  },
  {
    lati: 51.2441830835, longi: 6.80324516995,
    name: "Düsseldorf",
    plz: "40239"
  },
  {
    lati: 49.524225869, longi: 7.72113124439,
    name: "Mehlbach",
    plz: "67735"
  },
  {
    lati: 50.3429298197, longi: 7.72481627812,
    name: "Bad Ems",
    plz: "56130"
  },
  {
    lati: 47.5864379249, longi: 7.76917961999,
    name: "Rheinfelden (Baden)",
    plz: "79618"
  },
  {
    lati: 48.7904791259, longi: 9.00700300511,
    name: "Leonberg",
    plz: "71229"
  },
  {
    lati: 48.9690676629, longi: 8.75479744417,
    name: "Ölbronn-Dürrn",
    plz: "75248"
  },
  {
    lati: 53.0397196305, longi: 8.84994626007,
    name: "Bremen",
    plz: "28279"
  },
  {
    lati: 51.1444214957, longi: 14.0317879428,
    name: "Großröhrsdorf, Bretnig-Hauswalde",
    plz: "01900"
  },
  {
    lati: 51.1952051583, longi: 9.74594113828,
    name: "Hessisch Lichtenau",
    plz: "37235"
  },
  {
    lati: 50.268040208, longi: 9.41031610979,
    name: "Bad Soden-Salmünster",
    plz: "63628"
  },
  {
    lati: 48.5217800988, longi: 9.40576805904,
    name: "Hülben",
    plz: "72584"
  },
  {
    lati: 52.3290164994, longi: 12.6890791085,
    name: "Lehnin",
    plz: "14797"
  },
  {
    lati: 47.9024160667, longi: 7.58655518195,
    name: "Heitersheim",
    plz: "79423"
  },
  {
    lati: 50.0998306214, longi: 8.53474148685,
    name: "Frankfurt am Main",
    plz: "65929"
  },
  {
    lati: 49.6418507423, longi: 6.61247425588,
    name: "Wiltingen",
    plz: "54459"
  },
  {
    lati: 53.4719989517, longi: 10.3731098275,
    name: "Hohenhorn",
    plz: "21526"
  },
  {
    lati: 52.6559321728, longi: 11.5432940684,
    name: "Bismark",
    plz: "39629"
  },
  {
    lati: 50.376260247, longi: 11.7184626017,
    name: "Issigau",
    plz: "95188"
  },
  {
    lati: 51.3484786364, longi: 11.7919967336,
    name: "Mücheln",
    plz: "06255"
  },
  {
    lati: 54.1761233031, longi: 12.8551820718,
    name: "Franzburg, Richtenberg, u.a.",
    plz: "18461"
  },
  {
    lati: 50.3179202015, longi: 10.131007113,
    name: "Hohenroth",
    plz: "97618"
  },
  {
    lati: 48.3872214038, longi: 11.0667980813,
    name: "Dasing",
    plz: "86453"
  },
  {
    lati: 52.2041687427, longi: 14.4115024366,
    name: "Müllrose",
    plz: "15299"
  },
  {
    lati: 47.9246362776, longi: 10.7074792648,
    name: "Westendorf",
    plz: "87679"
  },
  {
    lati: 47.8786263105, longi: 10.9049543186,
    name: "Kinsau",
    plz: "86981"
  },
  {
    lati: 52.1814318561, longi: 10.9254819476,
    name: "Warberg",
    plz: "38378"
  },
  {
    lati: 53.8866775123, longi: 13.2679475416,
    name: "Bentzin",
    plz: "17129"
  },
  {
    lati: 50.5719902714, longi: 7.52834186613,
    name: "Willroth",
    plz: "56594"
  },
  {
    lati: 49.8906086661, longi: 7.17741714273,
    name: "Kleinich",
    plz: "54483"
  },
  {
    lati: 49.6881083609, longi: 8.42567555123,
    name: "Biblis",
    plz: "68647"
  },
  {
    lati: 48.1763293378, longi: 9.77736808396,
    name: "Schemmerhofen",
    plz: "88433"
  },
  {
    lati: 52.263181956, longi: 9.97100103012,
    name: "Algermissen",
    plz: "31191"
  },
  {
    lati: 53.3108790779, longi: 10.0087628336,
    name: "Marxen",
    plz: "21439"
  },
  {
    lati: 48.0885558453, longi: 11.4957036083,
    name: "München",
    plz: "81476"
  },
  {
    lati: 54.8455875656, longi: 9.02636112068,
    name: "Ladelund",
    plz: "25926"
  },
  {
    lati: 53.6444335884, longi: 9.2209568647,
    name: "Hechthausen",
    plz: "21755"
  },
  {
    lati: 51.7313919864, longi: 9.43059026399,
    name: "Boffzen, Derental",
    plz: "37691"
  },
  {
    lati: 50.0223902791, longi: 8.22227715313,
    name: "Mainz",
    plz: "55120"
  },
  {
    lati: 50.9952946848, longi: 6.42201309315,
    name: "Titz",
    plz: "52445"
  },
  {
    lati: 51.5989095586, longi: 7.324761617,
    name: "Castrop-Rauxel",
    plz: "44581"
  },
  {
    lati: 48.6177014212, longi: 9.06064087674,
    name: "Weil im Schönbuch",
    plz: "71093"
  },
  {
    lati: 48.8370127219, longi: 9.26687882187,
    name: "Fellbach",
    plz: "70736"
  },
  {
    lati: 48.6451593189, longi: 9.37716616592,
    name: "Oberboihingen",
    plz: "72644"
  },
  {
    lati: 50.4816424227, longi: 12.3890212785,
    name: "Ellefeld",
    plz: "08236"
  },
  {
    lati: 48.1360834789, longi: 11.5382272143,
    name: "München",
    plz: "80339"
  },
  {
    lati: 48.335477263, longi: 7.87227458777,
    name: "Lahr/Schwarzwald",
    plz: "77933"
  },
  {
    lati: 52.5582210013, longi: 9.70880882096,
    name: "Wedemark",
    plz: "30900"
  },
  {
    lati: 51.3784753239, longi: 6.43758382602,
    name: "Kempen",
    plz: "47906"
  },
  {
    lati: 52.3371778616, longi: 9.27973846445,
    name: "Heuerßen",
    plz: "31700"
  },
  {
    lati: 47.9084754661, longi: 9.42808119216,
    name: "Riedhausen",
    plz: "88377"
  },
  {
    lati: 49.2213693523, longi: 8.44474886276,
    name: "Philippsburg",
    plz: "76661"
  },
  {
    lati: 47.8568910867, longi: 7.69981489906,
    name: "Ballrechten-Dottingen",
    plz: "79282"
  },
  {
    lati: 50.0875358001, longi: 8.78486517985,
    name: "Offenbach am Main",
    plz: "63071"
  },
  {
    lati: 50.1873552234, longi: 8.92868243048,
    name: "Bruchköbel",
    plz: "63486"
  },
  {
    lati: 51.0287918994, longi: 9.66092332311,
    name: "Alheim",
    plz: "36211"
  },
  {
    lati: 48.2774508892, longi: 11.80793056,
    name: "Moosinning",
    plz: "85452"
  },
  {
    lati: 48.2103114062, longi: 11.3473614975,
    name: "Olching",
    plz: "82140"
  },
  {
    lati: 47.9425296302, longi: 12.6259117914,
    name: "Traunreut",
    plz: "83374"
  },
  {
    lati: 54.6782940007, longi: 9.59334649918,
    name: "Mittelangeln",
    plz: "24986"
  },
  {
    lati: 51.2024771791, longi: 6.39077658365,
    name: "Mönchengladbach",
    plz: "41068"
  },
  {
    lati: 54.6389471837, longi: 9.63883801739,
    name: "Struxdorf, Schnarup-Thumby",
    plz: "24891"
  },
  {
    lati: 53.5195775862, longi: 14.0091016487,
    name: "Pasewalk u.a.",
    plz: "17309"
  },
  {
    lati: 50.3337668932, longi: 10.7963759257,
    name: "Rodach b. Coburg",
    plz: "96476"
  },
  {
    lati: 49.8409928271, longi: 10.5054771813,
    name: "Ebrach",
    plz: "96157"
  },
  {
    lati: 49.0268201557, longi: 12.7317624326,
    name: "Haibach",
    plz: "94353"
  },
  {
    lati: 51.237392189, longi: 13.0530772459,
    name: "Mügeln",
    plz: "04769"
  },
  {
    lati: 49.2819056382, longi: 8.21178787934,
    name: "Venningen",
    plz: "67482"
  },
  {
    lati: 50.0825793218, longi: 8.81547150849,
    name: "Offenbach am Main",
    plz: "63073"
  },
  {
    lati: 48.9553562864, longi: 9.11839062781,
    name: "Bietigheim-Bissingen",
    plz: "74321"
  },
  {
    lati: 53.5298093096, longi: 9.70378780452,
    name: "Jork",
    plz: "21635"
  },
  {
    lati: 49.500362184, longi: 11.583405793,
    name: "Weigendorf",
    plz: "91249"
  },
  {
    lati: 49.9880931713, longi: 11.8355324083,
    name: "Mehlmeisel",
    plz: "95694"
  },
  {
    lati: 51.3447509878, longi: 7.68511510095,
    name: "Iserlohn",
    plz: "58644"
  },
  {
    lati: 47.7963046414, longi: 8.03706237176,
    name: "Bernau im Schwarzwald",
    plz: "79872"
  },
  {
    lati: 51.5237663323, longi: 8.73290313555,
    name: "Wünnenberg",
    plz: "33181"
  },
  {
    lati: 49.9032252151, longi: 6.62723535689,
    name: "Preist",
    plz: "54664"
  },
  {
    lati: 48.7156569239, longi: 11.5019762022,
    name: "Manching",
    plz: "85077"
  },
  {
    lati: 51.3358256097, longi: 9.48961355678,
    name: "Kassel",
    plz: "34127"
  },
  {
    lati: 48.3519527705, longi: 12.651900195,
    name: "Geratskirchen",
    plz: "84552"
  },
  {
    lati: 51.3820072496, longi: 12.9855014798,
    name: "Dahlen",
    plz: "04774"
  },
  {
    lati: 52.4106159623, longi: 13.3530647817,
    name: "Berlin",
    plz: "12279"
  },
  {
    lati: 47.6334596468, longi: 9.90415502188,
    name: "Heimenkirch",
    plz: "88178"
  },
  {
    lati: 53.9396022477, longi: 10.9910575714,
    name: "Dassow",
    plz: "23942"
  },
  {
    lati: 50.9344017046, longi: 10.9814836626,
    name: "Erfurt",
    plz: "99094"
  },
  {
    lati: 49.448236847, longi: 7.06016020643,
    name: "Marpingen",
    plz: "66646"
  },
  {
    lati: 53.6061989857, longi: 10.0837325117,
    name: "Hamburg",
    plz: "22179"
  },
  {
    lati: 48.170814581, longi: 10.1962012522,
    name: "Oberroth",
    plz: "89294"
  },
  {
    lati: 49.0565227963, longi: 8.64439432806,
    name: "Gondelsheim",
    plz: "75053"
  },
  {
    lati: 49.8750448194, longi: 8.81192847587,
    name: "Groß-Zimmern",
    plz: "64846"
  },
  {
    lati: 52.5695402848, longi: 13.4085486918,
    name: "Berlin Pankow",
    plz: "13187"
  },
  {
    lati: 49.4047128462, longi: 10.5198940277,
    name: "Flachslanden",
    plz: "91604"
  },
  {
    lati: 52.5779924723, longi: 10.7762850943,
    name: "Ehra-Lessien",
    plz: "38468"
  },
  {
    lati: 52.6131749663, longi: 6.84576818505,
    name: "Ringe, Laar, Emlichheim",
    plz: "49824"
  },
  {
    lati: 49.3864631346, longi: 7.05147124343,
    name: "Illingen",
    plz: "66557"
  },
  {
    lati: 51.4681278929, longi: 7.32005624687,
    name: "Bochum",
    plz: "44892"
  },
  {
    lati: 51.1819407448, longi: 7.75146612076,
    name: "Herscheid",
    plz: "58849"
  },
  {
    lati: 50.169818479, longi: 9.45367632333,
    name: "Jossgrund",
    plz: "63637"
  },
  {
    lati: 53.6888291359, longi: 9.99678768324,
    name: "Norderstedt",
    plz: "22850"
  },
  {
    lati: 52.616888943, longi: 10.08238445,
    name: "Celle",
    plz: "29221"
  },
  {
    lati: 52.4652368071, longi: 13.5280258688,
    name: "Berlin Oberschöneweide",
    plz: "12459"
  },
  {
    lati: 50.9052023448, longi: 6.67385629184,
    name: "Kerpen",
    plz: "50170"
  },
  {
    lati: 48.1351871695, longi: 12.5869744535,
    name: "Garching a.d. Alz",
    plz: "84518"
  },
  {
    lati: 48.0817240142, longi: 12.6545654203,
    name: "Kirchweidach",
    plz: "84558"
  },
  {
    lati: 52.5122759786, longi: 13.4394053367,
    name: "Berlin Friedrichshain",
    plz: "10243"
  },
  {
    lati: 48.6489869312, longi: 13.6289219136,
    name: "Hauzenberg",
    plz: "94051"
  },
  {
    lati: 52.7666606006, longi: 11.0873180255,
    name: "Kuhfelde",
    plz: "29416"
  },
  {
    lati: 49.4377839605, longi: 11.0816354955,
    name: "Nürnberg",
    plz: "90459"
  },
  {
    lati: 51.5174574609, longi: 7.51653279319,
    name: "Dortmund",
    plz: "44143"
  },
  {
    lati: 50.8118709167, longi: 8.43558846058,
    name: "Angelburg",
    plz: "35719"
  },
  {
    lati: 48.3896817928, longi: 11.2378633044,
    name: "Altomünster",
    plz: "85250"
  },
  {
    lati: 48.1076265813, longi: 11.6903497899,
    name: "München",
    plz: "81827"
  },
  {
    lati: 49.9054659319, longi: 6.88143335555,
    name: "Klausen",
    plz: "54524"
  },
  {
    lati: 51.4366337474, longi: 6.93449490281,
    name: "Mülheim an der Ruhr",
    plz: "45472"
  },
  {
    lati: 48.5927105417, longi: 9.27611403195,
    name: "Altdorf",
    plz: "72655"
  },
  {
    lati: 49.2922898182, longi: 9.19022238568,
    name: "Gundelsheim",
    plz: "74831"
  },
  {
    lati: 48.667479551, longi: 10.860738363,
    name: "Oberndorf a.Lech",
    plz: "86698"
  },
  {
    lati: 47.663707063, longi: 7.89511468614,
    name: "Hasel",
    plz: "79686"
  },
  {
    lati: 53.0417557263, longi: 8.67708889637,
    name: "Delmenhorst",
    plz: "27755"
  },
  {
    lati: 51.8962935575, longi: 8.39711908075,
    name: "Gütersloh",
    plz: "33332"
  },
  {
    lati: 50.4191740392, longi: 8.63150523274,
    name: "Butzbach",
    plz: "35510"
  },
  {
    lati: 54.270618486, longi: 9.657089672,
    name: "Westerrönfeld",
    plz: "24784"
  },
  {
    lati: 48.3653599069, longi: 11.466253141,
    name: "Vierkirchen",
    plz: "85256"
  },
  {
    lati: 53.1712834906, longi: 11.7081835988,
    name: "Karstädt, Dambeck, Klüß",
    plz: "19357"
  },
  {
    lati: 54.012347433, longi: 11.6735504846,
    name: "Neubukow, Ravensberg, Westenbrügge u.a.",
    plz: "18233"
  },
  {
    lati: 48.0663239651, longi: 11.6190868715,
    name: "Unterhaching",
    plz: "82008"
  },
  {
    lati: 50.3334238718, longi: 6.56434985455,
    name: "Schüller",
    plz: "54586"
  },
  {
    lati: 51.8253636615, longi: 6.95527076286,
    name: "Heiden",
    plz: "46359"
  },
  {
    lati: 48.0007806898, longi: 10.599753449,
    name: "Bad Wörishofen",
    plz: "86825"
  },
  {
    lati: 49.2108166495, longi: 10.6302311863,
    name: "Weidenbach",
    plz: "91746"
  },
  {
    lati: 53.1913583657, longi: 10.4419341119,
    name: "Deutsch Evern",
    plz: "21407"
  },
  {
    lati: 49.324984234, longi: 7.58856089873,
    name: "Obernheim-Kirchenarnbach u.a.",
    plz: "66919"
  },
  {
    lati: 48.1680374974, longi: 8.65280720046,
    name: "Rottweil",
    plz: "78628"
  },
  {
    lati: 52.4393598227, longi: 8.84136372519,
    name: "Warmsen",
    plz: "31606"
  },
  {
    lati: 50.7947856804, longi: 9.19034217698,
    name: "Antrifttal",
    plz: "36326"
  },
  {
    lati: 48.5416079006, longi: 10.0283237967,
    name: "Neenstetten",
    plz: "89189"
  },
  {
    lati: 54.112182367, longi: 10.650212665,
    name: "Eutin, Süsel",
    plz: "23701"
  },
  {
    lati: 53.5689578888, longi: 10.6312115351,
    name: "Woltersdorf, Müssen u.a.",
    plz: "21516"
  },
  {
    lati: 52.4054593501, longi: 13.4171841039,
    name: "Berlin",
    plz: "12305"
  },
  {
    lati: 50.9733077907, longi: 6.13106489457,
    name: "Geilenkirchen",
    plz: "52511"
  },
  {
    lati: 51.2341760719, longi: 7.66308718858,
    name: "Lüdenscheid",
    plz: "58513"
  },
  {
    lati: 48.9453811995, longi: 9.43870326846,
    name: "Backnang",
    plz: "71522"
  },
  {
    lati: 49.139493706, longi: 9.21610931639,
    name: "Heilbronn",
    plz: "74072"
  },
  {
    lati: 47.4668667719, longi: 10.401717593,
    name: "Bad Hindelang",
    plz: "87541"
  },
  {
    lati: 48.2469023028, longi: 10.442642814,
    name: "Ursberg",
    plz: "86513"
  },
  {
    lati: 50.3215237441, longi: 9.00283687506,
    name: "Glauburg",
    plz: "63695"
  },
  {
    lati: 48.5589197775, longi: 9.26434395461,
    name: "Riederich",
    plz: "72585"
  },
  {
    lati: 53.7286679715, longi: 9.7831465097,
    name: "Ellerhoop",
    plz: "25373"
  },
  {
    lati: 51.3814982389, longi: 7.01461606083,
    name: "Essen",
    plz: "45239"
  },
  {
    lati: 49.0184477363, longi: 10.3979412285,
    name: "Wilburgstetten",
    plz: "91634"
  },
  {
    lati: 51.7866336382, longi: 10.62767305,
    name: "Wernigerode",
    plz: "38879"
  },
  {
    lati: 50.5472034325, longi: 10.8884967293,
    name: "Schleusegrund",
    plz: "98667"
  },
  {
    lati: 48.3785630659, longi: 7.89513434834,
    name: "Friesenheim",
    plz: "77948"
  },
  {
    lati: 48.2951684742, longi: 8.57095705284,
    name: "Oberndorf am Neckar",
    plz: "78727"
  },
  {
    lati: 50.6680221519, longi: 8.89041141252,
    name: "Rabenau",
    plz: "35466"
  },
  {
    lati: 51.3011306585, longi: 6.58366618385,
    name: "Krefeld",
    plz: "47807"
  },
  {
    lati: 49.0409758008, longi: 10.608966717,
    name: "Wassertrüdingen",
    plz: "91717"
  },
  {
    lati: 47.7080261815, longi: 9.08856053534,
    name: "Reichenau",
    plz: "78479"
  },
  {
    lati: 51.5888995999, longi: 9.61223680292,
    name: "Oberweser",
    plz: "34399"
  },
  {
    lati: 50.9929406832, longi: 11.1160825121,
    name: "Erfurt",
    plz: "99098"
  },
  {
    lati: 50.4409039168, longi: 12.9481749972,
    name: "Oberwiesenthal",
    plz: "09484"
  },
  {
    lati: 52.9860218802, longi: 13.1385841311,
    name: "Gransee, Löwenberg",
    plz: "16775"
  },
  {
    lati: 54.0175152571, longi: 10.3334401459,
    name: "Nehms",
    plz: "23813"
  },
  {
    lati: 50.1382042824, longi: 10.5360853695,
    name: "Hofheim i. UFr.",
    plz: "97461"
  },
  {
    lati: 51.5957667873, longi: 10.8017006064,
    name: "Harztor",
    plz: "99768"
  },
  {
    lati: 48.1201026391, longi: 10.8265996446,
    name: "Hurlach",
    plz: "86857"
  },
  {
    lati: 49.1144253245, longi: 10.8604970379,
    name: "Pfofeld",
    plz: "91738"
  },
  {
    lati: 51.3484605075, longi: 11.6051917784,
    name: "Querfurt, Obhausen, Mücheln u.a.",
    plz: "06268"
  },
  {
    lati: 52.1635391588, longi: 11.6108603279,
    name: "Magdeburg",
    plz: "39128"
  },
  {
    lati: 51.1193407252, longi: 10.2233609549,
    name: "Treffurt",
    plz: "99830"
  },
  {
    lati: 51.8318181103, longi: 8.66852090825,
    name: "Hövelhof",
    plz: "33161"
  },
  {
    lati: 53.4086645325, longi: 8.44773860681,
    name: "Stadland",
    plz: "26935"
  },
  {
    lati: 49.5814004651, longi: 7.93795145496,
    name: "Börrstadt",
    plz: "67725"
  },
  {
    lati: 51.1119659079, longi: 7.4021116189,
    name: "Wipperfürth",
    plz: "51688"
  },
  {
    lati: 52.0539440089, longi: 7.83272562763,
    name: "Ostbevern",
    plz: "48346"
  },
  {
    lati: 48.4797507716, longi: 8.41462308427,
    name: "Freudenstadt",
    plz: "72250"
  },
  {
    lati: 52.7900221497, longi: 8.84927118811,
    name: "Affinghausen und Sudwalde",
    plz: "27257"
  },
  {
    lati: 49.470522614, longi: 9.00354340756,
    name: "Eberbach",
    plz: "69412"
  },
  {
    lati: 48.7051730246, longi: 8.99198961661,
    name: "Sindelfingen",
    plz: "71063"
  },
  {
    lati: 48.5295336716, longi: 9.2344925403,
    name: "Reutlingen",
    plz: "72766"
  },
  {
    lati: 48.5754587911, longi: 9.56492087378,
    name: "Neidlingen",
    plz: "73272"
  },
  {
    lati: 48.6890879997, longi: 9.558708885,
    name: "Albershausen",
    plz: "73095"
  },
  {
    lati: 53.3789589086, longi: 9.65487913787,
    name: "Regesbostel",
    plz: "21649"
  },
  {
    lati: 48.6399851577, longi: 9.63463308312,
    name: "Dürnau",
    plz: "73105"
  },
  {
    lati: 50.8052827469, longi: 7.23770461476,
    name: "Siegburg",
    plz: "53721"
  },
  {
    lati: 49.4016127861, longi: 11.115379189,
    name: "Nürnberg",
    plz: "90471"
  },
  {
    lati: 51.4383518022, longi: 11.2186018588,
    name: "Wallhausen, Blankenheim",
    plz: "06528"
  },
  {
    lati: 51.1678376868, longi: 14.5746258959,
    name: "Weißenberg, Hochkirch u.a.",
    plz: "02627"
  },
  {
    lati: 48.098752122, longi: 11.6328925726,
    name: "München",
    plz: "81737"
  },
  {
    lati: 48.0705027195, longi: 7.88434106672,
    name: "Denzlingen",
    plz: "79211"
  },
  {
    lati: 48.5574161665, longi: 8.15951295987,
    name: "Ottenhöfen im Schwarzwald",
    plz: "77883"
  },
  {
    lati: 50.4349535596, longi: 7.4706892601,
    name: "Neuwied",
    plz: "56564"
  },
  {
    lati: 51.3478360103, longi: 8.78198245783,
    name: "Diemelsee",
    plz: "34519"
  },
  {
    lati: 47.6223712944, longi: 9.71398059435,
    name: "Achberg",
    plz: "88147"
  },
  {
    lati: 53.3870091719, longi: 9.56331770245,
    name: "Sauensiek",
    plz: "21644"
  },
  {
    lati: 53.1511680885, longi: 12.7525447144,
    name: "Rheinsberg",
    plz: "16837"
  },
  {
    lati: 48.0348345905, longi: 10.8610577583,
    name: "Landsberg a. Lech",
    plz: "86899"
  },
  {
    lati: 50.81640059, longi: 11.0123070072,
    name: "Arnstadt",
    plz: "99310"
  },
  {
    lati: 48.183238757, longi: 10.529016082,
    name: "Eppishausen",
    plz: "87745"
  },
  {
    lati: 54.3439626824, longi: 13.6753540514,
    name: "Sellin",
    plz: "18586"
  },
  {
    lati: 50.9978001918, longi: 13.815323544,
    name: "Dresden",
    plz: "01257"
  },
  {
    lati: 51.0111805525, longi: 5.97171610403,
    name: "Gangelt, Selfkant",
    plz: "52538"
  },
  {
    lati: 50.3806935946, longi: 7.63357394774,
    name: "Urbar (bei Koblenz)",
    plz: "56182"
  },
  {
    lati: 48.4159706209, longi: 7.90830664982,
    name: "Hohberg",
    plz: "77749"
  },
  {
    lati: 48.6493690164, longi: 8.33052993261,
    name: "Forbach",
    plz: "76596"
  },
  {
    lati: 51.9786248249, longi: 8.45409175926,
    name: "Bielefeld",
    plz: "33649"
  },
  {
    lati: 47.6380199839, longi: 9.53739074913,
    name: "Eriskirch",
    plz: "88097"
  },
  {
    lati: 53.6520143568, longi: 10.5884028387,
    name: "Nusse",
    plz: "23896"
  },
  {
    lati: 48.7452654149, longi: 9.0900282021,
    name: "Stuttgart",
    plz: "70569"
  },
  {
    lati: 51.9711848199, longi: 9.27932316904,
    name: "Bad Pyrmont",
    plz: "31812"
  },
  {
    lati: 50.2557917492, longi: 11.9307583532,
    name: "Oberkotzau",
    plz: "95145"
  },
  {
    lati: 51.4626054996, longi: 13.6382166985,
    name: "Plessa, Schraden",
    plz: "04928"
  },
  {
    lati: 51.7153202963, longi: 14.3805861001,
    name: "Cottbus",
    plz: "03051"
  },
  {
    lati: 53.6134511434, longi: 9.89871400774,
    name: "Hamburg",
    plz: "22523"
  },
  {
    lati: 51.2593811094, longi: 12.0208758974,
    name: "Weißenfels",
    plz: "06688"
  },
  {
    lati: 48.2802155954, longi: 10.2483067209,
    name: "Roggenburg",
    plz: "89297"
  },
  {
    lati: 50.811230626, longi: 10.6511891898,
    name: "Georgenthal/ Thür. Wald",
    plz: "99887"
  },
  {
    lati: 49.4183135635, longi: 11.1255854428,
    name: "Nürnberg",
    plz: "90471"
  },
  {
    lati: 50.0471088199, longi: 7.98682859953,
    name: "Oestrich-Winkel",
    plz: "65375"
  },
  {
    lati: 50.0809251522, longi: 8.67935093555,
    name: "Frankfurt am Main",
    plz: "60598"
  },
  {
    lati: 53.94890802, longi: 9.1459243253,
    name: "Eddelak, Averlak, Dingen, Ramhusen",
    plz: "25715"
  },
  {
    lati: 48.3557511892, longi: 12.8119232804,
    name: "Wurmannsquick",
    plz: "84329"
  },
  {
    lati: 54.3096087653, longi: 9.32709284994,
    name: "Erfde",
    plz: "24803"
  },
  {
    lati: 47.7940108255, longi: 12.5727513653,
    name: "Bergen",
    plz: "83346"
  },
  {
    lati: 51.6572276027, longi: 12.6019204171,
    name: "Gräfenhainichen",
    plz: "06772"
  },
  {
    lati: 48.0311843447, longi: 11.9505400017,
    name: "Grafing b. München",
    plz: "85567"
  },
  {
    lati: 49.2749289178, longi: 8.02649378107,
    name: "Landau in der Pfalz",
    plz: "76829"
  },
  {
    lati: 47.6059847808, longi: 7.61096758151,
    name: "Weil am Rhein",
    plz: "79576"
  },
  {
    lati: 49.7831807196, longi: 9.96282050888,
    name: "Würzburg",
    plz: "97074"
  },
  {
    lati: 52.412341265, longi: 9.66657756054,
    name: "Hannover",
    plz: "30419"
  },
  {
    lati: 51.4845647093, longi: 9.74080450343,
    name: "Dransfeld u.a.",
    plz: "37127"
  },
  {
    lati: 48.1606553774, longi: 11.6641592915,
    name: "München",
    plz: "81929"
  },
  {
    lati: 51.5702015308, longi: 7.1920053073,
    name: "Recklinghausen",
    plz: "45661"
  },
  {
    lati: 50.4990462794, longi: 7.5986119858,
    name: "Kleinmaischeid",
    plz: "56271"
  },
  {
    lati: 47.8949405537, longi: 7.63616283917,
    name: "Eschbach",
    plz: "79427"
  },
  {
    lati: 52.3907327417, longi: 9.12840964865,
    name: "Wiedensahl",
    plz: "31719"
  },
  {
    lati: 49.4485342387, longi: 8.59931306542,
    name: "Edingen-Neckarhausen",
    plz: "68535"
  },
  {
    lati: 48.3045313952, longi: 10.9809484646,
    name: "Kissing",
    plz: "86438"
  },
  {
    lati: 48.6855914484, longi: 10.1319051591,
    name: "Heidenheim an der Brenz",
    plz: "89518"
  },
  {
    lati: 49.1688093121, longi: 10.4745232809,
    name: "Wieseth",
    plz: "91632"
  },
  {
    lati: 47.9875901235, longi: 12.5577945427,
    name: "Stein a.d. Traun",
    plz: "83371"
  },
  {
    lati: 48.2018664914, longi: 12.4206976104,
    name: "Waldkraiburg",
    plz: "84478"
  },
  {
    lati: 54.2857049804, longi: 11.0264535443,
    name: "Heringsdorf",
    plz: "23777"
  },
  {
    lati: 52.0155942176, longi: 11.5426385566,
    name: "Sülzetal",
    plz: "39171"
  },
  {
    lati: 52.9138754401, longi: 11.7521359793,
    name: "Seehausen, Werben, Leppin, Wahrenberg etc",
    plz: "39615"
  },
  {
    lati: 47.8213438939, longi: 10.6416976062,
    name: "Biessenhofen",
    plz: "87640"
  },
  {
    lati: 50.2999805548, longi: 9.65196955371,
    name: "Sinntal",
    plz: "36391"
  },
  {
    lati: 51.7229873172, longi: 8.22611779552,
    name: "Wadersloh",
    plz: "59329"
  },
  {
    lati: 49.585125361, longi: 7.6137000277,
    name: "Wolfstein",
    plz: "67752"
  },
  {
    lati: 53.5892844186, longi: 8.56383274399,
    name: "Bremerhaven",
    plz: "27580"
  },
  {
    lati: 52.3936465413, longi: 13.1042858486,
    name: "Potsdam",
    plz: "14482"
  },
  {
    lati: 48.7222021105, longi: 13.1608786733,
    name: "Iggensbach",
    plz: "94547"
  },
  {
    lati: 49.4222493289, longi: 10.9682867759,
    name: "Oberasbach",
    plz: "90522"
  },
  {
    lati: 54.3138792843, longi: 10.1158631345,
    name: "Kiel",
    plz: "24114"
  },
  {
    lati: 51.8906244928, longi: 9.5799660263,
    name: "Negenborn",
    plz: "37643"
  },
  {
    lati: 54.175819495, longi: 9.65945256175,
    name: "Hamweddel",
    plz: "24816"
  },
  {
    lati: 49.5733064513, longi: 10.5753204389,
    name: "Neustadt a.d. Aisch",
    plz: "91413"
  },
  {
    lati: 53.352721489, longi: 10.5825180772,
    name: "Hohnstorf (Elbe), Hittbergen",
    plz: "21522"
  },
  {
    lati: 51.7073273659, longi: 13.0158783918,
    name: "Annaburg",
    plz: "06925"
  },
  {
    lati: 52.1002177461, longi: 13.204533804,
    name: "Nuthe-Urstromtal",
    plz: "14947"
  },
  {
    lati: 49.6423248761, longi: 8.47118686971,
    name: "Bürstadt",
    plz: "68642"
  },
  {
    lati: 48.5265866978, longi: 9.3539117279,
    name: "Dettingen an der Erms",
    plz: "72581"
  },
  {
    lati: 49.5952086302, longi: 9.47967045974,
    name: "Hardheim",
    plz: "74736"
  },
  {
    lati: 53.7900429592, longi: 9.43974460689,
    name: "Glückstadt",
    plz: "25348"
  },
  {
    lati: 48.1191178355, longi: 9.5342843624,
    name: "Dürmentingen",
    plz: "88525"
  },
  {
    lati: 47.9679026831, longi: 8.02769260719,
    name: "Buchenbach",
    plz: "79256"
  },
  {
    lati: 50.423649156, longi: 7.1015208146,
    name: "Kempenich",
    plz: "56746"
  },
  {
    lati: 50.2446845379, longi: 7.16500500896,
    name: "Düngenheim",
    plz: "56761"
  },
  {
    lati: 48.9127017996, longi: 13.2595569658,
    name: "Kirchdorf i. Wald",
    plz: "94261"
  },
  {
    lati: 48.1107503275, longi: 11.5310483319,
    name: "München",
    plz: "81369"
  },
  {
    lati: 53.4919400384, longi: 9.09423384396,
    name: "Bremervörde",
    plz: "27432"
  },
  {
    lati: 47.973177444, longi: 10.9631877486,
    name: "Thaining",
    plz: "86943"
  },
  {
    lati: 50.7014123353, longi: 10.5944998503,
    name: "Steinbach-Hallenberg",
    plz: "98587"
  },
  {
    lati: 48.6985124941, longi: 9.63417353738,
    name: "Göppingen",
    plz: "73035"
  },
  {
    lati: 49.1902944142, longi: 8.19548999425,
    name: "Offenbach an der Queich",
    plz: "76877"
  },
  {
    lati: 52.5496979633, longi: 13.5059876578,
    name: "Berlin Alt-Hohenschönhausen",
    plz: "13053"
  },
  {
    lati: 48.2303919335, longi: 8.94951694397,
    name: "Albstadt",
    plz: "72459"
  },
  {
    lati: 53.7508300376, longi: 9.15357269865,
    name: "Oberndorf",
    plz: "21787"
  },
  {
    lati: 50.7879024809, longi: 6.15966283773,
    name: "Aachen",
    plz: "52080"
  },
  {
    lati: 54.4017092256, longi: 9.37719746848,
    name: "Börm",
    plz: "24863"
  },
  {
    lati: 48.5586628664, longi: 9.98071039285,
    name: "Weidenstetten",
    plz: "89197"
  },
  {
    lati: 54.4664028629, longi: 9.59599075141,
    name: "Selk, Geltdorf, Hahnekrug",
    plz: "24884"
  },
  {
    lati: 50.895005166, longi: 10.3787034092,
    name: "Ruhla",
    plz: "99842"
  },
  {
    lati: 49.3991614182, longi: 10.9801219476,
    name: "Stein",
    plz: "90547"
  },
  {
    lati: 49.4039633008, longi: 11.0467542769,
    name: "Nürnberg",
    plz: "90451"
  },
  {
    lati: 47.6455101939, longi: 7.61212829627,
    name: "Rümmingen",
    plz: "79595"
  },
  {
    lati: 52.1322549662, longi: 7.9329650729,
    name: "Lienen",
    plz: "49536"
  },
  {
    lati: 54.0551605742, longi: 9.47108446521,
    name: "Schenefeld",
    plz: "25560"
  },
  {
    lati: 53.9225838812, longi: 10.4631149504,
    name: "Geschendorf",
    plz: "23815"
  },
  {
    lati: 48.5230908346, longi: 11.1027839054,
    name: "Inchenhofen",
    plz: "86570"
  },
  {
    lati: 52.7669065981, longi: 13.2965293408,
    name: "Oranienburg, Mühlenbecker Land",
    plz: "16515"
  },
  {
    lati: 50.9227538562, longi: 13.2504773269,
    name: "Weißenborn, Oberschöna",
    plz: "09600"
  },
  {
    lati: 49.7251960501, longi: 7.82018383122,
    name: "Alsenz",
    plz: "67821"
  },
  {
    lati: 50.0506027246, longi: 8.10418000096,
    name: "Eltville am Rhein",
    plz: "65343"
  },
  {
    lati: 51.4461740853, longi: 7.14959059679,
    name: "Bochum",
    plz: "44867"
  },
  {
    lati: 49.4903387415, longi: 7.73591668852,
    name: "Otterbach, Otterberg",
    plz: "67731"
  },
  {
    lati: 53.0742233049, longi: 8.82501163379,
    name: "Bremen",
    plz: "28203"
  },
  {
    lati: 48.0698911672, longi: 8.32329704104,
    name: "Villingen-Schwenningen",
    plz: "78052"
  },
  {
    lati: 48.6599200272, longi: 12.0973604061,
    name: "Hohenthann",
    plz: "84098"
  },
  {
    lati: 48.0977708708, longi: 11.601180117,
    name: "München",
    plz: "81549"
  },
  {
    lati: 50.0158245475, longi: 6.2597925833,
    name: "Neuerburg u.a.",
    plz: "54673"
  },
  {
    lati: 50.7544225095, longi: 7.62471491222,
    name: "Pracht",
    plz: "57589"
  },
  {
    lati: 50.0110708213, longi: 8.25667996937,
    name: "Mainz",
    plz: "55118"
  },
  {
    lati: 52.2689572002, longi: 8.66635704331,
    name: "Hüllhorst",
    plz: "32609"
  },
  {
    lati: 50.201304596, longi: 12.0836462559,
    name: "Schönwald",
    plz: "95173"
  },
  {
    lati: 52.1461568065, longi: 10.4992432263,
    name: "Wolfenbüttel",
    plz: "38304"
  },
  {
    lati: 48.7129851932, longi: 8.90403607014,
    name: "Grafenau",
    plz: "71120"
  },
  {
    lati: 50.9655552486, longi: 11.1072066534,
    name: "Erfurt",
    plz: "99099"
  },
  {
    lati: 52.5769234171, longi: 13.1855520074,
    name: "Berlin Hakenfelde",
    plz: "13587"
  },
  {
    lati: 51.0996883999, longi: 14.5245984125,
    name: "Cunewalde",
    plz: "02733"
  },
  {
    lati: 47.656495242, longi: 9.6243282433,
    name: "Tettnang",
    plz: "88069"
  },
  {
    lati: 53.6769184621, longi: 9.90627334306,
    name: "Ellerbek",
    plz: "25474"
  },
  {
    lati: 51.8720987063, longi: 9.42153908826,
    name: "Heinsen",
    plz: "37649"
  },
  {
    lati: 51.726344801, longi: 9.403035446,
    name: "Fürstenberg",
    plz: "37699"
  },
  {
    lati: 50.1179053585, longi: 8.97917697082,
    name: "Hanau",
    plz: "63457"
  },
  {
    lati: 52.0336133514, longi: 8.90187104491,
    name: "Lemgo",
    plz: "32657"
  },
  {
    lati: 48.9773990332, longi: 8.90839274241,
    name: "Illingen",
    plz: "75428"
  },
  {
    lati: 49.4422358558, longi: 10.3661229033,
    name: "Marktbergel",
    plz: "91613"
  },
  {
    lati: 51.523768879, longi: 10.440318445,
    name: "Am Ohmberg, Sonnenstein",
    plz: "37345"
  },
  {
    lati: 51.5840236368, longi: 10.5851267889,
    name: "Wieda",
    plz: "37447"
  },
  {
    lati: 52.5116011158, longi: 13.1962646496,
    name: "Berlin Wlhelmstadt",
    plz: "13595"
  },
  {
    lati: 53.545211311, longi: 13.312447794,
    name: "Neubrandenburg",
    plz: "17036"
  },
  {
    lati: 50.2250082845, longi: 8.60915782715,
    name: "Bad Homburg v.d. Höhe",
    plz: "61348"
  },
  {
    lati: 49.6972411502, longi: 8.76752689572,
    name: "Lindenfels",
    plz: "64678"
  },
  {
    lati: 49.3380518323, longi: 8.79528557516,
    name: "Mauer",
    plz: "69256"
  },
  {
    lati: 50.2004096672, longi: 11.4545411043,
    name: "Rugendorf",
    plz: "95365"
  },
  {
    lati: 51.8163607146, longi: 11.2868335055,
    name: "Seeland",
    plz: "06466"
  },
  {
    lati: 52.662267084, longi: 9.13703116164,
    name: "Marklohe",
    plz: "31608"
  },
  {
    lati: 51.9725052993, longi: 11.4011092992,
    name: "Börde-Hakel",
    plz: "39448"
  },
  {
    lati: 51.4738962873, longi: 7.02361045531,
    name: "Essen",
    plz: "45141"
  },
  {
    lati: 53.7867710974, longi: 9.23109931722,
    name: "Oederquart",
    plz: "21734"
  },
  {
    lati: 48.7124194598, longi: 9.51995843503,
    name: "Ebersbach an der Fils",
    plz: "73061"
  },
  {
    lati: 48.6423880898, longi: 12.6024889929,
    name: "Mamming",
    plz: "94437"
  },
  {
    lati: 49.681306836, longi: 12.3283656214,
    name: "Waldthurn",
    plz: "92727"
  },
  {
    lati: 49.6353974368, longi: 11.5550491689,
    name: "Neuhaus a.d.Pegnitz",
    plz: "91284"
  },
  {
    lati: 48.7964401078, longi: 11.7552523112,
    name: "Neustadt a.d. Donau",
    plz: "93333"
  },
  {
    lati: 48.5307313004, longi: 13.3212464794,
    name: "Fürstenzell",
    plz: "94081"
  },
  {
    lati: 54.3230530406, longi: 13.5341535961,
    name: "Putbus",
    plz: "18581"
  },
  {
    lati: 50.2510749929, longi: 7.45395495997,
    name: "Lehmen, Niederfell, Oberfell, Wolken u.a.",
    plz: "56332"
  },
  {
    lati: 54.0498433616, longi: 9.34318732184,
    name: "Holstenniendorf",
    plz: "25584"
  },
  {
    lati: 48.2926740461, longi: 8.47497559147,
    name: "Fluorn-Winzeln",
    plz: "78737"
  },
  {
    lati: 52.0419094109, longi: 8.47026683515,
    name: "Bielefeld",
    plz: "33619"
  },
  {
    lati: 50.1382884412, longi: 8.67738885929,
    name: "Frankfurt am Main",
    plz: "60320"
  },
  {
    lati: 50.3754574427, longi: 8.74902876131,
    name: "Bad Nauheim",
    plz: "61231"
  },
  {
    lati: 50.1086998176, longi: 8.84670059586,
    name: "Mühlheim am Main",
    plz: "63165"
  },
  {
    lati: 51.4781085518, longi: 12.0362450792,
    name: "Halle/ Saale",
    plz: "06116"
  },
  {
    lati: 48.8373653897, longi: 12.9753301993,
    name: "Deggendorf",
    plz: "94469"
  },
  {
    lati: 50.7388723456, longi: 6.28445745582,
    name: "Stolberg",
    plz: "52224"
  },
  {
    lati: 49.2488512943, longi: 7.02252723427,
    name: "Saarbrücken",
    plz: "66123"
  },
  {
    lati: 49.1983782329, longi: 7.06243753681,
    name: "Saarbrücken",
    plz: "66130"
  },
  {
    lati: 48.5869653814, longi: 8.0237947508,
    name: "Renchen",
    plz: "77871"
  },
  {
    lati: 52.9958091516, longi: 9.85670057764,
    name: "Soltau",
    plz: "29614"
  },
  {
    lati: 49.6162662765, longi: 10.7102307028,
    name: "Gerhardshofen",
    plz: "91466"
  },
  {
    lati: 48.0002804166, longi: 10.0868258902,
    name: "Tannheim",
    plz: "88459"
  },
  {
    lati: 48.2584525241, longi: 10.0965193657,
    name: "Bellenberg",
    plz: "89287"
  },
  {
    lati: 48.4672106405, longi: 10.4272365441,
    name: "Dürrlauingen",
    plz: "89350"
  },
  {
    lati: 52.146363743, longi: 13.3737762875,
    name: "Am Mellensee",
    plz: "15838"
  },
  {
    lati: 53.0518280133, longi: 14.212485682,
    name: "Biesendahlshof, Berkholz-Meyenburg",
    plz: "16306"
  },
  {
    lati: 48.9544726785, longi: 12.152131076,
    name: "Obertraubling",
    plz: "93083"
  },
  {
    lati: 50.7276617953, longi: 12.3527578652,
    name: "Werdau",
    plz: "08412"
  },
  {
    lati: 51.8276238916, longi: 13.694880787,
    name: "Luckau, Waldrehna, Heideblick, Fürstlich Drehna",
    plz: "15926"
  },
  {
    lati: 48.3327263231, longi: 11.2832771771,
    name: "Erdweg",
    plz: "85253"
  },
  {
    lati: 53.91321344, longi: 12.847444885,
    name: "Dargun",
    plz: "17159"
  },
  {
    lati: 52.6373358265, longi: 7.98167144363,
    name: "Badbergen",
    plz: "49635"
  },
  {
    lati: 53.1228373388, longi: 7.54759599284,
    name: "Rhauderfehn",
    plz: "26817"
  },
  {
    lati: 47.6325318792, longi: 7.59415952102,
    name: "Eimeldingen",
    plz: "79591"
  },
  {
    lati: 51.213294433, longi: 6.80343977315,
    name: "Düsseldorf",
    plz: "40227"
  },
  {
    lati: 49.7824487176, longi: 7.34316597677,
    name: "Herrstein",
    plz: "55756"
  },
  {
    lati: 51.6586566148, longi: 10.1805562892,
    name: "Wulften",
    plz: "37199"
  },
  {
    lati: 50.8312492719, longi: 10.2504906621,
    name: "Bad Salzungen",
    plz: "36433"
  },
  {
    lati: 49.0887423662, longi: 12.1224147388,
    name: "Zeitlarn",
    plz: "93197"
  },
  {
    lati: 47.9881162273, longi: 7.82159230038,
    name: "Freiburg im Breisgau",
    plz: "79115"
  },
  {
    lati: 50.1506683567, longi: 8.56700024958,
    name: "Eschborn",
    plz: "65760"
  },
  {
    lati: 50.2084968029, longi: 6.99520179869,
    name: "Ulmen",
    plz: "56766"
  },
  {
    lati: 51.2246414773, longi: 7.07920066545,
    name: "Wuppertal",
    plz: "42329"
  },
  {
    lati: 48.6343229451, longi: 11.3318574107,
    name: "Brunnen",
    plz: "86564"
  },
  {
    lati: 47.7610471544, longi: 11.3812610892,
    name: "Penzberg",
    plz: "82377"
  },
  {
    lati: 48.8300258949, longi: 11.8582869074,
    name: "Abensberg",
    plz: "93326"
  },
  {
    lati: 49.4036107179, longi: 9.288285812,
    name: "Schefflenz",
    plz: "74850"
  },
  {
    lati: 50.6282113221, longi: 9.04781316784,
    name: "Mücke",
    plz: "35325"
  },
  {
    lati: 47.9869271987, longi: 12.2538956245,
    name: "Schonstett",
    plz: "83137"
  },
  {
    lati: 53.8019745449, longi: 9.77787648706,
    name: "Barmstedt",
    plz: "25355"
  },
  {
    lati: 50.8491646917, longi: 9.9760448697,
    name: "Philippsthal",
    plz: "36269"
  },
  {
    lati: 48.3595226396, longi: 10.8310424881,
    name: "Stadtbergen",
    plz: "86391"
  },
  {
    lati: 48.7474537949, longi: 10.4731640932,
    name: "Amerdingen",
    plz: "86735"
  },
  {
    lati: 51.5451773518, longi: 9.85784144631,
    name: "Göttingen",
    plz: "37079"
  },
  {
    lati: 53.5748591795, longi: 9.97732331216,
    name: "Hamburg",
    plz: "20144"
  },
  {
    lati: 52.6079191482, longi: 10.0237039501,
    name: "Celle",
    plz: "29225"
  },
  {
    lati: 49.1790349663, longi: 12.3471063312,
    name: "Reichenbach",
    plz: "93189"
  },
  {
    lati: 50.8139700993, longi: 12.5558574263,
    name: "Glauchau",
    plz: "08371"
  },
  {
    lati: 48.4816629948, longi: 10.674621051,
    name: "Emersacker",
    plz: "86494"
  },
  {
    lati: 53.6089230854, longi: 10.3689589057,
    name: "Trittau",
    plz: "22946"
  },
  {
    lati: 53.8988027379, longi: 10.3900274883,
    name: "Neuengörs",
    plz: "23818"
  },
  {
    lati: 49.6347648909, longi: 8.21074401701,
    name: "Monsheim",
    plz: "67590"
  },
  {
    lati: 51.2475499669, longi: 9.03452990189,
    name: "Waldeck",
    plz: "34513"
  },
  {
    lati: 52.1625650205, longi: 11.0204699667,
    name: "Büddenstedt",
    plz: "38372"
  },
  {
    lati: 53.7651385994, longi: 10.2729761102,
    name: "Elmenhorst",
    plz: "23869"
  },
  {
    lati: 48.2480886671, longi: 11.6395294995,
    name: "Garching b. München",
    plz: "85748"
  },
  {
    lati: 48.6725669561, longi: 9.74772342097,
    name: "Süßen",
    plz: "73079"
  },
  {
    lati: 49.2726546715, longi: 12.1744396993,
    name: "Steinberg",
    plz: "92449"
  },
  {
    lati: 50.7318433059, longi: 6.83852689215,
    name: "Weilerswist",
    plz: "53919"
  },
  {
    lati: 52.8543366293, longi: 8.1663923523,
    name: "Emstek",
    plz: "49685"
  },
  {
    lati: 53.7201842174, longi: 8.58204685542,
    name: "Wurster Nordseeküste",
    plz: "27639"
  },
  {
    lati: 49.8704960767, longi: 8.65236481531,
    name: "Darmstadt",
    plz: "64283"
  },
  {
    lati: 48.6815973282, longi: 13.3097630728,
    name: "Aicha vorm Wald",
    plz: "94529"
  },
  {
    lati: 51.4831708255, longi: 13.8544428837,
    name: "Schwarzheide N.L.",
    plz: "01987"
  },
  {
    lati: 53.7901917101, longi: 9.91517008806,
    name: "Alveslohe",
    plz: "25486"
  },
  {
    lati: 51.3131915552, longi: 9.27159419495,
    name: "Zierenberg",
    plz: "34289"
  },
  {
    lati: 51.804502264, longi: 14.3858075245,
    name: "Cottbus",
    plz: "03053"
  },
  {
    lati: 50.2746062198, longi: 10.6974490836,
    name: "Bad Colberg-Heldburg",
    plz: "98663"
  },
  {
    lati: 47.4981022473, longi: 11.0430948496,
    name: "Garmisch-Partenkirchen",
    plz: "82467"
  },
  {
    lati: 48.2131130956, longi: 11.0211026582,
    name: "Steindorf",
    plz: "82297"
  },
  {
    lati: 49.5468793061, longi: 6.5417138268,
    name: "Freudenburg",
    plz: "54450"
  },
  {
    lati: 48.9889655469, longi: 12.6232331173,
    name: "Ascha",
    plz: "94347"
  },
  {
    lati: 49.8603118948, longi: 9.52543020744,
    name: "Esselbach",
    plz: "97839"
  },
  {
    lati: 52.6421457796, longi: 13.5319112629,
    name: "Panketal",
    plz: "16341"
  },
  {
    lati: 53.305664347, longi: 13.8697233024,
    name: "Prenzlau u.a.",
    plz: "17291"
  },
  {
    lati: 53.8625461936, longi: 9.71373822854,
    name: "Brande-Hörnerkirchen",
    plz: "25364"
  },
  {
    lati: 53.583010044, longi: 10.1287047777,
    name: "Hamburg",
    plz: "22045"
  },
  {
    lati: 49.5470181095, longi: 7.71095964185,
    name: "Schallodenbach",
    plz: "67701"
  },
  {
    lati: 48.5782992399, longi: 10.3263100955,
    name: "Medlingen",
    plz: "89441"
  },
  {
    lati: 51.2128846941, longi: 10.5528565245,
    name: "Körner, Weinbergen",
    plz: "99998"
  },
  {
    lati: 52.4873079004, longi: 8.39544272647,
    name: "Lemförde u.a.",
    plz: "49448"
  },
  {
    lati: 51.9224882683, longi: 8.45826126785,
    name: "Gütersloh",
    plz: "33335"
  },
  {
    lati: 50.0798942617, longi: 8.46665183835,
    name: "Kriftel",
    plz: "65830"
  },
  {
    lati: 49.9754704708, longi: 8.33488122239,
    name: "Ginsheim-Gustavsburg",
    plz: "65462"
  },
  {
    lati: 49.6751953686, longi: 9.36000029613,
    name: "Eichenbühl",
    plz: "63928"
  },
  {
    lati: 50.2074565595, longi: 11.6128017455,
    name: "Grafengehaig",
    plz: "95356"
  },
  {
    lati: 50.6253381323, longi: 7.11757206802,
    name: "Wachtberg",
    plz: "53343"
  },
  {
    lati: 50.5394819061, longi: 10.7561217977,
    name: "Schleusingen u.a.",
    plz: "98553"
  },
  {
    lati: 50.6034839705, longi: 7.71227864206,
    name: "Kleinmaischeid",
    plz: "56271"
  },
  {
    lati: 49.1468889307, longi: 7.75985276917,
    name: "Dahn",
    plz: "66994"
  },
  {
    lati: 48.1024764484, longi: 10.1663113704,
    name: "Pleß",
    plz: "87773"
  },
  {
    lati: 48.0584974603, longi: 12.1490322829,
    name: "Edling",
    plz: "83533"
  },
  {
    lati: 50.3016794585, longi: 12.0491169249,
    name: "Regnitzlosau",
    plz: "95194"
  },
  {
    lati: 53.8385622377, longi: 11.4945941865,
    name: "Dorf Mecklenburg, Lübow u.a.",
    plz: "23972"
  },
  {
    lati: 48.1509918042, longi: 11.5436378514,
    name: "München",
    plz: "80636"
  },
  {
    lati: 51.7210069886, longi: 11.6671275425,
    name: "Alsleben/Saale, Plötzkau",
    plz: "06425"
  },
  {
    lati: 52.9939333504, longi: 13.3413160216,
    name: "Zehdenick",
    plz: "16792"
  },
  {
    lati: 50.8792823921, longi: 14.0815180249,
    name: "Königstein/Sächs.Schw.",
    plz: "01824"
  },
  {
    lati: 50.9268852694, longi: 6.10426693391,
    name: "Übach-Palenberg",
    plz: "52531"
  },
  {
    lati: 50.9548595992, longi: 7.01156953483,
    name: "Köln",
    plz: "51065"
  },
  {
    lati: 54.6066773802, longi: 9.35506098907,
    name: "Eggebek, Langstedt, Sollerup, Süderhackstedt",
    plz: "24852"
  },
  {
    lati: 48.2522951847, longi: 9.26125303686,
    name: "Gammertingen",
    plz: "72501"
  },
  {
    lati: 47.9968080278, longi: 8.58672791104,
    name: "Bad Dürrheim",
    plz: "78073"
  },
  {
    lati: 47.6212811041, longi: 8.57964090421,
    name: "Lottstetten",
    plz: "79807"
  },
  {
    lati: 51.9578118357, longi: 8.87513407017,
    name: "Detmold",
    plz: "32758"
  },
  {
    lati: 49.5901938627, longi: 9.88221018067,
    name: "Bütthard",
    plz: "97244"
  },
  {
    lati: 47.8539488035, longi: 9.74722720484,
    name: "Bergatreute",
    plz: "88368"
  },
  {
    lati: 51.2135792651, longi: 6.76244239778,
    name: "Düsseldorf",
    plz: "40219"
  },
  {
    lati: 53.000990581, longi: 7.41976621961,
    name: "Neubörger, Neulehe",
    plz: "26909"
  },
  {
    lati: 53.4405263169, longi: 8.82705109595,
    name: "Beverstedt",
    plz: "27616"
  },
  {
    lati: 52.280012546, longi: 10.5365384252,
    name: "Braunschweig",
    plz: "38106"
  },
  {
    lati: 50.3155142725, longi: 11.8692787972,
    name: "Hof",
    plz: "95030"
  },
  {
    lati: 48.2723816451, longi: 12.4736749017,
    name: "Mettenheim",
    plz: "84562"
  },
  {
    lati: 49.2977157799, longi: 8.03334548427,
    name: "Gommersheim",
    plz: "67377"
  },
  {
    lati: 49.1819241018, longi: 7.05060130784,
    name: "Saarbrücken",
    plz: "66129"
  },
  {
    lati: 50.9713633528, longi: 7.19336880288,
    name: "Bergisch Gladbach",
    plz: "51429"
  },
  {
    lati: 48.1274176053, longi: 9.9879233083,
    name: "Gutenzell-Hürbel",
    plz: "88484"
  },
  {
    lati: 49.0917449181, longi: 11.6323310829,
    name: "Breitenbrunn",
    plz: "92363"
  },
  {
    lati: 54.2234196499, longi: 10.8968014874,
    name: "Lensahn",
    plz: "23738"
  },
  {
    lati: 52.0914444633, longi: 9.098372721,
    name: "Extertal",
    plz: "32699"
  },
  {
    lati: 47.5933185428, longi: 11.3216321362,
    name: "Walchensee",
    plz: "82432"
  },
  {
    lati: 50.2856625132, longi: 11.9645733449,
    name: "Döhlau",
    plz: "95182"
  },
  {
    lati: 47.6259802507, longi: 12.1270241613,
    name: "Kiefersfelden",
    plz: "83088"
  },
  {
    lati: 48.397277679, longi: 12.1322712906,
    name: "Hohenpolding",
    plz: "84432"
  },
  {
    lati: 49.07415728, longi: 12.3850042264,
    name: "Brennberg",
    plz: "93179"
  },
  {
    lati: 48.5441491832, longi: 12.9619923926,
    name: "Johanniskirchen",
    plz: "84381"
  },
  {
    lati: 49.1363011324, longi: 12.9886648795,
    name: "Arnbruck",
    plz: "93471"
  },
  {
    lati: 51.1249797869, longi: 14.2051254544,
    name: "Bischofswerda u.a.",
    plz: "01877"
  },
  {
    lati: 49.5794184339, longi: 12.5249231691,
    name: "Eslarn",
    plz: "92693"
  },
  {
    lati: 51.513135155, longi: 12.3627359286,
    name: "Delitzsch, Krostitz u.a.",
    plz: "04509"
  },
  {
    lati: 48.7813555098, longi: 10.3286959354,
    name: "Neresheim",
    plz: "73450"
  },
  {
    lati: 47.7408514819, longi: 10.3398444219,
    name: "Kempten (Allgäu)",
    plz: "87437"
  },
  {
    lati: 50.2440527068, longi: 10.5656269445,
    name: "Sulzdorf a.d. Lederhecke",
    plz: "97528"
  },
  {
    lati: 52.5021028588, longi: 10.7053115782,
    name: "Bokensdorf",
    plz: "38556"
  },
  {
    lati: 49.6258489901, longi: 8.84310365204,
    name: "Grasellenbach",
    plz: "64689"
  },
  {
    lati: 52.2311550503, longi: 7.29521339587,
    name: "Wettringen",
    plz: "48493"
  },
  {
    lati: 53.2437542331, longi: 7.46148279093,
    name: "Leer",
    plz: "26789"
  },
  {
    lati: 47.9138945049, longi: 7.81940142182,
    name: "Bollschweil",
    plz: "79283"
  },
  {
    lati: 50.084533942, longi: 8.237372179,
    name: "Wiesbaden",
    plz: "65183"
  },
  {
    lati: 54.521705852, longi: 8.6494037568,
    name: "Süderoog, Pellworm",
    plz: "25849"
  },
  {
    lati: 51.5482442754, longi: 7.53551012594,
    name: "Dortmund",
    plz: "44328"
  },
  {
    lati: 50.6164847197, longi: 10.2039325932,
    name: "Wasungen",
    plz: "98634"
  },
  {
    lati: 47.8227801952, longi: 10.5806562174,
    name: "Ruderatshofen",
    plz: "87674"
  },
  {
    lati: 49.4026251507, longi: 7.37100814625,
    name: "Schönenberg-Kübelberg",
    plz: "66901"
  },
  {
    lati: 52.3152705403, longi: 11.1019132439,
    name: "Weferlingen, Behnsdorf, Belsdorf etc",
    plz: "39356"
  },
  {
    lati: 51.5629723063, longi: 12.1999368581,
    name: "Sandersdorf-Brehna",
    plz: "06796"
  },
  {
    lati: 54.3092583261, longi: 13.1208280871,
    name: "Stralsund",
    plz: "18439"
  },
  {
    lati: 48.6926892702, longi: 11.7127182886,
    name: "Aiglsbach",
    plz: "84089"
  },
  {
    lati: 48.0418489691, longi: 7.96221514631,
    name: "Glottertal",
    plz: "79286"
  },
  {
    lati: 53.0033890699, longi: 7.6269652031,
    name: "Esterwegen u.a.",
    plz: "26897"
  },
  {
    lati: 50.4047312157, longi: 7.63349794171,
    name: "Vallendar",
    plz: "56179"
  },
  {
    lati: 52.0421925893, longi: 8.33034454723,
    name: "Halle (Westfalen)",
    plz: "33790"
  },
  {
    lati: 53.7328556301, longi: 9.05078340673,
    name: "Wingst",
    plz: "21789"
  },
  {
    lati: 49.3727147582, longi: 8.69890720373,
    name: "Heidelberg",
    plz: "69126"
  },
  {
    lati: 48.1586788211, longi: 11.5790825643,
    name: "München",
    plz: "80801"
  },
  {
    lati: 51.5182494093, longi: 10.2466122379,
    name: "Duderstadt",
    plz: "37115"
  },
  {
    lati: 54.4305095108, longi: 12.7822523398,
    name: "Zingst a. Darß",
    plz: "18374"
  },
  {
    lati: 52.412751532, longi: 13.5416379237,
    name: "Berlin Altglienicke",
    plz: "12524"
  },
  {
    lati: 50.3834484571, longi: 7.72097724003,
    name: "Eitelborn",
    plz: "56337"
  },
  {
    lati: 49.9240392264, longi: 7.52850697927,
    name: "Tiefenbach u.a.",
    plz: "55471"
  },
  {
    lati: 53.647322029, longi: 9.79230456539,
    name: "Pinneberg",
    plz: "25421"
  },
  {
    lati: 53.5635017093, longi: 9.96802036847,
    name: "Hamburg",
    plz: "20357"
  },
  {
    lati: 53.578183592, longi: 9.99254731606,
    name: "Hamburg",
    plz: "20149"
  },
  {
    lati: 48.8160807266, longi: 10.1761084989,
    name: "Aalen",
    plz: "73432"
  },
  {
    lati: 50.2266025103, longi: 12.3037602757,
    name: "Bad Brambach",
    plz: "08648"
  },
  {
    lati: 52.5016963569, longi: 12.838168624,
    name: "Ketzin",
    plz: "14669"
  },
  {
    lati: 50.5347843382, longi: 13.1357254049,
    name: "Jöhstadt",
    plz: "09477"
  },
  {
    lati: 52.5024969068, longi: 13.4011207661,
    name: "Berlin Kreuzberg",
    plz: "10969"
  },
  {
    lati: 53.5755098187, longi: 10.0788902222,
    name: "Hamburg",
    plz: "22041"
  },
  {
    lati: 48.8898290772, longi: 10.208600873,
    name: "Westhausen",
    plz: "73463"
  },
  {
    lati: 48.897984712, longi: 9.86875615422,
    name: "Eschach, Obergröningen",
    plz: "73569"
  },
  {
    lati: 49.0773192038, longi: 8.84627052643,
    name: "Kürnbach",
    plz: "75057"
  },
  {
    lati: 47.6909425347, longi: 8.97432118571,
    name: "Gaienhofen",
    plz: "78343"
  },
  {
    lati: 50.6394618493, longi: 9.18113243639,
    name: "Feldatal",
    plz: "36325"
  },
  {
    lati: 54.5196839104, longi: 9.21794278064,
    name: "Wester-Ohrstedt",
    plz: "25885"
  },
  {
    lati: 51.8482254718, longi: 12.0521976217,
    name: "Aken (Elbe)",
    plz: "06385"
  },
  {
    lati: 47.7460957811, longi: 11.2315966916,
    name: "Obersöchering",
    plz: "82395"
  },
  {
    lati: 49.3546016375, longi: 8.15519970954,
    name: "Neustadt an der Weinstraße",
    plz: "67433"
  },
  {
    lati: 50.1642774677, longi: 9.10461181069,
    name: "Hasselroth",
    plz: "63594"
  },
  {
    lati: 50.3130733493, longi: 7.44135359362,
    name: "Kobern-Gondorf",
    plz: "56330"
  },
  {
    lati: 48.0967510805, longi: 7.73123810823,
    name: "Eichstetten",
    plz: "79356"
  },
  {
    lati: 54.8034077487, longi: 9.41879579109,
    name: "Flensburg",
    plz: "24939"
  },
  {
    lati: 52.8316633462, longi: 8.31313484771,
    name: "Visbek",
    plz: "49429"
  },
  {
    lati: 53.5976299195, longi: 8.84053517911,
    name: "Geestland",
    plz: "27624"
  },
  {
    lati: 48.6512515696, longi: 11.1953067115,
    name: "Königsmoos",
    plz: "86669"
  },
  {
    lati: 49.7530210463, longi: 12.1160719188,
    name: "Altenstadt a.d. Waldnaab",
    plz: "92665"
  },
  {
    lati: 53.567579754, longi: 9.91206686911,
    name: "Hamburg",
    plz: "22761"
  },
  {
    lati: 52.3039550223, longi: 10.5791009713,
    name: "Braunschweig",
    plz: "38108"
  },
  {
    lati: 49.1663043712, longi: 11.733588386,
    name: "Parsberg",
    plz: "92331"
  },
  {
    lati: 47.8254135504, longi: 12.7576801583,
    name: "Neukirchen am Teisenberg",
    plz: "83364"
  },
  {
    lati: 48.080353397, longi: 11.2019162918,
    name: "Wörthsee",
    plz: "82237"
  },
  {
    lati: 54.0600606546, longi: 11.8890794631,
    name: "Retschow, Admannshagen-Bargeshagen u.a.",
    plz: "18211"
  },
  {
    lati: 53.9088596043, longi: 10.2264932873,
    name: "Wittenborn",
    plz: "23829"
  },
  {
    lati: 52.1290427831, longi: 7.09325617217,
    name: "Heek",
    plz: "48619"
  },
  {
    lati: 50.3469501335, longi: 7.25199303241,
    name: "Kottenheim",
    plz: "56736"
  },
  {
    lati: 53.4040243888, longi: 7.94689962446,
    name: "Zetel",
    plz: "26340"
  },
  {
    lati: 51.3611457874, longi: 12.3597837425,
    name: "Leipzig",
    plz: "04155"
  },
  {
    lati: 49.3582727444, longi: 10.6211115895,
    name: "Weihenzell",
    plz: "91629"
  },
  {
    lati: 51.708672249, longi: 10.6298631094,
    name: "Braunlage",
    plz: "38700"
  },
  {
    lati: 47.7074512749, longi: 10.1951727493,
    name: "Buchenberg",
    plz: "87474"
  },
  {
    lati: 54.2247852622, longi: 9.68415136764,
    name: "Jevenstedt",
    plz: "24808"
  },
  {
    lati: 49.3247260474, longi: 12.7436840121,
    name: "Gleißenberg",
    plz: "93477"
  },
  {
    lati: 54.2825917633, longi: 12.9504057497,
    name: "Niepars",
    plz: "18442"
  },
  {
    lati: 48.320156774, longi: 12.898735249,
    name: "Tann",
    plz: "84367"
  },
  {
    lati: 53.3361623101, longi: 11.4194472482,
    name: "Ludwigslust",
    plz: "19288"
  },
  {
    lati: 47.7546653923, longi: 11.4507096485,
    name: "Bad Heilbrunn",
    plz: "83670"
  },
  {
    lati: 53.6429640943, longi: 11.4082411266,
    name: "Schwerin",
    plz: "19055"
  },
  {
    lati: 50.6665955478, longi: 8.6952403735,
    name: "Lollar",
    plz: "35457"
  },
  {
    lati: 53.0783892569, longi: 8.88520104769,
    name: "Bremen",
    plz: "28329"
  },
  {
    lati: 52.7415005963, longi: 8.28549669274,
    name: "Vechta",
    plz: "49377"
  },
  {
    lati: 49.1829483561, longi: 9.32573579454,
    name: "Eberstadt",
    plz: "74246"
  },
  {
    lati: 53.9776284956, longi: 9.44100373687,
    name: "Oldendorf",
    plz: "25588"
  },
  {
    lati: 49.1534270535, longi: 9.53407549071,
    name: "Pfedelbach",
    plz: "74629"
  },
  {
    lati: 54.4690020154, longi: 9.11233278336,
    name: "Mildstedt",
    plz: "25866"
  },
  {
    lati: 47.6946626513, longi: 9.11537316424,
    name: "Reichenau",
    plz: "78479"
  },
  {
    lati: 50.0569033149, longi: 8.07003983981,
    name: "Kiedrich",
    plz: "65399"
  },
  {
    lati: 50.8826443076, longi: 6.99198710571,
    name: "Köln",
    plz: "50996"
  },
  {
    lati: 50.832753355, longi: 7.43549641094,
    name: "Ruppichteroth",
    plz: "53809"
  },
  {
    lati: 52.0350085501, longi: 13.4841866538,
    name: "Baruth",
    plz: "15837"
  },
  {
    lati: 52.6171007388, longi: 13.307501983,
    name: "Berlin Hermsdorf",
    plz: "13467"
  },
  {
    lati: 51.1133028546, longi: 13.6410370882,
    name: "Radebeul",
    plz: "01445"
  },
  {
    lati: 48.6893306108, longi: 9.42147064039,
    name: "Wernau (Neckar)",
    plz: "73249"
  },
  {
    lati: 53.9992815366, longi: 11.906503329,
    name: "Satow",
    plz: "18239"
  },
  {
    lati: 50.8831686921, longi: 12.5853015957,
    name: "Waldenburg",
    plz: "08396"
  },
  {
    lati: 51.2173320509, longi: 6.9277355113,
    name: "Erkrath",
    plz: "40699"
  },
  {
    lati: 47.823181047, longi: 8.86787299249,
    name: "Volkertshausen",
    plz: "78269"
  },
  {
    lati: 0, longi: 0,
    name: "Rahden",
    plz: "32369"
  },
  {
    lati: 52.7166706864, longi: 13.2674426177,
    name: "Borgsdorf",
    plz: "16556"
  },
  {
    lati: 50.9978020611, longi: 13.6356217419,
    name: "Freital",
    plz: "01705"
  },
  {
    lati: 54.0554086899, longi: 9.19514312367,
    name: "Süderhastedt",
    plz: "25727"
  },
  {
    lati: 50.9898362406, longi: 6.96264758398,
    name: "Köln",
    plz: "50735"
  },
  {
    lati: 48.8174792916, longi: 9.92036371542,
    name: "Böbingen an der Rems",
    plz: "73560"
  },
  {
    lati: 49.2030199768, longi: 9.97713862764,
    name: "Kirchberg an der Jagst",
    plz: "74592"
  },
  {
    lati: 50.6481374783, longi: 9.85548455059,
    name: "Nüsttal",
    plz: "36167"
  },
  {
    lati: 49.3667759541, longi: 10.4011569249,
    name: "Colmberg",
    plz: "91598"
  },
  {
    lati: 52.3888345417, longi: 10.4515881072,
    name: "Adenbüttel",
    plz: "38528"
  },
  {
    lati: 51.7285104878, longi: 11.3261352539,
    name: "Falkenstein",
    plz: "06463"
  },
  {
    lati: 47.8168714643, longi: 8.67262753167,
    name: "Tengen",
    plz: "78250"
  },
  {
    lati: 49.3189030643, longi: 8.86247328861,
    name: "Eschelbronn",
    plz: "74927"
  },
  {
    lati: 49.6366010606, longi: 9.00965154451,
    name: "Erbach",
    plz: "64711"
  },
  {
    lati: 52.4436817692, longi: 9.61680645754,
    name: "Garbsen",
    plz: "30827"
  },
  {
    lati: 52.5034318632, longi: 12.4783771881,
    name: "Havelsee",
    plz: "14798"
  },
  {
    lati: 50.5370680906, longi: 12.5558428245,
    name: "Stützengrün",
    plz: "08328"
  },
  {
    lati: 50.4059642462, longi: 11.2105638065,
    name: "Sonneberg, Judenbach",
    plz: "96515"
  },
  {
    lati: 49.448475993, longi: 11.3326390667,
    name: "Leinburg",
    plz: "91227"
  },
  {
    lati: 49.5980197925, longi: 7.57871653633,
    name: "Rothselberg u.a.",
    plz: "67753"
  },
  {
    lati: 50.3392695361, longi: 7.86791458703,
    name: "Singhofen",
    plz: "56379"
  },
  {
    lati: 47.9035786973, longi: 8.64929641655,
    name: "Geisingen",
    plz: "78187"
  },
  {
    lati: 48.8158403791, longi: 8.8105816927,
    name: "Tiefenbronn",
    plz: "75233"
  },
  {
    lati: 49.5533747312, longi: 8.8462207391,
    name: "Wald-Michelbach",
    plz: "69483"
  },
  {
    lati: 53.0954588112, longi: 8.81086643064,
    name: "Bremen",
    plz: "28215"
  },
  {
    lati: 49.5185341416, longi: 8.28863122593,
    name: "Lambsheim",
    plz: "67245"
  },
  {
    lati: 49.9521471988, longi: 12.0708333578,
    name: "Waldershof",
    plz: "95679"
  },
  {
    lati: 51.45735883, longi: 7.01056563248,
    name: "Essen",
    plz: "45127"
  },
  {
    lati: 50.2095699336, longi: 8.55481578718,
    name: "Oberursel (Taunus)",
    plz: "61440"
  },
  {
    lati: 53.9624575045, longi: 8.98095300358,
    name: "Kronprinzenkoog, Marne u.a.",
    plz: "25709"
  },
  {
    lati: 50.7772048511, longi: 7.9486896376,
    name: "Herdorf",
    plz: "57562"
  },
  {
    lati: 49.9036219856, longi: 8.20287532125,
    name: "Nieder-Olm",
    plz: "55268"
  },
  {
    lati: 50.9149791851, longi: 8.72399585605,
    name: "Wetter",
    plz: "35083"
  },
  {
    lati: 48.3976843532, longi: 8.72314366677,
    name: "Empfingen",
    plz: "72186"
  },
  {
    lati: 52.624295333, longi: 11.7224223385,
    name: "Bismark",
    plz: "39599"
  },
  {
    lati: 53.2681274702, longi: 10.9526060326,
    name: "Amt Neuhaus, Stapel",
    plz: "19273"
  },
  {
    lati: 51.2791616822, longi: 11.2367641852,
    name: "Heldrungen",
    plz: "06577"
  },
  {
    lati: 51.4740807524, longi: 14.7933424811,
    name: "Krauschwitz, Weißkeißel",
    plz: "02957"
  },
  {
    lati: 51.1826365505, longi: 14.9179669346,
    name: "Markersdorf, Neißeaue u.a.",
    plz: "02829"
  },
  {
    lati: 50.9086556548, longi: 8.1606727846,
    name: "Netphen",
    plz: "57250"
  },
  {
    lati: 50.5151966293, longi: 7.50082459349,
    name: "Rengsdorf",
    plz: "56579"
  },
  {
    lati: 49.1761325174, longi: 7.48312386751,
    name: "Vinningen, Trulben, Ruppertsweiler u.a.",
    plz: "66957"
  },
  {
    lati: 47.7209570594, longi: 7.56459411013,
    name: "Bad Bellingen",
    plz: "79415"
  },
  {
    lati: 53.0487662127, longi: 8.53453263927,
    name: "Ganderkesee",
    plz: "27777"
  },
  {
    lati: 48.9120222025, longi: 8.70779077166,
    name: "Pforzheim",
    plz: "75177"
  },
  {
    lati: 49.8048585918, longi: 6.67257791796,
    name: "Trier",
    plz: "54293"
  },
  {
    lati: 51.1350889793, longi: 6.73694626771,
    name: "Neuss",
    plz: "41470"
  },
  {
    lati: 49.9300563176, longi: 10.2528920751,
    name: "Kolitzheim",
    plz: "97509"
  },
  {
    lati: 54.4326267034, longi: 10.0143306841,
    name: "Osdorf",
    plz: "24251"
  },
  {
    lati: 51.3613183384, longi: 11.1243466385,
    name: "Bad Frankenhausen/ Kyffhäuser",
    plz: "06567"
  },
  {
    lati: 53.5327162975, longi: 13.2254043172,
    name: "Neubrandenburg",
    plz: "17033"
  },
  {
    lati: 52.4490700799, longi: 13.3712816799,
    name: "Berlin Mariendorf",
    plz: "12105"
  },
  {
    lati: 48.9308810487, longi: 13.4184846795,
    name: "Sankt Oswald",
    plz: "94568"
  },
  {
    lati: 51.5425274815, longi: 13.8869100009,
    name: "Schipkau",
    plz: "01998"
  },
  {
    lati: 52.3197392819, longi: 7.86198122233,
    name: "Westerkappeln",
    plz: "49492"
  },
  {
    lati: 50.011153624, longi: 8.78446148835,
    name: "Dietzenbach",
    plz: "63128"
  },
  {
    lati: 52.4637439855, longi: 9.6866295346,
    name: "Langenhagen (Flughafen)",
    plz: "30669"
  },
  {
    lati: 53.7010513394, longi: 10.0700331786,
    name: "Tangstedt",
    plz: "22889"
  },
  {
    lati: 48.4258174433, longi: 11.533504088,
    name: "Hohenkammer",
    plz: "85411"
  },
  {
    lati: 50.0575464983, longi: 11.8066610554,
    name: "Bischofsgrün",
    plz: "95493"
  },
  {
    lati: 53.7074889735, longi: 10.3993437319,
    name: "Steinburg",
    plz: "22964"
  },
  {
    lati: 47.8430321946, longi: 12.0476064773,
    name: "Kolbermoor",
    plz: "83059"
  },
  {
    lati: 47.9704297852, longi: 9.58198661351,
    name: "Ebersbach-Musbach",
    plz: "88371"
  }
];
