import { TestBed } from '@angular/core/testing';

import { GeoplzService } from './geoplz.service';

describe('GeoplzService', () => {
  let service: GeoplzService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GeoplzService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
