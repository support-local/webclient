// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyAzpKb-LaCFtn8QMX-_AaI-oiLBVKexruU",
    authDomain: "projectsupportlocal.firebaseapp.com",
    databaseURL: "https://projectsupportlocal.firebaseio.com",
    projectId: "projectsupportlocal",
    storageBucket: "projectsupportlocal.appspot.com",
    messagingSenderId: "154639145161",
    appId: "1:154639145161:web:90ea4f77ef574b93bf1518",
    measurementId: "G-KEQELKRHPB"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
